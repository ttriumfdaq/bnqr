/*-----------------------------------------------------------------------------
 * Copyright (c) 1996      TRIUMF Data Acquistion Group
 * Please leave this header in any reproduction of that distribution
 * 
 * TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 * Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *        amaudruz@triumf.ca
 * ----------------------------------------------------------------------------
 *  
 * Description	: Header file for Pol Synth Module
 *
 * Author: Suzannah Daviel, TRIUMF
 * 
 *---------------------------------------------------------------------------*
 * Initial VMIC Version based on CVS 1.8
 *---------------------------------------------------------------------------*/
#ifndef _PSM_INCLUDE_
#define _PSM_INCLUDE_

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define EXTERNAL extern 

#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED
typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
typedef unsigned int       DWORD;
typedef unsigned int  BOOL;

#define SUCCESS 1
#endif /* MIDAS_TYPE_DEFINED */
#include "vmicvme.h"


/* PSM register offset defines  

          register        offset        bits     access     
            name                        used     type        */
#define MEMORY_BASE          0x00000
/* Data Memory (DM) : offsets from MEMORY_BASE */
#define BASE_DM_1F_IQ             0x00000   /* 10     word    RW  */
#define BASE_DM_3F_IQ             0x02000   /* 10     word    RW  */
#define BASE_DM_5F_IQ             0x04000   /* 10     word    RW  */
#define BASE_DM_FREF_IQ           0x06000   /* 10     word    RW  */
#define IDLE_IQ_OFFSET            0x01FFC   /* 10     word    RW  */
#define BASE_DM_FREQ_SWEEP        0x08000   /* 32     2*word  RW  */
#define FREQ_SWEEP_IDLE           0x08FFC   /* 32     2*word  RW  */

#define NUM_PROFILES      4       /* 4 profiles */

/* Base value of Register sets (1f-fREF) */
#define BASE_CONTROL_REGS    0x09000


/* Profile offsets (4 profiles) */
#define BASE_1F           0x00  /* base address offset of 1F profile */
#define BASE_3F           0x08  /* base address offset of 3F profile */
#define BASE_5F           0x10  /* base address offset of 5F profile */
#define BASE_FREF         0x18  /* base address offset of freq profile */


/* Profile Control Register offsets  */
#define PROFILE_CONTROL_REG_1       0x00      /*  8     byte    RW  */
#define PROFILE_CONTROL_REG_2       0x01      /*  8     byte    RW  */
#define PROFILE_CONTROL_REG_3       0x02      /*  8     byte    RW  */
#define PROFILE_OUT_SCALE_FAC       0x03      /*  8     byte    RW     Reg 4 */
#define PROFILE_IQ_DM_LENGTH       0x04       /* 16     word    RW     Reg 5 */ 
#define PROFILE_IQ_DM_ADDRESS      0x06       /* 16     word    R      Reg 6 */

/* Profile Control Register Numbers 1-6 for used with psmWriteProfileReg*/
#define PROFILE_REG1 1 
#define PROFILE_REG2 2
#define PROFILE_REG3 3
#define PROFILE_REG4 4
#define PROFILE_REG5 5
#define PROFILE_REG6 6

#define FREF_FREQ_MSW       0x20      /* 16     word    RW  */
#define FREF_FREQ_LSW       0x22      /* 16     word    RW  */

/* Module Control Registers (offset from BASE_CONTROL_REGS    0x09000 */

#define FREQ_SWEEP_LENGTH   0x24      /* 16     word    RW  */
#define FREQ_SWEEP_ADDRS    0x26      /* 16     word    R   */

#define END_SWEEP_CONTROL     0x28      /*  8     byte    RW  */
#define RF_TRIP_THRESHOLD     0x29      /*  8     byte    RW  */
#define FREQ_SWEEP_INT_STROBE 0x2A      /*  8     byte    RW  */
#define FREQ_SWEEP_ADDR_RESET 0x2B      /*  8     byte    RW  */
#define GATE_CONTROL_REG      0x2C      /*  8     byte    RW  */
#define RF_TRIP_STATUS_RESET  0x2D      /*  8     byte    RW  */
#define PSM_VME_RESET         0x2E      /*  8     byte    RW  */
#define MAX_BUF_FACTOR_NCMX   0x30      /*  8     byte    RW  */
#define BUF_FACTOR_1F         0x31      /*  8     byte    RW  */
#define BUF_FACTOR_3F         0x32      /*  8     byte    RW  */
#define BUF_FACTOR_5F         0x33      /*  8     byte    RW  */
#define BUF_FACTOR_FREF       0x34      /*  8     byte    RW  */

/* bits and masks : profiles */

/* PROFILE control reg 1 */
#define REG1_MASK 0x3F
#define PLL_LOCK_CONTROL  0x20 
#define REF_CLOCK_MULTIPLIER 0x1F

/* PROFILE control reg 2 */
#define REG2_MASK 0xCD  /* Valid bits */
#define SINGLE_TONE_MODE 0x1
#define AUTO_POWERDOWN 0x4
#define FULL_SLEEP_MODE 0x8
#define INVERSE_SINC_BYPASS 0x40
#define CIC_CLEAR 0x80
#define INVERSE_CIC_BYPASS 0x1
#define SPECTRAL_INVERT 0x2
#define CIC_INTERPOLATION_RATE 0xFC
#define IQ_DATA_MEMORY_LENGTH 0x7FF
#define IQ_DATA_MEMORY_ADDRESS 0x7FF

#define FREQ_SWEEP_MASK 0x3FF /* 10bits for length and address */
#define END_SWEEP_CNTRL_MASK 0x1F
#define RF_TRIP_STATUS_RESET_MASK 1
#define I_Q_DATA_MASK 0x3FF /* 10 bits */

/* Gate Control Register */
#define FP_GATE_DISABLED 0
#define GATE_NORMAL_MODE 1
#define GATE_PULSE_INVERTED 2
#define INTERNAL_GATE 3

/* Buffer Factor Registers (0x30) (max=32) */
#define NC_MASK  0x3F

//parameters temp test only

struct profile_reg_params
{
  INT profile_offset;
  INT registers[6];
  INT ref_clock_multiplier;
  BOOL lock_control; 
  BOOL operating_mode; 
  BOOL auto_powerdown; 
  BOOL full_sleep_mode;
  BOOL inverse_sinc_bypass;
  BOOL cic_clear;
  BOOL inverse_cic_bypass;
  BOOL spectral_invert;
  BOOL cic_filters_bypassed;
  INT cic_interpolation_rate;
  INT output_multiplier;
  INT IQ_data_memory_length;
  INT IQ_data_memory_address;
  BOOL end_sweep_mode; /* c.f. end sweep control reg */
  INT gate_control; /* from gate_control reg */
  DWORD I_idle; /* I,Q pair idle values (2's compliment) */
  DWORD Q_idle;
  DWORD buffer_factor;
};
typedef struct profile_reg_params PROFILE_REG_PARAMS;


struct four_profiles
{
  PROFILE_REG_PARAMS profile_reg_1f;
  PROFILE_REG_PARAMS profile_reg_3f;
  PROFILE_REG_PARAMS profile_reg_5f;
  PROFILE_REG_PARAMS profile_reg_fREF;
};
typedef struct  four_profiles FOUR_PROFILES;

struct profile_base_regs
{
  DWORD control_base[NUM_PROFILES];
  DWORD dm_base[NUM_PROFILES];
  DWORD control_reg_offset[6];
};
typedef struct  profile_base_regs PROFILE_BASE_REGS;

/* trPSM.c prototypes:    */
// VMIC read and write
uint32_t regRead(MVME_INTERFACE *mvme, DWORD base, int offset);
void regWrite(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value);
uint32_t regRead8(MVME_INTERFACE *mvme, DWORD base, int offset);
void regWrite8(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value);

/* Access the Control Area (Table 2, > 0x9000 ) */
INT  psmRegRead(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD reg_offset);
INT  psmRegWrite(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD reg_offset, const WORD value);
INT  psmRegWrite8(MVME_INTERFACE *mvme, const DWORD base_adr, const DWORD reg_offset, const BYTE value);
INT  psmRegRead8(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD reg_offset);

void psmGetStatus(MVME_INTERFACE *mvme, const DWORD base_addr, FILE *fout);
void psmInitTest(MVME_INTERFACE *mvme, const DWORD base_adr);


/* using profile areas (1f,3f,5f,Fref) */ 
void psmReadProfile( MVME_INTERFACE *mvme, DWORD base_addr, PROFILE_REG_PARAMS *p_profile_params);
void psmPrintProfiles(FOUR_PROFILES *p_four_profiles, FILE *fout);
void psmReadProfilsetscae(MVME_INTERFACE *mvme, const DWORD base_addr, PROFILE_REG_PARAMS *p_profile_params);
INT psmWriteProfileReg(MVME_INTERFACE *mvme, const DWORD base_addr, char *profile_code, INT my_reg, INT value );
INT psmReadProfileReg(MVME_INTERFACE *mvme, const DWORD base_addr, char *profile_code, INT my_reg);
INT psmSetQM_Off (MVME_INTERFACE *mvme, const DWORD base_addr);
INT psmSetQM_On (MVME_INTERFACE *mvme, const DWORD base_addr);
INT psmReadIQptr (MVME_INTERFACE *mvme, const DWORD base_addr,  char *profile_code);
INT psmReadIQlen (MVME_INTERFACE *mvme, const DWORD base_addr, char *profile_code);
INT psmSetScaleFactor (MVME_INTERFACE *mvme, const DWORD base_addr,  char *profile_code, DWORD value);
INT psmSetCIC_Rate_all (MVME_INTERFACE *mvme, const DWORD base_addr, DWORD ival);
INT psmSetCIC_Rate ( MVME_INTERFACE *mvme, DWORD base_addr, char *profile, DWORD ival);
INT psmWriteGateControl( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code,  
			 INT gate_code);
INT psmReadProfileBufFactor( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code, 
			     BOOL *identical);
INT psmWriteProfileBufFactor( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code, WORD Ncmx);
/* read/write to profile-independent control regs */
void psmReadControlRegs(MVME_INTERFACE *mvme, const DWORD base_addr,FILE *fout);
INT psmWrite_fREF_freq(MVME_INTERFACE *mvme, const DWORD base_addr, const DWORD freq);
INT psmWrite_fREF_freq_Hz( MVME_INTERFACE *mvme, DWORD base_addr, const DWORD freq_Hz);
INT psmReadMaxBufFactor( MVME_INTERFACE *mvme, DWORD base_addr );
/* resets */
INT psmFreqSweepMemAddrReset(MVME_INTERFACE *mvme, const DWORD base_addr);
INT psmSweepStrobe(MVME_INTERFACE *mvme, const DWORD base_addr);
INT psmVMEReset(MVME_INTERFACE *mvme, const DWORD base_addr);
INT psmClearRFpowerTrip(MVME_INTERFACE *mvme, const DWORD base_addr);
INT psmFreqSweepStrobe( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmReadRFpowerTrip( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmWriteRFpowerTripThresh ( MVME_INTERFACE *mvme,  DWORD base_addr, float Vtrip);
float psmReadRFpowerTripThresh( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmTryTwoFreq( MVME_INTERFACE *mvme, DWORD base_addr, DWORD Idle1_freq_Hz, DWORD Idle2_freq_Hz, DWORD fref_Hz) ;
INT psmFreqWriteEndSweepMode( MVME_INTERFACE *mvme, DWORD base_addr, INT end_sweep_bit);
/* access the DM area 0000 - 8FFF  */
INT  psmRegReadDM(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD reg_offset);
INT  psmRegWriteDM(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD reg_offset, const WORD value);
INT psmRead_oneIQ(MVME_INTERFACE *mvme,  const DWORD base_adr,  char * p_profile_code, DWORD *pI, DWORD *pQ,  DWORD memadd, BOOL twos_comp);
INT psmLoadFreqDM_ptr( MVME_INTERFACE *mvme, DWORD base_addr, INT *pfreq , INT nfreq, DWORD *first_freq);
INT psmReadOneFreq( MVME_INTERFACE *mvme, DWORD base_addr, DWORD memadd);
INT psmLoad_oneIQ(MVME_INTERFACE *mvme,  const DWORD base_adr, char * p_profile_code, DWORD I, DWORD Q, DWORD memadd, BOOL twos_comp);
INT psmLoad_oneFreq(MVME_INTERFACE *mvme,  const DWORD base_adr, DWORD freq, DWORD memadd);
INT psmLoadIQ_DM(MVME_INTERFACE *mvme,  const DWORD base_adr, char *file , char *profile_code, 
			DWORD *nvalues,DWORD *first_i, DWORD *first_q);
INT psmLoadIQ_DM_all(MVME_INTERFACE *mvme,  const DWORD base_adr, char *file , 
			    DWORD *nvalues,DWORD *first_i, DWORD *first_q);
INT psmReadIQ_DM(MVME_INTERFACE *mvme, const DWORD base_adr,  char * p_profile_code, DWORD *pI, DWORD *pQ, 
		     DWORD memadd, INT length, BOOL *diff_flag, BOOL print, char *loadfile);
INT psmLoadFreqDM(MVME_INTERFACE *mvme, const DWORD base_adr, char * file , DWORD *ninc, DWORD *first_freq);
INT psmLoadIdleFreq (MVME_INTERFACE *mvme, const DWORD base_adr, DWORD freq);
INT psmReadIdleFreq (MVME_INTERFACE *mvme, const DWORD base_adr);
INT psmReadIdleFreq_Hz ( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmLoadIdleIQ (MVME_INTERFACE *mvme, const DWORD base_adr, char *p_profile_code,  DWORD I, DWORD Q, BOOL twos_comp);
INT psmReadIdleIQ (MVME_INTERFACE *mvme, const DWORD base_adr, char *p_profile_code,  DWORD *I, DWORD *Q, BOOL twos_comp);
INT psmLoadIdleFreq_Hz ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq_Hz);
INT psmFreqSweepReadLength( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmFreqSweepReadAddress( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmTryIQ (MVME_INTERFACE *mvme, const DWORD base_addr, char * freq_filename, char * IQ_filename);
INT psmIQWriteEndSweepMode( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code,  
			     INT end_sweep_bit);
INT psmSetIQlen ( MVME_INTERFACE *mvme, DWORD base_addr, char * p_profile_code,  DWORD value);
INT psmLoadOneIQ( MVME_INTERFACE *mvme, DWORD base_addr, char * p_profile_code, DWORD I, DWORD Q, DWORD memadd, BOOL twos_comp);
INT psmReadOneIQ( MVME_INTERFACE *mvme, DWORD base_addr,  char * p_profile_code, DWORD *pI, DWORD *pQ,  DWORD memadd, BOOL *diff_flag, BOOL twos_comp);
INT psmLoadOneFreq( MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq, DWORD memadd);
void init(void);
//int main(MVME_INTERFACE *mvme, const DWORD base_adr);
INT  check_profile(char *p_profile_code);
INT psmTryOneFreq(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD freq_Hz, DWORD fref_Hz);
void psmTryScan( MVME_INTERFACE *mvme,  const DWORD base_adr, DWORD fref_Hz, BOOL jump_to_idle, char *filename);
INT psmSetOneFreq_Hz( MVME_INTERFACE *mvme, DWORD base_addr, DWORD Idle_freq_Hz, DWORD fref_Hz) ; // test procedure
// others
INT get_index(char *p_profile_code, INT *pindex);
INT bitset(INT data, INT bit, INT val);
INT TwosComp_convert(INT value, BOOL twos_comp);
void dump(MVME_INTERFACE *mvme,  DWORD base_addr);
int get_profile_code(char *code);
INT get_control_register(INT *reg);
void psm(const DWORD base_addr);
INT check_identical(void);
void print_data(char *code);
#endif



