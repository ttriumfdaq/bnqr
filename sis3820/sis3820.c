
/*********************************************************************

  Name:         sis3820.c
  Created by:   K.Olchanski

  Contents:     SIS3820 32-channel 32-bit multiscaler
                
  $Log: sis3820.c,v $
  Revision 1.2  2018/09/14 23:28:29  suz
  add test3 (real inputs) and rearrange other tests

  Revision 1.1  2006/05/25 05:53:42  alpha
  First commit

*********************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h> // pow
#include "sis3820drv.h"
#include "sis3820.h"
//#include "mvmestd.h"
#include "unistd.h" // for sleep
/*****************************************************************/

uint32_t write_reg (MVME_INTERFACE *mvme,  DWORD SIS3820_BASE,  uint32_t register, uint32_t data );
uint32_t read_reg (MVME_INTERFACE *mvme,  DWORD SIS3820_BASE,  uint32_t register);
void sis3820_CSR0(MVME_INTERFACE *mvme, DWORD base);
uint32_t read_copy_disable_reg(MVME_INTERFACE *mvme, DWORD base);
uint32_t write_copy_disable_reg(MVME_INTERFACE *mvme, DWORD base, DWORD bitpat);
uint32_t read_operation_mode_reg(MVME_INTERFACE *mvme, DWORD base);
void decode_operation_mode_reg(DWORD data, DWORD data1);
void decode_csr0_register(DWORD bitpat);
#ifdef MAIN_ENABLE
#define DATA_MASK         0xffffff   // needed for test3
#define SIS_FIFO_SIZE  4096   // Actually 64 * 1024      needed for test3
int read_SIS(MVME_INTERFACE *mvme, DWORD *pfifo , DWORD base , int toRead32);
void test3 (MVME_INTERFACE *mvme, DWORD base);
#define fsiz 10000  // needed for test3
void test2 (MVME_INTERFACE *mvme, DWORD base);
void test1 (MVME_INTERFACE *mvme, DWORD base);
int dsis;
#endif // MAIN_ENABLE
/*
Read sis3820 register value
*/
static uint32_t regRead(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A32);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  return mvme_read_value(mvme, base + offset);
}

/*****************************************************************/
/*
Write sis3820 register value
*/
static void regWrite(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A32);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  mvme_write_value(mvme, base + offset, value);
}

/*****************************************************************/
uint32_t sis3820_RegisterRead(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  return regRead(mvme, base, offset);
}

/*****************************************************************/
void     sis3820_RegisterWrite(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  regWrite(mvme,base,offset,value);
}

/*****************************************************************/
uint32_t sis3820_ScalerRead(MVME_INTERFACE *mvme, DWORD base, int ch)
{
  if (ch == 0)
    return regRead(mvme, base, SIS3820_COUNTER_CH1+ch*4);
  else
    return regRead(mvme, base, SIS3820_COUNTER_SHADOW_CH1+ch*4);
}

/*****************************************************************/
uint32_t write_copy_disable_reg(MVME_INTERFACE *mvme, DWORD base, DWORD bitpat)
{
  uint32_t reg, data;
  reg  =  SIS3820_COPY_DISABLE;
  data =write_reg(mvme, base, reg, (uint32_t)bitpat);
  return data;
}
/*****************************************************************/
uint32_t read_copy_disable_reg(MVME_INTERFACE *mvme, DWORD base)
{
  uint32_t data,reg,bit;
  int i;
  int first=1;

  reg  =  SIS3820_COPY_DISABLE;
  data = read_reg(mvme, base, reg);
  printf("Disabled channels bitpattern is 0x%x\n",data);
  if(data==0)
    {
      printf("No channels are disabled\n");
      return data;
    }
  printf("Disabled channels (1-31) are: ");
  for (i=0; i<32; i++)
    {
      bit = 1 << i;
      if (data & bit)
	{
	  if(first)
	    {
	      printf("%d",i+1);
	      first=0;
	    }
	  else
	    printf(",%d",i+1);
	}
    }
  printf("\n");
  return data;
}
/*****************************************************************/
uint32_t read_operation_mode_reg(MVME_INTERFACE *mvme, DWORD base)
{
  uint32_t data,data1,reg;
  
  reg  =  SIS3820_OPERATION_MODE;
  printf("Operation Mode Reg at Addr 0x%3.3x : ",reg);
  data = read_reg(mvme, base, reg);



  reg  =  SIS3820_LNE_PRESCALE;
  printf("Prescaling Reg     at Addr 0x%3.3x : ",reg); // read prescale reg in case of prescaling 
  data1 = read_reg(mvme, base, reg);

  decode_operation_mode_reg(data,data1);

  return data;
}

void decode_operation_mode_reg(DWORD data, DWORD data1)
{
  uint32_t bits;
  uint32_t data_format_mask=0xC;
  uint32_t lne_source_mask=0x70;
  uint32_t sdram_mode_mask=0x3000;
  uint32_t input_mode_mask=0x70000;
  uint32_t output_mode_mask=0x100000;
  uint32_t operation_mode_mask=0x70000000;
 
  printf("\nOperation Mode register contents: 0x%lx   Prescaling register contents: 0x%lx\n\n",data,data1);  
  printf("\nClearing Mode: %s",(data &  SIS3820_CLEARING_MODE) ? "N" : "Y");

  // Data Format
  bits = data & data_format_mask;
  printf("   Data Format  : ");
  if(bits == SIS3820_MCS_DATA_FORMAT_32BIT) // 0
    printf("32 bit\n");
  else if (bits & SIS3820_MCS_DATA_FORMAT_24BIT)
    printf("24 Bit\n");
  else if (bits & SIS3820_MCS_DATA_FORMAT_16BIT)
    printf("16 Bit\n");
  else if (bits & SIS3820_MCS_DATA_FORMAT_8BIT)
    printf("8 Bit\n");

  // LNE Source
  bits = data & lne_source_mask;
  printf("LNE Source  : ");
  if (bits == SIS3820_LNE_SOURCE_VME) //0
    printf("VME");
  else if (bits & SIS3820_LNE_SOURCE_CONTROL_SIGNAL)
    printf("Front Panel");
  else if (bits & SIS3820_LNE_SOURCE_INTERNAL_10MHZ)
    printf("10MHz Internal Pulser");
  else if (bits & SIS3820_LNE_SOURCE_CHANNEL_N)
    printf("Channel N");
  else if (bits & SIS3820_LNE_SOURCE_PRESET)
    printf("Preset Scaler N");

  if( (bits & SIS3820_LNE_SOURCE_CONTROL_SIGNAL) ||  (bits & SIS3820_LNE_SOURCE_PRESET))
    printf (";  LNE can NOT be prescaled in this mode\n");
  
  else
    { 
      printf (";  LNE CAN be prescaled in this mode  ");
      // Prescale register contents
      if(data1 == 0)
	printf(": No prescaling\n");
      else
	printf(": Prescale factor : %ld\n",data1);
    }

  // ARM/ENABLE  1 bit active only
  
 printf("Arm/enable source: %s\n",(data &  SIS3820_ARM_ENABLE_CHANNEL_N) ? "ChannelN" : "LNE Front Panel");

 // SDRAM mode
  bits = data & sdram_mode_mask;
  printf("SDRAM mode  : ");
  if(bits == SIS3820_FIFO_MODE) // 0
    printf("FIFO Emulation\n");
  else
    {
      if (bits & SIS3820_SDRAM_MODE )
	printf("SDRAM mode selected\n");
      else if (bits & SIS3820_SDRAM_ADD_MODE)
	printf("SDRAM add mode selected\n");
      else if (bits & SIS3820_HISCAL_START_SOURCE_VME)
	printf("HISCAL VME start source selected\n");
      else if  (bits & SIS3820_HISCAL_START_SOURCE_EXTERN )
	printf("HISCAL EXTERN start source selected\n");
    }

  // INPUT Mode
 bits = data & input_mode_mask;
 // printf("Input mode bits:  0x%x\n", bits );
 if(bits == SIS3820_CONTROL_INPUT_MODE0) // 0
   printf("Input  Mode 0 selected");
 else if (bits & SIS3820_CONTROL_INPUT_MODE1)
   printf("Input  Mode 1 selected");
 else if (bits & SIS3820_CONTROL_INPUT_MODE2)
   printf("Input  Mode 2 selected");
 else if (bits & SIS3820_CONTROL_INPUT_MODE3)
   printf("Input  Mode 3 selected");
 else if (bits & SIS3820_CONTROL_INPUT_MODE4)
   printf("Input  Mode 4 selected");
 else if (bits & SIS3820_CONTROL_INPUT_MODE5)
   printf("Input  Mode 5 selected");

 if(data & SIS3820_CONTROL_INPUTS_INVERT)
   printf("   Control Inputs  ARE inverted\n");
 else
   printf("   Control Inputs  are NOT inverted\n");


  // OUTPUT Mode
 bits = data & output_mode_mask;
 // printf("Output mode bits:  0x%x\n", bits );
 if(bits == SIS3820_CONTROL_OUTPUT_MODE0) // 0
   printf("Output Mode 0 selected");
 else if (bits & SIS3820_CONTROL_OUTPUT_MODE1)
   printf("Output Mode 1 selected");

 if(data & SIS3820_CONTROL_OUTPUTS_INVERT)
   printf("   Control Outputs ARE inverted\n");
 else
   printf("   Control Outputs are NOT inverted\n");


  // Operation Mode
 bits = data & operation_mode_mask;
 // printf("Operation mode bits:  0x%x\n", bits );

 if(bits == SIS3820_OP_MODE_SCALER) // 0
   printf("Operation Mode SCALER selected\n");
 else if(bits & SIS3820_OP_MODE_MULTI_CHANNEL_SCALER)
   printf("Operation Mode MULTI CHANNEL SCALER selected\n");
 else if(bits & SIS3820_OP_MODE_VME_FIFO_WRITE)
   printf("Operation TEST Mode VME FIFO WRITE selected\n");


  printf("\n");
  return;
}
/*****************************************************************/
/*
Read nentry of data from the data buffer. Will use the DMA engine
if size is larger then 127 bytes. 
*/
int sis3820_FifoRead(MVME_INTERFACE *mvme, DWORD base, void *pdest, int wcount)
{
  int rd;
  int save_am;

    mvme_get_am(mvme,&save_am);
    //  mvme_set_blt(  mvme, MVME_BLT_MBLT64);
    // mvme_set_am(   mvme, MVME_AM_A32_D64);

    rd = mvme_set_blt(  mvme, MVME_BLT_NONE); // SD  DMA not working
    mvme_read(mvme, pdest, base + SIS3820_FIFO_BASE, wcount*4);
    //  printf("fifo read wcount: %d, rd: %d\n", wcount, rd);
    mvme_set_am(mvme, save_am);


  return wcount;
}

int sis3820_sdramRead(MVME_INTERFACE *mvme, DWORD base, void *pdest, int wcount)
{
  int rd=0;
  int save_am;

    mvme_get_am(mvme,&save_am);
    mvme_set_blt(  mvme, MVME_BLT_MBLT64);
    mvme_set_am(   mvme, MVME_AM_A32_D64);
  printf("sdram wcount: %d\n", wcount);
  rd = mvme_read(mvme, pdest, base + SIS3820_SDRAM_BASE, wcount*4);
  printf("sdram read wcount: %d, rd: %d\n", wcount, rd);
  mvme_set_am(mvme, save_am);

  if(rd < 0) 
    return -1;
  else
    return wcount;
}


/*****************************************************************/
void sis3820_Reset(MVME_INTERFACE *mvme, DWORD base)
{
  regWrite(mvme,base,SIS3820_KEY_RESET,0);
}

/*****************************************************************/
int  sis3820_DataReady(MVME_INTERFACE *mvme, DWORD base)
{
  return regRead(mvme,base,SIS3820_FIFO_WORDCOUNTER);
}

/*****************************************************************/
void  sis3820_Status(MVME_INTERFACE *mvme, DWORD base)
{

  printf("SIS3820 at A32 0x%x\n", (int)base);
  printf("Registers:\n");
  printf("   ModuleID and Firmware Addr 0x%3.3x : 0x%x\n", 
	 SIS3820_MODID, regRead(mvme,base,SIS3820_MODID));
  printf("   CSR0                  Addr 0x%3.3x : 0x%x\n", 
	 SIS3820_CONTROL_STATUS, regRead(mvme,base,SIS3820_CONTROL_STATUS));
  printf("   Operation mode        Addr 0x%3.3x : 0x%x\n", 
	 SIS3820_OPERATION_MODE, regRead(mvme,base,SIS3820_OPERATION_MODE));
  printf("   Inhibit/Count disable Addr 0x%3.3x : 0x%x\n", 
	 SIS3820_COUNTER_INHIBIT, regRead(mvme, base, SIS3820_COUNTER_INHIBIT));
  printf("   Counter Overflow      Addr 0x%3.3x : 0x%x\n", 
	 SIS3820_COUNTER_OVERFLOW,regRead(mvme, base, SIS3820_COUNTER_OVERFLOW));
}

void sis3820_CSR0(MVME_INTERFACE *mvme, DWORD base)
{
  uint32_t bitpat;

  bitpat = regRead(mvme,base,SIS3820_CONTROL_STATUS);
  printf("Control/Status Reg at Addr 0x%3.3x : 0x%x\n",SIS3820_CONTROL_STATUS, bitpat);
  decode_csr0_register(bitpat);
  return;
}

void decode_csr0_register(DWORD bitpat)
{
  char onof[2][4]={"off","on"};
  char enab[2][10]={"disabled","enabled"};
  char acti[2][11]={"NOT active","active"};
  char arm[2][10]={"NOT armed","Armed"}; 
  char set[2][6]={"set","clear"}; 
  char tmp[30];

  printf("Decoding Control/Status Reg at Addr 0x%3.3x : 0x%lx\n\n",SIS3820_CONTROL_STATUS, bitpat);

  sprintf (tmp, "User LED %s",onof[((bitpat & 0x1 ) ? 1:0)]); // bit 0
  printf ("%-25s",tmp);
  sprintf (tmp,"25MHz Pulses %s", enab[((bitpat & 0x10) ? 1:0)]); // bit 4
  printf ("%-25s\n",tmp);
  sprintf (tmp,"Cntr TestMode %s",enab[((bitpat & 0x20) ? 1:0)]); // bit 5
  printf ("%-25s",tmp);
  sprintf (tmp,"50MHz RefCh1 %s", enab[((bitpat & 0x40) ? 1:0)]); // bit 6
  printf ("%-25s\n",tmp);
  sprintf (tmp,"Sclr enable %s", acti[((bitpat & 0x10000) ? 1:0)]); // bit 16
  printf ("%-25s",tmp);
  sprintf (tmp,"MCS enable %s", acti[((bitpat & 0x40000) ? 1:0)]); // bit 18
  printf ("%-25s\n",tmp);
  sprintf (tmp,"SDRAM/FIFO test %s", enab[((bitpat & 0x800000) ? 1:0)]); // bit 23
  printf ("%-25s",tmp);
  printf ("%-10s\n", arm[((bitpat & 0x1000000) ? 1:0)]); // bit 24
  sprintf (tmp,"HISCAL %s", enab[((bitpat & 0x2000000) ? 1:0)]); // bit 25
  printf ("%-25s",tmp);  sprintf (tmp,"Overflow %s", set[((bitpat & 0x8000000) ? 1:0)]); // bit 27
  printf("\nStatus of External Input and Latch bits depend on Input Mode:\n"); 
  sprintf (tmp,"Status of External Input bits %ld", ((bitpat & 0x30000000) >> 28) ); // bits 28,29
  printf ("%-25s\n",tmp);
  sprintf (tmp,"Status of External Latch bits %ld\n\n", ((bitpat & 0xC0000000) >> 30) ); // bits 30,31
  printf ("%-25s\n",tmp);  
  return;
}

void sis3820(void)
{
  printf("\nsis3820 commands: \n");
  printf("B read Operation Mode reg    C read  Control/Status reg CSR0\n");
  printf("D read copy disable reg      E write copy disable reg\n");
  printf("F read reg                   G write reg\n");
  printf("I read ACQ PRESET then ACQ COUNT reg 5 times (10ms intervals)\n");
  printf("J issue LNE                  K enable operation/counting\n");
  printf("L disable operation/counting M arm in MCS mode\n");
  printf("N Test3 ext LNE with PPG     O Test1 internal 10 MHz clock as LNE\n");
  printf("P print this list            Q Test2 \n");     
  printf("R reset module               S status\n");   
  printf("X quit\n");
  printf("Z decode entered values of Operation Mode reg, CSR0 reg\n");
 
}

uint32_t write_reg (  MVME_INTERFACE *mvme, DWORD base, uint32_t reg, uint32_t data )
{

  printf ("\nWriting 0x%x to reg 0x%x\n", data,  reg); 
  sis3820_RegisterWrite(mvme, base, reg, data);
  data = sis3820_RegisterRead(mvme, base, reg);
  printf ("Read back  0x%x from reg 0x%x\n", data,  reg);
  return data;
}
uint32_t read_reg (  MVME_INTERFACE *mvme, DWORD base, uint32_t reg )
{
  uint32_t data;
  data = sis3820_RegisterRead(mvme, base, reg);
  printf ("Read back  0x%x from reg 0x%x\n", data,  reg);
  return data;
}

#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {
  int s,status;
  uint32_t SIS3820_BASE  = 0x38000000;
  MVME_INTERFACE *myvme;
  DWORD offset, value,data;
  char cmd[]="hallo";
  dsis=0; // debug

 if (argc>1) {
    sscanf(argv[1],"%x", &SIS3820_BASE);
  }

 // Test under vmic
  status = mvme_open(&myvme, 0);
  //  sis3820_Reset(myvme,  SIS3820_BASE);

  printf("Base Address is 0x%x\n",(unsigned int)SIS3820_BASE);
  printf("To change Base Address, specify base address on command line\n");

  sis3820_Status(myvme,  SIS3820_BASE);


  sis3820();
  while ( isalpha(cmd[0]) ||  isdigit(cmd[0]) )
    {
      printf("\nEnter command (A-Z) X to exit, P Print commands?  ");
      scanf("%s",cmd);
      //  printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];


      switch(s)
	{
	case('Z') : // decode registers
	  {
	    printf("Decode Operation Register? y/n ");
	    scanf("%s",cmd);
	    cmd[0]=toupper(cmd[0]);
	    if(strncmp(cmd,"Y",1)==0)
	      { 
		printf("Enter value of Operation Register to decode 0x");
		scanf("%x",(unsigned int *)&value);
		printf("Enter value of Prescale Register (0=no prescale) 0x");
		scanf("%x",(unsigned int *)&data);
		decode_operation_mode_reg(value,data);
	      }

	    printf("Decode Control/Status Register? y/n ");
	    scanf("%s",cmd);
	    cmd[0]=toupper(cmd[0]);
	    if(strncmp(cmd,"Y",1)==0)
	      { 
		printf("Enter value of CSR Register to decode 0x");
		scanf("%x",(unsigned int *)&value);
		decode_csr0_register(value);
	      }
	    break;

	  }
	case ('B'):  // Read Op Mode reg
	  {
	     read_operation_mode_reg(myvme, SIS3820_BASE );
	    break;
	  }
	case ('C'):  // Read CSR 0
	  {
	     sis3820_CSR0(myvme, SIS3820_BASE );
	    break;
	  }
	case ('D'):  // Read copy disable reg
	  {
	    read_copy_disable_reg(myvme, SIS3820_BASE );
	    break;
	  }
	case ('E'):  // Write copy disable reg
	  {
	    printf("\nEnter bit pattern in hex 0x ?  ");
	    scanf("%x",(unsigned int *)&value);
	    write_copy_disable_reg(myvme, SIS3820_BASE, value );
	    break;
	  }
	case ('F'):  // Read  reg
	  {
	    printf("\nEnter register offset in hex 0x ?  ");
	    scanf("%x",(unsigned int *)&offset);
	    value = sis3820_RegisterRead(myvme, SIS3820_BASE, (uint32_t)offset );
	    printf("Read back value 0x%x (%d) at reg offset 0x%x \n",
		   (unsigned int)value,(int)value, (unsigned int)offset); 
	    break;
	  }
	case ('G'):  // Write  reg
	  {
	    printf("\nEnter register offset in hex 0x ?  ");
	    scanf("%x",(unsigned int *)&offset);
	    printf("\nEnter value to write in hex 0x ?  ");
	    scanf("%x",(unsigned int *)&value);

	    sis3820_RegisterWrite (myvme, SIS3820_BASE,(uint32_t)offset, (uint32_t)value );
	    break;
	  }

	case ('I'): // Reading Acquistion Preset Reg followed by Count Register 5 times (every 50ms)
	  {
	    uint32_t my_data=99999;
	    uint32_t my_preset;
	    my_preset = sis3820_RegisterRead(myvme, SIS3820_BASE, 
					     0x10 ); //SIS3820_ACQUISITION_PRESET
	    printf("Read Preset value as 0x%x or %d\n",my_preset,my_preset);
	    printf("Writes value only if changed from last read\n");
	    // int kk=1000;
	    int kk=5;
	    while(kk>0)
	      {		
		value = sis3820_RegisterRead(myvme, SIS3820_BASE, 
					     0x14 ); //SIS3820_ACQUISITION_COUNT
		if(value != my_data)
		  {
		    if(value == my_preset)
		      printf("Read back 0x%x (%d)   Reached preset value\n",
			     (unsigned int)value,(int)value); 
		    else
		      printf("Read back 0x%x (%d)\n ",
			     (unsigned int)value,(int)value); 
		    my_data=value;
		  }
		kk--;
		sleep (0.050);		  
	      }
	    break;
	  }

	case ('J'):
	  {
	    // issue LNE
	    sis3820_RegisterWrite (myvme, SIS3820_BASE,SIS3820_KEY_LNE_PULS, 1 );
	    break;
	  }

	case ('K'):
	  {
	    // enable
	    sis3820_RegisterWrite (myvme, SIS3820_BASE,SIS3820_KEY_OPERATION_ENABLE, 1 );
	    break;
	  }

	case ('L'):
	  {
	    // disable
	    sis3820_RegisterWrite (myvme, SIS3820_BASE,SIS3820_KEY_OPERATION_DISABLE, 1 );
	    break;
	  }

	case ('M'):
	  {
	    // arm
	    sis3820_RegisterWrite (myvme, SIS3820_BASE,SIS3820_KEY_OPERATION_ARM, 1 );
	    break;
	  }

	case ('N'): 	 // Test with real inputs from the PPG
	  test3(myvme, SIS3820_BASE);
	  break;

	case ('O'): 	 // Test with 10MHz internal clock as LNE
	  test2(myvme, SIS3820_BASE);
	  break;

	case ('Q'): 
	  test1(myvme, SIS3820_BASE);
	  break;


	case ('P'):  // Print list
	  {
	    sis3820();
	    break;
	  }
	case ('R'):  // Reset
	  {
	    sis3820_Reset(myvme, SIS3820_BASE );
	    break;
	  }
	case ('S'):  // 
	  {
	    sis3820_Status(myvme,  SIS3820_BASE);
	    break;
	  }
	case ('X'):  // 
	  {
	    return 1;
	  }

	}
    }
  return 1;
}

void test3 (MVME_INTERFACE *mvme, DWORD base)
{ 
  DWORD maximum_channel=4; 
  DWORD data;
  DWORD nbins;
  int bincount,oldbincount;
  int total,nwords;
  int i;
  int gbl_bin;
  char cmd[]="hallo";

  DWORD trueh, h,ub,ub1,ub2,*pbuf,sdata;
  int channel_field;
  DWORD reg,data_format;
  static DWORD my_data = 0;
  DWORD modid;

  DWORD *pfifo;

  const int input_offset=16; // our SIS3820 has first NIM input at Ch 17

  printf("test3: Scaler Inputs are assumed to be NIM, starting at Ch 17\n");
  printf("       All Scaler Input Channels will be disabled except channels 1 and 17-19\n");
  printf("       Channel 1 Reference pulse is enabled\n\n");
  printf("       PPG McsNext Output     should be connected to Scaler LNE Input\n");
  printf("       PPG CounterGate Output should be connected to Scaler Inhibit LNE Input\n"); 
  printf("       PPG should be loaded with a bnmr/bnqr loadfile so that it will run a cycle when started\n");
  printf("       Continue (y/n) ?");
  scanf("%s",cmd);
  cmd[0]=toupper(cmd[0]);
  if(strncmp(cmd,"N",1)==0)
    return;

  printf("\nEnter Number of bins ? ");		
  scanf("%lu",&nbins);
  printf("Number of bins=%lu\n",nbins);

  /* allocate SIS FIFO memory */
  pfifo= malloc(fsiz); /* for now fsiz=1000 */
  if (pfifo == NULL)
  {
    printf("test3: FIFO memory allocation failed\n");
    return;
  }
  else 
  {
     printf("malloc SIS3820 readout buffer: pfifo ptr %p\n",pfifo);
  }



  my_data = sis3820_RegisterRead(mvme, base , SIS3820_MODID);
  printf("check_sis3820:   ModuleID and Firmware: 0x%lx\n",my_data);
  modid = (my_data & 0xFFFF0000) >> 16;
  printf("ModuleID 0x%lx \n",modid);

  if(modid != 0x3820)
    {
      printf("test3:  Wrong VME module (Modid=0x%lx)  is installed at Base Address 0x%lx . Expecting a SIS3820 \n",modid,base);
      return;
    }

  ub2=ub1=ub=0; // initialize
  pbuf = pfifo;

  gbl_bin=0;

  printf("\nSetting up SIS3820 at base address 0x%x\n", 
	 (unsigned int) base);
  sis3820_Reset(mvme,  base);

  data = 0xFFF8FFFE;// disable all except channels 1 and 17-19
  reg  =  SIS3820_COPY_DISABLE;
  write_reg(mvme, base, reg, data);

  printf("Writing nbins=%d to preset register\n",(int)nbins);
  reg = SIS3820_ACQUISITION_PRESET;  // set number of acquistions (bins). Acquisition will stop when count register >= preset reg
  write_reg(mvme, base, reg, nbins);

  data_format = SIS3820_MCS_DATA_FORMAT_24BIT;
  

  int sis_control_input_mode =   3 << 16;
  int sis_control_output_mode =  2  << 20;
  DWORD opbits=0;
    // MCS Next of PPG must be plugged into input 1 of SIS scaler (LNE) and input signals should be plugged into
          // the enabled scaler channels

      printf("Writing 1 to LNE Prescale Register (no prescale)\n");
      sis3820_RegisterWrite(mvme,base,SIS3820_LNE_PRESCALE, 1); // 0x18  prescale LNE pulse rate reg  
      opbits = SIS3820_CLEARING_MODE |                    //   bit 0    set clearing mode
	//SIS3820_MCS_DATA_FORMAT_32BIT|            //   bits 2,3 32 bit data 
	SIS3820_MCS_DATA_FORMAT_24BIT |    //   bits 2,3 24 bit data 
        SIS3820_ARM_ENABLE_CONTROL_SIGNAL |       //  LNE as enable
	SIS3820_LNE_SOURCE_CONTROL_SIGNAL|        //   bit 4-6  external LNE signal 
	SIS3820_FIFO_MODE|                        //   bit 12,13 FIFO mode
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|     //   bit 28-31  MCS mode
	sis_control_input_mode |                  //   bit 16-18 Input mode 1
	sis_control_output_mode     ;             //   bit 20,21 Output mode 1
      
        sis3820_RegisterWrite(mvme,base,SIS3820_OPERATION_MODE, opbits); // 0x100 

	DWORD csrbits = CTRL_REFERENCE_CH1_ENABLE | CTRL_COUNTER_TEST_25MHZ_DISABLE |  CTRL_COUNTER_TEST_MODE_DISABLE ;
	sis3820_RegisterWrite(mvme,base , SIS3820_CONTROL_STATUS, csrbits );


	data = read_reg(mvme, base, SIS3820_ACQUISITION_PRESET);
	printf("Preset register: %d \n",(int)data);
	data = read_reg(mvme, base, SIS3820_ACQUISITION_COUNT);
	printf("Count register: %d \n",(int)data);



// enable counting, S LED should come on 
//  printf ("\nWriting 0x%x to reg 0x%x (S LED should come on) to enable counting\n", 
//	  SIS3820_KEY_OPERATION_ENABLE, SIS3820_KEY_OPERATION_ENABLE);

//  sis3820_RegisterWrite(mvme, base,SIS3820_KEY_OPERATION_ENABLE, SIS3820_KEY_OPERATION_ENABLE); 

// ARM rather than enable with Real Input LNE
	printf("ARMing Scaler\n");
      sis3820_RegisterWrite (mvme, base,SIS3820_KEY_OPERATION_ARM, 1 );
  

      // read status
      sis3820_Status(mvme, base);
      sis3820_CSR0(mvme, base);

      printf("\nNow start the PPG (e.g. with the program vppg)\n");
      printf("Has PPG now run a cycle (y/n)?");
      scanf("%s",cmd);
      cmd[0]=toupper(cmd[0]);
      if(strncmp(cmd,"N",1)==0)
	return;
      
      bincount=0;
      oldbincount=0;
      total=0;
      
      while (bincount!=nbins) 
	{
	  bincount = sis3820_RegisterRead(mvme,base, SIS3820_ACQUISITION_COUNT);
	  printf("acquisition count = %d\n",bincount);
	  
	  
	  if(bincount!=oldbincount)
	    {
	      oldbincount=bincount;
	      printf("\nMCS mode, bin %d completed\n",bincount);
	      
	      nwords = sis3820_RegisterRead(mvme, base , SIS3820_FIFO_WORDCOUNTER); // how many words to read
	      if (nwords < 1) 
		{
		  printf(" module is empty\n");
		  return;
		}
	      
	      printf("Module contains %d words\n", nwords);
	      read_SIS(mvme, pfifo , base , nwords);
	      
	      // Main Loop		  
	      for (i=0; i<nwords; i++)
		{
		  /* extract channel number */
		  channel_field = (*pbuf >> 24);
		  
		  /* h has a range from 0..31 */
		  trueh = channel_field & 0x1F;
		  
		  
		  //  if(h<1)
		  //   printf("test3: i=%d  gbl_bin=%d chan %d data 0x%x \n",i, gbl_bin, (int)h,(unsigned int)(*pbuf & 0xFFFFF));
		  
		  ub = channel_field >>6;
		  ub1 = ub & 1;
		  ub2 = ub & 2;
		  
		  /* make sure channel is correct */
		  if(trueh==0)
		    h=0;
		  else
		    h= trueh - input_offset +1;
		  //  printf("i=%d *pbuf=0x%8.8x trueh=%2.2d h=%2.2d  gbl_bin=%d\n",
		  //	     i, *pbuf,trueh,h,gbl_bin);
		  
		  if( h < maximum_channel)
		    {
		      
		      sdata = *pbuf & DATA_MASK; /* scaler data  */
		      
		      /* Regular debugging */
		      if(dsis)
			{
			  if(i<=100 && h == 0)
			    {
			      printf("i,pbuf,*pbuf,h,ub,sdata,gbl_bin: %d, %p %lx, %ld, %ld, %lx, %d \n",
				     i,pbuf,*pbuf,h,ub,sdata,gbl_bin); 
			      if(i == nwords -1 )
				printf("Last i,pbuf,h,ub,sdata,gbl_bin: %d, %lx, %ld, %ld, %lx,%d \n",
				       i,*pbuf,h,ub,sdata,gbl_bin); 
			    }
			}
		      printf("i=%d *pbuf=0x%8.8lx h=%2.2ld ub=%1.1ld sdata=0x%6.6lx gbl_bin=%d\n",
			     i, *pbuf,h,ub,sdata,gbl_bin);
		      /* next incoming data */
		      pbuf++; 
		      
		      /* Increment time bin every h = N_SCALER_REAL */
		      if (h == (maximum_channel-1) )
			{
			  if(gbl_bin == (nbins-1) ) // last bin 
			    {
			      if (ub != 2) 
				printf("expect ub=2 on last bin for Type 2 (ub=%ld gbl_bin=%d nbins=%ld\n",
				       ub,gbl_bin,nbins);
			    }
			  gbl_bin++;
			} /* end of h == max channel-1 */
		    } // h<maximum_channel
		  else
		    printf("Oops - incorrect channel: Idx:%d nW:%d ch:%ld  data:0x%8.8lx  and gbl_bin=%d\n",i, nwords, h, *pbuf, gbl_bin);
		  
		} // for	
	    } // end of module not empty
	  else
	    {
	      printf("sis3801_read says module is empty\n");
	      break;
	    }
	  
	}   // end of bincount != nbins

      data = read_reg(mvme, base, SIS3820_ACQUISITION_PRESET);
      printf("Preset register: %ld \n",data);
      data = read_reg(mvme, base, SIS3820_ACQUISITION_COUNT);
      printf("Count register: %ld \n",data);

      regWrite(mvme, base, SIS3820_KEY_OPERATION_DISABLE, 0x00);
      regWrite(mvme, base,SIS3820_KEY_COUNTER_CLEAR,0);
      printf ("Disabled and Cleared SIS3820 SCALER B \n");

  
    
  return;    
}



int read_SIS(MVME_INTERFACE *mvme, DWORD *pfifo , DWORD base , int toRead32)
{  // this is from bnmr frontend
  
  int max=100; // dump max words of raw data
  int tmp;
  int my_count;

  // isis = module number 0=A 1=B
  //dsis=1;
  if(dsis)
    printf("read_SIS: starting with toRead32=%d\n",toRead32);

  int used = 0; // FIFO array is empty
  int free = fsiz * 4;  // bytes (actually the fifo array size)
  int i,j,k;
  int wc=0;

 

  if(toRead32 <= 0 ) return 0; // nothing to do

 
  if (toRead32*4 > free)  // all in bytes
    {
      if(dsis)printf("read_SIS: buffer used %d, free %d, toread %d  (in bytes %d)\n", used, free, toRead32, toRead32*4);
      toRead32 = free;
    }
	

  int rd = sis3820_FifoRead(mvme,base,(char *)pfifo,toRead32);
  // rd and toRead32 should be the same
  if (rd != toRead32)
    printf("read_SIS: requested toRead32= %d words; returned rd=%d\n",toRead32,rd);
  wc+=rd;


  my_count = sis3820_RegisterRead(mvme,base, SIS3820_FIFO_WORDCOUNTER);
  if(dsis)
    printf("read_SIS: Now there are %d words left to read\n",my_count);


  if (dsis) // dump the data (if not too large!)
    {
      //  printf("sis3820 32-bit data (first 4 data words): 0x%x 0x%x 0x%x 0x%x\n",pdata32[0],pdata32[1],pdata32[2],pdata32[3]);
      i=0;
      tmp=rd;
      if (tmp > max)
	{
	  tmp = max;
	  printf("Dumping first  %d words of data\n",tmp);
          printf("Counter              Data");
	}
      j=10; // print sets of 10 across page
      if(tmp < 10)
	j=tmp; // less than 10 data words
      while (i<tmp)
	{
	  if (i % 10 ==0 )
	    printf("\n%6.6d  ", i);
	  for (k=0; k < j; k++) 
	    {
	      printf("0x%8.8lx ", pfifo[i]);
	      i++;
	    }
	}
      printf("\n");
    }
  return wc;
}

void test1 (MVME_INTERFACE *myvme, DWORD base)
{
 // Test with internal 10 MHz clock as LNE source, select MCS + SDRAM mode
  int i,j,k;
  uint32_t  scaler;
  uint32_t bits;
  uint32_t data,reg;
  int wc;
  int scancount,oldscancount;
  uint32_t data_buffer [100];
  
  int nbins =  3; // number of channel advances
  
  // Test under vmic
  sis3820_Reset(myvme,  base);
  
  data = 0xFFF0FFFF;// disable all except channels 17-21
  reg  =  SIS3820_COPY_DISABLE;
  write_reg(myvme, base, reg, data);
  
  reg = SIS3820_ACQUISITION_PRESET;  // set number of acquistions (bins)
  write_reg(myvme, base, reg, nbins);
  
  reg =  SIS3820_LNE_PRESCALE; // enable LNE prescaler
  data = 9999999;
  write_reg(myvme, base, reg, data);
  int t=10*pow(10,6); // 10MHz 
  printf ("Frequency of LNE pulses will be %d sec\n", t/(data+1) );
  
  // Select internal 10 MHz clock as LNE source, select MCS + SDRAM mode
  //  bits =  SIS3820_LNE_SOURCE_INTERNAL_10MHZ |  SIS3820_OP_MODE_MULTI_CHANNEL_SCALER |  SIS3820_SDRAM_MODE ;
  bits = SIS3820_CLEARING_MODE | SIS3820_MCS_DATA_FORMAT_32BIT | 
    SIS3820_LNE_SOURCE_INTERNAL_10MHZ |  SIS3820_OP_MODE_MULTI_CHANNEL_SCALER |
    SIS3820_SDRAM_MODE ;
  
  reg =   SIS3820_OPERATION_MODE;
  write_reg(myvme, base, reg, bits);
  
  // enable counting, S LED should come on 
  printf ("\nWriting 0x%x to reg 0x%x (S LED should come on)\n", 
	  SIS3820_KEY_OPERATION_ENABLE, SIS3820_KEY_OPERATION_ENABLE);
  sis3820_RegisterWrite(myvme, base,SIS3820_KEY_OPERATION_ENABLE, SIS3820_KEY_OPERATION_ENABLE); 
  
  // read status
  sis3820_Status(myvme, base);
  
  printf ("Waiting 1s...\n");
  sleep(1);
  
  scancount=0;
  oldscancount=0;
  while (scancount!=nbins) {
    scancount =   sis3820_RegisterRead(myvme, base, SIS3820_ACQUISITION_COUNT);
    if(scancount!=oldscancount){
      oldscancount=scancount;
      printf("\nMCS mode, scan %d completed\n",scancount);
    }
  }
  if(0)
    {
      /* readout of scaler data */
      wc = sis3820_sdramRead(myvme,  base, (void *)data_buffer, nbins);
      printf("read wc %d\n",wc);
      if(wc>0) {
	
	
	
	printf("scaler[%i] = %d\n", i, scaler);
	printf("scaler data\n");
	k=0;
	for (j=1;j<=nbins;j++) {
	  printf("scan: %3.3d ",j);
	  for (i=1;i<=4;i++) {
	    printf("ch%2.2d %8.8x  ",i,data_buffer[k]);
	    k++;
	  }
	  printf("\n");
	}
      }
    } 
  
  if(1)
    {
      // try single word reads
      int nchan = 32;
      int nread = nbins * nchan ; 
      for (i=0;i<nread;i++) {
	data_buffer[i]= sis3820_ScalerRead(myvme, base, i); // FIFO read from same place; SRAM advance ptr
	printf("data_buffer[%i] = %d\n", i, data_buffer[i]);
      }
      
      return;
    }
} // end test1

/*****************************************************************/

/*-PAA- For test purpose only */
void test2 (MVME_INTERFACE *myvme, DWORD base)
{
  int i;
  //  uint32_t dest[32]; 
  uint32_t scaler;

  // don't reset
  //  sis3820_Reset(myvme, base);

  regWrite(myvme, base, SIS3820_OPERATION_MODE, 0x100001); // non-clearing mode, outputs mode 1
  //  regWrite(myvme, base, SIS3820_COUNTER_INHIBIT, 0x00000000);  // Enabled
  regWrite(myvme, base, SIS3820_CONTROL_STATUS, 0x10000); // switch off user LED,

  regWrite(myvme, base,  SIS3820_COPY_DISABLE,  0xFFF0FFFF); // disable all except channels 17-21
  printf("Read back copy/disable reg 0x%x  as 0x%x\n", SIS3820_COPY_DISABLE, regRead (myvme, base,  SIS3820_COPY_DISABLE) );
  sis3820_Status(myvme, base);

  for (i=0;i<32;i++) {
    scaler = sis3820_ScalerRead(myvme, base, i);
    printf("scaler[%i] = %d\n", i, scaler);
  }
  regWrite(myvme, base, SIS3820_KEY_OPERATION_ENABLE, 0x00);
  regWrite(myvme, base, SIS3820_CONTROL_STATUS, 0x1); // switch on user LED
  sleep(10);

  regWrite(myvme, base, SIS3820_KEY_OPERATION_DISABLE, 0x00);

  regWrite(myvme, base, SIS3820_CONTROL_STATUS, 0x10000); // switch off user LED

  sis3820_Status(myvme, base);

  //sis3820_RegisterWrite(myvme, base, SIS3820_COUNTER_CLEAR, 0xFFFFFFFF);
  // sis3820_FifoRead(myvme, base, &dest[0], 32);
    for (i=0;i<32;i++) {
    scaler = sis3820_ScalerRead(myvme, base, i);
    printf("scaler[%i] = %d\n", i, scaler);
  }

  return;
} // end of test 2
#endif // MAIN_ENABLE

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */

//end


