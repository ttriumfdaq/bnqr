/*-- individual scaler histo increment ------------------------------------

  bnqr - no pulsepair

*/
INT scaler_increment(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
                            INT maximum_channel,INT *gbl_bin, DWORD *userbit )
/*
  - The fifo is composed of sequences of nscalers.
  Each DWORD data contains the channel and data info
  32      24                      1
  v       v                      v
  10987654321098765432109876543210
  UUFCCCCCddddDDDDddddDDDDddddDDDD
  where U: user bits (front panel inputs)
  F: latch bit (not used)
  C: channel bit (C4..C0)
  dD: data bit  (24bit)
  - for each word extract channel number (h) from the C4-C0 field
  - add that channel (h) total sum (per cycle).
  - increment current scaler[h] bin by fifo content (by cycle).
  - increment gbl_BIN every h=0 refering to a new scaler sequence.
  - type 2: if random_flag && e2a_flag or e2e_flag or e2f_flag increment correct bin (see note below)
  -         if any other mode 2, histogram bins directly

FIFO data from scaler comes out in this order:
            
ch 0 bin 0 
ch 1 bin 0
........
ch N bin 0

ch 0 bin 1
ch 1 bin 1
.........
ch N bin 1

----------
ch 0 bin M
ch 1 bin M
.........
ch N bin M


NOTE
if discard_first_bin is true, n_bins is one more than n_his_bins for most modes. First bin discarded
when histograms sent,

*/
{
  DWORD *ps, i, h, ub, ub1, ub2, *pbuf, *pus, sdata;
  WORD *qs;
  INT     channel_field;
  INT  index,status;
  INT ub_histo;
  INT hoffset;

 
  INT num_chans; // number of channels

  // SIS3801 -  All inputs are NIM, use the lowest channels for inputs

  num_chans = maximum_channel; // default
                               // when there's no offset from Ch0,  maximum_channel gives the max number of scaler channels
 
 

#ifdef HAVE_SIS3820
  INT input_offset;
  INT chan0_flag; // sis3820 Scaler B chan 0 flag true if Ch0 (Input 1) Reference pulse enabled
  INT true_ch;  // scaler channel number from the data word 
  INT first_ch; // data should have this value for the first real channel 
                 // our SIS3820 scalers inputs are half ECL half NIM
  INT last_ch; // used in ALPHA mode
  chan0_flag=input_offset=first_ch=0; // inputs start at 0 (default)
  ps=NULL;
  qs=NULL;
  
#ifdef GONE
  // this is for testing 8bit or 16bit scaler data with alpha mode
  // determined that 24bit data mode is fast enough, so this is not used
  if(gbl_alpha)
    {
      status = scaler_increment_alpha(nwords, pfifo, scaler_offset, 
				      maximum_channel, gbl_bin, userbit );
      return status;
    }
#endif // GONE

  // printf("scaler_increment_nopulsepair: starting\n");

  if(pulsepair_flag)
    {
      printf("scaler_increment_nopulsepair: ** incorrect scaler_increment routine has been called as pulsepair_flag is %d\n",
	     pulsepair_flag);
      cm_msg(MERROR,"scaler_increment_nopulsepair","incorrect scaler_increment routine has been called as pulsepair_flag is %d",
	     pulsepair_flag);
      return FAILURE;
    }
  
  //  For all cases except e2e,e2a, and e2f with random_flag, (pulsepair_flag true)  nprebins = n_bins */
  
  // SIS3820
  //  First 16 inputs are  ECL, last 16 are NIM
  //  Using NIM inputs unless more than ECL_OFFSET+1 = 16 real inputs are needed
  if(num_chans < (ECL_OFFSET+1))  // Maximum number of real channels required. 
    { // we are using only NIM inputs
      input_offset=ECL_OFFSET+1; // Ch 16 is the first NIM channel in use (ECL channel RefCh1 may also be enabled) 
      first_ch = input_offset; // default; channel value from data (h) will start at 16
    }
#ifdef TWO_SCALERS
  if(scaler_offset !=0 )
    { // Scaler B with 2 scalers (BNMR)
      if(fs.hardware.sis3820.sis_mode == 2) // REAL mode
	{
	  chan0_flag=1; // Ref Ch1 is  enabled 
	  first_ch=0; // channel  value from scaler data (h) will start at 0, followed by 16
	}
    }
#else  // one SIS3820 scaler, Scaler B  BNQR
  if(fs.hardware.sis3820.sis_mode == 2) // REAL mode
    {
      INT next_ch;
      if(gbl_alpha)
	{
	  chan0_flag = 0; // Ref Ch1 disabled
	  //     ALPHA mode  chan 17-24 in use (chan starts at 0)
	  first_ch = input_offset +  ALPHA_OFFSET -1 ; // first real channel in ALPHA mode 
                                                       // skip Ref 1 and Fl2 real input (16) so first_ch = 17
                                                       // channels < 17  DISABLED by scaler copy disable reg 
	  num_chans = num_chans - ALPHA_OFFSET;
	  last_ch = first_ch + num_chans -1; // last real channel in ALPHA mode chan 24
	  //	  if(dd[20])printf("alpha mode - first_ch=%d last_ch=%d, num_chans=%d\n",first_ch,last_ch,num_chans);
	}
      else
	{
	  if(!alpha1f_flag)
	    num_chans = num_chans - NUM_ALPHA; // alpha channels will be disabled in the scaler
	  
	  chan0_flag=1; // Ref Ch1 is  enabled in REAL mode by default
	  first_ch=0; // Ref Ch1
	  next_ch = input_offset; // channel 16 (FluorMon 2)
	  last_ch = next_ch + num_chans -2;  // last real channel
	  if(dd[20])printf("other modes - first_ch=%d next_ch=%d last_ch=%d, num_chans=%d\n",
			   first_ch,next_ch,last_ch,num_chans);
	}
    } // end REAL mode
#endif // TWO_SCALERS
#endif //  HAVE_SIS3820
  
  ub2=ub1=0; // zero these to get rid of warnings
  ub=0;
 
  pbuf = pfifo;
  /* user-bit histo number is hardcoded here */
#ifdef TWO_SCALERS
  ub_histo = N1_HISTO_MAXA -1;
#else
  if(alpha1f_flag) 
    ub_histo = N1_HISTO_MAXB - 1; /* add user bit histo if one scaler only */
  else
    ub_histo = N1_HISTO_MAXB_ORIG - 1; /* add user bit histo if one scaler only */ 
#endif
  pus = histo[ub_histo].ph ;   /*  User bit histogram is hardcoded  */
  
  if (dd[1])
    { 
      printf("scaler_increment: starting with  nwords:%d pfifo:%p, pus:%p num_chans=%d  *gbl_bin=%d\n",
	     nwords, pbuf,pus,num_chans,*gbl_bin);
    }
  if(dsis)
    {
#ifdef HAVE_SIS3820
      if(!gbl_alpha)
	{
	  printf("scaler_increment: SIS3820 inputs are offset from 0 by %d i.e. connected inputs are %d to %d \n",
		 input_offset,input_offset, (input_offset+num_chans));
	  if( chan0_flag)
	    printf("             Reference Ch1 is also enabled\n");
	}
#endif
      
      printf("scaler_increment: n_requested_bins=%d  n_bins=%d n_his_bins=%d gbl_nprebins=%d\n",
	     n_requested_bins, n_bins,n_his_bins,gbl_nprebins);
      
    } // dsis debug

  
  if(dd[17])
    {
      printf("\n nwords=%d  scaler_offset=%d",nwords,scaler_offset);
#ifdef TWO_SCALERS
      if(scaler_offset==0)
	printf("  Scaler A");
      else
	printf("  Scaler B");
#endif
      printf("\n");
    }
 
  for (i=0 ; i < nwords ; i++)
    { // main for loop on the number of words
   
      /* extract channel number */
      channel_field = (*pbuf >> 24);
      
      /* h has a range from 0..31 */
      h = channel_field & 0x1F;
#ifdef HAVE_SIS3820
      true_ch=h; // remember the channel number from the data  
#endif
      if(dsis)
	{  // debugging
	  // if(*gbl_bin < 2)
	  printf("scaler_increment: i=%d  *gbl_bin=%d scaler chan h=%d data 0x%x or %d \n",
		 i, *gbl_bin, h, (*pbuf & DATA_MASK), (*pbuf & DATA_MASK));
	}
      
      
#ifdef HACK2TO1
      /*  SIS3801 only
       *  Hack for switching scaler inputs 2 and 1 when one was broken. Now fixed.
       if( h==2 | h==1 ) printf ( "Hack: convert h from %d to %d\n", h, ( h==2 ? 1 : ( h==1 ? 2 : h )) );
      */
      
      h = ( h==2 ? 1 : ( h==1 ? 2 : h ));
#endif
    
      if(exp_mode == 1)
	{ 
	  ub = channel_field >>6;
	  if (ub>4 || ub < 0)
	    {
	      printf("\n ERROR ub is not in limits : %i\n",ub);
	      goto retminus1;
	    }
#ifdef TWO_SCALERS	  
	  if (!fill_uarray && scaler_offset == 0)
	    {  /* check that the user bit is the same for both modules */
	      
	      if(ub != *(pus + *gbl_bin))
		{
		  printf("ERROR current ub = %x; array ub = %x for i %d and h %d gbl_bin=%d\n",
			 ub,*(pus + *gbl_bin),i,h,*gbl_bin);
		  goto retminus1;
		}
	    }
#endif
	} // end of exp mode=1
      else  /* Type 2 */ 
	{
	  *userbit = channel_field >>6;
	  ub = *userbit;
	  ub1 = ub & 1;
	  ub2 = ub & 2;
	} //  end of exp mode=2

 
      /* make sure channel is correct */
      
      
#ifdef HAVE_SIS3820
      // adjust h for SIS3820
      if(!gbl_alpha)
	{ // Mode 2h
	  // deal with Ref Ch and offset input channels
	  if (input_offset > 0)
	    {
	      if(chan0_flag)  // Scaler B, ref Ch 1 enabled   true_ch=0 -> h=0
		{
		  if(h>0)  // true_ch=15 -> h=1 
		    h= h - input_offset+1; // first "real" scaler input should become h=1
		}
	      else
		{ // Ch 0 is not the reference
		  //if(dd[1]) printf("Channel h=%d from scaler will be adjusted to h=%d\n",h,(h-input_offset));
		  h=h - input_offset; //  first "real" scaler input should become h=0
		}
	    }
	}
      else
	{ //  Mode 2h with ALPHA          Scaler B 
	  //  This mode has ALPHA_OFFSET, no Ch Ref1
	  h=h-first_ch; 
	}
      if(dsis && (*gbl_bin < 2))
	printf("After channel adjustment,  *gbl_bin=%d num_chans=%d input_offset=%d  ScalerChan(true_ch)=%d and h is now %d ub=%d \n",
	       *gbl_bin,num_chans,input_offset,   true_ch,h,ub); 
      
#endif
      if( h < num_chans)
	{ // h < num_chans
	  // scaler_offset==0 for Scaler A
	  if(gbl_alpha)
	    index = h + ALPHA_OFFSET + scaler_offset; // skip first two array indices (Fl monitors, no malloc) 
	  else
	    index = h + scaler_offset;  // scaler array index; 
	  
	  /* assign pointer */
	  if(small_bin_flag)
	    qs = scaler[index].qs;  /* WORD pointer to start of histo  */
	  else
	    ps = scaler[index].ps;  /* DWORD pointer to start of histo  */
	  sdata = *pbuf & DATA_MASK; /* scaler data to be saved */
	  if(dsis)
	    {
	      if(*gbl_bin < 2)
#ifdef HAVE_SIS3820
		printf("h=%d true_ch=%d scaler_index=%d sdata=%d\n",h,true_ch, index,sdata);
#else
	      printf("h=%d  scaler_index=%d sdata=%d\n",h, index,sdata);
#endif
	    }
	  
	  
	  if(*gbl_bin >= n_bins) 
	    printf("\nscaler_increment: WARNING -  *gbl_bin >= n_bins i.e. %d > %d \n",*gbl_bin, n_bins);
	  
	  
	  // SIS3801  *gbl_bin never reached n_bins (?) and starts at 0 (0-999 if 1000 bins)
	  // SIS3820  *gbl_bin reaches n_bins at end-of-cycle, but should be zeroed 
	  //          before next cycle. When first bin is discarded, star_gbl_bin starts
	  //          at bin 1 rather than bin 0. n_bins includes extra bin.
	  //          
	  
	  /*  All modes that histogram bins directly will only come here
	      This includes Mode 20 
	      
	      
	  */
	  
	  
	  /* directly histo  for each channel */
	  
	  hoffset= *gbl_bin  ; /* histo offset within scaler histo for this bin */	
	  
	  
	  /* check user bit 2 for last bin of Type 2  
	     (Type 1 modes don't set ub2 on last bin) */
	  if(exp_mode == 2 && h==0) /* userbin same for all channels - check only Ch 0 */
	    {
	      if(*gbl_bin == fs.output.num_dwell_times -1)   // last channel
		{ 
		  if(!ub2)/* userbin same for all channels - check only 1 */
		    {
		      printf("*gbl_bin=%d, ub=%d; last bin=%d expected to be flagged with ub2\n",
			     *gbl_bin,ub, (fs.output.num_dwell_times -1));
		    }
		  else
		    printf("Found last bin IS flagged with ub2... *gbl_bin=%d, ub=%d; last bin=%d is flagged with ub2\n",
			   *gbl_bin,ub, (fs.output.num_dwell_times -1));
		}
	    }
	  
	  
	  /* Regular debugging */
	  
	   if(dd[1])
	     {
	       if(h==0)
		 {
		   
		   if(i<=100)
		     {
		       printf("hi     i=%d,pbuf=%p,*pbuf=%x,h=%d,ub=%d,sdata=%x,*gbl_bin=%d,hoffset=%d\n",
			      i,pbuf,*pbuf,h,ub,sdata,*gbl_bin,hoffset); 
		     }
		   
		   
		   if(ub > 0)
		     printf("UB1:%d 2:%d  i=%d,pbuf=%p,*pbuf=%x,h=%d,ub=%d,sdata=%x,*gbl_bin=%d,hoffset=%d\n",
			    ub1,ub2,i,pbuf,*pbuf,h,ub,sdata,*gbl_bin,hoffset); 
		   
		   if(*gbl_bin >= (n_bins -2))
		     {
		       printf("Last i=%d,pbuf=%x,h=%d,ub=%d,sdata=%x,*gbl_bin=%d,hoffset=%d\n",
			      i,*pbuf,h,ub,sdata,*gbl_bin,hoffset); 
		     }
	      
		   // Output all non-zero userbits for this channel
		   if(ub != 0)
		     {
		       printf(" User bit(s) set  (ub=%d) at *gbl_bin %d \n",ub,*gbl_bin);
		       printf("  where i=%d ub=%d *gbl_bin=%d ch=%d \n",
			      i,ub,*gbl_bin,h);
		     }
		 } // h==0
	     } // debug
	  
	  	
	
	  /* check offset within histogram  */
	  if(hoffset > n_his_bins )
	    {
	      printf("\n ERROR histo offset out of range : hoffset=%d  nhis=%d discard_first_bin=%d programming error \n",
		     hoffset,n_his_bins,discard_first_bin);
	      goto retminus1;
	    }
	  
	

	  if(discard_first_bin)
	    {
	      hoffset = hoffset -1;

	      if(dsis)
		{
		  if(h==0)
		    {
		      if(*gbl_bin == 0)
			printf("h=%d skipping bin %d  discard_first_bin=%d hoffset=%d sdata=0x%x %d\n",
			       h, *gbl_bin, discard_first_bin,hoffset,sdata,sdata);
		      else
			printf("h=%d discard first bin is true: now hoffset=%d\n",h,hoffset);
		    }
		}
	    }


	  if(hoffset >= 0)
	    { // hoffset >= 0
	       if(dd[17])
		{
		  if(h==0)
		    printf(" moving scaler data =%d (actual bin=%d) to hoffset %d\n",
			   sdata,*gbl_bin, hoffset);
		}
	      
	      /*  Now histogram the data
		  
		  Add current time bin content to cumulative scaler histo */
	      if(small_bin_flag)
		*(qs+hoffset) += sdata;
	      else
		*(ps+hoffset) += sdata;
	      /* increment local cycle sum scaler */
	      scaler[index].sum_cycle += (double)sdata;
	      
	      
#ifdef TWO_SCALERS
	      if(exp_mode == 1)
		{
#ifdef THIRTYTWO
		  /* increment cycle sum according to User Bit */
		  if(index < 16)
		    scaler[BACK_USB0 + ub].sum_cycle += sdata;
		  else if (index < 32)
		    scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#else  // TWO scaler inputs
		  /* increment cycle sum according to User Bit */
		  if(index ==0)
		    scaler[BACK_USB0 + ub].sum_cycle += sdata;
		  else if (index ==1)
		    scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#endif // THIRTYTWO
		}
#endif	  //  TWO_SCALERS
	    
	    } // hoffset > 0
	  /* next incoming data */
	  pbuf++;  
	  
	  /* Increment time bin every h = n_scaler_real */
	  //printf("checking time bin increment... h=%d num_chans-1=%d \n",h, num_chans-1);
	  if (h == (num_chans-1) ) // last channel
	    {
	      if(exp_mode == 1)
		{
		  if(fill_uarray && scaler_offset == 0) /* true for Scaler A (2 scalers) or Scaler B (1 scaler)*/
		    {
		      *(pus + *gbl_bin) = ub;
		      if(dd[1]) 
			{
			  if(*gbl_bin < 2 ) 
			    printf ("update pus %p with ub %d\n",(pus + *gbl_bin),ub);
			}
		    }
		}
	      (*gbl_bin)++;   // actual (true) bin number
	      if(dd[1])
		{
#ifdef HAVE_SIS3820
		  printf("h=%d, true_ch=%d, first_ch=%d chan0_flag=%d  num_chans=%d incremented *gbl_bin to %d\n",
			 h,true_ch,first_ch, chan0_flag, num_chans, *gbl_bin);
		  
#else
		  printf("h=%d, num_chans=%d incremented *gbl_bin to %d\n",h, num_chans, *gbl_bin);
#endif
		}
	    } /* end of last channel (h == max channel-1) */
	} /* end of h< num_chans */
      else
	{
      printf("Oops - incorrect channel: Idx:%d nW:%d ch:%d  data:0x%8.8x  and *gbl_bin=%d\n",i, nwords, h, *pbuf, *gbl_bin);
      goto retminus1;
	}
      
      
      
      
    } // loop on the number of words
  status = 0;
  
  if(dsis)
    printf("\nscaler_increment returns with *gbl_bin %d\n",*gbl_bin);
  dsis=dd[1]=0; // TEMP
  return 0; /* success */
  
 retminus1:
  stop_run();
  dsis=dd[1]=0; // TEMP
  return -1; 
}
