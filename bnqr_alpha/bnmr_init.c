/* bnmr_init

VMIC version based on CVS Revision 1.16

*/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#include <math.h>
#include <ctype.h>

#include "midas.h"
#include "mvmestd.h"
#include "evid.h"
#ifndef EXPERIM_INC
#define EXPERIM_INC
#include "experim.h"
#endif

extern BOOL gbl_waiting_for_run_stop;
extern BOOL  cycles_request_flag;
// Software Flags
#undef HAVE_ROOT
#undef USE_ROOT

// Hardware Flags used in this code (defined in Makefile)
// HAVE_VMIC
// HAVE_VXWORKS
// HAVE_SIS3820
// HAVE_SIS3801
// HAVE_SIS3801E Firmware version E
// HAVE_PPG  // Old PPG
// HAVE_NEWPPG  // New PPG. Define HAVE_PPG and HAVE_NEWPPG for this. 
//                    NOTE NewPPG (PPG32) Can only be used for testing (beam on , helicity hardware missing)
// HAVE_VMEIO
// HAVE_NIMIO32
// HAVE_PSM
// HAVE_PSMII
// HAVE_PSMIII
// EPICS_ACCESS
// CAMP_ACCESS
// GONE  // code is no longer used


//extern "C" {

#ifdef HAVE_VMIC
// VMIC
extern MVME_INTERFACE *gVme;
#endif


#ifdef HAVE_SIS3801
#include "sis3801.h"
#endif

#ifdef HAVE_SIS3820
#include "sis3820drv.h"
#include "sis3820.h"
#endif

#ifdef HAVE_VMEIO
#include "vmeio.h"
#endif

#ifdef HAVE_NIMIO32
#include "VMENIMIO32.h"
#endif

#ifdef HAVE_PPG 
 // PPG 
#include <sys/stat.h> // time 
#include "unistd.h" // for sleep 
#ifdef HAVE_NEWPPG 
#include "newppg.h"
//#include "ppg_modify_file.h" // prototypes
#else
#include "vppg.h"
#endif //  HAVE_NEWPPG 
#endif //  HAVE_PPG

#ifdef HAVE_PSM
#include "../psm/trPSM.h"
#include "setup_psm.h"
#endif //  HAVE_PSM
//} // extern "C"

#ifdef EPICS_ACCESS
#include "bnmr_epics.h"
#endif

/* prototypes */
#include "febnmr.h"
#ifdef HAVE_SIS3801
#include "sis3801_code.h" // for  SIS3801
#else
#ifdef HAVE_SIS3820
#include "sis3820_code.h" // for SIS3820
#endif
#endif
void show_scaler_pointers(void);
INT bnmr_init (char *p_name_fifo, char *p_name_info, char *p_name_scalers);
/* end */
INT check_defined_sizes(void);
INT reset_hardware_modules(void);

INT bnmr_init(char  *p_name_fifo, char *p_name_info, char *p_name_scalers )
{ 
  FIFO_ACQ_FRONTEND_STR(fifo_acq_frontend_str);
  CYCLE_SCALERS_SETTINGS_STR(cycle_scalers_settings_str);
  INFO_ODB_EVENT_STR(info_odb_event_str);
  
  INT    status, rstate, size;
  char   str_set[256];
  INT  i,len;
  DWORD data;
  
  gbl_IN_CYCLE = FALSE;
  gbl_BIN_A = 0;
  gbl_BIN_B = 0;
  gbl_HEL = HEL_DOWN;
  skip_cycle = FALSE;
  
  printf("\n");

  status = reset_hardware_modules(); // VME Reset all hardware modules; sleeps 0.25s
  cm_yield(250); // yield up to 250ms (prevents frontend_loop from running - OK here)

  /* get the experiment name */
  size = sizeof(expt_name);
  status = db_get_value(hDB, 0, "/experiment/Name", &expt_name, &size, TID_STRING, FALSE);
  
  printf("Front End code for experiment %s now running ... \n",expt_name);
  
  if(check_defined_sizes() != SUCCESS)
    {
      // cm_msg(MERROR,"bnmr_init","Scaler/Histogram defined sizes are incorrect");
      printf("bnmr_init: Scaler/Histogram defined sizes are incorrect\n");
	return FE_ERR_HW;
    }
    


  if(debug)
    printf("bnmr_init starting with fifo name:%s and info name %s...\n",p_name_fifo, p_name_info);
  
  /* get basic handle for experiment ODB */
  status=cm_get_experiment_database(&hDB, NULL);
  if(status != CM_SUCCESS)
    {
      cm_msg(MERROR,"bnmr_init","Not connected to experiment");
      return CM_UNDEF_EXP;
    }
  
 
#ifdef DEFERRED
  printf("bnmr_init: registering to a deferred transition \n");
  status= cm_register_deferred_transition(TR_STOP,  wait_end_cycle);
  if (status == CM_SUCCESS)
    printf("bnmr_init: successfully registered to deferred transition at TR_STOP\n");
  else
    {
      cm_msg(MERROR,"bnmr_init","failure registering to deferred transition (%d)",status);
      return status;
    }
#else
    printf("bnmr_init: NOT registering to a deferred transition \n");
#endif   // DEFERRED


  
 
 
  /* check for current run state; if not stopped, stop it */
  size = sizeof(rstate);
  status = db_get_value(hDB, 0, "/runinfo/State", &rstate, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"bnmr_init","cannot get /runinfo/State");
      return FE_ERR_ODB;
    }
    
  if (rstate != STATE_STOPPED)
    {    
      status = stop_run();
      if (status != SUCCESS)
	{
	  cm_msg(MINFO,"bnmr_init",
		 "!!! Please stop the run immediately ");
	  cm_msg(MINFO,"bnmr_init",
		 "!!! Then restart this frontend (VMIC) program");
	  return (FE_ERR_HW);
	}
    } // end of run not stopped
      

      /* create record /Equipment/INFO ODB/Variables to make sure it exists  */
  sprintf(str_set,"/Equipment/%s/Variables",p_name_info);
  
  /* find the key for info odb */
  status = db_find_key(hDB, 0, str_set, &hInfo);
  if (status != DB_SUCCESS)  
    {
      printf( "bnmr_init: Key %s not found; creating record for info odb\n",str_set);
      status = db_create_record(hDB, 0, str_set, strcomb(info_odb_event_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"bnmr_init","db_create_record fails %s status(%d)",str_set,status);
	  return status;
	}
      /* now get the key  */
      status = db_find_key(hDB, 0, str_set, &hInfo);
      if (status != DB_SUCCESS)  
	{
	  cm_msg(MERROR, "bnmr_init", "Key %s not found",str_set);
	  return status;
	}
      else
	printf("Key now found for %s\n",str_set);
    }
  
  /* got the key - check the record size */
  printf("Got the key for info odb\n");
  status = db_get_record_size(hDB, hInfo, 0, &size);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "bnmr_init", "error during get_record_size (%d)",status);
      return status;
    }

  printf("Size of info saved structure: %d, size of info record: %d\n", sizeof(INFO_ODB_EVENT) ,size);
  if (sizeof(INFO_ODB_EVENT) != size)  
    {
      printf("Creating record info odb  size (%d and %d)\n", 
	     sizeof(INFO_ODB_EVENT) ,size );
      status = db_create_record(hDB, 0, str_set, strcomb(info_odb_event_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"bnmr_init","db_create_record fails %s status(%d)",str_set,status);
	  return FE_ERR_ODB;
	}

      cm_msg(MERROR,"bnmr_init","INFO ODB successfully recreated due to structure size mismatch");
      status = db_get_record_size(hDB, hInfo, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "bnmr_init", "error during get_record_size (%d)",status);
	  return status;
	}
      printf("Size of info saved structure: %d, size of info record: %d\n", sizeof(INFO_ODB_EVENT) ,size);
      if (sizeof(INFO_ODB_EVENT) != size)  
	{
	  cm_msg(MERROR, "bnmr_init", "error; record sizes do not match even after creating record");
	  return DB_TYPE_MISMATCH;
	}

    }
 
  /* Get current  settings */

  /*  printf("Now size of cyinfo is %d\n",size); */
  printf("Size of info saved structure: %d, size of info record: %d\n", sizeof(INFO_ODB_EVENT) ,size);
  size = sizeof(INFO_ODB_EVENT); 
  status = db_get_record(hDB, hInfo, &cyinfo, &size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "bnmr_init", "cannot retrieve %s record (size of cyinfo=%d) (%d)", str_set,size,status);
    return DB_NO_ACCESS;
  }

 
  /* create record "/Equipment/FIFO_acq/frontend" to make sure it exists  */
  sprintf(str_set,"/Equipment/%s/frontend",p_name_fifo); 
  
  /* find the key for frontend */
  status = db_find_key(hDB, 0, str_set, &hFS);
  if (status != DB_SUCCESS)
    {
      printf( "bnmr_init: Cannot find key %s ",str_set);
      /* So try to create record */
      
      status = db_create_record(hDB, 0, str_set, strcomb(fifo_acq_frontend_str)); 
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"bnmr_init","db_create_record fails %s (status %d)",str_set,status);
	  return status;
	}
      /* Find the key  */
      status = db_find_key(hDB, 0, str_set, &hFS);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "bnmr_init", "Cannot find key %s ",str_set);
	  return DB_NO_KEY;
	}

    }
  else /* got a key */    
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hFS, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "bnmr_init", "error during get_record_size (%d)",status);
	  return status;
	}
      printf("Size of sis saved structure: %d, size of sis record: %d\n", sizeof(FIFO_ACQ_FRONTEND) ,size);
      if (sizeof(FIFO_ACQ_FRONTEND) != size) 
	{
	  /* create record */
	  cm_msg(MINFO,"bnmr_init","creating record (frontend); mismatch between size of structure (%d) & record size (%d)", sizeof(FIFO_ACQ_FRONTEND) ,size); 
	  status = db_create_record(hDB, 0, str_set , strcomb(fifo_acq_frontend_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"bnmr_init","Could not create frontend record (%d)",status);
	      return status;
	    }
	  else
	    printf("Success from create record for %s\n",str_set);
	}
    }

  // Get the (cold) debug key
  debug = fs.debug.debug; // global
  
#ifdef CAMP_ACCESS
  status = camp_create_rec(); /* creates record if record sizes don't match */
  printf("Returned from camp_create_rec with status=%d\n",status);
  if(status != SUCCESS)
    return (status);
#endif // CAMP_ACCESS

  /* Get current  settings */
  size = sizeof(fs);
  status = db_get_record(hDB, hFS, &fs, &size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "bnmr_init", "cannot retrieve %s record (size of fs=%d)", str_set,size);
    return DB_NO_ACCESS;
  }
  /* Setup names in cycle_scaler/setting equipment */
  sprintf(str_set,"/Equipment/%s/Settings",p_name_scalers);
  
  /* create record /Equipment/cycle_scalers to make sure it exists  */
   status = db_create_record(hDB, 0, str_set, strcomb(cycle_scalers_settings_str));
   if (status != DB_SUCCESS)
     printf("bnmr_init: db_create_record fails %s with status(%d) \n",str_set,status);

#ifdef HAVE_SIS3801  
  /* allocate SIS FIFO memory */
  pfifo_B = malloc(SIS_FIFO_SIZE); /* SIS hardware parameter 64kbytes */
#ifdef TWO_SCALERS
  pfifo_A = malloc(SIS_FIFO_SIZE); /* SIS hardware parameter 64kbytes */
  if (pfifo_A == NULL || pfifo_B == NULL)
#else
  if (pfifo_B == NULL)
#endif //  TWO_SCALERS
  {
    cm_msg(MERROR,"bnmr_init","FIFO memory allocation failed");
    return SS_NO_MEMORY;
  }
  else 
  {
     printf("malloc SIS3801 readout buffers: pfifo_A ptr %p, pfifo_B %p\n",pfifo_A,pfifo_B);
  }
#endif // HAVE_SIS3801

#ifdef HAVE_SIS3820
  //  pfifo_A=pfifo_B=NULL;
  /* allocate SIS FIFO memory */
  pfifo_B = malloc(SIS_FIFO_SIZE); /* SIS hardware parameter 64kbytes */
#ifdef TWO_SCALERS
  pfifo_A = malloc(SIS_FIFO_SIZE); /* SIS hardware parameter 64kbytes */
  if (pfifo_A == NULL || pfifo_B == NULL)
#else
  if (pfifo_B == NULL)
#endif //  TWO_SCALERS
  {
    cm_msg(MERROR,"bnmr_init","FIFO memory allocation failed");
    return SS_NO_MEMORY;
  }
  else 
  {
     printf("malloc SIS3820 readout buffers: pfifo_A ptr %p, pfifo_B %p\n",pfifo_A,pfifo_B);
  }
#endif // SIS3820


#ifdef EPICS_ACCESS
 /* Get EPICS odb keys for fe_epics */
  status = db_find_key(hDB, 0, "/equipment/epics/variables/demand", &hEPD);
  if (status != DB_SUCCESS) hEPD = 0;
  status = db_find_key(hDB, 0, "/equipment/epics/variables/measured", &hEPM);
  if (status != DB_SUCCESS) hEPM = 0;
#endif   //  EPICS_ACCESS

#ifdef HAVE_VMEIO
  /* Init VMEIO */
  /* by default, all outputs are in latched mode; output 6 is latched */
  printf("bnmr_init: set all selected VMEIO outputs into pulsed mode (bitpat=0x%x)\n", VMEIO_PULSE_BITS);
  vmeio_pulse_set(VMEIO_BASE, VMEIO_PULSE_BITS);    
  
  /* Reset external hardware to cycle off */ 
  /*  vmeio_pulse_write(VMEIO_BASE, EOC_PULSE); // end of cycle 
      vmeio_latch_write(VMEIO_BASE, 0); 
  */
#endif //  HAVE_VMEIO
  

#ifdef HAVE_NIMIO32
  data = nimio_CommandReg(gVme, NIMIO32_BASE, 1);  // Reset
  data = nimio_FirmwareRead(gVme, NIMIO32_BASE); // Read Firmware
  if(data == 0xFFFFFF)
    {
      cm_msg(MERROR,"bnmr_init","VME NIMIO32 Register is not responding. Please check hardware");
      return FE_ERR_HW;
    }
  printf("Firmware for VME NIMIO32 at base address 0x%x is 0x%x\n",  NIMIO32_BASE, data);
  nimio_OutputControlSet(gVme,  NIMIO32_BASE, 0); // Output 3 is 40MHz clock
  nimio_OutputSet(gVme,  NIMIO32_BASE, 0); // Clear output lines
#endif

#ifdef HAVE_SIS3801 
  /* Init SIS3801 - this will check that the module is there
     function init_sis3801() is in sis3801_code.c
      - calls sis3801_Setup to select the particular setting with the mode parameter 
      - sets up real mode by default 
  */
#ifdef TWO_SCALERS    // reset, sleep, set into test or real mode
  printf("Initializing SIS3801 Module A base 0x%x\n", SIS3801_BASE_A);
  init_sis3801(  SIS3801_BASE_A,  fs.hardware.enable_sis_ref_ch1_scaler_a);
#endif
  printf("Initializing SIS3801 Module B base 0x%x\n", SIS3801_BASE_B);
  init_sis3801(  SIS3801_BASE_B,  fs.hardware.enable_sis_ref_ch1_scaler_b);
  
  if (debug) printf("# of active ch.  : %d\n",N_SCALER_REAL);
#endif  
  

#ifdef HAVE_SIS3820
  status = init_sis3820(FALSE);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"bnmr_init","Problem initializing MCS SIS3820. Cannot run");
      printf("\n Exit from routine bnmr_init. Cannot run - problem initializing MCS SIS3820 \n");
      return FE_ERR_HW;
    }
#endif  //  HAVE_SIS3820

  /* Check the PPG is in the crate */
#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
  status = TPPGStatusRead(gVme, PPG_BASE);
#else
  status =  VPPGBeamCtlRegRead(gVme, PPG_BASE);
  if (status == 0xFF)  /* reads 0xFF if no module */
    {
      cm_msg(MERROR,"bnmr_init","There is no PPG (old) in the crate (or PPG is not responding). Cannot run");
      printf("\n Exit from routine bnmr_init. Cannot run - no PPG \n");
      return FE_ERR_HW;
    }
#endif //  HAVE_NEWPPG
#endif //  HAVE_PPG

  /*Disable the PPG module */
#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
  TPPGInit(gVme,PPG_BASE); // also stops the program , clears polarity, zero all outputs etc
  TPPGDisableExtTrig(  gVme, PPG_BASE);

#else // old PPG
  printf("Initializing PPG... setting VME to control Helicity\n");
  VPPGStopSequencer(gVme, PPG_BASE);
  VPPGDisableExtTrig ( gVme, PPG_BASE ); /* default.. external trigger input disabled */
  VPPGBeamCtlPPG(gVme,PPG_BASE); /* PPG controls the beam */
  VPPGPolmskWrite(gVme,PPG_BASE,0);
  VPPGPolzCtlVME(gVme,PPG_BASE); /* VME controls Helicity (DRV POL) */
#endif //  HAVE_NEWPPG
#else
  printf("Warning: HAVE_PPG is NOT DEFINED in this code\n");
#endif // HAVE_PPG

  if(debug)
    show_scaler_pointers();


  /* Get the experiment name from odb  */
  size = sizeof(beamline); 
  sprintf(str_set,"/Experiment/Name"); 
  status = db_get_value(hDB, 0, str_set, beamline, &size, TID_STRING, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"bnmr_init","cannot get beamline from \"%s\" (%d)",str_set,status);
      return status;
    }
  len=strlen(beamline);
  strncpy(BEAMLINE,beamline,len);
  /*BEAMLINE[4]=beamline[4]='\0'; // terminate */
  for  (i=0; i< len ; i++)
    { 
      BEAMLINE[i] = toupper (BEAMLINE[i]); 
      beamline[i] = tolower (beamline[i]);
    }

  setup_debug_hotlinks(); // open debug hotlinks 

#ifdef HAVE_SIS3801
  DWORD id;  
  char str[32];

#ifdef TWO_SCALERS
  status = check_sis3801(gVme, SIS3801_BASE_A, &id);
  if (status != SUCCESS)
    return status;

  sprintf(str,"output/sis3801/modid_a");
  status = db_set_value(hDB, hFS, str, &id, sizeof(id), 1, TID_DWORD);  
  if(status !=SUCCESS)
    cm_msg(MERROR,"frontend_init","cannot set ODB key  ...%s to 0x%x (%d)",str,id,status); 
 
#endif 
//  TWO_SCALERS

  status = check_sis3801(gVme, SIS3801_BASE_B, &id);
  if(status != SUCCESS)
    return status;

  sprintf(str,"output/sis3801/modid_a");
  status = db_set_value(hDB, hFS, str, &id, sizeof(id), 1, TID_DWORD);  
  if(status !=SUCCESS)
    cm_msg(MERROR,"frontend_init","cannot set ODB key  ...%s to 0x%x (%d)",str,id,status); 

#endif //  HAVE_SIS3801

#ifdef HAVE_PSM
  status =  init_freq_module(); /* initialize PSM */
  if (status != SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","Failure initializing VME PSM Module");
      return status;
    }

  /* and then turn the RF off for PSM */
  printf("frontend_init: disabling PSM at base address 0x%x\n",PSM_BASE);
  disable_psm(gVme,PSM_BASE); // disables gates for PSMIII (no full sleep mode)

#ifndef HAVE_PSMIII
  psmWriteGateControl(gVme, PSM_BASE,"all",0); /* disable external gates; old ppg sends spurious gates when loaded */
#endif
  printf("frontend_init: clearing PSM RF Trip \n");
  psmClearRFpowerTrip(gVme, PSM_BASE);
  cyinfo.rf_state = 0; /* set to zero or alarm will go off */

  /* Make sure rf alarm doesn't keep going off */
 
  size = sizeof(cyinfo.rf_state);
  status = db_set_value(hDB, hInfo, "RF state",
                        &cyinfo.rf_state, size, 1, TID_DWORD);
  if (status != DB_SUCCESS)
    cm_msg(MERROR, "frontend_init", "cannot clear \"...info_odb/variables/rf state\" (%d)",
           status);

  /* psmGetStatus  (gVme, PSM_BASE, stdout); */
#endif

  printf("Success from bnmr_init for beamline %s\n",beamline);
  return FE_SUCCESS;
}

/*-----------------------------------------------------------------------------------------------------*/

void show_scaler_pointers()

/* ----------------------------------------------------------------------------------------------------*/
{
  /* show the pointers for the scalers (debug usually) */

  printf("\nREAL scalers indexes  for experiment %s:\n",expt_name);
  printf("Number of real inputs for scaler(s) : %d\n",N_SCALER_REAL);
#ifdef TWO_SCALERS
  printf ("Max real scaler channel for Module A : %d\n",MAX_CHAN_SIS38xxA);
#else
  printf("\nModule A is not defined\n");
#endif
   
  printf ("Max real scaler channel for Module B : %d\n",MAX_CHAN_SIS38xxB);
  printf("Fluorescence monitors (2) start at Scaler[%d]\n",FLUOR_CHAN1);
  printf("Polarimeter counters (2)start at  Scaler[%d]\n",POL_CHAN1);
  printf("Neutral beam counters for back (%d) start at  Scaler[%d]\n",
	 NUM_NEUTRAL_BEAM, NEUTRAL_BEAM_B);
  printf("Neutral beam counters for front (%d) start at  Scaler[%d]\n",
	 NUM_NEUTRAL_BEAM, NEUTRAL_BEAM_F);

  printf("\nCYCLE scalers:  Number defined=%d;   indexes :\n",N1_SCALER_CYC);
#ifdef TWO_SCALERS
  printf("Back %d;  Front %d;  Asym %d;  Ratio %d\n",NA_BACK_CYC,NA_FRONT_CYC,NA_ASYM_CYC,NA_RATIO_CYC);
#endif
   printf("Pol sum %d;   Pol asym %d;   Neutral Beam sum %d;   Neutral Beam asym %d\n",
  	 NB_POL_CYC,NB_POL_ASYM,NB_NB_CYC,NB_NB_ASYM);

  
}

/*-----------------------------------------------------------------------------------------------------*/
INT stop_run(void)
/*-----------------------------------------------------------------------------------------------------*/
{
  char str[128];
  INT status,size;
  INT rstate;
  

  gbl_waiting_for_run_stop =  TRUE;

  /* check for current run state; if not stopped, stop it */
  size = sizeof(rstate);
  status = db_get_value(hDB, 0, "/runinfo/State", &rstate, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"stop_run","cannot get /runinfo/State (%d)",status);
      printf("run_state: Cannot determine present run state.\n");
    }
  else
    {
       
      if (rstate == STATE_STOPPED)
	{  
	  printf("stop_run: Run is already stopped\n");
	  return SUCCESS;
	}
    }

  skip_cycle=TRUE;
  printf("run_state: attempting to stop the run immediately...\n");

  status = cm_transition(TR_STOP | TR_DEFERRED, 0, str, sizeof(str), TR_SYNC, 0);
  if(status == CM_DEFERRED_TRANSITION)
    cm_msg(MINFO,"stop_run","Deferred transition is set");
  else if((status !=  CM_SUCCESS) && (status != CM_DEFERRED_TRANSITION))
    cm_msg(MERROR, "stop_run", "cannot stop run immediately: %s (%d)", str, status);
  else
    cm_msg(MINFO,"stop_run","run should now be stopped");

  status = set_client_flag("frontend",0); /* indicate the frontend stopped the run */
  if(cycles_request_flag)
    cm_msg(MINFO,"stop_run","frontend program stopped the run because requested number of cycles has been reached");
  else
  cm_msg(MERROR,"stop_run","frontend program febnqr_vmic stopped the run due to error");


  status = db_get_value(hDB, 0, "/runinfo/State", &rstate, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"stop_run","cannot get /runinfo/State (%d)",status);
      printf("run_state: Cannot determine present run state.\n");
      return status;
    }
  
 
      
  if(rstate != STATE_STOPPED)
    printf("stop_run: run state is not STOPPED\n");
  else
     printf("stop_run: run is now  STOPPED\n");

  return status;

}

/*-----------------------------------------------------------------------------------------------------*/
INT hardware_check(void)
/*-----------------------------------------------------------------------------------------------------*/
{
  INT i,j,k;

  // Check we have reasonable hardware enabled
#ifndef HAVE_VMIC
  cm_msg(MERROR,"hardware_check","VMIC must be defined for this version");
  return  FE_ERR_HW;
#endif

#ifdef HAVE_VXWORKS
  cm_msg(MERROR,"hardware_check","This version cannot be run under VxWorks");
  return  FE_ERR_HW;
#endif

/* make sure one of PSM,PSMII or PSMIII is defined */
  INT psm[3];
  for(i=0;i<3;i++)
    psm[i]=0;
  j=0;
#ifdef HAVE_PSM
  psm[0]=1;
#ifdef HAVE_PSMII
  psm[1]=1;
  psm[0]=0; // clear PSM
#endif
#ifdef HAVE_PSMIII
  psm[2]=1;
  psm[0]=0; // clear PSM
#endif
#endif

  j=0;

  if(psm[0]==1)
    {
      printf("hardware_check: PSM is defined\n");
      j++;
    }
  if(psm[1]==1)
    {
      printf("hardware_check: PSMII is defined\n");
      j++;
    }
  if(psm[2]==1)
    {
      printf("hardware_check: PSMIII is defined\n");
      j++;
    }
  if(j > 1) 
    {
      printf("hardware_check: Only one type of PSM module type can be defined \n");
      cm_msg(MERROR,"hardware_check", "Only one type of PSM module type can be defined \n");
      return FE_ERR_HW;
    }

#ifndef HAVE_PPG
  cm_msg(MINFO,"hardware_check","PPG is not defined in this code. Can only use SIS Test Mode");
  printf("hardware_check: PPG not defined.  Can only use SIS Test Mode with PPG Modes 10 or 20\n"); 
#endif

#ifdef HAVE_NEWPPG
  printf("hardware_check: NEWPPG is defined in this code. Use only for testing. Cannot set Helicity or Beam On/Off \n");
#endif

#ifndef EPICS_ACCESS
  printf("hardware_check: EPICS_ACCESS is not defined in this code; no EPICS scan available (e.g.  NaCell scan)\n");
#endif

#ifndef CAMP_ACCESS
  printf("hardware_check: CAMP_ACCESS is not defined in this code; no CAMP scan available \n");
#endif

#ifndef HAVE_PSM
   printf("hardware_check: HAVE_PSM is not defined in this code; no RF available\n");
#endif

#ifndef HAVE_NIMIO32
   printf("hardware_check: HAVE_NIMIO32 is not defined in this code. Use only for testing. \n");
   printf("                Helicity readback and Mode 1f with constant time between cycles are not available\n");
#endif

  return SUCCESS;
}


/*---------------------------------------------------------------*/
INT setup_debug_hotlinks(void)
/*---------------------------------------------------------------*/
{
  INT i,status;
  /* Called from bnmr_init  to set up the hotlinks for debug
     If a hotlink cannot be opened, a message is sent
  */
  char str[128];
  hdebug = hdbg = hdppg = hdcamp =  hdpsm = hdmps = hdsis =0; // global handles

  // Clear all debug levels
  for (i=0; i< NUM_DEBUG_LEVELS; i++)
    dd[i]=0;
  dcamp=dpsm=dsis=dppg=manual_ppg_start=0;


  // Get key to debug area in odb
  
  sprintf(str,"debug");
  status = db_find_key(hDB, hFS, str, &hdebug);
  if (status != DB_SUCCESS)
    {
      cm_msg(MINFO,"setup_debug_hotlinks","find key on \"%s\" failed (%d)",str,status);
      return status;
    }


  // find key for debug levels
  status = db_find_key(hDB, hdebug, "hot_debug_level (-1=clr)", &hdbg);
  if (status == DB_SUCCESS)
    {
      status = db_open_record(hDB, hdbg, &fs.debug.hot_debug_level___1_clr_
			      , sizeof(fs.debug.hot_debug_level___1_clr_)
			      , MODE_READ, call_back_debug_level, "hot debug level");
      if (status != DB_SUCCESS)
	cm_msg(MINFO,"setup_debug_hotlinks","open record on \"%s/hot_debug_level (-1=clr)\"  failed (%d)",str,status);
    }
  else
    cm_msg(MINFO,"setup_debug_hotlinks","find key on \"%s/hot_debug_level (-1=clr)\" failed (%d)",str,status);

   if(debug)
    printf("setup_debug_hotlinks: success for hotlink on   \"%s/hot_debug_level (-1=clr)\" \n",str);

  
  // find key for dppg
  status = db_find_key(hDB, hdebug, "dppg", &hdppg);
  if (status == DB_SUCCESS)
    {
      status = db_open_record(hDB, hdppg, &fs.debug.dppg
			      , sizeof(fs.debug.dppg)
			      , MODE_READ, call_back_debug, "hot dppg");
      if (status != DB_SUCCESS)
	cm_msg(MINFO,"setup_debug_hotlinks","open record on \"%s/dppg\"  failed (%d)",str,status);
    }
  else
    cm_msg(MINFO,"setup_debug_hotlinks","find key on \"%s/dppg\" failed (%d)",str,status);

  if(debug)
    printf("setup_debug_hotlinks: success for hotlink on   \"%s/dppg\" \n",str);


  // find key for dcamp
  status = db_find_key(hDB, hdebug, "dcamp", &hdcamp);
  if (status == DB_SUCCESS)
    {
      status = db_open_record(hDB, hdcamp, &fs.debug.dcamp
			      , sizeof(fs.debug.dcamp)
			      , MODE_READ, call_back_debug, "hot dcamp");
      if (status != DB_SUCCESS)
	cm_msg(MINFO,"setup_debug_hotlinks","open record on \"%s/dcamp\"  failed (%d)",str,status);
    }
  else
    cm_msg(MINFO,"setup_debug_hotlinks","find key on \"%s/dcamp\" failed (%d)",str,status);


  // find key for dpsm
  status = db_find_key(hDB, hdebug, "dpsm", &hdpsm);
  if (status == DB_SUCCESS)
    {
      status = db_open_record(hDB, hdpsm, &fs.debug.dpsm
			      , sizeof(fs.debug.dpsm)
			      , MODE_READ, call_back_debug, "hot dpsm");
      if (status != DB_SUCCESS)
	cm_msg(MINFO,"setup_debug_hotlinks","open record on \"%s/dpsm\"  failed (%d)",str,status);
    }
  else
    cm_msg(MINFO,"setup_debug_hotlinks","find key on \"%s/dpsm\" failed (%d)",str,status);

  if(debug)
    printf("setup_debug_hotlinks: success for hotlink on   \"%s/dpsm\" \n",str);


  // find key for dsis
  status = db_find_key(hDB, hdebug, "dsis", &hdsis);
  if (status == DB_SUCCESS)
    {
      status = db_open_record(hDB, hdsis, &fs.debug.dsis
			      , sizeof(fs.debug.dsis)
			      , MODE_READ, call_back_debug, "hot dsis");
      if (status != DB_SUCCESS)
	cm_msg(MINFO,"setup_debug_hotlinks","open record on \"%s/dsis\"  failed (%d)",str,status);
    }
  else
    cm_msg(MINFO,"setup_debug_hotlinks","find key on \"%s/dsis\" failed (%d)",str,status);

  if(debug)
    printf("setup_debug_hotlinks: success for hotlink on   \"%s/dsis\" \n",str);



  // find key for dmps
  status = db_find_key(hDB, hdebug, "manual_ppg_start", &hdmps);
  if (status == DB_SUCCESS)
    {
      status = db_open_record(hDB, hdmps, &fs.debug.manual_ppg_start
			      , sizeof(fs.debug.manual_ppg_start)
			      , MODE_READ, call_back_debug, "hot manual_ppg_start");
      if (status != DB_SUCCESS)
	cm_msg(MINFO,"setup_debug_hotlinks","open record on \"%s/manual_ppg_start\"  failed (%d)",str,status);
    }
  else
    cm_msg(MINFO,"setup_debug_hotlinks","find key on \"%s/manual_ppg_start\" failed (%d)",str,status);

  if(debug)
    printf("setup_debug_hotlinks: success for hotlink on   \"%s/manual_ppg_start\" \n",str);


  return SUCCESS;
}


INT check_defined_sizes(void)
{
  INT i;
  printf("Histos Scaler   MAX (Type 1)   MAX (Type 2)   Maximum\n");
  printf("         A         %2.2d              %2.2d            %2.2d\n",N1_HISTO_MAXA, N2_HISTO_MAXA,  N_HISTO_MAXA);
  printf("         B         %2.2d              %2.2d            %2.2d\n",N1_HISTO_MAXB, N2_HISTO_MAXB,  N_HISTO_MAXB);
  
  i =  N1_HISTO_MAXA;
  if ( N2_HISTO_MAXA > i) i= N2_HISTO_MAXA;
  if(N_HISTO_MAXA < i)
    {
      printf(" check_defined_sizes: N_HISTO_MAXA must be the largest of  N1_HISTO_MAXA  and N2_HISTO_MAXB\n");
      return FAILURE;
    }
  
  
  i =  N1_HISTO_MAXB;
  if ( N2_HISTO_MAXB > i) i= N2_HISTO_MAXB;
  if(N_HISTO_MAXB < i)
    {
      printf(" check_defined_sizes: N_HISTO_MAXB must be the largest of  N1_HISTO_MAXB and N2_HISTO_MAXB\n");
      return FAILURE;
    }
  
  // NA_CYCLE_SCALER   for type 1 and type 2
  // NA1_CYCLE_SCALER  for type 1 only
  // scalers
  printf(" Cycle Scalers       Total (Type 1) Total (Type 2)     Maximum\n");
  printf("         A                %2.2d               %2.2d            %2.2d\n",(NA1_CYCLE_SCALER+NA_CYCLE_SCALER), NA_CYCLE_SCALER ,(NA1_CYCLE_SCALER+NA_CYCLE_SCALER));
  printf("         B                %2.2d               %2.2d            %2.2d\n",NB_CYCLE_SCALER, NB_CYCLE_SCALER , NB_CYCLE_SCALER);
  printf("Total Type 1 (both scalers) = %d\n",  (NA1_CYCLE_SCALER + NA_CYCLE_SCALER + NB_CYCLE_SCALER)) ;
  printf("Total Type 2 (both scalers) = %d\n", (NA_CYCLE_SCALER   + NB_CYCLE_SCALER ));
	 
  printf("\n Cumulative Scalers: (Scaler A only)  Type 1 :%d  Type 2: %d  Total %d\n",NA_SCALER_CUM, NB_SCALER_CUM, N2_SCALER_CUM);

  printf("\n Number of REAL Scalers: %d\n",N_SCALER_REAL);
  printf("Type 1 Scaler Total: %d\n",N1_SCALER_TOTAL);
  printf("Type 2 Scaler Total: %d\n",N2_SCALER_TOTAL);
  printf("Scaler Max: %d\n",N_SCALER_MAX);
  i=N1_SCALER_TOTAL;
  if(N2_SCALER_TOTAL > i) i=N2_SCALER_TOTAL;
  if(N_SCALER_MAX < i)
    {
      printf(" check_defined_sizes: N_SCALER_MAX must be the largest of  N1_SCALER_TOTAL and N2_SCALER_TOTAL\n");
      return FAILURE;
    }
  return SUCCESS;
}


INT reset_hardware_modules(void)
{
  printf("reset_hardware_modules: starting\n");

  // Initialize hardware 
#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
  TPPGReset(gVme,PPG_BASE); // also stops the program even if in long delay
#else
  VPPGStopSequencer(gVme,PPG_BASE); // does a reset
#endif // HAVE_NEWPPG
#endif // HAVE_PPG
  
#ifdef HAVE_SIS3801
#ifdef TWO_SCALERS 
  sis3801_module_reset(gVme, SIS3801_BASE_A);
#endif
  sis3801_module_reset(gVme, SIS3801_BASE_B);
#endif // HAVE_SIS3801
  
#ifdef HAVE_SIS3820
#ifdef TWO_SCALERS 
  sis3820_Reset(gVme,  SIS3820_BASE_A);
#endif
  sis3820_Reset(gVme,  SIS3820_BASE_B);
#endif //  HAVE_SIS3820
  
  
#ifdef HAVE_PSM
#ifdef HAVE_PSMII
  printf("frontend_init: The PSM module defined is PSMII\n");
#endif
#ifdef HAVE_PSMIII
  printf("frontend_init: The PSM module defined is PSMIII\n");
#endif
  printf("Resetting PSM at base address 0x%x\n",PSM_BASE);
  psmVMEReset(gVme, PSM_BASE);  // same routine for all types
#endif
  
#ifdef HAVE_NIMIO32
  nimio_Reset(gVme,  NIMIO32_BASE);
#endif

  ss_sleep(250); // sleep 0.25s
  printf("reset_hardware_modules: ending\n");
  return 1;
}


