/********************************************************************\
  Name:         febnqr_vmic.c

        VMIC Version

        Version supports all BNMR experiment types
        --------------------------------------------------------
  For BNMR
  Acquire MAX_CHAN_SIS38xxA channels of a SIS3820 multi channel scaler 
  plus    MAX_CHAN_SIS3801B channels of a 2nd multi channel scaler.

  For BNQR
  Acquire MAX_CHAN_SIS3801B channels of a multi channel scaler.

  Generate individual channel SUM by cycle and ODB them in /variables
  Keep individual channel cumulative scaler histogram
  Generate periodically event containing n_histo_a + n_histo_b histograms
   2 histograms are composed of the sum of 16 scaler histos (0-15, 16-31)
   3rd histogram is the user bits   
   .. to n histogram for each individual channels of multiscaler B
  
  cycle_start() :
  The cycle is started by cycle_start() at BOR or during loop while running.
    reset the gbl_BIN time bin.
    clear the FIFO
    set flag gbl_IN_CYCLE
   
  FIFO_acq() :
  The polling function will call FIFO_acq which does the following:
   Check gbl_IN_CYCLE  to find out if the cycle is still in progress.
    yes : read 1/2 FIFO or FIFO FULL flags; read FIFO if either is set
    no  : flush FIFO, make cycle histograms and update run histograms

  frontend_loop():
  The looping function is called periodically independently of the ACQ.
  The main function of the loop is to switch helicity when cycle is
  complete AND while the run is in progress:
   Check if running:
    yes : check if still gbl_IN_CYCLE
          yes : nothing to do
          no  : wait for all Equipments to run
                start new cycle
    no  : nothing to do
  
  gbl_BIN : current time bin of the current cycle.
            cleared at BOR and every end of cycle (when full cycle is taken)

  Epics Scans:
  The frontend (ppc) can scan Epics devices (e.g. NaCell, Laser, Field) through direct access
  to Epics.

  Camp Scans:
  The frontend (ppc) can scan CAMP devices through direct access to CAMP.

  ** NOTE: fe_epics   ***
      This pgm also sends data to EPICS (i.e. some scaler totals, sums, differences etc). This
      is completely separate from the direct access frontend Epics device scan, and is handled by
      fe_epics running on the (linux) host. 
  
  Created by:   Pierre-Andre Amaudruz, Renee Poutissou
  Modified by Suzannah Daviel


Based on cvs Revision 1.89  2010/06/02 23:09:25  suz
  $Log$
\********************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>  // for sleep
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <sys/stat.h>  //time
#include <assert.h>
#include <math.h>
#undef HAVE_ROOT
#undef USE_ROOT


#ifdef CAMP_ACCESS /* camp includes come before midas.h */
#include "camp_clnt.h"
/* to avoid conflict with midas defines, undefine these
   - they will be redefined by midas the same as camp defined them */
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#define failure_status CAMP_FAILURE
#define IO_RETRIES 15
#endif /* end of CAMP_ACCESS */

#define FAILURE 0



/* NOTE   const1f mode = 1f mode run with constant time between PPG cycles.

          Can define BINCHECK (see below) MUST define SPEEDUP, MUST have NIMIO32 board INPUT5 connected to PPG DAQ Computer busy (see below)
  
   PPG runs in a loop, being restarted by the frontend code only
      1. At begin-of-run
      2. When helicity is flipped at end of scan
      3. If the computer busy time supplied by the PPG is exceeded, i.e. the readout has taken too long

   Note that in this mode, when PPG has to be restarted for item 3 or the threshold has failed, the "frequency step-back procedure" is used. 
   
     A.  The frequency is stepped back to the previous value,
     B.  One and only one "good" cycle is taken (i.e. "skip ncycle after out-of-tol" parameter is ignored)
         If cycle is "bad", cycle is repeated at this frequency until a "good" cycle is found.
     C.  Data from "good" cycle above is DISCARDED
     D.  Scan resumes at the next frequency.


   The  "frequency step-back procedure" is NOT used when the very first scan fails (just retakes the data at the same freq).
   If the frequency fails just after helicity flip, the helicity is NOT reflipped.

   SUPERCYCLES are not supported in this mode, i.e. number of cycles/supercycle must be 1 (algorithm not fixed for supercycles).

   For this mode, BINCHECK was added to check for the "empty bin problem" where an empty bin was found in the middle of the data, which happens
   when the DAQ cannot keep up with the PPG computer busy time. If the counting gate is removed from the Scaler (for testing) the empty bin becomes a
   bin with abnormally high number of counts.

   Also for this mode only, SPEEDUP must be defined. It causes the cycle to be restarted BEFORE the Histograms are sent out.

   There MUST be a NIMIO32 board fitted such that INPUT5 is connected to PPG DAQ Service Time output. This is read to check DAQ Service Time is set when
   enough LNE are received, and still set just before it re-enables the hardware for the next cycle.
                                                  OUTPUT5 is set when read_histo routine is running (shipping out the histos)
                                                  OUTPUT6 is set when gbl_in_cycle is true
                                                  OUTPUT7 is set when computer is busy reading/processing histos
						  OUTPUT4 when gbl_in_cycle is true, set and cleared by check_lne 

   Can define BINCHECK to check that no bad cycles are getting through, and MUST define SPEEDUP which (only for this mode) restarts the cycle 
   BEFORE the histograms are sent.   BINCHECK checks for zero or too many counts on HM00.  Therefore the Channel 1 Ref Pulser MUST be enabled on
   Scaler B if BINCHECK is enabled.

   The problem BINCHECK was added to check should be fixed by SPEEDUP mode
*/
#define BINCHECK    // BINCHECK checks for zero or too many counts on HM00. Used in const1f mode only, problem should be fixed by SPEEDUP mode
                    // and new const1f code (see above)
#define SPEEDUP     // Must be defined if using const1f mode (see above).


// NOTE  Now using 2 inputs only for Scaler A  (Jan 2014)
//#define THIRTYTWO  Define if  32 inputs are needed for Scaler A (Back and Front counters unsegmented)
//
//                   otherwise the Back  Counters are summed in hardware and connected to Scaler A Input 0
//                                 Front Counters are summed in hardware and connected to Scaler A Input 1


//        Sarah's microwave sample/ref mode (currently only available on BNQR, Type 20)
//        NIMIO32   OUTPUT2  turns on sample (microwave generator)
//                  INPUT6   reads back status of sample mode 

// #define THR_TEST    // with THR_TEST defined, uses random number and hot_threshold_trip to cause beam to
                       // go in and out of tolereance.
                       //Without THR_TEST defined and real signals can use hot_threshold trip in debug area 
                       //to go out in and of tol  
#include "midas.h"
#include "mvmestd.h"
//#include "evid.h"
#ifndef EXPERIM_INC
#define EXPERIM_INC
#include "experim.h"
#endif
#include "setup_scan_params.h" // function prototypes
//#include "write_client_code.h" // function prototypes

// Hardware defined in Makefile
#ifdef HAVE_SIS3820
int defined_sis3820(void);
#endif
#ifdef HAVE_PSM
int defined_psm(void);
#endif
#ifdef HAVE_PSMII
int defined_psmII(void);
#endif
#ifdef HAVE_PPG
int defined_ppg(void);
#endif
#ifdef BNQR
int defined_bnqr(void);
#endif
//extern "C" {
#ifdef HAVE_VMEIO
#include "vmeio.h"
#endif

#define DEFERRED

#ifdef HAVE_NIMIO32
#include "VMENIMIO32.h"
#endif

#ifdef HAVE_SIS3820
#include "sis3820drv.h"
#include "sis3820.h"
#include "sis3820_code.h" // prototypes
#define HAVE_SIS3820_OR_SIS3801E 1
#endif

#ifdef HAVE_SIS3801
#include "sis3801.h"
#include "sis3801_code.h" // prototypes
#endif

#ifdef HAVE_SIS3801E
#define HAVE_SIS3820_OR_SIS3801E 1
#endif

#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG 
#include "newppg.h" 
//#include "ppg_modify_file.h" // prototypes
#else  // old PPG
#include "vppg.h"
#endif
#endif // HAVE_PPG

#ifdef HAVE_PSM
#include "../psm/trPSM.h"
#include "setup_psm.h"    /* psm prototypes */
#endif

#ifdef EPICS_ACCESS    /* direct EPICS access by the frontend (ppc) defined in Makefile */
#include "bnmr_epics.h"
#include "connect.h"      /* for scanning Epics device */
#else
BOOL hel_warning;
#endif

#ifdef CAMP_ACCESS   /* direct CAMP access by the frontend (ppc) for scanning CAMP device(s) */
/* CAMP parameters */
#include "camp.h"
#include "camp_acq.h" /* parameters and prototypes for camp_acq 
		         note: must come after midas.h  */


/* structure camp_params is defined in camp_acq.h */
static CAMP_PARAMS camp_params;
BOOL dc = 0 ; /* internal debug for camp routines */
BOOL gotCamp = FALSE; 
INT read_sweep_dev(float *read_camp_val, CAMP_PARAMS camp_params); // need CAMP_PARAMS to be defined
#endif /* end of CAMP */

#include "febnmr.h"
#include "trandom.h" /* BNMR/BNQR only */ 
#include "client_error_codes.h"
//} // extern "C"

#ifdef HAVE_VMIC
// VMIC
MVME_INTERFACE *gVme = 0;
#endif

BOOL SET=TRUE;
BOOL CLEAR=FALSE;

BOOL last_piece_sent=1;

/*-- Frontend globals ------------------------------------------------*/

/* odb handles */
// extern HNDLE hDB, hFS, hEPD, hEPM, hBsf ; /* these defined in febnmr.h, assigned in bnmr_init.c */
HNDLE   hHs=0, hRR=0, hCT1=0, hCT2=0, hFL=0, hFT=0, hHM=0, hDC=0, hFF=0, hPd=0, hSK=0, hLP=0, hTh=0;
HNDLE   hCamp=0, hKey=0;
 
/* 32-bit storage for scalers and histos
*/
/* Individual scaler channel */
typedef struct {
    double  sum_cycle;   /* cycle histogram sum */
    DWORD   nbins;
    DWORD   *ps;  /* DWORD cumulative histogram */
    WORD    *qs;  /* WORD cumulative histogram (either WORD or DWORD is malloc-ed */
} MSCALER;

/* Histogram struct for Event builder */
typedef struct {
    double  sum;                    /* not used yet */
    double  bin_max;                /* not used yet */
    DWORD   nbins;
    DWORD   *ph;         /* cumulative histogram over run  */
    WORD    *qh;          
} MHISTO;

MHISTO   histo[N_HISTO_MAX];
MSCALER  scaler[N_SCALER_MAX];    /* max  (real + any calculated) */ 


INT cb_time[700];
INT cb_time_size=700;

//static char bars[] = "|/-\\";
//static int  i_bar;

/* Debug */
// "debug" is NOT hotlinked

BOOL histo_process_flag=0;

//INT  dd  -> dd[0]  ddd ->d1 -> dd[1]   dddd -> d2 -> dd[2]
//INT    d0=0, d1=0, d2=0, d4=0, d6=0; // d4 unassigned? now dc in this code
// dran  and dr -> d13 
                                     // d6 wasn't declared in this code.
//INT d3=0, d5=0, d7=0, d8=0, d9=0, d10 = 0,  d11=0;
//INT d12=0, d13=0, d14=0, d15=0;
INT     dhw=0 ; // not changed yet
INT     dj=0; // not changed ye
// INT dl=0; // not changed. Don't use vmeio
INT     dbyt=0 ; // not changed. Not used much
INT  ddi=0; // not changed yet
INT dnbins=2; // used in dd[20]  num bins to dump 
DWORD my_bin_count=-5;

float remember_sum_cycle=0;

/*   
  d12 was  dh  helicity 
  manual_ppg_start was  dq  normally 0. Set to 1 for manual ppg start 
  d0 was dd  debugs are in sis_setup and poll_event
  d1 was ddd frontend_loop, begin_run, debug scalers, histo banks
  dd[2] d2 was dddd indicates if running in cycle in frontend loop, vmeio
    d3  reference thresh

   dd[5] d5 Epics scan (higher level ... bnmr_epics.c)
    d6  Epics scan (lower level... EpicsStep.c)
   dd[7] d7  Epics watchdog (to keep Epics channel open )
    d8 display: for debugging, don't overwrite  scan values in cycle_start 
    d9 client flag and automatic stop and delayed transition
    d10  freq scan  
   d4 was dc  Camp scan (is in camp_acq.h)
    d11 silent mode; do not display anything
    d12 helicity 
  dpsm debug psm; also set pdd=1 for trPSM. If using pdd=1 may need to set midas watchdog 
    dbyt allows user to load bytecode.try instead of bytecode.dat ; recent time check is disabled 
  d15 debug sis3820
  
  d13  dran and dr debug randomizing freq values
    dhw  set to 1 to prevent helicity readback checking stop the run - may want to continue if testing.
    

  d17 debug 2e, 2a, 2f
  d18 1f const time between cycles mode and speedup histogramming
  d19 sample/ref mode
  d20 alpha mode (2h)   

  NOTE:  NUM_DEBUG_LEVELS in debug.h is currently set to 25
*/

INT     gbl_run_number;      
BOOL    gbl_IN_CYCLE;              /* software in cycle flag */
INT     gbl_FREQ_n;                /* current cycle # in freq/Camp/Dac sweep */
INT     gbl_inc_cntr;              /* seems to be used only for Epics scan */
DWORD   gbl_CYCLE_N;               /* current valid cycle */
INT  gbl_ppg_restart_cntr=0;
BOOL gbl_wait_helicity;
// Store all these parameters as send out data after next cycle is started in const_1f SPEEDUP mode
DWORD   gbl_CYCLE_H=0;    /* used in SPEEDUP only; cycle whose histograms are to be sent out */
DWORD   gbl_freq_H=0;     /*  used in SPEEDUP only; freq value for above cycle */
DWORD   gbl_SCYCLE_H=0;
DWORD   gbl_SCAN_H;
DWORD     gbl_helset_H; // store gbl_ppg_hel
DWORD     gbl_helread_H; // store gbl_hel_readback

#ifdef SPEEDUP
INT  gbl_prev_histo_cntr=0;   //   histo_process counts cases where previous histograms have not been sent out
INT  gbl_prev_skip_cntr=0;    //   cycle_start counts cases where last cycle skipped but waiteqp[HISTO] is set
INT  gbl_HistoRead_skipped_cycle=0; // histo_read: counts number of times skip_cycle is set. Routine shouldn't be called if skip_cycle is true
#endif
DWORD   gbl_SCYCLE_N;              /* current super cycle */
DWORD   gbl_SCAN_N;                /* current scan */
DWORD   gbl_watchdog_timeout;      /* midas watchdog timeout */
BOOL    gbl_watchdog_flag=TRUE;         /* midas watchdog flag */
DWORD   gbl_long_watchdog_time=180000;   /* 3 minutes for timeout when doing epics/camp */
DWORD   gbl_medium_watchdog_time = 120000; /* 2 minutes */ 
BOOL    gbl_dachshund_flag=FALSE;  /* true when a long watchdog timeout is set */
BOOL    gbl_hel_flipped;           /* true when helicity has flipped */
BOOL    gbl_heltest_timer=FALSE; // temporary for print_hel_time
BOOL    gbl_flip_mode;             /* used in dual channel mode only; true if flipping each cycle */
BOOL    gbl_hel_mismatch_flag, gbl_hel_recover;
INT     gbl_dcm_hcur;               /* used in dual channel mode only; current hel state for possible debugging */
BOOL    gbl_sr;    // sample/ref mode 
BOOL    gbl_alpha; // alpha mode (2h)
BOOL    alpha_chunks=0;
INT     alpha_scaler_offset=0; // offset to 1st real scaler (=ALPHA_OFFSET in Mode 2h, otherwise 0);
INT     histo_trigger_mask; // used in alpha mode (2h)
BOOL    gbl_bin_size;
BOOL    small_bin_flag; // used in 2h mode
DWORD   userbit_A,userbit_B;           /* userbits for last bin */
BOOL    waiteqp[4] = {TRUE,TRUE,TRUE,TRUE};
BOOL    hot_rereference=FALSE, lhot_rereference=TRUE;
//BOOL    skip_cycle=FALSE;
BOOL    fill_uarray = FALSE;  /* uarray contains users_bits per dwell times */
BOOL    gbl_transition_requested= FALSE;
BOOL    gbl_waiting_for_run_stop = FALSE; /* global flag to prevent restarting cycle on error */
DWORD   n_bins;          /* same as n_requested_bins  unless discard_first_bin is true  */
DWORD   n_requested_bins; /* number of bins requested by user.
			     n_requested_bins = # of dwell times = n_his_bins except 2a pulse pairs compaction modes 
			                               and e2e mode */
BOOL    gbl_die=FALSE;
BOOL    gbl_tolerance_flag=FALSE; // flag indicates last cycle was out-of-tolerance. Used ensure threshold alarms are cleared.  
DWORD   n_his_bins;       /* number of hist bins for e2a pulse_pairs compaction modes (1st,2nd,diff) 
                                              and e2e mode */
INT     exp_mode;        /* =1 for all TYPE1 ; = 2 for others */
INT     n_histo_total;   /* number of histos for this run */
INT     n_histo_a;       /* number of histos in scalerA for this run */
INT     n_histo_b;       /* number of histos in scalerB for this run */
INT     n_scaler_total;  /* number of scalers being used for this run (may be less than N_SCALER_MAX) */
INT     n_scaler_cum;  /* number of scalers for this run */
INT     n_scaler_real; /* number of real scalers enabled for this run */
/* assign these as DWORD (unsigned long int) for large freq. values */
DWORD     freq_val;        /* frequency value to load */
DWORD     freq_start;      /* start value for frequency sweep */
DWORD     freq_stop;       /* stop value for frequency sweep */

INT     freq_inc;        /* increment value for frequency at each sweep */
INT      freq_ninc;       /* number of frequency increments to do */
float   set_camp_val;  /* camp value to set */
float     camp_start;      /* start value for camp sweep */
float     camp_stop;       /* stop value for camp sweep */
float     camp_inc;        /* increment value for camp at each sweep */
INT     camp_ninc;       /* number of camp increments to do */
float   dac_val;  /* dac value to set */
float   dac_start;      /* start value for dac sweep */
float   dac_stop;       /* stop value for dac sweep */
float   dac_inc;        /* increment value for dac at each sweep */
INT     dac_ninc;       /* number of dac increments to do */
INT     gbl_scan_flag=0; /* scan type encoded at begin_of_run for histo_read */
INT     rftime=60;       /* check_file_time uses this */
INT  rf_count=0;
BOOL OLD_SISA_FLAG=FALSE;

INT gbl_count; // temp

INT     epicstime=0;
char ppg_mode[3];
INT gbl_fix_inc_cntr;
/* Scan Type Flags  */
BOOL rf_flag = FALSE;   /* flag indicates RF scan (1f, 1a,1b); not used for Type 2, all type 2 use RF */    
BOOL camp_flag = FALSE;   /* flag to indicate scan of CAMP device by frontend */ 
BOOL  epics_flag = FALSE; /* flag to indicate direct EPICS scan by frontend i.e. NaCell/Laser/Field */ 
BOOL mode10_flag = FALSE; /* flag to indicate ppg mode 10 (SCALER mode) is selected */
BOOL mode1g_flag = FALSE; /* flag to indicate ppg mode 1g is selected */
                         /* mode 1g is a combination of SLR (20) and 1f */
BOOL mode1j_flag = FALSE; /* flag to indicate ppg mode 1g is selected */
                         /* mode 1j is a combination of SLR (20) and 1c */
BOOL mode1f_flag = FALSE; // mode 1f selected
BOOL alpha1f_flag = FALSE; // mode 1f + ALPHA histos selected
BOOL const1f_flag = FALSE; // mode 1f/1h const_time_between_cycles is selected

/* type 1 only Single Channel Mode */
INT gbl_yield_ms = 100; /* 100ms default */

BOOL random_flag = FALSE; /* randomize the RF values (supported for types 1a,1b 2a 2e 2f) */
BOOL e2a_flag,e2a_pulse_pairs,e2a_compress; /* flags for running "2a" */ 
BOOL e2e_flag,e2f_flag; /* flags for running mode 2e,2f */
BOOL pulsepair_flag=FALSE;
BOOL CAMP_repeat_flag = FALSE;
BOOL first_time = FALSE;  /* used by routine cycle_start to know if this is begin_run */
BOOL first_cycle = FALSE;  /* used by readout routines to know if this is begin_run */
BOOL client_check = TRUE; /* if true, mdarc will stop the run on error (assuming mdarc is running!) */
char who_stops_run[7]; /* mdarc or user */
BOOL gbl_first_call = TRUE; /* used for deferred stop routine */
BOOL discard_first_bin = FALSE; // global (used by sis3820)
/* Epics scan variable
   Most are in header file */
INT     Epics_last_time, Hel_last_time, RFtrip_last_time;
BOOL    gbl_epics_live, gbl_hel_live, gbl_laser_live; /* flags to indicate Epics channel(s) should be kept alive */
INT     N_Scans_wanted;
INT     gbl_epics_scan=0;
BOOL    hold_flag = FALSE; /* BNMR hold flag */
BOOL    flip=FALSE; /* helicity flipping flag */
INT     gbl_ppg_hel; /* PPG helicity state set value */
#ifdef HAVE_NIMIO32
INT     gbl_hel_readback; /* helicity state according to NIMIO32 readback */
#endif
DWORD   lastBCDfreq; /* needed for checking fsc is correct */
DWORD   freq0A;
INT     loop_error_cntr=0;
BOOL    gbl_stopping_run_flag; // Trying to stop run
DWORD gbl_npostbins,gbl_ndepthbins,gbl_ntuple_width_h,gbl_ntuple_width_s;     
DWORD gbl_nprebins,gbl_nmidsection_h,gbl_nRFbins ;
float max_cycle_time; // approx cycle time for PPG
INT max_cycle_wait;
//BOOL wait_ppg_stop=0;
BOOL  cycles_request_flag=0;
INT gbl_lne_poll_time;  // used in frontend_loop
//static struct timeval th1, th2; gone
// Timing test
static struct timeval t1, t2;
double  gbl_maxElapsedTime, gbl_minElapsedTime;
double  gbl_sumElapsedTime;
INT     gbl_sumNum;
BOOL hel_flip_flag;
char latched[10];

// fifo_acq
double gbl_pmax, gbl_pmin, gbl_rmax, gbl_rmin;

#ifdef BINCHECK
INT bin1_XXMHz_count; // Ref Pulse is 25MHz 3801;  50MHz 3820
INT bad_cntr,good_cntr;
#endif

DWORD const1f_prev_freq_val;
INT   const1f_prev_FREQ_n;
BOOL const1f_stepped_back;
BOOL const1f_junk_data=0;

char Rname[32]; /* name of an EPICS channel to be read (helicity);*/
INT Rchid;  /* channel ID of above channel */
INT Rchid_lp; /* channel ID for laser power */
char Rlaser[32]; /* both filled by LaserPower_init */
float Laser_Power_volts=0;
INT Laser_last_time=0;
INT gbl_lne=0; // counter to limit number of msgs
BOOL e1f_started_flag=FALSE; // used in constant time between cycles mode

#ifdef HAVE_PPG  // globals and ppg_code
BOOL ppg_running;
char ppg_loadfile[80];
INT gbl_cb_cntr1, gbl_cb_cntr2;
INT gbl_ppg_cb_cntr1,gbl_ppg_cb_cntr2,gbl_ppg_cb_cntr3;  // counters for num cycles where PPG computer busy unexpectedly set/not set
#endif // HAVE_PPG

#ifdef HAVE_SIS3820
// static prototypes
static int have_SIS_data(int SIS3820_base, int isis);
//static int read_SIS(char*pevent , DWORD base , int toRead32, int isis);
static int read_SIS(DWORD *pfifo , DWORD base , int toRead32, int isis);
static double timeDiff(const struct timeval t1, const struct timeval t2);
static struct timeval gSisLastRead[2];
//static DWORD wc = 0; // uint32_t
HNDLE hfe=0;
#endif

#ifdef HAVE_SIS3820_OR_SIS3801E
static unsigned int racq_count = 0;
BOOL flush=FALSE;
//INT hot_debug=-1;
BOOL lhot_debug=FALSE;
#endif
INT hot_debug=-1;
INT hot_threshold_trip=0;

#ifdef THR_TEST
#define NUM_BEAM_OFF 3;
INT beam_off_cntr=NUM_BEAM_OFF;
#endif
#ifdef HAVE_NIMIO32
      INT write_cb_stats(void);
#endif

INT gbl_read_sample,gbl_set_sample; // mode 20 S/R mode enabled
INT send_histo_event=0; // histo_read is now periodic. This flag causes histo_read to return except when data is ready.
INT sis3820_test_flag=0; // global  set for sis3820 sis_mode=0 test mode without ppg
char *sis_module[]={"A","B"};

/* The frontend name (client name) as seen by other MIDAS clients   */
/* Either BNMR or BNQR must be defined in Makefile */
#ifdef BNMR
char *frontend_name = "feBNMR_VMIC";
#endif

#ifdef BNQR
char *frontend_name = "feBNQR_VMIC";
#endif

/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms    */
INT display_period = 0;

/* maximum event size produced by this frontend 
   must be less than DEFAULT_MAX_EVENT_SIZE (midas.h) 4 MiBytes */

//INT max_event_size = 500*1024;
INT max_event_size = 4096 * 1024; //  4meg 0x400000 

/* buffer size to hold events */
INT event_buffer_size =  4 * 1024 * 4096;  
/* maximum event size (for fragmented events only) not used */
INT max_event_size_frag = 5*1024*1024;

#define POLL_FIFO_ACQ 5         /* in ms (was 50)*/
#define POLL_EQUIPMENTS 50       /* in ms */
#define POLL_HISTO 10           /* in ms */
#define POLL_LNE 50             /* in ms */
#define POLL_LNE_FAST 10 // 10ms poll often for 1f constant time between cycles
//#define POLL_MONITOR   500      /* polled every 0.5 s */
/*-- Equipment list ------------------------------------------------*/
/* Note: Equipment FIFO_ACQ is polled every POLL_FIFO_ACQ ms.
   
         Equipments Histo, Cycle_Scalers, Hdiagnosis and Info ODB should
	 be polled with the same time period, i.e. every POLL_EQUIPMENTS ms.
*/

EQUIPMENT equipment[] ={
  { "FIFO_acq",           /* equipment name */
    {
      1,                 /* whoever sends the data must have event ID = 2 for mdarc
			    if FIFO_acq sends the data, its event ID must be 2 */
      1,                  /*   trigger mask */
      "SYSTEM",             /* Send Data (previously didn't send data and was set to "") */
      EQ_PERIODIC,           /* equipment type */
      0,
      "MIDAS",              /* format */
      TRUE,                 /* enabled */
      RO_RUNNING,           /* read only when running */
      POLL_FIFO_ACQ,        /* poll interval */
      0,                    /* stop run after this event limit */
      0,                    /* number of sub-event in the super event */
      0,                    /* don't log history */
      "", "", "",}
    ,
    FIFO_acq,             /* readout routine */
    NULL,NULL,
    NULL,
  },


  { "Histo",              /* equipment name */
    { 2, 1 ,                 /* event ID, trigger mask    was 1,1
                               for alpha mode, histo sends the event, not FIFO_acq, event ID must be 2 for mdarc to process event */
     "SYSTEM",             /* send data banks in SYSTEM event buffer */
     EQ_PERIODIC,          /* equipment type */
     0,                    /* event source */
     "MIDAS",              /* format */
     TRUE,                 /* enabled */
     RO_RUNNING | RO_EOR,  /* read when running and on end_of_run */
     POLL_HISTO,            /* polling period */
     0,                    /* stop run after this event limit */
     0,                    /* number of sub-event in the super event */
     0,                    /* log history */
     "", "", "",}
    ,
    histo_read,           /* readout routine */
    NULL,NULL,NULL,
  },


  
  { "Cycle_Scalers",      /* equipment name */
    {  3, (1<<3),                 /* event ID, trigger mask */
       "SYSTEM",             /* event buffer */
       EQ_PERIODIC,          /* equipment type */
       0,                    /* event source */
       "MIDAS",              /* format */
       TRUE,                 /* enabled */
       RO_RUNNING | RO_ODB,  /* read when running and on transitions */
       POLL_EQUIPMENTS,      /* polling period */
       0,                    /* stop run after this event limit */
       0,                    /* number of sub-event in the super event */
       0,                    /* log history */
       "", "", "",}
    ,
    scalers_cycle_read,   /* readout routine */
    NULL,NULL,NULL,
  },
  
  { "Hdiagnosis",         /* equipment name */
    {  4, (1<<4),                 /* event ID, trigger mask */
       "SYSTEM",             /* event buffer */
       EQ_PERIODIC,          /* equipment type */
       0,                    /* event source */
       "MIDAS",              /* format */
       FALSE,                /* disabled */
       RO_RUNNING,           /* read when running  */
       POLL_EQUIPMENTS,      /* polling period */
       0,                    /* stop run after this event limit */
       0,                    /* number of sub-event in the super event */
       0,                    /* log history */
       "", "", "",}
    ,
    diag_read,            /* readout routine */
    NULL,NULL,NULL,
  },
    
  { "Info ODB",           /* equipment name */
    {  10, (1<<10),          /* event ID, trigger mask */
       "",                   /* no banks sent */
       EQ_PERIODIC,          /* equipment type */
       0,                    /* event source */
       "FIXED",              /* format */
       TRUE,                 /* enabled */
       RO_ODB|RO_RUNNING|RO_TRANSITIONS,               /* send to odb */
       500,                  /* polling period */
       0,                    /* stop run after this event limit */
       0,                    /* number of sub-event in the super event */
       0,                    /* log history */
       "", "", "",}
    ,
    info_odb,             /* readout routine */
    NULL,NULL,NULL,
  },
  
  /* note - setting RO_ODB means event is copied to
     /Equipment/equipment_name/variables
     i.e./Equipment/Info ODB/variables 
  */


#ifdef EPICS_ACCESS
  { "EpicsAlive",            /* equipment name */
    { 12, (1<<12),                /* event ID, trigger mask */
      "",                   /* no banks sent */
      EQ_PERIODIC,          /* equipment type */
      0,                    /* event source */
      "MIDAS",              /* format */
      TRUE,                 /* enabled */
      RO_RUNNING,           /* not RO_ALWAYS  - to keep Epics alive when running */
      30000,                 /* polling period (ms) */
      0,                    /* stop run after this event limit */
      0,                    /* number of sub-event in the super event */
      0,                    /* log history */
      "", "", "",}
    ,
    epics_alive,             /* readout routine */
    NULL,NULL,NULL,
  },

#ifdef GONE
  // This was used to send the helicity value to a history plot
  // for debugging
 { "Helicity",            /* equipment name */
    { 11, (1<<12),                /* event ID, trigger mask */
      "",                   /* no banks sent */
      EQ_PERIODIC,          /* equipment type */
      0,                    /* event source */
      "MIDAS",              /* format */
      TRUE,                 /* enabled */
      RO_RUNNING,           /* running */
      500,                 /* polling period (ms) */
      0,                    /* stop run after this event limit */
      0,                    /* number of sub-event in the super event */
      1,                    /* log history */
      "", "", "",}
    ,
    helicity_history,             /* readout routine */
    NULL,NULL,NULL,
  },
#endif // helicity history



#endif
  
  { "" }
};

#ifdef HAVE_PSM
#include "setup_psm.c" /* psm subroutines */   
#endif

#include "write_client_code.c"
#ifdef HAVE_SIS3820
//#include "utils.c"; 
#include "sis3820_code.c"
#endif
#ifdef HAVE_SIS3801
#include "sis3801_code.c"
#endif
#ifdef GONE
INT scaler_increment_alpha(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
			   INT maximum_channel,INT *gbl_bin, DWORD *userbit );
#endif // GONE This is for testing timing using 8bit or 16bit data and the alpha mode

INT scaler_increment_pulsepair(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
			       INT maximum_channel,INT *gbl_bin, DWORD *userbit );
/*-- Call_back  ----------------------------------------------------*/
void call_back(HNDLE hDB, HNDLE hkey, void * info)
{
   KEY key;
   INT status;
   printf("call_back: starting\n");
   status =db_get_key(hDB, hKey, &key);
   if(status != SUCCESS)
     printf("call_back: error from db_get_key (%d)\n",status);
   else
     printf("call_back: key %s is of type %s %d\n",key.name,  rpc_tid_name(key.type), key.type);

  //  db_get_data(hDB,hkey, &value, &size, TID_INT);
 
  printf("call_back:  odb (hot link) touched; hkey= %d \"%s\" \n",hkey, (char *)info);
  cm_msg(MINFO,"call_back","odb (hot link) touched; hkey= %d \"%s\" ",hkey, (char *)info);

}


#ifdef HAVE_PSM
#ifdef BNMR
/*-- Call_back  ----------------------------------------------------*/

void hot_PSM_RFthr(HNDLE hDB, HNDLE hkey, void * info)
{
  INT status;

  printf("hot_PSM_rf_threshold: odb (hot link) touched; hkey= %d \"%s\" \n",hkey, (char *)info);
 /* Write the PSM RF Trip Threshold */
  status = write_RF_trip_threshold(gVme, PSM_BASE);

  if(status != SUCCESS)
    cm_msg(MINFO,"hot_PSM_rf_threshold","error writing PSM RF threshold");
  return;
}
#endif
#endif


void call_back_hot_threshold_trip(HNDLE hDB, HNDLE hkey, void * info)
{
  // This is for testing the thresholds or helicity mismatch 

   printf("\n ****  hot_threshold_trip: odb (hot link) touched; hkey= %d \"%s\" \n",hkey, (char *)info);
   printf("**** hot_threshold_trip: hot_threshold_trip= %d\n",fs.debug.hot_threshold_trip);

  hot_threshold_trip = fs.debug.hot_threshold_trip;
  if(hot_threshold_trip < 3)
    printf("hot_threshold_trip: hot-link touched ... next cycle will fail threshold check  %d\n",hot_threshold_trip);
  else
    printf("hot_threshold_trip: hot-link touched ... next cycle will fail helicity mismatch check  %d\n",hot_threshold_trip);
  return;
}

/*-- Call_back_skip  ----------------------------------------------------*/
void call_back_skip(HNDLE hDB, HNDLE hkey, void * info)
{
 printf("call_back_skip: odb (hot link) touched; hkey= %d \"%s\" \n",hkey, (char *)info);
 if(const1f_flag)
   {
     printf("call_back_skip: value fixed at 0 when running 1f in constant time mode\n");
     fs.hardware.skip_ncycles_out_of_tol = 0; // this must be set to 0 in this mode
   }
 return;

}
/*-- Call_back  ----------------------------------------------------*/
void call_back_debug(HNDLE hDB, HNDLE hkey, void * info)
{
  // toggles debugs
  char string[16];
  char str[8];
  char *p=str;
  int i;

  printf("call_back_debug: odb (hot link) touched; hkey= %d \"%s\" \n",hkey,(char *)info);
  strncpy(string,(void*)info, 12); // "hot_xxx"
  //printf("string=%s\n",string);
  // remove preciding "hot_"
  for (i=4; i<10; i++)
    {
      *p=string[i];
      p++;
    }
  *p='\0';

  //printf("str=%s\n",str);


  if(strncmp(str, "dppg",4)==0)
    {
      if (dppg == 0)
	dppg=1;
      else
	dppg=0;
    }
  else if (strncmp(str, "dcamp",5)==0)
    {
      if (dcamp == 0)
	dcamp=1;
      else
	dcamp=0;
    }
  else if (strncmp(str, "dpsm",4)==0)
    {
      if (dpsm == 0)
	dpsm=1;
      else
	dpsm=0;
    }

  else if (strncmp(str, "manu",4)==0)
    {
      if (manual_ppg_start == 0)
	manual_ppg_start=1;
      else
	manual_ppg_start=0;
    }

  else if (strncmp(str, "dsis",4)==0)
    {
      if (dsis == 0)
	dsis=1;
      else
	dsis=0;
    }


  else
    printf("call_back_debug: did not recognize \"%s\" \n",str);

}

/*-- Call_back  ----------------------------------------------------*/
void call_back_debug_level(HNDLE hDB, HNDLE hkey, void * info)
{
  INT i, index;
  //  printf("odb (hot link) touched; hkey= %d \"%s\" \n",hkey,(char *)info);

  //if(!dd[11])
    printf ("call_back_debug_level: toggling debug level %d\n", fs.debug.hot_debug_level___1_clr_);

  index = fs.debug.hot_debug_level___1_clr_;
  if (index < 0 || index >  NUM_DEBUG_LEVELS )
    {   // turn off all debugging
      for (i=0; i< NUM_DEBUG_LEVELS; i++)
	dd[i]=0;
      printf("call_back_debug_level: all debugging levels are now turned off\n");
      return;
    }

  // Toggle the debug
  if (dd[index] == 0 )
    {
      dd[index] =  1;
      //  printf("call_back_debug_level: debug level %d is now enabled\n",index);
    }
  else
    {
      dd[index] =  0;
      // printf("call_back_debug_level: debug level %d is now disabled\n",index);
    }  

   if(!dd[11])printf("call_back_debug_level: to turn off all debugging set to -1 \n");
 
  int j=0;
  for(i=0; i< NUM_DEBUG_LEVELS; i++)
    {
      if(dd[i]>0) printf("call_back_debug_level: debug level %d is enabled\n",i);
      j++;
    }
  if(j==0)
    printf("call_back_debug_level: all debug levels are disabled\n");
  else
    printf("call_back_debug_level: all other debug levels are disabled\n");


  if(dd[11])
    {
      printf("call_back_debug_level: level 11 is set. \n");
      printf(" \n    All printing to the screen is disabled. \n");
    }
}

/*-- Dummy routines ------------------------------------------------
  called by mfe.c                                */
INT interrupt_configure(INT cmd, INT source[], PTYPE adr){return 1;};

#ifdef DEFERRED
BOOL wait_end_cycle(INT run_number, BOOL flag)
{
  
  INT status;
  static INT first_time;
  INT elapsed_time;
  // dd[9]=1; // TEMP
  if(gbl_first_call)
    {
      printf("\n");
      if(dd[9])
	printf("\nwait_end_cycle: starting with waiteqp[HISTO]=%d, gbl_first_call=%d\n",
	       waiteqp[HISTO],gbl_first_call);
      cm_msg(MINFO,"wait_end_cycle","waiting for final cycle to complete before stopping run");
      first_time=ss_time();
      gbl_transition_requested = TRUE; /* prevents restarting cycle */

      if(max_cycle_time ==0)
	/* calculate the approximate time one cycle should take */
	max_cycle_time = fs.output.dwell_time__ms_ * (float)  fs.output.num_dwell_times;
      if(max_cycle_wait == 0)
	max_cycle_wait = (INT) ((max_cycle_time *  1.25/1000) + 0.5); /* add 25%, convert to seconds and round up */
      if (max_cycle_wait <= 1) 
	max_cycle_wait = 3; 
      printf("wait_end_cycle:time to complete one cycle:%.1f ms (or %.0f sec)\n",max_cycle_time,max_cycle_time/1000);
      printf("    Maximum time to wait for last histo event (before timeout) is: %d sec\n",max_cycle_wait);
      gbl_first_call = FALSE;
      status = iwait(200,"wait_end_cycle"); /* wait 200ms */
      if(status != SUCCESS)
	return status;
    }
  
  if(waiteqp[HISTO])
    {
      elapsed_time=ss_time()-first_time; /* in seconds */
      if(dd[9])
	printf("wait_end_cycle: Waiting for last HISTO event... max_cycle_wait=%d sec, elapsed time=%d sec\n",
	       max_cycle_wait,elapsed_time);
      if(elapsed_time < max_cycle_wait)
	return FALSE; 
      
      cm_msg(MINFO,"wait_end_cycle","timeout waiting for last event after %d seconds. Stopping run now",elapsed_time);
    }
  else
    cm_msg(MINFO,"wait_end_cycle","last histo event has been sent, stopping run now");
  
  
  gbl_transition_requested=FALSE;
  return TRUE;
  
}
#endif  // deferred

/*-- Frontend Init -------------------------------------------------
  called by mfe.c                                */
INT frontend_init()
{
  INT status,i,size;
  BOOL watchdog_flag;

  printf ("frontend_init: %s frontend code is starting (%s)\n",BEAMLINE, beamline);
  
  status = hardware_check();
  if(status != SUCCESS)
    return status;
    
  
  // VMIC
#ifdef HAVE_VMIC
  status = mvme_open(&gVme,0);
  status = mvme_set_am(gVme, MVME_AM_A24_ND);
#endif


  /* initialize  (get keys, malloc histo and scaler arrays, initializes SIS scalers  */
  status = bnmr_init(equipment[FIFO].name,  equipment[INFO].name,equipment[CYCLE].name );
  if(status != FE_SUCCESS)
    return status;

  status =   cm_register_transition(TR_STOP, post_end_run, 750) ;
  if(status != CM_SUCCESS)
     {
      cm_msg(MERROR,"frontend_init","Failed to register transition post end_of_run (%d)\n",status);
      return status;
    }

#ifdef DEFERRED
  status = cm_register_deferred_transition(TR_STOP,wait_end_cycle);
  if(status != CM_SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","Failed to register deferred transition on stop (%d)\n",status);
      return status;
    }
#endif

  cm_get_watchdog_params(&watchdog_flag, &gbl_watchdog_timeout);
  printf("frontend_init: current watchdog parameters are gbl_watchdog_timeout=%d; watchdog_flag=%d \n",
	 gbl_watchdog_timeout, watchdog_flag);

  exp_mode = -1;




#ifdef EPICS_ACCESS
  /* initialize epics scan globals in bnmr_epics.h */
  epics_params.Epics_bad_flag = 0;
  epics_params.NaCell_flag= epics_params.Laser_flag = epics_params.Field_flag = FALSE;
  epics_params.XxRchid = epics_params.XxWchid  =-1;
  Rchid=-1;

  caInit(); /* calls  ca_task_initialize(); */
  
  /* GetIDs for Epics scan ... now done at begin run */
  /*  EpicsGetIds( &epics_params ); */ 
      
  /* Time for watchdog (keeps channel open) on direct access to Epics channels */
  /* Epics_last_time = ss_time(); */
  
#endif /* Epics access */

  
  /* Init all pointers to potential MALLOC calls to NULL */
  for (i=0 ; i < N_SCALER_MAX ; i++)
    {
      scaler[i].ps = NULL; 
      scaler[i].qs = NULL;
    }
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {      
      histo[i].ph = NULL;
      histo[i].qh = NULL;
    }
   print_hardware_flags();

 
  /* Make sure threshold alarms don't keep going off if frontend crashed previously */
  cyinfo.last_failed_thr_test=0;
  size = sizeof(cyinfo.last_failed_thr_test);
  if(!hInfo)printf("\n ** hInfo not assigned\n");
  status = db_set_value(hDB, hInfo, "last failed thr test", 
			&cyinfo.last_failed_thr_test, size, 1, TID_DWORD);
  if (status != DB_SUCCESS)
      cm_msg(MERROR, "frontend_init", "cannot clear \"...info_odb/var/last failed thr test\" (%d)",
	     status);


  /* clear helicity mismatch alarm */
  status = set_alarm_flag("hel_mismatch",0);

  al_reset_alarm(NULL); /* reset all alarms */



#ifdef EPICS_ACCESS
#ifdef LASER
  /* keeping Laser channel open all the time seems to make it hard to shutdown 
     the frontend  - try opening it each run instead
     this code is moved to begin_of_run
*/

  /* Open a channel to the laser power */
  if(fs.hardware.disable_laser_thr_check)
    {
      printf("frontend_init: laser power threshold is disabled in the odb \n");
      epicstime=0;
    }
  else
    { /* laser power threshold check is enabled */
      
      /*     Reconnect to Epics Laser Power (channel access) */
      printf("frontend_init: opening direct channel access for Laser Power read\n");
      set_long_watchdog(gbl_long_watchdog_time);
      Rchid_lp = -1;
      status = LaserPower_init(&Rchid_lp, Rlaser); /* Opens direct access to Epics 
							   fills Rchid_lp, Rlaser 

 
							*/ 
      restore_watchdog();
      if (status != SUCCESS) 
	{
	  cm_msg(MERROR,"frontend_init","failure from LaserPower_init(%d)",status);
	  return status;
	} 
      /* now read the Laser Power value */ 
      set_long_watchdog(gbl_medium_watchdog_time); /* 2 min */ 
      status = read_Epics_chan(Rchid_lp,  &Laser_Power_volts);
      restore_watchdog();
      if(status != SUCCESS) 
	{
	  cm_msg(MERROR,"frontend_init","failure reading EPICS LaserPower(%d)",status);
	  return  (FE_ERR_HW);
	}

      Laser_last_time=ss_time();
      printf("frontend_init: read_Epics_chan returns Laser Power as %f Volts\n",Laser_Power_volts);	 
/*
#ifdef LASER
      if(Laser_Power_volts==0) Laser_Power_volts=glp; // give an initial value 
 printf("frontend_init: Laser Power Read value adjusted to 0.3 for testing\n");
#endif // LASER TEST */
    }
  /* end of opening EPICS laser power  */
#endif /* LASER */
#endif /* EPICS_ACCESS */

  // printf("DEFAULT_MAX_EVENT_SIZE=%d\n",DEFAULT_MAX_EVENT_SIZE);
#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
sprintf(ppg_loadfile,"%s/ppgload.dat", fs.input.cfg_path);
#else
sprintf(ppg_loadfile,"%s/bytecode.dat", fs.input.cfg_path);
#endif

 printf("\n ** frontend_init:  PPG load file is %s\n",ppg_loadfile);
#endif // HAVE_PPG

 // Do not exercise helicity in case other frontend is running
 // test_helicity(); // make sure frontend can control helicity


 // testing  for sample/ref mode
 if(dd[19])
   { 
     if(fs.hardware.enable_sampleref_mode)
       {
	 printf("frontend_init: testing sample/ref mode\n");
	 sample_read();
	 printf("gbl_read_sample= %d\n",gbl_read_sample);
	 sample_flip();
	 printf("after flip, gbl_read_sample= %d\n",gbl_read_sample);
	 gbl_set_sample=0;
	 sample_set(gbl_set_sample);
	 sample_read();
	 printf("set sample to 0, now gbl_read_sample=%d\n",gbl_read_sample);
	 gbl_set_sample=1;
	 sample_set(gbl_set_sample);
	 sample_read();
	 printf("set sample to 1, now gbl_read_sample=%d\n",gbl_read_sample);
	 printf("frontend_init: end of testing sample/ref mode\n");
       }
   }
#ifdef HAVE_SIS3820
#ifndef TWO_SCALERS  // BNQR only
 // Update the cycle_scalers area (alpha/sr mode 2 NB only when using SIS3820)
 CYCLE_SCALERS_TYPE2_SETTINGS_STR(type2_str);  // These names are good for Type 1 and Type 2
 status = db_find_key(hDB, 0, "/Equipment/Cycle_scalers/Settings/",&hKey);
 if(status != DB_SUCCESS && status != DB_NO_KEY)
   { 
     cm_msg(MERROR, "frontend_init", "error accessing \"/Equipment/Cycle_scalers/Settings/\"  (%d)",status);
     return status;
   }
 if(status == DB_SUCCESS)
   db_delete_key(hDB,hKey,FALSE);
 
 status = db_create_record(hDB,0,"/Equipment/Cycle_scalers/Settings",strcomb((const char **)type2_str));
 if(status !=DB_SUCCESS)
   {
     cm_msg(MERROR, "frontend_init", "cannot create \"/Equipment/Cycle_scalers/Settings\" (%d)",status);
     return DB_NO_ACCESS;
   }
 else
   printf("frontend_init: successfully updated Scaler 2 Names\n");
#endif 
#endif // HAVE_SIS3820
 
 // TEMP
#ifdef HAVE_SIS3820
 printf("SIS3820 Scaler:\n");
#else
#ifdef HAVE_SIS3801
 printf("SIS3801 Scaler:\n");
#endif
#endif
 printf("Note: Scaler Inputs in this list are numbered from zero\n");
 printf("NEUTRAL_BEAM_F =%d \n",NEUTRAL_BEAM_F);
 printf("NEUTRAL_BEAM_B =%d \n",NEUTRAL_BEAM_B);
 printf("ALPHA_CHAN0=%d  \n",ALPHA_CHAN0 );
 printf("ALPHA_OFFSET=%d\n", ALPHA_OFFSET);
 printf("MAX_CHAN_SIS38xxB=%d\n",MAX_CHAN_SIS38xxB ); 
 printf("N_SCALER_REAL =%d\n",N_SCALER_REAL ); 
 printf("N2_HISTO_ALPHA =%d\n",N2_HISTO_ALPHA );  
 printf("N2_HISTO_MAXB =%d\n",N2_HISTO_MAXB ); 
 printf("N_HISTO_MAXB=%d\n",N_HISTO_MAXB ); 
 printf("N2_SCALER_CYC  =%d\n",N2_SCALER_CYC ); 
 printf("N2_SCALER_CUM=%d\n",N2_SCALER_CUM );
 printf("N2_SCALER_TOT=N_SCALER_REAL+N2_SCALER_CYC+N2_SCALER_CUM = %d\n",N2_SCALER_TOTAL );
 printf("N_SCALER_MAX  =%d\n",N_SCALER_MAX); 
 printf(" N_HISTO_MAX  =%d\n", N_HISTO_MAX); 
 printf("N1_HISTO_MAXB=%d \n", 	 N1_HISTO_MAXB);
 printf("N1_HISTO_MAXB_ORIG=%d\n",N1_HISTO_MAXB_ORIG);

 // TEMP

 printf("\n End of routine frontend_init. VMIC Frontend is READY \n");
 return FE_SUCCESS;
}


/*-- Frontend Exit -------------------------------------------------
  called by mfe.c                                */
INT frontend_exit()
{
  int i;
  
  /* free SIS FIFO memory (both scalers) */
#ifdef TWO_SCALERS
  if (pfifo_A) free(pfifo_A);
  
  /* force acquisition stop */
#ifdef HAVE_SIS3801
  sis3801_next_logic(gVme, SIS3801_BASE_A, SIS3801_DISABLE_NEXT_CLK_WO);
#endif
#ifdef HAVE_SIS3820
  sis3820_RegisterWrite(gVme,SIS3820_BASE_A,SIS3820_KEY_OPERATION_DISABLE,0);
#endif
#endif // 2 SCALERS
  
  if (pfifo_B) free(pfifo_B);
  
#ifdef HAVE_SIS3801
  sis3801_next_logic(gVme, SIS3801_BASE_B, SIS3801_DISABLE_NEXT_CLK_WO);
 
  // VMIC with SIS3801E uses acquisition precount instead
#ifdef TWO_SCALERS
  sis3801_int_source_disable(gVme, SIS3801_BASE_A, SOURCE_CIP);
#else
  sis3801_int_source_disable(gVme, SIS3801_BASE_B, SOURCE_CIP);
#endif //  TWO_SCALERS
#endif // HAVE_SIS3801
  
#ifdef HAVE_SIS3820
#ifdef TWO_SCALERS
  sis3820_RegisterWrite(gVme,SIS3820_BASE_A,SIS3820_KEY_OPERATION_DISABLE,0);
#endif //  TWO_SCALERS
  sis3820_RegisterWrite(gVme,SIS3820_BASE_B,SIS3820_KEY_OPERATION_DISABLE,0);
#endif // HAVE_SIS3820 
  
  printf("\nfree scaler ");
  for (i=0 ; i < N_SCALER_MAX ; i++)
    {
      if (scaler[i].ps != NULL)
	{
	  printf(" %d", i);
	  free(scaler[i].ps);
	  scaler[i].ps = NULL;
	}
      if (scaler[i].qs != NULL)
	{
	  printf(" %d", i);
	  free(scaler[i].qs);
	  scaler[i].qs = NULL;
	}
    }
  printf("\n");
  printf("free histo ");
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {
      if (histo[i].ph != NULL)
	{
	  printf(" %d ", i);
	  free(histo[i].ph);
	  histo[i].ph = NULL;
	}
      if (histo[i].qh != NULL)
	{
	  printf(" %d ", i);
	  free(histo[i].qh);
	  histo[i].qh = NULL;
	}
    }
  printf("\n");
  
  
  if(random_flag)
    free_freq_pntrs();
  
  
  /*Disable the PPG module just in case */
#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
  TPPGReset(gVme,PPG_BASE); // also stops the program even if in long delay
  TPPGDisableExtTrig(  gVme, PPG_BASE);
#else
  VPPGStopSequencer(gVme, PPG_BASE);
  VPPGDisableExtTrig ( gVme, PPG_BASE );
#endif
#endif
  


#ifdef HAVE_PSM
  /* Shut down the PSM */
  INT status;
  status = disable_psm(gVme, PSM_BASE);
  if(status == SUCCESS)
    printf("frontend_exit: PSM is now disabled\n");
  else
    cm_msg(MINFO,"frontend_exit","error disabling PSM");
#endif
  
  
#ifdef EPICS_ACCESS
  caExit(); /* closes any open channels for Epics  */
#endif
  
#ifdef HAVE_VMIC
  mvme_close(gVme);
#endif // HAVE_VMIC


  printf("frontend_exit:  mfe procedure exiting \n");
  return CM_SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------
called periodically by a loop in mfe.c           
*/
INT frontend_loop()
/*
  - Main function for re-starting a cycle.
  - if RUNNING and off cycle 
  - start new cycle
*/
{
  // printf("frontend_loop: starting\n");
  INT status=SUCCESS;
  static struct timeval t5,t6;
  static int first=1;
  double elapsed_time;//,thd;
  static INT kounter=0;
  INT condition;
  /* DWORD rf_data=5; */
  

  if (run_state == STATE_RUNNING)
    { /*      STATE_RUNNING  
	      check if cycle is finished (and histo update being done) */
      if(first) 
	{
	  first=0;
	  gettimeofday(&t5, NULL);
	}

      if(gbl_IN_CYCLE) // running in cycle
	{
	  gettimeofday(&t6, NULL);
	  // compute elapsed time in millisec
	  elapsed_time = (t6.tv_sec - t5.tv_sec) * 1000.0;      // sec to ms
	  elapsed_time += (t6.tv_usec - t5.tv_usec) / 1000.0;   // us to ms

	// don't call this more often than every 10 ms (1fconst otherwise every 50ms)
	  if(elapsed_time >=  gbl_lne_poll_time)
	    {
	      // printf("frontend_loop: Elapsed time: %f  ms.  first=%d gbl_lne_poll_time=%d\n",elapsed_time,first,gbl_lne_poll_time);

	      check_lne(); // sets gbl_IN_CYCLE FALSE if SIS has received enough LNE
	      gettimeofday(&t5, NULL); // remember when this was last called
	    }
	} // end of gbl_IN_CYCLE

      if (!gbl_IN_CYCLE)
	{ /* not in cycle */
	  if(dd[2])
	    printf("frontend_loop: Not in cycle\n");

#ifdef GONE 
	  // this was for alpha chunks to be read out during sleep time; but hel flip
          // is called in the middle of cycle_start
	  if(!fs.hardware.enable_dual_channel_mode)
	    {
              // Dual channel mode - waits for hardware to restart cycle
	      // Single channel mode only... wait for helicity to flip without using a sleep
 
	      if(gbl_wait_helicity)
		{ /* don't restart the cycle until after the helicity sleep time
		     don't sleep or cm_yield because this won't allow histos to be sent out
		  */
		  gettimeofday(&th2,NULL);
		  // compute elapsed time in millisec
		  thd = (th2.tv_sec - th1.tv_sec) * 1000.0;      // sec to ms
		  thd += (th2.tv_usec - th1.tv_usec) / 1000.0;   // us to ms
		  //printf("\n hel wait time = %f ms\n",thd);
		  if (thd  >  (double) fs.hardware.helicity_flip_sleep__ms_ )
		    {
		      printf("\nfrontend_loop: elapsed time for hel flip sleep is %f ms \n",thd);
		      gbl_wait_helicity = 0;
		    }
		  else
		    {
		      printf(".");
		      ss_sleep(20);
		    }
		}
	      // printf("frontend_loop: gbl_wait_helicity=%d\n",gbl_wait_helicity);
	      if(gbl_wait_helicity)
		return SUCCESS;  // still waiting
	    }  // end single channel mode
#endif // GONE

	  /* wait until other equipments have run */
	  condition = (waiteqp[FIFO] || waiteqp[HISTO] || waiteqp[CYCLE] || waiteqp[DIAG]); // default	  
	  if (gbl_alpha && alpha_chunks)
	    condition =  (waiteqp[FIFO] || waiteqp[CYCLE] || waiteqp[DIAG]);

#ifdef SPEEDUP
	  if(const1f_flag)
	    {
	      // SPEEDUP for const1f mode. Restart the cycle before histo_read has run (but after histo_process)
	      condition = (waiteqp[FIFO] || waiteqp[CYCLE] || waiteqp[DIAG]); 
	    }
#endif
	  if(condition)
	    {	    
	      //	printf("frontend_loop: waiting for one of waiteqp[FIFO],[HISTO],[CYCLE],[DIAG]=%d %d %d %d; not restarting cycle\n",
	      //  waiteqp[FIFO],waiteqp[HISTO],waiteqp[CYCLE],waiteqp[DIAG]);
	      status= SUCCESS; /* cycle is not yet finished i.e. all equipments have not run */
	      goto exit_fe_loop;
	    }
	  
#ifdef DEFERRED 
	  if(gbl_transition_requested) /* we are waiting for final histos before stopping the run */
	    {
	      if(dd[9] || dd[1])
		{
		  printf("frontend_loop: a deferred transition has been requested\n");
		  if (waiteqp[HISTO])
		    printf("frontend_loop: waiting for HISTO to run \n");
		  else
		    printf("frontend_loop: HISTO equipment has run, not restarting cycle\n");
		}
	      
	      status= SUCCESS; 
	      goto exit_fe_loop;/* do not restart the cycle as we are waiting to stop */
	    }
#endif /* DEFERRED */ 
	  
	  
	  if (fs.hardware.num_cycles > 0)  
	    {
	      /* fixed number of cycles per run is enabled
		 Stop run after n_cycles +1 (first cycle is thrown away)  */
	      if (gbl_CYCLE_N >= fs.hardware.num_cycles+1)
		{
		  // write_client_code(LIMIT_ERR,SET,"frontend"); /* not an error... */
		  // cm_msg(MINFO,"frontend_loop",
		  //	 "Stopping the run after reaching requested number of cycles (i.e. %d)",
		  //	 fs.hardware.num_cycles);

		  cm_msg(MINFO,"frontend_loop", "Stopping the run after reaching requested number of cycles (i.e. %d)",
		  	 fs.hardware.num_cycles);
                  cycles_request_flag=TRUE; // needed for message in frontend loop
		  stop_run(); // sets gbl_waiting_for_run_stop flag
		}
	    }	    
	  /* Request for Fixed number of cycles is disabled (=0) 
	     i.e. free running, or we haven't reached the limit yet  */
	  
	  
	  
	  if(gbl_waiting_for_run_stop)
	    {
	      if(cycles_request_flag)
		printf("frontend_loop: Not restarting cycle... waiting for run to stop after requested number of cycles is reached\n");
	      else
		{
		  kounter++;
		  if(kounter > 10) 
		    goto exit_fe_loop;
		  if(kounter<10)printf("frontend_loop: Not restarting cycle...waiting for run to stop\n");
		  if(kounter%10)printf("If run does not stop, do a stop now\n");
		      
		  status=SUCCESS;
		  goto exit_fe_loop;
		}
	    }
	  
	   if(dd[1])
	  printf("frontend_loop: restarting cycle with waiteqp[FIFO],[HISTO],[CYCLE]=%d %d %d ; histo_process_flag=%d  send_histo_event=%d \n",
	         waiteqp[FIFO],waiteqp[HISTO],waiteqp[CYCLE], histo_process_flag, send_histo_event);

	  if(!dd[11])
	    {
	      if (dd[8])  printf("\n");  // do not overwrite info.
	      printf("\r;end, hel: %d;  ",gbl_ppg_hel);
	    } 

	  /* Start new cycle */
	  status = cycle_start();
          first=1; // reset first
	  if(status != SUCCESS)
	    {
	      cm_msg(MERROR,"frontend_loop","Error return from cycle_start. Stopping Run");
	      status=stop_run();
	    
	      goto exit_fe_loop;
	    }
	  
	} /* end of not in cycle */ 
      else
	{ /* running in Cycle (i.e. gbl_IN_CYCLE is TRUE) */

#ifdef HAVE_SIS3820_OR_SIS3801E
	  if(dd[2])printf("\nfrontend_loop: running in cycle gbl_bin_count=%d; n_bins=%d, cycle %d  \n",
			 gbl_bin_count,n_bins, gbl_CYCLE_N);
#endif
	  
#ifdef HAVE_SIS3801
	  /* nothing to do */
	  if(dd[2])
	    printf("frontend_loop: running in cycle; nothing to do; exit\n");
#endif
	  status=SUCCESS;
	  goto exit_fe_loop;
	  
	} 
    } /* end of STATE_RUNNING */
  else
    { /* NOT running ...   nothing to do */
      // if(dd[2])
      //	printf("frontend_loop: NOT running, nothing to do; exit\n"); 
 
      status=SUCCESS;
      kounter=0;
    } /* Not running */
  
 exit_fe_loop:

  return status;
}







#ifdef GONE
INT write_data_format(BOOL gbl_alpha)
{
  DWORD opbits, temp;
  INT i,data_format;
  if(gbl_alpha)
    i=8; // NumSisChannels; 8 bits
  else
    i=NUM_DATA_BITS; // NumSisChannels; 24 bits default 
  
  data_format = get_SIS_dataformat(i, NumSisChannels_B);  //  NumSisChannels; 8 bits
 
  cm_msg(MINFO,"write_data_format","SIS3820 data format will be set to : %d bits, data_format=0x%x",i,data_format);

  opbits = sis3820_RegisterRead(gVme,SIS3820_BASE_B,SIS3820_OPERATION_MODE); // 0x100 
  printf("Reading 0x%x from Operation Mode Register\n",opbits);
  temp= (opbits & 0xC) ;
  printf("opbits=0x%x; temp=0x%x, data_format =0x%x\n",opbits,temp,data_format);
  if(temp !=  data_format )  // get present format
    {
      opbits = ((opbits & 0xFFFFFFF3) | data_format);
      printf("Writing 0x%x to Operation Mode Register\n",opbits);
      sis3820_RegisterWrite(gVme,SIS3820_BASE_B,SIS3820_OPERATION_MODE, opbits); // 0x100 
      opbits = sis3820_RegisterRead(gVme,SIS3820_BASE_B,SIS3820_OPERATION_MODE); // 0x100 
      printf("Read 0x%x from  Operation Mode Register\n",opbits);
    }
  my_max_chan_sis38xxB= MAX_CHAN_SIS38xxB;
  if(gbl_alpha)
    {
      enabled_channels_mask_B = get_enabled_channels_bitpat(MAX_CHAN_SIS38xxB, 0); // ref ch1 DISABLED
      my_max_chan_sis38xxb= MAX_CHAN_SIS38xxB - 1 ; // one less channel
      csrbits = CTRL_REFERENCE_CH1_DISABLE | CTRL_COUNTER_TEST_25MHZ_DISABLE |  CTRL_COUNTER_TEST_MODE_DISABLE ;
    }
  else
    {
      enabled_channels_mask_B = get_enabled_channels_bitpat(MAX_CHAN_SIS38xxB, enable_ref_ch1); // ref ch1
      csrbits = CTRL_REFERENCE_CH1_ENABLE | CTRL_COUNTER_TEST_25MHZ_DISABLE |  CTRL_COUNTER_TEST_MODE_DISABLE ;
    }
  sis3820_RegisterWrite(gVme,SIS3820_BASE_B , SIS3820_CONTROL_STATUS, csrbits );

  NumSisChannels_B  = nchan_enabled(enabled_channels_mask_B, 1); // calculate number of enabled SIS channels
  if (NumSisChannels_B !=   my_max_chan_sis38xxb)
    {
      cm_msg(MERROR,"init_sis3820","Programming Error!!  number of enabled channels for Module B (%d) does not agree with required number %d",
	     NumSisChannels_B,my_max_chan_sis38xxB);
      return DB_INVALID_PARAM;
    }

    
   
  return 1;
}
#endif // GONE
 

/*-- Begin of Run sequence ------------------------------------------
                              called by mfe.c                        */

INT begin_of_run(INT run_number, char *error)
{
/*
  - update fs struct based on settings names (no hot links)
  - cleanup and book previous histo and scalers memory
  - check acq settings
  - book new histograms
  - reset histo
  - start acq

note: mdarc initializes client status flag for frontend on a prestart transition
       - set flag to success on successful begin of run
*/

  INT  i,j,k, status, size;
  // INT h;
  char str[128];
  
  BOOL rfc_flag;
  /* TEMP for PSM debug */
  /*
#ifdef HAVE_PSM
  BOOL watchdog_flag;
  DWORD watchdog_timeout;
#endif
  */
  
  INT ninc; /* number of increments of any type 1 scan */
#ifdef TWO_SCALERS
  CYCLE_SCALERS_TYPE1_SETTINGS_STR(type1_str);
  CYCLE_SCALERS_TYPE2_SETTINGS_STR(type2_str);
#endif
  gbl_die=0;
  gbl_transition_requested=FALSE; /* used for deferred transition */
  gbl_waiting_for_run_stop = FALSE; /* unrecoverable error */
  loop_error_cntr=0;
  gbl_run_number = run_number;
  gbl_IN_CYCLE = FALSE;
  gbl_CYCLE_N = 0;
  histo_trigger_mask=1; // default; changed when histos are sent in pieces (2h mode)
  small_bin_flag=0; // if true, gbl_bin_size is sizeof(WORD). True for 2h mode only
  gbl_bin_size=sizeof(DWORD); // default size for storage for scaler,histo bins
  alpha_scaler_offset=0; // default - no scaler offset
#ifdef SPEEDUP   // presently used in const1f mode only. Could be used for other modes if required/debugged
                 // causes cycle to be restarted before histos are sent.
  gbl_CYCLE_H=0; // Mode 1f const time only. Cycle number of histos waiting to be sent out. Zeroed when all are sent.
  gbl_freq_H=0;  // Mode 1f const time only. Frequency for histos at cycle  gbl_CYCLE_H 
  gbl_HistoRead_skipped_cycle=0; // counts number of times skip_cycle is set when HistoRead is called (expect 0)
  gbl_prev_histo_cntr=0; // counts cases where previous histograms have not been sent out (expect 0)
  gbl_prev_skip_cntr=0;  // counts cases where last cycle skipped, but waiteqp[HISTO] is set (expect 0)
  gbl_ppg_restart_cntr=0; // number of times ppg rebooted excluding at BOR, flip helicity. Counts number of computer busy too long for PPG. 
  gbl_count=0;// temp
  histo_process_flag=0; // clear
#ifdef HAVE_NIMIO32
  nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT5 ); // Clear OUTPUT5 (sending histograms)
#endif // HAVE_NIMIO32
#endif // SPEEDUP

  // const1f mode only
  const1f_stepped_back=0;
  const1f_prev_freq_val=0;
  const1f_junk_data=0;

  gbl_SCYCLE_N = 0;
  gbl_SCAN_N = 0;
  skip_cycle = FALSE;
  cyinfo.cancelled_cycle = 0;
  cyinfo.wrong_hel_cntr = 0;
  cyinfo.cycle_when_last_failed_thr=0;
  cyinfo.last_failed_thr_test=0;
  gbl_tolerance_flag = 0;
  cyinfo.ncycle_sk_tol = 0;
  rfc_flag = 0; 
  gbl_fix_inc_cntr = 0;
  gbl_first_call = TRUE; /* for prestop */
  cycles_request_flag=FALSE; // global set if number of requested cycles is reached 
#ifndef EPICS_ACCESS
  hel_warning=FALSE;
  Laser_last_time=ss_time();
#endif
  gbl_cb_cntr1=gbl_cb_cntr2=gbl_ppg_cb_cntr1=gbl_ppg_cb_cntr2=gbl_ppg_cb_cntr3=0;
  e1f_started_flag=FALSE;
  gbl_maxElapsedTime=gbl_sumElapsedTime=gbl_sumNum=0;
  gbl_minElapsedTime=5000.0; // set to large number
  hel_flip_flag=gbl_hel_mismatch_flag=gbl_hel_recover=0;
  gbl_lne_poll_time = POLL_LNE;
  hot_threshold_trip=0; // clear 
  epicstime=0;
  gbl_stopping_run_flag=loop_error_cntr=0;
  gbl_dcm_hcur=0; // Used only in dual_channel_mode
  gbl_pmax=gbl_rmax=0; // fifo_acq timing tests
  gbl_pmin = gbl_rmin = 20;  // set to large number
  gbl_sr=gbl_alpha=alpha_chunks=0;
 gbl_wait_helicity = 0;

 n_scaler_total=N_SCALER_MAX; // default; maximum possible scalers
 last_piece_sent=1;


  // Enable client alarm
  status = set_client_alarm(1);
  if(status != SUCCESS)
    return status;

 /* set client status flag for frontend to SUCCESS */
  status = set_client_flag("frontend",SUCCESS) ; /* set client flag to success */  
  if(status != SUCCESS)
    return status;

  write_client_code(0,0,"frontend"); /* zero clears all bits */
  set_alarm_flag("hel_mismatch",0); // clear flag
  printf("begin_of_run: gbl_ppg_hel = %d\n",gbl_ppg_hel);
 
  fflush(stdout);

  /* do not clear hold flag at BOR - set by mheader for type 1 */
 
  /* Get current FIFO_ACQ settings */
  size = sizeof(fs);
  status = db_get_record(hDB, hFS, &fs, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin_of_run", "cannot retrieve FIFO_ACQ/Settings (%d)",status);
      return DB_NO_ACCESS;
    }


#ifdef TWO_SCALERS
  status = db_find_key(hDB, 0, "/Equipment/Cycle_scalers/Settings/",&hKey);
  if(status != DB_SUCCESS && status != DB_NO_KEY)
    { 
      cm_msg(MERROR, "begin_of_run", "error accessing \"/Equipment/Cycle_scalers/Settings/\"  (%d)",status);
      return status;
    }
  if(status == DB_SUCCESS)
    db_delete_key(hDB,hKey,FALSE);
  
  if (  strncmp(fs.input.experiment_name,"1",1) == 0) {
      exp_mode = 1; /* Imusr type - scans */
      status = db_create_record(hDB,0,"/Equipment/Cycle_scalers/Settings",strcomb((const char **)type1_str));
    }
  else {
    exp_mode = 2; /* TDmusr types - noscans */
    status = db_create_record(hDB,0,"/Equipment/Cycle_scalers/Settings",strcomb((const char **)type2_str));
    /*    status = db_set_record(hDB,hKey, &type2_str,size,0); */
  }
  if(status !=DB_SUCCESS){
      cm_msg(MERROR, "begin_of_run", "cannot create \"/Equipment/Cycle_scalers/Settings\" (%d)",status);
      return DB_NO_ACCESS;
    }
#else
  if (  strncmp(fs.input.experiment_name,"1",1) == 0) 
    exp_mode = 1; /* Imusr type - scans */
  else 
    exp_mode = 2; /* TDmusr types - noscans */
#endif


#ifdef HAVE_PSM
  printf("begin_of_run: clearing PSM RF Trip \n");
  psmClearRFpowerTrip(gVme, PSM_BASE);
  cyinfo.rf_state = 0; /* set to zero or alarm will go off */

  size = sizeof(cyinfo.rf_state);
  status = db_set_value(hDB, hInfo, "RF state",
                        &cyinfo.rf_state, size, 1, TID_DWORD);
  if (status != DB_SUCCESS)
    cm_msg(MERROR, "begin_of_run", "cannot clear \"...info_odb/variables/rf state\" (%d)",
           status);
#endif
 
  /* ppg_mode is needed for PSM IQ pair files as well as PSM freq files */
  strncpy(ppg_mode,fs.input.experiment_name,2);
  ppg_mode[2]='\0'; /* terminate */
  for  (i=0; i< strlen(ppg_mode) ; i++) 
    ppg_mode[i] = tolower (ppg_mode[i]); 
  
  printf("Experiment type = %s; mode = %i, ppg_mode=%s ppg template=\"%s\"\n", 
	 fs.input.experiment_name,
	 exp_mode, ppg_mode, fs.output.ppg_template);


  // Helicity Flipping
  flip = fs.hardware.enable_helicity_flipping;
  printf("begin_of_run: flip = %d\n",flip);

  /* Set the initial helicity state  */
  status = set_init_hel_state(flip,HEL_DOWN); /* initial helicity state is HEL_DOWN */
  if(status != SUCCESS)
    {
      if(!dhw)
	{
	  printf("begin_of_run: error return from set_init_hel_state; run should be stopped\n");
	  stop_run();
	  return status;
	}
      else
	{
	  printf("begin_of_run: error return from set_init_hel_state; dhw is TRUE so run will continue\n");
	}
    }
  else
    if(dd[12])printf("begin_of_run: helicity requested is now in initial starting state of %d\n",gbl_ppg_hel);      
  sprintf(latched,"current "); // global for helicity readback messages

  gbl_sr =  fs.hardware.enable_sampleref_mode;  // only set for Type 20
  gbl_alpha = fs.hardware.enable_alpha_mode;  // only set for Type 2h
#ifdef TWO_SCALERS  // BNMR only
  if(gbl_sr)
    {
      cm_msg(MERROR,"begin_of_run", "sample/ref mode is not supported for BNMR (two scalers)" );
      stop_run();
      return FE_ERR_HW;
    }

 
  if(gbl_alpha)
    {
      cm_msg(MERROR,"begin_of_run", "alpha mode (2h) is not supported for BNMR (two scalers)" );
      stop_run();
      return FE_ERR_HW;
    }
#endif
#ifdef HAVE_SIS3801
  if(gbl_alpha)
    {
      cm_msg(MERROR,"begin_of_run", "alpha mode (2h) is not supported using SIS3801 scaler (buffer too small)" );
      stop_run();
      return FE_ERR_HW;
    }
#endif

  //  if(dd[20])
  printf("\n**** begin_of_run: gbl_alpha=%d and dd[20]=%d\n",gbl_alpha,dd[20]);

  if(gbl_sr)
    {   /* Set initial sample/reference mode if enabled (Type 2 only) */
      printf("begin_of_run:  gbl_sr=%d and daq_drives_sr=%d\n",gbl_sr,fs.hardware.daq_drives_sampleref);
      cm_msg(MINFO,"begin_of_run","Running in sample/reference mode");  /* Sarah's sample/ref mode with microwaves no RF */
      if(fs.hardware.daq_drives_sampleref)
	{
	  gbl_set_sample=0;
	  sample_set(gbl_set_sample);  // set 0  (reference mode)
	  ss_sleep(500); // sleep 500ms
	  sample_read();
	  if(gbl_set_sample != gbl_read_sample)
	    {
	      cm_msg(MERROR,"begin_of_run","Cannot set Reference mode (0) for Sample/Reference Mode; set=%d read=%d\n",
		     gbl_set_sample,gbl_read_sample);
	      printf("begin_of_run: error -  set sample/ref mode to %d, read back %d\n",gbl_set_sample, gbl_read_sample);
	      printf("Sample/Reference gate must be independently driven, and synchronized with DAQ.\n");
	      return  (FE_ERR_HW);
	    }
	  else
	    printf("begin_of_run: set Reference mode (0) for Sample/Reference Mode \n");
	}
      else
	{
	  printf("begin_of_run: fs.hardware.daq_drives_sampleref is false\n");
	  cm_msg(MINFO,"begin_of_run","Sample/Reference gate must be independently driven, and synchronized with DAQ");
	}
    } // end s/r mode
  else
    cm_msg(MINFO,"begin_of_run","Sample/reference mode is not active");  /* Sarah's sample/ref mode with microwaves no RF */


  if(fs.hardware.enable_dual_channel_mode)
    {
      sprintf(latched,"latched "); // for helicity readback messages
      // not flipping helicity in dual channel mode is supported
    }
  else
    cm_msg(MINFO,"begin_of_run","starting run %d in SINGLE CHANNEL MODE \n",run_number);
   
 
#ifdef EPICS_ACCESS
/* Helicity switch position is checked in rf_config if not disabled in odb */

  gbl_laser_live = FALSE;
#ifdef LASER
  /* Open a channel to the laser power unless disabled */
  if(fs.hardware.disable_laser_thr_check)
    {
      printf("begin_of_run: laser power threshold is disabled in the odb \n");
      epicstime=0;
    }
  else
    { /* laser power threshold check is enabled */
      /*     Reconnect to Epics Laser Power (channel access) */
      printf("begin_of_run: opening direct channel access for Laser Power read\n");
      set_long_watchdog(gbl_long_watchdog_time);
      Rchid_lp = -1;
      status = LaserPower_init(&Rchid_lp, Rlaser); /* Opens direct access to Epics 
						      fills Rchid_lp, Rlaser 
						      
						      
						   */ 
      restore_watchdog();
      if (status != SUCCESS) 
	{
	  cm_msg(MERROR,"begin_of_run","failure from LaserPower_init(%d)",status);
	  status = stop_run();
	  /* Epics device cannot be initialized  or the parameters were invalid ....
	     mdarc will stop the run since client flag will  not be set to success at end of BOR */
	  return  (FE_ERR_HW);
     
	} 
      /* now read the Laser Power value */ 
      set_long_watchdog(gbl_medium_watchdog_time); /* 2 min */ 
      status = read_Epics_chan(Rchid_lp,  &Laser_Power_volts);
      restore_watchdog();
      if(status != SUCCESS) 
	{
 	  cm_msg(MERROR,"begin_of_run","failure reading EPICS LaserPower(%d)",status);
	  return  (FE_ERR_HW);
	}
      
      Laser_last_time=ss_time();
      printf("begin_of_run: read_Epics_chan returns Laser Power as %f Volts\n",Laser_Power_volts);	 gbl_laser_live = TRUE;
      /*
	#ifdef THR_TEST // test for when LaserPower hardware is giving 0Volts 
	if(Laser_Power_volts==0) Laser_Power_volts=glp; // give an initial value 
	printf(" begin_of_run: Laser Power Read value adjusted to 0.3 for testing\n");
	#endif  THR_TEST 
      */
      
    }
  /* end of opening EPICS laser power  */ 
#endif // LASER


  /* write a warning  (checking is now done by rf_config */
  if (!fs.hardware.enable_epics_switch_checks)
    cm_msg(MINFO,"begin_of_run",
	   "WARNING Automatic checks on dual/single channel mode Epics switch positions are DISABLED");
#else /* no EPICS */
  epicstime=0;

  if(flip)
      cm_msg(MINFO,"begin_of_run","No EPICS access. Dummy Helicity flipping only (i.e. helicity not actually flipped)");
  set_init_hel_state(flip,HEL_DOWN); /* initial helicity state is HEL_DOWN */
  
#endif /* EPICS_ACCESS */
  
   
  if(fs.input.num_cycles_per_supercycle <=0)
    fs.input.num_cycles_per_supercycle=1; /* default = 1 */
  /* get the number of scans or cycles needed */
  N_Scans_wanted = fs.hardware.num_scans;  /* only used for Type 1 */
  
  /* clear EPICS and CAMP and other scanning flags */
#ifdef EPICS_ACCESS
  epics_params.NaCell_flag = epics_params.Laser_flag = epics_params.Field_flag = FALSE;
#endif
  camp_flag = epics_flag= gbl_epics_live = random_flag = e2a_flag = rf_flag = FALSE;
  mode10_flag = mode1g_flag= mode1j_flag = mode1f_flag= const1f_flag= alpha1f_flag=FALSE;
  gbl_scan_flag=0;  
  discard_first_bin = 0; // default
#ifdef HAVE_SIS3820
  discard_first_bin = fs.hardware.sis3820.discard_first_bin;
  if(fs.hardware.sis3820.sis_mode > 0)
    { // sis_mode > 0 
      printf("begin_of_run: sis3820 SIS Mode = %d (2=real inputs, LNE from PPG)\n",fs.hardware.sis3820.sis_mode); 
#endif // HAVE_SIS3820

      /* Check that rf_config has run. It makes the frequency file which is
	 needed now if random_flag is true */

      status = check_file_time(); /* check if bytecode.dat has been created recently */
      if(status != CM_SUCCESS)
	{
	  if(fs.input.check_recent_compiled_file) /* check flag */
	    {
	      status = stop_run();
	      return FE_ERR_HW ;   /* don't let the run start if file is not recent */
	    }
	  else            /* send an informational message because check flag is off */
	    cm_msg(MINFO,"begin_of_run",
		   "File bytecode.dat has not been created recently ... continuing as check flag is FALSE");
	  write_client_code(RF_ERR,SET, "frontend");
	 
	}
      else
	rfc_flag=TRUE; /* indicates rf_config has run successfully */
  
      /* The number of bins as calculated by rf_config 
	 (n_his_bins needed for e2e & e2a pulse pairs compaction modes ; initialize it to n_bins)

	 For e2e (and other pulsepair modes not yet fixed) rf_config adds an extra bin if discard is true (SIS3820 only)
         Those modes don't discard a bin in the frontend.
      */
      n_requested_bins =  fs.output.num_dwell_times ;   // includes extra bin for SIS3820 if discard_first_bin is set
      n_bins= n_his_bins = n_requested_bins ; // default

#ifdef HAVE_SIS3820
      if(discard_first_bin && pulsepair_flag)
	{   // only supported in the frontend for Type 20 and Type 1
	    {
	      printf("begin_of_run: discard_first_bin not supported in frontend for pulsepair mode %s\n",fs.input.experiment_name );
	      discard_first_bin=0;
	    }
	}

      if(discard_first_bin)
	  n_his_bins = n_requested_bins -1; 
#endif

      gbl_nprebins = n_bins; /* needed in scaler routine. Initialize to default modes
                                i.e. modes where 1 scaler bin equivalent to 1 histo bin */

     


#ifdef HAVE_SIS3820
    } // end of real mode
  else
    { // TEST mode only  sis_mode = 0 no PPG needed
      sis3820_test_flag=1;
      printf("begin_of_run: sis3820 Test Mode (no ppg) sismode = %d\n",fs.hardware.sis3820.sis_mode); 
      n_requested_bins =  fs.sis_test_mode.sis3820.num_bins_requested;
      n_bins =  n_his_bins =  n_requested_bins; 
      printf(" number of bins requested = %d\n",n_requested_bins); // requested_bins
      gbl_nprebins = n_bins; /* needed in scaler routine. */
      cm_msg(MINFO,"begin_of_run","Running in SIS3820 Test mode with No PPG. Requested number of bins is %d",n_bins);
    }


  // Write n_bins to output/LNE_preset 
  fs.output.sis3820.lne_preset = n_bins; // actual number of bins needed 
  sprintf(str,"output/sis3820/LNE preset");
  status = db_set_value(hDB, hFS, str, &fs.output.sis3820.lne_preset, sizeof(fs.output.sis3820.lne_preset), 1, TID_DWORD);  
  if(status !=SUCCESS)
      cm_msg(MERROR,"begin_of_run","cannot set ODB key  ...%s to %u (%d)",str,n_bins,status); 
#endif // SIS3820

  printf(" ***  begin_of_run: n_requested_bins = %d, gbl_nprebins = %d n_bins=%d  n_his_bins= %d  discard_first_bin=%d \n",
	 n_requested_bins, gbl_nprebins,n_bins, n_his_bins, discard_first_bin);

 
  /* Here is included all the settings for Imusr type scans */
  if(exp_mode == 1)
    {
      pulsepair_flag=FALSE;
      /* Set mode flags */
      if (  strncmp(fs.input.experiment_name,"10",2) == 0)
	mode10_flag = TRUE;  /* "Dummy mode" no frequency scan */
      else if (  strncmp(fs.input.experiment_name,"1g",2) == 0)
	{
	  mode1g_flag = TRUE;  /* Combination of SLR (20) and 1f */
	  rf_flag =  TRUE;  /* frequency scan needs RF */
	}

     
      else if ( ( strncmp (fs.input.experiment_name,"1a",2)== 0) ||
		( strncmp (fs.input.experiment_name,"1b",2)== 0))
	{ /* Type 1a or 1b 
	     these are the modes that use the PSM  */
	  rf_flag = TRUE;
	  if (fs.input.randomize_freq_values)
	    random_flag = TRUE; /* randomize the frequency values 1a,1b only */
	}
      else if (  strncmp(fs.input.experiment_name,"1f",2) == 0)
	{
          mode1f_flag=TRUE;
	  alpha1f_flag = fs.input.e1f_enable_alpha_histos;
	  rf_flag = TRUE;  /* frequency scan uses PSM */
	  if (fs.input.randomize_freq_values)
	    random_flag = TRUE; /* randomize the frequency values 1f */
	  if(fs.input.e1f_const_time_between_cycles)
	    {
	      const1f_flag=TRUE;
	      gbl_lne_poll_time = POLL_LNE_FAST; // poll every 10ms
	      fs.input.num_cycles_per_supercycle =  1; // not supported for > 1 (rf_config should have set it to 1)
	      for(i=0; i<cb_time_size; i++)
		cb_time[i]=0;	
	      //dd[11]=1; // silent mode 
	    }
	}
      else if ( ( strncmp(fs.input.experiment_name,"1c",2) == 0 )
		|| ( strncmp(fs.input.experiment_name,"1j",2) == 0 ) )
	{ 
	  if (  strncmp(fs.input.experiment_name,"1j",2) == 0)
	    {
	      mode1j_flag = TRUE;  /* Combination of SLR (20) and 1c ; RF is always OFF */
#ifdef CAMP_ACCESS	      
	      if(dc)printf("begin_of_run: detected mode 1j ; mode1j_flag is set to %d\n",mode1j_flag);
#endif
	    }
#ifdef CAMP_ACCESS
	  
	  printf("begin_of_run: camp scan mode is detected (ppg mode %s)\n",fs.input.experiment_name);
	  camp_flag = TRUE; /* camp scan mode */
	  CAMP_repeat_flag = FALSE;
#else
	  printf("begin_of_run: CAMP scan is not supported in this version\n");
	  cm_msg(MERROR,"begin_of_run","CAMP scan is not supported in this version of frontend code");
	  return(FE_ERR_HW);
#endif
	}
      else if (  strncmp(fs.input.experiment_name,"1n",2) == 0  ||  
		 strncmp(fs.input.experiment_name,"1d",2) == 0  ||
		 strncmp(fs.input.experiment_name,"1e",2) == 0  )
	{    /* EPICS device */
#ifdef EPICS_ACCESS
	  printf("begin_of_run: EPICS scan detected\n");
	  epics_flag = TRUE;
#else
	  cm_msg(MERROR,"begin_of_run","EPICS scan is not available");
	  return (FE_ERR_HW);
#endif
	}
    

      /* Set scan parameters for Type 1 experiments */
      if( mode10_flag)
	{
	  status = mode10_set_scan_params(&ninc);
	  if(status != SUCCESS) return status;
	}
      
#ifdef EPICS_ACCESS
      else if(epics_flag)
	{
	  printf("begin_of_run: calling epics_set_scan_params\n");
	  write_client_code(EPICS_ERR,SET,"frontend"); /* in case of failure */
	  status = epics_set_scan_params(&ninc);
	  if(status != SUCCESS)
	    {
	      cm_msg(MERROR,"begin_of_run","Failure from epics scan parameters. Run stopped");
	      status=stop_run();
	      
	    
	      return  FE_ERR_HW;
	    }
	  write_client_code(EPICS_ERR,CLEAR,"frontend"); /* clear */
	}
#endif
      
#ifdef CAMP_ACCESS
      else if (camp_flag)   
	{
	  write_client_code(CAMP_ERR,SET,"frontend"); /* in case of failure */
	  status = camp_set_scan_params(&ninc);
	  if(status != SUCCESS) 
	    {
	     
	      cm_msg(MERROR,"begin_of_run","Failure setting CAMP Scan Parameters. Stopping Run");
	      status = stop_run();
	      return  FE_ERR_HW;
	    }
	   write_client_code(CAMP_ERR,CLEAR,"frontend"); /* clear */
	}
#endif  /* ifdef CAMP_ACCESS */


      else if (random_flag) /* 1a/1b/1f random freq */
	{
	  status = random_set_scan_params(&ninc);
	  if(status != SUCCESS) return status;
	}
      
      else  /* all other frequency scans */
	{
	  status = RF_set_scan_params(&ninc);
	  if(status != SUCCESS) return status;
	}
      printf("begin_of_run: set_scan_params returns ninc=%d; gbl_scan_flag=0x%x\n",ninc,gbl_scan_flag);
      
      /* setup Type 1 specific histograms, scalers, related variables */
      
      fill_uarray = TRUE;
      n_histo_a = N1_HISTO_MAXA;  
      n_scaler_cum = N1_SCALER_CUM;
      n_scaler_total =  N1_SCALER_TOTAL; // default
      n_histo_b = N1_HISTO_MAXB; // default
      n_scaler_real = N_SCALER_REAL; // default 

#ifndef TWO_SCALERS  // extra alpha inputs only supported for BNQR (ONE SCALER)
      if(alpha1f_flag)
	{
	  n_histo_b = N1_HISTO_MAXB; //  For BNQR,(ONE Scaler) these include ALPHA histos
	  n_scaler_total =  N1_SCALER_TOTAL; // includes ALPHA scalers
	  n_scaler_real =  N_SCALER_REAL; // default - includes ALPHA scalers
	}
      else
	{ // all other Type 1 modes and 1f without alpha
	  // reduce number of histos and scalers 
	  n_histo_b =  N1_HISTO_MAXB_ORIG;
	  n_scaler_real = N_SCALER_REAL - NUM_ALPHA; // less REAL scalers
	  n_scaler_total = N1_SCALER_TOTAL - NUM_ALPHA; // not using alpha counters
	
	}
#endif // TWO_SCALERS
    } /* end of Type 1 */
  else
    { /* Type 2 */
      /* Set mode flags */
      printf("********* TYPE 2 calling assign_2_params\n");
      if(assign_2_params() != SUCCESS) /* does nothing if not 2a/2e/2f */
	return status;     

      /* Type 2 specific histograms, scalers, related variables */
      fill_uarray = FALSE;
      n_histo_a = N2_HISTO_MAXA;
      n_histo_b = N2_HISTO_MAXB ; // For BNMR, N2_HISTO_MAXB_ORIG i.e. 10
                                  // For BNQR, N2_HISTO_MAXB_ORIG +  N2_HISTO_SR i.e. 18   
      n_scaler_total = N2_SCALER_TOTAL; // set defaults
      n_scaler_cum = N2_SCALER_CUM;
      n_scaler_real =  N_SCALER_REAL; // default

#ifndef TWO_SCALERS  // gbl_sr or gbl_alpha only supported for BNQR (ONE SCALER)
      if(gbl_sr)
	printf("begin_of_run: gbl_sr enabled,  n_histo_b = %d\n",n_histo_b); // 18
      else if(gbl_alpha)
	{  // Mode 2h
	  // recalculate the number of histograms and scalers (Type 2)
	  n_histo_b = N2_HISTO_MAXB_ORIG + N2_HISTO_ALPHA ; // should be 16
	  n_scaler_total = N2_SCALER_TOTAL; // includes ALPHA scalers
	  n_scaler_real =  N_SCALER_REAL; //   includes ALPHA scalers
	  printf("begin_of_run: n_histo_b = %d\n",n_histo_b); // should be 16
	}
      else
	{ // All other Type 2 modes
	  // recalculate the number of histograms
	  n_histo_b =  N2_HISTO_MAXB_ORIG; // no extra sample or alpha histos
	  n_scaler_total = N2_SCALER_TOTAL - NUM_ALPHA;
	  n_scaler_real =   N_SCALER_REAL - NUM_ALPHA;
	  printf("begin_of_run: n_histo_b = %d\n",n_histo_b);
	}
    
#endif
    
    } /* end of Type 2 */

#ifdef TWO_SCALERS
  n_histo_total = n_histo_a + n_histo_b;
#else
  n_histo_total = n_histo_b;
#endif

  printf("PPG Mode %s;",ppg_mode);
  if(gbl_alpha || alpha1f_flag)  // Mode 2h or  1f with extra alpha scalers
    printf(" Alpha scaler channels ARE enabled\n");
  else
   printf(" Alpha scaler channels are NOT enabled\n");

  printf("# of real scaler channels in use (n_scaler_real): %d\n",n_scaler_real);
  printf("# of  cum  scaler channels in use (n_scaler_cum): %d\n",n_scaler_cum);
  printf("# of scaler channels in use (n_scaler_total): %d\n",n_scaler_total);
  printf("\n");
  printf("# of histograms scaler A (n_histo_a) : %d\n",n_histo_a);
  printf("# of histograms scaler B (n_histo_b) : %d\n",n_histo_b);
  printf("# of histograms (n_histo_total) : %d\n",n_histo_total);


  /* Check number of frontend histograms is correctly listed in odb; 
     these only change when more histograms are hardcoded */

  INT num_fe_histos;
  char strng[80];
 
  if (exp_mode == 1)
    {   /* type 1 */
      if(alpha1f_flag)
	{
	  num_fe_histos= fs.input.num_type1_fe_histos_alpha_mode;
	  sprintf(strng, "input/num type1 fe histos alpha mode");
	}
      else
	{
	  num_fe_histos=fs.input.num_type1_frontend_histograms ;
	  sprintf(strng, "input/num type1 frontend histograms");
	}

      if( num_fe_histos != n_histo_total)
	{

	  /* write the number of frontend histograms to odb (for rf_config to update mdarc area) */
	  // printf("begin_of_run: Number of frontend histos for type 1 has changed to : %d; previous odb value was %d\n",
	  //	 n_histo_total,fs.input.num_type1_frontend_histograms);
	  cm_msg(MINFO, "begin_of_run", "Number of frontend histos for type 1 has changed to : %d; previous odb value was %d",
                 n_histo_total, num_fe_histos);
	 
	  num_fe_histos = n_histo_total;
	  size=sizeof(num_fe_histos) ;
	  status=db_set_value(hDB, hFS, strng, &num_fe_histos, size, 1, TID_INT);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR, "begin_of_run", "could not write # type1 frontend histos to odb (%d)",status);
	      return DB_NO_ACCESS;
	    }
	}
    } // end type 1
  else
    {  /* type 2 */

      if(gbl_sr)
	{
	  num_fe_histos= fs.input.num_type2_fe_histos_sr_mode;
	  sprintf(strng, "input/num type2 fe histos SR mode");
	}
      else if (gbl_alpha)
	{ // 2h
	  num_fe_histos= fs.input.num_type2_fe_histos_alpha_mode;
	  sprintf(strng, "input/num type2 fe histos alpha mode");
	}
      else
        {
          num_fe_histos=fs.input.num_type2_frontend_histograms;
          sprintf(strng,"input/num type2 frontend histograms");
        }
      
      if( num_fe_histos != n_histo_total)
        {
          /* write the number of frontend histograms to odb (for rf_config to update mdarc area) */
          // printf("Number of frontend histos for type 2 has changed to : %d; previous odb value was %d\n",
          //     n_histo_total, num_fe_histos);
          cm_msg(MINFO, "begin_of_run", "Number of frontend histos for type 2 has changed to : %d; previous odb value was %d",
                 n_histo_total, num_fe_histos);
          num_fe_histos = n_histo_total;
	  size=sizeof(num_fe_histos) ;
          status=db_set_value(hDB, hFS, strng, &num_fe_histos, size, 1, TID_INT);
          
          if (status != DB_SUCCESS)
            {
              cm_msg(MERROR, "begin_of_run", "could not write # type2 frontend histos to odb (%d)",status);
              return DB_NO_ACCESS;
            }
        }
      
    }  
 
  
  /* Initialize scaler structure  */
#ifdef TWO_SCALERS
  /* Scaler A must have MAX_CHAN_SIS38xxA channels enabled plus at least one channel from scaler B */
  if(n_scaler_real <= MAX_CHAN_SIS38xxA)
    {
      cm_msg(MERROR,
	     "begin_of_run", "Total no. of real scalers (%d) must be at least  %d",
	     n_scaler_real,(MAX_CHAN_SIS38xxA+1));
      return FE_ERR_HW; 
    }
#endif
  if (n_scaler_total < n_scaler_real )
    {  // total number of scalers = n_scaler_real + cumulative + cycle
      cm_msg(MERROR,
	     "begin_of_run", "Total no. of scalers (%d) too small for no. real scalers (%d)",
	     n_scaler_total,n_scaler_real); 
      return FE_ERR_HW; 
    }
  printf("n_scaler_real = %d, n_scaler_total = %d\n",n_scaler_real, n_scaler_total);
  
  
  
  /*  Stopping after num_scans or num_cycles is implemented June 2003 */
  
  /* the number of scans or cycles needed has been assigned above 
    N_Scans_wanted = fs.hardware.num_scans;   only used for Type 1 */
  
  /* We are using fs.hardware.num_scans as the input parameter (edit on start) for Type 1 and
     fs.hardware.num_cycles  for Type 2.
     When changing mode, fs.hardware.num_cycles will be set to 0 to avoid any confusion (by do_link.pl)
     
     For type 1,  fs.hardware.num_cycles will be calculated, and written to odb
  */
  
  if(exp_mode == 1)
    {

      printf("Number of increments (ninc) = %d\n",ninc);
      
      /* Type 1:  write number of increments to odb */
      fs.output.e1_number_of_incr__ninc_=ninc;
      size=sizeof( fs.output.e1_number_of_incr__ninc_);
      status=db_set_value(hDB, hFS, "output/e1 number of incr (ninc)", &fs.output.e1_number_of_incr__ninc_, 
			  size, 1, TID_DWORD);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "begin_of_run", "could not write # of increments (ninc) to odb (%d)",status);
	  return DB_NO_ACCESS;
	}
            

      /* Calculate number of cycles making up one scan */

      fs.output.e1_number_of_cycles_per_scan = ninc;
      /* not necessary as one complete scan is scan up (or down)  flipping helicity each time
	 if( (mode1g_flag && flip)|| (mode1j_flag && flip))
	 fs.hardware.num_cycles *=2;  one cycle at each helicity */
      
      if(fs.input.num_cycles_per_supercycle > 1) /* number of cycles repeated at each frequency */
	fs.output.e1_number_of_cycles_per_scan *= fs.input.num_cycles_per_supercycle;
      
      
      if(flip)fs.output.e1_number_of_cycles_per_scan *= 2.0 ; /* helicity flipping; one complete cycle; scan up then down
						 if modes 1g or 1j, one cycle at each helicity, scan up only */


      printf("Number of cycles per scan=%d\n",fs.output.e1_number_of_cycles_per_scan);

      /* write this for custom webpage to display */
      size=sizeof(fs.output.e1_number_of_cycles_per_scan);
      status=db_set_value(hDB, hFS, "output/e1 number of cycles per scan", &fs.output.e1_number_of_cycles_per_scan, 
			  size, 1, TID_DWORD);
      if(status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "begin_of_run", "could not write number of cycles per scan to odb (%d)",status);
	  return status;
	}
      else
	printf("Wrote Number of cycles per scan=%d to odb \n",fs.output.e1_number_of_cycles_per_scan);



      if(N_Scans_wanted > 0) /* if N_Scans_wanted > 0, run will be stopped after this number of scans 
				->  calculate number of cycles after which run should be stopped */
	{
	  fs.hardware.num_cycles = fs.output.e1_number_of_cycles_per_scan * N_Scans_wanted;     
	  
	
	
	  printf("begin_of_run: run will be stopped after %d complete scans (%d cycles) are complete\n",
		 N_Scans_wanted, fs.hardware.num_cycles);
	  cm_msg(MINFO,"begin_of_run","run will be stopped after %d complete scans (%d cycles) are completed",
		 N_Scans_wanted, fs.hardware.num_cycles);	    
	  
	}
      else  /* free-running */
	fs.hardware.num_cycles=0; /* free-running; make sure this is zero */

      
      status = db_set_value(hDB, hFS, "hardware/num cycles", &fs.hardware.num_cycles, 4, 1, TID_DWORD);
      if(status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "begin_of_run", "could not write num cycles to odb (%d)",status);
	  return status;
	}


    }  /* end of Type 1 */

  else
    {   /* type 2 */
      if( fs.hardware.num_cycles > 0 )
	{
      
	  printf("begin_of_run: mdarc will stop run after %d cycles are complete\n",fs.hardware.num_cycles);
	  cm_msg(MINFO,"begin_of_run","Mdarc will stop the run after %d cycles are completed",
		 fs.hardware.num_cycles);	    
	  
	}
    } /* end of type 2 */
  
  setup_hotlinks();/* set up hot links */

  
  /* set up gbl_flip_mode flag for dual channel mode */
  if(fs.hardware.enable_dual_channel_mode)
    gbl_flip_mode =  (exp_mode == 2 || mode1g_flag || mode1j_flag);
  
  if(const1f_flag)
    { // set to zero after setup_hotlinks 
      cm_msg(MINFO,"begin_of_run", "num extra cycles skipped & repeat scan when out-of-tol are not used in this mode");
      fs.hardware.skip_ncycles_out_of_tol=0; // cycle repeat is handled using step-back method
      fs.hardware.out_of_tol_repeat_scan=0; // this also is not supported for const1f_flag
      printf("begin_of_run: const1f_flag true, fs.hardware.skip_ncycles_out_of_tol = %d\n", fs.hardware.skip_ncycles_out_of_tol);
    }

  /* ----------   Load PPG ------------ 
     already checked that rf_config has run recently 
  */

#ifdef HAVE_PPG
  if (sis3820_test_flag) 
    printf("begin_of_run: SIS3820 test mode with NO ppg; NOT loading PPG\n");
  else
    {
      if(dbyt)
	{  /* load bytecode.try instead  for debugging */
	  
	  printf("\nbegin_of_run: DEBUG Loading bytecode.try instead of bytecode.dat \n");
#ifdef HAVE_NEWPPG
	  sprintf(str,"%s/ppgload/ppgload.try",fs.input.cfg_path);
#else
	  sprintf(str,"%s/ppgload/bytecode.try",fs.input.cfg_path);
#endif
	}
      else
	{
	  /* already checked that rf_config has run recently */
	  strncpy(str, ppg_loadfile, 79);
	}
      printf("\n\n *** begin_of_run: calling ppg_load with file %s\n",str);
      status = ppg_load( str); // handles both PPG types  
    
      if (status != SUCCESS)
	{
	  cm_msg(MERROR,"begin_of_run","Failure from ppg_load: file %s not loaded",str);
	  return FE_ERR_HW;
	}
      cm_msg(MINFO,"begin_of_run","PPG file successfully loaded");
#ifdef HAVE_NEWPPG
      printf("begin_of_run: PPG32 (newppg) cannot control Polarization (Helicity) or Beam\n");
      printf("      therefore can only use for testing \n");
      cm_msg(MINFO,"begin_of_run","WARNING  PPG32 (newppg) cannot control Polarization (Helicity) or Beam");
      cm_msg(MINFO,"begin_of_run","therefore can only use for testing ");
      TPPGInit( gVme,  PPG_BASE); // internal trig,clock set pol mask to zero and PC back to zero. Clear all outputs. 
#else
      VPPGInit(gVme, PPG_BASE); /* loads default mask, VME controls Hel, PPG controls beam, Disables Ext start */
#endif
#else
  printf("begin_of_run: No PPG defined. Cannot load file %s\n",str);
#endif //  HAVE_PPG
    } // SIS3820 sis mode is non-zero
  
  // VPPG puts out spurious pulses when loaded. 
  // Now initialize other hardware modules

#ifdef HAVE_SIS3801
#ifdef TWO_SCALERS    // sets into SIS test or real mode
  printf("Initializing SIS3801 Module A base 0x%x\n", SIS3801_BASE_A);
  init_sis3801(  SIS3801_BASE_A,  fs.hardware.enable_sis_ref_ch1_scaler_a);
#endif
  printf("Initializing SIS3801 Module B base 0x%x\n", SIS3801_BASE_B);
  init_sis3801(  SIS3801_BASE_B,  fs.hardware.enable_sis_ref_ch1_scaler_b);
  if (debug) printf("# of active ch.  : %d\n",N_SCALER_REAL);
  
  if(fs.sis_test_mode.sis3801.test_mode)
    {
      n_requested_bins= n_bins= n_his_bins= gbl_nprebins = fs.sis_test_mode.sis3801.num_bins;
      printf("begin_of_run: sis_test_mode number of bins is selected to be %d \n",n_bins);
    }
#endif  //  HAVE_SIS3801


  /* parameters needed for SIS setup 
     fs.hardware.n_channels : # of requested channels (add 1 for ref )
     fs.hardware.num_bins     : # of time bins
     fs.hardware.dwell_time__ms_ : dwell time in msec
  */
  
  /* check number of bins is valid for outputting histos */
  
  // if (n_his_bins >= N_BINS_MAX) /* n_his_bins=n_bins except for pulse_pairs compaction modes  */
       //{
  //cm_msg(MINFO,"begin_of_run", "Number of bins requested is too large %d > %d",n_his_bins, N_BINS_MAX);
  //   return FE_ERR_HW;
  // }
  
  if (dd[1]) 
    printf("begin_of_run: Number of time bins in incoming data: %d; incoming scaler bins: %d; number of histo bins %d\n",
	   n_bins,n_requested_bins, n_his_bins);
    
#ifdef HAVE_SIS3801
    /* Initialization of hardware SIS3801 */



  // if(dd[1])
    printf("Calling sis3801_setup_bor with n_scaler_real=%d\n",n_scaler_real);
  
  status = sis3801_setup_bor(n_scaler_real, 0 );  // also for sis3801D writes n_bins to prescale 
  /* note: for real mode, sis_setup  ignores dwell_time param */ 
  
  if (status != FE_SUCCESS)
    {
      cm_msg(MERROR,"begin_of_run","SIS parameters out of range");
      return status;
    }
#endif // HAVE_SIS3801
  
#ifdef HAVE_SIS3820
  gSisBufferOverflow = 0;  // global in febnmr.h
  status =init_sis3820(TRUE);
  if (status != SUCCESS)
    return status;
   

#endif // HAVE_SIS3820

  /* Set up the frequency scans for all Types */  
  if(exp_mode == 2) 
    {
      if ( strncmp(fs.input.experiment_name,"20",2) == 0 || 
	   strncmp(fs.input.experiment_name,"2d",2) == 0 ||
 	   strncmp(fs.input.experiment_name,"2h",2) == 0) 
	{  /* SLR (e20) or 2d,2h  */  
	  if( init_freq_20() != SUCCESS) /* not freq table driven */
	    return FE_ERR_HW;
	}
      else
	{
	  if(init_freq_2() != SUCCESS) /* freq table driven */
	    return FE_ERR_HW;
	}
       
       
    }   /* end of exp_mode = 2 */
   
  else 
    {  /* exp_mode = 1 */
#ifdef HAVE_PSM
      if( rf_flag) /* Modes 1a,1b,1f which use PSM */
	{	  
	  if(init_freq_1() != SUCCESS)
	    return FE_ERR_HW;
	}
      else
	{
	  printf("begin_of_run: RF not needed; disabling PSM\n");
	  disable_psm(gVme,PSM_BASE);
	}
#endif
    } /* end of exp_mode =1  */

#ifdef HAVE_PSM
  if(dpsm) 
    psmGetStatus(gVme, PSM_BASE,stdout);  
   /* TEMP */
   
   status = write_psmregs(); /* write register values to a file */
   if(status != SUCCESS)
     return status;
#endif /* HAVE_PSM */

   small_bin_flag=0; // flag
   gbl_bin_size=sizeof(DWORD); // default size for storage for scaler,histo bins

 

   j=k=alpha_chunks=0;

   /* check approximate space needed for shipping histos */
   i = n_his_bins * n_histo_total * gbl_bin_size;

 
   // k is approx max histo size that can be shipped
   k = max_event_size/( n_histo_total * gbl_bin_size);
 
   cm_msg(MINFO,"begin_of_run", "approx space for shipping histos: %d bins * %d histos * %d bytes = %d",  
	  n_his_bins, n_histo_total, gbl_bin_size,i);
   printf("begin_of_run: approx space needed for shipping histos: %d bins * %d histos * %d bytes = %d; max_event_size=%d\n",  
	  n_his_bins, n_histo_total, gbl_bin_size,i, max_event_size);
    
 

   if (i >  max_event_size)      
     {

       if(gbl_alpha)
	 { // Mode 2h with large histograms in chunks
	   gbl_bin_size=sizeof(WORD); // scalers,histos
	   small_bin_flag=1; // flag small bins
	   i = n_his_bins * n_histo_total * gbl_bin_size;
	   k = max_event_size/( n_histo_total * gbl_bin_size);
	   i=i/4;  // histos shipped in 4 chunks
	   k=k*4;  // chunks
	   alpha_chunks=1;
	   printf("Setting up histogram chunks for gbl_alpha\n");
	   printf("begin_of_run: 16-bit bins  approx space needed for shipping histos: %d bins * %d histos * %d bytes = %d; max_event_size=%d\n",  
		  n_his_bins, n_histo_total, gbl_bin_size,i, max_event_size);

	 }
     }
   
   if (i >  max_event_size)      
     {
       cm_msg(MERROR,"begin_of_run", "Event size (%d) too large (max=%d); nbins=%d, reduce no. his bins (%d) to < %d",i, max_event_size,n_requested_bins, n_his_bins,k);	
       return FE_ERR_HW;
     }




  /* cleanup previous booking
     realloc memory for scalers */

 
   free_all_scalers();/* Frees all possible scalers
                         Inits all pointers to potential MALLOC calls to NULL */


   printf("\n");

   /* 1. do not malloc any space for software (calculated) scalers 
      2. in gbl_alpha (2h) mode only, do not malloc any space for Fluoride monitors
      3. in gbl_alpha (2h) mode and 1f with alpha inputs, malloc space for Alpha inputs 
      4. in all other modes, do not malloc any space for Alpha inputs 
   */
 
   alpha_scaler_offset=0; // default; malloc scalers starting at 0

   // gbl_alpha (2h) mode skips the first 2 scaler inputs to save space
   if(gbl_alpha)
     { //2h mode 
       alpha_scaler_offset=ALPHA_OFFSET;
       // Treat skipped scalers (RefCh1 and Fl2) like the cumulative scalers (don't allocate bins)
       for (i=0; i<alpha_scaler_offset; i++)
	 {
	   scaler[i].nbins = 0;     /* set bin count to zero */
	   scaler[i].sum_cycle=0.0; /* software scalers clear sums only */
	   // if (dd[1]) 
	     printf("begin_of_run:  Mode 2h with alpha inputs... cleared sum of real scaler %d (no malloc) \n", i); 
	 }
     }
 
   for (i=alpha_scaler_offset; i < n_scaler_total ; i++) 
     { 
       // for the number of real scalers actually being used...
       if (i < n_scaler_real)
	 {	 
	   scaler[i].nbins = n_his_bins;
	   if(small_bin_flag)
	     {
	       scaler[i].qs = malloc(gbl_bin_size * n_his_bins); // scaler[i].qs = malloc(sizeof(WORD) * n_his_bins);
	       //if (dd[1]) 	  
		 printf("malloc real scaler %d SMALL bin size:%d; num_bins:%d (pointer %p)\n", 
			i,gbl_bin_size, n_his_bins, scaler[i].qs);
	     }	   
	   else
	     {
	       scaler[i].ps = malloc(gbl_bin_size * n_his_bins); // scaler[i].ps = malloc(sizeof(DWORD) * n_his_bins);
	       //if (dd[1]) 	  
		 printf("malloc real scaler %d bin size:%d; num_bins:%d (pointer %p)\n", 
			i,gbl_bin_size, n_his_bins, scaler[i].ps);
	     }
	   scaler_clear(i);
	   if (dd[1]) 
	     printf("cleared real scaler %d (%d)\n",i , scaler[i].nbins);
	   
	 }
       else
	 {
	   scaler[i].nbins = 0;     /* set bin count to zero */
	   scaler[i].sum_cycle=0.0; /* software scalers clear sums only */
	   //  if (dd[1]) 
	     printf("cleared sum of software scaler %d (no malloc) \n", i);      
	 }
     }
   


   for (i=alpha_scaler_offset; i<n_scaler_real; i++)
     printf("scaler[%d].nbins=%d\n", i,scaler[i].nbins);



   /* cleanup previous booking
      realloc memory for histograms */
   free_all_histos();/*  // frees all possible histos
                            Init all pointers to potential MALLOC calls to NULL */

   for (i=0 ; i < n_histo_total ; i++)
     { // allocated required histos for this mode; no alpha offset needed as skipped scaler channels are not histogrammed.
       histo[i].nbins = n_his_bins;
       if(small_bin_flag)
	 {
	   histo[i].qh = malloc(gbl_bin_size * n_his_bins);
	   printf("malloc histo[%d] histo[%d].nbins (%p) \n", i, n_his_bins, histo[i].qh );
	 }
       else
	 {
	   histo[i].ph = malloc(gbl_bin_size * n_his_bins);
	   if(dd[1])
	     printf("malloc histo[%d] histo[%d].nbins (%p) \n", i, n_his_bins, histo[i].ph );
	 }
       // printf("bor: calling histo_clear\n");
       histo_clear(i);
       if (dd[1]) printf("cleared histo %d (%d)\n",i , histo[i].nbins);
     }
   

   // if(dd[1])
     {
       printf("begin_of_run:\n");
       printf(" PPG Mode %s\n",ppg_mode);
       printf("# cycle:%d\n",fs.hardware.num_cycles);
       printf("# bins :%d\n",n_bins);
       printf("# sizeof bins :%d\n",gbl_bin_size);
       if(discard_first_bin)
	 printf("# bins requested :%d\n",(n_requested_bins));
       printf("# his bins:%d\n",n_his_bins);
       printf("# histograms: %d\n",n_histo_total);
       printf("# real scalers: %d\n",n_scaler_real);
       printf("alpha scaler offset: %d\n", alpha_scaler_offset);
       printf("thresh1 :%f\n",fs.hardware.cycle_thr1);
       printf("thresh2 :%f\n",fs.hardware.cycle_thr2);
       printf("thresh3 :%f\n",fs.hardware.cycle_thr3); /* BNMR laser Power */
       printf("skip #cyc: %d\n",fs.hardware.skip_ncycles_out_of_tol);
       printf("diag   :%d\n",fs.hardware.diagnostic_channel_num);
     }

#ifdef GONE
     int joffset=0;
     int soffset=0;
     j=0;
     int hindex,sindex;
     int sample_offset=0;
     int hel_offset=0;
     int n_histo_a = 0;
     int type_offset=2;
     int alpha_hel_offset=0;

     for (k=0; k<2; k++)
       {
	 printf("hel_offset=%d\n",hel_offset);
	 
	 joffset=0;
	 
	 
	 for (j=0 ; j < 2 ; j++)
	   printf("fl monitors histo index [%d] scaler index [%d]\n", n_histo_a+j, MAX_CHAN_SIS38xxA+j);
	 
	 joffset+=2;
	 

	 for (j=0 ; j < 2 ; j++) /* next 2 of ScalerB's (Pol monitors) HMLP HMRP OR HMLN HMRN  */
	   {
	     hindex = n_histo_a+joffset+j+hel_offset+sample_offset; // histo index
	     sindex =  MAX_CHAN_SIS38xxA+joffset+j; // scaler index; add ALPHA_OFFSET for 2h gbl_alpha
	     
	     printf("pol histo[%d]   scaler[%d]   \n",hindex,sindex);
	   }
	 
	 joffset+=2;
	 
	 for (j=0 ; j < NUM_NEUTRAL_BEAM ; j++)  /* SCALER B  NBB */
	   {
	     printf("NB histo[%d] scaler[%d]\n", 
		    n_histo_a+4+hel_offset+type_offset+sample_offset, NEUTRAL_BEAM_B -ECL_OFFSET + j );
	     printf("NB histo[%d] scaler[%d]\n", 
		    n_histo_a+5+hel_offset+type_offset+sample_offset, NEUTRAL_BEAM_F -ECL_OFFSET + j );
	   }   
	 

	 hel_offset=2; // next helicity
       }

     printf("\n==== Now gbl_alpha is true   =========\n");
     gbl_alpha = 1;
	   
     printf("gbl_alpha skipping Fl mons\n");
     // valid scaler values start at 2. histos start at 0;
     hel_offset=0;
     for (k=0; k<2; k++)
       {
	 joffset=0;
	 printf("hel_offset=%d\n",hel_offset);
      
	 
	 for (j=0 ; j < 2 ; j++) /* next ScalerB's (Pol monitors) HMLP HMRP OR HMLN HMRN  */
	   {
	     hindex = n_histo_a+joffset+j+hel_offset+sample_offset; // histo index
	     // sindex =  MAX_CHAN_SIS38xxA+j+ALPHA_OFFSET; // scaler index; add ALPHA_OFFSET to start at 2
	     sindex = POL_CHAN1 - ECL_OFFSET +j;
	     printf("alpha pol histo[%d]   scaler[%d]   \n",hindex,sindex);
	   }
	 
	 joffset=2;
	 
	 for (j=0 ; j < NUM_NEUTRAL_BEAM ; j++)  /* SCALER B  NBB */
	   {
	     printf("NB histo[%d] scaler[%d]\n", 
		    n_histo_a+joffset+hel_offset+type_offset+sample_offset, NEUTRAL_BEAM_B -ECL_OFFSET + j );
	     printf("NB histo[%d] scaler[%d]\n", 
		    n_histo_a+joffset+1+hel_offset+type_offset+sample_offset, NEUTRAL_BEAM_F -ECL_OFFSET + j );
	   }   
	 
	 joffset=6;
	 alpha_hel_offset = 2 * hel_offset;
	 for (j=0 ; j < NUM_ALPHA ; j++)  /* SCALER B  ALPHA */
	   {    
	     hindex = n_histo_a+joffset+alpha_hel_offset+type_offset+j;  // was joffset was 6 now 4 (no Fl histos)
	     sindex = ALPHA_CHAN0 - ECL_OFFSET + j ;
	     printf("ALPHA %d histo[%d] scaler [%d]\n",j,hindex,sindex);
	   }
	   
	 hel_offset=2; // next helicity
       }

#endif  
 
     //         printf("Returning temporarily\n");
     //return DB_INVALID_PARAM;
 
#ifdef CAMP_ACCESS
  if(camp_flag)
    { 
      status = camp_get_rec();
      if (status != SUCCESS)
	return(status);
      status = camp_update_params(); /* scan start,stop and incr.  are not included; they are in the input tree */
      if (status != SUCCESS)
	return(status);
      
      if(!rfc_flag)    /* indicates if rf_config has run */
	{
	  printf("begin_of_run: Warning - rf_config has not run recently; full initialize of camp will be done\n");
	  status = init_sweep_device(camp_params); /* full initialize */
	}
      else
	{
	  /* initialize camp - rf_config did full initialize with init_sweep_device */	  
	  /*if(dc) */ 
	  printf("begin_of_run: about to call camp initPath\n");
	  status = camp_initPath(camp_params); /* lesser initiailization */ 
	}
      
      if(dc) printf("begin_of_run: after camp_initPath or init_sweep_device, status=%d\n",status);
      if (status != CAMP_SUCCESS)
	{
	  write_client_code(CAMP_ERR,SET,"frontend");
	  cm_msg(MERROR,"begin_of_run","Run will be stopped. Check CAMP status then retry run start."); 
	  status=stop_run();
	  return  FE_ERR_HW;
	}
      else
	{
	  /*if(dc)*/
	  printf("begin_of_run: successfully initialized camp \n");
	  gotCamp=TRUE;
	}
    }  /* end of camp_flag */
#endif


  // TEMP
  //  printf("Begin_of_run: TEMP waiting 5s...\n");
  //ss_sleep(5000);


  /* now set Beam Control to VME (for continuous beam)  or PPG  (pulsed beam)  */
#ifdef HAVE_PPG
#ifndef HAVE_NEWPPG // does not exist
  if(fs.output.vme_beam_control)
    {
      printf("begin_of_run: Continuous beam: setting beam on with VME control\n");
      VPPGBeamOn(gVme, PPG_BASE); /* set beam on with VME control */
    }
  else
    {
      printf("begin_of_run: Pulsed beam: setting beam control to PPG (PPG script controls beam)\n");
      VPPGBeamCtlPPG(gVme, PPG_BASE); /* PPG script controls the beam */
    }
#endif // not HAVE_NEWPPG
#endif // HAVE_PPG
 if (  strncmp(fs.input.experiment_name,"1f",2) == 0)
   {
       printf("Internal PPG start is selected\n");
   }

 //    printf("begin_of_run: gbl_scan_flag = %d, calling cycle_start\n",gbl_scan_flag);

  /* Start cycle */
  first_time = TRUE;
  status = cycle_start();
  if(status != SUCCESS)
    {
     
      cm_msg(MERROR,"begin_of_run","FAILURE from cycle_start. Stopping run");
      status = stop_run(); // sets   gbl_waiting_for_run_stop
      return FE_ERR_HW;
    }
  
  first_cycle = TRUE;

  if(const1f_flag) // SPEEDUP, BINCHECK only in const1f mode
    {
#ifdef BINCHECK
#ifdef HAVE_SIS3820
      bin1_XXMHz_count = 50000 * fs.output.dwell_time__ms_; //50MHz Ref Ch1 3820
#else
      bin1_XXMHz_count = 25000 * fs.output.dwell_time__ms_; // 25MHz Ref Ch1 3801
#endif
      bad_cntr=good_cntr=0;
#endif
    }

  /* calculate the approximate time one cycle should take */
  max_cycle_time = fs.output.dwell_time__ms_ * (float)  fs.output.num_dwell_times;
  max_cycle_wait = (INT) (((max_cycle_time + fs.input.daq_service_time__ms_) *1.1)/1000 + 0.5); /* add 10%, convert to sec and round up */

  if(dd[1])printf("end of begin_of_run\n");
  return CM_SUCCESS;
}

#include "setup_scan_params.c"

/*-- Start cycle sequence ------------------------------------------*/
INT cycle_start(void)
{
  /*
    - clear cycle sum scalers
    - reset flags for sequencer
    - clear FIFO, enable interrupt on SIS, start SIS
    - generate hardware pulses
  */
  
  INT i,status;
  BOOL new_supercycle;

#ifdef HAVE_SIS3820_OR_SIS3801E
  racq_count = 0;  // remember acquire count
  flush=FALSE; // reset flush
#endif

   if(dd[1]) 
    printf("start of cycle_start;  clearing gbl_BIN_A and gbl_BIN_B\n");  
  /* New cycle => start at bin 0 */
  gbl_BIN_A = 0;
  gbl_BIN_B = 0;

#ifdef HAVE_PSM
  if(dpsm)debug_psm();
#endif

  
  //   printf("cycle_start: skip_cycle=%d  gbl_hel_mismatch_flag=%d\n",skip_cycle,  gbl_hel_mismatch_flag);

  if(skip_cycle && (gbl_hel_mismatch_flag > 0))
    {
      status=recover_helicity();
      if(status != SUCCESS)
	stop_run(); 
      return status;
      printf("cycle_start: after recover_helicity skip_cycle=%d gbl_hel_mismatch_flag=%d hot_threshold_trip=%d \n",
	     skip_cycle,gbl_hel_mismatch_flag, hot_threshold_trip);
    }


  // 1f with constant step 
  if(const1f_flag)
    {
      if(skip_cycle)   // Last cycle failed for some reason
	{ 
	  if(dd[18])
	    printf("cycle_start: const1f_flag and skip_cycle are true\n");
	  if(const1f_stepped_back)
	    {   /* we stepped back in frequency,  but cycle was not successful. 
	           Do not step back again, retry at same frequency  */
	      
	        if(dd[18])
		  printf("cycle_start: Stepped_back flag and skip_cycle are true \n");
	      if(!dd[11])printf("\rcycle_start: repeating cycle at freq %u ",freq_val);
	    }
	  else if(const1f_junk_data)
	    {  //  we stepped back in frequency and cycle was in fact successful. 
	      skip_cycle=0; // clear so we step forward
	      const1f_junk_data=0;  // clear flag
	      if(dd[18])
		printf("cycle_start: step-back cycle at freq_val=%u was successful. Data was discarded. Continuing with scan. \n",freq_val);
	      //printf("\rcycle_start: continuing with scan\n");
	    }
	  else
	    {
	      if(gbl_CYCLE_N > 1)  // don't step back on first cycle. Just repeat it.
		{
		  //printf("cycle_start: calling set_previous_freq_1f()\n");
		  set_previous_freq_1f();  // step back in frequency; will set flag const1f_stepped_back
		}
	    }
	} // skip_cycle was true
    } // end of const1f_flag

  if(!skip_cycle)
    {  /* Last cycle was successful */
      if( gbl_CYCLE_N % fs.input.num_cycles_per_supercycle == 0 )
	{
	  if(dd[8]) 
	    printf("\ncycle_start: start of supercycle\n"); 
	  new_supercycle=TRUE;
	  gbl_SCYCLE_N ++;
	}
      else
	new_supercycle=FALSE; /* do not want to clear histos or step Epics/Camp/freq etc */
      
      /* Note - first time through, gbl_cycle_N=0 */  
      gbl_CYCLE_N++;  /* Count cycle from 1 on */
      if(dd[8])printf("cycle_start: cycle: %d Supercycle: %d exp_mode=%d, new_supercycle=%d\n",
		      gbl_CYCLE_N,gbl_SCYCLE_N,exp_mode, new_supercycle);
      
      if(exp_mode == 1  && new_supercycle)
	{
#ifdef SPEEDUP
	  // SPEEDUP and const1f mode... histos will be cleared in histo_process, before the next scaler data is added	 
	  if(!const1f_flag) // SPEEDUP only in const1f mode at present
	    {
	       if (dd[8])  
		printf("cycle_start: calling   clear_all_histo()\n");
	      clear_all_histo(); /* Type 1  Clear histograms UNLESS we are mid-supercycle 
				    Type 2  Histograms are cumulative (not cleared, acting as Histogram Memory) */
	    }// (const1f_flag) // SPEEDUP only in const1f mode at present
#else // no SPEEDUP
	   if (dd[8])printf("cycle_start: calling   clear_all_histo()\n");
	  clear_all_histo(); /* Type 1  Clear histograms UNLESS we are mid-supercycle 
				Type 2  Histograms are cumulative (not cleared, acting as Histogram Memory) */
#endif // SPEEDUP 
	  
	  
	  if (epics_flag)  // Type 1 scan
	    status = set_next_epics_value();
	  
	  else if(camp_flag) /*  // Type 1 scan camp device */
	    status = set_next_camp_value();
	  
	  else if (random_flag)  /*  // Type 1 scans Modes 1a/1b/1f with RANDOMIZED FREQUENCY STEPS */
	    status = set_next_randomized_freq();
	  
	  else if (mode1g_flag && flip)  /*  // Type 1 scan   Mode 1g  with flip : flip helicity each cycle */
	    status = set_next_1g_flip();
	  
	  else /* all other Type 1 freq scans */	    
	    status = set_next_freq_1();  /*  FREQUENCY SCAN (PSM) any other Type 1 freq modes 
					     (i.e. 10, 1a/1b/1f (not randomized) & 1g without flip   */
	  
	} /* End for Type 1 modes  && new supercycle */
      
      
      else if(exp_mode == 2)  // Type 2 modes are frequency scans
	{
	  set_next_freq_2(); // all Type 2 modes; flips helicity, returns unless 2a,2e,2f and random flag
	} /* end of mode 2 */
      
      
      
      
    }   /* end of NOT skipping cycle  */
  
  else
    {  /* Last cycle was skipped. Data was not saved */
      if(dd[3])printf("\n");
      if(!dd[8] && !dd[11])
	{
	  printf("\r cycle_start: last cycle was skipped ");
	  if(hold_flag)printf("run on HOLD ");
	}
    } /* end of else skipping cycle */

  /* clear REAL sum cycle scalers */
  // for (i=0 ; i < N_SCALER_REAL; i++)
  
  
  for (i=alpha_scaler_offset ; i < n_scaler_real; i++) // alpha_scaler_offset=0 except for 2h mode
    scaler_clear(i);
  if (dd[1])
    printf("cycle_start: called scaler_clear to clear REAL scalers %d to %d\n",
	   alpha_scaler_offset, n_scaler_real);
  
  /* clear software scalers  BUT not CUMULATIVE ones */
  for (i=n_scaler_real ; i < n_scaler_total-n_scaler_cum; i++)
    {
      scaler[i].sum_cycle=0.0; /* software cycle scalers clear sums only */
        if(dd[1])
	printf("cycle_start: cleared software CYCLE scaler %d (sum only)\n",i);
    }
  
  
  /* setup the bin counter */
#ifdef HAVE_SIS3801
  gbl_bin_count = n_bins;  /* count the incoming bins (same as outgoing except pulse pair mode)
			      OLD  SIS3801 with interrupt counts down */
#endif // HAVE_SIS3801 (with interrupt)
  
#ifdef HAVE_SIS3820_OR_SIS3801E
  gbl_bin_count = 0;  /* SIS3820 and new SIS3801E count up */
#endif  

  /* Cycle process sequencer */
  waiteqp[CYCLE] = waiteqp[DIAG] = FALSE;
  waiteqp[FIFO] = TRUE;                /* FIFO acq */
#ifdef SPEEDUP
  if(!const1f_flag) // SPEEDUP only in const1f mode at present; do not change waiteqp[HISTO]
    {
      // waiteqp[HISTO] = FALSE;   Event associated with HISTO is now disabled
      // if ((&equipment[HISTO].info)->enabled)  /* Histo */
	waiteqp[HISTO] = TRUE;
    }    
#else // no SPEEDUP
  //  waiteqp[HISTO] = FALSE;
  // if ((&equipment[HISTO].info)->enabled)  /* Histo */
    waiteqp[HISTO] = TRUE;
#endif      
     if(dd[1])
      printf("cycle_start:  waiteqp[HISTO] =%d and send_histo_event=%d\n",waiteqp[HISTO], send_histo_event);
  if ((&equipment[CYCLE].info)->enabled)  /* cycle */
    waiteqp[CYCLE] = TRUE;
  if ((&equipment[DIAG].info)->enabled)  /* diag */
    {
    waiteqp[DIAG] = TRUE;
    printf("cycle_start:  WARNING set DIAG true ...  \n");
    }
  
  
  /* hotlink rereference */
  if (lhot_rereference)    
    {
      /* Sep 09 Allow rereference on HEL_DOWN or HEL_UP even for Type 2
	 - otherwise cycle can never rereference if stuck with Hel Up
	 and out of tol for Type 2
	 
	 In Apr 03 Rob says that there should not be differences between
	 helup or heldown cycles now when using the total sum of neutral beam
	 monitors. */
      
      hot_rereference = TRUE;
      lhot_rereference = FALSE;
      
      
      /* if(dd[3]) */ printf("\ncycle_start: Rereferenced threshold!!\n");
    } /* end of lhot-rereference */
  
  
  /* clear FIFO */
  if(dsis) printf("\nClearing FIFO\n");
#ifdef HAVE_SIS3801
#ifdef TWO_SCALERS
  sis3801_FIFO_clear(gVme, SIS3801_BASE_A);
#endif
  sis3801_FIFO_clear(gVme, SIS3801_BASE_B);
#endif //  HAVE_SIS3801
  
#ifdef HAVE_SIS3820
#ifdef TWO_SCALERS
  sis3820_RegisterWrite(gVme, SIS3820_BASE_A,SIS3820_KEY_COUNTER_CLEAR,0);
  if(dsis)printf ("Cleared SIS3820 SCALER A \n");
#endif  // TWO_SCALERS
  sis3820_RegisterWrite(gVme, SIS3820_BASE_B,SIS3820_KEY_COUNTER_CLEAR,0);
  if(dsis)printf ("Cleared SIS3820 SCALER B \n");
#endif // HAVE_SIS3820
  
#ifdef SPEEDUP
  if(const1f_flag) // SPEEDUP only in const1f mode at present
    {
      if(skip_cycle)
	{
	  if( waiteqp[HISTO]) 
	    {
	      printf("cycle_start: problem... skipped last cycle and waiteqp[HISTO] (%d) should not have been set !!\n",waiteqp[HISTO]);
	      gbl_prev_skip_cntr++;
	    }
	}
    } // end SPEEDUP only in const1f mode at present
#endif // end SPEEDUP
  
  
  first_time = FALSE;
  skip_cycle = FALSE;
  
  get_readout_time(); // calculate elapsed time for readout 
  
  //printf("cycle_start: calling start_scalers with gbl_CYCLE_N=%d\n",gbl_CYCLE_N);
  /* Now we are ready to restart the hardware */
  start_scalers();

  if(dd[8])
    printf("\nScalers armed\n");

#ifdef HAVE_PSM
  /* Single and dual channel modes: ppg is no longer free-running for Type 2  */
  if(exp_mode == 2) 
    { /* reset Frequency memory pointer for Type 2 */
      status = psmFreqSweepMemAddrReset(gVme, PSM_BASE); /* reset PSM memory pointer */
    } 
#endif /* HAVE_PSM */

#ifdef HAVE_SIS3801E
  // if(dsis)
  {
    gbl_bin_count =  sis3801_read_acquisition_count(gVme, SIS3801_BASE_B); // global
    printf("cycle_start:read gbl_bin_count as %d ; expecting zero\n", gbl_bin_count);
  }
#endif //  HAVE_SIS3801E


#ifdef HAVE_PPG  
  if (! fs.hardware.enable_dual_channel_mode)
    {  // Single Channel Mode
    
      /* For testing... we can start by hand if we set manual_ppg_start=1   */ 
      if(manual_ppg_start)
	printf("\ncycle_start: Waiting for user to start sequencer (set manual_ppg_start=0 for continuous) \n");
      
#ifdef HAVE_NIMIO32	  
      else if(const1f_flag &&  e1f_started_flag)
	{  /* Check whether PPG computer busy is set in e1f_const_time_between_cycles mode */
	  if(!nimio_ReadOneInput(gVme,  NIMIO32_BASE, INPUT5))
	    { // DAQ Busy is NO LONGER SET... need to restart PPG

#ifdef HAVE_SIS3801E
	      gbl_bin_count =  sis3801_read_acquisition_count(gVme, SIS3801_BASE_B); // global
#endif //  HAVE_SIS3801E
#ifdef HAVE_SIS3820
	      gbl_bin_count = sis3820_RegisterRead(gVme,SIS3820_BASE_B, SIS3820_ACQUISITION_COUNT); // global
#endif //  HAVE_SIS3820
	      if(gbl_bin_count != 0)  // cycle shouldn't have started
		printf("cycle_start:read gbl_bin_count as %d ; expecting zero\n", gbl_bin_count);
  
	      gbl_ppg_cb_cntr1++;
	      if(!dd[11])
		{
		  printf("\n\n cycle_start: cycle %d  error... computer busy from PPG should still be set (cntr=%d)! \n", 
			 gbl_CYCLE_N,gbl_ppg_cb_cntr1);
		      
		  printf("cycle_start: stopping the PPG\n");
		}
	      cm_msg(MINFO,"cycle_start","cycle %d  error... computer busy from PPG should still be set (cntr=%d); restarting PPG ", 
		     gbl_CYCLE_N,gbl_ppg_cb_cntr1); 
		  
	      /* Stop the Scaler modules */
	      if(!dd[11])
		printf("... stopping the scaler(s) ");
	      stop_scalers();
		  
	      /* Stop the PPG module */
#ifdef HAVE_NEWPPG
	      TPPGReset(gVme,PPG_BASE); // also stops the program even if in long delay
	      TPPGDisableExtTrig(  gVme, PPG_BASE);
#else
	      VPPGStopSequencer(gVme, PPG_BASE);
	      VPPGDisableExtTrig ( gVme, PPG_BASE );
#endif //  HAVE_NEWPPG
	      e1f_started_flag = 0; // clear the flag which indicates PPG is running
	      // Restart the scalers
	      start_scalers();
 
	      gbl_ppg_restart_cntr++; // count the number of times we have to restart PPG due to PPG computer busy not set
	     
	      if(gbl_CYCLE_N > 1) 
		{
		  if(const1f_stepped_back) // check the flag
		    printf("... freq has already been stepped back\n");
		  else
		    set_previous_freq_1f();  // step back in frequency 
		}
                
	      // Now restart the PPG after test wait 100ms

	      printf("Restarting PPG (e1f mode with constant time between cycles) after test wait 100ms \n");
	      iwait(100,"test");
#ifdef HAVE_NEWPPG
	      TPPGStartSequencer(gVme, PPG_BASE);
#else
	      VPPGStartSequencer(gVme, PPG_BASE);
#endif // HAVE_NEWPPG
	      e1f_started_flag = 1; // set the flag to say PPG is running
	    } // end DAQ busy not set
	} // end check computer busy
#endif  // HAVE_NIMIO32
	    
      else
	{ // all other modes
	  if(const1f_flag)
	    { // first time start the PPG, then it should run in a loop
	      if(dd[18])
		printf("\ncycle_start: Starting PPG (e1f mode with constant time between cycles)\n");
	      e1f_started_flag=TRUE;
	    }
	  // printf("Starting PPG after 500ms test wait\n");
	  // iwait(500,"test");
#ifdef HAVE_NEWPPG
	  TPPGStartSequencer(gVme, PPG_BASE);
#else
	  VPPGStartSequencer(gVme, PPG_BASE);
#endif // HAVE_NEWPPG 
	} // end of single channel modes
    }
  else
    { // Dual Channel Mode
      if(dd[8])printf("\ncycle_start: Dual Channel Mode, waiting for External Trigger to start PPG Sequencer\n");
    }

#else // DO NOT HAVE_PPG
  printf ("\ncycle_start: NO PPG HARDWARE IS AVAILABLE. Cannot start PPG Sequencer. \n");
  if( sis3820_test_flag ) //  (global)  set for sis3820 sis_mode=0 test mode without ppg
    printf("cycle_start: SIS3820 test mode with NO ppg; NOT starting PPG\n");
#endif // HAVE_PPG 

/* Cycle started */
  gbl_IN_CYCLE = TRUE; 
  // printf("cycle_start: gbl_IN_CYCLE set true %d\n",gbl_IN_CYCLE);
#ifdef HAVE_NIMIO32
  nimio_SetOneOutput(gVme,  NIMIO32_BASE, OUTPUT6 ); // Set OUTPUT6 (gbl_in_cycle)
  nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT7 ); // Clear OUTPUT7 (computer is not busy)
#endif

  /* dd[11]=print nothing, dd[8]=verbose mode */
  // if(dd[8])  printf("\n");
  
  // if(!dd[11])  /* dd[11]->print nothing */
  //  printf("\rCyc %d SCyc %d ",gbl_CYCLE_N,gbl_SCYCLE_N);
  
  if(exp_mode == 2)
    if(!dd[11])printf(", last ub :%d hel %d;",userbit_A,gbl_ppg_hel);
  
  if(!dd[11])printf(" go..");
  
  
  status = CM_SUCCESS;
  return (status);
  
}  /* end of cycle_start routine */
  
  

//======================================================


/*--------------------------------------------------------------*/
INT set_next_epics_value(void)
/*--------------------------------------------------------------*/
{ // called from cycle_start() if epics_flag is set
 
#ifdef EPICS_ACCESS
  BOOL epics_same_value;
  INT status;

  epics_same_value = FALSE;
  /* ---------------------------------------------------------
     Epics scan (NaCell/Laser/Field) 
     ---------------------------------------------------------*/
  printf("gbl_inc_cntr=%d  epics_params.Epics_ninc=%d\n",gbl_inc_cntr, epics_params.Epics_ninc);
  
  if(gbl_inc_cntr ==  epics_params.Epics_ninc)  
    {
      if (!dd[11])printf("set_next_epics_value: new scan, flip=%d     \n",flip);
      /*  N E W  S C A N */		  
      
      /* check if helicity flipping is enabled */
      if(flip) 
	{
	  /* really flip the helicity ONLY when terminating a positive scan  */
	  if(epics_params.Epics_inc > 0)
	      gbl_hel_flipped = flip_helicity(FALSE); /* prints a message */
	  else
	    if(!dd[8] && !dd[11])printf("\n");
	  
	  if(gbl_CYCLE_N !=1)
	    {
	      update_fix_counter(epics_params.Epics_ninc); /* update fix_incr_cntr */
	      epics_same_value = TRUE; /* set a flag to say Epics set value is kept the same
					  (except at begin of run) */
	    }
	  else
	    {
	      if(dd[5])
		printf("set_next_epics_value: detected BOR, gbl_CYCLE_N=%d  flip=%d ,epic_same_value=%d gbl_fix_inc_cntr=%d\n",
		       gbl_CYCLE_N,flip,epics_same_value,gbl_fix_inc_cntr);
	    }
	  
	  epics_params.Epics_inc *= -1.; /* change scan direction */
	} /* end of flip=TRUE */
      else 
	{ /* helicity flipping is not enabled */
	  if(dd[5])printf("set_next_epics_value: setting 1 incr. away from start value (large change) \n");
	  epics_params.Epics_val = epics_params.Epics_start - epics_params.Epics_inc; 
	  /* this may be a large change, so set 1 incr. away from start & allow to 
	     stabilize before stepping... except at Begin run, because this value has
	     already been set */
	  if(gbl_CYCLE_N > 1)
	    {
	      if(dd[5])printf("set_next_epics_value: calling set_epics_val with  epics_parameters.Epics_last_offset=%.2f\n",epics_params.Epics_last_offset);
	      status = set_epics_val();
	      if(status != SUCCESS)
		{
		  write_client_code(EPICS_ERR,CLEAR,"frontend");
		  cm_msg(MERROR,"set_next_epics_value","Error setting EPICS value. Stopping Run");
		  stop_run();
		  return  FE_ERR_HW; 

		}
	    }
	  else
	    {
	      if(dd[8])
		printf("set_next_epics_value:detected BOR .... NOT calling set_epics_val, gbl_CYCLE_N=%d flip is %d, epic_same_value FALSE \n", gbl_CYCLE_N,flip);
	    }
	}
      gbl_inc_cntr = 0;
      gbl_SCAN_N ++;
    } /* E N D  of new scan for Epics */
  
  /* calculate new EPICS set value; calculate from start value to avoid rounding errors */
  epics_params.Epics_val = epics_params.Epics_start + 
    ( (gbl_inc_cntr - gbl_fix_inc_cntr) * epics_params.Epics_inc);
  
  gbl_inc_cntr ++; 
  
  if(!epics_same_value)
    {
      status = set_epics_incr();
      if(status != SUCCESS)   
	{
	  write_client_code(EPICS_ERR,SET,"frontend");
	  cm_msg(MERROR,"set_next_epics_value","Could not increment EPICS value. Stopping Run.");
	  stop_run();

	  return  FE_ERR_HW;  
	}
    }
  else
    if(dd[5])printf("set_next_epics_value: keeping epics value the same, skipping call to set_epics_incr\n");
  
  
#endif /* and EPICS */

  return SUCCESS;
}

/*----------------------------------------------------------------*/
INT set_next_camp_value(void)
/*----------------------------------------------------------------*/
{  
  /* Called from cycle_start() if camp_flag is true */

#ifdef CAMP_ACCESS
  INT status;
  float read_camp_val; /* CAMP value read back */


  /*  --------------------------------------------------------
      Mode 1j CAMP scan with flip  : flip helicity each cycle  
      -------------------------------------------------------- */
  if (mode1j_flag && flip)
    {  /* Mode 1j with helicity flip 
	  flip the helicity after each cycle; 
	  i.e. repeat cycle at the same sweep value for other helicity */
      gbl_hel_flipped =  flip_helicity(FALSE); /* flip the helicity  */   
      if(gbl_ppg_hel==HEL_UP)  /* only increment the sweep value on HEL_UP */
	{
	  if( gbl_FREQ_n ==  camp_ninc )  /* new scan */
	    {
	      if(dc)printf("\nset_next_camp_value: HEL UP 1j New scan detected, setting gbl_FREQ_n=0\n");
	      gbl_FREQ_n = 0;
	      gbl_SCAN_N ++;
	      if(gbl_CYCLE_N !=1)     /* except at beginning of run
					 for up/down scan, increment counter has to be fixed */ 
		{
		  if(dc)
		    printf("set_next_camp_value: calling update_fix_counter with camp_ninc=%d\n",camp_ninc);
		  update_fix_counter(camp_ninc); /* update fix_incr_cntr */
		}
	      else
		{
		  if(dc) 
		    printf("\nset_next_camp_value: HEL UP 1j Detected BOR, gbl_CYCLE_N=%d  flip=%d hel=%d , gbl_fix_inc_cntr=%d\n",
			   gbl_CYCLE_N,flip,gbl_ppg_hel,gbl_fix_inc_cntr);
		}
	      camp_inc *= -1.; /* reverse scan direction */
	    }  /* end of new scan for 1j+flip */
	  else  /* not a new scan */
	    {
	      set_camp_val = camp_start + ((gbl_FREQ_n - gbl_fix_inc_cntr) * camp_inc); /* calculate new value */ 
	      if(dc)		
		printf("set_next_camp_value: HEL UP 1j: incremented set_camp_val to = %f, gbl_FREQ_n=%d, gbl_fix_inc_cntr=%d, camp_inc=%f\n",
		       set_camp_val, gbl_FREQ_n, gbl_fix_inc_cntr, camp_inc );
	    } /* end of if old scan */
	  
	  
	  gbl_FREQ_n ++;  /* increment counter for next time */
	  if(!dd[11])
	  printf("\rCyc %d SCyc %d; hel %d  ; setting camp scan value to %f %s ",
		 gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel,set_camp_val, camp_params.units);
	  if(dc)
	    printf("set_next_camp_value: calling set_camp_value to set CAMP value to %f\n",set_camp_val);
	  status=set_camp_value(set_camp_val); /* set camp value */
	  if(status != CAMP_SUCCESS)
	    {
	      cm_msg(MERROR,"set_next_camp_value","Error attempting to set CAMP value of %f %s; stopping run (%d)",
		     set_camp_val,camp_params.units,status);
	      printf("\nset_next_camp_value: Error from CAMP set_camp_value (%d); disconnecting from CAMP \n",status);
	      write_client_alarm_message("Cannot set CAMP step value");
	      goto err;
	    }		
	} /* increment sweep value only on HEL_UP */
      else
	{ /* HEL DOWN... don't do anything
	     don't increment gbl_FREQ_n or sweep value, don't pass Go, don't collect $200 */
	  if(dc)printf("\n set_next_camp_value: HEL DOWN ... 1j continuing; gbl_FREQ_n=%d\n",gbl_FREQ_n);
	  if(!dd[11]) printf("\rCyc %d SCyc %d; hel %d ; camp scan value remains at %f %s ",
		 gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel, set_camp_val,camp_params.units);
	}
    }  /* end of Mode 1j + flip */
  else
    { /* regular CAMP scan including mode 1j without flip */
      /*   ---------------------------------------------------
	   CAMP SCAN
	   ---------------------------------------------------- */
      if (dc)printf("set_next_camp_value: detected camp scan\n");
      /* determine if this is a new scan */
      if(gbl_FREQ_n ==  camp_ninc) 
	{ /* N E W  S C A N   */		  		 
	  gbl_FREQ_n = 0;		 
	  gbl_SCAN_N ++;
	  if(flip) /* check if helicity flipping is enabled */
	    {
	      /* really flip the helicity ONLY when terminating a positive scan */
	      if(camp_inc > 0)
		  gbl_hel_flipped =  flip_helicity(FALSE);
		
	      if(!dd[8])printf("\n");
	      
	      if(gbl_CYCLE_N !=1) /* except at Begin of Run,
				     for up/down scan, increment counter has to be fixed */
		update_fix_counter(camp_ninc); /* update fix_incr_cntr */
	      else
		{
		  if(dc) 
		    printf("set_next_camp_value: detected BOR, gbl_CYCLE_N=%d  flip=%d gbl_fix_inc_cntr=%d\n",
			   gbl_CYCLE_N,flip,gbl_fix_inc_cntr);
		}
	      
	      camp_inc *= -1.;  /* reverse scan direction */
	      if(dc)
		printf("set_next_camp_value: set_next_camp_value and flip are true,  set_camp_val=%f, reversed camp_inc to %f, gbl_fix_inc_cntr=%d\n",
		       set_camp_val,camp_inc,gbl_fix_inc_cntr);
	    } /* end of flip */
	  else 
	    { /* flip is false */
	      set_camp_val = camp_start;  /* start a new scan */   
	      if(dc)printf("set_next_camp_value: set_next_camp_value is true, flip is false  set_camp_val=%f\n",set_camp_val);
	    }
	}	  /* end of new scan for CAMP */
      else    /* not a new CAMP scan */
	{
	  /* increment scan value;  calculate from start value to avoid rounding errors */
	  set_camp_val = camp_start + ( (gbl_FREQ_n - gbl_fix_inc_cntr) * camp_inc);
	}
      gbl_FREQ_n ++;
      
      /* Set CAMP value  */ 
      printf("\r set_next_camp_value %d:    hel %d;  Setting camp scan value to %f %s",
	     gbl_CYCLE_N,gbl_ppg_hel,set_camp_val, camp_params.units);
      if(dc)
	printf("set_next_camp_value: calling set_camp_value to set CAMP value to %f\n",set_camp_val);
      status = set_camp_value(set_camp_val);
      
      if(status != CAMP_SUCCESS)
	{
	  cm_msg(MERROR,"set_next_camp_value","Error attempting to set CAMP value of %f %s; stopping run  (%d)",
		 set_camp_val,camp_params.units,status);
	  printf("\nset_next_camp_value: Error from CAMP set_camp_value (%d); disconnecting from CAMP and stopping run\n",status);
	  write_client_alarm_message("Cannot set CAMP step value");
	  goto err;
	}
      
    } /* end of 1j/regular CAMP scan 
	 all camp scans continue here  */
  
  
  cyinfo.campdev_set = set_camp_val;
   
  /* now read camp back to fill cyinfo.campdev_read */
  if(dc) printf("set_next_camp_value: calling read_sweep_device to read CAMP value \n");
  read_camp_val=-999; /* initialize value to something */
  status = read_sweep_dev(&read_camp_val, camp_params);
  if(status != CAMP_SUCCESS)
    { /* read_sweep_dev sets client flag to stop the run */
      cm_msg(MERROR,"set_next_camp_value",
	     "Error from CAMP read_sweep_device (%d) stopping run ",status);
      printf("\nset_next_camp_value: Error from CAMP read_sweep_device  (%d) \n",
	     status);
      write_client_alarm_message("Cannot read CAMP value");
      goto err;
    }
  else
    {
      if(dc)printf("set_next_camp_value: Read back value from CAMP device = %f \n",read_camp_val);
      cyinfo.campdev_read = read_camp_val;
    }
  return SUCCESS;

 err:
      write_client_code(CAMP_ERR,SET,"frontend");
      stop_run();
      return  FE_ERR_HW ; /* error return.  */

#endif /* CAMP */
  return SUCCESS;
}

/*------------------------------------------------------------------*/
INT set_next_randomized_freq(void)
/*------------------------------------------------------------------*/
{
  /* called from cycle_start() if random_flag is set ( Type 1 ) */
  INT status;


  /* ---------------------------------------------------------------
     FREQUENCY SCAN (PSM) 1a/1b/1f with RANDOMIZED FREQUENCY STEPS
     -------------------------------------------------------------- */
  
  { /* 1a/1b/1f random */
    if( gbl_FREQ_n ==  freq_ninc) 
      { /* N E W  S C A N  FOR RANDOMIZED FREQ SCAN */ 
	
	gbl_FREQ_n = 0;
	gbl_SCAN_N ++;
	
	
	/* check if helicity flipping is enabled */
	if(flip)
	  {
	    /* flip the helicity when terminating any scan (positive or negative) */ 
	    gbl_hel_flipped =  flip_helicity(FALSE);
	    
	  } /* end of flip */
	
	
	if(gbl_CYCLE_N !=1)     /* except at beginning of run */
	  {
	    double elapsed_time;
	    static struct timeval t3, t4;
	    
	    if(dd[10])printf("set_next_randomized_freq:New scan for 1a/1b/1f random... getting a new set of random frequency values\n");
	    
	    /*  see how long it takes */
	    if(dd[13]) 
	      gettimeofday(&t3, NULL); // start timer
	    
	    /* get new random freq step values (already done at BOR) */
	    status = randomize_freq_values(ppg_mode, freq_ninc);
	    
	    if(dd[13])
	      gettimeofday(&t4, NULL);  // stop timer
	    if(status != SUCCESS) /* error message sent by randomize_freq_values */
		return  (status); /* error return */

	    if(dd[13])  // compute and print the elapsed time in millisec
	      {
		elapsed_time = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
		elapsed_time += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
		printf("set_next_randomized_freq: time to randomize was  %f ms\n",elapsed_time);
	      }
	  }
	else
	  {
	    if(dd[10]) 
	      printf("set_next_randomized_freq: detected BOR, gbl_CYCLE_N=%d  flip=%d gbl_fix_inc_cntr=%d\n",
		     gbl_CYCLE_N,flip,gbl_fix_inc_cntr);
	  }

	if( const1f_flag)
	  {
	    const1f_prev_freq_val = freq_val; // remember in case of skip_cycle (repeat previous freq);
	    const1f_prev_FREQ_n=gbl_FREQ_n;
	  }


	/* let's do a sanity check */
	if(prandom_freq != NULL)
	  freq_val = (DWORD)prandom_freq[0]; /* set to first value */
	else
	  {
	    cm_msg(MERROR,"set_next_randomized_freq","Error... null pointer prandom_freq. Programming Error");
	    stop_run();
	    
	    return  DB_NO_MEMORY; /* error return. Client_flag will be set */
	  }
      } /* end of new scan */
    
    else  /* not a new scan */
      {
	/*  do a sanity check */
	if(prandom_freq != NULL &&  gbl_FREQ_n > 0 && gbl_FREQ_n < freq_ninc)
	  freq_val = (DWORD)prandom_freq[gbl_FREQ_n]; /* get new value */
	else
	  {
	    cm_msg(MERROR,"set_next_randomized_freq","Error... null prandom_freq(%p) or invalid gbl_FREQ_n(%d)",
		   prandom_freq,gbl_FREQ_n);
	    
	    return  DB_NO_MEMORY; /* error return.  */
	  }
	if(dd[10]) 		
	  printf("set_next_randomized_freq: freq_val = %u (randomized), gbl_FREQ_n=%d\n",
		 freq_val, gbl_FREQ_n);
      }
    
    gbl_FREQ_n ++;  /* increment counter for next time */
    
    /* Set the Frequency */ 
    if(!dd[11])printf("\rCyc %d SCyc %d;   hel %d ; random freq: %u Hz; ",
	   gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel,freq_val);
    
#ifdef HAVE_PSM
    status=psm_setone(gVme, PSM_BASE, freq_val); /* set PSM freq val  in Hz; does a strobe */
    if(status == -1)
      cm_msg(MERROR,"set_next_randomized_freq","Error from psm_setone. Frequency %d not set",freq_val);
#endif
  }  /* end of 1a/1b/1f randomized scan */
  return SUCCESS;
}



/*---------------------------------------------------------------*/
INT set_next_1g_flip(void)
/*---------------------------------------------------------------*/
{
  /* called from cycle_start()  if (mode1g_flag && flip)
     
     Mode 1g with helicity flip 
     flip the helicity after each cycle; 
     i.e. repeat cycle at the same freq value for other helicity */

  INT status;

  gbl_hel_flipped =  flip_helicity(FALSE); /* flip the helicity  */

  if(gbl_ppg_hel==HEL_UP)  /* only increment the sweep value on HEL_UP */
    {
      if( gbl_FREQ_n ==  freq_ninc )  /* new scan */
	{
	  if(dd[10])printf("\nset_next_1g_flip: HEL UP 1g New scan detected, setting gbl_FREQ_n=0\n");
	  gbl_FREQ_n = 0;
	  gbl_SCAN_N ++;
	  if(gbl_CYCLE_N !=1)     /* except at beginning of run */
	    {
	      if(dd[10])
		printf("set_next_1g_flip: calling update_fix_counter with freq_ninc=%d\n",freq_ninc);
	      update_fix_counter(freq_ninc); /* update fix_incr_cntr */
	    }
	  else
	    {
	      if(dd[10]) 
		printf("\nset_next_1g_flip: HEL UP 1g Detected BOR, gbl_CYCLE_N=%d  flip=%d hel=%d , gbl_fix_inc_cntr=%d\n",
		       gbl_CYCLE_N,flip,gbl_ppg_hel,gbl_fix_inc_cntr);
	    }
	  freq_inc *= -1.; /* reverse scan direction */
	}
      else  /* not a new scan */
	{
	  freq_val = freq_start + ((gbl_FREQ_n - gbl_fix_inc_cntr) * freq_inc); /* calculate new value */ 
	  if(dd[10])		
	    printf("set_next_1g_flip: HEL UP 1g: incremented freq_val to = %u, gbl_FREQ_n=%d, gbl_fix_inc_cntr=%d, freq_inc=%d\n",
		   freq_val, gbl_FREQ_n, gbl_fix_inc_cntr, freq_inc );
	} /* end of if old scan */
      
      
      if(!dd[11])printf("\rCyc %d SCyc %d; hel %d  ; setting 1g freq to %u hz ",
	     gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel,freq_val);
#ifdef HAVE_PSM
      status=psm_setone(gVme, PSM_BASE, freq_val); /* set PSM freq val  in Hz; does a strobe */
      if(status == -1)
	cm_msg(MERROR,"set_next_1g_flip","Error from psm_setone. Frequency %d not set",freq_val);
      
      //  status=set_frequency_value(); /*  OLD set PSM frequency */
#endif
      
      gbl_FREQ_n ++;  /* increment counter for next time */
      
    } /* increment sweep value only on HEL_UP */
  else
    { /* HEL DOWN... don't do anything
	 don't increment gbl_FREQ_n or sweep value, don't pass Go, don't collect $200 */
      if(dd[10])printf("\n set_next_1g_flip: HEL DOWN ... continuing; gbl_FREQ_n=%d\n",gbl_FREQ_n);
      if(!dd[11])printf("\rCyc%d SCyc %d; hel %d ; 1g freq remains at %u hz ",
	     gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel,freq_val);
    }
  return SUCCESS;
}

/*----------------------------------------------------------------------*/
INT set_next_freq_1(void)
/*----------------------------------------------------------------------*/
{
  /*  called from cycle_start()

      FREQUENCY SCAN (PSM) any other Type 1 freq modes 
      (i.e. 10, 1a/1b/1f (not randomized) & 1g without flip  
      ---------------------------------------------------------------------------------------------*/

  INT status;
  // printf("set_next_freq_1: starting with gbl_FREQ_n = %d\n",gbl_FREQ_n);
  if( const1f_flag)
    {
      const1f_prev_freq_val = freq_val; // remember previous in case of skip_cycle (step back to prev freq needed)
      const1f_prev_FREQ_n=gbl_FREQ_n;
      if(dd[18])
	printf("set_next_freq_1: starting with gbl_FREQ_n=%d  freq_val=%d  freq_ninc=%d\n",
	       gbl_FREQ_n, freq_val, freq_ninc);
	//	cm_msg(MINFO,"set_next_freq_1","starting with gbl_FREQ_n=%d  freq_val=%d  freq_ninc=%d",gbl_FREQ_n, freq_val, freq_ninc); //temp
    }
  if( gbl_FREQ_n ==  freq_ninc)
    { /* N E W  S C A N  FOR FREQ */

      if( const1f_flag)
           cm_msg(MINFO,"set_next_freq_1","New scan detected (gbl_FREQ_n = freq_ninc) "); // temp
      gbl_FREQ_n = 0;
      gbl_SCAN_N ++;
      /* check if helicity flipping is enabled */
      if(flip)
	{
	  /*  really flip the helicity ONLY when terminating a positive scan */
	  if(freq_inc > 0)
          {	
	      gbl_hel_flipped =  flip_helicity(FALSE);
              if( const1f_flag) cm_msg(MINFO,"set_next_freq_1","flipping helicity after positive scan"); // temp
          }
	  else
	      if (!dd[11] && !dd[8])printf("\n"); /* dd[11]=print nothing  dd[8]=verbose mode*/
	    	
	  
	  if(gbl_CYCLE_N !=1)     /* except at beginning of run */
	      update_fix_counter(freq_ninc); /* update fix_incr_cntr */
	  else
	    {
	      if(dd[10]) 
		printf("set_next_freq_1: detected BOR, gbl_CYCLE_N=%d  flip=%d , gbl_fix_inc_cntr=%d\n",
		       gbl_CYCLE_N,flip,gbl_fix_inc_cntr);
	    }
	  
	  freq_inc *= -1.; /* reverse scan direction */
	} 
      else   /* flip is false */
	freq_val = freq_start; /* set to start value */	 
      
    } /* end of new scan */
  
  else /* not a new scan */
    {
      freq_val = freq_start + ((gbl_FREQ_n - gbl_fix_inc_cntr) * freq_inc); /* calculate new value */ 
      
      if(dd[10]) 		
	printf("set_next_freq_1: freq_val = %u, gbl_FREQ_n=%d, gbl_fix_inc_cntr=%d, freq_inc=%d,(gbl_FREQ_n - gbl_fix_inc_cntr)=%d\n",
	     freq_val, gbl_FREQ_n, gbl_fix_inc_cntr, freq_inc, 
	     (gbl_FREQ_n - gbl_fix_inc_cntr));	
      if(const1f_flag && dd[18])
	printf("set_next_freq_1:  freq_val = %u, gbl_FREQ_n=%d\n",freq_val, gbl_FREQ_n);
   }
  

  
  gbl_FREQ_n ++;  /* increment counter for next time */
 

  /* Set Frequency */ 
   if(!dd[11])
    printf("\rCyc %d SCyc %d;", gbl_CYCLE_N,gbl_SCYCLE_N );

 
  if (mode10_flag) /*  Dummy scan (SCALER mode ) nothing scanned */
    {
      if(!dd[11]) /* dd[11] print nothing */
	printf(" Mode10: nothing scanned;  hel %d ",gbl_ppg_hel);
    }
  else
    { /* type1 mode but NOT mode10 */
      if(!dd[11])printf("hel %d ; freq %u Hz frqstep=%d ",gbl_ppg_hel,freq_val,gbl_FREQ_n);
#ifdef HAVE_PSM
      status=psm_setone(gVme, PSM_BASE, freq_val); /* set PSM freq val  in Hz; does a strobe */
      if(status == -1)
	{
	  cm_msg(MERROR,"set_next_freq_1","Error from psm_setone. Frequency %d not set",freq_val);
	  return  FE_ERR_HW ; /* error return.  */
	}
#endif /* HAVE_PSM */
      
    }   /* end of NOT mode 10 */
  return SUCCESS;
} 


/*----------------------------------------------------------------------*/
INT set_next_freq_2(void)
/*----------------------------------------------------------------------*/
{
  /*  called from cycle_start() for all Type 2 modes */

  INT status;
  DWORD first_frequency;

  if(gbl_sr)  
    {
      if(flip)
	{
	  if(gbl_ppg_hel == HEL_UP)
	    {
	      printf("set_next_freq_2: HEL_UP; gbl_set_sample=%d gbl_read_sample=%d\n", gbl_set_sample, gbl_read_sample);
	      sample_flip();  // flip sample/reference mode only when changing from hel_up to hel_down
	      printf("set_next_freq_2: flipped sample/ref at cycle %d; Now  gbl_set_sample=%d gbl_read_sample=%d\n", 
		     gbl_CYCLE_N, gbl_set_sample, gbl_read_sample);
	    }
	}
      else
	{
	  if(gbl_CYCLE_N % 2)
	    {
	      printf("set_next_freq_2: hel flipping disabled;  flipping sample/ref at cycle number: %d\n",gbl_CYCLE_N);
	      sample_flip();
	      printf("set_next_freq_2: flipped sample/ref at cycle %d; Now  gbl_set_sample=%d gbl_read_sample=%d\n", 
		     gbl_CYCLE_N, gbl_set_sample, gbl_read_sample);
	    }
	}
    }
  
  printf("set_next_freq_2: flipping helicity\n");
  if(flip) 
      gbl_hel_flipped = flip_helicity(FALSE);
    
  
  if(random_flag)
    {    
      if(e2a_flag || e2e_flag || e2f_flag)
	{ /*  get new random freq step values and reload freq table */	  
	  if(dd[13])printf("Cycle_start: calling randomize_freq_values for type 2a or 2e or 2f \n");
	  status = randomize_freq_values( ppg_mode, fs.output.num_frequency_steps);
	  if(status != SUCCESS) /* error message sent by randomize_freq_values */
	    {
	      cm_msg(MERROR,"cycle_start","Error randomizing frequency values. Stopping run");
	      stop_run();
	      return  FE_ERR_HW ; 
	    } // error
#ifdef HAVE_PSM
	  if(dd[13])printf("Cycle_start: calling LoadFreqDM_ptr with prandom_freq=%p \n",prandom_freq);
	  status = psmLoadFreqDM_ptr(gVme, PSM_BASE,prandom_freq, fs.output.num_frequency_steps, &first_frequency);
#endif
	  if(status == -1)
	    {
	      cm_msg(MERROR,"cycle_start","Error: frequency values at pointer %p are not loaded. Stopping run", 
		     prandom_freq);
	      stop_run();
	      return   FE_ERR_HW; 
	      
	    }
	}
    } // random flag  
  return SUCCESS;
}

/*---------------------------------------------------------*/
INT set_previous_freq_1f(void)
/*---------------------------------------------------------*/
{
  // Used by 1f with constant time between cycles to repeat cycle at previous frequency value 
  INT status;
  if(!const1f_flag)
    return  SUCCESS;  // not supported except for 1f const

 if(dd[18])
  printf("\nset_previous_freq_1f: starting with gbl_CYCLE_N=%d, freq=%d freqstep=%d  previous freq=%d previous freq step=%d\n",gbl_CYCLE_N, freq_val, gbl_FREQ_n, const1f_prev_freq_val,const1f_prev_FREQ_n );

  //  cm_msg(MINFO,"set_previous_freq_1f","starting with gbl_CYCLE_N=%d, gbl_FREQ_n=%d freq=%d  previous freq=%d",gbl_CYCLE_N,gbl_FREQ_n, freq_val, const1f_prev_freq_val ); // temp
  if(gbl_CYCLE_N == 0)
    {
      printf("\nset_previous_freq_1f: cannot step back in frequency from first cycle\n");
      return SUCCESS;
    }

  if  (gbl_FREQ_n  == 0)
    gbl_FREQ_n = freq_ninc-1; // cycle around
  else
    gbl_FREQ_n--; // step back counter

  freq_val =  const1f_prev_freq_val;    // set_previous_frequency();
  		  
#ifdef HAVE_PSM
  status=psm_setone(gVme, PSM_BASE, freq_val); /* set PSM freq val  in Hz; does a strobe */
  if(status == -1)
    {
      cm_msg(MERROR,"set_previous_freq_1f","Error from psm_setone. Previous frequency %d not set",freq_val);
      return  FE_ERR_HW ; /* error return.  */
    }
  if(!dd[11])
	printf(" set_previous_freq_1f : stepped back...  set previous frequency %d; now gbl_FREQ_n=%d\n", freq_val, gbl_FREQ_n);
#endif /* HAVE_PSM */
  cm_msg(MINFO,"set_previous_freq_1f","after step back, gbl_FREQ_n=%d and freq_val=%d ",gbl_FREQ_n, freq_val ); // temp
 
  const1f_stepped_back=1;
  return SUCCESS;
}
	    
#ifdef GONE
/*--------------------------------------------------------------*/
BOOL flip_helicity(BOOL timer)
/*--------------------------------------------------------------*/
{
  /* Returns TRUE (helicity flipped) or FALSE (error)
   */
  INT status, old_ppg_hel;

  // If timer is true, time the flip
  struct timeval time1,time2;
  double td = 1000; /* initialize to eliminate compiler warning */
  INT i,sleep_time_ms;
  
  if(dd[12]) printf("\n flip_helicity:starting \n");
  if(!fs.hardware.enable_all_hel_checks)
    printf("flip_helicity: Warning - all helicity checks are off\n");
  /* flip the requested helicity */
  old_ppg_hel = gbl_ppg_hel; // set value
  
  printf("\nflip_helicity: current set value gbl_ppg_hel = %d (%s)\n",gbl_ppg_hel,helicity_state(gbl_ppg_hel));
	 
#ifdef HAVE_NIMIO32
  INT current_hel_readback;
  
  // Check that current (or latched in dcm) readback is the same as set value
  current_hel_readback =  hel_read_ioreg(FALSE);  // latched readback if dual_channel_mode
  if( fs.hardware.enable_all_hel_checks && current_hel_readback != gbl_ppg_hel)
    {
     
      cm_msg(MERROR,"flip_helicity","%s helicity readback (%d) is not the same as previous set value (%d)",
	     latched, current_hel_readback, gbl_ppg_hel);
      return FALSE; // ERROR
    }
#endif
  printf("flip_helicity: %s helicity readback is %d (%s)\n",latched,  current_hel_readback, helicity_state( current_hel_readback));
  

#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
 printf("\n flip_helicity: Cannot actually flip helicity with PPG32. Flipping dummy helicity\n");
   if(gbl_ppg_hel== HEL_UP)
     gbl_ppg_hel=HEL_DOWN;
   else
     gbl_ppg_hel=HEL_UP;
   return TRUE;
#endif // HAVE_NEWPPG

 
  if(timer)gettimeofday(&time1,NULL);
  // complement for BNMR
  // we can see how long it takes hel to change to determine sleep time
  gbl_ppg_hel = complement( VPPGPolzFlip(gVme, PPG_BASE));  // flip the helicity
  printf("flip_helicity: after flip, gbl_ppg_hel=%d\n", gbl_ppg_hel);

#else
   // no hardware
   printf("\n flip_helicity: flipping dummy helicity\n");
   if(gbl_ppg_hel== HEL_UP)
     gbl_ppg_hel=HEL_DOWN;
   else
     gbl_ppg_hel=HEL_UP;
   return TRUE;
#endif // HAVE_PPG
   
  /* Dual Channel Mode : helicity flip sleep time is incorporated in PPG program  */
   if(fs.hardware.enable_dual_channel_mode)
     return TRUE; 
  

   /* single channel mode */
   gettimeofday(&th1,NULL); // time helicity sleep time (single channel mode)

   
     
       i=0;
       while (gbl_hel_readback == current_hel_readback)
	 {
	   gbl_hel_readback =  hel_read_ioreg(FALSE );
	   printf("Count %d  gbl_hel_readback=%d (%s)\n",i, gbl_hel_readback, helicity_state(gbl_hel_readback));
	   i++;
	   if(i > 20)
	     break;
	   ss_sleep(100); // sleep 100ms
	 }
       
       if(timer)
	 {
	   // time how long it takes to flip hel
	   gettimeofday(&time2,NULL);
	   // compute elapsed time in millisec
	   td = (time2.tv_sec - time1.tv_sec) * 1000.0;      // sec to ms
	   td += (time2.tv_usec - time1.tv_usec) / 1000.0;   // us to ms
	   
	   if(i==0)
	     printf("flip_helicity: helicity is already in the correct state\n");
	   else if(i > 20)
	     {
	       printf("flip_helicity: timeout after %f ms. Could not flip helicity. Current hel readback=%d",
		      td, gbl_hel_readback);
	       
	     }
	   else
	     printf("Time for helicity to change =  %f ms\n",td);
	 } // end timer
   
   if(fs.hardware.enable_all_hel_checks)
     {
       if(old_ppg_hel == gbl_ppg_hel)
	 {  /* shouldn't get this message unless a hardware problem */
	   printf("\nflip_helicity: Problem with PPG module... helicity set values didn't flip !!!\n");
	   return FALSE;
	 }
     }
   
   
   
   /* Single Channel Mode:  allow helicity more time to change
    */
   
   if (fs.hardware.helicity_flip_sleep__ms_ > 0)
     {
       if(!dd[11])printf("\n");
       
#ifdef HAVE_PPG  
       // Special mode for e1f with a loop - stop PPG during sleep time
       
       if( const1f_flag &&  e1f_started_flag)
	 {
	   if(dd[18])printf("flip_helicity: e1f constant time mode - stopping then restarting PPG for helicity sleep time\n");
#ifdef HAVE_NEWPPG
	   TPPGReset(gVme,PPG_BASE); // reset stops PPG even if in long delay
#else
	   VPPGStopSequencer(gVme, PPG_BASE);
#endif //  HAVE_NEWPPG 
	   e1f_started_flag=FALSE; // clear flag
	 }
       
#endif // HAVE_PPG
       if(timer)
	 {
	   sleep_time_ms = fs.hardware.helicity_flip_sleep__ms_ -  (int)td;
	   if (sleep_time_ms < 0 )
	     sleep_time_ms = 0;
	   if(sleep_time_ms > 0)
	     {
	       printf("Sleeping for %d ms more for helicity flip sleep time... \n",sleep_time_ms);
	       ss_sleep(sleep_time_ms); // timer test - not likely to be using mode 2h 
                                        // where event pieces sent out during helicity sleep (prevented by ss_sleep,cm_yield)
	     }
	 }
       else
	 sleep_time_ms =  fs.hardware.helicity_flip_sleep__ms_ ;
       
       if(!dd[11])printf("flip_helicity: waiting %d ms to allow helicity more time to flip...\n",
			 sleep_time_ms);
       //  if(sleep_time_ms > 0)
       //gbl_wait_helicity=1;  // gbl_alpha single channel mode only
	 
       printf("\nhelicity_flip: slept for %f ms\n",td);
       ss_sleep(50);
     
       

       // Stop the e1f timer getting hel_flip_sleep_time as the max by setting flag;
       hel_flip_flag=1; // ignore this point (used for e1fconst time mode)
     } // hel flip time > 0


#ifdef HAVE_NIMIO32
   gbl_hel_readback =  hel_read_ioreg(FALSE );
   if(fs.hardware.enable_all_hel_checks)
     {
       if(gbl_hel_readback > 1)
	 {
	   cm_msg(MERROR,"helicity_flip","helicity in unknown state (%d) according to NIMIO32 readback",gbl_hel_readback);
	   stop_run();
	   return FALSE;
	 }
       
       else if (gbl_hel_readback == current_hel_readback)
	 {
	   cm_msg(MERROR,"helicity_flip","helicity has not flipped according to NIMIO32 readback");
	   stop_run();
	   return FALSE;
	 }
     }
#endif
   return TRUE; // helicity has flipped
}

#endif // GONE

/*--------------------------------------------------------------*/
BOOL flip_helicity(BOOL timer)
/*--------------------------------------------------------------*/
{
  /* Returns TRUE (helicity flipped) or FALSE (error)
   */
  INT status, old_ppg_hel;

  // If timer is true, time the flip
  struct timeval time1,time2;
  double td = 1000; /* initialize to eliminate compiler warning */
  INT i,sleep_time_ms;
  
  if(dd[12]) printf("\n flip_helicity:starting \n");
  if(!fs.hardware.enable_all_hel_checks)
    printf("flip_helicity: Warning - all helicity checks are off\n");
  /* flip the requested helicity */
  old_ppg_hel = gbl_ppg_hel; // set value
  
  printf("\nflip_helicity: current set value gbl_ppg_hel = %d (%s)\n",gbl_ppg_hel,helicity_state(gbl_ppg_hel));
	 
#ifdef HAVE_NIMIO32
  INT current_hel_readback;
  
  // Check that current (or latched in dcm) readback is the same as set value
  current_hel_readback =  hel_read_ioreg(FALSE);  // latched readback if dual_channel_mode
  if( fs.hardware.enable_all_hel_checks && current_hel_readback != gbl_ppg_hel)
    {
     
      cm_msg(MERROR,"flip_helicity","%s helicity readback (%d) is not the same as previous set value (%d)",
	     latched, current_hel_readback, gbl_ppg_hel);
      return FALSE; // ERROR
    }
#endif
  printf("flip_helicity: %s helicity readback is %d (%s)\n",latched,  current_hel_readback, helicity_state( current_hel_readback));
  

#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
 printf("\n flip_helicity: Cannot actually flip helicity with PPG32. Flipping dummy helicity\n");
   if(gbl_ppg_hel== HEL_UP)
     gbl_ppg_hel=HEL_DOWN;
   else
     gbl_ppg_hel=HEL_UP;
   return TRUE;
#endif // HAVE_NEWPPG

  // complement for BNMR
  if(timer)gettimeofday(&time1,NULL);
  // we can see how long it takes hel to change to determine sleep time
  gbl_ppg_hel = complement( VPPGPolzFlip(gVme, PPG_BASE));  // flip the helicity
  printf("flip_helicity: after flip, gbl_ppg_hel=%d\n", gbl_ppg_hel);

#else
   // no hardware
   printf("\n flip_helicity: flipping dummy helicity\n");
   if(gbl_ppg_hel== HEL_UP)
     gbl_ppg_hel=HEL_DOWN;
   else
     gbl_ppg_hel=HEL_UP;
   return TRUE;
#endif // HAVE_PPG
   
  /* Dual Channel Mode : helicity flip sleep time is incorporated in PPG program  */
   if(fs.hardware.enable_dual_channel_mode)
     return TRUE; 
  

   /* single channel mode */

   if( timer)
     {
       i=0;
       while (gbl_hel_readback == current_hel_readback)
	 {
	   gbl_hel_readback =  hel_read_ioreg(FALSE );
	   printf("Count %d  gbl_hel_readback=%d (%s)\n",i, gbl_hel_readback, helicity_state(gbl_hel_readback));
	   i++;
	   if(i > 20)
	     break;
	   ss_sleep(100); // sleep 100ms
	 }


       // time how long it takes to flip hel
       gettimeofday(&time2,NULL);
       // compute elapsed time in millisec
       td = (time2.tv_sec - time1.tv_sec) * 1000.0;      // sec to ms
       td += (time2.tv_usec - time1.tv_usec) / 1000.0;   // us to ms
       
       if(i==0)
	 printf("flip_helicity: helicity is already in the correct state\n");
       else if(i > 20)
	 {
	   printf("flip_helicity: timeout after %f ms. Could not flip helicity. Current hel readback=%d",
		  td, gbl_hel_readback);
	   
	 }
       else
	 printf("Time for helicity to change =  %f ms\n",td);
     } // timer

   if(fs.hardware.enable_all_hel_checks)
     {
       if(old_ppg_hel == gbl_ppg_hel)
	 {  /* shouldn't get this message unless a hardware problem */
	   printf("\nflip_helicity: Problem with PPG module... helicity set values didn't flip !!!\n");
	   return FALSE;
	 }
     }
  
 

  /* Single Channel Mode:  allow helicity more time to change
   */
  
   if (fs.hardware.helicity_flip_sleep__ms_ > 0)
     {
       if(!dd[11])printf("\n");

#ifdef HAVE_PPG  
       // Special mode for e1f with a loop - stop PPG during sleep time

       if( const1f_flag &&  e1f_started_flag)
	 {
	   if(dd[18])printf("flip_helicity: e1f constant time mode - stopping then restarting PPG for helicity sleep time\n");
#ifdef HAVE_NEWPPG
	   TPPGReset(gVme,PPG_BASE); // reset stops PPG even if in long delay
#else
	   VPPGStopSequencer(gVme, PPG_BASE);
#endif //  HAVE_NEWPPG 
	   e1f_started_flag=FALSE; // clear flag
	 }
       
#endif // HAVE_PPG
       if(timer)
	 {
	   sleep_time_ms = fs.hardware.helicity_flip_sleep__ms_ -  (int)td;
	   if (sleep_time_ms < 0 )
	     sleep_time_ms = 0;
	   if(sleep_time_ms > 0)
	     printf("Sleeping for %d ms more for helicity flip sleep time... \n",sleep_time_ms);
	 }
       else
	 sleep_time_ms =  fs.hardware.helicity_flip_sleep__ms_ ;

       if(!dd[11])printf("flip_helicity: waiting %d ms to allow helicity more time to flip...\n",
			 sleep_time_ms);
       if(sleep_time_ms > 0)
	 {
       
       //gbl_wait_helicity=1;  // gbl_alpha single channel mode only
	 
	   status = ss_sleep(sleep_time_ms);
	   if(status != SUCCESS)
	     return status;
	 }

       // Stop the e1f timer getting hel_flip_sleep_time as the max by setting flag;
       hel_flip_flag=1; // ignore this point (used for e1fconst time mode)
     } // hel flip time > 0


#ifdef HAVE_NIMIO32
   gbl_hel_readback =  hel_read_ioreg(FALSE );
   if(fs.hardware.enable_all_hel_checks)
     {
       if(gbl_hel_readback > 1)
	 {
	   printf("problem - gbl_hel_readback=%d... retrying after 0.5s\n",gbl_hel_readback);
	   // try again
	   ss_sleep(500);
	   gbl_hel_readback =  hel_read_ioreg(FALSE );
	   if(gbl_hel_readback > 1)
	     cm_msg(MERROR,"helicity_flip","helicity in unknown state (%d) according to NIMIO32 readback",gbl_hel_readback);
	   stop_run();
	   return FALSE;
	 }
       
       else if (gbl_hel_readback == current_hel_readback)
	 {
	   cm_msg(MERROR,"helicity_flip","helicity has not flipped according to NIMIO32 readback");
	   stop_run();
	   return FALSE;
	 }
     }
#endif
   return TRUE; // helicity has flipped
}

INT sample_flip(void)
{
#ifdef HAVE_NIMIO32
  int read_sample;

  sample_read();
  read_sample = gbl_read_sample; // remember value
  if(gbl_read_sample)
  {
      nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT2 ); // Clear OUTPUT2 (2g microwaves off) REFERENCE mode
      gbl_set_sample=0; // clear
  }
  else
  {
      nimio_SetOneOutput(gVme,  NIMIO32_BASE, OUTPUT2 ); // Set OUTPUT2 (2g microwaves on) SAMPLE mode
      gbl_set_sample=1; // set
  }
  sample_read();
  if(gbl_read_sample == read_sample)
    {
      printf("sample_flip: flip failed\n");
      return FAILURE;
    }
  printf("sample_flip: sample/ref flipped!!  Was %d  now %d\n",read_sample,gbl_read_sample);
#endif
  return SUCCESS;
}

void sample_read(void)
{
#ifdef HAVE_NIMIO32
  if( nimio_ReadOneInput(gVme,  NIMIO32_BASE, INPUT6))
    gbl_read_sample=1;
  else
    gbl_read_sample=0;
#endif
   return;
}

void sample_set(INT val)
{
#ifdef HAVE_NIMIO32
  if(val)
     nimio_SetOneOutput(gVme,  NIMIO32_BASE, OUTPUT2 ); // Set OUTPUT2 (2g microwaves on) SAMPLE mode
  else
     nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT2 ); // Clear OUTPUT2 (2g microwaves off) REFERENCE mode
  gbl_set_sample=val;
#endif
   return;
}


/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  INT status,size;
  printf("\nend_of_run: starting... and ending\n");  
  if(const1f_flag) // SPEEDUP, BINCHECK only in const1f mode
    {
#ifdef BINCHECK
      if(bad_cntr==0)
	printf("end_of_run: Bincheck found no events with 1f const time zero bin problem have been found, i.e. all prevented by restarting PPG\n");
      else
	printf("end_of_run: BINCHECK detected %d bad cycles and %d good cycles\n",bad_cntr,good_cntr);
#endif // BINCHECK
  
  
#ifdef SPEEDUP
      printf("end_of_run: Counters: \n");
      printf("  histo_process counts cases where previous histograms have not been sent out... gbl_prev_histo_cntr = %d\n",  gbl_prev_histo_cntr);
      printf("  cycle_start counts cases where last cycle skipped but waiteqp[HISTO] is set... gbl_prev_skip_cntr=%d\n", gbl_prev_skip_cntr);
      printf("  histo_read counts number of times skip_cycle is set  (expect none)         ... gbl_HistoRead_skipped_cycle=%d \n", gbl_HistoRead_skipped_cycle);
      printf(" Number of PPG restarts due to computer busy overrun : %d  \n",gbl_ppg_restart_cntr);
#endif // SPEEDUP
#ifdef HAVE_NIMIO32
      nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT5 ); // Clear OUTPUT5 (sending histograms)
      nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT6 ); // Clear OUTPUT6 (gbl_in_cycle)
      nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT7 ); // Clear OUTPUT7 (computer busy processing)
#endif
      printf("Timers:\n");
      printf("histo_process max =%f ms  min = %f ms \n", gbl_pmax,gbl_pmin);
      printf("histo_read    max =%f ms  min = %f ms \n", gbl_rmax,gbl_rmin);
    }


  if(const1f_flag)
    {
      printf("Elapsed time for computer busy..  max: %f ms and  min: %f ms \n", gbl_maxElapsedTime,gbl_minElapsedTime);
      printf("Timer exceeded programmed computer busy (%f ms)  %d times (value includes next counter)\n", fs.input.daq_service_time__ms_, gbl_cb_cntr2  );
      printf("Number of cycles where PPG computer busy not still set after histo processing : %d\n", gbl_ppg_cb_cntr1);
      printf("Number of cycles where computer busy not set at end of PPG cycle: %d\n",gbl_ppg_cb_cntr2);
      printf("Number of cycles where computer busy true during the cycle: %d\n",gbl_ppg_cb_cntr3);
    }
  /* Make sure threshold alarms don't keep going off when not running */
  cyinfo.last_failed_thr_test=0;
  size = sizeof(cyinfo.last_failed_thr_test);
 if(!hInfo)printf("\n *** hInfo not assigned\n");
  status = db_set_value(hDB, hInfo, "last failed thr test", 
			&cyinfo.last_failed_thr_test, size, 1, TID_DWORD);
  if (status != DB_SUCCESS)
      cm_msg(MERROR, "end_of_run", "cannot clear \"...info_odb/var/last failed thr test\" (%d)",
	     status);

  

  status = al_reset_alarm("thresholds");
  if(status == AL_RESET)
    printf("end_of_run: threshold alarm is now reset\n");
  else if (status == AL_INVALID_NAME)
    printf("end_of_run: Error threshold alarm invalid name\n");
  else
    printf("end_of_run: Success\n");

  /* make sure  hold flag is cleared */
  hold_flag = FALSE;
  size = sizeof(hold_flag);
  if(!hFS)printf("Error hFS not assigned\n");
  status = db_set_value(hDB, hFS, "flags/hold", &hold_flag, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    cm_msg(MERROR, "end_of_run", "cannot clear hold flag at end_of_run");

  gbl_stopping_run_flag=loop_error_cntr=0; 
  fflush(stdout);
  return CM_SUCCESS;
}

/*-- Post End of Run ----------------------------------------------------*/

INT post_end_run(INT run_number, char *error)
{

  INT status,size;

  /* Since end_of_run runs BEFORE equipments EOR transition add this post-stop routine
     running after the logger, to close Epics channels, clear histo pointers etc. */
 
  printf("\npost_end_run: starting\n"); 

#ifdef EPICS_ACCESS
  printf("post_end_run: setting epics flag(s) false and disconnecting\n");

  gbl_epics_live = FALSE; 
  if(epics_flag)
    EpicsDisconnect(&epics_params); /* epics scan */

#ifdef LASER
  if(gbl_hel_live)
    {
      gbl_hel_live = FALSE;
      ChannelDisconnect(&Rchid); /* hel or bias */
    }
  if (gbl_laser_live)
    {
      gbl_laser_live = FALSE;
      ChannelDisconnect(&Rchid_lp); /* laser voltage */
    }
#endif
  caExit(); /* close any open channels */
  
#endif
  
#ifdef CAMP_ACCESS
  if(camp_flag)
    camp_end(); /*  disconnect from CAMP */
#endif
 
#ifdef HAVE_NIMIO32
  if(const1f_flag) // SPEEDUP, BINCHECK only in const1f mode
    {
      if(write_cb_stats() != SUCCESS)
	printf("end_of_run: failure from write_cb_stats\n");
    }
#endif
  /*  free random arrays  */
  
  printf("post_end_run: freeing pointers for random arrays & storing seed value=%d \n",seed);
  if(random_flag)
    {
      random_flag=FALSE;
      fs.output.last_seed__for_random_freqs_=seed; /* remember the seed value for next time */
      size = sizeof(fs.output.last_seed__for_random_freqs_);
      status = db_set_value(hDB, hFS, "output/last seed (for random freqs)", 
			    &fs.output.last_seed__for_random_freqs_, size, 1, TID_DWORD);
      if (status != DB_SUCCESS)
	cm_msg(MERROR, "post_end_run", "cannot write seed value=%d to \"output/last seed (for random freqs)\"(%d)",
	       fs.output.last_seed__for_random_freqs_,status);
    }
  /* occasionally randomize_freq_values is called while 2e/2a run is still stopping; 
     do not clear pointers while random_flag is true  */
  e2a_flag=e2e_flag=e2f_flag=FALSE;
  free_freq_pntrs(); /* free memory for arrays - used for non-random as well */
  free_all_scalers(); // frees all possible scalers
  free_all_histos();  // frees all possible histos
  fflush(stdout);
  if(!dd[1])printf("\n");

#ifdef HAVE_PSM
  printf("end_of_run: disabling PSM");
  disable_psm(gVme, PSM_BASE);
  psmWriteGateControl(gVme, PSM_BASE,"all",0); /* disable external gates; ppg may send spurious gates when loaded */
#endif
  printf("\npost end of run %d\n",run_number);

  status = set_client_alarm(0); // Disable client alarm while run is stopped
 
 if(status != DB_SUCCESS)
   return status;
 
fflush(stdout);
return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  cm_msg(MERROR,"pause_run","This command DOES NOT WORK for %s experiment. Use \"%s hold\" button instead",
	 beamline,beamline);
  cm_msg(MERROR,"pause_run"," MIDAS PAUSE detected. RUN MUST BE STOPPED then restarted.");
 
  stop_run();
  return  FE_ERR_HW;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  cm_msg(MERROR,"resume_run","This command DOES NOT WORK for BNMR experiment Use BNMR resume button instead");
  cm_msg(MERROR,"resume_run","MIDAS RESUME detected. RUN MUST BE STOPPED and restarted.");
  stop_run();
 

  return  FE_ERR_HW;
}

/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  int   i;
  for (i=0 ; i<count ; i++)
  {
    ss_sleep(1); // include 1ms delay 
    if (!test)
      return FALSE;
  }
  return FALSE;
}

#ifdef HAVE_SIS3801
/*-- Faked Event readout ---------------------------------------------*/
INT FIFO_acq(char * pevent, INT off)
/*
  - Check if still in cycle
  - acquire the FIFO data
  - increment the histo
    Takes about 10ms for 1/2FIFO and scaler incrementation
  - update the /variable scalers if it's time
  - return the in cycle status

  - runs every 50ms & tests if FIFO is 1/2 full
 */
{
  INT status, nwords_A, nwords_B, size;
  static struct timeval tt1, tt2;
  double elapsed_time;
  // INT readbck_val;
  // DWORD data;
  // INT ppgTrigReg;
  

  // SIS3801 version

  //  if(dd[1])
    printf("FIFO_acq (SIS3801) starting...\n");

  if(gbl_die) return 0; // hardware error; trying to stop run

  if (gbl_IN_CYCLE)
  {          /* cycle started */

    if (dd[0])printf("IN CYCLE ,gbl_bin_count(%d), gbl_BIN_A(%d), gbl_BIN_B(%d)\n",
                  gbl_bin_count,gbl_BIN_A,gbl_BIN_B);

    /*

              Test MODULE A FIFO  Half full
    */
    
#ifdef TWO_SCALERS
    if (sis3801_CSR_read( gVme, SIS3801_BASE_A, IS_FIFO_HALF_FULL) != 0) /* test on FIFO A */
    {
     
      /* FIFO A is Half full at least so read out data */
      nwords_A = sis3801_HFIFO_read(gVme, SIS3801_BASE_A, pfifo_A);
      
      /* check for error conditions:  FULL (nwords=-1) or EMPTY (nwords=0)  */
      if (nwords_A < 1)
      {
        printf ("FIFO_acq-Module A FULL(-1) or Empty(0);  nwords:%d\n",nwords_A);
    die();
	return 0;
      }
      /* Move data & increment corresponding scaler histogram  */
      if(dsis)printf("Calling scaler_increment for Module A nwords_A=%d pfifo_A=0x%x scaler_offset=%d, max_chan=%d\n",
	     nwords_A, (unsigned int)pfifo_A, 0, MAX_CHAN_SIS38xxA);

      if(pulsepair_flag) // Mode 2e...
	status = scaler_increment_pulsepair(nwords_A, pfifo_A, 0, MAX_CHAN_SIS38xxA, &gbl_BIN_A, &userbit_A);
      else
	status = scaler_increment(nwords_A, pfifo_A, 0, MAX_CHAN_SIS38xxA, &gbl_BIN_A, &userbit_A);     
      if(status !=0)
	printf("FIFO_acq: error return from scaler_increment for Scaler A\n");

      if (dd[0]) printf ("IN CYCLE eqp[FIFO]:%d  nW:%d gbl_BIN_A :%d\n",waiteqp[FIFO],nwords_A,gbl_BIN_A);
      if(dd[1])
      {
        status = sis3801_CSR_read(gVme, SIS3801_BASE_A,CSR_FULL);
        printf("After read %d words, Module A  gbl_BIN_A=%d, CSR 0x%x\n",nwords_A,gbl_BIN_A,status);
      }
      
    } /* Module A not half full yet */

#endif
    /*
              Test MODULE B FIFO  Half full
    */
    if (sis3801_CSR_read(gVme, SIS3801_BASE_B, IS_FIFO_HALF_FULL) != 0) /* test on FIFO B */
    {
  gbl_count++;
      /* FIFO B is Half full at least so read out data */
      nwords_B = sis3801_HFIFO_read(gVme, SIS3801_BASE_B, pfifo_B);
    
      /* check for error conditions:  FULL (nwords=-1) or EMPTY (nwords=0)  */
      if (nwords_B < 1)
      {
        printf ("FIFO_acq- Module B FULL(nwords=-1) or Empty(nwords=0);  nwords:%d\n",nwords_B);
  	printf("gbl_count=%d ,gbl_bin_count(%d), gbl_BIN_A(%d), gbl_BIN_B(%d)\n",
	       gbl_count, gbl_bin_count,gbl_BIN_A,gbl_BIN_B);
    
        die();
	return 0;
      }
      /* Move data & increment corresponding scaler histogram */
      if(dsis)
	printf("\nCalling scaler_increment gbl_count=%d  for Module B nwords_B=%d pfifo_B=0x%x scaler_offset=%d, max_chan=%d\n",
	       gbl_count, nwords_B, (unsigned int)pfifo_B, MAX_CHAN_SIS38xxA , MAX_CHAN_SIS38xxB);
      if(pulsepair_flag)  // Mode 2e...
	status =  scaler_increment_pulsepair(nwords_B, pfifo_B, MAX_CHAN_SIS38xxA, MAX_CHAN_SIS38xxB, &gbl_BIN_B, &userbit_B);   
      else
	status = scaler_increment(nwords_B, pfifo_B, MAX_CHAN_SIS38xxA, MAX_CHAN_SIS38xxB, &gbl_BIN_B, &userbit_B); 
      if(status !=0)
	printf("FIFO_acq: error return from scaler_increment for Scaler B\n");
      
      if (dd[0]) printf ("IN CYCLE eqp[FIFO]:%d  nW:%d gbl_BIN_B :%d\n",waiteqp[FIFO],nwords_B,gbl_BIN_B);
      //   if(dd[1])
      {
        status = sis3801_CSR_read(gVme, SIS3801_BASE_B,CSR_FULL);
        printf("After read %d words, Module B  gbl_BIN_B=%d, CSR 0x%x\n",nwords_B,gbl_BIN_B,status);
      }
      
    } /* Module B not half full yet */
  
  } /* end of  if gbl_IN_CYCLE */ 

  else
  { /* Cycle is OFF expected to be in end of cycle state */
       if (dd[0]) 
	 printf ("OFF cycle: gbl_bin_count=%d waiteqpF:%d H:%d C:%d D:%d\n"
                     ,gbl_bin_count,waiteqp[FIFO],waiteqp[HISTO],waiteqp[CYCLE],waiteqp[DIAG]);
    
    if (!waiteqp[FIFO]) return 0; /* not in end-of-cycle state */

    nwords_A = nwords_B = 0; 
#ifdef TWO_SCALERS
    /* Flush Module A FIFO   (Test FIFO empty) */
    if (sis3801_CSR_read(gVme, SIS3801_BASE_A, IS_FIFO_EMPTY) == 0)
    {
      /* Not empty */
      nwords_A = sis3801_FIFO_flush(gVme, SIS3801_BASE_A, pfifo_A); /* get the last data */
      if(dd[0])printf("FIFO_ACQ_flush - read %d words from Module A \n",nwords_A);
      
      /* check for error conditions:  FULL (nwords=-1) or EMPTY (nwords=0)  */
      if (nwords_A < 1)
      {
        printf ("FIFO_acq-Module A FULL(-1) or Empty(0);  nwords:%d\n",nwords_A);
        die();
	return 0;
      }
      /* Move data & increment corresponding scaler histogram */
      if(dsis)printf("after flush - calling scaler_increment for Module A nwords_A=%d pfifo_A=0x%x scaler_offset=%d, max_chan=%d\n",
	     nwords_A, (unsigned int)pfifo_A, 0, MAX_CHAN_SIS38xxA);

      if(pulsepair_flag)  // Mode 2e...
	status = scaler_increment_pulsepair(nwords_A, pfifo_A, 0, MAX_CHAN_SIS38xxA, &gbl_BIN_A, &userbit_A); 
      else
	status = scaler_increment(nwords_A, pfifo_A, 0, MAX_CHAN_SIS38xxA, &gbl_BIN_A, &userbit_A);       
      if(status !=0)
	printf("FIFO_acq: error return from scaler_increment for Scaler A\n");
     
    } /* Module A has been flushed */
  
#endif

    /* Flush Module B FIFO   (Test FIFO empty) */
    if (sis3801_CSR_read(gVme, SIS3801_BASE_B, IS_FIFO_EMPTY) == 0)
    {
      /* Not empty */
      nwords_B = sis3801_FIFO_flush(gVme, SIS3801_BASE_B, pfifo_B); /* get the last data */
      if(dd[0])
        printf("FIFO_ACQ_flush - read %d words from Module B \n",nwords_B);
      
      /* check for error conditions:  FULL (nwords=-1) or EMPTY (nwords=0)  */
      if (nwords_B < 1)
      {
        printf ("FIFO_acq-Module B FULL(-1) or Empty(0);  nwords:%d\n",nwords_B);
        die();
	return 0;
      }
      /* Move data & increment corresponding scaler histogram */
      if(dsis)printf("flush - calling scaler_increment for Module B nwords_B=%d pfifo_B=0x%x scaler_offset=%d, max_chan=%d\n",
	     nwords_B, (unsigned int)pfifo_B, MAX_CHAN_SIS38xxA,MAX_CHAN_SIS38xxB);
      if(pulsepair_flag)  // Mode 2e...
	status =  scaler_increment_pulsepair(nwords_B, pfifo_B, MAX_CHAN_SIS38xxA, MAX_CHAN_SIS38xxB, &gbl_BIN_B, &userbit_B);  
      else
	status = scaler_increment(nwords_B, pfifo_B, MAX_CHAN_SIS38xxA, MAX_CHAN_SIS38xxB, &gbl_BIN_B, &userbit_B);  
      if(status !=0)
	printf("FIFO_acq: error return from scaler_increment for Scaler B\n");

    } /* Module B has been flushed */
  


#ifdef TWO_SCALERS
    if (nwords_A <= 0 || nwords_B <= 0)
    {       /* Error if both modules do not have data */
      printf ("FIFO_acq-Flush: Module A or B EMPTY  nwords: A(%d) B(%d)\n",nwords_A,nwords_B);
      die();
      return 0;
    }
#else
    if (nwords_B <= 0 )
    {       /* Error if module does not have data */
      printf ("FIFO_acq-Flush: Module B is EMPTY \n");
      die();
      return 0;
    }
#endif
  
#ifdef TWO_SCALERS
  if (dd[1])
    {
      status = sis3801_CSR_read(gVme, SIS3801_BASE_A,CSR_FULL);
      printf("After flush, gbl_bin_count %d, gbl_BIN_A %d,  CSR A 0x%x\n",
             gbl_bin_count,gbl_BIN_A,status);
    }
#endif
  if (dd[1])
    {
      status = sis3801_CSR_read(gVme, SIS3801_BASE_B,CSR_FULL);
      printf("             gbl_BIN_B %d,  CSR B 0x%x\n",
             gbl_BIN_B,status);
    }
#ifdef TWO_SCALERS    
    if ( gbl_BIN_A != gbl_BIN_B )
      {      /* Error if both modules do not have the same bin count */
        printf("\nFIFO_acq: Mismatch-last bin A(%d),B(%d); nwords A(%d),B(%d)\n",
               gbl_BIN_A,gbl_BIN_B,nwords_A,nwords_B);
        die();
        return 0;
      }
    /* Check Userbits in type 2 mode */
    if(exp_mode == 2){ 
      if(userbit_A != userbit_B)
	printf("\nFIFO_acq: Mismatch- userbits A(%d),B(%d)\n",
	       userbit_A,userbit_B);
    }
#endif

    waiteqp[FIFO]=FALSE; 

    /* As the cycle is completed compact histo S0 to 15 H0..1, S16..S31 to H2..3 */
    gettimeofday(&tt1, NULL); // start timer
    //printf("fifo_acq: calling histo_process \n");
    histo_process_flag=1;
    status = histo_process();
    histo_process_flag=0;

    gettimeofday(&tt2, NULL);  // stop timer
    elapsed_time = (tt2.tv_sec - tt1.tv_sec) * 1000.0;      // sec to ms
    elapsed_time += (tt2.tv_usec - tt1.tv_usec) / 1000.0;   // us to ms
    // printf("fifo_acq: time for histo_process was  %f ms\n",elapsed_time);
    if(elapsed_time > gbl_pmax)
      gbl_pmax=elapsed_time;
    if(elapsed_time < gbl_pmin)
      gbl_pmin=elapsed_time;

    if(skip_cycle)
      {
	//printf("fifo_acq:calling skip_cycle is true, returning\n");
	waiteqp[HISTO]=FALSE;
	return 0;
      }

   
#ifdef SPEEDUP
    if(const1f_flag) // SPEEDUP only in const1f mode
      {

	if(  gbl_CYCLE_H != 0) // shouldn't happen except at bor or on hold
	  {
	    printf("\n ** fifo_acq: - ERROR previous cycle %d not yet histogrammed hold=%d!! ** \n",hold_flag,gbl_CYCLE_H);
	    cm_msg(MERROR,"fifo_acq","ERROR previous cycle %d not yet histogrammed!! ",gbl_CYCLE_H); // what do we do?	      
	  }
   } // end const1f_flag
#endif
	if(dd[18])
	  printf("fifo_acq: cycle %d is complete, histo_process has finished\n",gbl_CYCLE_N);


    // histo_read was called here before ALPHA mode was added
    gettimeofday(&tt1, NULL); // start timer
    //    size = histo_read(pevent, off);  //clears waiteqp[HISTO]
    gettimeofday(&tt2, NULL);  // stop timer
    elapsed_time = (tt2.tv_sec - tt1.tv_sec) * 1000.0;      // sec to ms
    elapsed_time += (tt2.tv_usec - tt1.tv_usec) / 1000.0;   // us to ms
    // printf("fifo_acq: time for histo_read was  %f ms\n",elapsed_time);
    if(elapsed_time > gbl_rmax)
      gbl_rmax=elapsed_time;
    if(elapsed_time < gbl_rmin)
      gbl_rmin=elapsed_time;

    // printf("fifo_acq: size returned from histo_read is %d; waiteqp[HISTO]=%d; gbl_CYCLE_H=%d time for histo_read: %f ms\n",
    //  size,waiteqp[HISTO],gbl_CYCLE_H, elapsed_time);
    return size;


  } // cycle is OFF
    
 
  return 0;
}
#endif // HAVE_SIS3801

/*-- Scaler Channel Event  ---------------------------------------------------------*/
INT diag_read(char *pevent, INT off)
/*
  - Compose scaler event for one channel only
*/
{
  INT    ch;
  DWORD *pdata;
  WORD *qdata;
  char   bank_name[4];
  
 
  ch = fs.hardware.diagnostic_channel_num;
  if (!waiteqp[FIFO] && waiteqp[DIAG])
  {
    if (dd[1]) printf("diag_read %d diag ch %d ", waiteqp[FIFO], ch);     
    if ( (ch >= 0) && (ch < n_scaler_real)) 
    {
      /* init bank structure */
      
      bk_init32(pevent);
      sprintf(bank_name, "CH%2.2i", ch);
      if(small_bin_flag)
	{
	  bk_create(pevent, bank_name, TID_WORD, (void**)&qdata);
	  memcpy(qdata, scaler[ch].qs, scaler[ch].nbins * sizeof(WORD));
	  qdata +=  scaler[ch].nbins;
	  bk_close(pevent, qdata);
	}
      else
	{
	  bk_create(pevent, bank_name, TID_DWORD, (void**)&pdata);
	  memcpy(pdata, scaler[ch].ps, scaler[ch].nbins * sizeof(DWORD));
	  pdata +=  scaler[ch].nbins;
	  bk_close(pevent, pdata);
	}
      /* inform that the histo bank has been sent out */
      if (dd[1]) printf("Sbksize:%d\n",bk_size(pevent));
      waiteqp[DIAG] = FALSE;
      return bk_size(pevent);
    }
    else
    {
      if (dd[1]) printf("diag_read: Invalid channel\n");
      waiteqp[DIAG] = FALSE;
    }
  }
  return 0;
}


/*-- Histo Event  ---------------------------------------------------------*/
INT histo_read(char *pevent, INT off)
/*
  00   01   02  
  - Generate histo event -HIBP HIFP UBIT                Scaler A
  HM00 HM01 HM02 HM03 HM04 HM05 HM06 Scaler B - 
  
  when cycle is completed
  called periodically
*/
{
  INT    h,  nhb, i,j;
  DWORD *pdata;
  WORD *qdata;
  //WORD *qstart;
  char   bank_name[4];
  char string[5];
  //dd[18]=1;// TEMP
  // if(dd[8])
  //printf("histo_read: starting with pevent=%p and skip_cycle=%d, gbl_HEL=%d, flip=%d send_histo_event=%d\n",
  //  pevent,skip_cycle,gbl_HEL,flip,send_histo_event); 

  // printf("histo_read: starting with waiteqp[HISTO]=%d,gbl_IN_CYCLE=%d,send_histo_event=%d, skip_cycle=%d\n",
  //		     waiteqp[HISTO],gbl_IN_CYCLE,send_histo_event,skip_cycle);

  if(send_histo_event == 0)
      return 0; // nothing to do
    
  if (!waiteqp[HISTO])goto ret;

  if(skip_cycle)
    goto ret;
  
  if(dd[18])  printf("histo_read: starting with waiteqp[HISTO]=%d,gbl_IN_CYCLE=%d,send_histo_event=%d, skip_cycle=%d\n",
		     waiteqp[HISTO],gbl_IN_CYCLE,send_histo_event,skip_cycle);
  
  if(exp_mode == 1 &&  gbl_CYCLE_N % fs.input.num_cycles_per_supercycle != 0 )
    goto ret;  // mid supercycle; do not send event
  
#ifdef SPEEDUP
  if(const1f_flag)
    {
      if(dd[18])
	printf("histo_read: sending histos  send_histo_event=%d waiteqp[FIFO]=%d waiteqp[HISTO]=%d  gbl_IN_CYCLE=%d, gbl_CYCLE_N=%d, freq_val=%u ,gbl_CYCLE_H=%d, gbl_freq_H=%u\n",
	       send_histo_event, waiteqp[FIFO], waiteqp[HISTO], gbl_IN_CYCLE, gbl_CYCLE_N, freq_val, gbl_CYCLE_H, gbl_freq_H);
      if(!gbl_IN_CYCLE)
	{
	  if(dd[18])printf("histo_read: const1f mode, not in cycle, returning\n");
	  return 0;
	}
#ifdef HAVE_NIMIO32
      nimio_SetOneOutput(gVme,  NIMIO32_BASE, OUTPUT5 ); // Set OUTPUT5 (sending histograms)
#endif
    }
#else
  if(dd[18])
    printf("histo_read: sending histos  send_histo_event=%d waiteqp[FIFO]=%d waiteqp[HISTO]=%d  gbl_IN_CYCLE=%d, gbl_CYCLE_N=%d\n",
	   send_histo_event, waiteqp[FIFO], waiteqp[HISTO], gbl_IN_CYCLE, gbl_CYCLE_N);
#endif // SPEEDUP

  if(dd[8])
    printf("histo_read: end of supercycle, sending histos for gbl_CYCLE_N=%d\n",gbl_CYCLE_N);
  
  
  if(dd[20])
    printf("histo_read: starting with pevent=%p send_histo_event=%d mask=%d (0x%x)\n",pevent, send_histo_event, histo_trigger_mask,histo_trigger_mask);
  /*
    if(histo_trigger_mask==0)
    {
    printf("histo_read: returning because histo_trigger_mask=0\n");
    return 0;
    }
  */
  
  /* For TDtype modes */
  if(exp_mode == 2) 
    {
      /*  moved to fifo_acq for SIS3820 and split banks */
#ifdef HAVE_SIS3801 
      if(flip && (gbl_ppg_hel == HEL_DOWN))/*  use ppg set value */
 	{
	  goto ret; // don't send histos
	  printf("histo_read: type 2 flip is true,  %d (HEL_DOWN), returning\n",
		 gbl_ppg_hel);
	}
#endif

      if((histo_trigger_mask <= 1)  && alpha_chunks && gbl_alpha)
	{
	  printf("histo_read: ERROR alpha_chunks is true, but histo_trigger_mask=%d send_histo_event == %d waiteqp[HISTO]=%d\n",
		 histo_trigger_mask,send_histo_event,waiteqp[HISTO]); 
          return 0;
	}



      /* init bank structure (Type 2) */    
      bk_init32(pevent);
      // set trigger mask
      TRIGGER_MASK(pevent) = histo_trigger_mask;    
      


      if(histo_trigger_mask < 4) // send always for all modes except 2h (alpha mode in pieces). 
	// Send with 1st piece for 2h mode 
	{
	  if(dd[1])printf("creating bank CYCL\n");
	  if(small_bin_flag)
	    { 	
	      bk_create(pevent, "CYCL", TID_WORD, (void**)&qdata);
              //qstart= qdata; // save pointer
	      *qdata++ = gbl_CYCLE_N;     
	      *qdata++ = (WORD)gbl_ppg_hel; /*  ppg set value */
	      *qdata++ = (WORD)gbl_hel_readback; /* helicity readback (latched in dual channel mode) */      
	      bk_close(pevent, qdata);
	      if(dd[1])printf("after CYCL closed, bk_size(pevent)=%d\n",(INT)bk_size(pevent));
	    }
	  else
	    { 	
	      bk_create(pevent, "CYCL", TID_DWORD, (void**)&pdata);
	      *pdata++ = gbl_CYCLE_N;     
	      *pdata++ = (DWORD)gbl_ppg_hel; /*  ppg set value */
	      *pdata++ = (DWORD)gbl_hel_readback; /* helicity readback (latched in dual channel mode) */      
	      bk_close(pevent, pdata);
	    }
	}
      
#ifdef TWO_SCALERS     // alpha mode not supported for BNMR
      /* n_histo_a = 4 type 2 front,back for each hel */
      for (h=0; h < n_histo_a ; h++)
	{
	  sprintf(bank_name, "HI%s%s", h % 2 ? "F" : "B", h > 1 ? "N" : "P");
	  if(dd[1])printf("Creating bank %s\n",bank_name);
	  if(small_bin_flag)
	    {
	      bk_create(pevent, bank_name, TID_WORD, (void**)&qdata);
	      memcpy(qdata, histo[h].qh, histo[h].nbins * sizeof(WORD));
	      qdata += histo[h].nbins;  
	      bk_close(pevent, qdata);
	    }
	  else
	    {
	      bk_create(pevent, bank_name, TID_DWORD, (void**)&pdata);
	      memcpy(pdata, histo[h].ph, histo[h].nbins * sizeof(DWORD));
	      pdata += histo[h].nbins;  
	      bk_close(pevent, pdata);
	    }
	}
#endif
       if(dd[1]) 
	printf("n_histo_b=%d n_histo_a=%d\n", n_histo_b,n_histo_a);

      if(gbl_sr)
	{ // sample/reference mode; extra histos
     	  if(dd[19])printf("histo_read: sample/ref mode n_histo_b=%d \n", n_histo_b);
	  for (h=0; h < n_histo_b ; h++)
	    {
	        if(h<2)   /* Fluorescence monitors 1 and 2 (note: first channels may have SIS REF CH1 enabled) */
			sprintf(bank_name, "HMF%s", h % 2 ? "2" : "1");  // HMF1 HMF2
	     
		 else if (h<6)      /* Polarimeter counters Left and Right  HMLP HMRP HMLN HMRN */
		sprintf(bank_name, "HM%s%s", h % 2 ? "R" : "L", h > 3 ? "N" : "P");
	      else if (h<10)     /* NB backward,forwards    HMBP HMFP HMBN HMFN */
		sprintf(bank_name, "HM%s%s", h % 2 ? "F" : "B", h > 7 ? "N" : "P");

	      else if (h<14)      /* Polarimeter counters Left and Right  HSLP HSRP HSLN HSRN  sample */
		sprintf(bank_name, "HS%s%s", h % 2 ? "R" : "L", h > 11 ? "N" : "P");
	      else   if (h<18)         /* NB backward,forwards    HSBP HSFP HSBN HSFN */
		sprintf(bank_name, "HS%s%s", h % 2 ? "F" : "B", h > 15 ? "N" : "P");


	      if(dd[19]) printf("h=%d creating bank %s\n",h,bank_name);
	      if(small_bin_flag)
		{
		  bk_create(pevent, bank_name, TID_WORD, (void**)&qdata);
		  memcpy(qdata, histo[n_histo_a+h].qh, histo[n_histo_a+h].nbins * sizeof(WORD));
		  printf("adding size %d to qdata \n", histo[n_histo_a+h].nbins);
		  qdata += histo[n_histo_a+h].nbins;  
		  bk_close(pevent, qdata);		 
		}
	      else
		{
		  bk_create(pevent, bank_name, TID_DWORD, (void**)&pdata);
		  memcpy(pdata, histo[n_histo_a+h].ph, histo[n_histo_a+h].nbins * sizeof(DWORD));
		  /* printf("adding size %d to pdata \n", histo[n_histo_a+h].nbins);*/
		  pdata += histo[n_histo_a+h].nbins;  
		  bk_close(pevent, pdata);
		}
	      /*  printf("after bk_close, pevent=%p\n",pevent); */
	    }
	}
      else if (gbl_alpha)
	{
	  if(alpha_chunks)
	    { // event sent in pieces;   extra alpha histos, no Fl histos
	      //	  if(dd[19])
              INT my_chunk=0;
              if(histo_trigger_mask==2)my_chunk=1;
	      if(histo_trigger_mask==4)my_chunk=2;
	      if(histo_trigger_mask==8)my_chunk=3;
	      if(histo_trigger_mask==16)my_chunk=4;
              //printf("histo_read: qsize = qdata-qstart  %d = %p-%p \n",(qdata-qstart),qdata,qstart);
	      printf("histo_read: histo_trigger_mask=%d  n_histo_b = %d gbl_alpha=%d CHUNKS working on chunk %d\n",
		     histo_trigger_mask, n_histo_b,gbl_alpha,my_chunk);
	      if(histo_trigger_mask == (1<<1))
		{ // Piece 1  trigger_mask = 2
	      for (h=0; h < 4 ; h++)
		{    // h=1,2,3,4 pol;   no Fl histos
		  if (dd[20])printf("working on pol histos h=%d\n",h);
		  /* Polarimeter counters Left and Right */
		  sprintf(bank_name, "HM%s%s", h % 2 ? "R" : "L", h > 1 ? "N" : "P");
		  
		  // small bins only for gbl_alpha mode in chunks
		  bk_create(pevent, bank_name, TID_WORD, (void**)&qdata);
		  memcpy(qdata, histo[n_histo_a+h].qh, histo[n_histo_a+h].nbins * sizeof(WORD));
		  if (dd[20])
		    {
		      strncpy(string,bank_name,4);
		      string[4]='\0';
		      printf("histo_read: sending piece 1 mask %d bank %s : adding size %d to qdata \n", 
			     histo_trigger_mask, string, histo[n_histo_a+h].nbins);
		    }
		  qdata += histo[n_histo_a+h].nbins;  
		  bk_close(pevent, qdata);
		}
	      histo_trigger_mask = (histo_trigger_mask <<1);
             if(dd[20])  printf("histo_read:  done chunk 1, set histo_trigger_mask to %d for chunk 2\n",histo_trigger_mask);
	    } // end trigger_mask=2
	      
	      else if (histo_trigger_mask == (1<<2))
		{ // Piece 2  trigger_mask = 4
		  i=0;
		  for (h=4; h < 8 ; h++)
		    {                      //  h= 5,6,7,8 nb+/-
		      sprintf(bank_name, "HM%s%s", h % 2 ? "F" : "B", h > 5 ? "N" : "P");
		      // small bins only for gbl_alpha mode in chunks
		      bk_create(pevent, bank_name, TID_WORD, (void**)&qdata);
		      memcpy(qdata, histo[n_histo_a+h].qh, histo[n_histo_a+h].nbins * sizeof(WORD));
		       if(dd[20])                     
		      {
			strncpy(string,bank_name,4);
			string[4]='\0';
			printf("histo_read: sending piece 2 mask %d histo %s : adding size %d to qdata \n", 
			       histo_trigger_mask, string, histo[n_histo_a+h].nbins);
		      }
		      qdata += histo[n_histo_a+h].nbins;  
		      bk_close(pevent, qdata);
		    }
		  histo_trigger_mask = histo_trigger_mask <<1;
		  if(dd[20]) printf("histo_read:  done chunk 2, set histo_trigger_mask to %d for chunk 3\n",histo_trigger_mask);
		}
	      else if (histo_trigger_mask == (1<<3))
		{ // Piece 3  trigger_mask = 8
		  for (h=8; h < 12 ; h++)
		    {   // h8,9,10,11 HA+
		      j=h-8; // j=0-3 HA0P to HA3P
		      sprintf(bank_name, "HA%1.1dP", j );
		      
		      // small bins only for gbl_alpha mode in chunks
		      bk_create(pevent, bank_name, TID_WORD, (void**)&qdata);
		      memcpy(qdata, histo[n_histo_a+h].qh, histo[n_histo_a+h].nbins * sizeof(WORD));
		         if (dd[20])
			{
			  strncpy(string,bank_name,4); 
			  string[4]='\0';
			  
			  printf("histo_read: sending piece 3 mask %d histo %s : adding size %d to qdata \n", 
				 histo_trigger_mask, string, histo[n_histo_a+h].nbins);
			}
		      qdata += histo[n_histo_a+h].nbins;  
		      bk_close(pevent, qdata);
		    }
		  histo_trigger_mask = histo_trigger_mask <<1;
		  if(dd[20]) printf("histo_read:  done chunk 3, setting histo_trigger_mask to %d for chunk 4\n",histo_trigger_mask);

		}
	      
	      else if (histo_trigger_mask == (1<<4))
		{ // Piece 4  trigger_mask = 16
		  for (h=12; h < n_histo_b  ; h++)   // n_histo_b should be 16
		    {   // h12,13,14,15 HA-
		      j=h-12; // j=0-4
		      sprintf(bank_name, "HA%1.1dN", j);
		      
		      bk_create(pevent, bank_name, TID_WORD, (void**)&qdata);
		      memcpy(qdata, histo[n_histo_a+h].qh, histo[n_histo_a+h].nbins * sizeof(WORD));
		      if(dd[20])
			{
			  strncpy(string,bank_name,4);
			  string[4]='\0';
			  printf("histo_read: sending piece 4 mask %d histo %s : adding size %d to qdata \n", 
				 histo_trigger_mask, string, histo[n_histo_a+h].nbins);
			}
		      qdata += histo[n_histo_a+h].nbins;  
		      bk_close(pevent, qdata);
		    }
		  if(dd[20])printf("histo_read:  done chunk 4, clearing histo_trigger_mask to 0 \n");

		  histo_trigger_mask = 0; // clear
		}
	      else
		{
		  printf("histo_read : Illegal trigger mask %d\n",histo_trigger_mask);
		  goto ret;
		}
	    } // end of Mode 2h ALPHA with histo pieces


	  else
	    { //  Mode 2h ALPHA histo sent in one piece. DWORD bins

	      printf("histo_read: histo_trigger_mask=%d  n_histo_b = %d gbl_alpha=%d ONE piece\n",
		     histo_trigger_mask, n_histo_b,gbl_alpha);
	      
	      for (h=0; h < 4 ; h++)
		{    // h=1,2,3,4 pol;   no Fl histos
		  if (dd[20])printf("working on pol histos h=%d\n",h);
		  /* Polarimeter counters Left and Right */
		  sprintf(bank_name, "HM%s%s", h % 2 ? "R" : "L", h > 1 ? "N" : "P");
		  
		  // large bins only for gbl_alpha mode in one piece
		  bk_create(pevent, bank_name, TID_DWORD, (void**)&pdata);
		  memcpy(pdata, histo[n_histo_a+h].ph, histo[n_histo_a+h].nbins * sizeof(DWORD));
		  if (dd[20])
		    {
		      strncpy(string,bank_name,4);
		      string[4]='\0';
		      printf("histo_read: mask %d bank %s : adding size %d to pdata \n", 
			     histo_trigger_mask, string, histo[n_histo_a+h].nbins);
		    }
		  pdata += histo[n_histo_a+h].nbins;  
		  bk_close(pevent, pdata);
		}
	   
	    
	      i=0;
	      for (h=4; h < 8 ; h++)
		{                      //  h= 5,6,7,8 nb+/-
		  sprintf(bank_name, "HM%s%s", h % 2 ? "F" : "B", h > 5 ? "N" : "P");
		  // large bins only for gbl_alpha mode one piece
		  bk_create(pevent, bank_name, TID_DWORD, (void**)&pdata);
		  memcpy(pdata, histo[n_histo_a+h].ph, histo[n_histo_a+h].nbins * sizeof(DWORD));
		  if(dd[20])
		    {
		      strncpy(string,bank_name,4);
		      string[4]='\0';
		      printf("histo_read: mask %d histo %s : adding size %d to pdata \n", 
			     histo_trigger_mask, string, histo[n_histo_a+h].nbins);
		    }
		  pdata += histo[n_histo_a+h].nbins;  
		  bk_close(pevent, pdata);
		}
	     
	      for (h=8; h < 12 ; h++)
		{   // h8,9,10,11 HA+
		  j=h-8; // j=0-3 HA0P to HA3P
		  sprintf(bank_name, "HA%1.1dP", j );
		      
		  // large bins only for gbl_alpha mode one piece
		  bk_create(pevent, bank_name, TID_DWORD, (void**)&pdata);
		  memcpy(pdata, histo[n_histo_a+h].ph, histo[n_histo_a+h].nbins * sizeof(DWORD));
		  if (dd[20])
		    {
		      strncpy(string,bank_name,4); 
		      string[4]='\0';
			  
		      printf("histo_read: mask %d histo %s : adding size %d to pdata \n", 
			     histo_trigger_mask, string, histo[n_histo_a+h].nbins);
		    }
		  pdata += histo[n_histo_a+h].nbins;  
		  bk_close(pevent, pdata);
		}

	      for (h=12; h < n_histo_b  ; h++)
		{   // h12,13,14,15 HA-
		  j=h-12; // j=0-3  HA0N to HA3N
		  sprintf(bank_name, "HA%1.1dN", j);
		      
		  bk_create(pevent, bank_name, TID_DWORD, (void**)&pdata);
		  memcpy(pdata, histo[n_histo_a+h].ph, histo[n_histo_a+h].nbins * sizeof(DWORD));
		  if(dd[20])
		    {
		      strncpy(string,bank_name,4);
		      string[4]='\0';
		      printf("histo_read: mask %d histo %s : adding size %d to pdata \n", 
			     histo_trigger_mask, string, histo[n_histo_a+h].nbins);
		    }
		  pdata += histo[n_histo_a+h].nbins;  
		  bk_close(pevent, pdata);
		}
	    
	    }//  end Mode 2h ALPHA histo sent in one piece. DWORD bins
	} // end gbl_alphs
      else
	{  // original  h=1,2 fluor; h=3,4,5,6 pol; 7,8,9,10 nb+/-; 
	  for (h=0; h < n_histo_b ; h++)
	    {
	      //printf("working on h=%d\n",h);
	      if(h<2)   /* Fluorescence monitors 1 and 2 (note: first channels may have SIS REF CH1 enabled) */
		sprintf(bank_name, "HMF%s", h % 2 ? "2" : "1");
	      else if (h<6)      /* Polarimeter counters Left and Right */
		sprintf(bank_name, "HM%s%s", h % 2 ? "R" : "L", h > 3 ? "N" : "P");
	      else if (h<10)
		sprintf(bank_name, "HM%s%s", h % 2 ? "F" : "B", h > 7 ? "N" : "P");
	     
	    
	        if(dd[19]) 
		printf("Creating bank %s at h=%d (small bin flag=%d)\n",bank_name,h,small_bin_flag);
	      if(small_bin_flag)
		{
		  bk_create(pevent, bank_name, TID_WORD, (void**)&qdata);
		  memcpy(qdata, histo[n_histo_a+h].qh, histo[n_histo_a+h].nbins * sizeof(WORD));
		  //printf("adding size %d to qdata \n", histo[n_histo_a+h].nbins);
		  qdata += histo[n_histo_a+h].nbins;  
		  bk_close(pevent, qdata);
		}
	      else
		{
		  bk_create(pevent, bank_name, TID_DWORD, (void**)&pdata);
		  memcpy(pdata, histo[n_histo_a+h].ph, histo[n_histo_a+h].nbins * sizeof(DWORD));
		  /* printf("adding size %d to pdata \n", histo[n_histo_a+h].nbins);*/
		  pdata += histo[n_histo_a+h].nbins;  
		  bk_close(pevent, pdata);
		}
	      /*  printf("after bk_close, pevent=%p\n",pevent); */
	    } // for loop
	}
      
    } /* end of TD type modes */
  else 
    {/* For I type modes */
      /* init bank structure */
      /* printf("initializing bank structure for type 1\n"); */
      
      // set trigger mask
      TRIGGER_MASK(pevent) = histo_trigger_mask;
      
      bk_init32(pevent);
      bk_create(pevent, "CYCL", TID_DWORD, (void**)&pdata);
#ifdef SPEEDUP
      if(const1f_flag)
	{
	  *pdata++ = gbl_CYCLE_H;    /* word 1 cycle number of processed histos */
	  *pdata++ = gbl_SCYCLE_H;   /* word 2 */
	  *pdata++ = gbl_SCAN_H;     /* word 3 */
	  *pdata++ = (DWORD)gbl_helset_H;        /* word 4  stored ppg set value */
	} 
      else
	{
	  *pdata++ = gbl_CYCLE_N;    /* word 1 present cycle */
	  *pdata++ = gbl_SCYCLE_N;   /* word 2 */ 
	  *pdata++ = gbl_SCAN_N;     /* word 3 */
	  *pdata++ = (DWORD)gbl_ppg_hel;        /* word 4  use ppg set value */
	}
#else   
      *pdata++ = gbl_CYCLE_N;    /* word 1 present cycle */
      *pdata++ = gbl_SCYCLE_N;   /* word 2 */  
      *pdata++ = gbl_SCAN_N;     /* word 3 */
      *pdata++ = (DWORD)gbl_ppg_hel;        /* word 4  use ppg set value */
#endif      
      if(dd[18])printf("histo_read: CYCL bank gbl_CYCLE_N=%d gbl_CYCLE_H=%d\n",gbl_CYCLE_N,gbl_CYCLE_H);
      /* Use next value as flag to indicate what kind of data 
	 is in this bank 
	 gbl_scan_flag has been set up at begin_of_run
	 
	 gbl_scan_flag = 0 freq scan 1f,1g,1a,1b
	 = 1 NaCell 1n
	 = 2 Laser  1d
	 = 
	 = 4 dummy  10
	 = 0x100 (256) CAMP frq 1c 
	 = 0x200 (512) CAMP Mag 1c 
	 = 0x400 (1024)CAMP DAC (POL) 
	 = 0x800 (2048)1a/1b with randomized frequencies (BNMR/BNQR)
	 = 0x1000 1g with flip (helicity pairs)
	 = 0x10 1f with randomized frequencies
      */
      *pdata++ = gbl_scan_flag;  /* word 5 */
#ifdef EPICS_ACCESS      
      if(epics_flag)  /* epics scan flag */
	{
	  /* Epics scan value */
	  *pdata++ = (DWORD) (epics_params.Epics_read*1000);   /* word 6 EPICS value read */
	  *pdata++ = (DWORD) (epics_params.Epics_val*1000);    /* word 7 EPICS value written */
	  *pdata++ = 0;  /* word 8  clear CAMP or PSM freq value */
	}
      else
#endif
	{ /* NOT EPICS scan; clear EPICS parameters */
	  *pdata++ =0; /*  word 6 */
	  *pdata++ =0; /*  word 7 */
	  
	  if(mode10_flag)
	    *pdata++ =0 ;  /* word 8; nothing is actually being scanned */
#ifdef CAMP_ACCESS
	  else if(camp_flag)
	    {
	      INT value;
	      value =  (DWORD) ((set_camp_val * camp_params.conversion_factor) +0.49);
	      if(dc)
		printf("\n +++ CAMP +++gbl_scan_flag=%d,set_camp_val=%f, factor=%d, value=%d\n",
		       gbl_scan_flag,set_camp_val, camp_params.conversion_factor, value);
	      *pdata++ = value; /* word 8; store camp setpoint */
	    }
#endif
	  else
	    {  /* 1f, 1g, 1a, 1b etc */
	      if(dd[10])
		printf("\n +++ FREQ +++gbl_scan_flag=%d,freq_val=%u\n",
		       gbl_scan_flag,freq_val);
              if(dd[18])
		printf("\n CYCL bank created with gbl_CYCLE_N=%d freq_val=%u  gbl_freq_H=%u gbl_CYCLE_H=%d\n",
		       gbl_CYCLE_N,freq_val,gbl_freq_H,gbl_CYCLE_H);
#ifdef SPEEDUP
	      if(const1f_flag)
		{
		  //printf("histo_read: storing freq as gbl_freq_H =%u\n",gbl_freq_H);
		  *pdata++ = gbl_freq_H;    /* word 8; store PSM frequency value that goes with these histograms */
		  *pdata++ = gbl_helread_H; // word 9; stored helicity readback
		}
	      else
		{
		  *pdata++ = freq_val;    /* word 8; store current PSM frequency value */
		  *pdata++ = (DWORD)gbl_hel_readback; // add word 9  helicity readback
		}
#else   
	      *pdata++ = freq_val;    /*  word 8; store current PSM frequency value */      
	      *pdata++ = (DWORD)gbl_hel_readback; // add word 9  helicity readback

#endif 
	      
	    }
	}
      bk_close(pevent, pdata);
      
#ifdef TWO_SCALERS
      for (h=0; h < n_histo_a - 1 ; h++)
	{
	  sprintf(bank_name, "HI%sP", h % 2 ? "F" : "B");
	  bk_create(pevent, bank_name, TID_DWORD, (void**)&pdata);
	  memcpy(pdata, histo[h].ph, histo[h].nbins * sizeof(DWORD));
	  pdata += histo[h].nbins;  
	  bk_close(pevent, pdata);
	}
      if(fill_uarray)
	{
	  fill_uarray = FALSE;
	  bk_create(pevent,"UBIT", TID_DWORD,  (void**)&pdata);
	  memcpy(pdata,  histo[n_histo_a-1].ph , histo[n_histo_a-1].nbins * sizeof(DWORD));
	  pdata += histo[n_histo_a-1].nbins;  
	  bk_close(pevent, pdata);
	}
      nhb=n_histo_b;    /* TWO SCALERS */
#else         
      nhb= n_histo_b-1 ; /* ONE SCALER - the last histo is now the UBIT */
#endif
      for (h=0; h < nhb ; h++)
	{
	  sprintf(bank_name, "HM%2.2d", h);
	  bk_create(pevent, bank_name, TID_DWORD,  (void**)&pdata);
	  memcpy(pdata, histo[n_histo_a+h].ph, histo[n_histo_a+h].nbins * sizeof(DWORD));
	  pdata += histo[n_histo_a+h].nbins;
	  
#ifdef BINCHECK
	  if(h==0 && const1f_flag) // SPEEDUP, BINCHECK only in const1f mode
	    {
	      if(bincheck(h) != SUCCESS)
		{
		  skip_cycle=TRUE;
		  goto ret; // don't send the event
		}
	    }

#endif
	  
	  bk_close(pevent, pdata);
	}
      
#ifndef TWO_SCALERS  /* ONE_SCALER: now fill user bit array for Scaler B  */
      if(fill_uarray)
	{
	  fill_uarray = FALSE; /* probably we have UBIT1 only once per run */
	  bk_create(pevent,"UBIT", TID_DWORD, (void**) &pdata);
	  memcpy(pdata,  histo[n_histo_b-1].ph , histo[n_histo_b-1].nbins * sizeof(DWORD));
	  pdata += histo[n_histo_b-1].nbins;  
	  bk_close(pevent, pdata);
	}
#endif
      
    } /* end of I-type modes */
      /* inform that the histo bank has been sent out */
  
     
 if(gbl_alpha && alpha_chunks)
   {
     if (histo_trigger_mask == 1 || histo_trigger_mask == 0)
       {  // Last piece sent
	  if(dd[20])printf("histo_read: ***  last histogram piece sent  histo_trigger_mask=%d\n",histo_trigger_mask);
	 last_piece_sent=1;
       }
   }
#ifdef SPEEDUP
     if(const1f_flag)
       {
	 if(dd[18]) 
	   printf("\n *** histo_read: histograms have been sent out for cycle %d; clearing gbl_CYCLE_H\n", gbl_CYCLE_H);
	 gbl_CYCLE_H=0; // clear
#ifdef HAVE_NIMIO32
      nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT5 ); // Clear OUTPUT5 (sending histograms)
#endif
    }
#endif // SPEEDUP  Type 1 histos are cleared in histo_read; Type 2 are cumulative
 
  //printf("histo_read: setting waiteqp[HISTO] false\n");
  //  if (dd[1])  
    if (histo_trigger_mask == 1 || histo_trigger_mask == 0)
      {
	waiteqp[HISTO] = FALSE;
	send_histo_event=0;
      }
       if(dd[18])
      printf("histo_read: sending event of bksize:%d ... histo_trigger_mask=%d send_histo_event=%d waiteqp[HISTO]=%d\n", 
	     bk_size(pevent),histo_trigger_mask, send_histo_event, waiteqp[HISTO]);
    //  printf("histo_read: sending out event of size bksize:%d  histo_trigger_mask=%d waiteqp[HISTO]=%d\n",
    //bk_size(pevent),histo_trigger_mask, waiteqp[HISTO]);

    return bk_size(pevent);

 ret:
    send_histo_event=0;
  waiteqp[HISTO] = FALSE;
  gbl_CYCLE_H=0;  // clear
  return 0;
    }

/*-- Event readout -------------------------------------------------*/
INT scalers_cycle_read(char *pevent, INT off)
/* - periodic equipment reading the scalers sum over a cycle
   - generate event only when cycle has been completed 
   and cycle is !skip_cycle
*/
{

  double *pdata;

  float fTemp;
  double sum_f, sum_b; /*temporary variables */
  INT    i,offset,offsetb;


#ifdef TWO_SCALERS
  INT    back_cyc,front_cyc,ratio_cyc,asym_cyc;
  double sum_all, dTemp;
#endif


  if (!waiteqp[FIFO] && waiteqp[CYCLE])
  {

    if (dd[1])
    printf("scalers_cycle_read %d \n ", waiteqp[FIFO]);
    bk_init(pevent);



    bk_create(pevent, "HSCL", TID_DOUBLE,  (void**) &pdata);
    
    /* Note: for experiment 1A/1B there are  software scalers filled in 
       directly in scaler_increment */
    
    /* add in any software scalers here (e.g. sums of inputs) */
/*
  !!!!!!!!!!!!!!!!!!!!!!!!        NOTE           !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!    Note: these must be filled into pdata in the SAME ORDER as the INDEX  or   !!!!!
  !!    they will be muddled up in odb & scaler event                              !!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/  
#ifdef TWO_SCALERS
    offset = 0; /* calculate the offset due to scalerA cycle counters */
    if(exp_mode == 1) {
      offset = n_scaler_real+NA1_CYCLE_SCALER;
      back_cyc = offset + NA_BACK_CYC;
      front_cyc = offset + NA_FRONT_CYC;
      ratio_cyc = offset + NA_RATIO_CYC;
      asym_cyc = offset + NA_ASYM_CYC;
     

      /* All Back */
      scaler[back_cyc].sum_cycle = scaler[BACK_USB0].sum_cycle 
	+ scaler[BACK_USB1].sum_cycle
	+ scaler[BACK_USB2].sum_cycle + scaler[BACK_USB3].sum_cycle;
      
      /* All front */
      scaler[front_cyc].sum_cycle  = scaler[FRONT_USB0].sum_cycle 
	+ scaler[FRONT_USB1].sum_cycle
	+ scaler[FRONT_USB2].sum_cycle + scaler[FRONT_USB3].sum_cycle;
      
      /* Front + back */
      sum_all = scaler[back_cyc].sum_cycle
	+ scaler[front_cyc].sum_cycle; 

        remember_sum_cycle=(float)sum_all;
     if(dd[3])
       printf("scalers_cycle_read: TWO_SCALERS Type 1 sum_all = %f remember_sum_cycle=%f\n",(float)sum_all,remember_sum_cycle);
    
      /* Asymmetry (back-front) / (back+front) */
      if ( sum_all > 0)
	scaler[asym_cyc].sum_cycle = 
	  (scaler[back_cyc].sum_cycle  - scaler[front_cyc].sum_cycle)/ 
	  sum_all;
      else
	scaler[asym_cyc].sum_cycle = -99999. ; // was -88888.
      offset += NA_CYCLE_SCALER;


    } else  {/* exp_mode = 2 */
      offset = n_scaler_real;
      back_cyc = offset + NA_BACK_CYC;
      front_cyc = offset + NA_FRONT_CYC;
      ratio_cyc = offset + NA_RATIO_CYC;
      asym_cyc = offset + NA_ASYM_CYC;

#ifdef THIRTYTWO
      /* sum of back (0...15) per cycle  */
      scaler[back_cyc].sum_cycle=0.;          /* clear scaler sum */
      for (i=0 ; i < 16  ; i++) 
	scaler[back_cyc].sum_cycle += scaler[i].sum_cycle;

      /* sum of front (16...31) per cycle  */
      scaler[front_cyc].sum_cycle=0.;          /* clear scaler sum */
      for (i=16 ; i < 32  ; i++)
	scaler[front_cyc].sum_cycle += scaler[i].sum_cycle;
#else // TWO channels
      scaler[back_cyc].sum_cycle = scaler[0].sum_cycle;
      scaler[front_cyc].sum_cycle += scaler[1].sum_cycle;
#endif //  THIRTYTWO
      /* Front + back */
      sum_all = scaler[back_cyc].sum_cycle
	+ scaler[front_cyc].sum_cycle; 
      remember_sum_cycle=sum_all;
    if(dd[3])  
      printf("scalers_cycle_read: TWO_SCALERS Type 2 sum_all = %f, remember_sum_cycle=%f\n",(float)sum_all,remember_sum_cycle);
  
      /* Asymmetry (back-front) / (back+front) */
      if ( sum_all > 0)
	scaler[asym_cyc].sum_cycle = 
	  (scaler[back_cyc].sum_cycle  - scaler[front_cyc].sum_cycle)/ 
	  sum_all;
      else
	scaler[asym_cyc].sum_cycle = -99999.; // was -88888. 

      /* Cycle Ratio BACK / FRONT */
      if (scaler[front_cyc].sum_cycle > 0)
	{
	  scaler[ratio_cyc].sum_cycle = scaler[back_cyc].sum_cycle 
	    / scaler[front_cyc].sum_cycle;
	}
      else
	scaler[ratio_cyc].sum_cycle = -99999.; // was 77777

      offset += NA_CYCLE_SCALER;
    }
#else 
/* one scaler */
    offset = n_scaler_real;
#endif	
 
      /* Polarimeter counters  (ch 2 & 3 of scaler B ) */
      scaler[NB_POL_CYC+offset].sum_cycle = scaler[POL_CHAN1 - ECL_OFFSET].sum_cycle + scaler[POL_CHAN2 - ECL_OFFSET].sum_cycle; 
#ifndef TWO_SCALERS  // remember the counts for threshold, bnqr only
   remember_sum_cycle=(float)scaler[NB_POL_CYC+offset].sum_cycle;
     if(dd[3])
       printf("scalers_cycle_read: polarimeter counters sum_cycle=%f remember_sum_cycle=%f \n", 
	      (float)scaler[NB_POL_CYC+offset].sum_cycle, remember_sum_cycle);
#endif   
    if( scaler[NB_POL_CYC+offset].sum_cycle >0)
      scaler[NB_POL_ASYM+offset].sum_cycle =
	(scaler[POL_CHAN1 - ECL_OFFSET].sum_cycle - scaler[POL_CHAN2 - ECL_OFFSET].sum_cycle)/scaler[NB_POL_CYC+offset].sum_cycle;
    else
      scaler[NB_POL_ASYM+offset].sum_cycle = -99999.; // was -66666.
    
    /* Calculate the sums of all 8 neutral beam counters
       sum per cycle  */
    sum_f=sum_b=0.;  /* clear scaler sums */
    for (i=0 ; i < NUM_NEUTRAL_BEAM  ; i++)
      {
	sum_b += scaler[NEUTRAL_BEAM_B  - ECL_OFFSET + i ].sum_cycle;
	sum_f += scaler[NEUTRAL_BEAM_F  - ECL_OFFSET + i ].sum_cycle;
      } 
    scaler[NB_NB_CYC+offset].sum_cycle = sum_b+sum_f;
    if( scaler[NB_NB_CYC+offset].sum_cycle >0)
      scaler[NB_NB_ASYM+offset].sum_cycle =
	(sum_f-sum_b)/scaler[NB_NB_CYC+offset].sum_cycle;
    else
      scaler[NB_NB_ASYM+offset].sum_cycle = -99999.; // was -55555.
    offsetb = offset; 


#ifdef TWO_SCALERS
    if(exp_mode == 2) {
      /* ----------- Start doing cumulative calculations --------------*/      
      if(gbl_ppg_hel == HEL_UP) /* use set value */
	offset += NB_CYCLE_SCALER ;
      else
	offset += (NB_CYCLE_SCALER + 4);
      if(!skip_cycle) {
	scaler[offset + BACK_CUM].sum_cycle += scaler[back_cyc].sum_cycle;
	scaler[offset + FRONT_CUM].sum_cycle += scaler[front_cyc].sum_cycle;
	/* cumulative asymmetry (over run) (back-front) / (back+front) */
	if( (scaler[offset + BACK_CUM].sum_cycle 
	     + scaler[offset + FRONT_CUM].sum_cycle) >  0 )
	  {
	    dTemp = (float) ( (scaler[offset + BACK_CUM].sum_cycle -
			       scaler[offset + FRONT_CUM].sum_cycle) / 
			      (scaler[offset + BACK_CUM].sum_cycle +
			       scaler[offset + FRONT_CUM].sum_cycle) );
	  }
	else
	  {
	    if(dd[1]) printf("Setting cumulative asymmetry to -99999 (zero data)\n");
	    dTemp = -99999;
	  }
	scaler[offset + ASYM_CUM].sum_cycle = dTemp; 
	
	/* Cumulative Ratio BACK / FRONT */
	if (scaler[offset + FRONT_CUM].sum_cycle > 0)
	  {
	    dTemp = scaler[offset+BACK_CUM].sum_cycle / 
	      scaler[offset+FRONT_CUM].sum_cycle;
	  }
	else
	  dTemp = -99999;
	scaler[offset+RATIO_CUM].sum_cycle = dTemp;
      }
    }
#endif    
    
    /* Add all the defined cycle scalers to the event */
    for (i=0 ; i < n_scaler_total ; i++)
      *pdata++ = scaler[i].sum_cycle;
   
    bk_close(pevent, pdata);
    
    /* Add to Epics  (fe_epics) (BNMR.BNQR only) */
    if (hEPD)
      {
	  
#ifdef TWO_SCALERS
	  /* all back  in Epics var[2] */
	  fTemp = (float) scaler[back_cyc].sum_cycle;
          db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), 2, TID_FLOAT);
	  
	  /* All front in Epics var[3]  */
	  fTemp = (float) scaler[front_cyc].sum_cycle ;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), 3, TID_FLOAT);

	  /* Front + back in Epics var[0] */
	  fTemp = (float)sum_all;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), 0, TID_FLOAT);


	  /*Asymmetry (back-front) / (back+front)  in Epics var[1]  */
	  fTemp =  (float)scaler[asym_cyc].sum_cycle;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), 1, TID_FLOAT);
	

	  /* Back/Front in Epics var[4]  */
	  fTemp = (float) (scaler[ratio_cyc].sum_cycle) ;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), 4, TID_FLOAT);
	
#endif 
	  /* 01-May-2001 RP : Send scalerB,ch 0  sum to Epics (fe_epics)  */
	  /* scalerB,PolLeft to Epics in var[A] */
	  fTemp = (float)scaler[POL_CHAN1 - ECL_OFFSET].sum_cycle ;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A, TID_FLOAT);
	  /* scalerB, PolRight to Epics in var[A+1] */
	  fTemp = (float)scaler[POL_CHAN2 - ECL_OFFSET].sum_cycle ;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A+1, TID_FLOAT);
	  
	  /* scalerB,ch 2+ch 3  Pol sum to Epics in var[A+2] */
	  fTemp = (float) scaler[offsetb + NB_POL_CYC].sum_cycle ;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A+2, TID_FLOAT);
	  /* scalerB, (ch 2-3)/ch 2+3) Pol asym to Epics in var[A+3] */
	  fTemp = (float) scaler[offsetb +NB_POL_ASYM].sum_cycle ;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A+3, TID_FLOAT);
	  
	  /* 
	     Add Scaler B Neutral Beam  monitors to Epics (fe_epics)
	  */
	  /* Neutral Beam Backward Sum to Epics in var[A+4] */
	  fTemp = (float)sum_b ;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A+4, TID_FLOAT);
	  
	  /* Neutral Beam Forward Sum to Epics in var[A+5]  */
	  fTemp = (float)sum_f ;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A+5, TID_FLOAT);
	  
	  /* Neutral Beam sum to Epics in var[A+6] */
	  fTemp = (float) scaler[offsetb + NB_NB_CYC].sum_cycle;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A+6, TID_FLOAT);
	  
	  /* Neutral Beam assymetry to Epics in var[A+7]*/
	  fTemp = (float) scaler[offsetb +NB_NB_ASYM].sum_cycle;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A+7, TID_FLOAT);
	  
	  /* Cycle # to Epics in var[A+9] */
	  fTemp = (float) gbl_CYCLE_N;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A+9, TID_FLOAT);
      }
    
      waiteqp[CYCLE] = FALSE;
      
      if (dd[1]) printf("bksize:%d\n",bk_size(pevent));


      if(skip_cycle)
	{
	  if (dd[1]) printf("scalers_cycle_read: skip_cycle=TRUE returning event length=0\n");
	  return 0;  /* No bank generated when skip_cycle is TRUE */
	}

      if(dd[1])printf("scalers_cycle_read: returning event length=%d\n",bk_size(pevent));
      return bk_size(pevent);
  }
  else
    return 0;    
}

/*-- Event readout -------------------------------------------------*/
INT info_odb(char * pevent, INT off)
/* - periodic equipment updating the ODB ONLY
   - no event generation for the data stream.
*/
{  
  // const1f mode...  send only during the cycle to avoid slowing down readout
  if(const1f_flag &&  !gbl_IN_CYCLE)
    return 0;

  cyinfo.helicity_set =  gbl_ppg_hel; /* Set value */
  cyinfo.helicity_read = gbl_hel_readback ; /* readback value */
  cyinfo.current_cycle = gbl_CYCLE_N;
  cyinfo.current_scan = gbl_SCAN_N;
#ifdef EPICS_ACCESS

  if(epics_flag)
    {
      cyinfo.epicsdev_set_v_ = epics_params.Epics_val;   /* epics scan */
      cyinfo.epicsdev_read_v_ = epics_params.Epics_read; /* epics scan */
    }
  else
#endif
    {
      cyinfo.epicsdev_set_v_ = 0;   /* epics scan */
      cyinfo.epicsdev_read_v_ = 0; /* epics scan */
    }
  if(!camp_flag)
    {
      cyinfo.campdev_set = 0;   /* camp scan */
      cyinfo.campdev_read = 0; /* camp scan */
    }
  if(mode1f_flag)
    cyinfo.rf_set_1f = (INT)freq_val; // current frequency value
  else
    cyinfo.rf_set_1f = 0;

  if(const1f_flag)
    {
#ifdef HAVE_NIMIO32
      cyinfo.const1f_ppg_cb_cntr = gbl_ppg_cb_cntr1;
      cyinfo.const1f_cb_cntr = gbl_cb_cntr1;
      if(gbl_CYCLE_N > 0)
	cyinfo.const1f_percent = ((float)gbl_ppg_cb_cntr1/ (float)gbl_CYCLE_N) * 100.0;
#else
      cyinfo.const1f_ppg_cb_cntr= cyinfo.const1f_cb_cntr = cyinfo.const1f_percent = -1; // no info available
#endif //  HAVE_NIMIO32
 
      // Computer Busy Timer
      cyinfo.const1f_max_cb__ms_ =  gbl_maxElapsedTime;
      cyinfo.const1f_min_cb__ms_ =  gbl_minElapsedTime;
      int i=  (int)  (0.5 + gbl_sumElapsedTime / gbl_sumNum); // round to nearest integer

      cyinfo.const1f_av_cb__ms_  =  (double)i;
    }
  else
    { // other modes
      cyinfo.const1f_ppg_cb_cntr= cyinfo.const1f_cb_cntr =  cyinfo.const1f_percent = 0;
      cyinfo.const1f_max_cb__ms_ =  cyinfo.const1f_min_cb__ms_ =  cyinfo.const1f_av_cb__ms_ =0;
    }
  if(gbl_sr)
    {
      cyinfo.microwave_set =  gbl_set_sample; // sample/ref mode  sets microwaves on/off
      cyinfo.microwave_read = gbl_read_sample;
      // printf("info_odb: microwave_set=%d and read=%d\n", cyinfo.microwave_set,  cyinfo.microwave_read);
    }


  memcpy(pevent, (char *)&(cyinfo.helicity_set), sizeof(cyinfo));
  pevent += sizeof(cyinfo);
  if (dd[2]) printf ("info_odb %d size:%d\n",gbl_CYCLE_N,sizeof(cyinfo));
  return sizeof(cyinfo);
}

#include "scaler_increment_nopulsepair.c" 
#include "scaler_increment_pulsepair.c"


/*-- Histo Event  ---------------------------------------------------------*/
INT histo_process(void)
/*------------------------------------------------------------------------*/
/*
  Based on the helicity do:
  - check threshold if > 0
    threshold = sum of 32 scalers under this helicity
    check versus cycle_thr
      if > don't include cycle
      if < increment histo[x] form scaler[y] see comment below 
      Type 1:
           - 2 scalers: sum scaler A  0-15  to h0  (only if THIRTYTWO is defined) 
           - 2 scalers: sum scaler A  16-31  to h1 (only if THIRTYTWO is defined)
	   - 2 scalers: h2 filled in scaler_increment (type 1)

	   - sum scaler B ch 0,1 to h1,2 (fluorescence monitors or SIS Ref Ch1 may be enabled)
	   - sum scaler B ch 2,3 to h3,4 (pol monitors )
	   - sum scaler B ch 4-7  to h5 (backward  neutral beam counters)
	   - sum scaler B ch 8-11 to h6 (forward neutral beam counters)
	   - 1 scaler ONLY:  h7 filled in scaler_increment (type 1)

*/
{
#ifdef TWO_SCALERS
#ifdef THIRTYTWO // 32 inputs 
  INT h;
#endif
#endif // TWO_SCALERS
    INT i, j;
#ifdef BNMR
  DWORD async_data=0;
#endif
  INT status;
  float   end_value;
  float ref_value;

  INT hel_offset,type_offset;
  //  INT offset, back_cyc,front_cyc;
  INT sample_offset; // sample/ref mode
  INT alpha_hel_offset; // alpha helicity offset
  INT joffset,hindex,sindex;

  static struct timeval t1,t2;


  double elapsed_time;
  gettimeofday(&t1, NULL); // time how long histo_process takes

    if(dd[1]) 
  printf("Entering histo_process routine with waiteqp[HISTO]=%d and histo_process_flag=%d\n",
	 waiteqp[HISTO],histo_process_flag);

  if(dd[18])printf("histo_process: starting with gbl_CYCLE_N=%d freq step=%d\n",gbl_CYCLE_N, gbl_FREQ_n);

  /* Check if someone has set skip_cycle to TRUE before  */
  if(skip_cycle){
    printf("\n skip_cycle is TRUE when entering routine histo_process... ");
    if(const1f_flag)
	printf("PPG computer busy unexpectedly off at end of cycle\n");
    else
      printf("probably run is being stopped \n");
    cyinfo.cancelled_cycle ++;
    status=1;
    goto ret;
  }

  if(first_cycle){
    printf("\n Histo process: this is the first cycle of the run - ignore it \n");

  
    //#ifdef BINCHECK
    //  if(const1f_flag) // 1f with constant step
    //	fsum=scaler[FLUOR_CHAN1].sum_cycle; // this is the sum Ch1. Not used for checking any more  double fsum
    //#endif
   
    if(dd[11])printf("\n\n ** Note: progress display is turned off by default **\n\n");

    skip_cycle = TRUE;
    first_cycle = FALSE;

    cyinfo.cancelled_cycle ++;
    cyinfo.current_heldown_thr=cyinfo.current_helup_thr=0.0; /* make sure prev. threshold gets 
								set to 0 first time through */
    status=1;
    goto ret;
  }

  /* check if the run has been put on hold (using (hot-linked) BNMR hold flag)  */
  if (fs.flags.hold)
  {
    if (!hold_flag)
    {
      if(dd[1]) printf("BNMR Hold flag is detected; no histograms will be sent\n");
      cm_msg(MINFO,"histo_process","Run is on hold. No histograms will be sent.");
    }
   
    hold_flag = TRUE;
    skip_cycle = TRUE;
    cyinfo.cancelled_cycle ++;
    status=1;
    goto ret;
  }
  else
  {
    if (hold_flag)
    {
      if(dd[1]) printf("Run is being resumed; histograms will now be sent\n");
      cm_msg(MINFO,"histo_process","Run is resumed. Histograms will now be sent.");
    }
    hold_flag= FALSE;
  }

  /* put these tests in particularly for dual_channel_mode */
#ifdef HAVE_NIMIO32
  gbl_hel_readback =  hel_read_ioreg(FALSE ); // read the helicity (dual channel mode returns latched values);
  if(fs.hardware.enable_all_hel_checks)
    {
      if(gbl_hel_readback > 1)
	{
	  cm_msg(MERROR,"histo_process","skipping cycle because gbl_hel_readback is illegal %d (%s)",gbl_hel_readback,helicity_state(gbl_hel_readback));
	  skip_cycle = TRUE;
	  cyinfo.cancelled_cycle ++;
	  gbl_hel_mismatch_flag=1; // set a flag for recover_helicity()
	  status=1;
	  goto ret;
	}

      if(gbl_ppg_hel != gbl_hel_readback  ||  hot_threshold_trip == 3)
	{
	  if(hot_threshold_trip == 3)
	    printf("histo_process: hot_threshold_trip=3, failing helicity mismatch test, skipping cycle\n");
	  else
	    {
	      cm_msg(MERROR,"histo_process","gbl_hel_readback %d is not the same as set value %d",gbl_hel_readback,gbl_ppg_hel);
	      cm_msg(MINFO,"histo_process","skipping cycle because helicity readback (%d) is not the same as set value %d",
		     gbl_hel_readback,gbl_ppg_hel);
	    }
	  skip_cycle = TRUE;
     
	  cyinfo.wrong_hel_cntr++;
	  cyinfo.cancelled_cycle++;
	  gbl_hel_mismatch_flag=1; // set a flag for recover_helicity()
	  status=1;
	  goto ret;
	}
    } // hel checks enabled
#endif //  HAVE_NIMIO32
     
 // Successfully passed helicity mismatch
 if(gbl_hel_recover)
   {
     gbl_hel_recover=0; // clear recover flag
     set_alarm_flag("hel_mismatch",0); // clear the alarm
   }

  /* check epics if relevant */
  if (epics_flag)
    {
      /* check value again before histogramming data */
      /*  recheck value by comparing present value */
      end_value=0.0;

#ifdef EPICS_ACCESS

      if(dd[5])printf("histo_process: calling read_epics_val to recheck value before histogramming \n");
      status = read_epics_val( &end_value);
      if(status==-1)
	{
	  printf("Bad status returned from read_epics_val for %s (%d)\n",fs.output.e1n_epics_device,status);
	  cm_msg(MERROR, "histo_process", "error  reading %s Epics scan value (%d)",fs.output.e1n_epics_device,status);
	  stop_run();
	  write_client_code(EPICS_ERR,SET,"frontend");
	  status =DB_NO_ACCESS;
	  goto ret;
	}
    
      if(dd[5])
	  printf ("histo_process: read back %.4f, expected value %.4f for %s\n",
		       epics_params.Epics_read, end_value, fs.output.e1n_epics_device);
 /* check the stability */
      if(fabs (end_value - epics_params.Epics_read) > epics_params.Epics_stability )
	{
	  printf("\nhisto_process: Unstable %s Value, Set value=%.4f%s,just read  %.4f,expected %.4f, diff > %.4f\n",
		 fs.output.e1n_epics_device,epics_params.Epics_val, epics_params.Epics_units, end_value, epics_params.Epics_read, epics_params.Epics_stability);
	  skip_cycle = TRUE; 
	  status = set_epics_val(); /* set Epics device to epics_params.Epics_val */
	  cyinfo.cancelled_cycle ++;
	  if(status != SUCCESS) 
	    {
	      cm_msg(MERROR,"histo_process","Cannot set EPICS set value. Stopping Run");
	      write_client_code(EPICS_ERR,SET,"frontend");
	      stop_run();

	      status= FE_ERR_HW;
	      goto ret;
	    }
	}
      else
	epics_params.Epics_last_value = end_value; /* remember this value ... */

#endif
    } /* end of epics flag */
  
#ifdef CAMP_ACCESS
  /*        CAMP
	    may skip the cycle later 
  */
  else if (camp_flag)
    {
      float read_camp_val;
      /* now read camp value back to check  */
      if(dc) printf("histo_process: calling read_primary_device to read CAMP value \n");
      read_camp_val=-999; /* initialize */
      status = read_sweep_dev(&read_camp_val, camp_params);
      if(status != CAMP_SUCCESS)
	{  /* read_sweep_dev sets client flag to stop the run */
	  cm_msg(MERROR,"histo_process","Error from CAMP read_sweep_dev  (%d); run should be stopped \n",status);
	  write_client_code(CAMP_ERR,SET,"frontend");
	  stop_run();
	  status=FE_ERR_HW;
	  goto ret;
	}
      
      if(dc)printf("histo_process: Read back value from CAMP device = %f \n",read_camp_val);
      cyinfo.campdev_read = read_camp_val;
      /* Later we may do some kind of check on the set value and the value read back. For now, don't check */
      /*if (read_camp_val != set_camp_val) 
	if (fabs (read_camp_val - set_camp_val) > fabs(.01 * set_camp_val) )
	printf("histo_process: WARNING camp read back %f ; set value = %f ** expected to be within 1% ** \n",read_camp_val,set_camp_val);
      */
    }
#endif
  
/* We only have an RF Trip in BNMR crate where there are two scalers ... is this still true? */
#ifdef BNMR
  /* check if RF tripped */
  if (fs.hardware.check_rf_trip)
  {
#ifdef HAVE_PSM
      async_data = psmReadRFpowerTrip(gVme, PSM_BASE);
      if(dpsm)printf("PSM RF trip register reads %x\n",async_data);
#endif
    cyinfo.rf_state = async_data;
    if ( async_data & RF_TRIPPED ) 
    {
      cm_msg(MTALK,"histo_process"," RF has tripped at cycle %d !!!",gbl_CYCLE_N);
      skip_cycle = TRUE;
      cyinfo.cancelled_cycle ++;
#ifdef HAVE_PSM
      printf("histo_process: clearing PSM RF Trip \n");
      psmClearRFpowerTrip(gVme, PSM_BASE);
#endif

      status=1;
      goto ret;
    }
  }
#endif /* BNMR (TWO_SCALERS) */


 /*===========================================
   BNMR/BNQR threshold checks 
   =============================================*/
 
 /* check Fluorescence monitor 
    July 2000 - It is located in 1st ch of MSC    
    27-Jun-01 now located ch0 of 2nd scaler)
    so we can only do this check if we have two scalers
    
    Dec 02 : SIS Ref Ch 1 may be turned on instead of fluor 1
    This check is likely not used, but use fluor mon 2 instead of 1
 */
 


 /*  cyinfo.fluor_monitor_counts = scaler[FLUOR_CHAN].sum_cycle; // fluor 1 */
 cyinfo.fluor_monitor_counts = scaler[FLUOR_CHAN2-ECL_OFFSET].sum_cycle; /* fluor 2 */
 if (fs.hardware.fluor_monitor_thr > 0)
   {
     if(cyinfo.fluor_monitor_counts <= fs.hardware.fluor_monitor_thr)
       {
	 cm_msg(MTALK,"histo_process","Fluorescence monitor too low");
	 skip_cycle = TRUE;
	 cyinfo.cancelled_cycle ++;
	 status=1;
	 goto ret;
       }
   }
 



#ifdef THR_TEST
 { // THR_TEST 
   BOOL jran; 

   // Generate some uncertainty for thr calcs.
   float ran;
   INT iran;
   INT nval=10;
   
   iran=get_random_index(nval); // get a random number, iran is an integer between 0 & nval-1 
   if(dd[3])
     printf("nval = %d  random no. iran (between 0 and %d) = %d\n",nval,(nval-1),iran);
   if(iran >= 9)
     jran=TRUE; // send neutral beam to zero to simulate beam off 
   else
     jran=FALSE;
   
   ran = 0.1 * ((float)iran);
   if(ran >= 0.5)
     ran=(ran-0.5)*-1;
#endif // THR_TEST

#ifdef EPICS_ACCESS
#ifdef LASER
 /*  End-of-cycle Laser Power threshold check here   
  */
 
   if(!fs.hardware.disable_laser_thr_check)
     {
       
       if(fs.hardware.cycle_thr3 > 0.f)  /* LAser power */
	 {
	   set_long_watchdog(gbl_long_watchdog_time); /* 3 min */
	   status = read_Epics_chan(Rchid_lp,  &cyinfo.laser_power_v_);
	   restore_watchdog();
	   Laser_last_time=ss_time();
	   if(status != SUCCESS)
	     {
	       cm_msg(MERROR,"histo_process","failure reading EPICS LaserPower(%d)",status);
	       cyinfo.cancelled_cycle ++;
	       skip_cycle = TRUE;
	       
	       /* user can now define two different "skip cycles" params; */
	       if( cyinfo.ncycle_sk_tol < fs.hardware.skip_ncycles_laser_tol)
		 cyinfo.ncycle_sk_tol = fs.hardware.skip_ncycles_laser_tol; 
	       printf("histo_process: skipping %d cycles out-of-tol\n", cyinfo.ncycle_sk_tol);
	       set_threshold_alarm(3);

	       status=1;
	       goto ret;
	     }
/*	   
#ifdef THR_TEST
	   if( cyinfo.laser_power_v_ == 0 )
	     {  // laser power may not be on for testing so give it some data 
	       cyinfo.laser_power_v_  = glp +ran;
	       if(dd[3])
		 printf("histo_process: TEST Adjusting laser read value 0 V to %fV\n",cyinfo.laser_power_v_ );
	     }
#endif THR_TEST
*/	   
	   
	   if(dd[3])
	     printf("histo_process: checking laser power threshold\n"); 
	   
	  
	   
	   if (cyinfo.laser_power_v_  < fs.hardware.cycle_thr3)
	     {
	       cm_msg(MERROR,"histo_process","Laser Power (%f Volts) is below threshold(%f)",
		      cyinfo.laser_power_v_,fs.hardware.cycle_thr3);
	       skip_cycle = TRUE;
	       cyinfo.cancelled_cycle ++;
	       /* user can now define two different "skip cycles" params; */
	       if( cyinfo.ncycle_sk_tol < fs.hardware.skip_ncycles_laser_tol)
		 cyinfo.ncycle_sk_tol = fs.hardware.skip_ncycles_laser_tol; 
	       printf("histo_process: skipping %d cycles out-of-tol\n", cyinfo.ncycle_sk_tol);
	       set_threshold_alarm(3);
	       status=1;
	       goto ret;
	     }
	 }
     }
#endif // LASER
#endif /* EPIC ACCESS */
      


  /* set reference values */
  /* use all 8 neutral beam scalers */
   // May 2015 Don't use the neutral beam especially in dual channel mode 
 ref_value = 0;
 // for (i=0; i<8; i++ )    
 // ref_value += (float)scaler[NEUTRAL_BEAM_B - ECL_OFFSET + i].sum_cycle;
 //printf("scaler[NEUTRAL_BEAM_B - ECL_OFFSET + 0].sum_cycle= %f\n",
 //	scaler[NEUTRAL_BEAM_B - ECL_OFFSET ].sum_cycle);
 /* offset=0;
 #ifdef TWO_SCALERS

 if(exp_mode == 1) 
   offset = n_scaler_real+NA1_CYCLE_SCALER;
 else
  offset = n_scaler_real;
 printf("\n ** histo_process: two_scalers, type %d,  offset=%d\n",exp_mode, offset);
 back_cyc = offset + NA_BACK_CYC;
 front_cyc = offset + NA_FRONT_CYC;
 ref_value = (float)scaler[back_cyc].sum_cycle	+ (float)scaler[front_cyc].sum_cycle;
 printf("scaler[%d].sum_cycle=%f scaler[%d].sum_cycle=%f   \n", back_cyc,scaler[back_cyc].sum_cycle,front_cyc, scaler[front_cyc].sum_cycle);
#else  // ONE SCALER

   offset = n_scaler_real;
 printf("\n ** histo_process: one scaler,  offset=%d\n",offset);
   ref_value  = (float)scaler[NB_POL_CYC+offset].sum_cycle;
   printf("scaler[%d].sum_cycle=%f\n",(NB_POL_CYC+offset), scaler[NB_POL_CYC+offset].sum_cycle);
#endif 
 */   
 ref_value=remember_sum_cycle;
 if(dd[3])
   {
     printf("\nhisto_process: reference value is %f \n",ref_value);
     printf("Clearing remember_sum_cycle\n");
   }
     remember_sum_cycle=0;
#ifdef THR_TEST
  //need to simulate beam off to test thresholds with real data.
  //there needs to be some data in the neutral beam counters to set the thresholds up 
  if(ref_value < 10) 
    ref_value=4000; // invent some data
  printf("Threshold Test...Neutral beam count: %f ran=%f Randomized neutral beam count -> %f\n",ref_value,ran,ref_value+ran); 

  if(beam_off_cntr == 0)
    {

  if(jran)
   {
     printf("\n ======== Beam off!! ======================= \n");
     beam_off_cntr = NUM_BEAM_OFF+1;
     hot_threshold_trip=1; // fail test 1
   }
  else
    printf("Beam on...");
    }
  else
    {
      beam_off_cntr--;
      if(beam_off_cntr > 0)
	{
	  printf("Beam still off  cntr=%d\n",beam_off_cntr);
	  hot_threshold_trip=2; // fail test 2
	}
      else
	{
	  printf("Beam back on... cntr=%d\n",beam_off_cntr);
	  hot_threshold_trip=0;
	}
    }

 }
#endif // THR_TEST
 


 /*check if this is the beginning of a run 
    April 2003 - Rob says that there should not be differences between
    helup or heldown cycles now when using the total sum of neutral beam
    monitors. In case this changes later, I keep both sets of Up and Down
    values but I take the sum for the 1st cycle of the run as the ref_value 
    
    March 2005 - Add a second threshold. 
    test 1 : is (current rate-ref_rate)/ref_rate < thresh1 (original test)
    if test1 gives OK, test 2 is done. If test1 fails, update prev_rate, kill cycle
    
    test 2:  is (current rate-prev_rate)/(prev_rate+current rate)/2 < thresh2
    where prev_rate = previous cycle (not previous good cycle)
    
  */
 if(gbl_CYCLE_N ==1)
   {
     cyinfo.ref_heldown_thr = ref_value;
     cyinfo.ref_helup_thr = ref_value;
     if(!dd[11])
       printf("\nFirst valid cycle...resetting reference threshold to current (%f)\n",ref_value);
   }
 
 
 /* check for Re-reference of the threshold - see comment above */
 if (hot_rereference)
   {
     cyinfo.ref_heldown_thr = ref_value;
     cyinfo.ref_helup_thr = ref_value;

     /* Sept 09 reref previous as well */
     cyinfo.prev_heldown_thr = ref_value;
     cyinfo.prev_helup_thr = ref_value;

     
     hot_reference_clear();
     printf("\nRereference requested...resetting ref and prev thres to current (%f)\n",
	    ref_value); 
     cyinfo.ncycle_sk_tol = 0; 	      
   }
 



  /* check threshold */
 if (gbl_ppg_hel == HEL_DOWN)  /*  use hel set value (checked against readback above) */
   {
     /* compose total sum for HEL_DOWN  */
     cyinfo.prev_heldown_thr= cyinfo.current_heldown_thr; /* fill previous cycle value (good or bad) */
     cyinfo.current_heldown_thr = ref_value; /* update current value */

     if(dd[3])
       printf("updating cyinfo.prev_heldown_thr to %7.0f and current value to %7.0f; ref is %7.0f\n",
		  cyinfo.prev_heldown_thr,ref_value,cyinfo.ref_heldown_thr); 

     /*printf("calling thresh_check; cyinfo.last_failed_thr_test =%d\n", cyinfo.last_failed_thr_test);
      */
     status = thresh_check(cyinfo.prev_heldown_thr,
			   cyinfo.current_heldown_thr,
			   cyinfo.ref_heldown_thr);

   } /* end of hel down */

 else
   { /* hel up */

      /* compose total sum for HEL_UP */
      cyinfo.prev_helup_thr= cyinfo.current_helup_thr; /* fill previous cycle value (good or bad) */
      cyinfo.current_helup_thr = ref_value; /* update current value */
      if(dd[3])
	printf("updating cyinfo.prev_helup_thr to %7.0f and current value to %7.0f; ref is %7.0f\n",
	cyinfo.prev_helup_thr,ref_value, cyinfo.ref_helup_thr); 

      /*printf("calling thresh_check; cyinfo.last_failed_thr_test =%d\n", cyinfo.last_failed_thr_test);*/
    status = thresh_check(cyinfo.prev_helup_thr,
			   cyinfo.current_helup_thr,
			   cyinfo.ref_helup_thr);
    
   } /* end of HEL UP */

 if(skip_cycle)
   { 
     if(dd[18])
       printf("histo_process: returning after thresh_check because skip_cycle is true, gbl_FREQ_n=%d gbl_CYCLE_N=%d\n",  gbl_FREQ_n,gbl_CYCLE_N);
     status=1; // threshold failed
     goto ret;
   }
 if(dd[3])printf("histo_process: after thresh_check status=%d skip_cycle=%d\n",status,skip_cycle);


 if(const1f_flag)
   {
     if(const1f_stepped_back)
       { // success -  this cycle run at previous frequency has passed threshold
	 const1f_stepped_back=0; // clear the flag
	 if(dd[18])
	   printf("histo_process: stepped_back flag is true; successful repeat at freq=%u gbl_FREQ_n=%d gbl_CYCLE_N=%d ... setting junk_data and skip_cycle \n",
			  freq_val,gbl_FREQ_n,gbl_CYCLE_N);
	 const1f_junk_data=1;  // set a flag for cycle_start
	 skip_cycle = TRUE;  // needed to discard this repeat data
	 cyinfo.cancelled_cycle ++;
	 status=1;
	 goto ret;
       }
   }




 // End of threshold checks

#ifdef SPEEDUP
      if(const1f_flag)
	{
	  // clear the previous histograms
	  if (waiteqp[HISTO])
	    {
	      printf("histo_process: **** problem... previous histograms have not been sent out!!\n");
	      gbl_prev_histo_cntr++;  // increment counter. What should we do?
	    }
	  if (exp_mode==1  &&  gbl_CYCLE_N % fs.input.num_cycles_per_supercycle == 0 ) // new supercycle
	    {
	      if(dd[18])   
		printf("histo_process: calling clear_all_histos with gbl_CYCLE_N=%d and gbl_CYCLE_H=%d\n",
				    gbl_CYCLE_N,  gbl_CYCLE_H);
	      clear_all_histo();
	    }
	} // const1f_flag
#endif

 if(dd[3])	
   {
#ifdef EPICS_ACCESS
     if(epics_flag)
       cm_msg(MINFO,"histo_process","Histogramming this cycle; hel=%d Epics set %.2f read %.2f",
	      gbl_ppg_hel,epics_params.Epics_val,epics_params.Epics_read );
     else
#endif // EPICS_ACCESS
       cm_msg(MINFO,"histo_process","Histogramming this cycle; helicity=%d\n",gbl_ppg_hel);
   }


 
 /* remember the helicity state of the last good cycle
    not used anywhere */
 cyinfo.last_good_hel=gbl_ppg_hel;
 if(dd[3])printf("cyinfo.last_good_hel=%d\n",  cyinfo.last_good_hel);
 
 
 /*   ----------------------------------------------
         NOW all tests are done - fill histograms 
      ----------------------------------------------*/

 if(dd[1])printf("histo_process - start processing data\n");
 if(exp_mode == 1) {
   hel_offset = 0; /* no separate P and M histograms */
   type_offset = 0; 
 } else {
   type_offset = 2;
   if (gbl_ppg_hel == HEL_DOWN)  /*  use hel set value */
     hel_offset = 0;
   else
     hel_offset = 2;
 }

 //dd[20]=1;
 //printf("histo_process: temp setting dd[20] to 1\n");
 // printf("histo_process: adding current cycle to H0 bin content (i=0)  histo[0+hel_offset].ph[0]= %d\n", histo[0+hel_offset].ph[0]);
#ifdef TWO_SCALERS
 /* debug */
 if(dd[12])
   printf("histo_process: adding current cycle to histos H%d and H%d (hel=%d)\n",
	  (0+hel_offset),(1+hel_offset), gbl_ppg_hel);
 
 /* add current cycle to overall run histo H0 */
 for (i=0 ; i < n_his_bins ; i++)
   {
#ifdef THIRTYTWO  // thirtytwo inputs
     /* add all 16 first scalers bin content */
     for (h=0 ; h < 16 ; h++)
       {
	 if(small_bin_flag)
	   histo[0+hel_offset].qh[i] += scaler[h].qs[i];
	 else
	   histo[0+hel_offset].ph[i] += scaler[h].ps[i];
       } 
#else // two inputs
     if(small_bin_flag)
       histo[0+hel_offset].qh[i] += scaler[0].qs[i];
     else
       histo[0+hel_offset].ph[i] += scaler[0].ps[i];

#endif  //  THIRTYTWO
   }  
 
 // printf("histo_process: adding current cycle to H1 bin content (i=0)  histo[1+hel_offset].ph[0]= %d\n", histo[1+hel_offset].ph[0]);
  /* add current cycle to overall run histo H1 */
  for (i=0 ; i < n_his_bins ; i++)
  {
#ifdef THIRTYTWO
    /* add all 16 first scalers bin content */
    for (h=16 ; h < 32 ; h++)
      {
	if(small_bin_flag)
	  histo[1+hel_offset].qh[i] += scaler[h].qs[i];
	else
	  histo[1+hel_offset].ph[i] += scaler[h].ps[i];
      } // end loop
#else // TWO inputs
    if(small_bin_flag)
      histo[1+hel_offset].qh[i] += scaler[1].qs[i];
    else
      histo[1+hel_offset].ph[i] += scaler[1].ps[i];
#endif //   THIRTYTWO
  }
 
#endif  // TWO_SCALERS


  /* Sample/reference may be enabled on BNQR (Scaler B) */
  sample_offset=0; // default
  int fluor_offset=0; //  TEMP s/r default 
  if(gbl_sr)
    {
      if(gbl_read_sample)  // "sample" IOREG bit is true
	{
	  sample_offset=8;
	  if(0) fluor_offset=1; // TEMP s/r default for TEMP code below
	}
    }
 
  
  if(0)
    { // TEMP for testing s/r mode. Use fluor ch 1 for sample and direct counts to ch 2 for ref
      printf("histo_process: hel_offset=%d  sample_offset=%d fluor_offset=%d\n",hel_offset,sample_offset,fluor_offset);
      printf("histo_process:  Filling fluor histos at ");
      if(gbl_sr)  // TEMP s/r for testing
	{
	  j=0;
	  printf(" index %d ",n_histo_a+j);
	  for (i=0 ; i < n_his_bins ; i++)
	    {
	      if(small_bin_flag)
		histo[n_histo_a+j+fluor_offset].qh[i] += scaler[MAX_CHAN_SIS38xxA+j].qs[i];
	      else
		histo[n_histo_a+j+fluor_offset].ph[i] += scaler[MAX_CHAN_SIS38xxA+j].ps[i];

	    }
	}
      else
	{ // normal
	  /* take care of Scal B histos  */
	  for (j=0 ; j < 2 ; j++) /* fill first 2 of ScalerB's histos (Fl. monitors) */
	    {
	      printf(" index %d ",n_histo_a+j);
	      for (i=0 ; i < n_his_bins ; i++)
		{
		  if(small_bin_flag)
		    histo[n_histo_a+j].qh[i] += scaler[MAX_CHAN_SIS38xxA+j].qs[i];
		  else
		    histo[n_histo_a+j].ph[i] += scaler[MAX_CHAN_SIS38xxA+j].ps[i];
		}
	    }
	}
    } //end of TEMP for testing,  not used
  


  // TEMP
  //dd[20]=dd[19]=1;
    if(dd[19] || dd[20])
    {
      printf("histo_process: hel_offset=%d  sample_offset=%d\n",hel_offset,sample_offset);
      if(hel_offset == 0)
	printf(" +++++++  HEL  DOWN +++++++++ \n");
      else
	printf(" +++++++  HEL  UP  +++++++++ \n");

    }
    joffset=0; // histogram offset
    
    if(gbl_alpha)
      {
        // Alpha mode available with Type 2h
        // Either Alpha mode Or S/R mode (not both)
        // type_offset=2 for PPG Mode 2 (alpha mode is "2h")
        // scaler is offset for Fl monitors (no malloc). First scaler is scaler[2]
        // no offset for histograms.                     First histo is histo[0]
	
	printf("Mode 2h (gbl_alpha) skipping Fl monitors\n");
	// valid scaler values start at 2. histos start at 0;
	  
	joffset=0;
	printf("hel_offset=%d\n",hel_offset);    
	    
	for (j=0 ; j < 2 ; j++) /* next ScalerB's (Pol monitors) HMLP HMRP OR HMLN HMRN  */
	  {
	    hindex = n_histo_a+joffset+j+hel_offset; // histo index	   
	    sindex = POL_CHAN1 - ECL_OFFSET +j;
	    
	    if(dd[20])
	      printf("Mode 2h (alpha mode) pol histo[%d]   scaler[%d]   \n",hindex,sindex);
	    for (i=0 ; i < n_his_bins ; i++)
	      {
		if(small_bin_flag)
		  {
		    if( (dd[20]) && i<dnbins)
		      printf("adding scaler[%d] bin %d contents %d \n", 
			     sindex, i, scaler[sindex].qs[i]);
		    histo[hindex].qh[i] += scaler[sindex].qs[i];
		  }
		else
		  {
		    if( (dd[20]) && i<dnbins)
		      printf("adding scaler[%d] bin %d contents %d \n", 
			     sindex, i, scaler[sindex].ps[i]);
		    histo[hindex].ph[i] += scaler[sindex].ps[i];
		  }
	      }
	  }

	joffset=2;
	
	for (j=0 ; j < 2 ; j++)  /* SCALER B  NBBM 2 summed inputs  */
	  {
	    hindex =  n_histo_a+joffset+hel_offset+type_offset+j;
	    sindex =  NEUTRAL_BEAM_B -ECL_OFFSET + j;
	    if(dd[20])printf("Mode 2h (alpha mode) NB histo[%d] scaler[%d]\n", 
		   hindex,sindex );
	    
	    for (i=0 ; i < n_his_bins ; i++)    
	      {                                    
		if(small_bin_flag)
		  {
		    /* add scalers bin contents (NUM_NEUTRAL_BEAM backward NB monitors) -> total 4 histos */
		    if( (dd[20]) && i<dnbins)
		      {
			printf("scaler[%d] bin %d contains %d \n", 
			       sindex , i,scaler[sindex].qs[i]);
		      }
		    histo[hindex].qh[i] += scaler[sindex].qs[i];
		  } // end small bins
		else
		  {
		    /* add scalers bin contents (NUM_NEUTRAL_BEAM backward NB monitors) -> total 4 histos */
		    if( (dd[20]) && i<dnbins)
		      {
			printf("scaler[%d] bin %d contains %d \n", 
			       sindex , i,scaler[sindex].ps[i]);
		      }
		    histo[hindex].ph[i] += scaler[sindex].ps[i];
		  } // end large bins
	      } 
	  }  
	joffset+=4;
	
	// 
	// Type 2 with hel flip.  Note that Alpha_hel_offset may be 0 or 4 (4 alpha channels)
	alpha_hel_offset = 2 * hel_offset;
	
	for (j=0 ; j < NUM_ALPHA ; j++)  /* SCALER B  ALPHA */
	  {    
	    hindex = n_histo_a+joffset+alpha_hel_offset+type_offset+j;  // was joffset was 6 now 4 (no Fl histos)
	    sindex = ALPHA_CHAN0 - ECL_OFFSET + j ;
	    if(dd[20])printf("Mode 2h (gbl_alpha)  ALPHA %d histo[%d] scaler [%d]\n",j,hindex,sindex);
	    
	    for (i=0 ; i < n_his_bins ; i++)    
	      {
		
		if(small_bin_flag)
		  {
		    if( (dd[20]) && i<dnbins)
		      printf("scaler[%d] bin %d contains %d \n",sindex ,i,  scaler[sindex].qs[i]);	  
		    histo[hindex].qh[i] += scaler[sindex].qs[i];
		  } // small bins
		else
		  {
		    if( (dd[20]) && i<dnbins)
		      printf("scaler[%d] bin %d contains %d \n",sindex ,i,  scaler[sindex].ps[i]);	  
		    histo[hindex].ph[i] += scaler[sindex].ps[i];
		  } // end large bins
	      }
	  }	
	/*
	  Alpha enabled : fill
	  histo[n_histo_a + 4 + 0 + 2 ] histo[n_histo_a + 5 + 0 + 2 ]  (Hel Down)
	  histo[n_histo_a + 4 + 2 + 2 ] histo[n_histo_a + 5 + 2 + 2 ]  (Hel Up)
	*/

      } // end 2h (gbl_alpha) mode


    else
      { // all other modes

	 if(dd[19] || dd[20])	
	printf("histo_process:  Filling fluor histos at \n");
	
	/* take care of Scal B histos  */
	for (j=0 ; j < 2 ; j++) /* fill first 2 of ScalerB's histos (Fl. monitors)  */
	  {
	    if(dd[19] || dd[20] )
	      printf("Fl histo[%d]  scaler[%d] : ",n_histo_a+j, MAX_CHAN_SIS38xxA+j );
	    
	    for (i=0 ; i < n_his_bins ; i++)
	      {
		if(small_bin_flag)
		  {
		    if( (dd[19] || dd[20]) && i<dnbins)
		      printf("adding scaler[%d] bin %d contents %d  (WORD)\n", MAX_CHAN_SIS38xxA+j, i, scaler[MAX_CHAN_SIS38xxA+j].qs[i]);
		    histo[n_histo_a+j].qh[i] += scaler[MAX_CHAN_SIS38xxA+j].qs[i];
		  }
		else
		  {
		    if( (dd[19] || dd[20]) && i<dnbins)
		      printf("adding scaler[%d] bin %d contents %d \n", MAX_CHAN_SIS38xxA+j,i, scaler[MAX_CHAN_SIS38xxA+j].ps[i]);
		    histo[n_histo_a+j].ph[i] += scaler[MAX_CHAN_SIS38xxA+j].ps[i];
		  }
	      }
	  }
	joffset+=2; // filled 2 histos for Fluor monitors
 

	/* 
	   Total of 2 histograms for POL monitors
	*/

	if(dd[19] || dd[20] )
	  { // sample/ref debug
	    printf("histo_process:  Filling polarimeter histos at ... \n");
	  }



	for (j=0 ; j < 2 ; j++) /* next 2 of ScalerB's (Pol monitors) HMLP HMRP OR HMLN HMRN  */
	  {
	    hindex = n_histo_a+joffset+j+hel_offset+sample_offset; // histo index
	    sindex =  MAX_CHAN_SIS38xxA+joffset+j; // scaler index
	    if(dd[19] || dd[20])
	      printf("pol histo[%d]  scaler[%d]:",  hindex,sindex);

	    for (i=0 ; i < n_his_bins ; i++)
	      {
		if(small_bin_flag)
		  {
		    if( (dd[19] || dd[20]) && i<dnbins)
		      printf("adding scaler[%d] bin %d contents %d \n", 
			     sindex, i,scaler[sindex].qs[i]);
		    histo[hindex].qh[i] += scaler[sindex].qs[i];
		  }
		else
		  {
		    if( (dd[19]) && i<dnbins)
		      printf("adding scaler[%d] bin %d contents %d \n", 
			     sindex, i,scaler[sindex].ps[i]);
		    histo[hindex].ph[i] += scaler[sindex].ps[i];
		  }
	      }
	  }

	joffset+=2; // filled 2 + hel_offset  histos for POL monitors
 
  /* POL Monitors:  filled POL+ histo[n_histo_a + 2 + 0] and [n_histo_a + 3 + 0]  
                       or  POL- histo[n_histo_a + 2 + 2] and  histo[n_histo_a + 3 + 2]
                                     HMLP HMRP OR HMLN HMRN 
                          TYPE 2                                  TYPE 1   no separate P and M histograms
                        4 histograms for POL monitors             2 histograms for POL monitors 
           S/R Mode:    4 extra histograms for POL monitors          -
  
  
     Neutral Beam (NB) monitors
     
     fill histograms [n_histo_a+4+type]  backward h7 + hel_offset 
     & [n_hsto_a+5] forward    h8 */
  
	if(dd[19] || dd[20])
	  {
	    printf("\n");
	    printf("histo_process:  Filling neutral beam histos\n");
	  }

	for (j=0 ; j < NUM_NEUTRAL_BEAM ; j++)  /* SCALER B  NBB */
	  { 	  
	    for (i=0 ; i < n_his_bins ; i++)    
	      {

		if(small_bin_flag)
		  {
		    /* add scalers bin contents (NUM_NEUTRAL_BEAM backward NB monitors) -> total 4 histos */
		    if( (dd[19] || dd[20]) && i<dnbins)
		      {
			printf("histo[%d] :", n_histo_a+4+hel_offset+type_offset+sample_offset);
			printf("scaler[%d] bin %d contains %d \n", 
			       NEUTRAL_BEAM_B - (ECL_OFFSET) + j, i, scaler[NEUTRAL_BEAM_B -ECL_OFFSET + j].qs[i]);
			printf("histo[%d] :", n_histo_a+5+hel_offset+type_offset+sample_offset);
			printf("scaler[%d] bin %d contains %d \n", 
			       NEUTRAL_BEAM_F -ECL_OFFSET + j,i, scaler[NEUTRAL_BEAM_F -ECL_OFFSET + j].qs[i]);
		      }
		    histo[n_histo_a+4+hel_offset+type_offset+sample_offset].qh[i] += scaler[NEUTRAL_BEAM_B -ECL_OFFSET + j].qs[i];
		    histo[n_histo_a+5+hel_offset+type_offset+sample_offset].qh[i] += scaler[NEUTRAL_BEAM_F -ECL_OFFSET + j].qs[i];
		  } // end small bins
	      
	      else
		{
		  /* add scalers bin contents (NUM_NEUTRAL_BEAM backward NB monitors) -> total 4 histos */
		  if( (dd[19] || dd[20]) && i<dnbins)
		    {
		      printf("histo[%d] :", n_histo_a+4+hel_offset+type_offset+sample_offset);
		      printf("scaler[%d] bin %d contains %d \n", 
			     NEUTRAL_BEAM_B -ECL_OFFSET + j,i, scaler[NEUTRAL_BEAM_B -ECL_OFFSET + j].ps[i]);
		      printf("histo[%d] :", n_histo_a+5+hel_offset+type_offset+sample_offset);
		      printf("scaler[%d] bin %d contains %d \n", 
			     NEUTRAL_BEAM_F -ECL_OFFSET + j,i, scaler[NEUTRAL_BEAM_F -ECL_OFFSET + j].ps[i]);
		    }
		  histo[n_histo_a+4+hel_offset+type_offset+sample_offset].ph[i] += scaler[NEUTRAL_BEAM_B -ECL_OFFSET + j].ps[i];
		  histo[n_histo_a+5+hel_offset+type_offset+sample_offset].ph[i] += scaler[NEUTRAL_BEAM_F -ECL_OFFSET + j].ps[i];
		  
		} // end large bins
	    } 
	  }
  /* Neutral Beam :   filled histo[n_histo_a + 4 + 0 + 2] histo[n_histo_a + 5 + 0 + 2]  (Hel Down)
                             histo[n_histo_a + 4 + 2 + 2] histo[n_histo_a + 5 + 2 + 2]  (Hel Up)
                     
         S/R Mode enabled : fill also SAMPLE histograms
                             histo[n_histo_a + 4 + 0 + 2 + 8] histo[n_histo_a + 5 + 0 + 2 + 8]  (Hel Down)
                             histo[n_histo_a + 4 + 2 + 2 + 8] histo[n_histo_a + 5 + 2 + 2 + 8]  (Hel Up)

                            TYPE 2                                           TYPE 1   no separate P and M histograms
                       4 histograms for NB monitors (no SR Mode)             4 histograms for NB monitors
                       4 extra histograms for NB monitors in SR Mode
                     
  */

	joffset+=2; // filled 2 + hel_offset  histos for NB

	if(alpha1f_flag)
	  { // Fill alpha histos (Type1f with alpha)
	    if(dd[19] || dd[20])
	      {
		printf("\n");
		printf("histo_process:  Filling alpha histos (mode_1h)\n");
	      }
	    
	    // regular size bins for this mode
	    for (j=0 ; j < NUM_ALPHA ; j++)  /* SCALER B  ALPHA */
	      { 	  
		for (i=0 ; i < n_his_bins ; i++)    
		  {
		    /* add scalers bin contents (ALPHA counters) -> total 4 histos */
		    
		    histo[ n_histo_a+joffset+hel_offset+type_offset+j].ph[i] += scaler[ALPHA_CHAN0 -ECL_OFFSET + j].ps[i];
		    if( (dd[19] || dd[20]) && i<dnbins)
		      {
			printf("histo[%d] :", n_histo_a+joffset+hel_offset+type_offset+j);			 
			printf("scaler[%d] bin %d contains %d;  histo[%d] bin %d now contains %d \n", 
			       ALPHA_CHAN0 - (ECL_OFFSET) + j, i, scaler[ALPHA_CHAN0 -ECL_OFFSET + j].ps[i],
			       n_histo_a+joffset+hel_offset+type_offset+j, i, 
			       histo[n_histo_a+joffset+hel_offset+type_offset+j].ph[i]);
		      }
		  }
	      }

	  } // end of alpha histos (type 1f with alpha only)
      } // end all other modes except 2h (gbl_alpha)
 

    gettimeofday(&t2, NULL);
    // compute elapsed time in millisec
    elapsed_time = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    elapsed_time += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
	  
    if(dd[1]) 
    printf("histo_process: done. Time to process histos = %f ms\n",elapsed_time);
    status=SUCCESS;
 ret:


  return status;
}


/*------------------------------------------------------------------------*/
INT helicity_read(void)
/*------------------------------------------------------------------------*/
{
  /* Now reads the helicity value from the IOREG only 

     Updates gbl_hel_readback

     Returns SUCCESS or FE_ERR_HW
  */

#ifdef  HAVE_NIMIO32
  gbl_hel_readback =  hel_read_ioreg(FALSE ); // read current (or latched in dual channel mode) helicity state
  if(gbl_hel_readback > 1)
    {
      cm_msg(MERROR,"helicity_read","helicity in unknown state (%d) according to NIMIO32 readback",gbl_hel_readback);
      return FE_ERR_HW;
    }
#endif
  return SUCCESS;
}


INT set_init_hel_state(BOOL flip, INT hel_state)
{
  /* 
     Parameters: 
     flip = TRUE if helicity flipping is enabled
     init_hel_state = desired initial helicity state
     

     1. Read current helicity using NIMIO32 (current_state)
     2. PPG writes current_state 
     3. PPG writes desired state (init_hel_state)

 // gbl_ppg_hel is the PPG helicity set value

  */
  INT hel_orig;

  printf("set_init_hel_state: starting with desired hel_state= %d (%s)\n",
	 hel_state,  helicity_state(hel_state));

 if (!fs.hardware.enable_all_hel_checks)
	printf("Warning: all checking on helicity state is disabled\n");


  if(!flip)
    {
      if(fs.hardware.enable_dual_channel_mode)
	{  // If flip is disabled, helicity will have to be set to desired state every time. Not supported.
	  cm_msg(MERROR,"set_init_hel_state","Cannot run in dual channel mode without helicity flip (not supported)");
	  return FE_ERR_HW;
	}
      else // Single channel mode
	{
	  printf("set_init_hel_state: Flipping is disabled, user assumed to have set helicity to desired state\n");
	  return SUCCESS;   // if not flip, user will set helicity to desired state manually
	}
    }


  if(fs.hardware.enable_dual_channel_mode)
    {
      printf("set_init_hel_state: not possible at present in dual channel mode\n");
      return SUCCESS;
    }

#ifdef HAVE_NIMIO32
  // Single Channel Mode
  gbl_hel_readback =  hel_read_ioreg(FALSE); // read present helicity state
  if (fs.hardware.enable_all_hel_checks && gbl_hel_readback > 1)
    {
      cm_msg(MERROR,"set_init_hel_state","helicity in unknown state (%d) according to NIMIO32 readback",gbl_hel_readback);
      return FE_ERR_HW;
    }
  hel_orig = gbl_hel_readback;
  printf("set_init_hel_state: current helicity state : %d (%s) \n", gbl_hel_readback, helicity_state(gbl_hel_readback));
#endif

  // PPG writes the same value
   
  // BNMR ONLY - due to hardware setup, we must actually write the complement
 
  VPPGPolzSet(gVme, PPG_BASE, complement(hel_orig));
  sleep(2); // Sleep
  VPPGPolzSet(gVme, PPG_BASE,  complement(hel_state));
  sleep(2); // Sleep

  gbl_hel_readback =  hel_read_ioreg(TRUE ); // read present helicity state
  if (fs.hardware.enable_all_hel_checks && gbl_hel_readback > 1)
    {
      printf(" set_init_hel_state: helicity in unknown state (%d); retrying after 2s...\n",gbl_hel_readback);
      ss_sleep(2000); // sleep
      gbl_hel_readback =  hel_read_ioreg(TRUE);
      if(gbl_hel_readback > 1)
	{
	  cm_msg(MERROR,"set_init_hel_state","helicity in unknown state (%d) according to NIMIO32 readback",gbl_hel_readback);
	  return FE_ERR_HW;
	}
      else
	printf(" set_init_hel_state: helicity is now reading %d\n",gbl_hel_readback);
    }
  
  gbl_ppg_hel = complement( VPPGPolzRead(gVme, PPG_BASE));
  

  printf("set_init_hel_state: helicity state is now : %d (%s). Set value gbl_ppg_hel=%d \n", 
	 gbl_hel_readback, helicity_state(gbl_hel_readback),gbl_ppg_hel);

  if (fs.hardware.enable_all_hel_checks)
    {
      if(gbl_hel_readback != hel_state)
	{
	  cm_msg(MERROR,"set_init_hel_state","Cannot set helicity into initial state %d (%s)",hel_state,helicity_state(hel_state));
	  printf("set_init_hel_state: Cannot set helicity into initial state %d (%s). Current Helicity state is %d (%s)\n",
		 hel_state,helicity_state(hel_state), gbl_hel_readback,helicity_state(gbl_hel_readback));
	  return FE_ERR_HW;
	}
      if(gbl_hel_readback != gbl_ppg_hel)
	{
	  printf("set_init_hel_state: Set value gbl_hel_readback (%d) does not agree with readback (%d)\n",gbl_hel_readback, gbl_ppg_hel);
	  cm_msg(MERROR,"set_init_hel_state","Helicity set value %d (%s) does not agree with readback %d  (%s)",
		 gbl_ppg_hel  ,helicity_state( gbl_ppg_hel),  hel_state,helicity_state(hel_state));
	  return FE_ERR_HW;
	}
    }
  printf("set_init_hel_state returns SUCCESS\n");
  cm_msg(MINFO,"set_init_hel_state","returning SUCCESS");
  return SUCCESS;
}

INT complement(INT hel)
{   /* return the one's complement for BNMR only 

       This is to correct a hardware difference between BNMR and BNQR
       BNQR PPG writes a 1  =>  HEL_UP or 0 => HEL_DOWN  correct
       BNMR PPG writes a 1  =>  HEL_DOWN or 0 => HEL_UP  incorrect
       
       Therefore  complement() is called to 
       1. invert the  bit sent to VPPGPolzSet for BNMR only
       2. invert the bit read from  VPPGPolzFlip for BNMR only
    */ 
#ifdef BNMR
  printf("complement: reversing hel signal for bnmr\n");
  return (hel ==0 ) ? 1 : 0 ;
#else
  return hel; // no change for BNQR
#endif
}

INT test_helicity(void)
{
  BOOL flip;
  // Test whether the helicity flipping is working correctly
  
  if(fs.hardware.enable_dual_channel_mode)
    printf("test_helicity: Warning... dual channel mode is set; still working on setting helicity to correct state\n");
  
  if (!fs.hardware.enable_all_hel_checks)
    printf("test_helicity: Warning: epics helicity switch position check (in rf_config) is disabled\n");
  
  flip = fs.hardware.enable_helicity_flipping;
  if(flip)
    printf("test_helicity: flipping is enabled\n");
  else
    {
      printf("test_helicity: helicity flipping is disabled\n");
      return FAILURE;
    }

  
  printf("\n ---  test_helicity:  calling set_init_hel_state to set helicity into HEL_UP state ---  \n");
  if( set_init_hel_state(flip, HEL_UP) == SUCCESS)
    printf(" test_helicity:  hel state should now be HEL_UP\n");
  else
    {
      printf(" test_helicity:  set_init_hel_state returns FAILURE\n");
      return FAILURE;
    }
  

  printf("\n ---  test_helicity:  calling flip_helicity ---  \n");

  gbl_hel_flipped = flip_helicity(TRUE); // with timer
  if(gbl_hel_flipped)
    printf("test_helicity: helicity was flipped\n");
  else
    {
      printf(" test_helicity:   returns FAILURE\n");
      return FAILURE;
    }

  printf("test_helicity: gbl_hel_readback = %d  (%s) gbl_ppg_hel= %d \n",gbl_hel_readback,  helicity_state(gbl_hel_readback), gbl_ppg_hel);
  printf("test_helicity: helicity should be HEL_DOWN\n");

  return SUCCESS;
}

INT recover_helicity()
{
#ifdef HAVE_NIMIO32
  // called from cycle_start if helicity mismatch
  INT status;


  printf("\nrecover_helicity starting with  gbl_hel_recover=%d gbl_hel_mismatch_flag=%d hot_threshold_trip=%d \n",
  	 gbl_hel_recover,gbl_hel_mismatch_flag, hot_threshold_trip);
 

  gbl_hel_mismatch_flag=0; // clear mismatch flag

  if(gbl_hel_recover)
    { // we have already reset the helicity once
      printf("recover_helicity: error - helicity has not recovered... stop run\n");
      cm_msg(MERROR,"recover_helicity","Cannot recover from helicity mismatch. Check epics helicity state and switch");
      return FE_ERR_HW;
    }

  status =  set_alarm_flag("hel_mismatch",1); // set a flag to send alarm

 
 // recheck the helicity readback

  gbl_hel_readback =  hel_read_ioreg(FALSE ); // read the helicity (dual channel mode returns latched values);
  if( (gbl_ppg_hel != gbl_hel_readback) ||  ( hot_threshold_trip==3) )
    {  // reset the helicity
      printf("recover_helicity: resetting the helicity to the desired state: %d \n",gbl_ppg_hel);

      if(fs.hardware.enable_dual_channel_mode)
#ifdef HAVE_PPG
	VPPGPolzSet(gVme, PPG_BASE, complement(gbl_ppg_hel));
#endif
      else
	{ // single channel mode
	  if(const1f_flag)
	    {
	      if(!const1f_stepped_back)
		{
		  if(gbl_CYCLE_N > 1)  // don't step back on first cycle. Just repeat it.
		    set_previous_freq_1f();  // step back in frequency
		}
	      
	      /* Stop the PPG module */
	      printf("recover_helicity: now stopping the PPG in const1f_mode \n");
#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
	      TPPGReset(gVme,PPG_BASE); // also stops the program even if in long delay
	      TPPGDisableExtTrig(  gVme, PPG_BASE);
#else
	      VPPGStopSequencer(gVme, PPG_BASE);
	      VPPGDisableExtTrig ( gVme, PPG_BASE );
#endif //  HAVE_NEWPPG
#endif //   HAVE_PPG
	      e1f_started_flag = 0; // clear the flag to say PPG is running
	      hel_flip_flag=1; // set flag to prevent max time including this point
	    } // end of const1f_flag
	  
	  
	  status = set_init_hel_state(flip, gbl_ppg_hel);
	  if(status !=SUCCESS)
	    {
	      cm_msg(MERROR,"recover_helicity","Cannot set helicity to desired state after helicity mismatch");
	      return  FE_ERR_HW;
	    }
	  
	  printf("recover_helicity: sleeping for helicity sleep time...\n");
	  // Helicity sleep time
	  iwait(fs.hardware.helicity_flip_sleep__ms_,"recover_helicity") ;
	} // end of single channel mode
    }


  if(hot_threshold_trip==3)
    hot_threshold_trip=0; // clear hot_threshold_trip

  gbl_hel_recover=1; // set a flag so helicity is reset once only
#endif
  return SUCCESS;
}


#ifdef GONE
INT hel_send_edge(INT hel_state)
{
  /* 
     Input parameter: desired helicity state

     NOTE: The Epics Set helicity switch is edge driven, therefore  we have to send an edge to change the
           helicity. So if readback is HEL_UP & desired value is HEL_DOWN, ppg
	   has to send HEL_UP followed by HEL_DOWN to get helicity to change.
  */

  // Send an edge to the hel switch. 

  INT other_hel;

#ifdef HAVE_PPG	
#ifdef HAVE_NEWPPG
  if(fs.hardware.enable_dual_channel_mode)
    cm_msg(MERROR,"set_init_hel_state","Cannot run in dual channel mode with no helicity control (newppg)");
  return FE_ERR_HW;
#endif

  if(hel_state == HEL_DOWN)
    other_hel = HEL_UP;
  else
    other_hel = HEL_DOWN;



  // Send an edge to the switch
  VPPGPolzSet(gVme, PPG_BASE, other_hel);
  sleep(1); // sleep 1s
  VPPGPolzSet(gVme, PPG_BASE, hel_state );
  gbl_ppg_hel = VPPGPolzRead(gVme, PPG_BASE);
  if(gbl_ppg_hel != hel_state)
    {
      cm_msg(MERROR,"hel_send_edge","Problem with PPG. Cannot write initial hel state");
      return FE_ERR_HW;
    }
  else
    printf("hel_send_edge: ppg set value is %d (%s)\n",gbl_ppg_hel, helicity_state(gbl_ppg_hel));
#endif // HAVE_PPG
  return SUCCESS;
}
#endif // GONE


/*-- Clear hot link ------------------------------------------------------*/
void hot_reference_clear(void)
/*------------------------------------------------------------------------*/
{
  INT size;
  
  hot_rereference = FALSE;
  size = sizeof(hot_rereference);
  db_set_value(hDB, hFS, "Hardware/re-Reference", &hot_rereference, size, 1, TID_BOOL);
  if(dd[3])printf("hot_reference_clear: Cleared hot rereference in odb\n");
}

/*-- Clear scaler i ------------------------------------------------------*/
void scaler_clear(INT i)
/*------------------------------------------------------------------------*/
{
  /* scaler_clear is for REAL SCALERS only */
  INT j;
  
  for (j=0;j<scaler[i].nbins;j++)
    {
      if(small_bin_flag)
	scaler[i].qs[j]=0;
      else
 	scaler[i].ps[j]=0;
      scaler[i].sum_cycle = 0.0;
    }

  if(dd[8]) 
  {
    if(small_bin_flag)
      printf("scaler_clear: Cleared Scaler %d nbins=%d SMALL bins\n",i, scaler[i].nbins);
    else
      printf("scaler_clear: Cleared Scaler %d nbins=%d \n",i, scaler[i].nbins);
  }
}

void clear_all_histo()
{  // called if (exp_mode==1  && new_supercycle)

  // clear all defined histograms (no ALPHA offset for histograms)
  int i;
    
    if(dd[8])
      printf("\nclear_all_histos: clearing histos at start of new SuperCycle\n");
  /* clear histograms if starting a new SuperCycle */

  for (i=0 ; i < n_histo_a - 1 ; i++)
    {
      histo_clear(i);
    }
  /* do not clear n_histo_a since this is the USER BIT histo */
  for (i=0 ; i < n_histo_b ; i++)
    {
      histo_clear(n_histo_a + i);  
    }
  return;
}

/*-- Clear histo h ------------------------------------------------------*/
void histo_clear(INT h)
/*------------------------------------------------------------------------*/
{
  INT j;
 
  for (j=0;j<histo[h].nbins;j++)
    {
      if(small_bin_flag)
	histo[h].qh[j]=0;
      else
	histo[h].ph[j]=0;
    }
  histo[h].sum = 0.0;
  histo[h].bin_max = 0.0;

 if(dd[8])
  {
    if(small_bin_flag)
      printf("histo_clear: Cleared Histo %d nbins=%d SMALL bins\n",h, histo[h].nbins);
    else
      printf("histo_clear: Cleared Histo %d nbins=%d \n",h, histo[h].nbins);
  }
}

/*-- Check time file was changed last  -----------------------------------*/
/*------------------------------------------------------------------------*/
INT check_file_time(void)
/*------------------------------------------------------------------------*/
{
  /* checks whether a file bytecode.dat has been made recently by rf_config.
     If file not created recently, checks if rf_config is running.
     If rf_config IS running, waits 5s and retries.
     
  */
  struct stat stbuf;  
  time_t timbuf;
  char timbuf_ascii[30];
  INT elapsed_time=1000;
  INT status;
  char ppgfile[128];
  int i;
  BOOL got_file;

  i=got_file=0;


  /* check whether rf_config is running */
  status = cm_exist ("rf_config",FALSE);
  if (status  != CM_SUCCESS)
    {
      cm_msg(MERROR,"check_file_time","rf_config is not running");
      return(status);
    }
  printf("check_file_time: rf_config IS running ");

  time(&timbuf);

  sprintf(ppgfile,"%s/bytecode.dat",fs.input.cfg_path);

  got_file=0; // flag
  while (i<4)
    {
      i++; // while loop counter
      printf("Looking for recent copy of ppg load file \"%s\"  (loop %d)\n",ppgfile,i);
      stat (ppgfile,&stbuf);       
      if(stbuf.st_size > 0) 
	{
	  printf("check_file_time: found file %s\n",ppgfile);
     
	  printf("check_file_time: file %s  size %d\n",ppgfile, (int)stbuf.st_size); 
	  strcpy(timbuf_ascii, (char *) (ctime(&stbuf.st_mtime)) ); 
	  printf ("check_file_time: PPG loadfile last modified:  %s or (binary) %d \n",timbuf_ascii,(int)stbuf.st_size); 
  
  
	  // get present time 
	  time (&timbuf); 
      
	  strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) ); 
	  printf ("check_file_time: Present time:  %s or (binary) %d \n",timbuf_ascii,(int)timbuf); 
  
	  elapsed_time= (int) ( timbuf - stbuf.st_mtime ); 
	  if(dd[1])
	    printf("Time since file last updated: %d seconds \n",elapsed_time);
  
	  printf("check_file_time: time since ppg loadfile last updated: %d seconds \n",elapsed_time); 
  
	  printf("check_file_time: comparing elapsed_time (%d sec) with (%d sec)\n",elapsed_time, (rftime+epicstime));
	  if(elapsed_time < rftime+epicstime)
	    {
	      got_file=1; // flag
	      break;
	    }
          printf("check_file_time: retrying in 3s... \n");
	  iwait(3000,"check_file_time"); // delay
	}
    } // while

  if(stbuf.st_size < 0)
    {  
      cm_msg(MERROR,"check_file_time","PPG load file %s cannot be found",ppgfile); 
      printf("check_file_time: PPG load file %s cannot be found after %d tries",ppgfile,i); 
      return DB_FILE_ERROR ; 
    }

  if(!got_file)	    
    {
      cm_msg(MERROR,"check_file_time",
	     "rf_config has not created file bytecode.dat within %d seconds (elapsed time = %d)",
	     rftime,elapsed_time);
      printf("check_file_time: rf_config has not created file bytecode.dat within %d seconds (elapsed time = %d)\n",
	     rftime,elapsed_time);
      stop_run();
      return DB_NO_ACCESS;  /* return a failure code */
    }


#ifdef HAVE_NEWPPG
  // check for converted file ppgload.dat


 printf("check_file_time: looking for recent copy of ppg load file \"%s\"\n",
ppg_loadfile);
  stat (ppg_loadfile,&stbuf); 

 if(stbuf.st_size < 0) 
    { 
      status=iwait(3000,"check_file_time"); // wait and retry

      stat (ppg_loadfile,&stbuf);
      if(stbuf.st_size < 0)
	{ 
	  cm_msg(MERROR,"check_file_time","PPG load file %s cannot be found",ppg_loadfile); 
	  return DB_FILE_ERROR ; 
	}
    } 
 printf("check_file_time: file %s  size %d\n",ppg_loadfile, (int)stbuf.st_size); 
  strcpy(timbuf_ascii, (char *) (ctime(&stbuf.st_mtime)) ); 
  printf ("check_file_time: PPG loadfile last modified:  %s or (binary) %d \n",timbuf_ascii,(int)stbuf.st_size); 
  
  
  // get present time 
  time (&timbuf); 
      
  strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) ); 
  printf ("check_file_time: Present time:  %s or (binary) %d \n",timbuf_ascii,(int)timbuf); 
  
  elapsed_time= (int) ( timbuf - stbuf.st_mtime ); 
  if(dd[1])
    printf("Time since file last updated: %d seconds \n",elapsed_time);
 printf("check_file_time: time since ppg loadfile last updated: %d seconds \n",elapsed_time); 
  
  
  if(elapsed_time > rftime+epicstime)
    { 
      cm_msg(MERROR,"check_file_time",
	     "rf_config has not created file bytecode.dat within %d seconds (elapsed time = %d)",
	     rftime,elapsed_time);
      return DB_NO_ACCESS;  /* return a failure code */
    }

#endif

  return(CM_SUCCESS);
}

/*-- die ------------------------------------------------------------------*/
void die()
/*------------------------------------------------------------------------*/
{
  gbl_die=1; // try to stop fifo_acq from running
#ifdef HAVE_SIS3801
  printf("\nScaler B status:\n");
  SIS3801_Status(gVme,SIS3801_BASE_B);
#endif
  stop_run();

  printf ("DAQ dies at cycle %d\n",gbl_CYCLE_N);
  printf ("Attempting to stop the run\n");
 

  return;

#ifdef GONE
  /* for now cancel the cycle */

  skip_cycle = TRUE;
  cyinfo.cancelled_cycle ++;
  /* Set this for  histo_read equipment; it will skip the cycle by testing skip_cycle */
  waiteqp[FIFO] = FALSE;
  printf ("DAQ dies at cycle %d; cycle is cancelled \n",gbl_CYCLE_N); 
  stop_run();
  write_client_code(EPICS_ERR,SET,"frontend");
  return;
#endif
}

// now we can stop the run directly but still want to indicate who stopped the run

/*------------------------------------------------------------------*/
INT set_client_flag(char *client_name, BOOL value)
/*------------------------------------------------------------------*/
{
  /* set the flag in /equipment/fifo_acq/client flags/febnmr to indicate to mdarc that is should stop the run
     set the flag  "/equipment/fifo_acq/client flags/client alarm" to get browser mhttpd to put up an alarm banner

     Note that an almost identical routine is in mdarc_subs.c for use of linux clients;  later can try to just have 
     one routine with ifdefs

  */
  char client_str[128];
  INT client_flag;
  BOOL my_value;
  INT status,size;
  if (dd[9])  
    printf("set_client_flag: starting\n");
 
  my_value = value;
  sprintf(client_str,"/equipment/%s/client flags/%s",equipment[FIFO].name,client_name );
  /* if(dd[9]) */
    printf("set_client_flag: setting client flag for client %s to %d\n",client_name,my_value); 
  /* Set the client flag to my_value 
                 value =  TRUE (success)  or FALSE (failure) */
  size = sizeof(my_value);
  status = db_set_value(hDB, 0, client_str, &my_value, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client status flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }

  /* Set the alarm flag; TRUE - alarm should go off, FALSE alarm stays off  */
  size = sizeof(client_flag);
  if(value) 
    client_flag = 0;
  else
    client_flag = 1;

  sprintf(client_str,"/equipment/%s/client flags/client alarm",equipment[FIFO].name );
  if(dd[9])printf("set_client_flag: setting alarm flag to %d\n",client_flag); 

  status = db_set_value(hDB, 0, client_str, &client_flag, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }
  return CM_SUCCESS ;
}



/*------------------------------------------------------------------*/
INT set_alarm_flag(char *flag_name, BOOL value)
/*------------------------------------------------------------------*/
{
  /* set/clear a flag in client flags area /Equipment/FIFO_acq/client flags/
       ... which will cause alarm to go off
  */
  char str[128];
  INT status,size;
  BOOL my_value;
  if (dd[9])  
    printf("set_alarm_flag: starting\n");
  my_value=value;
  sprintf(str,"/equipment/%s/client flags/%s",equipment[FIFO].name,flag_name );
   if(dd[9]) 
    printf("set_client_flag: setting flag %s to %d\n",flag_name,my_value); 
  /* Set the client flag to my_value 
                 value =  TRUE (success)  or FALSE (failure) */
  size = sizeof(my_value);
  status = db_set_value(hDB, 0, str, &my_value, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set ODB path \"%s\" to %d (%d) ",
	     str,my_value,status);
      return status;
    }

  return SUCCESS;
}










INT iwait(INT msec, char *caller)
{
  /* Input parameter: time to wait in msec
     

  Sep 09 - Note that cm_yield has been removed from iwait since it prevents 
  frontend_loop from running. 

  The time parameter sent to cm_yield is the maximum delay. 
  cm_yield will return sooner if it has nothing to do.

  */

  if(msec > 0) 
    {
      printf("iwait (%s): sleeping %d ms\n",caller,msec);
      ss_sleep(msec);
    }
    return SUCCESS;
}


void update_fix_counter(INT num_increments)
{
  /* this subroutine does something only if flip is true and
     we are at the end of a scan */

  if(gbl_CYCLE_N == 1 || !flip)
    { /* no action unless flip is true and not at begin of run */
      printf("update_fix_counter: no action, gbl_CYCLE_N=%d  flip=%d\n",gbl_CYCLE_N,flip);
      return;
    }

/* except at Begin of Run,
   for up/down scan, increment counter has to be fixed */

  /* printf("update_fix_counter: starting with num increments = %d\n",num_increments);*/
  if(gbl_fix_inc_cntr == 0)  /* global */ 
    gbl_fix_inc_cntr = num_increments -1;
  else
    gbl_fix_inc_cntr = 0;

   if(dd[8]) 
    printf("\nupdate_fix_counter: end of scan, gbl_CYCLE_N=%d. flip=%d, gbl_fix_inc_cntr changed to %d\n",
	   gbl_CYCLE_N, flip,gbl_fix_inc_cntr);
  return;
}


void ppgStop(void)
{
#ifdef HAVE_PPG
  if(dd[1])printf("ppgStop: stopping PPG's sequencer\n");


#ifdef HAVE_NEWPPG
 if(dppg)
   printf("ppgStop: Warning  PPG32 (new PPG) cannot set Beam Off, Beam Ctl, Helicity\n");
 TPPGReset(gVme, PPG_BASE); // reset stops PPG even if in long delay
 TPPGDisableExtTrig (gVme, PPG_BASE);
#else
  if(dppg)
    printf("ppgStop: setting Beam Off\n");

  VPPGBeamOff(gVme, PPG_BASE);
  VPPGStopSequencer(gVme,  PPG_BASE ); 
  VPPGDisableExtTrig(gVme, PPG_BASE);
  VPPGBeamCtlPPG(gVme, PPG_BASE); /* ppg controls the beam cf bnmr_init.c */
  // VPPGPolzSet(gVme, PPG_BASE,HEL_DOWN); /* set helicity to HEL_DOWN - not so simple! */
#endif 
#endif // HAVE_PPG
    
  return;
}


INT print_hardware_flags(void)
{
  char str[256];
  char nscal[]="ONE ";

  sprintf(str,"none");

#ifdef BNMR
  sprintf(str,"BNMR");
#endif
#ifdef BNQR
  sprintf(str,"BNQR");
#endif

  printf("This version of the frontend code is built for experiment %s\n",str);
  if(strncmp(str,BEAMLINE,strlen(str) != 0))
  { /* code is built for the wrong beamline */
    cm_msg(MERROR,"frontend","Mismatch - code was built for %s, but this Midas experiment is %s",
	   str,BEAMLINE);
    return FE_ERR_HW;
  }

  sprintf(str,"with these hardware flags defined: ");
#ifdef HAVE_VMIC
  strcat(str,"VMIC, ");
#endif



#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
 strcat(str,"PPG32 (i.e. NEW PPG), "); // can only be used for testing 
#else
 strcat(str,"PulseBlaster PPG, ");  // old PPG
#endif
#endif // HAVE_PPG


#ifdef HAVE_PSM
  strcat(str,"PSM, ");
#endif
#ifdef HAVE_PSMII
  strcat(str,"PSMII, ");
#endif
#ifdef EPICS_ACCESS
 strcat(str,"EPICS ACCESS, ");
#endif
#ifdef CAMP_ACCESS
 strcat(str,"CAMP ACCESS, ");
#endif

#ifdef TWO_SCALERS
  sprintf(nscal,"TWO ");
#endif

#ifdef HAVE_SIS3801
  strcat(str,nscal);
#ifdef HAVE_SIS3801E
   strcat(str,"SIS3801 Version E Scaler");
#else
  strcat(str,"SIS3801 Scaler");
#endif
#ifdef TWO_SCALERS
  strcat(str,"s");
#endif
#endif

#ifdef HAVE_SIS3820
  strcat(str,nscal);
  strcat(str,"SIS3820 Scaler");
#ifdef TWO_SCALERS
  strcat(str,"s");
#endif
#endif
 
  printf("%s\n",str);

#ifdef  HAVE_SIS3820_OR_SIS3801E
  printf("Flag \"HAVE_SIS3820_OR_SIS3801E\" is set \n");
#endif
#ifdef THR_TEST
  printf("WARNING THR_TEST flag is set. This is for testing only\n");
#endif
  return SUCCESS;

}

/*---------------------------------------------------------------*/
void setup_hotlinks(void)
/*---------------------------------------------------------------*/
{
  INT status;
  /* Called from begin_of_run to set up the hotlinks
     If a hotlink cannot be opened, a message is sent
  */
  char str[128];
  
  /* setup hot link on "re-reference" */
  if (hRR==0)
    {
      sprintf(str,"Hardware/Re-reference");
      status = db_find_key(hDB, hFS, str, &hRR);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hRR, &lhot_rereference
				  , sizeof(hot_rereference)
				  , MODE_READ, call_back, "rereference");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }


  
  if (hCT1==0)
    {
      /* setup hot link on "Cycle thr1" */
      sprintf(str,"Hardware/Cycle thr1");
      status = db_find_key(hDB, hFS, str, &hCT1);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hCT1, &fs.hardware.cycle_thr1
				  , sizeof(fs.hardware.cycle_thr1)
				  , MODE_READ, call_back, "cycle threshold1");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }

  if (hCT2==0)
    {
      /* setup hot link on "Cycle thr2" */
      sprintf(str,"Hardware/Cycle thr2");
      status = db_find_key(hDB, hFS, str, &hCT2);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hCT2, &fs.hardware.cycle_thr2
				  , sizeof(fs.hardware.cycle_thr2)
				  , MODE_READ, call_back, "cycle threshold2");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","hot link on \"%s\" failed (%d)",str,status);
    }

  if (hFL==0)
    {
      /* setup hot link on "Fluor monitor thr" */
      sprintf(str,"Hardware/Fluor monitor thr");
      
      status = db_find_key(hDB, hFS, str, &hFL);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hFL, &fs.hardware.fluor_monitor_thr
				  , sizeof(fs.hardware.fluor_monitor_thr)
				  , MODE_READ, call_back, "fluor monitor threshold");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }

#ifdef LASER
  if(hFT ==0)
    {
      /* setup hot link on Cycle thr3 for BNMR/BNQR where it is used for Laser Power */
      sprintf(str,"Hardware/Cycle thr3");
      status = db_find_key(hDB, hFS, str, &hFT);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hFT, &fs.hardware.cycle_thr3
				  , sizeof(fs.hardware.cycle_thr3)
				  , MODE_READ, call_back, "cycle threshold3");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }
#endif
  
  if (hDC==0)
    {
      /* setup hot link on "Diagnostic channel #" */
      sprintf(str,"Hardware/Diagnostic channel num");
      status = db_find_key(hDB, hFS,str, &hDC);
      if (status == DB_SUCCESS)    
	{
	  status = db_open_record(hDB, hDC, &fs.hardware.diagnostic_channel_num
				  , sizeof(fs.hardware.diagnostic_channel_num)
				  , MODE_READ, call_back, "diagnostic channel number");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\"  failed (%d)",str,status);
    }

  if (hSK==0)
    {
      /* setup hot link on "skip ncycles out-of-tol" */
      sprintf(str,"Hardware/skip ncycles out-of-tol");
      status = db_find_key(hDB, hFS, str, &hSK);
      if (status == DB_SUCCESS)    
	{
	  status = db_open_record(hDB, hSK, &fs.hardware.skip_ncycles_out_of_tol
				  , sizeof(fs.hardware.skip_ncycles_out_of_tol)
				  , MODE_READ, call_back_skip, "skip ncycles out-of-tol");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }

   if (hHs==0)
    { 
      /* setup hot link on "helicity flip sleep (ms)" */
      sprintf(str,"Hardware/helicity flip sleep (ms)");

      status = db_find_key(hDB, hFS, str, &hHs);
      if (status == DB_SUCCESS)    
	{
	  status = db_open_record(hDB, hHs, &fs.hardware.helicity_flip_sleep__ms_
				  , sizeof(fs.hardware.helicity_flip_sleep__ms_)
				  , MODE_READ, call_back, "helicity flip sleep time");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\"  failed (%d)",str,status);
    } 


  if (hFF==0)
    {
      /* setup hot link on "flags/hold" */
    sprintf(str,"flags/hold");
      status = db_find_key(hDB, hFS, str, &hFF);
      if (status == DB_SUCCESS)  
	{  
	status = db_open_record(hDB, hFF, &fs.flags.hold, sizeof(fs.flags.hold)
				, MODE_READ, call_back, "hold flag");
	if (status != DB_SUCCESS) 
	  cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\"  failed (%d)",str,status);
    }

/* BNQR does not use RF trip */
#ifdef BNMR 
#ifdef HAVE_PSM
  if (hPd==0)
    {
       /* setup hot link on "rf trip threshold"  */
      sprintf(str, "Hardware/RF trip threshold (0-5V)");
      status = db_find_key(hDB, hFS,str, &hPd);
      if (status == DB_SUCCESS)  
	{ 
	  status = db_open_record(hDB, hPd, &fs.hardware.rf_trip_threshold__0_5v_
				  , sizeof(fs.hardware.rf_trip_threshold__0_5v_)
				  , MODE_READ,  hot_PSM_RFthr, "RF threshold");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\"  failed (%d)",str,status);
    } 
#endif
#endif

 if (hTh==0)
    {
       /* setup hot link on "threshold trip"  */
      sprintf(str, "Debug/hot threshold trip");
      status = db_find_key(hDB, hFS,str, &hTh);
      if (status == DB_SUCCESS)  
	{ 
	  status = db_open_record(hDB, hTh, &fs.debug.hot_threshold_trip
				  , sizeof(fs.debug.hot_threshold_trip)
				  , MODE_READ,  call_back_hot_threshold_trip, "hot threshold trip");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\"  failed (%d)",str,status);
    } 




#ifdef GONE
 if (hfe==0)
    {
      sprintf(str,"Hardware/sis3820/hot debug");
      status = db_find_key(hDB, hFS, str, &hfe);
      if (status == DB_SUCCESS)
        {
          status = db_open_record(hDB, hfe, &lhot_debug
                                  , sizeof(lhot_debug)
                                  , MODE_READ, call_back_debug, NULL);
          if (status != DB_SUCCESS)
            cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
        }
      else
        cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }
#endif // GONE


  return;
}

#ifdef CAMP_ACCESS

/* ------------------------------------------------------------------*/
INT camp_get_rec(void)
/*------------------------------------------------------------------------*/
{
  /* retrieve the ODB structure for CAMP sweep device
       called by begin_of_run

 */
  INT size, status;
  char str[128];

  /* check we have a key hCamp (found in main)  */
  if( hCamp)
    {
 /* get the record for camp area  */
      size = sizeof(fcamp);
      status = db_get_record (hDB, hCamp, &fcamp, &size, 0);/* get the whole record for mdarc */
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"camp_get_record","Failed to retrieve %s record  (%d) size=%d ",str,status,size);
	  return(status); /* error */
	}
      else
	if(dc)printf("Got the record for camp\n");
    }
  else
    {
      cm_msg(MERROR,"camp_get_record","No key has been found for camp record ");
      return(DB_NO_KEY); /* error */
    }
  return(SUCCESS);
}



/* ------------------------------------------------------------------*/
INT camp_create_rec(void)
/* ------------------------------------------------------------------*/
{
  /* retrieve the ODB structure for CAMP sweep device
  */
  INT size, status;
  char str[128];
  FIFO_ACQ_CAMP_SWEEP_DEVICE_STR(fifo_acq_camp_sweep_device_str); /* for camp (from experim.h) */


  /* get the key hCamp  */
  sprintf(str,"/Equipment/%s/camp sweep devices",equipment[FIFO].name); 
  status = db_find_key(hDB, 0, str, &hCamp);
  if (status != DB_SUCCESS)
    {
      if(dc) printf("camp_create_rec: Failed to find the key %s ",str);
      
      /* Create record for camp area */     
      if(dc) printf("camp_create_rec:Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(fifo_acq_camp_sweep_device_str));
      if (status != DB_SUCCESS)
	{
	  if(dc)printf("camp_create_rec: Failure creating camp record\n");
	  cm_msg(MINFO,"camp_create_rec","Could not create record for %s  (%d)\n", str,status);
	  return(-1);
	}
      else
	if(dc) printf("camp_create_rec: Success from create record for %s\n",str);
      /* try again to get the key hCamp  */
      status = db_find_key(hDB, 0, str, &hCamp);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"camp_create_rec", "Failed to get the key %s ",str);
	  return(-1);
	}
    }    
  else  /* key hCamp has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hCamp, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "camp_create_rec", "error during get_record_size (%d) for camp",status);
	  return status;
	}
      printf("camp_create_rec - Size of camp saved structure: %d, size of camp record: %d\n", 
	     sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICE) ,size);
      if (sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICE) != size) 
	{
	  cm_msg(MINFO,"camp_create_rec","creating record (camp; mismatch between size of structure (%d) & record size (%d)", 
		 sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICE) ,size);
	  /* create record */
	  status = db_create_record(hDB, 0, str , strcomb(fifo_acq_camp_sweep_device_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"camp_create_rec","Could not create camp record (%d)\n",status);
	      return status;
	    }
	  else
	    if (dc)printf("camp_create_rec: Success from create record for %s\n",str);
	}
    }
  return(SUCCESS);
}

/* ------------------------------------------------------------------*/
INT camp_update_params(void)
/* ------------------------------------------------------------------*/
{
  char str[128];
  INT size,status;
  char hostname[128];

  /* camp handle and record should have been got already */
  if(!hCamp)
    {
      cm_msg(MINFO,"e1c_compute","Error: no CAMP settings available");
      return(DB_NO_ACCESS);
    }
  /*  the camp parameters should have been checked already */

  /* fill structure camp_params */
  // General Camp device
  strcpy(camp_params.SweepDevice,"general"); // general camp device
  strncpy(camp_params.InsPath, fcamp.camp_path,32);
  strncpy(camp_params.InsType,  fcamp.instrument_type,32);
  strncpy(camp_params.IfMod,  fcamp.gpib_port_or_rs232_portname,32);
  strncpy(camp_params.setPath,  fcamp.camp_scan_path,80); /* /afg/frequency */
  strncpy(camp_params.DevDepPath, fcamp.camp_device_dependent_path,80); /* blank - not used */
  strncpy(camp_params.units,  fcamp.scan_units,9);
  camp_params.maximum_value = fcamp.maximum;
  camp_params.minimum_value = fcamp.minimum;
  camp_params.conversion_factor = fcamp.integer_conversion_factor;
  /* and set gbl_scan_flag to indicate CAMP General scan */
  gbl_scan_flag = 1<<8;      /* camp general scan (value = 0x100) */
 
  
  /* Get the camp hostname from the mdarc area of odb  */
  size = sizeof(hostname); 
  sprintf(str,"/Equipment/%s/mdarc/camp/camp hostname",equipment[FIFO].name); 
  status = db_get_value(hDB, 0, str, hostname, &size, TID_STRING, FALSE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"camp_update_params","cannot get Camp hostname at %s (%d)",str,status);
    return FE_ERR_ODB;
  }
  /*printf("Hostname: %s\n",hostname);*/
  strncpy(camp_params.serverName, hostname,LEN_NODENAME);
  if(dc)
    {
      printf("camp parameter settings:\n");
      printf("camp hostname: %s\n",camp_params.serverName);
      printf("sweep device: %s\n",camp_params.SweepDevice);
      printf("InsPath: %s\n",camp_params.InsPath);
      printf("InsType: %s\n",camp_params.InsType);
      printf("IfMod: %s\n",camp_params.IfMod);
      printf("setPath: %s\n",camp_params.setPath);
      printf("DevDepPath: %s\n",camp_params.DevDepPath);
      printf("units: %s\n",camp_params.units);
      printf("maximum: %f\n",camp_params.maximum_value);
      printf("minimum: %f\n",camp_params.minimum_value);
      printf("conversion factor: %d\n",camp_params.conversion_factor);
      printf("gbl_scan_flag=%d\n",gbl_scan_flag);
    }
  return(SUCCESS);
}


/*------------------------------------------------------------------------*/
INT set_camp_value(float set_camp_val)
/*------------------------------------------------------------------------*/
{
  INT icount,status,stat;

  /*  BOOL watchdog_flag;
      DWORD watchdog_timeout; */

  /* this can take some time so set watchdog for 5 min 
   cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
   cm_set_watchdog_params(watchdog_flag, 300000);  // 5 min for reconnect 5*60*1000 
*/
 
  set_long_watchdog(gbl_long_watchdog_time);  /* 3 min for reconnect 3*60*1000 */

  status = set_sweep_device(set_camp_val, camp_params); 
  if(status == CAMP_SUCCESS)
    {
      /*      cm_set_watchdog_params(watchdog_flag, watchdog_timeout); // restore watchdog timeout */
      restore_watchdog();  /* restore watchdog timeout */
      return status;
    }
  else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

  /* failure */
  printf("set_camp_value: Error attempting to set CAMP value of %f %s; (%d)",
	 set_camp_val,camp_params.units,status);
  
  for(icount=0; icount < 3; icount++)
    { 
      if(gotCamp)
	{
	  status=camp_watchdog(); /* can we talk to CAMP? */
	  if(status == CAMP_SUCCESS)
	    {  /* try to set the device again */
	      cm_msg(MINFO,"set_camp_value","sleeping 5s before retrying set point");
	      printf("Sleeping 5s then retrying set_sweep_device (count=%d)...\n",icount);
	      ss_sleep(5000); // sleep 5s
	      status = set_sweep_device(set_camp_val,camp_params);
	      if(status == CAMP_SUCCESS)
		{
		  printf("set_camp_value: successfully set CAMP value of %f %s after %d retries\n",
			 set_camp_val,camp_params.units, icount);
		  restore_watchdog(); /* restore watchdog timeout */
		  //		  cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
		  return(status);
		}
	      else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

	    }
	}/* camp_watchdog failed... reconnect */
      gotCamp=FALSE;
      printf("set_camp_value: waiting 3s, then calling camp_reconnect...(count=%d)\n",icount);
      cm_msg(MINFO,"set_camp_value","waiting 3s then trying to reconnect to CAMP");
      ss_sleep(3000);
      status = camp_reconnect(); /* disconnects from CAMP then reconnects (tries 20 times) */
      if(status == CAMP_SUCCESS)
	{
	  cm_msg(MINFO,"set_camp_value","successfully reconnected to CAMP");
	  gotCamp=TRUE;
	  if(dc)
	    printf("set_camp_value: camp_reconnect was successful...retrying set_sweep_device...(count=%d)\n",icount);
	  status = set_sweep_device(set_camp_val,camp_params);
	  if(status == CAMP_SUCCESS)
	    {
	      cm_msg(MINFO,"set_camp_value","successfully set CAMP value=%f",set_camp_val);
  		  restore_watchdog(); /* restore watchdog timeout */
		  //	      cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	      return(status);
	    }
	  else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

	  printf("set_camp_value:failed to  set CAMP value =%f (retry=%d)\n",
		 set_camp_val,icount);  
      
	}
      printf("set_camp_value: waiting 5s then retrying\n");
      stat = iwait(5000,"set_camp_value");
      if(stat != SUCCESS) return stat;
     
    } /* end of for loop */
  
  cm_msg(MERROR,"set_camp_value","Cannot set CAMP value = %f after %d retries, stop the run and fix the problem",
	 set_camp_val,icount);  
  
  /* stop the run */
  camp_end(); /*  disconnect from CAMP */
  gotCamp=FALSE;
  restore_watchdog(); /* restore watchdog timeout */
  
  write_client_code(CAMP_ERR,SET,"frontend"); 
  stop_run();
  return  (status);
}


/*------------------------------------------------------------------------*/
INT camp_reconnect(void)
/*------------------------------------------------------------------------*/
{
  char *msg;
  INT status,stat;
  INT max_err=20;
  INT icount;

  /* calling programs have already set up long midas watchdog.
     Not called from the main programs */

  status = camp_clntEnd(); 
  if(status != CAMP_SUCCESS) 
    {
      msg = camp_getMsg();
      if( *msg != '\0' ) 
	{
	  printf("camp_reconnect: failure from camp_clntEnd\n");
	  printf( "CAMP error msg is \"%s\"\n", msg );
	  cm_msg(MERROR,"camp_reconnect","Failure from camp_clntEnd. Camp error message:\"%s\"",msg);
	}
    }
  else
    printf("camp_reconnect: success from camp_clntEnd\n");
  
  for(icount=0; icount < max_err; icount++)
    { 
      status = camp_init(camp_params);
      if( status == CAMP_SUCCESS  )
	{  /* camp_init sends its own message */
	  printf("camp_reconnect: successfully reconnected to camp\n");
          status=camp_watchdog(); /* can we talk to CAMP? */
	  if(status == CAMP_SUCCESS)
	      return (status);
	}

      icount++;
      printf("camp_reconnect: waiting 7s then retrying (retry=%d)\n",icount);

      stat = iwait(7000,"camp_reconnect" );
      if(stat != SUCCESS) return stat;      
    }
  cm_msg(MERROR,"camp_reconnect","Could not reconnect to CAMP");
  return (status);
}


/*------------------------------------------------------------------------*/
INT camp_watchdog(void)
/*------------------------------------------------------------------------*/
{
  /* Access camp to keep connection alive

     everything calling this has set a long midas watchdog parameter
  */
  char *msg;
  INT status;
  INT camp_errcount;

  printf("camp_watchdog: starting\n");

  status = campSrv_cmd("sysGetLogActs");
  if(status != CAMP_SUCCESS) 
    {
      camp_errcount++;
      msg = camp_getMsg();
      if( *msg != '\0' ) 
	{
	  printf("camp_watchdog: failure from campSrv_cmd\n");
	  printf( "CAMP error msg is \"%s\"\n", msg );
	  cm_msg(MERROR,"camp_watchdog","Failure from campSrv_cmd; camp error message:\"%s\"",msg);
	}
    }
  return (status);
}

/*-------------------------------------------------------------------*/
INT read_sweep_dev(float *read_camp_val, CAMP_PARAMS camp_params)
/*-------------------------------------------------------------------*/
{
  INT icount,status;
  float rcv;
  /*  BOOL watchdog_flag;
      DWORD watchdog_timeout; */


  /* camp access can take some time so set watchdog for 5 min 
    cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  // 5 min for reconnect 5*60*1000 
*/
  set_long_watchdog(gbl_long_watchdog_time); /* 2 min for reconnect 2*60*1000 */

  status = read_sweep_device(&rcv, camp_params); 
  if(status == CAMP_SUCCESS)
    {
      *read_camp_val=rcv;
      //      cm_set_watchdog_params(watchdog_flag, watchdog_timeout);/* restore watchdog timeout */
      restore_watchdog();/* restore watchdog timeout */
      return status;
    }
  else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

  /* failure */
  printf("read_sweep_dev: Error attempting to read sweep value\n");
  
  for(icount=0; icount < 3; icount++)
    { 
      if(gotCamp)
	{
	  status=camp_watchdog(); /* can we talk to CAMP? */
	  if(status == CAMP_SUCCESS)
	    {  /* try to read the device again */
	      printf("Retrying read_sweep_device (count=%d)...\n",icount);
	      status = read_sweep_device(&rcv,camp_params);
	      if(status == CAMP_SUCCESS)
		{
		  printf("read_sweep_dev: successfully read CAMP value of %f %s after %d retries\n",
			 rcv,camp_params.units, icount);
		  *read_camp_val=rcv;
		  restore_watchdog();/* restore watchdog timeout */
		  //	  cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
		  return(status);
		}
	      else if (status == -1) gotCamp=FALSE; /* don't have camp any more */
	    }
	}/* camp_watchdog failed... reconnect */
      gotCamp=FALSE;
      printf("read_sweep_dev: calling camp_reconnect...(count=%d)\n",icount);
      status = camp_reconnect(); /* disconnects from CAMP then reconnects (tries 20 times) */
      if(status == CAMP_SUCCESS)
	{
	  gotCamp=TRUE;
	  if(dc)
	    printf("camp_reconnect was successful...retrying read_sweep_device...(count=%d)\n",icount);
	  status = read_sweep_device(&rcv,camp_params);
	  if(status == CAMP_SUCCESS)
	    {
	      cm_msg(MINFO,"read_sweep_dev","successfully read CAMP value=%f after reconnecting to CAMP (count=%d)",
		     rcv,icount);
	      *read_camp_val=rcv;
	      restore_watchdog();/* restore watchdog timeout */
	      // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	      return(status);
	    }
	  else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

	  
	  printf("read_sweep_dev:failed to  read CAMP sweep value  (retry=%d)\n",icount);  
      
	}
      printf("read_sweep_dev: waiting 5s then retrying\n");
      status = iwait(5000,"read_sweep_dev" );
      if(status != SUCCESS)
	return status;
    } /* end of for loop */
  
  cm_msg(MERROR,"read_sweep_dev","Cannot read CAMP sweep device after %d retries, stop the run and fix the problem",
	 icount);  
  
  /* stop the run */
  camp_end(); /*  disconnect from CAMP */
  gotCamp=FALSE;
  restore_watchdog();/* restore watchdog timeout */
  // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  stop_run();
 
  return  (status);
}


#endif  /* CAMP */

#ifdef EPICS_ACCESS

/*-----------------------------------------------------------------------*/
INT set_epics_incr(void)
/*-----------------------------------------------------------------------*/
{
  /* set an epics value
     retry on failure
     stop the run after n retries
  */
  INT status;
  INT num_retries=10;
  // BOOL watchdog_flag;
  // DWORD watchdog_timeout;

  epics_params.Epics_bad_flag = 0;
  /* retrying can take some time so set watchdog for 5 min 
    cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  // 5 min for reconnect 5*60*1000 
  set_long_watchdog(gbl_long_watchdog_time );  // 3 min for reconnect 3*60*1000 
*/
  status = EpicsIncr( &epics_params );
  if(status == SUCCESS)
    {
      restore_watchdog(); /* restore watchdog timeout */
      //      cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
      epics_params.Epics_bad_flag = 0;
      return (status);
    }
  else
    {
      if(status == -1 )
	{
	  /* serious error from reading/writing Epics device 
	     try to reconnect  */
	  status = EpicsReconnect(&epics_params);
	  if(status != SUCCESS)
	    {
	      gbl_epics_live=FALSE; /* cannot maintain Epics live */
	      printf("\n  *** set_epics_incr: Error  setting %s. Stop & restart run after checking Epics device *** \n",
		     fs.output.e1n_epics_device);
	      printf("set_epics_incr: calling set_client_flag with FAILURE\n");
	     
	      restore_watchdog(); /* restore watchdog timeout */
	     
	      stop_run();
	      return(FE_ERR_HW);
	    }
	}
      epics_params.Epics_bad_flag++;
    }
  printf("set_epics_incr: Waiting 0.5s ..... then rechecking Epics voltage \n");
  status=iwait(500,"set_epics_incr"); 
  if(status != SUCCESS)return status;
 
  while(epics_params.Epics_bad_flag < num_retries) 
    {
      status = EpicsCheck( &epics_params);  /* Resends the set point */
      if(status==SUCCESS)
	{
	  if(dd[5])printf("set_epics_incr: success from  EpicsCheck after %d failures\n",epics_params.Epics_bad_flag);
	  epics_params.Epics_bad_flag = 0;
	  restore_watchdog(); /* restore watchdog timeout */
	  // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  return SUCCESS;
	}	
      else if (status == -1)
	{
	  cm_msg(MERROR, "set_epics_incr","Error accessing Epics device");
	  
	  stop_run();
	  return FE_ERR_HW;
	}
      else if (status == -2)
	{ /* this should be picked up in earlier checks */
	  cm_msg(MERROR, "set_epics_incr","Invalid set value outside min/max range ");
	 
	     restore_watchdog(); /* restore watchdog timeout */
	     stop_run();
	     //cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  return FE_ERR_HW;
	}

      epics_params.Epics_bad_flag++; /*   increment epics_params.Epics_bad_flag */
      printf("set_epics_incr: waiting 3s...... then retrying (attempt %d)\n",
	     epics_params.Epics_bad_flag);
      status = iwait(3000,"set_epics_incr");
      if(status != SUCCESS)return status;
    } /* while ends */
  
  /* cannot set Epics voltage */
     restore_watchdog(); /* restore watchdog timeout */
     // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  printf("\n  *** set_epics_incr: Given up trying to set %s. Stop & restart run after checking Epics device *** \n",
	 fs.output.e1n_epics_device);


  stop_run();
  return(FE_ERR_HW);
}

/*-----------------------------------------------------------------------*/
INT set_epics_val(void)
/*-----------------------------------------------------------------------*/
{
  INT status;
  // BOOL watchdog_flag;
  //DWORD watchdog_timeout;
  INT ncounts;

  /* Set the Epics Scan device to the value  epics_params.Epics_val
     Usually called at the beginning of a Scan  */

  epics_params.Epics_bad_flag = 0;
     
  /* EpicsNewScan involves  waiting so set watchdog for 3 min 
    cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  // 5 min for reconnect 5*60*1000 
  set_long_watchdog(gbl_long_watchdog_time);  // 3 min for reconnect 3*60*1000 
*/

  ncounts=0;
  while (ncounts < 10)
    {
      /* Note: EpicsNewScan sets Epics device, reads it back and checks correct value is set.

	   - it is called EpicsNewScan because it is called at the beginning of a scan to set a value (unless
           helicity is flipped -> scan direction reversed).
           At the beginning of a scan Epics device  will be set to 1 increment different from start value
           to give device time to settle (there may be big change in value). Then EpicsIncr will be called to set
           device to correct value.

      */

      status = EpicsNewScan( &epics_params ); /* set Epics value to  epics_params.Epics_val */
					      
      if(status == SUCCESS) 
	{
	  restore_watchdog(); /* restore watchdog timeout */
	  // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  return status;
	}
      if(status == -1)
	{
	  /* hardware error */ 
	  cm_msg(MERROR,"set_epics_val","trying to reconnect to Epics \n");
	  status=EpicsReconnect(&epics_params);
	  if(status != SUCCESS)
	    {
	      gbl_epics_live=FALSE; /* cannot maintain Epics live */
	      printf("\n  *** set_epics_val: Error  setting %s. Stop & restart run after checking Epics device *** \n",
		     fs.output.e1n_epics_device);
	      restore_watchdog(); /* restore watchdog timeout */
	    
	      stop_run();
	      return(FE_ERR_HW);
	    }
	  
	}
      else if (status == -2)
	{
	  cm_msg(MERROR,"set_epics_val","Epics step parameters are outside max or min value allowed");
	  restore_watchdog(); /* restore watchdog timeout */
	 
	  stop_run();
	  return (FE_ERR_HW);
	}
      else if (status == DB_NO_ACCESS)
	{
	  cm_msg(MERROR,"set_epics_val","no access to scan device. Check device is available and switched on");
	  restore_watchdog(); /* restore watchdog timeout */
	 
	  stop_run();
	  return(FE_ERR_HW);
	}
      
      printf("set_epics_val:  waiting 10s then retrying.... \n");
      iwait(10000,"set_epics_val"); /* wait 10s */
      ncounts++;
    }

  restore_watchdog(); /* restore watchdog timeout */
  // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  
  printf("set_epics_val:  Epics Hardware is not responding after %d retries ... make sure device is switched on \n",ncounts);

  stop_run();
  return (FE_ERR_HW);
}

/*-------------------------------------------------------------------------*/
INT read_epics_val(float *pval )
/*-------------------------------------------------------------------------*/
{
  INT status;
  // BOOL watchdog_flag;
  //DWORD watchdog_timeout;
  INT ncounts;

  /* Read the Epics Scan device into the value  epics_params.Epics_read */

  epics_params.Epics_bad_flag = 0;

  /* EpicsRead may involve waiting if we have to reconnect so set watchdog for 5 min 
    cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  // 5 min for reconnect 5*60*1000 
  set_long_watchdog(gbl_long_watchdog_time );  // 3 min for reconnect 3*60*1000 
*/
  ncounts=0;

  while (ncounts < 10)
    {
      status = EpicsRead( pval, &epics_params ); /* read value from Epics */
      if(status == SUCCESS) 
	{
	  restore_watchdog(); /* restore watchdog timeout */
	  // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  return status;
	}
      
  
      if(status == -1)
	{
	  /* no connection or timeout */ 
	  cm_msg(MERROR,"read_epics_val","trying to reconnect to Epics \n");
	  status=EpicsReconnect(&epics_params);
	  if(status != SUCCESS)
	    {
	      gbl_epics_live=FALSE; /* cannot maintain Epics live */
	      printf("\n  *** read_epics_val: Error  setting %s. Stop & restart run after checking Epics device *** \n",
		     fs.output.e1n_epics_device);
	      restore_watchdog(); /* restore watchdog timeout */
	    
	      stop_run();
	      return(FE_ERR_HW);
	    }
	  
	}
      
      printf("read_epics_val:  waiting 10s then retrying.... \n");
      iwait(10000,"read_epics_val"); /* wait 10s */
      ncounts++;
    }

  restore_watchdog(); /* restore watchdog timeout */
  // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  
  printf("read_epics_val: Epics Hardware is not responding after %d retries ... make sure device is switched on \n",ncounts);
 
  stop_run();
  return (FE_ERR_HW);
}


/*----------------------------------------------------------------------------*/
INT epics_reconnect(BOOL hel)
/*----------------------------------------------------------------------------*/
{ /* Input parameter hel
       hel = FALSE reconnects the Epics scan devices 
             TRUE  reconnects Epics helicity read  

   set long watchdog before calling EpicsReconnect */
  /*  BOOL watchdog_flag;
      DWORD watchdog_timeout; */
  INT status;
  
  status = FAILURE;
  printf("epics_reconnect starting with parameter=%d (TRUE for helicity)\n",hel);
  /* EpicsReconnect may involve waiting so set watchdog for 5 min 
     cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  // 5 min for reconnect 5*60*1000 
*/
  set_long_watchdog(gbl_long_watchdog_time ); 
  if(hel)
    { /* reconnect helicity */
      printf("calling ChannelReconnect to %s\n",Rname);
      Rchid=-1; /* clear old chid */
      Rchid = ChannelReconnect(Rname); /* reconnect */
     if(Rchid != -1)
       status = SUCCESS;
    }
  else
    {
      printf("calling EpicsReconnect\n");
      status=EpicsReconnect(&epics_params); /* reconnect the scan device */
      printf("after Reconntect, status = %d\n",status);
    }
  restore_watchdog(); /* restore watchdog timeout */
  // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  
  if(status != SUCCESS)
   {
     if(hel)
       cm_msg(MERROR,"epics_reconnect","cannot connect to Epics device %s",Rname);
     else
       cm_msg(MERROR,"epics_reconnect", "cannot connect to Epics device %s",
	      fs.output.e1n_epics_device);
     
   
     stop_run();
     return(FE_ERR_HW);
   }
  else    
    return SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT  epics_watchdog(EPICS_PARAMS *p_epics_params, BOOL hel)
/*----------------------------------------------------------------------------*/
{
  /* Set epics watchdog to a long time, then call EpicsWatchdog (if scan) 
           otherwise ChannelWatchdog 
  */
  
  // BOOL watchdog_flag;
  //DWORD watchdog_timeout;
  INT status;
  

  printf("epics_watchdog: starting\n");
   
  /* EpicsWatchdog will try to reconnect if channels have disconnected */
 
  if (hel)
    { /* helicity  */
      if(Rchid == -1)
	{
	  printf("epics_watchdog: Epics channel %s has disconnected\n",Rname);
	  return -1;
	}
    }
  else
    {
      if(dd[7])printf("epics_watchdog: epics_params.NaCell_flag=%d epics_params.XxWchid =%d  epics_params.XxRchid = %d\n",
	epics_params.NaCell_flag,	 epics_params.XxWchid, epics_params.XxRchid);

      if(epics_params.XxWchid == -1 || epics_params.XxRchid == -1)
	{
	  printf("epics_watchdog: Epics channel(s) have disconnected\n");
	  return -1;
	}
    }
 
  /* Either read the Epics Scan device into the value  epics_params.Epics_read
     or read the helicity  if hel is true.
     
     May involve waiting if we have to reconnect so set watchdog for 5 min */

  set_long_watchdog( gbl_long_watchdog_time);  /* 5 min for reconnect 5*60*1000 */
  if(hel)
    status = ChannelWatchdog(Rname, &Rchid); /* helicity  */
  else
    status =  EpicsWatchdog( p_epics_params );

  if(status != SUCCESS)printf("Error detected from watchdog (%d)\n",status);

  restore_watchdog(); /* restore watchdog timeout */
  return status;
 
}
#endif /* Epics */



/* ====================================================
   Routines for VME PSM frequency module  
   ====================================================
*/


/*---------------------------------------------------*/
INT init_freq_module(void)
/*---------------------------------------------------*/
{
  /* Initialize the hardware for the PSM frequency scan
  */

#ifdef HAVE_PSM

  INT status,Ncmx;
  char str[128];
  DWORD data;

  /* See if we can read something... is the module present */
  data = psmRegRead8(gVme, PSM_BASE, 0);  // expect 0x94 for PSMII ox84 for PSM
  if (data != 0x84 && data != 0x94)
    {
      //     printf ("Cannot read correct data from PSM Control Register 1  (0x%x). Expect 0x84 or 0x94. Retrying... \n",data);
      ss_sleep(2000); // sleep 2s (may still be resetting)

      data = psmRegRead8(gVme, PSM_BASE, 0);  // expect 0x94 for PSMII ox84 for PSM
      if (data != 0x84 && data != 0x94)
      {
         printf ("Cannot read correct data from PSM Control Register 1  (0x%x). Expect 0x84 or 0x94. \n",data);
         return  FE_ERR_HW;
      }
    }
  printf("Read 0x%x from PSM module\n",data);

#ifdef HAVE_PSMII      
      if(data != 0x94)
	{
	  printf("Detected PSM in crate. Expect a PSMII\n");
	  return FE_ERR_HW ;
	}
      else
	printf("Detected a PSMII in the VME Crate\n");
#else
      if(data != 0x84)
	{
	  printf("Detected PSMII in crate. Expect a PSM\n");
	  return FE_ERR_HW ;
	}
      else
	printf("Detected a PSM in the VME Crate\n");
#endif

  /* Initialize the PSM and set 1f freq to 1MHz, ref freq to 0.5MHz
     single tone mode, all gates enabled
  */
  status =  psmSetOneFreq_Hz(gVme, PSM_BASE, 10000000, 5000000);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"init_freq_module","Error return from psmSetOneFreq_Hz; cannot setup PSM");
      return FE_ERR_HW;
    }
  /* read Ncmx from the PSM and write it into the odb for rf_config to use */
  Ncmx = psmReadMaxBufFactor(gVme, PSM_BASE );
  sprintf(str,"/Equipment/FIFO_acq/frontend/output/psm/max cycles iq pairs (Ncmx)");
  if(dpsm)
    { 
      printf(" init_freq_module: read Ncmx from PSM as %d\n",Ncmx);
      printf("    now writing it to \"%s\" for rf_config\n",str);
    }
  status = db_set_value(hDB,0,str,&Ncmx,sizeof(Ncmx),1,TID_INT);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR," init_freq_module","Error writing Ncmx=%d to ODB Key\"%s\"; cannot setup PSM (%d)",Ncmx,str,status);
      return FE_ERR_HW;
    }
#endif
  return SUCCESS;
}


/*-------------------------------------------------------*/
INT init_freq_2 (void)
/*-------------------------------------------------------*/
{
  /* Called for all Type 2 freq table driven modes (i.e. NOT 20 and 2d)
     Load the PSM frequency table for type 2 */

#ifdef HAVE_PSM
  INT status;

  status = init_psm(gVme, PSM_BASE); /* reset and initialize psm */
  if(status != SUCCESS)
    {
      printf(" init_freq_2: error from init_psm\n");
      return(FE_ERR_HW);
    }

  status = psm_loadFreqFile(gVme, PSM_BASE, ppg_mode, random_flag);
  if(status != SUCCESS)
    {
      printf(" init_freq_2: error loading PSM frequency file\n");
      return(status);
    }
 
   /* if quad mode, loads the IQ pair file (or loads Idle) 
     for all enabled profiles  */
  status=load_iq_file(gVme, PSM_BASE);
  if(status != SUCCESS)
    return status;


  printf("init_freq_2: PSM is now ready, waiting for external strobe from PPG \n");
#endif
  return SUCCESS;
}



/*-------------------------------------------------------*/
INT init_freq_20(void)
/*-------------------------------------------------------*/
{
  /* Load the PSM frequency for type 20 (SLR) and 2d only
      (these are not table-driven )
  */
  INT status;

#ifdef HAVE_PSM
  printf(" init_freq_20: setting up PSM for experiment SLR (20) or 2d\n");
  if(init_psm(gVme, PSM_BASE) == -1) return(FE_ERR_HW);
  

  status = load_tuning_freq(gVme, PSM_BASE); /* load tuning freq if fREF profile is enabled */
  if (status != SUCCESS)
    return status;

  // If one_f profile is disabled, RF is not being used
  if( !fs.hardware.psm.one_f.profile_enabled)
    {
      cm_msg(MINFO,"init_freq_20","RF is disabled");
      return SUCCESS; // nothing to do
    }

  /* The Idle frequency is in  fs.hardware.psm.idle_freq__hz_ for BNMR/PSM 
  */

  printf("\n init_freq_20: Now loading PSM frequency %dHz into IDLE\n",
	 fs.hardware.psm.idle_freq__hz_);
  status = psmLoadIdleFreq_Hz (gVme, PSM_BASE,fs.hardware.psm.idle_freq__hz_);
  if(status != -1) 
    printf(" init_freq_20: read back PSM Idle frequency as %dHz\n",status);
  else
    {
      cm_msg (MERROR," init_freq_20"," error writing PSM idle frequency %dHz",
	      fs.hardware.psm.idle_freq__hz_);
      return(FE_ERR_HW);
    }
  
  printf("\n init_freq_20: Now writing internal strobe \n");
  psmFreqSweepStrobe(gVme, PSM_BASE);


  /* IQ pairs */
  
  /* if quad mode, loads the IQ pair file (or loads Idle)
     for all enabled profiles
  */
  status = load_iq_file(gVme, PSM_BASE);  /* loads I,Q pair for single tone mode (Hubert's fix) */
  if(status != SUCCESS)
    {
      printf("init_freq_20: error from load_iq_file\n");
      return status;
    }
  status = psmFreqSweepMemAddrReset(gVme, PSM_BASE);

  status = iwait(500,"init_freq_20"); /* sleep 0.5s*/
  if(status != SUCCESS)return status;

  printf(" init_freq_20: Sending an internal strobe command to the PSM  (type 20) \n");
  psmFreqSweepStrobe (gVme, PSM_BASE); 
#endif

  return SUCCESS;
}

/*-------------------------------------------------------*/
INT init_freq_1(void)
/*-------------------------------------------------------*/
{

  /* Setup the PSM Type 1 experiment (except Type 10 (scalers)
     where nothing is actually scanned 
  */


#ifdef HAVE_PSM
  INT status;
  printf(" init_freq_1: setting up PSM for experiment type 1 (except 10)\n");
  /* Initialize psm  */
  if(init_psm(gVme, PSM_BASE) == -1) 
    return(FE_ERR_HW);
  

  status = load_tuning_freq(gVme, PSM_BASE); /* load tuning freq if fREF profile is enabled */
  if (status != SUCCESS)
    return status;
  
  /* if quad mode, loads the IQ pair file (or loads Idle) 
     for all enabled profiles  */
  status=load_iq_file(gVme, PSM_BASE);
  if(status != SUCCESS)
    return status;

  printf("  init_freq_1: PSM is now waiting for external strobe (type 1) \n");
  

#endif

  return SUCCESS;
}






/*--------------------------------------------------*/
INT get_int_version(char *p, int len)
/*--------------------------------------------------*/
{
  int i;
  char *q;
  char digits[6]="";

  q=&digits[0];
  for (i=0; i<len; i++)
    {
      if( isdigit(*p))
	{
	  strncpy(q,p,1);
	  q++;
	}
      p++;
    }
  printf("%s\n",digits);
  return(atoi(digits));
}

INT write_client_alarm_message(char* message)
{
  char string[80];
  char str[]="/alarms/alarms/client alarm/alarm message";
  INT status;
  strncpy(string,message,79); // truncate message if necessary
  status = db_set_value(hDB,0,str,string,sizeof(string),1,TID_STRING);
  if(status != DB_SUCCESS)
    cm_msg(MERROR,"set_client_alarm_message","Error writing \"%s\" to \"%s\"(%d)",
	   string,str,status);
  return status;

}
/*---------------------------------------------------------
*/
INT set_threshold_alarm(INT alarm_num)
/*---------------------------------------------------------
*/
{

  INT status;
  
  const INT max_defined = 4; /* number of alarms defined */
  static char *threshold_name[]= {
    "none","Reference", "Previous","Laser Power"
  };
  char string[80];
  char str[]="/alarms/alarms/thresholds/alarm message";

  if(dd[3])printf("set_threshold_alarm: starting with alarm_num=%d last threshold test failed =%d at cycle %d\n",
	 alarm_num,cyinfo.last_failed_thr_test, cyinfo.cycle_when_last_failed_thr );

  cyinfo.cycle_when_last_failed_thr= gbl_CYCLE_N ;

  if(cyinfo.last_failed_thr_test == alarm_num) 
    {
      if(dd[3])printf("set_threshold_alarm: alarm is already set; returning\n");
      return SUCCESS; /* no change; alarm is already set */
    }
  
  if(alarm_num < max_defined)
    sprintf(string,"%s threshold has tripped ! ",threshold_name[alarm_num]);
  else
    sprintf(string,"Skipping cycles after %s threshold trip...",
	    threshold_name[cyinfo.last_failed_thr_test]);

  /*printf("string=%s \n",string);*/

  status = db_set_value(hDB,0,str,string,sizeof(string),1,TID_STRING);
  if(status != DB_SUCCESS)
    cm_msg(MERROR,"set_threshold_alarm","Error writing \"%s\" to \"%s\"(%d)",
	   string,str,status);

  cyinfo.last_failed_thr_test= alarm_num; /* alarm should now go off */
  if(dd[3])printf("set_threshold_alarm: cyinfo.last_failed_thr_test is set to %d; returning\n",alarm_num);
  return SUCCESS;
}


#include "trandom.c" /* BNMR/BNQR 1a,1b and type 2a,2b,2c randomize freq values */

void dbug(void)
{
  printf("\nSet the following to 1 to turn on specific debugging:\n");
  printf("dd     debugs are in sis3801_setup_bor and poll_event\n");
  printf("dd[1]    frontend_loop, begin_run, debug scalers, histo banks\n");
  printf("dd[2]   indicates if running in cycle in frontend loop\n");
  printf("dd[3]     reference threshold\n");
  printf("dd[5]     Epics scan (higher level ... bnmr_epics.c)\n");
  printf("dd[6]     Epics scan (lower level... EpicsStep.c)\n");
  printf("dd[7]     Epics watchdog,\n");
  printf("dd[8]     display: for debugging, don't overwrite  scan values in cycle_start \n");
  printf("dd[9]     client flag; automatic stop; delayed transition if available\n");
  printf("dd[10]    frequency scan  \n");
  printf("dc     Camp scan (1c and 1j) \n");
  printf("dd[11]    Silent mode (d11=1 for POL); do not display anything \n");
  printf("dpsm   Debug PSM; also set pdd=1 for trPSM (BNQR only)\n");
  printf("manual_ppg_start     Set to 1 for manual ppg start\n");  
  printf("dbyt   Loads bytecode.try instead of bytecode.dat \n");
  printf("dd[13]   Randomizing frequency values (can also use dr=1 for lower level)\n");
  printf("dd[12]     Helicity (direct Epics channel access)\n");
  printf("dhw     Set to 1 to ignore failed helicity flip & disable helicity warnings(for testing)\n");
  printf("dd[17] debug e2e \n");
  printf("dl 0:cm_yield; 1:histo_read; 2:histo_process; 3:scalers_cyc_rd; 4:scalers_incr 5: cycle_start 6:cmyield feloop VMEIO_6\n");
}

/*-----------------------------------------------------*/
INT set_long_watchdog(DWORD my_watchdog_timeout)
/*-----------------------------------------------------*/
{
 /* Set a long midas watchdog timer 
    Parameter  my_watchdog_timeout in ms 
  */
  DWORD k9;




  if(gbl_dachshund_flag)
    {  /* check flag */
      cm_get_watchdog_params(&gbl_watchdog_flag, &k9); /* returns k9  */
      printf("set_long_watchdog: long watchdog is set already, currently watchdog is set to %d ms\n",k9);
      return SUCCESS;
    }

  cm_set_watchdog_params(gbl_watchdog_flag, my_watchdog_timeout );
  // if(dd[12] || dd[7] ) 
    printf("set_long_watchdog:  gbl_watchdog_timeout=%d; set timeout to %d ms ; \n",
	   gbl_watchdog_timeout, my_watchdog_timeout);
  
  gbl_dachshund_flag = TRUE;
  return SUCCESS;
}

/*-----------------------------------------------------*/
INT restore_watchdog(void)
/*-----------------------------------------------------*/
{
  /* restore the midas watchdog to the original value 
   */
  DWORD my_timeout;

  if(!gbl_dachshund_flag)
    {  /* check flag */
      printf("restore_watchdog: set_long_watchdog has not been called previously\n");
      return SUCCESS;
    }
  /* temp check... */
  cm_get_watchdog_params(&gbl_watchdog_flag, &my_timeout);
  if(dd[12] || dd[7]) 
    printf("restore_watchdog: watchdog value is presently %d ms; restoring it to %d ms; gbl_watchdog_flag=%d\n",
	 my_timeout, gbl_watchdog_timeout, gbl_watchdog_flag);
  cm_set_watchdog_params(gbl_watchdog_flag, gbl_watchdog_timeout);  /* e.g. 5 min for reconnect = 5*60*1000ms */
  gbl_dachshund_flag = FALSE;
  return SUCCESS;
}


INT assign_2_params(void)
{
  INT status,ninc;
  INT first_freq_bin=0; // set to 0 to get rid of warnings

  e2e_flag=e2a_flag=e2f_flag=random_flag=0;
  //n_requested_bins = fs.output.num_dwell_times;
  pulsepair_flag=FALSE; // default
#ifdef HAVE_SIS3820 
  if(discard_first_bin)
    {
      printf("assign_2_params: discard_first_bin is TRUE; n_his_bins=%d n_requested_bins=%d n_bins=%d\n",
	     n_his_bins, n_requested_bins,n_bins);
    }
#endif
  /* none of these are used except for e2e,e2a,e2f so set them to zero just in case */
  gbl_ndepthbins=gbl_npostbins=0;
  gbl_ntuple_width_s=gbl_ntuple_width_h=0;
  gbl_nmidsection_h=0;
  
  if (  strncmp(fs.input.experiment_name,"2a",2) == 0)
    e2a_flag = TRUE;
  else if (  strncmp(fs.input.experiment_name,"2e",2) == 0)
    e2e_flag = TRUE;
  else if  (strncmp(fs.input.experiment_name,"2f",2)  == 0) 
    e2f_flag = TRUE;
  else
    return SUCCESS;
 
 /* check for random frequencies */
  if (fs.input.randomize_freq_values)/* random freq */
    random_flag = TRUE;
 
  if( e2f_flag)
    {
      // All bins are histogrammed directly, but freq bins may be randomized
      gbl_nprebins = fs.input.num_rf_on_delays__dwell_times_;  // number of bins before RF. These are histogrammed directly
      gbl_ntuple_width_h = gbl_ntuple_width_s = gbl_nRFbins = 1; /* fixed at 1 RF bin. RF IS randomized  */
      gbl_npostbins= fs.output.e2e_num_post_ntuple_bins; // number of bins after RF. These are histogrammed directly
      gbl_nmidsection_h = gbl_ntuple_width_h *  fs.output.num_frequency_steps;
    }
   
  else if( e2a_flag)
    {
      /*  max ntuple is  fs.output.num_frequency_steps */
      gbl_nprebins = fs.input.num_rf_on_delays__dwell_times_;
      //  gbl_num_sbins_ch = fs.output.num_dwell_times; /* number of scaler bins per channel */

      if (fs.input.e2a_pulse_pairs)
	{  /* pulse pairs */
	  e2a_pulse_pairs=TRUE; /* flag */ 
	  gbl_nRFbins=2; /* scaler will have 2 bins per frequency */
	  gbl_ntuple_width_s = gbl_nRFbins; /* width of ntuple in scaler data */

	  if(fs.output.e2a_pulse_pairs_mode < 0 || fs.output.e2a_pulse_pairs_mode > 3)
	    if (fs.output.e2a_pulse_pairs_mode != 9 )
	      {  /* 9 means pair mode disabled */
		printf("Invalid value for fs.output.e2a_pulse_pairs_mode (%d)\n",
	       fs.output.e2a_pulse_pairs_mode);
		return -1;  
	      }
	  if(fs.output.e2a_pulse_pairs_mode > 0 && fs.output.e2a_pulse_pairs_mode < 4 )
	    { /* pulse_pairs compaction modes -> userbit1 action : 0=pairs 1=first 2=second 3=diff */
	      n_his_bins = n_requested_bins -  fs.output.num_frequency_steps; /* end up with less bins */
	      gbl_ntuple_width_h =  gbl_ntuple_width_s /2;
	    }
	  else
	    gbl_ntuple_width_h =  gbl_ntuple_width_s ; /* non-compaction mode */
	}
      else
	{ /* not pulse pair mode */
	  e2a_pulse_pairs=FALSE; /* flag */
	  gbl_ntuple_width_h = gbl_ntuple_width_s = gbl_nRFbins = 1; /* fixed at 1 RF bin */
	 
	}
    }



  else if( e2e_flag)
    {
      /*  max ntuple is  fs.output.num_frequency_steps */
      gbl_nprebins = fs.input.num_rf_on_delays__dwell_times_;

      n_his_bins =  fs.output.e2e_num_histo_bins_per_ch; /* filled by rf_config */
      gbl_npostbins= fs.output.e2e_num_post_ntuple_bins; /* filled by rf_config. Includes postrfbeamon + beam_off dwelltimes */
      gbl_ndepthbins =  fs.output.e2e_ntuple_depth__bins_;
      gbl_ntuple_width_h = fs.output.e2e_histo_ntuple_width__bins_;
      gbl_ntuple_width_s =  fs.input.e2e_num_dwelltimes_per_freq;     
      gbl_nmidsection_h = gbl_ntuple_width_h *  fs.output.num_frequency_steps;
      gbl_nRFbins = 1; /* fixed at 1 RF bin  for e2e */

      pulsepair_flag=TRUE;
    }


  printf("assign_2_params: Parameter values: \n");
 
  printf("Mode %s selected\n",ppg_mode); 
  printf("Number of prebins : %d\n",gbl_nprebins);
  printf("Number of postbins : %d\n",gbl_npostbins);
  printf("Number of depthbins : %d\n",gbl_ndepthbins);
  printf("Number of RFbins : %d\n",gbl_nRFbins);
  printf("Number of scaler bins/channel: %d \n", 
	 fs.output.num_dwell_times); /* number of scaler bins per channel */
  printf("Number of histogram bins/channel: %d \n", n_his_bins);
  printf("Number of frequency steps: %d\n", fs.output.num_frequency_steps);
  printf("Randomize data : %d\n",random_flag);
  
  if (e2a_flag)
    {
      printf("Enable e2a Pulse pair mode %d\n",fs.input.e2a_pulse_pairs);
      if(fs.input.e2a_pulse_pairs)
	{
	  printf("e2a pulse pair mode param (0-3) : %d\n",fs.output.e2a_pulse_pairs_mode);
	  pulsepair_flag=TRUE;
	}
    }

  
  /* check */

  if( (gbl_nprebins +  gbl_npostbins + fs.output.num_frequency_steps*gbl_ntuple_width_s + gbl_ndepthbins)
      !=   fs.output.num_dwell_times) 
    {
      printf("assign_2_params: error in calculation for scaler width per channel\n");
      cm_msg(MERROR,"assign_2_params","error in calculation for scaler width per channel");
      return DB_INVALID_PARAM;
    }
  
  if( (gbl_nprebins +  gbl_npostbins + fs.output.num_frequency_steps*gbl_ntuple_width_h)
      !=  n_his_bins)
    {
      printf("assign_2_params: error in calculation for histogram width per channel\n");
      cm_msg(MERROR,"assign_2_params","error in calculation for histogram width per channel");
      return DB_INVALID_PARAM;
    }

 
  /* Call this whether random or not - for type 2 and random, freqs are randomized each cycle */
  printf("Calling random_set_scan_params with ninc=%d\n",ninc);
  status = random_set_scan_params(&ninc);
  if(status != SUCCESS)
    { 
      printf("assign_2_params: error from random_set_scan_params\n");
      return status;
    }
  else
    if(dd[16])printf("assign_2_params: returned successfully from random_set_scan_params\n");
  
  printf("after random_set_scan_params, ninc=%d\n",ninc);
  
  if(dd[16]){
    int i;
    printf("First 10 pointers, random_flag=%d\n",random_flag);
    for (i=0; i<ninc; i++)
      {
	if(i<10)printf("pseqf[%d]=%d\n",i,pseqf[i]);
      }
  }

  if(ninc !=  fs.output.num_frequency_steps)
    {
      printf("Error ninc = %d   fs.output.num_frequency_steps=%d\n",ninc, fs.output.num_frequency_steps);
      return -1;
    }

  if(random_flag && e2a_flag)
    {  /* 2a random */

      if(! fs.input.e2a_pulse_pairs)
	first_freq_bin= fs.output.num_dwell_times - fs.output.num_frequency_steps;
      else
	first_freq_bin= fs.output.num_dwell_times - 2 *fs.output.num_frequency_steps;
      
      if(first_freq_bin < 0)
	{
	  printf("Number of frequency steps (%d) > num dwell times (%d). Cannot determine 1st freq bin", fs.output.num_frequency_steps, fs.output.num_dwell_times );
	  return DB_INVALID_PARAM;
	}
      printf("2a first RF bin will be bin=%d\n",first_freq_bin);
    } /* end of 2a random */
  if(first_freq_bin != gbl_nprebins)printf("first_freq_bin=%d gbl_nprebins=%d\n",first_freq_bin,gbl_nprebins);
 
  return SUCCESS;
}




#ifdef HAVE_PSM

INT write_psmregs(void)
{
  FILE *psmfile;
  char filename[64];
  INT status;
  
  // cannot use /isdaq/data1 because /isdaq is not mounted from lxbn[mq]r
  sprintf(filename,"%s/psmlog/psmlog.txt",getenv("EXPERIM_DIR"));
  
  if(dpsm)printf("****** write_psmregs starting *******\n");
  
  psmfile = fopen (filename,"a");
  status = iwait(100,"write_psmregs"); /* sleep 100ms */
  if(status != SUCCESS)return status;
  
  if(psmfile == NULL)
    {
      cm_msg(MERROR,"write_psmregs","could not open log file %s",filename);
      return SUCCESS; /* non-fatal error */
    }
  
  printf("\n\n==========  Run %d  PPG Mode %s ===========\n", 
	 gbl_run_number, fs.input.experiment_name);
  
  fprintf(psmfile,"\n\n==========  Run %d  PPG Mode %s ===========\n", 
	  gbl_run_number, fs.input.experiment_name);
  
  
  // psmGetStatus(gVme, PSM_BASE, psmfile);
  fclose (psmfile);
  if(psmfile==NULL)
    printf("file %s is closed\n",filename);
  return SUCCESS;
}

#endif    

INT thresh_check(double prev_hel_thr, double current_hel_thr, double ref_hel_thr)
{
  /* Called from histo_process()  for threshold check on cycle with hel Up or Down
     Returns 1 if cycle fails tolerance tests 
          or 0 if cycle is in tolerance

     Threshold fails if hot_threshold > 0 (debug - testing thresholds only!! )
  */
  INT status;
  double ratio,rtmp;
  char hel_state[5];
 

  if(gbl_ppg_hel == HEL_DOWN)
    sprintf(hel_state,"Down");
  else
    sprintf(hel_state,"Up");

    if(dd[3])
    {
      printf("thresh_check: checking thresholds for helicity %s (%d)\n",hel_state,gbl_ppg_hel);
      printf("  prev_hel_thr = %7.0f current_hel_thr= %7.0f; ref_hel_thr=%7.0f\n",
	     prev_hel_thr, current_hel_thr,  ref_hel_thr);
    

      printf("thresh_check: hot_threshold_trip = %d\n", hot_threshold_trip );
    }
  /* Start of Test 1 */
  if (fs.hardware.cycle_thr1 > 0.f)
    {
      if(dd[3])
	printf("thresh_check: Test1 starting: Cycle_thr level:%f; current %f; ref %f\n",
	       fs.hardware.cycle_thr1,
	       current_hel_thr,
	       ref_hel_thr);
      /* check total sum versus corresponding thr */
      if (ref_hel_thr != 0)
	ratio = abs(ref_hel_thr - current_hel_thr) / ref_hel_thr;
      else
	ratio = 0.;
      if(dd[3]) 
	printf("thresh_check: test1 hel %s ratio=%.3f thr1=%.3f \n",
		   hel_state,ratio, ((double)fs.hardware.cycle_thr1 / 100.) );

      if ( (ratio > (double)fs.hardware.cycle_thr1 / 100.) || hot_threshold_trip ==1 ) // hot_threshold_trip for testing only
	{
	  /* Test 1 fails */
	  if(hot_threshold_trip == 1)
	    {
	      hot_threshold_trip = 0; // clear
	      cm_msg(MINFO,"thresh_check","Hel %s cycle out of tolerance due to hot_threshold_trip =1 (testing)", hel_state);
	      printf("thresh_check: Test1 fails due to  hot_threshold_trip = 1 (testing current cycle threshold)\n");
	    }
	  else
	    {
	      cm_msg(MINFO,"thresh_check","Hel %s cycle below Reference Cycle threshold (%5.3f);  out of tolerance",
		     hel_state,ratio);
	      if(dd[3])
		printf("thresh_check: Test1 fails, Ref = %7.0f; Current = %7.0f; Ratio (%5.3f) > ref cycle thr/100 (%5.3f)\n",
			      ref_hel_thr,  current_hel_thr,ratio, 
			      ((double)fs.hardware.cycle_thr1 / 100.));
	    }   
	  skip_cycle = TRUE;
	  cyinfo.cancelled_cycle ++;

	  gbl_tolerance_flag=TRUE; // out of tolerance flag used to ensure threshold alarms are cleared

	   // fs.hardware.skip_ncycles_out_of_tol has been set to 0 for const1f mode
	  if(cyinfo.ncycle_sk_tol <  fs.hardware.skip_ncycles_out_of_tol)
	    cyinfo.ncycle_sk_tol = fs.hardware.skip_ncycles_out_of_tol;

	  if(!dd[11])printf("thresh_check: skipping %d cycles out-of-tol\n", cyinfo.ncycle_sk_tol);
	  set_threshold_alarm(1);
	  // printf("test 1 gbl_tolerance_flag=%d\n",gbl_tolerance_flag);
	  if(dd[18])
	    printf("thresh_check: Failed threshold test 1. Returning with skip_cycle=%d  gbl_FREQ_n=%d\n",skip_cycle,gbl_FREQ_n);
	  return 1; /* fail Test 1 */
	}	

      /* Test 1 passes  */
      if(dd[3])printf("thresh_check: passed test 1 for hel %s\n",hel_state);
    }  /* end of Test 1 */
  
  
  /*   Start of  Test 2    */
  if (fs.hardware.cycle_thr2 > 0.f && prev_hel_thr>0.f )
    {
      if(dd[3])
	printf("thresh_check: Test 2 Starting for hel %s... Cycle_thr level:%f; current %f; prev %f\n",
	       hel_state, fs.hardware.cycle_thr2,
	       current_hel_thr,
	       prev_hel_thr);
      /* check total sum versus corresponding thr */
      rtmp =  ((prev_hel_thr + current_hel_thr)/2) ;
      if (rtmp!=0)
	ratio = abs(prev_hel_thr - current_hel_thr) / rtmp;
      else
	ratio = 0.;
      if(dd[3])
	printf("thresh_check: test2 hel %s ratio=%.3f  thr2=%.3f \n",
		   hel_state, ratio, ((double)fs.hardware.cycle_thr2 / 100.) );
      if ( (ratio > ((double)fs.hardware.cycle_thr2 / 100.)) || hot_threshold_trip == 2)
	{
	  /* Test 2 fails  */
	  if(hot_threshold_trip == 2)
	    {
	      hot_threshold_trip = 0; // clear
	      cm_msg(MINFO,"thresh_check","Hel %s cycle out of tolerance due to hot_threshold_trip = 2 (testing Prev Cycle threshold)", hel_state);
		printf("thresh_check: Test2 fails due to  hot_threshold_trip = 2 (testing Prev Cycle threshold)\n");
	    }
	  else
	    {
	      cm_msg(MINFO,"thresh_check","Hel%s cycle below Previous Cycle threshold  (%5.3f);  out of tolerance",
		     hel_state,ratio);
	      
	      if(dd[3]) 
		printf("thresh_check: Hel%s Prev = %7.0f; Current = %7.0f; Ratio (%5.3f) > prev cycle thr/100 (%5.3f)\n",
			       hel_state,prev_hel_thr,  
			       current_hel_thr,ratio, 
			       ((double)fs.hardware.cycle_thr2 / 100.));   
	    }
	  skip_cycle = TRUE;
	  cyinfo.cancelled_cycle ++;

          gbl_tolerance_flag = TRUE; // used to clear threshold alarms when not skipping any cycles out-of-tol
       
	  // fs.hardware.skip_ncycles_out_of_tol has been set to 0 for const1f mode
	  if(cyinfo.ncycle_sk_tol < fs.hardware.skip_ncycles_out_of_tol)
	    cyinfo.ncycle_sk_tol = fs.hardware.skip_ncycles_out_of_tol;
	   
	  if(!dd[11])printf("thresh_check: skipping %d cycles out-of-tol\n", cyinfo.ncycle_sk_tol);
	  set_threshold_alarm(2);
	  //  printf("test 2 gbl_tolerance_flag=%d\n",gbl_tolerance_flag);
	  if(dd[18])
	    printf("thresh_check: Failed threshold test 2. Returning with skip_cycle=%d  gbl_FREQ_n=%d\n",skip_cycle,gbl_FREQ_n);

	  return 1;  /* Test 2 fails */
	}
      if(dd[3])printf("thresh_check: passed test 2 for hel%s \n",hel_state);
    } /* end of Test2 */
  
  /* 
       Passed all tolerance tests 
  */
  if(dd[3])
    {
      printf("Passed all tolerance tests\n");
      printf("gbl_tolerance_flag is %d cyinfo.ncycle_sk_tol=%d\n",gbl_tolerance_flag,cyinfo.ncycle_sk_tol);
    }
  if (cyinfo.ncycle_sk_tol <=0 && !gbl_tolerance_flag)
    return 0; /* cycle is good; histogram it */
 

  if (cyinfo.ncycle_sk_tol > 0)
    {
      /* Cycle is being skipped; counting down to come back into tolerance */
      skip_cycle = TRUE;
      cyinfo.cancelled_cycle++;
      if(!dd[11])printf("thresh_check: Hel%s Cycle is in tolerance but cyinfo.ncycle_sk_tol still positive (%d)\n",
			hel_state,cyinfo.ncycle_sk_tol);
      set_threshold_alarm(4); /* skip cycles out of tol alarm (if necessary) */
      cyinfo.ncycle_sk_tol--; 

      return 0; /* still skipping cycles (skip_cycle is true) */
    }
  

  /* 
     Next cycle will be histogrammed after being out of tolerance
     
          (  cyinfo.ncycle_sk_tol == 0 ) 
  */
 
  printf("Next new cycle will be histogrammed; clearing gbl_tolerance_flag\n");
  gbl_tolerance_flag = FALSE; /* clear flag */
  cyinfo.last_failed_thr_test = 0;  /* clear this value */

  /* make sure odb is updated or reset will not work for long!  */
  if(!hInfo)printf("\n hInfo not assigned! \n");
  status = db_set_value(hDB, hInfo, "last failed thr test", 
	       &cyinfo.last_failed_thr_test, sizeof(cyinfo.last_failed_thr_test) , 1, TID_DWORD);
  if(status != SUCCESS)
    cm_msg(MERROR,"thresh_check","error setting \"last failed thr test\" to zero (%d)",status);
  else
    {
      status = al_reset_alarm("thresholds");
      
      if(status == AL_RESET)
	printf("thresh_check: threshold alarm is now reset & cyinfo.last_failed_thr_test is now zero\n");
      else if (status == AL_INVALID_NAME)
	cm_msg(MERROR,"thresh_check","Error resetting threshold alarm: invalid name \"thresholds\" ");
      else if (status != AL_SUCCESS)
	cm_msg(MERROR,"thresh_check","Error resetting threshold alarm (%d) ",status);
    }
  
  /* DA added boolean gbl_repeat_scan. 
     gbl_repeat_scan changed to odb param fs.hardware.out_of_tol_repeat_scan.
     user sets this param true to repeat the scan */
  if(exp_mode == 1 && fs.hardware.out_of_tol_repeat_scan) 
    {
      cm_msg(MINFO,"thresh_check","out_of_tol_repeat_scan parameter is TRUE, repeating scan for Type 1 run");
      /* must restart the current scan for mode 1 */
      if (epics_flag) 
	{
#ifdef EPICS_ACCESS
	  gbl_inc_cntr = 0;
	  gbl_SCAN_N ++;
	  if(epics_params.Epics_inc > 0)
	    epics_params.Epics_val= epics_params.Epics_start - epics_params.Epics_inc;
	  else
	    epics_params.Epics_val= (epics_params.Epics_start - 
				     ((epics_params.Epics_ninc+1)*epics_params.Epics_inc));
#endif
	}
#ifdef CAMP_ACCESS
      else if (camp_flag) 
	{
	  gbl_FREQ_n = 0;
	  gbl_SCAN_N ++;
	  if(camp_inc > 0)
	    set_camp_val= camp_start - camp_inc;
	  else
	    set_camp_val= (camp_start - ((camp_ninc+1)*camp_inc));
	}
#endif
      else if (!mode10_flag) 
	{ /* DA */
	  gbl_FREQ_n = 0;
	  gbl_SCAN_N ++;
	  if(freq_inc > 0)
	    freq_val= freq_start - freq_inc;
	  else
	    freq_val= (freq_start - ((freq_ninc+1)*freq_inc));
	} /* DA added these braces, right??? Yes!!  */
    }

  return 0; /* cycle is in tolerance */
}

  
void reset(void)
{
  INT status;
  status = al_reset_alarm("thresholds");
  
  if(status == AL_RESET)
    printf("reset: threshold alarm is now reset\n");
  else if (status == AL_INVALID_NAME)
    printf("reset: al_reset_alarm -> Error threshold alarm invalid name\n");
  else if (status == AL_SUCCESS)
    printf("reset: al_reset_alarm returns Success\n");
  else
    printf("reset: al_reset_alarm returns status=%d\n",status);

  fflush(stdout);
  return;
}

#ifdef HAVE_PPG
INT ppg_load(char *ppgfile) 
{ 
  /* download ppg file
   */ 
 

  /* Stop the PPG sequencer  */ 
#ifdef HAVE_NEWPPG
  //TPPGStopSequencer(gVme, PPG_BASE);
  if(TPPGLoad(gVme, PPG_BASE, 0, ppgfile) != SUCCESS) // TPPGLoad stops sequencer
#else  
  VPPGStopSequencer(gVme,   PPG_BASE); 
  /* tr_precheck checked that tri_config has run recently */ 
  if(VPPGLoad(gVme, PPG_BASE, ppgfile) != SUCCESS)
#endif 
    { 
      cm_msg(MERROR,"ppg_load","failure loading ppg with file %s",ppgfile); 
      return FE_ERR_HW; 
    } 
  printf("\nppg_load: PPG file %s successfully loaded",ppgfile); 
  //  cm_msg(MINFO,"ppg_load","PPG file %s successfully loaded",ppgfile); 
  return SUCCESS; 
} 
INT check_ppg_stopped(void)
{
  // check that ppg is stopped before restarting
  // the VPPG ignores multiple starts when running.

	    
  if (wait_ppg_stop())  // false if PPG is stopped
    {
      /* 1. Checks if PPG is running. 
	 2. If running, waits max 5 loops of 100ms for PPG to stop. 
	 3. If still not stopped
	 stops PPG.
	 waits 250ms
	 checks if PPG is running
	 6. Returns ppg_running
	 
	 PPG should be set with DAQ service time = 0 except const1f mode. 
      */
      iwait(100,"check_ppg_stopped"); // wait 100ms longer for PPG to stop
      check_ppg_running(); // recheck
      if(ppg_running)
	{
	  printf("check_ppg_stopped: error - cannot stop PPG\n");
	  cm_msg(MERROR,"check_ppg_stopped","PPG should be stopped. Cannot stop it");
	  return DB_INVALID_PARAM;
	}
    }
  if(dsis)
    {
      if(!ppg_running) 
	printf("PPG is stopped\n");
    }
  return SUCCESS;
}



INT wait_ppg_stop(void)
{   // Full debug version

/* 1. Checks if PPG is running. 
   2. If running, waits max 5 loops of 100ms for PPG to stop. 
   3. If still not stopped
         stops PPG.
         waits 250ms
         checks if PPG is running
   6. Returns ppg_running
   
   PPG should be set with DAQ service time = 0 except const1f mode. 
*/
#ifdef HAVE_PPG
  static struct timeval tt1, tt2;
  double elapsed_time;
  INT max_loop=5;
  INT loop=0;
  DWORD bin_count;

#ifdef HAVE_SIS3820
  if(fs.hardware.sis3820.sis_mode == 0)  // SIS test mode; no ppg 
    {
      printf("wait_ppg_stop: sis test mode without PPG\n");
      ppg_running=FALSE; // not running
      return ppg_running;
    }
#endif

  check_ppg_running();

  /* Expect PPG to be stopped with bin_count = gbl_bin_count  
     Read bin_count to check
  */
#ifdef HAVE_SIS3820
  bin_count = sis3820_RegisterRead(gVme,SIS3820_BASE_B, SIS3820_ACQUISITION_COUNT); // global
#endif //  HAVE_SIS3820
#ifdef HAVE_SIS3801E
  bin_count =  sis3801_read_acquisition_count(gVme, SIS3801_BASE_B); // global
#endif //  HAVE_SIS3801E
  if(bin_count != gbl_bin_count)
    {
      printf("\nwait_ppg_stop: Unexpected bin_count. Expect  PPG to be stopped with bin_count = gbl_bin_count\n");
      printf("wait_ppg_stop: bin_count is %d while gbl_bin_count is %d  ppg_running=%d\n",bin_count,gbl_bin_count,ppg_running);
    }

  if(!ppg_running)
    return ppg_running;

  loop=0;
  gettimeofday(&tt1, NULL); // start timer


  while(loop <= max_loop)
    {
      if (loop > 0)
	iwait(100,"wait_ppg_stop"); // wait 100ms
      check_ppg_running();
      if(!ppg_running)
	break;
      cm_yield(50);
      loop++;
    }
  gettimeofday(&tt2, NULL);  // stop timer
  elapsed_time = (tt2.tv_sec - tt1.tv_sec) * 1000.0;      // sec to ms
  elapsed_time += (tt2.tv_usec - tt1.tv_usec) / 1000.0;   // us to ms
  printf("\nwait_ppg_stop: elapsed time = %f ms; loop=%d\n",elapsed_time, loop);

#ifdef HAVE_SIS3820
  bin_count = sis3820_RegisterRead(gVme,SIS3820_BASE_B, SIS3820_ACQUISITION_COUNT); // global
#endif //  HAVE_SIS3820
#ifdef HAVE_SIS3801E
  bin_count =  sis3801_read_acquisition_count(gVme, SIS3801_BASE_B); // global
#endif //  HAVE_SIS3801E
  if(bin_count != gbl_bin_count)
    printf("wait_ppg_stop: after wait loop, bin_count is %d while gbl_bin_count is %d  ppg_running=%d\n",bin_count,gbl_bin_count,ppg_running);



  if(ppg_running)
    {
      printf("wait_ppg_stop: PPG is still running, so sending stop command\n");
      // Stop the PPG
#ifdef HAVE_NEWPPG
      TPPGReset(gVme,PPG_BASE); // reset stops PPG even if in long delay
#else
      VPPGStopSequencer(gVme, PPG_BASE); // does a reset
      VPPGDisableExtTrig ( gVme, PPG_BASE );
#endif //  HAVE_NEWPPG 
      iwait(250,"wait_ppg_stop" ); // wait 250ms
     check_ppg_running();
    }
#else
  printf("wait_ppg_stop: no PPG present\n");
  ppg_running=FALSE;
#endif
  return ppg_running;
}

void check_ppg_running(void)
{
  // TEMP
  BYTE value;
#ifdef HAVE_SIS3820
  if(fs.hardware.sis3820.sis_mode == 0)  // SIS test mode; no ppg 
    return;
#endif

#ifdef HAVE_NEWPPG
  value =  TPPGRegRead(  gVme, PPG_BASE, TPPG_CSR_REG );
  if (value & 1)
#else
  value = VPPGRegRead( gVme, PPG_BASE, VPPG_VME_READ_STAT_REG ); 
  if  (value & 2)
#endif 
    {
      if(dd[1])
         printf("check_ppg_running: Pulse blaster IS running   value=0x%x\n",value); 
      ppg_running = TRUE;
    }
  else 
    { 
      if(dd[1])
         printf("check_ppg_running: Pulse blaster NOT running value=0x%x\n",value); 
      ppg_running = FALSE;
    }
  return;
}


#endif // HAVE_PPG
 

#ifdef GONE
// This was used to send helicity to history plot for debugging
/*-- Event readout -------------------------------------------------*/
INT helicity_history(char *pevent, INT off)
{
  float *pfloat;
  const float max = 4.5;
  const float min = -0.2;
  
/* - periodic equipment recording helicity is called */
#ifdef HAVE_NIMIO32
  gbl_hel_readback =  hel_read_ioreg(FALSE); // read the helicity (dual channel mode returns latched values);
#endif
 
  /* init bank structure */
      bk_init32(pevent);
      bk_create(pevent, "HELH", TID_FLOAT, (void**) &pfloat);
      // use some offsets for scaling
      *pfloat++ = (float)gbl_ppg_hel + 1.1;  // set value
      *pfloat++ = (float)gbl_hel_readback + 2.2 ; // current helicity readback (latched in dual_channel_mode)
      *pfloat++ = (float)fs.hardware.enable_dual_channel_mode;  // 0 or 1
      *pfloat++ = (float)gbl_dcm_hcur + 3.3; // current helicity state in dual_channel_mode
      *pfloat++ = max;
      *pfloat++ = min;
      bk_close(pevent, pfloat);
      return bk_size(pevent);
}
#endif // GONE

#ifdef EPICS_ACCESS
/*-- Event readout -------------------------------------------------*/
INT epics_alive(char *pevent, INT off)
{
/* - periodic equipment keeping EPICS alive
*/

  INT status;

  if (gbl_epics_live || gbl_hel_live || gbl_laser_live)
    printf("epics_alive: starting with e,h,l=%d %d %d",
	   gbl_epics_live,gbl_hel_live,gbl_laser_live);
  else
    return 0; /* nothing to do */

  if(gbl_epics_live)
    {  /* NaCell */
      /* Maintain EPICS scan channels live only when running epics scan */
      /* check that channel(s) have not disconnected */
      if(dd[7])printf("epics_alive: calling epics_watchdog\n");
      status = epics_watchdog( &epics_params, FALSE);
      
      if (status != SUCCESS)
	printf("epics_alive: error from epics_watchdog\n");
    }
 
  if(gbl_hel_live)
    {
      /* Maintain EPICS Helicity channel live (or Bias for POL)  */
      /* check that channel has not disconnected */
      if(dd[7])
	printf("epics_alive: calling epics_watchdog for hel\n");
      status = epics_watchdog( &epics_params, TRUE);
	  
      if (status != SUCCESS)
	printf("epics_alive: error from epics_watchdog for helicity\n");
      Hel_last_time = ss_time(); 
    }

#ifdef LASER
  if(gbl_laser_live)
    {	  	  
      if(dd[7])
	printf("epics_alive: calling ChannelWatchdog keep epics Laser live\n");
      set_long_watchdog(gbl_long_watchdog_time); /* in ms */
      status = ChannelWatchdog(Rlaser, &Rchid_lp);
      restore_watchdog();
      Laser_last_time=ss_time();
      if(status != SUCCESS)
	cm_msg(MERROR,"epics_alive","failure reading EPICS LaserPower(%d)",status);      
    }
#endif

  return 0; /* event length is 0 */
}
#endif /* EPICS */


#ifdef HAVE_SIS3820

INT FIFO_acq(char *pevent, INT off)
{
  /*    formerly read_vme_event
	
	- Check if still in cycle
	- acquire the FIFO data
	- increment the histo
	- update the /variable scalers if it's time
	- return the in cycle status
	
	- runs every 50ms & tests if data is ready to read  */
  //  static int gCountEmpty = 0;
  int size = 0;
  //int nw;
  int countA,countB,counts;
  int status;
  static struct timeval tt1, tt2;
  double elapsed_time;
  // SIS3820
  // dd[1]=1; //TEMP
  // if(dd[1])
  // printf("FIFO_acq (SIS3820) starting...\n");
  if (gbl_IN_CYCLE)
    {          /* cycle started */
      
      if (dd[0])printf("IN CYCLE ,gbl_bin_count(%d), gbl_BIN_A(%d), gbl_BIN_B(%d)\n",
		    gbl_bin_count,gbl_BIN_A,gbl_BIN_B);
      
      /*
	
	Test MODULE A   SIS has how much data?
      */
      
#ifdef TWO_SCALERS
      countA = sis3820_RegisterRead(gVme,SIS3820_BASE_A , SIS3820_FIFO_WORDCOUNTER);
      if(dsis)printf("FIFO_acq: Module A has %d words to read \n",countA);

      if (countA <= 0)
	printf("FIFO_acq: Module A is empty\n");
      else
	{
	  counts = have_SIS_data(SIS3820_BASE_A,0);  // returns countA > 0 if there is a reasonable amount of data to read
	  //if(dsis)printf("FIFO_acq: Module A  counts to readout=%d count_A=%d fifo wordcounter %d  SIS3820_FIFO_SIZE_CHECK=%d\n", counts, countA, sis3820_DataReady(gVme,SIS3820_BASE_A),SIS3820_FIFO_SIZE_CHECK );
	
	  if (counts > 0)
	    {
	      size = read_SIS(pfifo_A, SIS3820_BASE_A , countA, 1);  // calls scaler_increment	  
	      if(dsis ||dd[0] )printf("FIFO_acq: Module A read out %d words \n", size);
	    }
	  else
	     if(dsis || dd[0]) printf("FIFO_acq: Module A has %d words;  too few for readout (min is %d)\n",countA,SIS3820_FIFO_SIZE_CHECK);
	}
#endif

      countB = sis3820_RegisterRead(gVme,SIS3820_BASE_B , SIS3820_FIFO_WORDCOUNTER);
      if(dsis)printf("FIFO_acq: Module B now has %d words to read \n",countB);

      if (countB <= 0)
	{
	if(dsis)printf("FIFO_acq: (SIS3820) Module B is empty\n");
        }
      else
	{
	  counts = have_SIS_data(SIS3820_BASE_B,1);  // returns counts > 0 if there is a reasonable amount of data to read
	  if(dsis)printf("FIFO_acq: Module B counts to readout=%d  count_B=%d fifo wordcounter %d  SIS3820_FIFO_SIZE_CHECK=%d\n", counts, countB, sis3820_DataReady(gVme,SIS3820_BASE_B),SIS3820_FIFO_SIZE_CHECK );
	  if (counts > 0)
	    {
	      size = read_SIS(pfifo_B, SIS3820_BASE_B , counts, 1);  // calls scaler_increment	  
	      if(dsis ||dd[0] )printf("FIFO_acq: Module B read out %d words\n", size);
	    }
	  else 
	    if(dsis || dd[0])printf("FIFO_acq: Module B has %d words; too few for readout (min is %d)\n",countB,SIS3820_FIFO_SIZE_CHECK);
	}
    } // end of GBL_IN_CYCLE

  else
    { /* Cycle is OFF expected to be in end of cycle state */
      
      if (dd[0]) printf ("OFF cycle: gbl_bin_count=%d waiteqpF:%d H:%d C:%d D:%d\n"
		      ,gbl_bin_count,waiteqp[FIFO],waiteqp[HISTO],waiteqp[CYCLE],waiteqp[DIAG]);
      
      if (!waiteqp[FIFO]) return 0; /* not in end-of-cycle state */
      
      countA = countB = 0; 
      //printf("FIFO_acq: setting FLUSH true\n");
      flush = TRUE;
      //dsis=1; // temp

#ifdef TWO_SCALERS
      /* Flush module (read the last data) */
     
      countA = have_SIS_data(SIS3820_BASE_A, 0);
      printf("Module A SIS countA : %d fifo %d\n", countA, sis3820_DataReady(gVme,SIS3820_BASE_A) );
      
      if (countA > 0)
	{
	  //bk_init32(pevent);
	  //size |= read_SIS(pevent, SIS3820_BASE_A , countA, 0);// calls scaler_increment
	  size |= read_SIS(pfifo_A, SIS3820_BASE_A , countA, 0);// calls scaler_increment
	}
#endif
      countB = have_SIS_data(SIS3820_BASE_B, 1);
      if(dsis)printf("Module B SIS countB : %d fifo %d\n", countB, sis3820_DataReady(gVme,SIS3820_BASE_B) );
      
      if (countB > 0)
	  size |= read_SIS(pfifo_B, SIS3820_BASE_B , countB, 1);// calls scaler_increment

      
      //dsis=0; // TEMP
#ifdef TWO_SCALERS
      countA = sis3820_RegisterRead(gVme,SIS3820_BASE_A , SIS3820_FIFO_WORDCOUNTER);
      if (countA != 0) 
	{
	  printf ("FIFO_acq-Flush: Module A  is not EMPTY  after FLUSH; countA=%d\n",countA);
	  die();
	  return 0;
	}
#endif
    
      countB = sis3820_RegisterRead(gVme,SIS3820_BASE_B , SIS3820_FIFO_WORDCOUNTER);
      if (countB != 0) 
	{
	  printf ("FIFO_acq-Flush: Module B  is not EMPTY  after FLUSH; countB=%d\n",countB);
	  die();
	  return 0;
	}
      
#ifdef TWO_SCALERS    
      if ( gbl_BIN_A != gbl_BIN_B )
	{      /* Error if both modules do not have the same bin count */
	  printf("\nFIFO_acq: Mismatch-last bin A(%d),B(%d); nwords A(%d),B(%d)\n",
		 gbl_BIN_A,gbl_BIN_B,countA,countB,0,0);
	  die();
	  return 0;
	}
      /* Check Userbits in type 2 mode */
      if(exp_mode == 2)
	{ 
	  if(userbit_A != userbit_B)
	    printf("\nFIFO_acq: Mismatch- userbits A(%d),B(%d)\n",
		   userbit_A,userbit_B,0,0,0,0);
	}
#endif
          waiteqp[FIFO] = FALSE;  
  



#ifdef SPEEDUP
      if(const1f_flag) // SPEEDUP only in const1f mode
	{ 
	  if(  gbl_CYCLE_H != 0)  // cleared by histo_read 
	    {
	      if(!hold_flag)
		{
		  printf("\n ** fifo_acq: - warning previous cycle %d not yet histogrammed!! current cycle=%d ** \n",
			 gbl_CYCLE_H, gbl_CYCLE_N);
		  cm_msg(MINFO,"fifo_acq","warning previous cycle %d not yet histogrammed!! current cycle=%d ",
			 gbl_CYCLE_H, gbl_CYCLE_N); // what do we do?
		}
	    }
	  
	  // remember this information for this data
	  gbl_CYCLE_H=gbl_CYCLE_N; // cycle to be histogrammed: gbl_CYCLE_H
	  gbl_freq_H=freq_val;   // remember the values to be put in CYCL bank
	  gbl_SCYCLE_H=gbl_SCYCLE_N;
	  gbl_SCAN_H=gbl_SCAN_N;
	  gbl_helset_H=(DWORD)gbl_ppg_hel;
	  gbl_helread_H=(DWORD)gbl_hel_readback;
	}
#endif
      /* As the cycle is completed compact histo S0 to 15 H0..1, S16..S31 to H2..3 */
      //gettimeofday(&tt1, NULL); // start timer
      if (gbl_alpha && alpha_chunks)
	{
	  if(last_piece_sent)
	    last_piece_sent=0;
	  else
	    printf("\n *** last histo piece has not been sent. Need to make cycle longer in time *****\n");
	}
    histo_process_flag=1;
      status = histo_process();
       histo_process_flag=0;
       if(0)
	 {
	   gettimeofday(&tt2, NULL);  // stop timer
	   elapsed_time = (tt2.tv_sec - tt1.tv_sec) * 1000.0;      // sec to ms
	   elapsed_time += (tt2.tv_usec - tt1.tv_usec) / 1000.0;   // us to ms
	   // printf("fifo_acq: time for histo_process was  %f ms\n",elapsed_time);
	   if(elapsed_time > gbl_pmax)
	     gbl_pmax=elapsed_time;
	   if(elapsed_time < gbl_pmin)
	     gbl_pmin=elapsed_time;
	 }

      if(skip_cycle)
	{
	  //printf("fifo_acq: skip_cycle is true, returning\n");
	  waiteqp[HISTO]=FALSE;
	  return 0;
	}
	
    	if(dd[18])
	  printf("fifo_acq: cycle %d is complete, histo_process has finished\n",gbl_CYCLE_N);


      // Moved out of histo_read for split banks; may be reading out after hel is flipped
      /* For TDtype modes */
      if(exp_mode == 2) 
	{
	  if(flip && (gbl_ppg_hel == HEL_DOWN))/*  use ppg set value */
	    {
	      // don't send histos
	      printf("fifo_acq: type 2 flip is true,  %d (HEL_DOWN), not sending histo data\n",
		     gbl_ppg_hel);
	      waiteqp[HISTO]=FALSE;
	      return 0;
	    }
	}
      waiteqp[HISTO] = TRUE; //(should be true anyway) waiting for HISTOS; will be cleared by histo_read()

      gettimeofday(&tt1, NULL); // start timer
      if(gbl_alpha && alpha_chunks) // Mode 2h sends histos in pieces
	{
	  histo_trigger_mask = 1<<1 ;
	  printf("fifo_acq: histo_trigger_mask is shifted for CHUNKS ; it is now %d\n",histo_trigger_mask);
	}
      else
	{
	  histo_trigger_mask = 1;
	  //printf("histo_trigger_mask is set to %d\n",histo_trigger_mask);	      
	}
      //if(dd[18])
	  if(gbl_alpha && alpha_chunks) 
	    printf("fifo_acq: setting send_histo_event true\n");

      send_histo_event=1;  // enable histo_read to send out histograms
      /*
      if(0)
	{
      if(gbl_alpha)
	{
	  for(i=1; i<5; i++)
	    {
	      histo_trigger_mask = 1<<i;
	      size = histo_read(pevent, off);
	      pevent += size;
	      printf("histo_read: gbl_alpha=%d i=%d called histo_read with trigger_mask=%d returned size=%d\n",
		     gbl_alpha, i,histo_trigger_mask, size);
	     
	    }
	}
      else
	size = histo_read(pevent, off);  //clears waiteqp[HISTO]

      gettimeofday(&tt2, NULL);  // stop timer
      elapsed_time = (tt2.tv_sec - tt1.tv_sec) * 1000.0;      // sec to ms
      elapsed_time += (tt2.tv_usec - tt1.tv_usec) / 1000.0;   // us to ms
      // printf("fifo_acq: time for histo_read was  %f ms\n",elapsed_time);
      if(elapsed_time > gbl_rmax)
	gbl_rmax=elapsed_time;
      if(elapsed_time < gbl_rmin)
	gbl_rmin=elapsed_time;

      printf("fifo_acq: size returned from histo_read is %d; waiteqp[HISTO]=%d; gbl_CYCLE_H=%d time for histo_read: %f ms\n",
	     size,waiteqp[HISTO],gbl_CYCLE_H, elapsed_time);
	 return size;
	} // if 0
      */

    }// cycle is OFF
      return 0;
}

int have_SIS_data(int SIS3820_base, int isis)
{
  struct timeval now;
  
  
  int haveData = sis3820_DataReady(gVme,SIS3820_base);
  if (haveData == (-1))
    haveData = 0;
  
  // int minread  = 1000*32; 
  int minread  =  SIS3820_FIFO_SIZE_CHECK ;
  
  gettimeofday(&now,NULL);
  
  double td = timeDiff(now, gSisLastRead[isis]);
  
  if(dsis)
    printf("have_sis_data: td: %f, haveData: %d  minread=%d flush=%d \n",td,haveData, minread, flush );
  
  if(!flush)
    {
      if (td < 1.0 && haveData < minread)  
	return 0;
      else if(dsis)
	    printf("have_sis_data: time to readout data...\n");
    }

  int toRead = haveData;

  // if (flush) // flush last data words
     //  toRead = toRead & 0xFFFFFFE0;
  

   if(dsis)printf("have_SIS_data:  haveData=%d flush=%d toRead=%d\n",haveData,flush,toRead) ;
    
  if (toRead == 0)
    return 0;
  
  return toRead;
}



int read_SIS(DWORD *pfifo , DWORD base , int toRead32, int isis)
{
  
  // called from read_VME_event / FIFO_acq
  int status;
  int max=100; // dump 10 words of raw data
  int tmp;
  int my_count;
  int maxDma = 64*1024; 

  // isis = module number 0=A 1=B
  //dsis=1;
  if(dsis)
    printf("read_SIS: starting with toRead32=%d\n",toRead32);

  int used = 0; // FIFO array is empty
  int free = SIS_FIFO_SIZE * 4;  // bytes (actually the fifo array size)
  int i,j,k;
  int wc=0;

 start:

  if(toRead32 <= 0 ) return 0; // nothing to do

 
  if (toRead32*4 > free)  // all in bytes
    {
      if(dsis)printf("read_SIS: buffer used %d, free %d, toread %d  (in bytes %d)\n", used, free, toRead32, toRead32*4);
      toRead32 = free;
    }
	

  if (toRead32*4 > maxDma)
    toRead32 = maxDma/4;

  gettimeofday(&gSisLastRead[isis], NULL);

  int rd = sis3820_FifoRead(gVme,base,(char *)pfifo,toRead32);
  // rd and toRead32 should be the same
  if (rd != toRead32)
    printf("read_SIS: requested toRead32= %d words; returned rd=%d\n",toRead32,rd);
  wc+=rd;


  my_count = sis3820_RegisterRead(gVme,SIS3820_BASE_B , SIS3820_FIFO_WORDCOUNTER);
  if(dsis)
    printf("read_SIS: Now there are %d words left to read\n",my_count);


  if (dsis) // dump the data (if not too large!)
    {
      //  printf("sis3820 32-bit data (first 4 data words): 0x%x 0x%x 0x%x 0x%x\n",pdata32[0],pdata32[1],pdata32[2],pdata32[3]);
      i=0;
      tmp=rd;
      if (tmp > max)
	{
	  tmp = max;
	  printf("Dumping first  %d words of data\n",tmp);
          printf("Counter              Data");
	}
      j=10; // print sets of 10 across page
      if(tmp < 10)
	j=tmp; // less than 10 data words
      while (i<tmp)
	{
	  if (i % 10 ==0 )
	    printf("\n%6.6d  ", i);
	  for (k=0; k < j; k++) 
	    {
	      printf("0x%8.8x ", pfifo[i]);
	      i++;
	    }
	}
      printf("\n");
    }
  
  /* Move data & increment corresponding scaler histogram  */
#ifdef TWO_SCALERS
  if ( isis == 0 )
    {  // Scaler A
      if(pulsepair_flag)  // Mode 2e...
	status = scaler_increment_pulsepair(toRead32, (DWORD *)pfifo, 0, MAX_CHAN_SIS38xxA, &gbl_BIN_A, &userbit_A);  
      else
	status = scaler_increment(toRead32, (DWORD *)pfifo, 0, MAX_CHAN_SIS38xxA, &gbl_BIN_A, &userbit_A);   
      if(status !=0)
	printf("read_SIS: error return from scaler_increment for Scaler A\n");

      if (dd[0]) printf ("IN CYCLE eqp[FIFO]:%d  nW:%d gbl_BIN_A :%d\n",waiteqp[FIFO],toRead32,gbl_BIN_A);
    }
#endif
  if (isis == 1)
    {    // Scaler B    
      if(pulsepair_flag)  // Mode 2e...
	status = scaler_increment_pulsepair(toRead32,  (DWORD *)pfifo, 0, MAX_CHAN_SIS38xxB, &gbl_BIN_B, &userbit_B);   
      else
	status = scaler_increment(toRead32,  (DWORD *)pfifo, 0, MAX_CHAN_SIS38xxB, &gbl_BIN_B, &userbit_B);    
      if(status !=0)	
	printf("read_SIS: error return from scaler_increment for Scaler B\n");   
      if (dd[0]) printf ("IN CYCLE eqp[FIFO]:%d  nW:%d gbl_BIN_B :%d\n",waiteqp[FIFO],toRead32,gbl_BIN_B);
    }
  
    if (flush)
      {
	if(my_count > 0)
	  {
	    toRead32=my_count;
	    if(dsis)
	      printf("Flushing out the last data for this cycle\n");
	    goto start;
	  }
      }
  
    if(dsis)
      {
	if(flush && my_count==0)
	  printf("read_SIS: all data has been read from SIS3820 module %s \n",sis_module[isis]);
      }
    return wc;  // return the number of words read
}
double timeDiff(const struct timeval t1, const struct timeval t2)
{
  return (t1.tv_sec - t2.tv_sec) + 0.000001*(t1.tv_usec - t2.tv_usec);
}

#endif // HAVE_SIS3820




#ifdef HAVE_SIS3820_OR_SIS3801E
// check_lne is now called directly from frontend_loop.
// old code (equipment lne)  is stored in old_lne_equip.c

INT check_lne()
{ // Called if gbl_IN_CYCLE is true to check whether PPG has sent all the LNE
  DWORD ppg_daq_busy=0;
  gbl_lne++;
  
  if( dd[1] )
    printf("check_lne: starting with gbl_IN_CYCLE = %d and  gbl_bin_count=%d\n",  gbl_IN_CYCLE,gbl_bin_count);
  
  if (!gbl_IN_CYCLE)
    return SUCCESS;

#ifdef SPEEDUP
  if(const1f_flag)
    {
#ifdef HAVE_NIMIO32
      ppg_daq_busy = nimio_ReadOneInput(gVme,  NIMIO32_BASE, INPUT5);  // Computer busy should not be true during cycle
      nimio_SetOneOutput(gVme,  NIMIO32_BASE, OUTPUT4 ); // Set OUTPUT4
#endif
    }
#endif // SPEEDUP
  
  // Check whether we have reached n_bins ( n_bins = LNE_preset)
  
#ifdef HAVE_SIS3820
  gbl_bin_count = sis3820_RegisterRead(gVme,SIS3820_BASE_B, SIS3820_ACQUISITION_COUNT); // global
#endif //  HAVE_SIS3820
  
#ifdef HAVE_SIS3801E
  gbl_bin_count =  sis3801_read_acquisition_count(gVme, SIS3801_BASE_B); // global
#endif //  HAVE_SIS3801E
  

  if(!dd[11])
    { 
      if( (gbl_lne%40)==0)  // slow down printing
	{
	  if(dd[8])  // do not overwrite
	    printf("\ngbl_bin_count, prevcount, n_bins  = %d, %d,%d  gbl_lne=%d ",
		   gbl_bin_count,racq_count,(int)n_bins,gbl_lne);
	  else
	    //	printf("gbl_bin_count= %d n_bins= %d                       ",gbl_bin_count,(int)n_bins);
	    printf("%d.",gbl_bin_count);
	  
	  gbl_lne=0;
	}
    } // !dd[11]  suppress messages
  
  if(dsis)
    printf("\n");
  
  
  if ( ( gbl_bin_count > 0 ) && ( gbl_bin_count >= (unsigned int)n_bins ))  // preset count has been reached or num_bins done
    { // At end of PPG cycle
      if(!dd[11])
	printf("..%d bins complete; gbl_CYCLE_N=%d ",n_bins,gbl_CYCLE_N);
      stop_scalers();
      // printf("check_lne: stopped the scalers\n");
#ifdef SPEEDUP
      if(const1f_flag)
	{
#ifdef HAVE_NIMIO32
	  if(!ppg_daq_busy)
	    {
	      printf("check_lne: ppg_daq_busy is unexpectedly NOT SET at end_of_cycle; gbl_bin_count=%d\n",gbl_bin_count);
	      gbl_ppg_cb_cntr2 ++;
	      // Do step back procedure or stop run?
	    }
#endif
	}
#endif // SPEEDUP
      if(dsis)
	printf("check_lne: detected end of Cycle (gbl_bin_count=n_bins=%d) \n",gbl_bin_count);
      
      gbl_IN_CYCLE = FALSE; // PPG cycle is done
      //printf("check_lne: gbl_IN_CYCLE set false %d\n",gbl_IN_CYCLE);
#ifdef HAVE_NIMIO32
      nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT6 ); // Clear output 6 (gbl_IN_CYCLE)
#endif //  HAVE_NIMIO32
      
      // timing test for computer busy
      gettimeofday(&t1, NULL);
      
      
#ifdef TWO_SCALERS    // Read and check bin count of second scaler
#ifdef HAVE_SIS3820	 
      my_bin_count =  sis3820_RegisterRead(gVme,SIS3820_BASE_A, SIS3820_ACQUISITION_COUNT); 
#endif //  HAVE_SIS3820
      
#ifdef HAVE_SIS3801E
      if(!OLD_SISA_FLAG)
	my_bin_count =   sis3801_read_acquisition_count(gVme, SIS3801_BASE_A); // global
      else
	my_bin_count =  gbl_bin_count; // cannot check
#endif //  HAVE_SIS3801E
      
      if (my_bin_count !=  gbl_bin_count)
	{
	  printf("check_lne: ERROR gbl_bin_count for both Scaler modules are not the same\n");
	  printf("check_lne:  gbl_bin_count for Module B is %d and Module A is %d\n",(int)gbl_bin_count,(int)my_bin_count);
	  
	  cm_msg(MERROR,"check_lne","ERROR gbl_bin_count for both Scaler modules should be identical (A=%d B=%d). Check hardware",
		 (int)gbl_bin_count,(int)my_bin_count);
	  stop_run();
	  return FE_ERR_HW;
	}
      else 
	{
	  if(dsis)
	    printf("check_lne: Scaler B gbl_bin_count=%d Scaler A my_bin_count=%d . Bin counts agree\n",gbl_bin_count,my_bin_count); 
	}
#endif  // TWO SCALERS	  
      
      
#ifdef HAVE_NIMIO32
      if(const1f_flag &&  e1f_started_flag)
	{ // count these, don't take any action at present (shouldn't happen !!)
	  if(!nimio_ReadOneInput(gVme,  NIMIO32_BASE, INPUT5))
	    {
	      gbl_cb_cntr1++;
	      printf("\n\n **  check_lne: end-of-cycle %d, computer busy NOT set as expected (cntr=%d)\n",gbl_CYCLE_N, gbl_cb_cntr1);
	    }		
	}
      nimio_SetOneOutput(gVme,  NIMIO32_BASE, OUTPUT7 ); // Set OUTPUT7  computer is busy
#endif //  HAVE_NIMIO32
    } // at end of PPG Cycle
  else
    {
      racq_count =  gbl_bin_count; // remember the present count
      if(const1f_flag)
	{
#ifdef HAVE_NIMIO32
	  if( (gbl_bin_count > 0) && ppg_daq_busy) // if gbl_bin_count==0, waiting for next PPG cycle to start
	    {
	      printf("check_lne: ppg_daq_busy is unexpectedly SET during PPG cycle; gbl_bin_count=%d n_bins=%d\n",
		     gbl_bin_count,n_bins);
	      gbl_ppg_cb_cntr2++;
	      // Step back or restart run?
	    }
	  nimio_ClrOneOutput(gVme,  NIMIO32_BASE, OUTPUT4 ); // Clear OUTPUT4
#endif
	}
    }
  return SUCCESS;
}
#endif  //  HAVE_SIS3820_OR_SIS3801E


#ifndef HAVE_SIS3801
#ifndef HAVE_SIS3820
INT FIFO_acq(char *pevent, INT off)
{
  return 0;
}
#endif
#endif

#ifdef HAVE_PSM
void debug_psm(void)
{
  INT status;
  // Debugging PSM only
  if (dpsm)  // multi profiles
    { 
      char my_profile[5];
  
      /* only two profiles are supported */ 
      
      if(fs.hardware.psm.one_f.profile_enabled)
	{
	  sprintf(my_profile, get_profile(0));
	  if(fs.hardware.psm.one_f.quadrature_modulation_mode)
	    {
	      status = psmReadIQlen (gVme, PSM_BASE, my_profile); 
	      printf("cycle_start: profile \"%s\" IQlen=%d ",my_profile,status);
	      status = psmReadIQptr (gVme, PSM_BASE, my_profile);
	      printf("IQpointer=%d",status);
	      printf("\n");
	    }
	}
      
      if(fs.hardware.psm.fref.profile_enabled)
	{
	  sprintf(my_profile, get_profile(3));
	  if(fs.hardware.psm.fref.quadrature_modulation_mode)
	    {
	      status = psmReadIQlen (gVme, PSM_BASE, my_profile); 
	      printf("cycle_start: profile \"%s\" IQlen=%d ",my_profile,status);
	      status = psmReadIQptr (gVme, PSM_BASE, my_profile);
	      printf("IQpointer=%d",status);
	      printf("\n");
	    }
	}
    } // dpsm
  return;
}
#endif
 
#ifdef HAVE_NIMIO32
INT hel_read_ioreg(BOOL write)
{
  /* returns  helicity state (latched if dual channel mode is true)
     0 = HEL DOWN
     1 = HEL UP
     2 = Waveplate split in half (in both states) or no io module in crate
     3 = Waveplate in transit (in neither state)
     4 = unexpected error

     Parameters
     Input   write  TRUE  calls read_bnmrinputs for debugging
       For debugging,
             gbl_dcm_hcur In dual channel mode, returns current hel state in this global            
  */

  INT state, held, helu;

  if(write || dd[12]) 
    read_bnmrinputs(gVme,  NIMIO32_BASE); // Write out states for debug


  held=helu=0; // clear
 

 // read unlatched values 
  if( nimio_ReadOneInput(gVme,  NIMIO32_BASE, INPUT1)) // hel down
    held=1;
  if(nimio_ReadOneInput(gVme,  NIMIO32_BASE, INPUT2)) // hel up
    helu=1;

  state = decode_hel_state(held,helu);

  if(!fs.hardware.enable_dual_channel_mode)
    {
      if(gbl_heltest_timer)
	print_hel_time(state);
      return state;  // Single channel mode
    }

  // Dual channel mode
  gbl_dcm_hcur  = state; // return current state dual channel mode only

  held=helu=0; // clear
  // read latched values 
  if(nimio_ReadOneInput(gVme,  NIMIO32_BASE, INPUT3)) // latched hel down
    held=1;
  if(nimio_ReadOneInput(gVme,  NIMIO32_BASE, INPUT4))  // latched hel up
    helu=1;
 
  state = decode_hel_state(held,helu);
  if(gbl_heltest_timer)
     print_hel_time(state);
  return state;
}

INT decode_hel_state(INT held, INT helu)
{
  INT state;
  
  if(held == helu) // error; both are the same 
    {
      if(held ==1)  // both on (shouldn't get this)
	state = 3; // error...  both states are true
      else
	state = 2; // both states are false (waveplate in transit)
    }
  
  else if (held == 1 )
    state =  HEL_DOWN; 
  else if (helu == 1 )
    state =  HEL_UP; 
  else
    state = 4; // peculiar error
  
  if(dd[12])printf("decode_hel_state: hel down=%d hel up=%d state=%s\n",
	 held,helu, helicity_state(state));
  
  return state;
}
#endif // HAVE_NIMIO32


char *helicity_state(INT n)
{
  static char *name[]= {
    "HEL DOWN","HEL UP","IN TRANSIT" ,"PLATE SPLIT","ERROR"
  };
  return ( n < 0 || n > 4) ? name[4] : name[n] ;
}

#ifdef BINCHECK
INT bincheck(INT h)
{

  // check on empty bin HM00 1f const. time mode
  INT i,j,k,min,max;
  INT s=0;
  DWORD *ph0;
  INT idiff,istart;
 
  if(!const1f_flag)
    return SUCCESS; // this check for const1f mode only
  if(h!=0) 
    return SUCCESS; // check h=0 only
  
  // For SIS3801 :
   // Checking histo HM00 which must have Ref 1 25MHz enabled for this test to work

  // Look for a bin count of zero (counter has been cleared by counter disable external input, i.e. MSC Gate
  //  or a bin count of a number > 0.001% of typical value ( counter disable is disconnected for testing)
  // both of which mean computer busy is too short
  
  // Added another check in check_lne() to check computer busy while checking bin count

  if(dsis)
    {
      printf("bincheck: calling display_his_data for input channel h=%d\n",h);
      display_his_data(h);
    }
  i=j=k=max=0;
  min = bin1_XXMHz_count*2; // set to large number

  istart=0;

#ifdef HAVE_SIS3820
  if(!discard_first_bin)
    istart=1;
#endif

 ph0= histo[n_histo_a+h].ph;
 s=*ph0; // first value

 //printf("bincheck: starting check at bin %d; bin 0 value is %d\n",istart,*ph0);

  for (i=istart; i< histo[n_histo_a+h].nbins; i++)
    {
      if( *ph0 > max)
	{
	  max= *ph0 ;
	  j=i;
	}
      else if ( *ph0 < min)
	{
	  min =  *ph0;
	  k=i;
	} 
      ph0++; 
    }
  idiff = abs(max - min);
    if(dd[18])
    printf("\n bincheck: Cycle %d HM00 max=%d at bin %d, min=%d at bin %d  calculated count=%d first count=%d  abs(max-min) =%d \n",
	   gbl_CYCLE_N,max,j,min,k,bin1_XXMHz_count,s,idiff);
  if( idiff > 3)
    {
      // display_his_data(h);
      bad_cntr++;
      if(!dd[18])
	printf("\nbincheck: Cycle %d HM00 max=%d at bin %d, min=%d at bin %d  calculated count=%d first count=%d  abs(max-min) =%d \n",
	       gbl_CYCLE_N,max,j,min,k,bin1_XXMHz_count,s,idiff);
      printf("bincheck: cycle %d  HM00 difference between max and min > 3; Bincheck has failed. Bad count=%d Good count=%d. Repeating cycle. \n\n",
	     gbl_CYCLE_N, bad_cntr,good_cntr);
      return  FE_ERR_HW;
    }
  else
    good_cntr++;
  return SUCCESS;
} 

void display_his_data(INT h)
{
  // display all bins of the histogram data for channel h
  // called from bincheck()
  DWORD *ph0;
  INT num_bins,i,j;
  ph0= histo[n_histo_a+h].ph;  
  i=j=0;
  num_bins =  histo[n_histo_a+h].nbins;

  printf("\n%4.4d ",i);
  for (i=0; i< num_bins; i++)
    {
      if(j<10)
	{
	  printf("%10.10d ",*ph0);
	  ph0++;
	  j++;
	  if(j==10)
	    {
	      if( num_bins > (i+1))
		{
		  printf("\n%4.4d ",i);
		  j=0;
		}
	    }
	}
    }
  //printf("\n i=%d nbins=%d\n",i, num_bins );
  return;
}
#endif // BINCHECK 


void free_all_scalers(void)
{
  INT i;
 printf("\nfree_all_scalers: freeing all possible scalers, N_SCALER_MAX=%d \n",N_SCALER_MAX);
  for (i=0 ; i < N_SCALER_MAX ; i++)
  {
    if (scaler[i].ps != NULL)
      {
	printf(" %d", i);  
	if(dd[1])printf("\nfreeing scaler[%d], scaler[%d].nbins=%d n_his_bins=%d (%p)\n", 
			i,i, scaler[i].nbins, n_his_bins, scaler[i].ps); 
	fflush(stdout);
	free(scaler[i].ps);
        scaler[i].nbins=0;
	scaler[i].ps = NULL;
      }
    else
      {
	if(dd[1])printf("scaler[%d].ps is already NULL; scaler[%d].nbins=%d;  NOT freeing\n",i,i,scaler[i].nbins);
	if(scaler[i].nbins>0)
	  scaler[i].nbins=0;
      }

 if (scaler[i].qs != NULL)
      {
	printf(" %d", i);  
	if(dd[1])
	  printf("\nfreeing scaler[%d], scaler[%d].nbins=%d (%p) SMALL bins\n", 
		 i,i,scaler[i].nbins, scaler[i].qs); 
	fflush(stdout);
	free(scaler[i].qs);
	scaler[i].nbins=0;
	scaler[i].qs = NULL;
      }
    else
      {
	if(dd[1])printf("scaler[%d].qs is already NULL; scaler[%d].nbins=%d; SMALL bins, NOT freeing\n",i,i,scaler[i].nbins);
	if(scaler[i].nbins>0)
	  scaler[i].nbins=0;
      }
  }
  printf("\n");
}

void free_all_histos(void)
{
  INT i;
  printf("\nfree_all_histos: freeing all possible histos (N_HISTO_MAX=%d)\n",N_HISTO_MAX);
  
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {
      if (histo[i].ph != NULL)
	{
	  if(dd[1])
	    printf("\nfreeing histo[%d] histo[%d].nbins=%d n_his_bins=%d (%p)\n", 
		   i,i,histo[i].nbins, n_his_bins, histo[i].ph);
	  else
	    printf(" %d", i); 
	  fflush(stdout);
	  free(histo[i].ph);
	  histo[i].nbins=0;
	  histo[i].ph = NULL;
	}
      else
	{
	  if(dd[1])printf("histo[%d].ph is already NULL; histo[%d].nbins=%d; NOT freeing\n",i,i,histo[i].nbins);
	  if(histo[i].nbins>0)
	    histo[i].nbins=0;
	}

      if (histo[i].qh != NULL)
	{
	  if(dd[1])
	    printf("\nfreeing histo[%d] histo[%d].nbins=%d n_his_bins=%d SMALL_BINS (%p)\n", 
		   i,i,histo[i].nbins, n_his_bins, histo[i].qh);
	  else
	    printf(" %d", i); 
	  fflush(stdout);
	  free(histo[i].qh);
	  histo[i].nbins=0;
	  histo[i].qh = NULL;
	}
      else
	{
	  if(dd[1])printf("histo[%d].qh is already NULL; histo[%d].nbins=%d; SMALL bins; NOT freeing\n",i,i,histo[i].nbins);
	  if(histo[i].nbins>0)
	    histo[i].nbins=0;
	}
    }
}


INT stop_scalers(void)
{
#ifdef HAVE_SIS3801
#ifdef TWO_SCALERS
  sis3801_next_logic(gVme, SIS3801_BASE_A, SIS3801_DISABLE_NEXT_CLK_WO); /* force acquisition stop */
  sis3801_FIFO_clear(gVme, SIS3801_BASE_A);   /* clear FIFO */
  if(dsis)printf ("Disabled and Cleared FIFO SIS3801 SCALER A \n");
#endif
  sis3801_next_logic(gVme, SIS3801_BASE_B, SIS3801_DISABLE_NEXT_CLK_WO); /* force acquisition stop */
  if(dsis)printf ("Disabled next clock SIS3801 SCALER B \n");
  sis3801_FIFO_clear(gVme, SIS3801_BASE_B);    /* clear FIFO */
  if(dsis)printf ("Disabled and Cleared FIFO SIS3801 SCALER B \n");
#endif //  HAVE_SIS3801


#ifdef HAVE_SIS3820
#ifdef TWO_SCALERS
  sis3820_RegisterWrite(gVme,SIS3820_BASE_A,SIS3820_KEY_OPERATION_DISABLE,0);
  sis3820_RegisterWrite(gVme, SIS3820_BASE_A,SIS3820_KEY_COUNTER_CLEAR,0);
  if(dsis)printf ("Disabled and Cleared SIS3820 SCALER A \n");
#endif  // TWO_SCALERS
  sis3820_RegisterWrite(gVme,SIS3820_BASE_B,SIS3820_KEY_OPERATION_DISABLE,0);
  sis3820_RegisterWrite(gVme, SIS3820_BASE_B,SIS3820_KEY_COUNTER_CLEAR,0);
  if(dsis)printf ("Disabled and Cleared SIS3820 SCALER B \n");

  // DWORD temp = sis3820_RegisterRead(gVme,SIS3820_BASE_B, SIS3820_ACQUISITION_COUNT); // global
  //  printf("\nstop_scalers: now SIS3820_ACQUISITION_COUNT reg reads %d bins) \n",temp);

#endif // HAVE_SIS3820

  return SUCCESS;
}


INT start_scalers(void)
{
  printf("Start scalers : starting\n");
  INT temp;
#ifdef HAVE_SIS3801
  /* Enable Next clock on SIS3801 (but will wait for LNE until PPG is started) */
#ifdef TWO_SCALERS
  sis3801_next_logic(gVme, SIS3801_BASE_A, SIS3801_ENABLE_NEXT_CLK_WO);
#endif //  TWO_SCALERS
  printf("Clearing FIFO of Scaler B\n");
  sis3801_FIFO_clear(gVme, SIS3801_BASE_B );
  printf("Wrote enable next logic to Scaler B at base 0x%x\n", SIS3801_BASE_B);
  sis3801_next_logic(gVme, SIS3801_BASE_B, SIS3801_ENABLE_NEXT_CLK_WO);
  //  printf("Scaler B status:\n");
  //  SIS3801_Status(gVme,SIS3801_BASE_B);
#endif // SIS3801 
  
  /* Arm SIS3820 */
#ifdef HAVE_SIS3820
#ifdef TWO_SCALERS
#ifdef ARM
  // ARM (rather than enable) scalers with real LNE input (or get empty first bin problem)
  sis3820_RegisterWrite(gVme, SIS3820_BASE_A,SIS3820_KEY_OPERATION_ARM,0);
  // if(dsis)
    printf ("Armed SIS3820 module A \n");
#else
  // ENABLE scalers
  sis3820_RegisterWrite(gVme, SIS3820_BASE_A,SIS3820_KEY_OPERATION_ENABLE,0);
  // if(dsis)
    printf ("Enabled SIS3820 module A \n");
#endif // ARM
#endif //  TWO_SCALERS
#ifdef ARM
  // ARM  scalers with real LNE input (or get empty first bin problem)
    sis3820_RegisterWrite(gVme, SIS3820_BASE_B,SIS3820_KEY_OPERATION_ARM,0);
    if(dsis)printf ("Armed SIS3820 module B \n");
#else
    sis3820_RegisterWrite(gVme, SIS3820_BASE_B,SIS3820_KEY_OPERATION_ENABLE,0);
    if(dsis)printf ("Enabled SIS3820 module B \n");
#endif 
    //  temp = sis3820_RegisterRead(gVme,SIS3820_BASE_B, SIS3820_ACQUISITION_COUNT); // global
    //  printf("\nstart_scalers: SIS3820_ACQUISITION_COUNT reg reads %d bins \n",temp);
    //  sis3820_Status(gVme, SIS3820_BASE_B);
#endif // HAVE_SIS3820
  return SUCCESS;
}

void get_readout_time(void)
{
 // readout timing test
 // t1 is set when number of LNE is reached
  double elapsedTime;
  int i;

  if(gbl_CYCLE_N > 1)
    {
      // stop timer
      gettimeofday(&t2, NULL);
      
      if(hel_flip_flag)  // helicity has just flipped. Time includes helicity sleep - ignore 
	hel_flip_flag=0; // reset flag
      else
	{
	  // compute and print the elapsed time in millisec
	  elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
	  elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
	  
          i=(INT)elapsedTime;
          if(i<cb_time_size)
	    cb_time[i]++;

	  if( elapsedTime >  gbl_maxElapsedTime)
	    gbl_maxElapsedTime =  elapsedTime;
	  if( elapsedTime <  gbl_minElapsedTime)
	    gbl_minElapsedTime =  elapsedTime;

	  if(elapsedTime > fs.input.daq_service_time__ms_ )
	    gbl_cb_cntr2++; // count number times exceeds service time - should be the same as gbl_ppg_cb_cntr1
	  gbl_sumElapsedTime+=elapsedTime;
	  gbl_sumNum++; // number of values added to gbl_sumElapsedTime

	  if(dd[18]) 
	    printf("Elapsed time: %f  ms.  Max: %f ms  Min: %f ms \n",elapsedTime, gbl_maxElapsedTime, gbl_minElapsedTime );
	}
    }
}
#ifdef HAVE_NIMIO32
INT write_cb_stats(void)
{
  char filename[128];
  char str[]="/Experiment/Name"; 
  char path[80];
  FILE *cbfile;
  int i,j,size,status;
  char name[32];

  size = sizeof(name); 
  status = db_get_value(hDB, 0, str, name, &size, TID_STRING, FALSE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"write_cb_stats","cannot get experiment name at %s (%d)",str,status);
    return FE_ERR_ODB;
  }
  sprintf(path,"%s/stats",getenv("EXPERIM_DIR"));
  sprintf(filename,"%s/cbstats_%d.txt",path,gbl_run_number);
  printf("write_cb_stats: opening stats file %s\n",filename);
  cbfile = fopen (filename,"w");
  if(cbfile == NULL)
    {
      printf("write_cb_stats: could not open file %s\n",filename);
      return FAILURE;
    }
  printf("write_cb_stats: opened stats file %s\n",filename);

  fprintf(cbfile, "Time taken to process scaler data into histograms:\n");
  fprintf(cbfile,"Counts  0    5    10    15    20    25\n");
  fprintf(cbfile,"      ---------------------------------------------------------\n");
  fprintf(cbfile,"Time (ms)   Starting at minimum ...\n");
  for(i=gbl_minElapsedTime; i<gbl_maxElapsedTime; i++)
    {
      fprintf(cbfile,"%3.3d     |",i);
      for (j=0; j<cb_time[i]; j++)
	fprintf(cbfile,"*");
      fprintf(cbfile,"\n");
    } //  end of const1f mode
  fprintf(cbfile," Ending at maximum time...\n");    
 

  fprintf(cbfile,"\n----------------------------------------------------------------\n");
  fprintf(cbfile,"Data:\n");
  i=j=0;
  while (i< cb_time_size)
    {
      if(j==0 || j >=10 )
	{
	  fprintf(cbfile,"\n%3.3d   %6.6d ",i,cb_time[i]);
	  j=1;
	}
      else if (j<10)
	{
	  fprintf(cbfile,"%6.6d ",cb_time[i]);
	  j++;
	}
      i++;
    }
  fprintf(cbfile,"\n");
  fclose(cbfile);
  return SUCCESS;
}
#endif // HAVE_NIMIO32

// ===================================================
// parked here just in case ever needed
#ifdef GONE
#ifdef HAVE_SIS3801
 printf(" sending an internal LNE \n");
#ifdef TWO_SCALERS
  sis3801_next_clock(gVme, SIS3801_BASE_A);
#endif //  TWO_SCALERS
  sis3801_next_clock(gVme, SIS3801_BASE_B);
#endif // HAVE_SIS3801
#endif //GONE

void print_hel_time(INT state)
{
  print_time();
  if(fs.hardware.enable_dual_channel_mode)
    printf("   read latched helicity = %d or %s  (current %d or %s) \n",state, helicity_state(state), gbl_dcm_hcur, helicity_state(gbl_dcm_hcur));
  else
    printf("   read helicity = %d or %s\n",state, helicity_state(state));
}

void print_time ()
{ // from internet
 struct timeval tv;
 struct tm* ptm;
 char time_string[40];
 long milliseconds;

 /* Obtain the time of day, and convert it to a tm struct. */
 gettimeofday (&tv, NULL);
 ptm = localtime (&tv.tv_sec);
 /* Format the date and time, down to a single second. */
 strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);
 /* Compute milliseconds from microseconds. */
 milliseconds = tv.tv_usec / 1000;
 /* Print the formatted time, in seconds, followed by a decimal point
   and the milliseconds. */
 printf ("%s.%03ld", time_string, milliseconds);
}


INT set_client_alarm(INT val)
{
  const char str[]="/Alarms/alarms/client alarm/active";
  INT status;
  BOOL b;
  char *alarmstate[]={"disabled","enabled"};

  b=val;
  status = db_set_value(hDB, 0,str , &b, sizeof(b), 1, TID_BOOL);
  if(status != DB_SUCCESS)
      cm_msg(MERROR,"set_client_alarm","Error setting ODB Key\"%s\" (%d)",str,status);
  else
    printf("set_client_alarm: client alarm is now %s\n",alarmstate[val]);
  return status;
}
