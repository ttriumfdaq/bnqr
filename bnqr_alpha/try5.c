#include <stdio.h>
#include <string.h>
#include <ctype.h>


int main(void)
{
  int i,j,k,tmp,rd;
  int max=500;
  unsigned int pfifo[1000];

  rd=25;
  for(i=0;i<50;i++)
    pfifo[i]=i;
  
  //  printf("sis3820 32-bit data (first 4 data words): 0x%x 0x%x 0x%x 0x%x\n",pdata32[0],pdata32[1],pdata32[2],pdata32[3]);
  i=0;
  tmp=rd;
  if (tmp > max)
    {
      tmp = max;
      printf("Dumping first  %d words of data\n",tmp);
      printf("Counter              Data");
    }
  j=10; // print sets of 10 across page
  if(tmp < 10)
    j=tmp; // less than 10 data words
  while (i<tmp)
    {
      if (i % 10 ==0 )
	printf("\n%6.6d  ", i);
      for (k=0; k < j; k++) 
	{
	  printf("0x%8.8x ", pfifo[i]);
	  i++;
	  if(i>=tmp)break;
	}
    }
  printf("\n");
  return 1;
}
