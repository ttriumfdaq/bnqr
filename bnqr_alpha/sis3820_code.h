// sis3820_code.h
#ifndef SISCODE
#define SISCODE
#ifdef HAVE_SIS3820
// These are for SIS3820 only
INT NumSisChannels_A, NumSisChannels_B;

// prototypes from sis3820_code.h
INT  init_sis3820(BOOL bor_flag);
INT get_SIS_dataformat(INT nbits, INT NumSisChannels );
void SIS_readback(unsigned int sis_base, unsigned int wrote_mode, unsigned int wrote_csr);
void opmode_bits(unsigned int data);
void csr_bits(unsigned int data);
int nchan_enabled(unsigned int bp, int en );
int check_sis3820(MVME_INTERFACE *mvme, DWORD base, DWORD *id);
DWORD get_enabled_channels_bitpat(int num_ch_enabled,  BOOL ch1ref);
DWORD get_enabled_channels_bitpat_alpha(int num_ch_enabled, int alpha_offset);
#endif
#endif
