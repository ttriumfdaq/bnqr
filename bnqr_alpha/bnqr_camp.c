#ifdef CAMP_ACCESS

/* ------------------------------------------------------------------*/
INT camp_get_rec(void)
/*------------------------------------------------------------------------*/
{
  /* retrieve the ODB structure for CAMP sweep device
       called by begin_of_run

 */
  INT size, status;
  char str[128];

  /* check we have a key hCamp (found in main)  */
  if( hCamp)
    {
 /* get the record for camp area  */
      size = sizeof(fcamp);
      status = db_get_record (hDB, hCamp, &fcamp, &size, 0);/* get the whole record for mdarc */
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"camp_get_record","Failed to retrieve %s record  (%d) size=%d ",str,status,size);
	  return(status); /* error */
	}
      else
	if(dc)printf("Got the record for camp\n");
    }
  else
    {
      cm_msg(MERROR,"camp_get_record","No key has been found for camp record ");
      return(DB_NO_KEY); /* error */
    }
  return(SUCCESS);
}



/* ------------------------------------------------------------------*/
INT camp_create_rec(void)
/* ------------------------------------------------------------------*/
{
  /* retrieve the ODB structure for CAMP sweep device
  */
  INT size, status;
  char str[128];
  FIFO_ACQ_CAMP_SWEEP_DEVICES_STR(fifo_acq_camp_sweep_devices_str); /* for camp (from experim.h) */


  /* get the key hCamp  */
  sprintf(str,"/Equipment/%s/camp sweep devices",equipment[FIFO].name); 
  status = db_find_key(hDB, 0, str, &hCamp);
  if (status != DB_SUCCESS)
    {
      if(dc) printf("camp_create_rec: Failed to find the key %s ",str);
      
      /* Create record for camp area */     
      if(dc) printf("camp_create_rec:Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(fifo_acq_camp_sweep_devices_str));
      if (status != DB_SUCCESS)
	{
	  if(dc)printf("camp_create_rec: Failure creating camp record\n");
	  cm_msg(MINFO,"camp_create_rec","Could not create record for %s  (%d)\n", str,status);
	  return(-1);
	}
      else
	if(dc) printf("camp_create_rec: Success from create record for %s\n",str);
      /* try again to get the key hCamp  */
      status = db_find_key(hDB, 0, str, &hCamp);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"camp_create_rec", "Failed to get the key %s ",str);
	  return(-1);
	}
    }    
  else  /* key hCamp has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hCamp, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "camp_create_rec", "error during get_record_size (%d) for camp",status);
	  return status;
	}
      printf("camp_create_rec - Size of camp saved structure: %d, size of camp record: %d\n", 
	     sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICES) ,size);
      if (sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICES) != size) 
	{
	  cm_msg(MINFO,"camp_create_rec","creating record (camp; mismatch between size of structure (%d) & record size (%d)", 
		 sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICES) ,size);
	  /* create record */
	  status = db_create_record(hDB, 0, str , strcomb(fifo_acq_camp_sweep_devices_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"camp_create_rec","Could not create camp record (%d)\n",status);
	      return status;
	    }
	  else
	    if (dc)printf("camp_create_rec: Success from create record for %s\n",str);
	}
    }
  return(SUCCESS);
}

/* ------------------------------------------------------------------*/
INT camp_update_params(void)
/* ------------------------------------------------------------------*/
{
  char str[128];
  INT size,status;
  char hostname[128];

  /* camp handle and record should have been got already */
  if(!hCamp)
    {
      cm_msg(MINFO,"e1c_compute","Error: no CAMP settings available");
      return(DB_NO_ACCESS);
    }
  /* rf_config or this pgm MUST have checked the parameters already */
  strcpy(camp_params.SweepDevice, fs.input.e1c_camp_device);

  /* fill structure camp_params */
  if(  strncmp(fs.input.e1c_camp_device, fcamp.frequency_generator.sweep_device_code,2 ) == 0)
    {
      strncpy(camp_params.InsPath, fcamp.frequency_generator.camp_path,32);
      strncpy(camp_params.InsType,  fcamp.frequency_generator.instrument_type,32);
      strncpy(camp_params.IfMod,  fcamp.frequency_generator.gpib_port_or_rs232_portname,32);
      strncpy(camp_params.setPath,  fcamp.frequency_generator.camp_scan_path,80); /* /afg/frequency */
      strncpy(camp_params.DevDepPath, fcamp.frequency_generator.camp_device_dependent_path,80); /* should be blank */
      strcpy(camp_params.units,  fcamp.frequency_generator.scan_units);
      camp_params.maximum_value = fcamp.frequency_generator.maximum;
      camp_params.minimum_value = fcamp.frequency_generator.minimum;
      camp_params.conversion_factor = fcamp.frequency_generator.integer_conversion_factor;
      /* and set gbl_scan_flag to indicate CAMP freq scan */
      gbl_scan_flag = 1<<8;      /* camp frequency scan (value = 0x100) */
    }
  else if (strncmp(fs.input.e1c_camp_device, fcamp.magnet.sweep_device_code,2)  == 0)
    {
      strcpy(camp_params.InsPath, fcamp.magnet.camp_path);
      strcpy(camp_params.InsType,  fcamp.magnet.instrument_type);
      strcpy(camp_params.IfMod,  fcamp.magnet.gpib_port_or_rs232_portname);
      strcpy(camp_params.setPath,  fcamp.magnet.camp_scan_path); 
      strncpy(camp_params.DevDepPath, fcamp.magnet.camp_device_dependent_path ,80 );
      strcpy(camp_params.units,  fcamp.magnet.scan_units);
      camp_params.maximum_value = fcamp.magnet.maximum;
      camp_params.minimum_value = fcamp.magnet.minimum;
      camp_params.conversion_factor = fcamp.magnet.integer_conversion_factor;
      /* and set gbl_scan_flag to indicate CAMP Magnet scan */
      gbl_scan_flag = 1<<9;      /* camp magnet scan (value = 0x200) */
    }
  /* BNMR supports DAC scan for testing */
   else if (strncmp(fs.input.e1c_camp_device, fcamp.dac.sweep_device_code,3)  == 0)
    {
      strcpy(camp_params.InsPath, fcamp.dac.camp_path);
      strcpy(camp_params.InsType,  fcamp.dac.instrument_type);
      strcpy(camp_params.IfMod,  fcamp.dac.gpib_port_or_rs232_portname);
      strcpy(camp_params.setPath,  fcamp.dac.camp_scan_path); 
      strncpy(camp_params.DevDepPath, fcamp.dac.camp_device_dependent_path ,80 );/* path for monitoring */
      strcpy(camp_params.units,  fcamp.dac.scan_units);
      camp_params.maximum_value = fcamp.dac.maximum;
      camp_params.minimum_value = fcamp.dac.minimum;
      camp_params.conversion_factor = fcamp.dac.integer_conversion_factor;
      /* and set gbl_scan_flag to indicate CAMP Dac scan */
      gbl_scan_flag = 1<<10;      /* camp dac scan (value = 0x400) */
    }

  else
    {
      cm_msg(MERROR,"camp_update_params","unknown scan device %s ",fs.input.e1c_camp_device);
      return FE_ERR_ODB;
    }
  
  /* Get the camp hostname from the mdarc area of odb  */
  size = sizeof(hostname); 
  sprintf(str,"/Equipment/%s/mdarc/camp/camp hostname",equipment[FIFO].name); 
  status = db_get_value(hDB, 0, str, hostname, &size, TID_STRING, FALSE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"camp_update_params","cannot get Camp hostname at %s (%d)",str,status);
    return FE_ERR_ODB;
  }
  /*printf("Hostname: %s\n",hostname);*/
  strncpy(camp_params.serverName, hostname,LEN_NODENAME);
  if(dc)
    {
      printf("camp parameter settings:\n");
      printf("camp hostname: %s\n",camp_params.serverName);
      printf("sweep device: %s\n",camp_params.SweepDevice);
      printf("InsPath: %s\n",camp_params.InsPath);
      printf("InsType: %s\n",camp_params.InsType);
      printf("IfMod: %s\n",camp_params.IfMod);
      printf("setPath: %s\n",camp_params.setPath);
      printf("DevDepPath: %s\n",camp_params.DevDepPath);
      printf("units: %s\n",camp_params.units);
      printf("maximum: %f\n",camp_params.maximum_value);
      printf("minimum: %f\n",camp_params.minimum_value);
      printf("conversion factor: %d\n",camp_params.conversion_factor);
      printf("gbl_scan_flag=%d\n",gbl_scan_flag);
    }
  return(SUCCESS);
}



/*------------------------------------------------------------------------*/
INT set_camp_value(float set_camp_val)
/*------------------------------------------------------------------------*/
{
  INT icount,status,stat;

  /*  BOOL watchdog_flag;
      DWORD watchdog_timeout; */

  /* this can take some time so set watchdog for 5 min 
   cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
   cm_set_watchdog_params(watchdog_flag, 300000);  // 5 min for reconnect 5*60*1000 
*/
 
  set_long_watchdog(gbl_long_watchdog_time);  /* 3 min for reconnect 3*60*1000 */

  status = set_sweep_device(set_camp_val, camp_params); 
  if(status == CAMP_SUCCESS)
    {
      /*      cm_set_watchdog_params(watchdog_flag, watchdog_timeout); // restore watchdog timeout */
      restore_watchdog();  /* restore watchdog timeout */
      return status;
    }
  else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

  /* failure */
  printf("set_camp_value: Error attempting to set CAMP value of %f %s; (%d)",
	 set_camp_val,camp_params.units,status);
  
  for(icount=0; icount < 3; icount++)
    { 
      if(gotCamp)
	{
	  status=camp_watchdog(); /* can we talk to CAMP? */
	  if(status == CAMP_SUCCESS)
	    {  /* try to set the device again */
	      printf("Retrying set_sweep_device (count=%d)...\n",icount);
	      status = set_sweep_device(set_camp_val,camp_params);
	      if(status == CAMP_SUCCESS)
		{
		  printf("set_camp_value: successfully set CAMP value of %f %s after %d retries\n",
			 set_camp_val,camp_params.units, icount);
		  restore_watchdog(); /* restore watchdog timeout */
		  //		  cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
		  return(status);
		}
	      else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

	    }
	}/* camp_watchdog failed... reconnect */
      gotCamp=FALSE;
      printf("set_camp_value: calling camp_reconnect...(count=%d)\n",icount);
      status = camp_reconnect(); /* disconnects from CAMP then reconnects (tries 20 times) */
      if(status == CAMP_SUCCESS)
	{
	  gotCamp=TRUE;
	  if(dc)
	    printf("set_camp_value: camp_reconnect was successful...retrying set_sweep_device...(count=%d)\n",icount);
	  status = set_sweep_device(set_camp_val,camp_params);
	  if(status == CAMP_SUCCESS)
	    {
	      cm_msg(MINFO,"set_camp_value","successfully set CAMP value=%f after reconnecting to CAMP (count=%d)",
		     set_camp_val,icount);
  		  restore_watchdog(); /* restore watchdog timeout */
		  //	      cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	      return(status);
	    }
	  else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

	  printf("set_camp_value:failed to  set CAMP value =%f (retry=%d)\n",
		 set_camp_val,icount);  
      
	}
      printf("set_camp_value: waiting 5s then retrying\n");
      stat = iwait(5000,"set_camp_value");
      if(stat != SUCCESS) return stat;
     
    } /* end of for loop */
  
  cm_msg(MERROR,"set_camp_value","Cannot set CAMP value = %f after %d retries, stop the run and fix the problem",
	 set_camp_val,icount);  
  
  /* stop the run */
  camp_end(); /*  disconnect from CAMP */
  gotCamp=FALSE;
  restore_watchdog(); /* restore watchdog timeout */
  
  stop_run();
  return  (status);
}


/*------------------------------------------------------------------------*/
INT camp_reconnect(void)
/*------------------------------------------------------------------------*/
{
  char *msg;
  INT status,stat;
  INT max_err=20;
  INT icount;

  /* calling programs have already set up long midas watchdog.
     Not called from the main programs */

  status = camp_clntEnd(); 
  if(status != CAMP_SUCCESS) 
    {
      msg = camp_getMsg();
      if( *msg != '\0' ) 
	{
	  printf("camp_reconnect: failure from camp_clntEnd\n");
	  printf( "CAMP error msg is \"%s\"\n", msg );
	  cm_msg(MERROR,"camp_reconnect","Failure from camp_clntEnd. Camp error message:\"%s\"",msg);
	}
    }
  else
    printf("camp_reconnect: success from camp_clntEnd\n");
  
  for(icount=0; icount < max_err; icount++)
    { 
      status = camp_init(camp_params);
      if( status == CAMP_SUCCESS  )
	{  /* camp_init sends its own message */
	  printf("camp_reconnect: successfully reconnected to camp\n");
          status=camp_watchdog(); /* can we talk to CAMP? */
	  if(status == CAMP_SUCCESS)
	      return (status);
	}

      icount++;
      printf("camp_reconnect: waiting 7s then retrying (retry=%d)\n",icount);
      stat = iwait(7000,"camp_reconnect" );

      if(stat != SUCCESS) return stat;      
    }
  cm_msg(MERROR,"camp_reconnect","Could not reconnect to CAMP");
  return (status);
}


/*------------------------------------------------------------------------*/
INT camp_watchdog(void)
/*------------------------------------------------------------------------*/
{
  /* Access camp to keep connection alive

     everything calling this has set a long midas watchdog parameter
  */
  char *msg;
  INT status;
  INT camp_errcount;

  printf("camp_watchdog: starting\n");

  status = campSrv_cmd("sysGetLogActs");
  if(status != CAMP_SUCCESS) 
    {
      camp_errcount++;
      msg = camp_getMsg();
      if( *msg != '\0' ) 
	{
	  printf("camp_watchdog: failure from campSrv_cmd\n");
	  printf( "CAMP error msg is \"%s\"\n", msg );
	  cm_msg(MERROR,"camp_watchdog","Failure from campSrv_cmd; camp error message:\"%s\"",msg);
	}
    }
  return (status);
}

/*-------------------------------------------------------------------*/
INT read_sweep_dev(float *read_camp_val, CAMP_PARAMS camp_params)
/*-------------------------------------------------------------------*/
{
  INT icount,status;
  float rcv;
  /*  BOOL watchdog_flag;
      DWORD watchdog_timeout; */


  /* camp access can take some time so set watchdog for 5 min 
    cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  // 5 min for reconnect 5*60*1000 
*/
  set_long_watchdog(gbl_medium_watchdog_time); /* 2 min for reconnect 2*60*1000 */

  status = read_sweep_device(&rcv, camp_params); 
  if(status == CAMP_SUCCESS)
    {
      *read_camp_val=rcv;
      //      cm_set_watchdog_params(watchdog_flag, watchdog_timeout);/* restore watchdog timeout */
      restore_watchdog();/* restore watchdog timeout */
      return status;
    }
  else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

  /* failure */
  printf("read_sweep_dev: Error attempting to read sweep value\n");
  
  for(icount=0; icount < 3; icount++)
    { 
      if(gotCamp)
	{
	  status=camp_watchdog(); /* can we talk to CAMP? */
	  if(status == CAMP_SUCCESS)
	    {  /* try to read the device again */
	      printf("Retrying read_sweep_device (count=%d)...\n",icount);
	      status = read_sweep_device(&rcv,camp_params);
	      if(status == CAMP_SUCCESS)
		{
		  printf("read_sweep_dev: successfully read CAMP value of %f %s after %d retries\n",
			 rcv,camp_params.units, icount);
		  *read_camp_val=rcv;
		  restore_watchdog();/* restore watchdog timeout */
		  //	  cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
		  return(status);
		}
	      else if (status == -1) gotCamp=FALSE; /* don't have camp any more */
	    }
	}/* camp_watchdog failed... reconnect */
      gotCamp=FALSE;
      printf("read_sweep_dev: calling camp_reconnect...(count=%d)\n",icount);
      status = camp_reconnect(); /* disconnects from CAMP then reconnects (tries 20 times) */
      if(status == CAMP_SUCCESS)
	{
	  gotCamp=TRUE;
	  if(dc)
	    printf("camp_reconnect was successful...retrying read_sweep_device...(count=%d)\n",icount);
	  status = read_sweep_device(&rcv,camp_params);
	  if(status == CAMP_SUCCESS)
	    {
	      cm_msg(MINFO,"read_sweep_dev","successfully read CAMP value=%f after reconnecting to CAMP (count=%d)",
		     rcv,icount);
	      *read_camp_val=rcv;
	      restore_watchdog();/* restore watchdog timeout */
	      // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	      return(status);
	    }
	  else if (status == -1) gotCamp=FALSE; /* don't have camp any more */

	  
	  printf("read_sweep_dev:failed to  read CAMP sweep value  (retry=%d)\n",icount);  
      
	}
      printf("read_sweep_dev: waiting 5s then retrying\n");
      status = iwait(5000,"read_sweep_dev" );
      if(status != SUCCESS)
	return status;
    } /* end of for loop */
  
  cm_msg(MERROR,"read_sweep_dev","Cannot read CAMP sweep device after %d retries, stop the run and fix the problem",
	 icount);  
  
  /* stop the run */
  camp_end(); /*  disconnect from CAMP */
  gotCamp=FALSE;
  restore_watchdog();/* restore watchdog timeout */
  // cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  stop_run();
 
  return  (status);
}


#endif  /* CAMP */
