#ifdef GONE
INT scaler_increment_alpha(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
			   INT maximum_channel,INT *gbl_bin, DWORD *userbit )
{
  // This is for testing the SIS3820 scaler in 8 or 16 bit data mode, using alpha mode
  // and the user bit 2 (last bin) signal in one of the inputs

  DWORD *ps, i, h, ub, ub1, ub2, *pbuf, *pus, sdata;
  INT ch,ub_histo;
  if(!gbl_alpha)
    return 0;

  // 8 bit or 16 bit Scaler data; no channel number
#ifdef TWO_SCALERS
  ub_histo = N1_HISTO_MAXA -1;
#else
  ub_histo = N1_HISTO_MAXB - 1; /* add user bit histo if one scaler only */
#endif
  pus = histo[ub_histo].ph ;   /*  User bit histogram is hardcoded  */
  pbuf = pfifo;

  printf("scaler_increment_alpha: starting with  nwords:%d pfifo:%p, pus:%p maximum_channel=%d  *gbl_bin=%d\n",
	     nwords, pbuf,pus,maximum_channel,*gbl_bin);
  printf(" Reference Ch1 should not be enabled\n");

  printf("  Scaler B in Alpha mode: %d words\n",nwords);
  // 16bit data Ref 1 Enabled  Ch2 and Ch26 are sent by scaler (not used)
  // Word 1  (Ch2)   Ch1 (Ref)
  // Word 2  Ch18    Ch17 (UB)  feeding user bit into a channel
  // Word 3  Ch20    Ch19
  // Word 4  Ch22    Ch21
  // Word 5  Ch24    Ch23
  // Word 6  Ch25    (Ch26)
  ch=0;
  INT channels[]={1,2,17,18,19,20,21,22,23,24,25,26}; // these are the channels readout by the scaler (16bit mode)
  INT ch_index,word;
  INT mask;
  // 8 bit data comes in groups of 4 channels 
  //  INT channels[]={1,2,3,4,17,18,19,20,21,22,23,24,25,26,27,28}; // these are the channels readout by the scaler (8bit mode)
  //  The following is for 16 bit data mode. Add 8 bit mode if needed.
  DWORD datahi, datalo;
  word=0;
  ch_index=0;
 

  ub=0;
  for (i=0 ; i < nwords ; i++)
    { // main for loop on the number of words
      word++;
      if(word > 6)
	{ 
	  word=1;
      
	  (*gbl_bin)++; // next bin
	  if(ub)
	    {
	      *gbl_bin=0; // new cycle
	      ub=0;
	    }
	}
      if(ch_index > 11) ch_index=0;

      printf("scaler_increment_alpha: i=%d  word=%d *gbl_bin=%d  data 0x%x ",
	     i, word, *gbl_bin,  (*pbuf & 0xFFFFFFFF));
      datalo=(*pbuf & 0xFFFF);
      datahi=(*pbuf & 0xFFFF0000)>>16;
      printf("Ch %d : 0x%x(%d) ;  Ch %d : 0x%x(%d) ",  channels[ch_index+1], datahi, datahi,
	     channels[ch_index], datalo,datalo);
      if(datalo == 1 || datahi == 1 )
	{ // look in lo or hi i.e. all channels (test.. in case miscounted! )
	  ub=1; // user bit is 1 (last bin);
	  printf("user bit!  Last bin");
	}
  
      printf("\n");    
      ch_index+=2;
      pbuf++;
    }
  
    printf("\nscaler_increment_alpha returns with *gbl_bin %d\n",*gbl_bin);
  return 0;
}
#endif // GONE

