/* 
 Scaler_increment without discard first bin, with UBIT histo
 for BNMR/BNQR                          - NO pulse pair mode 
 */
/*-- individual scaler histo increment ------------------------------------*/
INT scaler_increment_nopp(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
                            INT maximum_channel,INT *gbl_bin, DWORD *userbit )
/*
  - The fifo is composed of sequences of nscalers.
  Each DWORD data contains the channel and data info
  32      24                      1
  v       v                      v
  10987654321098765432109876543210
  UUFCCCCCddddDDDDddddDDDDddddDDDD
  where U: user bits (front panel inputs)
  F: latch bit (not used)
  C: channel bit (C4..C0)
  dD: data bit  (24bit)
  - for each word extract channel number (h) from the C4-C0 field
  - add that channel (h) total sum (per cycle).
  - increment current scaler[h] bin by fifo content (by cycle).
  - increment gbl_BIN every h=0 refering to a new scaler sequence.
  - type 2: if random_flag && e2a_flag or e2e_flag or e2f_flag increment correct bin (see note below)
  -         if any other mode 2, histogram bins directly

FIFO data from scaler comes out in this order:
            
ch 0 bin 0 
ch 1 bin 0
........
ch N bin 0

ch 0 bin 1
ch 1 bin 1
.........
ch N bin 1

----------
ch 0 bin M
ch 1 bin M
.........
ch N bin M
*/
{
  DWORD *ps, i, h, ub, ub1, ub2, *pbuf, *pus, sdata;
  INT     channel_field;
  INT  index,status;
  INT ub_histo=0;
  INT input_offset;

  // for BNQR test
  INT ub_histo_flag,fill_uarray_2,pulsepair_flag;
  ub_histo_flag=fill_uarray_2=pulsepair_flag=0;

  if(gbl_nprebins != n_bins)
    { // Type 2 pulse pair modes only 
      printf("scaler_increment: ERROR this routine does not histogram Type 2 pulse-pair modes. Expect gbl_nprebins(%d) = n_bins(%d)\n",
	     gbl_nprebins,n_bins);
      printf("scaler_increment: use scaler_increment_pulsepair() for pulsepair modes. pulsepair_flag=%d\n",pulsepair_flag);

      cm_msg(MERROR,"scaler_increment",
	     "Programming ERROR - this routine histograms all Type 1 and non-pulsepair Type 2 modes; number of prebins must equal num bins");
      goto retminus1;
    }

  // All other modes Type 1, Type 20,2s,2w

  input_offset=0; // inputs start at 0
#ifdef HAVE_SIS3820
  if(maximum_channel != 32)  // Scaler B  NIM inputs start at 16
      input_offset=16;
#endif //  HAVE_SIS3820

  ub2=ub1=ub=0; // initialize
  pbuf = pfifo;

  if(ub_histo_flag) // TRUE if userbit histo is wanted. TRUE for Type 1 and some TYPE 2 modes. 
    {
      /* user-bit histo number is hardcoded here */
#ifdef TWO_SCALERS
      if(exp_mode == 1)
	ub_histo = N1_HISTO_MAXA -1;  // Type 1 userbit histo always required for Type 1
      else
	ub_histo = N2_HISTO_MAXA -1;  // Type 2
#else
      if(exp_mode == 1)
	ub_histo = N1_HISTO_MAXB - 1; /* add user bit histo if one scaler only */
      else
	ub_histo = N2_HISTO_MAXB - 1;  // Type 2
#endif
      pus = histo[ub_histo].ph ;   /*  User bit histogram is hardcoded  */
      printf("ub_histo = %d\n",ub_histo);  
    } // ub_histo_flag
  else
    pus = NULL; // no userbit histogram

  if (dd[1])
    { 
      printf("scaler_increment: starting with  nwords:%d pfifo:%p, pus:%p maximum_channel=%d  *gbl_bin=%d\n",
	     nwords, pbuf,pus,maximum_channel,*gbl_bin);
      
#ifdef HAVE_SIS3820
      printf("scaler_increment: SIS3820 inputs are offset from 0 by %d i.e. connected inputs are %d to %d \n",
	     input_offset,input_offset, (input_offset+maximum_channel));
#endif
      
      printf("scaler_increment  n_bins=%d n_his_bins=%d \n",
		       n_bins,n_his_bins);
      
    } // debug
  
  if(dd[17])
    {
      printf("\n nwords=%d  scaler_offset=%d",nwords,scaler_offset);
#ifdef TWO_SCALERS
      if(scaler_offset==0)
	printf("  Scaler A");
#endif
	printf("  Scaler B");
      printf("\n");
    }

  for (i=0 ; i < nwords ; i++)
    { // main loop on the number of words
    
      /* extract channel number */
      channel_field = (*pbuf >> 24);
      
      /* h has a range from 0..31 */
      h = channel_field & 0x1F;
      
      if(dsis)
	if(h<1)printf("scaler_increment: i=%d  *gbl_bin=%d chan %d data 0x%x \n",i, *gbl_bin, h,(*pbuf & 0xFFFFF));

      if(ub_histo_flag)
	{
	  ub = channel_field >>6;
	  if (ub>4 || ub < 0)
	    {
	      printf("\n ERROR ub is not in limits : %i\n",ub);
	      cm_msg(MERROR,"scaler_increment","programming error  ub is not in limits : %i",ub);
	      goto retminus1;
	    }
	  // fill_uarray false once array filled with first valid cycle data
	  if (!fill_uarray)   // Scaler A and Scaler B
	    {  /* check that the user bit is the same for this cycle for both modules (checks against array) */	      
	      if(ub != *(pus + *gbl_bin))
		{
		  printf("ERROR current ub = %x; array ub = %x for i %d and h %d gbl_bin=%d\n",
			 ub,*(pus + *gbl_bin),i,h,*gbl_bin);
		  cm_msg(MERROR,"scaler_increment","Error - user bit is not the same as that in array");
		  goto retminus1;
		}
	    }
	} // end of ub_histo_flag
      else  /* not histogramming userbits  */ 
	{
	  *userbit = channel_field >>6;
	  ub = *userbit;
	  ub1 = ub & 1;
	  ub2 = ub & 2;
	} //  end of  not histogramming userbits  
      
      /* make sure channel is correct */
#ifdef HAVE_SIS3820
      if (input_offset > 0)
	{
	  //if(dd[1]) printf("Channel h=%d from scaler will be adjusted to h=%d\n",h,(h-input_offset));
	  h=h - input_offset;
	}
#endif
      
      if( h < maximum_channel)
	{
	  index = h + scaler_offset;
	  /* assign pointer */
	  ps = scaler[index].ps;  /* pointer to start of histo  */
	  sdata = *pbuf & DATA_MASK; /* scaler data to be saved */
	  /* increment local cycle sum scaler */
	  scaler[index].sum_cycle += (double)sdata;
      
	  /* Regular debugging */
	  if(dd[1])
	    {
	      if(i<=100 && h == 0)
		{
		  printf("i,pbuf,*pbuf,h,ub,sdata,*gbl_bin: %d, %p %x, %d, %d, %x, %d \n",
			 i,pbuf,*pbuf,h,ub,sdata,*gbl_bin); 
		  if(i == nwords -1 )
		    printf("Last i,pbuf,h,ub,sdata,*gbl_bin: %d, %x, %d, %d, %x,%d \n",
			   i,*pbuf,h,ub,sdata,*gbl_bin); 
		    
		}
	    } // if dd[1]

	  /* add current time bin content to cumulative scaler histo */
	  *(ps+*gbl_bin) += sdata;

#ifdef TWO_SCALERS
	  if(exp_mode == 1)
	    {
#ifdef THIRTYTWO
	      /* increment cycle sum according to User Bit */
	      if(index < 16)
		scaler[BACK_USB0 + ub].sum_cycle += sdata;
	      else if (index < 32)
		scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#else  // TWO scaler inputs
	      /* increment cycle sum according to User Bit */
	      if(index ==0)
		scaler[BACK_USB0 + ub].sum_cycle += sdata;
	      else if (index ==1)
		scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#endif // THIRTYTWO
	    } // end of Type 1
#endif	  //  TWO_SCALERS
	  
	      
	  /* next incoming data */
	  pbuf++;  
	      
	  /* Increment time bin every h = N_SCALER_REAL */
	  if (h == (maximum_channel-1) )
	    {
	      if(exp_mode == 1)
		{
		  if(fill_uarray && scaler_offset == 0) /* true for Scaler A (2 scalers) or Scaler B (1 scaler)*/
		    {
		      *(pus + *gbl_bin) = ub;
		      if(dd[1]) 
			{
			  if(*gbl_bin < 5 ) 
			    printf ("update pus %p with ub %d\n",(pus+ *gbl_bin),ub);
			}
		    }
		}
	      else
		{ // Mode 2
		  if(ub_histo_flag)
		    {
		      if(fill_uarray_2 && scaler_offset == 0) // fill array once only per run or userbits will be summed)
			{
			  printf("Mode 2 filling ubit array\n");
			  *(pus + *gbl_bin) = ub;
			  if(ub > 0 ) 
			    printf ("updated pus (%p + *gbl_bin=%d)  with ub %d\n",pus,*gbl_bin,ub);
			  if(*gbl_bin >= (n_bins-1) ) // last bin 
			    fill_uarray_2=0; // don't fill histo again this run
			}
		    }
		  else  // check last bin for userbit 2
		    {
		      if(*gbl_bin == (n_bins-1) ) // last bin 
			{
			  if (ub != 2) 
			    printf("expect ub=2 on last bin for Type 2 (ub=%d *gbl_bin=%d n_bins=%d\n",
				   ub,*gbl_bin,n_bins);
			}
		    }
		} // end Mode 2


	      (*gbl_bin)++;   // next bin
	      if(dd[1])
		printf("h=%d, maximum_channel=%d incremented *gbl_bin to %d\n",h, maximum_channel, *gbl_bin);
	    } /* end of h == max channel-1 */
	}/* end of h< max_channel */
      else
	printf("Oops - incorrect channel: Idx:%d nW:%d ch:%d  data:0x%8.8x  and *gbl_bin=%d\n",i, nwords, h, *pbuf, *gbl_bin);
	  
     
    } // loop on the number of words
  status = 0;
  
 ret:
  if(dsis)
    printf("\nscaler_increment returns with *gbl_bin %d\n",*gbl_bin);
  return 0; /* success */
  
 retminus1:
  status =  -1;
  goto ret;
}

