/*-- individual scaler histo increment ------------------------------------*/
INT scaler_increment_pulsepair(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
                            INT maximum_channel,INT *gbl_bin, DWORD *userbit )
/*

  This is the combined version for all modes without skip first bin
  For now, use only for pulsepair modes


  - The fifo is composed of sequences of nscalers.
  Each DWORD data contains the channel and data info
  32      24                      1
  v       v                      v
  10987654321098765432109876543210
  UUFCCCCCddddDDDDddddDDDDddddDDDD
  where U: user bits (front panel inputs)
  F: latch bit (not used)
  C: channel bit (C4..C0)
  dD: data bit  (24bit)
  - for each word extract channel number (h) from the C4-C0 field
  - add that channel (h) total sum (per cycle).
  - increment current scaler[h] bin by fifo content (by cycle).
  - increment gbl_BIN every h=0 refering to a new scaler sequence.
  - type 2: if random_flag && e2a_flag or e2e_flag or e2f_flag increment correct bin (see note below)
  -         if any other mode 2, histogram bins directly

FIFO data from scaler comes out in this order:
            
ch 0 bin 0 
ch 1 bin 0
........
ch N bin 0

ch 0 bin 1
ch 1 bin 1
.........
ch N bin 1

----------
ch 0 bin M
ch 1 bin M
.........
ch N bin M

NOTE:  Type 2a : randomized
  *gbl_bin = true bin number
   findex =  RF data bin index; findex=0 at first bin with RF
   1st freq bin = number of bins with non-RF data
   actual freq value for this bin = prandom_freq[findex]
   pseqf[findex] = pointer in non-random freq array for this frequency value
  	
  Address to incr      *gbl_bin    Increment  Random   pseqf  Initial  
  data if not random                 at       freq(Hz)        freqency
  (Channel 0)
  ffc300                  0         ffc300        -      -       -  
  ffc304                  1         ffc30c      240000   2      200000   
  ffc308                  2         ffc304      200000   0      220000 
  ffc30c                  3         ffc310      260000   3      240000
  ffc310                  4         ffc308      220000   1      260000
  etc;

Similar tables could be made for the rest of the channels

NOTE

Enable pulse pairs:
If pulse_pairs are enabled, we histogram based on the userbit1_action:
e2a pairs mode      userbit1_action:
0   pairs - histo all the data; n_bins = n_his_bins = fs.output.num_dwell_times as calculated by rf_config

                  Compaction Modes:
       n_bins =  fs.output.num_dwell_times;  n_his_bins =  n_bins -  output.num_freq_steps
1   first - histo only the first bin of the pair (odd) ; 
2   second - histo only the second bin of the pair (even); 
3   diff - odd-even; 

Action: 1=keep 2=discard 3=diff 

Note: Rf_config ensures number of bins with no RF MUST be set to 0 for pulse pair modes

Histogramming in pulse pair mode=1st (i.e. compaction) not random
 Address to incr      *gbl_bin pseqf UB1 Action   Increment   
  data                                                at         
  (Channel 0)                                                    
  ffc304                  0      0     1    1      ffc304     
  ffc308                  1            0    2        -    
  ffc30c                  2      1     1    1      ffc308      
  ffc310                  3            0    2        -
  ffc314                  4      2     1    1      ffc30C   
  ffc318                  5            0    2        -
  ffc31C                  6      3     1    1      ffc310
  ffc320                  7            0    2        -





  Derandomizing in pulse pair mode (pairs):

 Address to incr      *gbl_bin   UB1    Increment  Random   pseqf        Initial  
  data if not random                      at       freq(Hz)              frequency
  (Channel 0)                                                            Array
  ffc304                  0       0      ffc314      240000   2         200000   
  ffc308                  1       1      ffc318      240000   2         220000 
  ffc30c                  2       0      ffc31C      260000   3         240000
  ffc310                  3       1      ffc320      260000   3         260000
  ffc314                  4       0      ffc30C      220000   1
  ffc318                  5       1      ffc310      220000   1
  ffc31C                  6       0      ffc304      200000   0
  ffc320                  7       1      ffc308      200000   0


*/


{
  DWORD *ps, i, h, ub, ub1, ub2, *pbuf, *pus, sdata;
  WORD *qs;
  INT     channel_field;
  INT  index,status;
  // INT findex,sindex;
  // INT action,offset,dy;
  // char comment[32];
  INT ub_histo;
  INT hoffset;
  // INT  ntuple_width_h ;
  INT ib,ll;
  INT nbin,im,imh;
  BOOL ntuple,last_ntuple,flag_twice,ub_flag;
  char region[30];
  BOOL discard,subtract;
  
  INT beam_off_bin=0; // e2f only
  INT num_chans; // number of channels

  // SIS3801 -  All inputs are NIM, use the lowest channels for inputs

  if(dsis)printf("scaler_increment_pulsepair: starting\n");
  if(!pulsepair_flag)
    {
      printf("scaler_increment_pulsepair: ** incorrect scaler_increment routine has been called as pulsepair_flag is %d\n",
	     pulsepair_flag);
      printf("scaler_increment_pulsepair: use scaler_increment_nopulsepair\n");

      cm_msg(MERROR,"scaler_increment_pulsepair","incorrect scaler_increment routine has been called as pulsepair_flag is %d",
	     pulsepair_flag);
      return FAILURE;
    }



  num_chans = maximum_channel; // default
                               // when there's no offset from Ch0,  maximum_channel gives the max number of scaler channels
 
  //dsis=1; // TEMP
  //dd[1]=1;
 

#ifdef HAVE_SIS3820
  INT input_offset;
  INT chan0_flag; // sis3820 Scaler B chan 0 flag true if Ch0 (Input 1) Reference pulse enabled
  INT true_ch;  // scaler channel number from the data word 
  INT first_ch; // data should have this value for the first real channel 
                 // our SIS3820 scalers inputs are half ECL half NIM
  INT last_ch; // used in ALPHA mode
  chan0_flag=input_offset=first_ch=0; // inputs start at 0 (default)
  ps=NULL;
  qs=NULL;
  
#ifdef GONE
  // this is for testing 8bit or 16bit scaler data with alpha mode
  // determined that 24bit data mode is fast enough, so this is not used
  if(gbl_alpha)
    {
      status = scaler_increment_alpha(nwords, pfifo, scaler_offset, 
				      maximum_channel, gbl_bin, userbit );
      return status;
    }
#endif // GONE
  

  // SIS3820
  //  First 16 inputs are  ECL, last 16 are NIM
  //  Using NIM inputs unless more than ECL_OFFSET+1 = 16 real inputs are needed
  if(num_chans < (ECL_OFFSET+1))  // Maximum number of real channels required. 
    { // we are using only NIM inputs
      input_offset=ECL_OFFSET+1; // Ch 16 is the first NIM channel in use (ECL channel RefCh1 may also be enabled) 
      first_ch = input_offset; // default; channel value from data (h) will start at 16
    }
#ifdef TWO_SCALERS
  if(scaler_offset !=0 )
    { // Scaler B with 2 scalers (BNMR)
      if(fs.hardware.sis3820.sis_mode == 2) // REAL mode
	{
	  chan0_flag=1; // Ref Ch1 is  enabled 
	  first_ch=0; // channel  value from scaler data (h) will start at 0, followed by 16
	}
    }
#else  // one SIS3820 scaler, Scaler B  BNQR
  if(fs.hardware.sis3820.sis_mode == 2) // REAL mode
    {
      INT next_ch;
      if(gbl_alpha)
	{
	  chan0_flag = 0; // Ref Ch1 disabled
	  //     ALPHA mode  chan 17-24 in use (chan starts at 0)
	  first_ch = input_offset +  ALPHA_OFFSET -1 ; // first real channel in ALPHA mode 
                                                       // skip Ref 1 and Fl2 real input (16) so first_ch = 17
                                                       // channels < 17  DISABLED by scaler copy disable reg 
	  num_chans = num_chans - ALPHA_OFFSET;
	  last_ch = first_ch + num_chans -1; // last real channel in ALPHA mode chan 24
	  //	  if(dd[20])printf("alpha mode - first_ch=%d last_ch=%d, num_chans=%d\n",first_ch,last_ch,num_chans);
	}
      else
	{
	  if(!alpha1f_flag)
	    num_chans = num_chans - NUM_ALPHA; // alpha channels will be disabled in the scaler
	  
	  chan0_flag=1; // Ref Ch1 is  enabled in REAL mode by default
	  first_ch=0; // Ref Ch1
	  next_ch = input_offset; // channel 16 (FluorMon 2)
	  last_ch = next_ch + num_chans -2;  // last real channel
	  if(dd[20])printf("other modes - first_ch=%d next_ch=%d last_ch=%d, num_chans=%d\n",
			   first_ch,next_ch,last_ch,num_chans);
	}
    }
#endif // TWO_SCALERS
#endif //  HAVE_SIS3820
  
  ib=im=ub2=ub1=0; // zero these to get rid of warnings
  ub=discard=subtract=ntuple=flag_twice=0;
  ub_flag=1; /* default - if true, e2a/e2e userbit check is skipped */
  region[0]='\0'; // clear
  pbuf = pfifo;
  /* user-bit histo number is hardcoded here */
#ifdef TWO_SCALERS
  ub_histo = N1_HISTO_MAXA -1;
#else
  if(alpha1f_flag) 
    ub_histo = N1_HISTO_MAXB - 1; /* add user bit histo if one scaler only */
  else
    ub_histo = N1_HISTO_MAXB_ORIG - 1; /* add user bit histo if one scaler only */ 
#endif
  pus = histo[ub_histo].ph ;   /*  User bit histogram is hardcoded  */
  
  /* Rewritten this lot to include e2e.
     For all cases except e2e,e2a, and e2f with random_flag, nprebins = n_bins */
  if (dd[1])
    { 
      printf("scaler_increment: starting with  nwords:%d pfifo:%p, pus:%p num_chans=%d  *gbl_bin=%d\n",
	     nwords, pbuf,pus,num_chans,*gbl_bin);
    }
  if(dsis)
    {
#ifdef HAVE_SIS3820
      if(!gbl_alpha)
	{
	  printf("scaler_increment: SIS3820 inputs are offset from 0 by %d i.e. connected inputs are %d to %d \n",
		 input_offset,input_offset, (input_offset+num_chans));
	  if( chan0_flag)
	    printf("             Reference Ch1 is also enabled\n");
	}
#endif
      
      printf("scaler_increment: n_requested_bins=%d  n_bins=%d n_his_bins=%d gbl_nprebins=%d\n",
	     n_requested_bins, n_bins,n_his_bins,gbl_nprebins);
      
      
      if(e2e_flag || e2a_flag || e2f_flag) printf("scaler_increment: Mode 2a, 2e or 2f random_flag=%d pseqf=%p\n",
						  random_flag,pseqf);
    } // dsis debug
  
  if(e2f_flag)
    beam_off_bin = fs.output.num_dwell_times -  fs.input.e00_beam_off_dwelltimes;
  
  /* e2e/e2a/e2f : supports both random and not random */
  
  if(dd[17])
    {
      printf("\n nwords=%d  scaler_offset=%d",nwords,scaler_offset);
#ifdef TWO_SCALERS
      if(scaler_offset==0)
	printf("  Scaler A");
      else
	printf("  Scaler B");
#endif
      printf("\n");
    }
  
  for (i=0 ; i < nwords ; i++)
    { // main for loop on the number of words
      if(e2a_flag)
	discard=subtract=ntuple=ub_flag=0;
      else if(e2e_flag)
	flag_twice=ntuple=ub_flag=0;
      else if(e2f_flag)
        ub_flag=0;
      
      /* extract channel number */
      channel_field = (*pbuf >> 24);
      
      /* h has a range from 0..31 */
      h = channel_field & 0x1F;
#ifdef HAVE_SIS3820
      true_ch=h; // remember the channel number from the data  
#endif
      if(dsis)
	{  // debugging
	  // if(*gbl_bin < 2)
	    printf("scaler_increment: i=%d  *gbl_bin=%d scaler chan h=%d data 0x%x or %d \n",
		   i, *gbl_bin, h, (*pbuf & DATA_MASK), (*pbuf & DATA_MASK));
	}
      
      
#ifdef HACK2TO1
      /*  SIS3801 only
       *  Hack for switching scaler inputs 2 and 1 when one was broken. Now fixed.
       if( h==2 | h==1 ) printf ( "Hack: convert h from %d to %d\n", h, ( h==2 ? 1 : ( h==1 ? 2 : h )) );
      */
      
      h = ( h==2 ? 1 : ( h==1 ? 2 : h ));
#endif
      
      if(exp_mode == 1)
	{ 
	  ub = channel_field >>6;
	  if (ub>4 || ub < 0)
	    {
	      printf("\n ERROR ub is not in limits : %i\n",ub);
	      goto retminus1;
	    }
#ifdef TWO_SCALERS	  
	  if (!fill_uarray && scaler_offset == 0)
	    {  /* check that the user bit is the same for both modules */
	      
	      if(ub != *(pus + *gbl_bin))
		{
		  printf("ERROR current ub = %x; array ub = %x for i %d and h %d gbl_bin=%d\n",
			 ub,*(pus + *gbl_bin),i,h,*gbl_bin);
		  goto retminus1;
		}
	    }
#endif
	} // end of exp mode=1
      else  /* Type 2 */ 
	{
	  *userbit = channel_field >>6;
	  ub = *userbit;
	  ub1 = ub & 1;
	  ub2 = ub & 2;
	} //  end of exp mode=2
      
      
      /* make sure channel is correct */
      
      
#ifdef HAVE_SIS3820
      // adjust h for SIS3820
      if(!gbl_alpha)
	{ // Mode 2h
	  // deal with Ref Ch and offset input channels
	  if (input_offset > 0)
	    {
	      if(chan0_flag)  // Scaler B, ref Ch 1 enabled   true_ch=0 -> h=0
		{
		  if(h>0)  // true_ch=15 -> h=1 
		    h= h - input_offset+1; // first "real" scaler input should become h=1
		}
	      else
		{ // Ch 0 is not the reference
		  //if(dd[1]) printf("Channel h=%d from scaler will be adjusted to h=%d\n",h,(h-input_offset));
		  h=h - input_offset; //  first "real" scaler input should become h=0
		}
	    }
	}
      else
	{ //  Mode 2h with ALPHA          Scaler B 
	  //  This mode has ALPHA_OFFSET, no Ch Ref1
	  h=h-first_ch; 
	}
      if(dsis && (*gbl_bin < 2))
	printf("After channel adjustment,  *gbl_bin=%d num_chans=%d input_offset=%d  ScalerChan(true_ch)=%d and h is now %d ub=%d \n",
	       *gbl_bin,num_chans,input_offset,   true_ch,h,ub); 
      
#endif
      
      if( h < num_chans)
	{ // scaler_offset==0 for Scaler A
	  if(gbl_alpha)
	    index = h + ALPHA_OFFSET + scaler_offset; // skip first two array indices (Fl monitors, no malloc) 
	  else
	    index = h + scaler_offset;  // scaler array index; 
	  
	  /* assign pointer */
	  if(small_bin_flag)
	    qs = scaler[index].qs;  /* WORD pointer to start of histo  */
	  else
	    ps = scaler[index].ps;  /* DWORD pointer to start of histo  */
	  sdata = *pbuf & DATA_MASK; /* scaler data to be saved */
	  if(dsis)
	    {
	      if(*gbl_bin < 2)
#ifdef HAVE_SIS3820
		printf("h=%d true_ch=%d scaler_index=%d sdata=%d\n",h,true_ch, index,sdata);
#else
	      printf("h=%d  scaler_index=%d sdata=%d\n",h, index,sdata);
#endif
	    }
	
	  
	  
	  if(*gbl_bin < gbl_nprebins)  // SIS3801  *gbl_bin never reached n_bins (?) and starts at 0 (0-999 if 1000 bins)
	    // SIS3820  *gbl_bin reaches n_bins at end-of-cycle, but should be zeroed 
	    //          before next cycle. When first bin is discarded, star_gbl_bin starts
	    //          at bin 1 rather than bin 0. n_bins includes extra bin.
	    //          
	    {   // PREBINS 
	      /*  All modes that histogram bins directly will only come here since gbl_nprebins is set equal to number of bins
		  This includes Mode 20 
		  
		  PREBINS for e2a,e2a,e2f come here
		  prebins will be histogrammed directly
	      */
	      
	      sprintf(region,"PREBIN bin %d < gbl_nprebins %d",*gbl_bin,gbl_nprebins);
	      /* directly histo  for each channel (pre bins) */
	      
	      hoffset= *gbl_bin  ; /* histo offset within scaler histo for this bin */	


	      /* check user bit 2 for last bin of Type 2 (Type 1 don't set ub2 on last bin) - will only find modes that histogram directly */
	      if(exp_mode == 2 && h==0) /* userbin same for all channels - check only Ch 0 */
		{
		  if(*gbl_bin == fs.output.num_dwell_times -1)
		    { 
		      if(!ub2)/* userbin same for all channels - check only 1 */
			{
			  printf("*gbl_bin=%d, ub=%d; last bin=%d expected to be flagged with ub2\n",
				 *gbl_bin,ub, (fs.output.num_dwell_times -1));
			}
		      else
			printf("Found last bin IS flagged with ub2... *gbl_bin=%d, ub=%d; last bin=%d is flagged with ub2\n",
			       *gbl_bin,ub, (fs.output.num_dwell_times -1));
		    }
		}
	    }
	      
	  else if (*gbl_bin >=  fs.output.num_dwell_times - gbl_npostbins)
	    {  /*  POSTBINS  (modes e2e/e2a/e2f  only) */
	      
	      if(!e2a_flag && !e2e_flag && !e2f_flag )
		{
		  printf("programming ERROR - non 2a,2e,2f modes should have no postbins\n");
		  printf("*gbl_bin=%d  fs.output.num_dwell_times=%d   gbl_nprebins=%d\n",
			 *gbl_bin,fs.output.num_dwell_times, gbl_nprebins);
		
		  goto retminus1;
		}
	      
	      nbin = (*gbl_bin + gbl_npostbins) -  fs.output.num_dwell_times; /* post bin no. */
	      sprintf(region,"POSTBIN %d (bin %d) >=(%d - %d) or %d ",
		      nbin,*gbl_bin,fs.output.num_dwell_times,gbl_npostbins,(fs.output.num_dwell_times-gbl_npostbins));
	      hoffset= gbl_nprebins + gbl_nmidsection_h + nbin ; /* offset within histogram */
	      
	      /* check user bit for last bin */
	      if(*gbl_bin == fs.output.num_dwell_times -1)
		{ 
		   ub_flag=1;
		  if(!ub2 && h==0)/* userbin same for all channels - check only 1 */
		    {
		      printf("*gbl_bin=%d, ub=%d; last bin=%d expected to be flagged with ub2\n",
			     *gbl_bin,ub, (fs.output.num_dwell_times -1));
		    }
		  else
		    printf("Found last bin IS flagged with ub2... *gbl_bin=%d, ub=%d; last bin=%d is flagged with ub2\n",
			   *gbl_bin,ub, (fs.output.num_dwell_times -1));
		}
	      
	      if(e2f_flag && h==0)
		{
		  if (*gbl_bin == beam_off_bin)
		    {
		      if ( ub1)
			ub_flag=1; // start of beam off flagged by ub1
		      else
			printf("Expect ub1 to mark start of beam off for e2f at *gbl_bin %d\n",*gbl_bin);
		    }
		  
		  
		  if (*gbl_bin >= beam_off_bin)
		    {
		      nbin = *gbl_bin - beam_off_bin;
		      sprintf(region, "BEAM OFF bin %d (bin %d) >= beamoff bin %d",nbin, *gbl_bin,beam_off_bin );
		    }
		}
	      
	    }
	  
	  else 
	    {  /* now we have the NTUPLES	  
		  these will only be used in modes 2a, 2e, 2f. Other modes don't come here */
	      
	      //All Modes where gbl_nprebins less than number of bins 
	      
	      /*Look for the very first preRFbins which are part of the 1st ntuple. 
		e2e only....  e2a and randomized e2f prebins are not part of the ntuple and gbl_ndepthbins =0
	      */	 
	      ib = *gbl_bin-gbl_nprebins; /* ib = bin counter for ntuples */
	      if (ib < gbl_ndepthbins)
		{/* prebins e2e only */
		  sprintf(region,"PreRFbin %d < %d first Ntuple",ib,gbl_ndepthbins);
		  ntuple=0; /* these bins are part of first ntuple */
		  hoffset= gbl_nprebins + pseqf[ntuple] * gbl_ntuple_width_h + ib ;   
		}
	      else
		{  /* ntuple bins */
		  ll = ib - gbl_ndepthbins;
		  /* calculate ntuple number */
		  ntuple = ll / gbl_ntuple_width_s;
		  if(ntuple > fs.output.num_frequency_steps )/* max ntuple */
		    {
		      printf("ERROR: calculated ntuple number (%d) > max (%d)\n",
			     ntuple, fs.output.num_frequency_steps);
		      goto retminus1;
		    }
		  /* check for last ntuple & set a flag */
		  if( (ntuple+1) <  fs.output.num_frequency_steps )
		    last_ntuple=FALSE;
		  else
		    last_ntuple=TRUE; /* e2e data will be flagged by user bit 1 */
		  
		  im= ll %  gbl_ntuple_width_s  ; /* im is counter in scaler's ntuple */
		  imh= im+ gbl_ndepthbins; /* counter in histo's ntuple */
		  
		  /* RF region of ntuple... used for both e2e and e2a */
		  if(im < gbl_nRFbins)
		    {
		      
		      sprintf(region,"RFbin word %d ntuple # %d",im,(ntuple+1));
		      /* if running 2a, deal with pulse-pair mode now */
		      if(e2a_flag)
			{  /* pulse-pair mode : 
			      fs.output.e2a_pulse_pairs_mode = 9  not pulse pair mode
			      = 0  pairs mode
			      = 1  keep first bin of pair
			      = 2  keep second bin of pair
			      = 3  difference
			      
			      mode > 0 ... sort according to ub1 */
			  if( fs.output.e2a_pulse_pairs_mode < 4) /* pulse-pairs modes 0-3 */
			    {
			      ub_flag=1; /* special userbits; don't check ubins later */
			      
			      if(fs.output.e2a_pulse_pairs_mode==1)
				{       /* e2a mode 1 : save first bin of pulse-pairs (imh=0) */
				  if(!ub1)
				    discard=TRUE;
				}
			      else if(fs.output.e2a_pulse_pairs_mode == 2)
				{  /* mode 2 : save second bin of pulse-pairs (imh=1) */
				  if(ub1)
				    discard=TRUE;
				  else
				    imh=0; /* save data in the only RFbin in the Histo */
				}
			      else if ( fs.output.e2a_pulse_pairs_mode== 3)
				{ /* mode 3 : subtract second bin of pulse-pairs (imh=1) */
				  if(!ub1)
				    {
				      subtract=TRUE;
				      imh=0; /*  save data in the only RFbin in the Histo */
				    }
				}
			      else if ( fs.output.e2a_pulse_pairs_mode== 0)
				{
				  ub_flag = TRUE;
				  /* check for h==0 only */
				  if(h==0)
				    {
				      /* ub1 cycles on and off */
				      if (imh==0 && !ub1)
					printf("*gbl_bin=%d, 1st pulse of pair should have ub1 true ub=%d\n", *gbl_bin,ub); 
				      if (imh==1 && ub1)
					printf("*gbl_bin=%d, 2nd pulse of pair should have ub1 false ub=%d\n", *gbl_bin,ub); 
				    }
				}
			      if(dd[17])
				printf("imh %d ub1 %d e2a pulse pair mode %d discard %d subtract %d\n",
				       imh,ub1,fs.output.e2a_pulse_pairs_mode,discard,subtract);
			    } /* end of e2a pulse-pair mode enabled */
			  else 
			    {
			      ub_flag=TRUE;
			      /* e2a NOT pulse pair mode has ub1 set for all ntuple bins */
			      if (!ub1 && h==0) /* userbin same for all channels - check only 1 */
				printf("*gbl_bin=%d, ub=%d; RFbin e2a NOT pulsepair... expect ub1 to be true\n",
				       *gbl_bin,ub);
			      
			    } /* end of e2a NOT pulse pair */
			} /* end of e2a flag */
		      else if(e2e_flag)
			{ /*  2e mode */
			  if(last_ntuple)
			    {  /* check userbit for h=0 (all channels are the same)*/
			      ub_flag=TRUE;
			      if(!ub1 && h==0) /* only write the message for one channel */
				printf("UB mismatch:  *gbl_bin=%d, ub=%d; RFbin last ntuple... expect ub1 to be true\n",
				       *gbl_bin,ub);
			      else
				{
				  if(dsis)printf("*gbl_bin=%d, ub=%d; RFbin last ntuple... ub1 is true as expected\n",
				       *gbl_bin,ub);
				}
			    }
			}
		      else if(e2f_flag)
			{ /* e2f flag
			     all frequency ntuple bins should have UB1 set TRUE */
			  
			  sprintf(region,"RFbin %d (actual bin %d) ",ntuple,*gbl_bin);
			  ub_flag = TRUE;
			  if (!ub1 && h==0) /* userbin same for all channels - check only 1 */
			    printf("*gbl_bin=%d, ub=%d; RFbin Mode e2f  ... expect ub1 to be true\n",
				   *gbl_bin,ub);
			  
			}
		    } /* end of RF region of ntuple */
		  
		  
		  else
		    { /* these are 2e POST RF depth bins in this Ntuple */
		      if( !last_ntuple )
			{  /* except for very last Ntuple.. they also form PRE RF depth bins for next Ntuple */
			  sprintf(region,"PostRFbin %d Ntuple # %d",im, (ntuple+1)); 		    
			  flag_twice=1; /* flag these; they have to be histogrammed twice */
			  
			  /*  but are they from the last but one ntuple, i.e. prebins for last ntuple? */
			  if( ntuple+1 ==  fs.output.num_frequency_steps  - 1)
			    {
			      /* yes... they should be flagged with ub1 */
			      if(!ub1 && h==0) /* print message once only */
				printf("UB mismatch: *gbl_bin=%d, ub=%d; expect postbins for penultimate ntuple to be flagged by ub1\n",
				       *gbl_bin,ub);
			      else
				{
				  if(dsis)printf("*gbl_bin=%d, ub=%d; postbins for penultimate ntuple ARE flagged by ub1 as expected\n",
				       *gbl_bin,ub);
				}
			      ub_flag=TRUE; /* do not check ub */
			    }
			}  		    
		      else /* last ntuple, histogram bins only once as POST...check userbits */
			{
			  flag_twice=0;
			  ub_flag=TRUE;
			  
			  if(!ub1 && h==0) 
			    { /* print message once only; all channels are the same */
			      printf("UB mismatch: *gbl_bin=%d, ub=%d; expect last ntuple postbins to be flagged by ub1\n",
				     *gbl_bin,ub);
			    }
			  else
			    {
			      if(dsis)printf("*gbl_bin=%d, ub=%d; last ntuple postbins ARE flagged by ub1 as expected\n",
					     *gbl_bin,ub);
			    }
			  
			  if(imh ==  gbl_ndepthbins)
			    { /* last ntuple & last POST depth bin should also have ub2 true*/
			      if(!ub2 && h==0)/* userbin same for all channels - check only 1 */
				printf("UB mismatch: *gbl_bin=%d, ub=%d; expect very last ntuple postbin to also be flagged by ub2\n",
				       *gbl_bin,ub);
			      else if(dsis)
				printf("*gbl_bin=%d, ub=%d; very last ntuple postbin is also flagged by ub2 as expected\n",
				       *gbl_bin,ub);

			      sprintf(region,"PostRFbin last word %d (%d) last Ntuple",im,imh);
			    }
			  else
			    sprintf(region,"PostRFbin word %d (%d) last Ntuple",im,imh); 
			} /* end of last Ntuple */
		    } /* end of 2e  POST-RF bins of Ntuple */	
		  
		  /* calculate offsets to histogram these RF or post-depth bins for this ntuple */
		  /*         start of ntuples + which ntuple +  offset in ntuple */	      
		  hoffset= gbl_nprebins + pseqf[ntuple]*gbl_ntuple_width_h + imh ; 
		} // end of ntuple bins
	    } // end of NTUPLES
	
	      
	  /* 
	     Histogram the Data 
	  */
	      
	  /* Regular debugging */

	    if(dd[1])
	    {
	       if(h==1)
		{
		  if(i<=100)
		    {
		      printf("     i,pbuf,*pbuf,h,ub,sdata,*gbl_bin,hoffset: %d, %p %x, %d, %d, %x, %d, %d\n",
			     i,pbuf,*pbuf,h,ub,sdata,*gbl_bin,hoffset); 
		    }
	
	      
		  if(ub > 0)
		    printf("UB1:%d 2:%d  i,pbuf,*pbuf,h,ub,sdata,*gbl_bin,hoffset: %d, %p %x, %d, %d, %x, %d, %d\n",
			   ub1,ub2,i,pbuf,*pbuf,h,ub,sdata,*gbl_bin,hoffset); 
		  
		  if(*gbl_bin >= (n_bins -2))
		    {
		      printf("Last i,pbuf,h,ub,sdata,*gbl_bin,hoffset: %d, %x, %d, %d, %x, %d, %d\n",
			     i,*pbuf,h,ub,sdata,*gbl_bin,hoffset); 
		    }
		}
	    } // if dd[1]
	    
	  
	  
	  /* 2e/2a/2f debugging with dd[17] .... will dump every single bin e2e/e2a/e2f only */
	  //   if(dd[17])printf("i=%d ub=%d *gbl_bin=%d ch=%d ntuple=%d pseqf[%d]=%d %s",
	  //		       i,ub,*gbl_bin,h,ntuple,ntuple,pseqf[ntuple],region);
	  
	  
	  
	  if(dd[17])
	    { /* 2e/2a/2f debugging channel 0 */
	      if(h==0)
		printf(" i=%d *pbuf=%8.8x ub=%d *gbl_bin=%d ch=%d hoffset=%d %s \n",
		       i,*pbuf,ub,*gbl_bin,h,hoffset,region);
	    }
	  
	   if(!ub_flag) /* check the User Bits are zero (unless ub_flag is set for a region of 2e/2a/2f) */
	    {
	      if(h==0 && ub != 0)
		{
		  
		  printf(" Unexpected user bits set  (ub=%d) at *gbl_bin %d \n",ub,*gbl_bin);
		  printf("  where i=%d ub=%d *gbl_bin=%d ch=%d  %s \n",
			 i,ub,*gbl_bin,h,region);
		}
	    }
		  
	  /* check offset within histogram  */
	  if(hoffset > n_his_bins )
	    {
	      if(e2e_flag || e2a_flag || e2f_flag)  // e2f has same number of scaler bins as histo bins
		{
		  if(!dd[17])printf("i=%d ub=%d  *gbl_bin=%d ch=%d ntuple=%d pseqf[%d]=%d %s\n",
				    i,ub,*gbl_bin,h,ntuple,ntuple,pseqf[ntuple],region);
		}
	      printf("\n ERROR histo offset out of range (%s): hoffset=%d  nhis=%d programming error \n",
		     region,hoffset,n_his_bins);
	      goto retminus1;
	    }
	  
	  
	  
	  
	  
	  /* Sum data, and histogram at offset hoffset */
	  if(e2a_flag)
	    {
	      if(subtract)
		{ /* e2a pulse pair mode Diff only */
		  if(small_bin_flag)
		    {
		      if(dd[17]) 
			printf("subtracting data %d from stored data (%d)  at hoffset=%d\n",
			       sdata, *(qs+hoffset), hoffset);
		      *(qs+hoffset) -= sdata;   /* DIFF... subtract this data */
		    }
		  else
		    {
		      if(dd[17]) 
			printf("subtracting data %d from stored data (%d)  at hoffset=%d\n",
			       sdata, *(qs+hoffset), hoffset);
		      *(ps+hoffset) -= sdata;   /* DIFF... subtract this data */
		      
		    }
		  scaler[index].sum_cycle += (double)sdata; /* sum this data */
		  discard = TRUE; /* data alread dealt with */
		}
	      else if(discard)
		if(dd[17])printf("discarded data at *gbl_bin=%d\n", *gbl_bin);
	    }
	  else /* end of 2a_flag */
	    discard = FALSE;
	  
	  if (!discard)
	    {
	      if(dd[17])
		{
		  if(h==0)
		    printf(" moving scaler data =%d (actual bin=%d) at to hoffset %d\n",
			   sdata,*gbl_bin, hoffset);
		}
	      /*  Now histogram the data
		  
		  Add current time bin content to cumulative scaler histo */
	      if(small_bin_flag)
		*(qs+hoffset) += sdata;
	      else
		*(ps+hoffset) += sdata;
	      /* increment local cycle sum scaler */
	      scaler[index].sum_cycle += (double)sdata;
	    }
	  
	  
	  /* e2e only.....
	     Now histogram data for post-depth bins also as 
	     pre-depth bins for the next ntuple (except for last one) */
	  if(e2e_flag && flag_twice)
	    {
	      flag_twice=0; 
	      if(ntuple +1 <=  fs.output.num_frequency_steps )
		{
		  sprintf(region, "is also Prebin word %d (%d)",im,im - gbl_nRFbins);
		  /*     start of ntuples + next ntuple +     offset in next ntuple */	      
		  hoffset= gbl_nprebins + pseqf[ntuple+1]*gbl_ntuple_width_h + im - gbl_nRFbins ; 
		  if(dd[17])printf("i=%d ub=%d  *gbl_bin=%d ch=%d ntuple=%d pseqf[%d]=%d %s",
				   i,ub, *gbl_bin,h,ntuple+1,ntuple+1,pseqf[ntuple+1],region);
		  
		  
		  /* check the offsets */
		  if(hoffset > n_his_bins)
		    {
		      if(!dd[17])printf("i=%d *gbl_bin=%d ch=%d ntuple=%d pseqf[%d]=%d %s",
					i,*gbl_bin,h,ntuple+1,ntuple+1,pseqf[ntuple+1],region);
		      printf("\nERROR histogram offset is out of range (%s): hoffset=%d  nhis=%d \n",
			     region,hoffset,n_his_bins);
		      
		      goto retminus1;
		    }
		  if(dd[17])printf(" moving data=%d to histo[%d]\n", sdata,hoffset);
		  
		  
		  /* add current time bin content to cumulative scaler histo */
		  if(small_bin_flag)
		    *(qs+hoffset) += sdata;
		  else
		    *(ps+hoffset) += sdata;
		  
		  /* do not sum this; it has already been summed above */
		}
	    } /* end of e2e flag_twice */
	  
#ifdef TWO_SCALERS
	  if(exp_mode == 1)
	    {
#ifdef THIRTYTWO
	      /* increment cycle sum according to User Bit */
	      if(index < 16)
		scaler[BACK_USB0 + ub].sum_cycle += sdata;
	      else if (index < 32)
		scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#else  // TWO scaler inputs
	      /* increment cycle sum according to User Bit */
	      if(index ==0)
		scaler[BACK_USB0 + ub].sum_cycle += sdata;
	      else if (index ==1)
		scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#endif // THIRTYTWO
	    }
#endif	  //  TWO_SCALERS
	  
	  
	  /* next incoming data */
	  pbuf++;  
	  
	  /* Increment time bin every h = n_scaler_real */
	  //printf("checking time bin increment... h=%d num_chans-1=%d \n",h, num_chans-1);
	  if (h == (num_chans-1) ) // last channel
	    {
	      if(exp_mode == 1)
		{
		  if(fill_uarray && scaler_offset == 0) /* true for Scaler A (2 scalers) or Scaler B (1 scaler)*/
		    {
		      *(pus + *gbl_bin) = ub;
		      if(dd[1]) 
			{
			  if(*gbl_bin < 2 ) 
			    printf ("update pus %p with ub %d\n",(pus + *gbl_bin),ub);
			}
		    }
		}
	      (*gbl_bin)++;   // actual (true) bin number
	      if(dd[1])
#ifdef HAVE_SIS3820
		printf("h=%d, true_ch=%d, first_ch=%d chan0_flag=%d  num_chans=%d incremented *gbl_bin to %d\n",
		       h,true_ch,first_ch, chan0_flag, num_chans, *gbl_bin);
	      
#else
	      printf("h=%d, num_chans=%d incremented *gbl_bin to %d\n",h, num_chans, *gbl_bin);
#endif
	    } /* end of last channel (h == max channel-1) */
	}/* end of h< num_channels */
      else
	{
	  printf("Oops - incorrect channel: Idx:%d nW:%d ch:%d  data:0x%8.8x  and *gbl_bin=%d\n",i, nwords, h, *pbuf, *gbl_bin);
	  goto retminus1;
	}
    } // loop on the number of words
  status = 0;
  
  if(dsis)
    printf("\nscaler_increment returns with *gbl_bin %d\n",*gbl_bin);
  dsis=dd[1]=0; // TEMP
  return 0; /* success */
  
 retminus1:
  stop_run();
  dsis=dd[1]=0; // TEMP
  return -1; 
}
