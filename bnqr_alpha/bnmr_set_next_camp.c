
/*----------------------------------------------------------------*/
INT set_next_camp_value(void)
/*----------------------------------------------------------------*/
{  
  /* Called from cycle_start() if camp_flag is true */

#ifdef CAMP_ACCESS
  INT status;
  float read_camp_val; /* CAMP value read back */


  /*  --------------------------------------------------------
      Mode 1j CAMP scan with flip  : flip helicity each cycle  
      -------------------------------------------------------- */
  if (mode1j_flag && flip)
    {  /* Mode 1j with helicity flip 
	  flip the helicity after each cycle; 
	  i.e. repeat cycle at the same sweep value for other helicity */
      gbl_hel_flipped =  flip_helicity(FALSE); /* flip the helicity  */   
      if(gbl_ppg_hel==HEL_UP)  /* only increment the sweep value on HEL_UP */
	{
	  if( gbl_FREQ_n ==  camp_ninc )  /* new scan */
	    {
	      if(dc)printf("\nset_next_camp_value: HEL UP 1j New scan detected, setting gbl_FREQ_n=0\n");
	      gbl_FREQ_n = 0;
	      gbl_SCAN_N ++;
	      if(gbl_CYCLE_N !=1)     /* except at beginning of run
					 for up/down scan, increment counter has to be fixed */ 
		{
		  if(dc)
		    printf("set_next_camp_value: calling update_fix_counter with camp_ninc=%d\n",camp_ninc);
		  update_fix_counter(camp_ninc); /* update fix_incr_cntr */
		}
	      else
		{
		  if(dc) 
		    printf("\nset_next_camp_value: HEL UP 1j Detected BOR, gbl_CYCLE_N=%d  flip=%d hel=%d , gbl_fix_inc_cntr=%d\n",
			   gbl_CYCLE_N,flip,gbl_ppg_hel,gbl_fix_inc_cntr);
		}
	      camp_inc *= -1.; /* reverse scan direction */
	    }  /* end of new scan for 1j+flip */
	  else  /* not a new scan */
	    {
	      set_camp_val = camp_start + ((gbl_FREQ_n - gbl_fix_inc_cntr) * camp_inc); /* calculate new value */ 
	      if(dc)		
		printf("set_next_camp_value: HEL UP 1j: incremented set_camp_val to = %f, gbl_FREQ_n=%d, gbl_fix_inc_cntr=%d, camp_inc=%f\n",
		       set_camp_val, gbl_FREQ_n, gbl_fix_inc_cntr, camp_inc );
	    } /* end of if old scan */
	  
	  
	  gbl_FREQ_n ++;  /* increment counter for next time */
	  if(!dd[11])
	  printf("\rCyc %d SCyc %d; hel %d  ; setting camp scan value to %f %s ",
		 gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel,set_camp_val, camp_params.units);
	  if(dc)
	    printf("set_next_camp_value: calling set_camp_value to set CAMP value to %f\n",set_camp_val);
	  status=set_camp_value(set_camp_val); /* set camp value */
	  if(status != CAMP_SUCCESS)
	    {
	      cm_msg(MERROR,"set_next_camp_value","Error attempting to set CAMP value of %f %s; stopping run (%d)",
		     set_camp_val,camp_params.units,status);
	      printf("\nset_next_camp_value: Error from CAMP set_camp_value (%d); disconnecting from CAMP \n",status);
	      write_client_alarm_message("Cannot set CAMP step value");
	      goto err;
	    }		
	} /* increment sweep value only on HEL_UP */
      else
	{ /* HEL DOWN... don't do anything
	     don't increment gbl_FREQ_n or sweep value, don't pass Go, don't collect $200 */
	  if(dc)printf("\n set_next_camp_value: HEL DOWN ... 1j continuing; gbl_FREQ_n=%d\n",gbl_FREQ_n);
	  if(!dd[11]) printf("\rCyc %d SCyc %d; hel %d ; camp scan value remains at %f %s ",
		 gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel, set_camp_val,camp_params.units);
	}
    }  /* end of Mode 1j + flip */
  else
    { /* regular CAMP scan including mode 1j without flip */
      /*   ---------------------------------------------------
	   CAMP SCAN
	   ---------------------------------------------------- */
      if (dc)printf("set_next_camp_value: detected camp scan\n");
      /* determine if this is a new scan */
      if(gbl_FREQ_n ==  camp_ninc) 
	{ /* N E W  S C A N   */		  		 
	  gbl_FREQ_n = 0;		 
	  gbl_SCAN_N ++;
	  if(flip) /* check if helicity flipping is enabled */
	    {
	      /* really flip the helicity ONLY when terminating a positive scan */
	      if(camp_inc > 0)
		  gbl_hel_flipped =  flip_helicity(FALSE);
		
	      if(!dd[8])printf("\n");
	      
	      if(gbl_CYCLE_N !=1) /* except at Begin of Run,
				     for up/down scan, increment counter has to be fixed */
		update_fix_counter(camp_ninc); /* update fix_incr_cntr */
	      else
		{
		  if(dc) 
		    printf("set_next_camp_value: detected BOR, gbl_CYCLE_N=%d  flip=%d gbl_fix_inc_cntr=%d\n",
			   gbl_CYCLE_N,flip,gbl_fix_inc_cntr);
		}
	      
	      camp_inc *= -1.;  /* reverse scan direction */
	      if(dc)
		printf("set_next_camp_value: set_next_camp_value and flip are true,  set_camp_val=%f, reversed camp_inc to %f, gbl_fix_inc_cntr=%d\n",
		       set_camp_val,camp_inc,gbl_fix_inc_cntr);
	    } /* end of flip */
	  else 
	    { /* flip is false */
	      set_camp_val = camp_start;  /* start a new scan */   
	      if(dc)printf("set_next_camp_value: set_next_camp_value is true, flip is false  set_camp_val=%f\n",set_camp_val);
	    }
	}	  /* end of new scan for CAMP */
      else    /* not a new CAMP scan */
	{
	  /* increment scan value;  calculate from start value to avoid rounding errors */
	  set_camp_val = camp_start + ( (gbl_FREQ_n - gbl_fix_inc_cntr) * camp_inc);
	}
      gbl_FREQ_n ++;
      
      /* Set CAMP value  */ 
      printf("\r set_next_camp_value %d:    hel %d;  Setting camp scan value to %f %s",
	     gbl_CYCLE_N,gbl_ppg_hel,set_camp_val, camp_params.units);
      if(dc)
	printf("set_next_camp_value: calling set_camp_value to set CAMP value to %f\n",set_camp_val);
      status = set_camp_value(set_camp_val);
      
      if(status != CAMP_SUCCESS)
	{
	  cm_msg(MERROR,"set_next_camp_value","Error attempting to set CAMP value of %f %s; stopping run  (%d)",
		 set_camp_val,camp_params.units,status);
	  printf("\nset_next_camp_value: Error from CAMP set_camp_value (%d); disconnecting from CAMP and stopping run\n",status);
	  write_client_alarm_message("Cannot set CAMP step value");
	  goto err;
	}
      
    } /* end of 1j/regular CAMP scan 
	 all camp scans continue here  */
  
  
  cyinfo.campdev_set = set_camp_val;
   
  /* now read camp back to fill cyinfo.campdev_read */
  if(dc) printf("set_next_camp_value: calling read_sweep_device to read CAMP value \n");
  read_camp_val=-999; /* initialize value to something */
  status = read_sweep_dev(&read_camp_val, camp_params);
  if(status != CAMP_SUCCESS)
    { /* read_sweep_dev sets client flag to stop the run */
      cm_msg(MERROR,"set_next_camp_value",
	     "Error from CAMP read_sweep_device (%d) stopping run ",status);
      printf("\nset_next_camp_value: Error from CAMP read_sweep_device  (%d) \n",
	     status);
      write_client_alarm_message("Cannot read CAMP value");
      goto err;
    }
  else
    {
      if(dc)printf("set_next_camp_value: Read back value from CAMP device = %f \n",read_camp_val);
      cyinfo.campdev_read = read_camp_val;
    }
  return SUCCESS;

 err:
      write_client_code(CAMP_ERR,SET,"frontend");
      stop_run();
      return  FE_ERR_HW ; /* error return.  */

#endif /* CAMP */
  return SUCCESS;
}
