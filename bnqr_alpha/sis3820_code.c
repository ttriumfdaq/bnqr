INT  init_sis3820(BOOL bor_flag)
{

#ifdef HAVE_SIS3820
  INT data_format;

#ifdef TWO_SCALERS
 DWORD test_pulse_mask_A=0; // used in SIS test mode
 DWORD enabled_channels_mask_A=0;
#endif
 DWORD test_pulse_mask_B =0; // used in SIS test mode
 DWORD enabled_channels_mask_B=0 ;
  BOOL enable_ref_ch1=0;
  unsigned int opbits,csrbits;
  DWORD lne_prescale_factor;
  DWORD LNE_prescale_register=0; // register contents
  DWORD dtmp;
  // DWORD dbp;
  DWORD sis_control_input_mode=0;
  DWORD sis_control_output_mode=0;
  DWORD id;
  char str[32];
  INT status;
  DWORD data_bits; // 8,16,24,32 bits
  INT enabled_channel_offset;
  INT num_enabled_channels;
  opbits=csrbits=0;

  if(dsis)
    printf("init_sis3820: starting with bor_flag = %d\n",bor_flag);

  // called by frontend_init and begin_of_run

  //     ... check we have the right module

#ifdef TWO_SCALERS
  status =  check_sis3820( gVme,SIS3820_BASE_A, &id );
  if(status != SUCCESS)
    return status;

  sprintf(str,"output/sis3820/modid_a");
  status = db_set_value(hDB, hFS, str, &id, sizeof(id), 1, TID_DWORD);  
  if(status !=SUCCESS)
    cm_msg(MERROR,"begin_of_run","cannot set ODB key  ...%s to 0x%x (%d)",str,id,status); 
 
#endif //  TWO_SCALERS

  status =  check_sis3820( gVme,SIS3820_BASE_B, &id);
  if(status != SUCCESS)
    return status;

  sprintf(str,"output/sis3820/modid_b");
  status = db_set_value(hDB, hFS, str, &id, sizeof(id), 1, TID_DWORD);  
  if(status !=SUCCESS)
    cm_msg(MERROR,"begin_of_run","cannot set ODB key  ...%s to 0x%x (%d)",str,id,status); 
      
 
  printf("\n init_sis3820: resetting  SIS3820\n");
  sis3820_Reset(gVme,SIS3820_BASE_B);
#ifdef TWO_SCALERS 
  sis3820_Reset(gVme,SIS3820_BASE_A);
#endif
  
  // do not bother setting up the module if called from frontend_init ( parameters may change)
  if(!bor_flag)
    return SUCCESS;

  data_bits = NUM_DATA_BITS; // default

  /* Special case - mode 2h (alpha with long histograms) using SIS3820 scaler (larger internal buffer)
     does not enable ref Ch1 or fluoride monitor 2, enables 4 alpha channels 
     For sis3820,  enabled_channel_offset refers to these REAL channels (use NIM channels 16-31)

     mode 1f with alpha enables all channels including Alpha, 2 Fl monitors 
  */
  enabled_channel_offset=0; // default; channels counting starts with the first channel
  if(fs.hardware.sis3820.sis_mode == 2) // REAL mode
    enable_ref_ch1=1; // Scaler B only  (default)

  if(gbl_alpha)
    {  //mode 2h
      // Using 24 bit data as need user bin.data_bits = 16;  // data_bits = 8;  testing only.
      // 24-bit data is fast enough; saved as 16-bits in the frontend scaler/histo arrays
      
      enabled_channel_offset = ALPHA_OFFSET; //  2 skipped (Ch0,16), so start at Ch 17 for SIS3820
      enable_ref_ch1 = 0; // disable Ref Ch1
    }
    


  printf("\ninit_sis3820: input parameters\n");
  printf("sismode           = %d\n",fs.hardware.sis3820.sis_mode);
  printf("num data bits     = %d\n",data_bits); // default is fixed at NUM_DATA_BITS=24 in febnmr.h for both modules
  printf("enabled chan offset = %d\n",enabled_channel_offset); // default is 0


  // Calculate the bit pattern for the number of enabled channels
#ifdef TWO_SCALERS
  enabled_channels_mask_A = get_enabled_channels_bitpat(MAX_CHAN_SIS38xxA, 0); // ref ch1 disabled on Scaler A
#endif
  
  if(gbl_alpha)
    { // PPG Mode 2h
      enabled_channels_mask_B = get_enabled_channels_bitpat_alpha((MAX_CHAN_SIS38xxB-ALPHA_OFFSET), ALPHA_OFFSET); // ref,Flu2 skipped on Scaler B
    }
  else if(alpha1f_flag)
    enabled_channels_mask_B = get_enabled_channels_bitpat(MAX_CHAN_SIS38xxB, enable_ref_ch1); // ref ch1 & ALPHA ch enabled on Scal
  else
    enabled_channels_mask_B = get_enabled_channels_bitpat((MAX_CHAN_SIS38xxB-NUM_ALPHA), enable_ref_ch1); // ref ch1 enabled on Scaler B

#ifdef TWO_SCALERS
  printf("Scaler A enabled chan mask = 0x%x\n",enabled_channels_mask_A);
  data_format = get_SIS_dataformat(NUM_DATA_BITS, NumSisChannels_A);  // checks NumSisChannels; NUM_DATA_BITS=24 in febnmr.h
#endif
  printf("Scaler B enabled chan mask = 0x%x\n",enabled_channels_mask_B);

  data_format = get_SIS_dataformat(data_bits, NumSisChannels_B); // get data_format according to number of data bits enabled
 
  
  if (fs.hardware.sis3820.sis_mode > 0)
    { // read control modes (only relevent for modes that use the PPG)
       
      if(fs.hardware.sis3820.input_mode > 6)
	{
	  cm_msg(MERROR,"init_sis_3820","illegal SIS input mode %d. Must be 0-6",
		 fs.hardware.sis3820.input_mode);
	  return DB_INVALID_PARAM;
	}
      if(fs.hardware.sis3820.output_mode > 3)
	{
	  cm_msg(MERROR,"init_sis_3820","illegal SIS output mode %d. Must be 0-3",
		 fs.hardware.sis3820.output_mode);
	  return DB_INVALID_PARAM;
	}
      
      //printf(" sis_control_input_mode = %d  sis_control_output_mode = %d\n",
      //sis_control_input_mode, sis_control_output_mode);
  
      sis_control_input_mode =   fs.hardware.sis3820.input_mode << 16;
      sis_control_output_mode =  fs.hardware.sis3820.output_mode << 20;
      printf("Using sis_control_input_mode = 0x%x  sis_control_output_mode = 0x%x\n",
	     sis_control_input_mode, sis_control_output_mode);

      lne_prescale_factor=LNE_PRESCALE_FACTOR; // must be 1 when using PPG. Defined in febnmr.h
    }
  else  // SIS test mode=0; no ppg 
    lne_prescale_factor = fs.sis_test_mode.sis3820.lne_prescale_factor; // prescale factor may vary
  
  LNE_prescale_register =  lne_prescale_factor -1; // value to be written to the register
  printf("init_sis3820: input parameters...\n");
  printf("sismode = %d number of bits for data word = %d (data format = %d)\n",fs.hardware.sis3820.sis_mode, data_bits, data_format);  
  printf("number of bins = LNE preset = %u  \n",n_bins); // global 
  printf("LNE prescale factor = %u  \n",lne_prescale_factor); // global constant
  

  if(fs.hardware.sis3820.sis_mode != 2 ) // uses test input pulses
    {
#ifdef TWO_SCALERS
      test_pulse_mask_A = fs.sis_test_mode.sis3820.module_a.test_pulse_mask; // 0 = no mask
#endif  
      test_pulse_mask_B =  fs.sis_test_mode.sis3820.module_b.test_pulse_mask; // 0 = no mask
    }

  NumSisChannels_B  = nchan_enabled(enabled_channels_mask_B, 1); // calculate number of enabled SIS channels for Module B
  num_enabled_channels =  MAX_CHAN_SIS38xxB; // expected number enabled

#ifndef TWO_SCALERS
  // BNQR only - added for alpha mode 
  if(gbl_alpha)
    { // alpha mode 2h 
      num_enabled_channels = num_enabled_channels - enabled_channel_offset;
      if(enable_ref_ch1) // not enabled in this mode (except for testing maybe)
	num_enabled_channels++; // add 1 for reference channel 
    }
  else
    {
      if(alpha1f_flag)
	num_enabled_channels =  MAX_CHAN_SIS38xxB ; // Alpha 1h mode; ref ch1 enabled
      else
	num_enabled_channels =  MAX_CHAN_SIS38xxB - NUM_ALPHA; // not alpha mode
      if( !enable_ref_ch1 )
	num_enabled_channels-- 	; // reduce number as max includes reference ch
    }
#endif // ONE scaler

  if (NumSisChannels_B != num_enabled_channels)
    {
      cm_msg(MERROR,"init_sis3820","Programming Error!!  number of enabled channels for Module B (%d) does not agree with required number %d",
	     NumSisChannels_B,num_enabled_channels);
      return DB_INVALID_PARAM;
    }

#ifdef TWO_SCALERS
  NumSisChannels_A  = nchan_enabled(enabled_channels_mask_A, 1); // calculate number of enabled SIS channels
  if (NumSisChannels_A != MAX_CHAN_SIS38xxA)
    {
      cm_msg(MERROR,"init_sis3820","Programming Error!!  number of enabled channels for Module A (%d) does not agree with required number %d",
	     NumSisChannels_A,MAX_CHAN_SIS38xxA);
      return DB_INVALID_PARAM;
    }
#endif


  // check validity of input params (not at frontend_init) 
  
  if (fs.hardware.sis3820.sis_mode == 0)  // SIS TEST mode; no PPG
    { 
      dtmp = 10000000/lne_prescale_factor; // calculate actual frequency (10MHz / prescale factor)
      printf("SIS mode %d: test mode, internal LNE, prescale factor %d LNE frequency=%u Hz\n", 
	    fs.hardware.sis3820.sis_mode ,lne_prescale_factor,dtmp); // 0x18 prescale LNE pulse rate
      printf("Writing %d (0x%x) to LNE Prescale Register\n",LNE_prescale_register,LNE_prescale_register);
#ifdef TWO_SCALERS
      sis3820_RegisterWrite(gVme,SIS3820_BASE_A,SIS3820_LNE_PRESCALE,LNE_prescale_register); // 0x18  prescale LNE pulse rate
#endif
      sis3820_RegisterWrite(gVme,SIS3820_BASE_B,SIS3820_LNE_PRESCALE,LNE_prescale_register); // 0x18  prescale LNE pulse rate

      opbits =
	SIS3820_CLEARING_MODE|                           //   bit 0    set clearing mode
	//SIS3820_MCS_DATA_FORMAT_8BIT|                  //   bits 2,3 8 bit data 
	data_format |                                    //   input param
	SIS3820_LNE_SOURCE_INTERNAL_10MHZ|               //   bit 4-6  LNE source 10MHz pulser
	SIS3820_FIFO_MODE|                               //   bit 12,13 FIFO mode
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|            //   bit 28-31  MCS mode
	SIS3820_CONTROL_INPUT_MODE1|                     //   bit 16-18 Input mode 1
	SIS3820_CONTROL_OUTPUT_MODE2 ;                   //   bit 20,21 Output mode 2
      
	  
      printf("Writing 0x%x to Operation Mode Register\n",opbits);
#ifdef TWO_SCALERS
      sis3820_RegisterWrite(gVme,SIS3820_BASE_A,SIS3820_OPERATION_MODE, opbits); // 0x100 
#endif
      sis3820_RegisterWrite(gVme,SIS3820_BASE_B,SIS3820_OPERATION_MODE, opbits); // 0x100 
	  
    }
      
      
  else if (fs.hardware.sis3820.sis_mode == 1)  // SIS REAL LNE  with test pulses (Max LNE pulse rate 5MHz unless using prescale)
    {     // "MCS Next" signal from PPG must be plugged into input 1 of SIS scaler (LNE)

      printf("SIS mode %d: test mode with external LNE\n",fs.hardware.sis3820.sis_mode);
      printf("Writing %d (0x%x) to LNE Prescale Register\n",LNE_prescale_register,LNE_prescale_register);
#ifdef TWO_SCALERS
      sis3820_RegisterWrite(gVme,SIS3820_BASE_A,SIS3820_LNE_PRESCALE,LNE_prescale_register); // 0x18  prescale LNE pulse rate reg
#endif
      sis3820_RegisterWrite(gVme,SIS3820_BASE_B,SIS3820_LNE_PRESCALE,LNE_prescale_register); // 0x18  prescale LNE pulse rate reg 
      opbits =     
	SIS3820_CLEARING_MODE|                    //   bit 0    set clearing mode
	//SIS3820_MCS_DATA_FORMAT_32BIT|            //   bits 2,3 32 bit data 
	data_format | 
	SIS3820_LNE_SOURCE_CONTROL_SIGNAL|        //   bit 4-6  external LNE signal 
	SIS3820_FIFO_MODE|                        //   bit 12,13 FIFO mode
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|     //   bit 28-31  MCS mode
	sis_control_input_mode |                  //   bit 16-18 Input mode 1
	sis_control_output_mode      ;            //   bit 20,21 Output mode 2
      
      printf("Writing 0x%x to Operation Mode Register\n",opbits);
#ifdef TWO_SCALERS
      sis3820_RegisterWrite(gVme,SIS3820_BASE_A,SIS3820_OPERATION_MODE, opbits); // 0x100 
#endif      
      sis3820_RegisterWrite(gVme,SIS3820_BASE_B,SIS3820_OPERATION_MODE, opbits); // 0x100  
      
    }
  else if (fs.hardware.sis3820.sis_mode == 2)  // SIS REAL LNE  with REAL input  pulses
    {     // MCS Next of PPG must be plugged into input 1 of SIS scaler (LNE) and input signals should be plugged into
          // the enabled scaler channels
      printf("SIS mode %d: external LNE\n",fs.hardware.sis3820.sis_mode);
      printf("Writing %d (0x%x) to LNE Prescale Register\n",LNE_prescale_register,LNE_prescale_register);
#ifdef TWO_SCALERS
      sis3820_RegisterWrite(gVme,SIS3820_BASE_A,SIS3820_LNE_PRESCALE,LNE_prescale_register); // 0x18  prescale LNE pulse rate reg
#endif
      sis3820_RegisterWrite(gVme,SIS3820_BASE_B,SIS3820_LNE_PRESCALE,LNE_prescale_register); // 0x18  prescale LNE pulse rate reg  
      opbits = SIS3820_CLEARING_MODE|                    //   bit 0    set clearing mode
	//SIS3820_MCS_DATA_FORMAT_32BIT|            //   bits 2,3 32 bit data 
	data_format | 
	SIS3820_LNE_SOURCE_CONTROL_SIGNAL|        //   bit 4-6  external LNE signal 
        SIS3820_ARM_ENABLE_CONTROL_SIGNAL|        //   LNE as enable
	SIS3820_FIFO_MODE|                        //   bit 12,13 FIFO mode
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|     //   bit 28-31  MCS mode
	sis_control_input_mode |                  //   bit 16-18 Input mode 1
	sis_control_output_mode     ;             //   bit 20,21 Output mode 1
      
      
      printf("Writing 0x%x to Operation Mode Register\n",opbits);
#ifdef TWO_SCALERS
      sis3820_RegisterWrite(gVme,SIS3820_BASE_A,SIS3820_OPERATION_MODE, opbits); // 0x100 
#endif
      sis3820_RegisterWrite(gVme,SIS3820_BASE_B,SIS3820_OPERATION_MODE, opbits); // 0x100 
    
    }
  else 
    {
      printf ("init_sis3820:  unknown SIS mode %d\n",fs.hardware.sis3820.sis_mode);
      cm_msg(MERROR,"init_sis3820","Unknown SIS mode");
      return DB_INVALID_PARAM;
    }
  
  if(fs.hardware.sis3820.sis_mode != 2)   // sis modes with test pulses as input
    {             
      // Enable test pulses
      csrbits =  CTRL_COUNTER_TEST_25MHZ_ENABLE |  CTRL_COUNTER_TEST_MODE_ENABLE;
#ifdef TWO_SCALERS
      printf("Scaler A:\n");

      printf("Enabling test pulses\n");
      sis3820_RegisterWrite(gVme,SIS3820_BASE_A , SIS3820_CONTROL_STATUS, csrbits );

      printf("Writing 0x%x (test pulse mask) to  SIS3820_TEST_PULSE_MASK (0x%x)\n",
	     test_pulse_mask_A, SIS3820_TEST_PULSE_MASK );

      sis3820_RegisterWrite(gVme,SIS3820_BASE_A , SIS3820_TEST_PULSE_MASK, test_pulse_mask_A); // mask ch(s) from test pulses
      test_pulse_mask_A = test_pulse_mask_A & ~enabled_channels_mask_A; // masks only enabled channels 
      nchan_enabled(test_pulse_mask_A, 0); // displays masked test pulse channels
      printf("Scaler B:\n");
#endif

      printf("Enabling test pulses\n");
      sis3820_RegisterWrite(gVme,SIS3820_BASE_B , SIS3820_CONTROL_STATUS, csrbits );
	  
      printf("Writing 0x%x (test pulse mask) to  SIS3820_TEST_PULSE_MASK (0x%x)\n",
	     test_pulse_mask_B, SIS3820_TEST_PULSE_MASK );

      sis3820_RegisterWrite(gVme,SIS3820_BASE_B , SIS3820_TEST_PULSE_MASK, test_pulse_mask_B); // mask ch(s) from test pulses
      test_pulse_mask_B = test_pulse_mask_B & ~enabled_channels_mask_B; // masks only enabled channels 
      nchan_enabled(test_pulse_mask_B, 0); // displays masked test pulse channels
    }
  else
    {
      // SIS Mode 2  REAL Mode ; Scaler A
#ifdef TWO_SCALERS
      printf("Scaler A:\n");  // disable all test pulses
      csrbits = CTRL_REFERENCE_CH1_DISABLE | CTRL_COUNTER_TEST_25MHZ_DISABLE |  CTRL_COUNTER_TEST_MODE_DISABLE ;
      sis3820_RegisterWrite(gVme,SIS3820_BASE_A , SIS3820_CONTROL_STATUS, csrbits );
#endif
      printf("Scaler B:\n");  // enable Ch 1 test pulses if enable_ref_ch1 is true
      if(enable_ref_ch1 )
	csrbits = CTRL_REFERENCE_CH1_ENABLE | CTRL_COUNTER_TEST_25MHZ_DISABLE |  CTRL_COUNTER_TEST_MODE_DISABLE ;
      else
	csrbits = CTRL_REFERENCE_CH1_DISABLE | CTRL_COUNTER_TEST_25MHZ_DISABLE |  CTRL_COUNTER_TEST_MODE_DISABLE ;
       sis3820_RegisterWrite(gVme,SIS3820_BASE_B , SIS3820_CONTROL_STATUS, csrbits );
    }
  
  // All SIS modes
#ifdef TWO_SCALERS
  printf("Scaler A:\n");
  sis3820_Status(gVme,SIS3820_BASE_A);
  printf("\nScaler B:\n");
#endif
  sis3820_Status(gVme,SIS3820_BASE_B);  

#ifdef TWO_SCALERS
  printf("Writing 0x%x (enabled channels mask) to  SIS3820_COPY_DISABLE (0x%x) for Module A\n",
	 enabled_channels_mask_A, SIS3820_COPY_DISABLE );
  sis3820_RegisterWrite(gVme,SIS3820_BASE_A , SIS3820_COPY_DISABLE, enabled_channels_mask_A); 
  
  printf("\n ***  Writing %d (number of bins) to  Scaler A SIS3820_ACQUISITION_PRESET register (0x%x)\n *** \n", 
	 n_bins,SIS3820_ACQUISITION_PRESET);
 
  cm_msg(MINFO,"init_sis3820","wrote number of bins %d to  SIS3820_ACQUISITION_PRESET register (Scalar A)",n_bins);

  sis3820_RegisterWrite(gVme,SIS3820_BASE_A , SIS3820_ACQUISITION_PRESET, n_bins); // select number of LNE     
#endif

  printf("Writing 0x%x (enabled channels mask) to  SIS3820_COPY_DISABLE (0x%x) for Module B\n",
	 enabled_channels_mask_B, SIS3820_COPY_DISABLE );
  sis3820_RegisterWrite(gVme,SIS3820_BASE_B , SIS3820_COPY_DISABLE, enabled_channels_mask_B); 


   printf("\n ***  Writing %d (number of bins) to  SIS3820_ACQUISITION_PRESET register (0x%x)\n *** \n", 
  	 n_bins,SIS3820_ACQUISITION_PRESET);

  cm_msg(MINFO,"init_sis3820","wrote number of bins %d to  Scaler B SIS3820_ACQUISITION_PRESET register (Scalar A)",n_bins);
 
  sis3820_RegisterWrite(gVme,SIS3820_BASE_B , SIS3820_ACQUISITION_PRESET, n_bins ); // select number of LNE   
  cm_msg(MINFO,"init_sis3820","wrote number of bins %d to  SIS3820_ACQUISITION_PRESET register (Scalar B)",n_bins);


  
  printf("\n");
#ifdef TWO_SCALERS
  printf("Scaler A:\n");
  SIS_readback(SIS3820_BASE_A, opbits,csrbits);

  printf("Scaler B:\n");
#endif
  SIS_readback(SIS3820_BASE_B, opbits,csrbits);

#else // HAVE_SIS3820
  printf("init_sis3820:  HAVE_SIS3820 is not defined\n");
#endif
  return SUCCESS;
}


INT get_SIS_dataformat(INT nbits, INT NumSisChannels )
{
  INT data_format;

  printf("get_SIS_dataformatData: nbits = %d bits and Num Enabled Channels=%d \n",nbits,NumSisChannels);

  switch(nbits)
    {
    case 8:
      data_format = SIS3820_MCS_DATA_FORMAT_8BIT;
      data_bytes = 1; // number of bytes for each data word (global)
      cm_msg(MINFO,"get_SIS_dataformat","Groups of 4 channels will be copied for 8-bit format");
      printf("get_SIS_dataformat: Groups of 4 adjacent channels will be copied for 8-bit data format\n");
      break;

    case 16:
      data_format = SIS3820_MCS_DATA_FORMAT_16BIT;
      data_bytes = 2;  // number of bytes for each data word
      cm_msg(MINFO,"get_SIS_dataformat","Pairs channels will be copied for 16-bit format"); 
      printf("get_SIS_dataformat: Pairs of adjacent channels will be copied for 16-bit data format\n");
      break;
    case 24:
      data_format = SIS3820_MCS_DATA_FORMAT_24BIT;
      data_bytes = 3;  // number of bytes for each data word (actually 4 with userbit, ch)
     printf("get_SIS_dataformat: Individual enabled channels will be copied for 24-bit data format\n");
      break;
   case 32:
      data_format = SIS3820_MCS_DATA_FORMAT_32BIT;
      data_bytes = 4;  // number of bytes for each data word
      printf("get_SIS_dataformat: Individual enabled channels will be copied for 32-bit data format\n");
      break; 
    default:
      data_format = SIS3820_MCS_DATA_FORMAT_32BIT;
      data_bytes = 4;  // number of bytes for each data word
      printf("Unknown SIS data format (%d bits). Setting default of 32 bits\n",nbits);
      cm_msg(MINFO,"get_SIS_dataformat","Unknown SIS data format (%d bits). Setting default of 32 bits",nbits);
 
    }  
  return data_format;
}





void SIS_readback(unsigned int sis3820_base, unsigned int wrote_mode, unsigned int wrote_csr)
{
 static unsigned int my_data = 0;

 printf( "\n=== SIS_readback:  (sismode=%d) ===\n",fs.hardware.sis3820.sis_mode);

 my_data = sis3820_RegisterRead(gVme,sis3820_base ,SIS3820_OPERATION_MODE);
 if(wrote_mode > 0)
   printf("Wrote 0x%x to OP mode reg; read back 0x%x\n",wrote_mode, my_data);
 else
   printf("Read 0x%x from OP mode reg\n",my_data);

 opmode_bits(my_data);


 my_data = sis3820_RegisterRead(gVme,sis3820_base , SIS3820_CONTROL_STATUS);
 if(wrote_csr > 0)
   printf("\nWrote 0x%x to CSR; read back 0x%x\n",wrote_csr, my_data);
 else
  printf("\nRead 0x%x from CSR\n",my_data);
 
 csr_bits(my_data);


 printf("\nCntrl/Status reg 0x%x  =  0x%x\n", SIS3820_CONTROL_STATUS, sis3820_RegisterRead(gVme, sis3820_base,  SIS3820_CONTROL_STATUS) );
 printf("\nOperation reg 0x%x  =  0x%x\n", SIS3820_OPERATION_MODE, sis3820_RegisterRead (gVme, sis3820_base,  SIS3820_OPERATION_MODE) );
 printf("\nCopy/Disable reg 0x%x  =  0x%x\n", SIS3820_COPY_DISABLE, sis3820_RegisterRead (gVme, sis3820_base,  SIS3820_COPY_DISABLE) );
 printf("\nTest Pulse Mask reg 0x%x  =  0x%x\n", SIS3820_TEST_PULSE_MASK, sis3820_RegisterRead (gVme, sis3820_base,  SIS3820_TEST_PULSE_MASK) );
 printf("\nPreset reg 0x%x  =  0x%x \n", SIS3820_ACQUISITION_PRESET, sis3820_RegisterRead (gVme, sis3820_base,  SIS3820_ACQUISITION_PRESET) );
 printf("\nPrescale reg 0x%x  =  0x%x\n", SIS3820_LNE_PRESCALE, sis3820_RegisterRead (gVme, sis3820_base, SIS3820_LNE_PRESCALE ) );

 return;
}

void opmode_bits(unsigned int data)
{
  unsigned int tmp;

  printf("\nSIS Operation Mode bits register 0x%x : 0x%x\n",SIS3820_OPERATION_MODE,data);
  if(data & SIS3820_NON_CLEARING_MODE)
    printf("Clearing mode:              N ");
  else
    printf("Clearing mode:              Y ");

  // data format
  tmp = (data >> 2) & 0x3;
  printf("  Data format:  ");
  if (tmp == 0) 
    printf(" 32-bit Mode ");
  else if (tmp == 1)
    printf(" 24-bit Mode ");	
  else if (tmp == 2)
    printf( "16-bit Mode ");	
  else if (tmp == 3)
    printf("  8-bit Mode ");	
  else 
    printf("       error ");


  // LNE source
  tmp = (data  >> 4) & 7 ;
  printf("  LNE source: "); 
  if (tmp == 0) 
    printf("       Internal ");
  else if (tmp == 1)
    printf("    Front Panel ");
  else if (tmp == 2)
    printf("10MHz testpulse ");
  else if (tmp == 3)
    printf("      Channel N ");
  else if (tmp == 4)
    printf("Preset Scaler N ");
  else 
   printf("           error ");

  printf("\n");

  // ARM/Enable source
  tmp = (data  >> 8) & 3 ;
  printf("Arm/Enable source: ");
  if (tmp == 0) 
    printf(" LNE input ");
  else if (tmp == 1) 
    printf(" Channel N ");
  else
    printf("  Reserved ");

  // SDRAM
  tmp = (data  >> 12) & 3 ;
  printf("  SDRAM add:    ");
  if (tmp == 0) 
    printf("       FIFO ");
  else
    printf("      SDRAM ");

  // HISCAL start
  tmp = (data  >> 14) & 1 ;
  printf("  HISCAL Start: ");
  if (tmp == 0) 
    printf("     internal "); // VME
  else
    printf("     external "); // signal

     printf("\n");
   

  // Control Input mode
  tmp = (data  >> 16) & 7 ;
  printf("Control Input Mode:         %d ",tmp);
  
  // Control Input inverted
  tmp = (data  >> 19) & 1 ;
   printf("  Inverted:               ");
  if(tmp)
    printf("Y ");
  else
    printf("N ");
 
  printf("\n");
 
  // Control Output mode
  tmp = (data  >> 20) & 3 ;
  printf("Control Output Mode:        %d ",tmp);
  
  
  // Control Output inverted
  tmp = (data  >> 23) & 1 ;
  printf("  Inverted:               ");
  if(tmp)
    printf("Y ");
  else
    printf("N ");
  
    printf("\n");
  // Operation mode
  tmp = (data  >> 28) & 7 ;
  printf("Operation Mode:      ");
  if(tmp ==0)
    printf("Scaler Counter/Latching/Preset ");
  else if (tmp ==2)
    printf("MultiChannel Scaler (MCS)      ");
  else if (tmp ==7)
    printf("SDRAM/FIF) VME write test mode ");
  else
    printf("Reserved                       ");

      printf("\n");

}


void csr_bits(unsigned int data)
{
  unsigned int tmp;

  printf("SIS Control Status register 0x%x : 0x%x\n",SIS3820_CONTROL_STATUS,data);

  printf("User LED:               ");
  if(data & 1)
    printf("On  ");
  else
    printf("Off ");

  // 25MHz test pulse
  tmp = (data  >> 4 ) & 1 ;
  printf("  25MHz Test Pulse: ");
  if(tmp & 1)
    printf("On  ");
  else
    printf("Off ");


  // test mode enable
  tmp = (data  >> 5 ) & 1 ;
  printf("  Counter Test Mode: ");
  if(tmp & 1)
    printf("On  ");
  else
    printf("Off ");

  printf("\n");
  // reference pulse
  tmp = (data  >> 6 ) & 1 ;
  printf("Reference Pulser:       ");
  if(tmp & 1)
    printf("On  ");
  else
    printf("Off ");

  // scaler enable
  tmp = (data  >> 16 ) & 1 ;
  printf("Scaler Enable Active:   ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  // MCS enable
  tmp = (data  >> 18 ) & 1 ;
  printf("MCS Enable Active:      ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");
  printf("\n");
  // SDRAM/FIFO test enable
  tmp = (data  >> 23 ) & 1 ;
  printf("SDRAM/FIFO Test Enable:   ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  // armed
  tmp = (data  >> 24 ) & 1 ;
  printf("  Armed:              ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  // HISCAL
  tmp = (data  >> 25 ) & 1 ;
  printf("  HISCAL operation:    ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  printf("\n");
  // Overflow
  tmp = (data  >> 27 ) & 1 ;
  printf("Overflow:                 ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  // Status external input bits
  tmp = (data  >> 28 ) & 3 ;
  printf("  Status Input bits:  %d",tmp);
  // Status external latch bits
  tmp = (data  >> 30 ) & 3 ;
  printf("   Status Latch bits:   %d",tmp);
  printf("\n");
  return;
}


int nchan_enabled(unsigned int bp, int en )
{
  /* Input params
     bp  bitpattern   enabled channel mask or test pulse masked channels
     en  enabled/disabled   TRUE for enabled channel mask or FALSE for test pulse mask 

   Determine which and how many SIS3820 channels are enabled
   or which channels are masked from the test pulse

   NOTE: channels are labelled 1-32  rather than 0-31
         This routine uses 0-31 except for output for the user

  
  */
  int i,n,j;
  int chan[32];
  int num,cntr;
  //  char *order[]={"first","second","third"}; 

  for (i=0; i<32; i++)
    chan[i]=1;

  n=0;
  if (bp & 1)
    {
      chan[0]=0; // ch 0 disabled  or ch 0 not masked
      n++;
    }
  for (j=0; j<31; j++)
    {
      bp = bp >> 1;
      if (bp & 1)
	{
	  n++;
	  chan[j+1]=0;
	}
    }

  if(en)
    {
      num = 32-n;
      printf ("nchan_enabled: num channels enabled=%d\n",num);
      printf ("   Enabled channels are (channels are numbered from 1 to 32 ): ");
    }
  else
    {
      num = n;
      printf ("nchan_enabled: num enabled channels masked from test pulses =%d\n",num);
      printf ("   Masked channels are (channels are numbered from 1 to 32 ): ");
    }

  cntr=0;
  for (j=0; j<32; j++)
    {
      if (en && chan[j] > 0)  // check enabled channels
	{
	  cntr++;
	  printf("%d ",j+1);
	 
	}
      else if (!en && chan[j] == 0) // check masked channels
	printf("%d ",j+1);
    }
  printf("\n");
 
  return num;
}


int check_sis3820(MVME_INTERFACE *mvme, DWORD base, DWORD *id)
{
  // check correct SIS module is present (modid 3820)

  static unsigned int my_data = 0;
  int modid;

  my_data = sis3820_RegisterRead(gVme, base , SIS3820_MODID);
  *id=(DWORD)my_data;

  printf("check_sis3820:   ModuleID and Firmware: 0x%x\n",my_data);
  modid = (int) (my_data & 0xFFFF0000) >> 16;
  // printf("ModuleID 0x%x \n",modid);

  if(modid != 0x3820)
    {
      printf("check_sis3820:  Wrong VME module (Modid=0x%x)  is installed at Base Address 0x%x . Expecting a SIS3820 \n",modid,SIS3820_BASE_B);
      cm_msg(MERROR, "check_sis3820","Wrong VME module  (Modid=0x%x) is installed at Base Address 0x%x . Expecting a SIS3820",modid,SIS3820_BASE_B);
      return FE_ERR_HW;
    }

  return SUCCESS;
}


DWORD get_enabled_channels_bitpat(int num_ch_enabled,  BOOL ch1ref)
{
  /*
     SIS3820 has channels ECL 0-15 and NIM 16-31
     If > 16 inputs are used,
         enabled channels are assumed to start at Ch 0
                                   and end with   Ch num_ch_enabled-1

     If less than 16 inputs used, uses NIM channels 17 and up
     If ch1ref is true, also enables ch1 (reference pulser) 

     NOTE: Getting the bitpattern of enabled channels, but
           actually returning a MASK; 0=enable channel 1=disable
           mask = 0x0 means all channels are enabled
  */
  int i;
  
  DWORD bitpattern=0;
  int last_enabled_ch; 
  int bit;
  int istart,iend;
  
  int first_enabled_ch;

   if (num_ch_enabled >= 32)
    {  // all enabled
      bitpattern=0; // all channels enabled; mask is 0
      printf("get_enabled_channels_bitpat: all channels enabled; bitpattern is 0x%x\n",bitpattern);
      return bitpattern;
    }
   else if (num_ch_enabled == 0)
     return 0xFFFFFFFF; // none enabled

   if (num_ch_enabled > 16)
     {
       first_enabled_ch=0;   // default is Channel 0;  use ECL and NIM inputs
       last_enabled_ch = first_enabled_ch +  num_ch_enabled-1;
       istart = first_enabled_ch; // bit
       iend   = last_enabled_ch ; // i<this 
       for (i=istart; i<iend; i++)
	 {
	   bit = 1 << i;
	   bitpattern = bitpattern | bit;	
	 }
       printf("get_enabled_channels_bitpat: bitpattern of enabled channels is 0x%x \n",bitpattern);
       bitpattern = ~bitpattern; 
       printf("      returning bitpattern of disabled channels 0x%x (i.e. mask) \n",bitpattern);
       return bitpattern;
     }


   // Using the NIM inputs only for real inputs.
   // ECL_OFFSET = 15
   first_enabled_ch=ECL_OFFSET+1; // First enabled channel=16. SIS3820 Module has ECL inputs for ch0-15
   if (ch1ref)
     {
       bitpattern=1; //  enable channel 0 (real signals start in Channel 16 for SIS3801
       num_ch_enabled --; // reduce total channels because of channel 0	
     }
   last_enabled_ch = first_enabled_ch +  num_ch_enabled-1;
  
   if(ch1ref)
     {
       printf("get_enabled_channels_bitpat:   Ch 0 will be used for reference counter\n");
       printf("     Ch 0 enabled, plus channels starting at %d and ending with %d. Total number of channels enabled: %d\n",
	      first_enabled_ch,  last_enabled_ch,  (num_ch_enabled+1));
     }
   else
     printf("get_enabled_channels_bitpat: First channel enabled: %d  Last channel enabled: %d  Number of channels enabled: %d\n",
	    first_enabled_ch,  last_enabled_ch,  num_ch_enabled);
   istart = first_enabled_ch; // bit
   iend   = last_enabled_ch+1 ; // i<this 
   for (i=istart; i<iend; i++)
     {
       bit = 1 << i;
       bitpattern = bitpattern | bit;	
     }
   printf("get_enabled_channels_bitpat: bitpattern of enabled channels is 0x%x \n",bitpattern);
   bitpattern = ~bitpattern; 
   printf("      returning bitpattern of disabled channels 0x%x (i.e. mask) \n",bitpattern);
   return bitpattern;
}
 

DWORD get_enabled_channels_bitpat_alpha(int nch, int alpha_offset)
{
  /* 
      Alpha mode only; enable less that the maximum number of channels to save space/time
                       the enabled channels are offset
      inputs nch: number of channels to actually enable = (max - offset)
            alpha_offset: offset to first enabled channel (skip ref and fl monitor);
           
       If > 16 channels in the SIS3820
           normally ch0 to ch nch-1 are enabled
           in alpha mode, ch 2 to ch nch-1 will be enabled
       If < 16 channels are enabled,
           normally ch0, ch16 to ch nch-2 will be enabled
           in alpha mode ch 16+offset to 16+nch-1 will be enabled
            

     SIS3820 complicated because use upper NIM inputs (16-31) rather than ECL (0-15) if < 16 inputs enabled
             Also reference ch is still Ch 0 (ECL).

  
     NOTE: Getting the bitpattern of enabled channels, but
           actually returning a MASK; 0=enable channel 1=disable
           mask = 0x0 means all channels are enabled
  */
  int i;
  
  DWORD bitpattern=0;
  int last_enabled_ch; 
  int bit;
  int istart,iend;
  
  int first_enabled_ch;

  if(alpha_offset == 0)
    {
      printf(" get_enabled_channels_bitpat_alpha: alpha_offset is zero, use function get_enabled_channels_bitpat\n");
      return 0xFFFFFFFF;
    }
 



   if (nch + alpha_offset > 32)
     {
       printf(" get_enabled_channels_bitpat_alpha: too many channels are enabled nch=%d alpha_offset=%d\n",nch,alpha_offset);
       return 0xFFFFFFFF;
     }

   if (nch > 16)
     first_enabled_ch=0 + alpha_offset;   // Channel 0;  use ECL and NIM inputs
   else
     {
       // ECL_OFFSET = 15     ; first skipped channel is channel 0 (ref); 2nd skipped is Fl mon on Ch 16
       first_enabled_ch=ECL_OFFSET +alpha_offset  ; // First NIM enabled channel=16 SIS3820 Module has ECL inputs for ch0-15
     }
 
     last_enabled_ch = first_enabled_ch +  nch-1;


     printf("get_enabled_channels_bitpat: First chan enabled: %d  Last chan enabled: %d  Number channels enabled: %d\n",
	   first_enabled_ch,  last_enabled_ch,  nch);


  istart = first_enabled_ch; // bit
  iend   = last_enabled_ch+1 ; // i<this 
  for (i=istart; i<iend; i++)
    {
      bit = 1 << i;
      bitpattern = bitpattern | bit;	
    }
  printf("get_enabled_channels_bitpat: bitpattern of enabled channels is 0x%x \n",bitpattern);
  bitpattern = ~bitpattern; 
  printf("      returning bitpattern of disabled channels 0x%x (i.e. mask) \n",bitpattern);
  return bitpattern;
}
 

