#!/bin/csh
# Uses VMIC_HOST
$EXPERIM_DIR/bin/check-host-$BEAMLINE
if ( "$?" != "0" ) then
    echo "Failure from check-host-$HOST";
    exit;
endif

foreach f ( `ls *.odb` )
  echo "\n\n\n\n ========  Working on file $f ============\n"
  check_file.pl $f
 if ( "$?" == "0" ) then
    echo "check_all: success from file $f  "
 else
    echo "check_all: failure from file $f  "
    exit
 endif
end
echo "\n All *.odb files have been checked successfully"
exit

# check_file.pl 20_defaults.odb
# check_file.pl 2a_defaults.odb
# check_file.pl 2b_defaults.odb
# check_file.pl 2c_defaults.odb
# check_file.pl 2g_defaults.odb
# check_file.pl 2h_defaults.odb
# check_file.pl 1a_defaults.odb
# check_file.pl 1b_defaults.odb
# check_file.pl 1c_defaults.odb
# check_file.pl 1f_defaults.odb
# check_file.pl 1j_defaults.odb
# check_file.pl 1g_defaults.odb
# check_file.pl 1h_defaults.odb
# check_file.pl 1k_defaults.odb
# check_file.pl 1n_defaults.odb
