#!/bin/csh 
#   (-x for debug)
echo "argv: $argv ; number of args: $#argv" 
if ($#argv < 1) then
  echo "supply filename" 
  exit
endif

set file = $argv[1];

# Don't run this on VMIC
$EXPERIM_DIR/bin/check-host-$BEAMLINE
if ( "$?" != "0" ) then
    echo "Failure from check-host-$HOST";
    exit;
endif

check_file.pl $file
echo "return code is $?\n";
exit
end
