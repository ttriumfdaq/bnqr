#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
# Check that the keys in the default load files exist
# 
# Run the check like this:
#     check_file.pl <expt>  <filename>
#     e.g. check_file.pl bnmr 1e_defaults.odb
#
#  see AAA_README in this directory for more information
#
# $Log: check_file.pl,v $
# Revision 1.2  2014/01/09 23:48:01  suz
# check for ONLINE
#
# Revision 1.1  2013/01/21 21:14:22  suz
# initial version for VMIC
#
# Revision 1.1  2005/09/27 19:40:31  suz
# original - checks load files for mode defaults
#
#
#
my $name="check_file";
my @cmd_array;

our $expt=$ENV{'MIDAS_EXPT_NAME'};
our $online=$ENV{'ONLINE'};
our ($file ) = @ARGV;
my $cmd;

unless ($expt)
{
    print "FAILURE: experiment not supplied in environment variable MIDAS_EXPT_NAME\n"; 
    die;
}

unless ($online)
{
    die "FAILURE: ONLINE not supplied  in environment variable\n"; 
}
unless (-e $file)
{
    die "FAILURE: non-existent file \"$file\"\n"; 
}

print "Experiment is $expt\n";
print "ONLINE is $online\n";


$cmd="/home/$expt/$online/perl/mode_check.pl /home/$expt/$online/perl $expt /home/$expt/$online/modefiles/$file $expt";

print "$name cmd=\"$cmd\"\n";
unless (open (MFC,"$cmd |"))
{
 print "$name: cannot open filehandle MFC for command: $cmd ($!)\n";
 die "$name: cannot execute $cmd";
}

my $flag=0;
while(<MFC>)
{      # get the output from the command
    print $_;
    unless ($flag)
    {
	if  (/\*\*\*\*\*/)
	{ 
	  $flag=1;
	}
    }
}
unless($flag)
    { print "\n check_file: success\n";}
else
    { print "\n check_file: detected failure\n";}

exit $flag;
