#!/usr/bin/perl
# above is magic first line to invoke perl
# or for debug -d and warnings -w
###  !/usr/bin/perl -d
 
# invoke this script with cmd
#                 include             experiment  disable auto run  beamline ppg
#                  path                           numbering flag             mode
# real.pl  /home/bnmr/online/mdarc/perl   bnmr1          n            bnmr   1a
# real.pl  /home/musrdaq/musr/mdarc/perl  musr           n            m15    2  ** always 2 for musr **
#
# Set a parameter (run type) in odb (mdarc area) for a real run,
# and call get_next_run_number.pl to supply the next valid run number.
#
# $Log: real_run.pl,v $
# Revision 1.1  2013/01/21 21:47:40  suz
# initial VMIC version for cvs
#
# Revision 1.14  2004/06/10 17:44:05  suz
# requires init_check.pl; our; Rev1.13 message is bad
#
# Revision 1.13  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.12  2004/04/21 19:10:06  suz
# MUSR uses 'enable logging' not 'write data' in edit_on_start;add a space in search string to fix bug for beamlines m15/m20
#
# Revision 1.11  2004/03/30 22:51:04  suz
# add some timing info using write_time
#
# Revision 1.10  2004/03/29 18:31:55  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.9  2004/03/23 19:37:09  suz
# set write data to y when selecting real run
#
# Revision 1.8  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.7  2003/01/08 18:35:25  suz
# add polb
#
# Revision 1.6  2002/04/12 21:45:57  suz
# add parameter include_path
#
# Revision 1.5  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.4  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.3  2001/09/14 19:23:38  suz
# add MUSR support and 1 param; use strict;imsg now msg
#
# Revision 1.2  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.1  2001/04/30 19:55:17  suz
# Initial version
#
#
#
use strict;
sub write_clicked($$);
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#########################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
our ($inc_dir, $expt,$dis_rn_check, $beamline, $ppg_mode, $old_rn ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "real_run"; #same as filename
our $parameter_msg = "include_path, experiment, flag (disable run number check),  beamline, ppg mode";
our $outfile = "real_run.txt";
our $nparam = 6;  # no. of input parameters
################################################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my $mdarc_path ;
my ($cmd, $eqp_name);
my $debug=$FALSE;
my $hidden_path = "/Custom/hidden/";

$|=1; # flush output buffers


# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
## Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 


# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt;  Flag to disable run number check = $dis_rn_check; beamline = $beamline; runnum=$old_rn\n";

unless ($beamline)
{
    print FOUT "FAILURE: beamline not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: beamline not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure: beamline not supplied \n"; } 
        die  "FAILURE:  beamline  not supplied \n";
}


write_clicked(0,$beamline); # bnm/q r clear /custom/hidden/clicked runmode button - perlscript not in action yet until checks are done

#
# Does not matter whether data logger is running or not
# But automatic run numbering must be enabled
#
unless ($dis_rn_check eq "n") 
{
        print FOUT "FAILURE: Automatic run numbering is DISABLED . \n"; 
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Automatic run numbering is DISABLED. " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        die  "FAILURE:  Automatic run numbering is DISABLED \n"; 
}

unless ($ppg_mode =~/^[12]/)
{
    print_3 ($name, "FAILURE: invalid ppg mode ($ppg_mode) supplied", $MERROR, 1);
}

#
#      determine equipment name from beamline
#
if( ($beamline =~ /bn[mq]r/i))
{
# BNMR experiments use equipment name of FIFO_ACQ
$eqp_name = "FIFO_ACQ";
}
else
{
# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
} 

$mdarc_path= "/Equipment/$eqp_name/mdarc/";
print FOUT "mdarc path: $mdarc_path\n";

#
# check whether run is in progress
#

($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state != $STATE_STOPPED)
{   # Run is going

# return
    if ($ppg_mode =~ /^2/i) # match 2 at beginning of string (e.g 2a, 2b) 
    {
	print FOUT "Run in progress. Use Toggle button to change run type \n";
	($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING - run is in progress. Use Toggle button to change run type" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die " Run is in progress. Use Toggle button to change the run type\n";
    }
    else  # Type 1
    {
	print FOUT "Run in progress.  Run type cannot be changed while run is in progress  \n";
	($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING - Run type cannot be changed while run is in progress " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "  Run type cannot be changed while run is in progress\n";
    }
}
else
{       # run is stopped
    if ($beamline =~ /bn[qm]r/i)     # bnm/qr only
    {
	## New - write "/custom/hidden/clicked ppgmode button" and "remember runnum"
        ## write from perlscript instead of status page javascript
	($status, $path, $key) = odb_cmd ( "set","$hidden_path","remember runnum","$old_rn") ;
	unless ($status)
	{ 
	    print FOUT "$name: Failure from odb_cmd (set); Error setting $hidden_path remember runnum to $old_rn \n";
	    odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting remember runnum to  $old_rn " ) ;
	    die "$name: Failure setting $hidden_path remember runnum to $old_rn  ";
	}
	write_clicked(1,$beamline); # set "/custom/hidden/clicked runmode button  (perlscript in progress)
    } # end New for beamline bnm/qr

    ($status) = odb_cmd ( "set","$mdarc_path","run type" ,"real") ;
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error setting run type\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting key run type " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
	write_clicked(0,$beamline); # clear "/custom/hidden/clicked runmode button  (perlscript NOT in progress)
        die "$name: Failure setting key run type\n";
    }
    else 
    { 
        print FOUT "$name: Success -  key run type has been set to real in odb\n"; 
    }


# Make sure that logging data is enabled
    if( ($beamline =~ /bn[mq]r/i))
      {
	($status) = odb_cmd ( "set","/experiment/edit on start","write data" ,"y") ;
      }
    else  # MUSR
      {
	($status) = odb_cmd ( "set","/experiment/edit on start","enable logging" ,"y") ;
      }
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error setting write data\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting key write data to y " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        write_clicked(0,$beamline); # clear "/custom/hidden/clicked runmode button  (perlscript NOT in progress)
        die "$name: Failure setting key write data\n";
    }
    else 
    { 
        print FOUT "$name: Success -  key write data has been set to y  in odb\n"; 
    }
    print FOUT "$name: About to call get_next_run_number.pl\n";
    write_time($name, $CONT);

    # now launch get_next_run_number - set check_for_holes TRUE for a real run
    print FOUT "Calling get_next_run_number with parameters: $inc_dir $expt 0 $eqp_name 1 $beamline $ppg_mode\n";
    $cmd = sprintf("$inc_dir/get_next_run_number.pl %s %s 0 %s 1 %s  %s",  
		   $inc_dir,$expt,$eqp_name,$beamline,$ppg_mode);
#    print "command: $cmd\n";
#    print FOUT "command: $cmd\n";
    $status=system "$cmd";

    print FOUT "$name: returned from get_next_run_number.pl\n";
    write_time($name, $CONT);

    if( $status == 0)
    {
	print FOUT "Success after system command, status=$status\n";
	print "Success after system command, status=$status\n";
    }
    else
    {
	print FOUT "Failure after system command, status=$status\n";
	print      "Failure after system command, status=$status\n";
	if ($status == -1 )
	{      # there is a message in errno
	    print FOUT "Error message: $!\n"; # errno is put in $! for system cmd
	    print      "Error message: $!\n";
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "Failure return from get_next_run_number due to: $! " ) ;
	    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
	}
	else
	{
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "Failure return from get_next_run_number (status=$status) " ) ;
	    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)"; }
	}
    }
}
write_clicked(0,$beamline); # clear "/custom/hidden/clicked runmode button  (perlscript NOT in progress)
write_time($name, 3);
exit;


sub write_clicked($$)
{
    my $value = shift;
    my $beamline = shift;
    my $path = $hidden_path."clicked runmode button";
    my ($status,$key);
  
    # write_clicked is for bnm/qr only
    unless ($beamline =~ /bn[qm]r/i) { return;}

    ($status, $path, $key) = odb_cmd ( "set","$hidden_path","clicked runmode button","$value") ;
    unless ($status)
    { 
	print FOUT "$name: Failure from odb_cmd (set); Error setting $hidden_path clicked runmode button true \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting clicked runmode button" ) ;
	die "$name: Failure setting $hidden_path clicked runmode button";
    }
    if($value == 0) { print "$name: cleared $path\n";}
    else { print "$name: set $path\n"; }
    return; 
}









