#!/usr/bin/perl
use strict;

#   tunes.pl
#
#                                        Input Parameters:   
#                  include          expt   beamline     ppg_mode  action        tunename      newtunename   OR  description
#                  directory                                                                 (rename only)    (save only)
# tunes.pl /home/bnmr/online/perl   bnmr   bnmr         2a        1-7 or 9      tune name               param7       
# 
#                                                                                               
#                                                                                              
#
#          action 1=load 2=save 3=delete 4=list  5=rename  9=abort
#                 6= add/replace description  7=clear description  (file  ..custom/tune_descriptions.js )
#                 8= compare two tunes (can be two tunefiles or with current params)
#
#          action 4,9    require first 5 parameters
#                 5      requires  all 7 parameters with param7=newtunename      
#                 1,2,3,7  require first 6 parameters
#                 2      may also have param7 (description)  
#                 6      param7 contains description to be added as a new  description  or replaces  description for a tune  
#                 8      requires 7 params 
#  
#         Output text file is      /home/midas/musr/log/vmic_bnmr/tunes.txt 
#         Tune files are in        /home/bn[mq]r/tunes/mode_*
#         Tune descriptions are in /home/bn[mq]r/custom/tune_descriptions.js               
######### G L O B A L S ##################################################################################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$TRUE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
##########################################################################
#  parameters needed by init2_check.pl (required code common to perlscripts)
#
# input parameters :
our ( $inc_dir, $expt, $beamline, $ppg_mode, $action, $tunename,  $param7 ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "tunes"; # same as filename
our $outfile = "tunes.txt";
our $nparam = 5; # need 5 input parameters for list, 7 for rename, 6 for everything else
our $parameter_msg = "include_path,  experiment , beamline, ppg_mode, parameter, [tunename,  newtunename]";
my $parameter_msg2 ="    parameter 1=load 2=save 3=delete 4=list  5=rename 9=abort ";
$parameter_msg = $parameter_msg . "\n" . $parameter_msg2 . "\n";
my ($newtunename, $descrip,$tunename1); # for param7
our $descripname="tune_descriptions";
our $odbpath="/custom/hidden/perlscript_done"; # 1= in progress  2 = success  3= error 4=set by javascript
our $in_progress_code = 1; # codes as above
our $done_code=2;
our $error_code=3;
our $online = $ENV{"ONLINE"};
our $htmlfile = "/home/$expt/$online/custom/tune_diffs.html";
our $needpsm;
my $compare_flag=0; # set for action= 8
#----------------------------------------------------------------------------------------
#
$|=1; # flush output buffers
my $done = get_done(0) +0;
print "done=$done\n";
if($done != $in_progress_code)
  {
      #print "calling set_done as done $done != $in_progress_code\n";
      set_done($in_progress_code);
  } # in progress

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl";
require "$inc_dir/init_check.pl";


if($done == $in_progress_code)
{print_3($name, "FAILURE: perlscript status code indicates that perlscript is already running. Try again later",$MERROR,$DIE ) ;}

print FOUT    " tunes  starting with parameters:  \n";
print FOUT    "   Experiment = $expt;  beamline = $beamline; ppg mode = $ppg_mode   \n";
print FOUT    "   parameter = $action  Tune name = $tunename  param7 = $param7; \n";  

#
#          Check the parameters
#
my ($i,$j);
$j=$#ARGV +1;
print "*** Number of parameters supplied: $j  ***\n";
$j=1;
for ($i==0; $i<= $#ARGV; $i++)
{ 
    print "Parameter ($j) = $ARGV[$i]\n"; 
    $j++;
}
print ("param7 = $param7\n");
$descrip = "";
$newtunename="";
$tunename1="";

unless ($EXPERIMENT =~ /bn[qm]r/i)  
{
    print_3($name, "FAILURE: Experiment type $EXPERIMENT not supported ",$MERROR,$CONT ) ;
    set_done($error_code); #error
    exit;
}
unless ($ppg_mode) 
{
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg   \n"; 
    print_3 ($name, "FAILURE: ppg mode not supplied. ",$MERROR,$CONT ) ;
    set_done($error_code); #error
    exit;
}
unless ($ppg_mode=~/^[12]/)
{
    print_3 ($name, "Invalid PPG mode supplied ($ppg_mode)",$MERROR,$CONT);
    set_done($error_code); #error
    exit;
}

unless ($action)
{
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n "; 
    print_3 ($name, "FAILURE: Parameter not supplied. ",$MERROR,$CONT);
    set_done($error_code); #error
    exit;
}

 $needpsm=0; # global
 unless(($ppg_mode =~/^1[0cjn]/) || ($ppg_mode =~/^2[g]/)) # these modes do not use RF
 { $needpsm=1; }
 print_2($name, "needpsm=$needpsm",$CONT);

if($action==9)
{
    print_2 ($name, "parameter is 9, aborting",$CONT);
    set_done($done_code);
    exit;
}

unless ($action=~/^[1-8]/)
{  
    print_3 ($name, "Invalid parameter supplied ($action). ",$MERROR,$CONT);
    set_done($error_code); #error
    exit;
}

unless ($action== 4)  # 4=list
{
    unless ($tunename)
    {
	print "Invoke this perl script $name with the parameters:\n";
	print "   $parameter_msg \n "; 
	print_3 ($name, "FAILURE: Tune name not supplied. ",$MERROR,$CONT);
        set_done($error_code); #error
        exit;
    }
}

if($action==5 )   # 5 rename a tune 
{ 
   unless ($param7)
   {
       print "Invoke this perl script $name with the parameters:\n";
       print "   $parameter_msg  \n"; 
       print_3 ($name, "FAILURE: NewTunename parameter not supplied. ",$MERROR,$CONT);
       set_done($error_code); #error
       exit;
   }
   else
   {
       $newtunename = $param7;
       print "newtunename = $newtunename\n";
   }
}

if($action==2 )  # 2 save
{
    print "param7= $param7\n";
   
   if ($param7)
   {
       $descrip = $param7;
       print_2 ($name, "Description to be added to file  $descripname .js is \"$descrip\" ",$CONT);
   }
   else
   {
       print_2 ($name, "Info: no Description supplied to be added to  file  $descripname .js ",$CONT);
   }
}

if($action==6 )  # 6 add or replace description
{
    print "param7= $param7\n";
   
   if ($param7)
   {
       $descrip = $param7;
       print_2 ($name, "Description to be added to file  $descripname .js  is \"$descrip\" ",$CONT);
   }
   else
   {
       print_2 ($name, "The Description to add has not been supplied ",$CONT);
       set_done($error_code); #error
       exit;
   }
}

if($action==8 )   #   8= compare tunes
{ 
    unless ($param7)
   {
       print "Invoke this perl script $name with the parameters:\n";
       print "   $parameter_msg  \n"; 
       print_3 ($name, "FAILURE: second tune for comparison is not supplied. ",$MERROR,$CONT);
       set_done($error_code); #error
       exit;
   }
   else
   {
       	$tunename1  = $param7;
       print "tunename1 = $tunename1\n";
   }
}
# Path in ODB to input parameter area
my $tunesfilepath = "/home/$expt/$online/tunes/mode_$ppg_mode/";
my $descripfilepath =  "/home/$expt/$online/custom/$expt/";
#Reserved tune filenames
my $savecurrenttune=  "_current"; # reserved filename to save current parameters  (used in action #8)
my $lasttune="_last"; # reserved filename to save last run's parameters
print_2 ($name, "Tunes file path is $tunesfilepath",$CONT);

if ($action == 1)
{
    load_tune_file( $tunesfilepath, $tunename);
}
elsif ($action == 2)
{
    $compare_flag=0;
    save_tune($tunesfilepath, $tunename, $ppg_mode, $descrip, $compare_flag ); 
}
elsif ($action == 3)
{
    delete_tune($tunesfilepath, $tunename, $ppg_mode );
}
elsif($action==4)
{
    find_tune_files($tunesfilepath, $ppg_mode);
}
elsif($action==5)
{
    rename_tune($tunesfilepath, $tunename, $ppg_mode, $newtunename);
}
elsif($action==6 || $action==7 )
{
    add_tune_description ($descripfilepath, $tunename, $descrip, $action);
}
elsif($action==8)
{   
    my $save_current=0;
    my $save_current_index=0; 
    my $tunename0=$tunename;

    if ($tunename1=~/^_?current/i) 
    {
	$save_current=1;
        $save_current_index=1; # $tunename1 

    }
    elsif  ($tunename0=~/^_?current/i) 
    {
	$save_current=1;
	$save_current_index=0; # $tunename0
    }
   
    print_2 ($name, "Tune \"$tunename0\" will be compared with tune \"$tunename1\". save_current=$save_current and save_current_index=$save_current_index ",$CONT);
    
    
  
    compare_tunes($tunesfilepath, $tunename0, $tunename1, $ppg_mode, $save_current, $save_current_index );
}

else
{
    print_2 ($name, "illegal parameter value $action",$CONT);
    set_done($error_code); #error
    exit;
}

exit;


sub load_tune_file($$)
{
    my  ($tunesfilepath, $tunename) = @_ ; # get the parameter(s)
    my  ($tunefile1, $tunefile2);
    my $status;

 
     print_2 ($name,"load_tune_file is starting with  ppg_mode $ppg_mode  and tune name $tunename",$CONT);

   $tunefile1 = $tunesfilepath.$tunename.".odb";
    print_2 ($name,"tunefile is  $tunefile1",$CONT);
    
# Check file exists
    unless (-f $tunefile1)  # includes directory
    { 
	print_2  ($name,"tune file $tunefile1 has not been found",$CONT);
        set_done($error_code); #error
	return ($FAILURE);  #No file found
    }
    
    if($needpsm)
    {
	$tunefile2 = $tunesfilepath.$tunename."_psm.odb";   
        print_2  ($name,"psm tunefile will be $tunefile2",$CONT);

	unless (-f $tunefile2)  # includes directory
	{ 
	    print_2($name,"cannot find tune file $tunefile2",$CONT);
	    #($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE:last saved file from odb key $last_saved_filename has not been found " ) ;
	    #unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
            set_done($error_code); #error
	    return ($FAILURE); 
	}
    }


# Load tune file(s) into ODB
    print_2 ("$name","Loading tunefile $tunefile1 into odb", $CONT);
    ($status) = odb_cmd ( "load","$tunefile1" ,"","","") ;
    print_2 ($name,"after odb_cmd, COMMAND=$COMMAND ",$CONT);
    if  ($status){  print_2($name, "loaded tune file $tunefile1",$CONT); }
    else { print_2 ($name,"$name: after odb_cmd, ANSWER=$ANSWER ",$CONT);}
    
    if($needpsm)
    {
	print_2 ("$name","Loading tunefile $tunefile2 into odb", $CONT);
	($status) = odb_cmd ( "load","$tunefile2" ,"","","") ;
	
	print_2 ($name,"after odb_cmd, COMMAND=$COMMAND ",$CONT);
	if  ($status){  print_2($name, "loaded tune file $tunefile2",$CONT); }
	else { print_2 ($name,"$name: after odb_cmd, ANSWER=$ANSWER ",$CONT);}
	
    }
 
    print_3 ($name,"tune: $tunename has been loaded for mode $ppg_mode ",$MINFO,$CONT);
    set_done($done_code); # done
    return;
    
}

sub delete_tune($$$)
{
    my ($tunesfilepath, $tunename, $ppg_mode) = @_;  # get the parameter(s)
    my  ($tunefile1, $tunefile2);
    my ($ans,$status);

    print_2 ($name,"delete_tune_file is starting with  ppg_mode $ppg_mode and tune name $tunename",$CONT);
    $tunefile1 = $tunesfilepath.$tunename.".odb";
    
    print_2($name,"deleting tunefile  $tunefile1",$CONT);
    
# Check file exists
    unless (-f $tunefile1)  # includes directory
    { 
	print_3  ($name," tune file $tunefile1 has not been found. Cannot be deleted",$MINFO,$CONT ) ;
        set_done($error_code); # error
	return ($FAILURE);  #No file found
    }
    
    print "command is  rm -rf $tunefile1\n";
    $ans=`rm -rf $tunefile1`;
    if($!)
    { 
	print_3 ($name,"error from rm $tunefile1 :  $!", $MERROR,$CONT);
        set_done($error_code); #error
        return;
	
    }
    else
    {print_2($name, "successfully removed file $tunefile1",$CONT);}
    
    if($needpsm)
    {
	$tunefile2 = $tunesfilepath.$tunename."_psm.odb";   
        print_2($name,"psm tunefile will be $tunefile2",$CONT);
	
	unless (-f $tunefile2)  # includes directory
	{ 
	    print_3  ($name," tune file $tunefile2 has not been found. Cannot be deleted",$MINFO,$CONT ) ;
            set_done($error_code); # error
	    return ($FAILURE);  #No file found   
	}
	
	
	print "command is  rm -rf $tunefile2\n";
	$ans=`rm -rf $tunefile2`;
        if($!)
        { 
	    print_3 ($name,"error from rm $tunefile2 :  $!", $MERROR,$CONT);
            set_done($error_code); #error
            return;
        }
        else
	{print_2($name, "successfully removed file $tunefile2",$CONT);}
}	
	

    print_3 ($name,"tune $tunename has been deleted for mode $ppg_mode",$MINFO,$CONT);

#  Update the array of tune names 
    print_2($name, "calling find_tune_files",$CONT);
	
    find_tune_files($tunesfilepath, $ppg_mode );

# remove any description for this tune in tune_descriptions.js
    remove_tune_description($descripfilepath, $tunename);
    set_done($done_code); # done
    return; # temp
    
}

sub save_tune($$$$$)
{
    my  ($tunesfilepath, $tunename, $ppg_mode, $description, $compare_flag ) = @_ ; # get the parameter(s)
    my ($tunefile1,$tunefile2, $status);
    
    print_2 ($name,"save_tune_file is starting with ppg_mode $ppg_mode and tune name $tunename",$CONT);
    
    $tunefile1 = $tunesfilepath.$tunename.".odb";
    
    print("tunefile will be $tunefile1 \n");
    
# Check that file does not exist already
    if (-f $tunefile1)  # includes directory
    { 
	print_2  ($name," tune file $tunefile1 already exists",$CONT);
        set_done($error_code); # error
	return ($FAILURE); 
    }
    
    if($needpsm)
    {
	$tunefile2 = $tunesfilepath.$tunename."_psm.odb";   
        print("psm tunefile will be $tunefile2\n");
	
	if (-f $tunefile2)  # includes directory
	{ 
	    print_2  ($name,"tune file $tunefile2 already exists",$CONT);
            set_done($error_code); # error
	    return ($FAILURE); 
	}
    }
    else
    { print_2 ($name, "not saving psm for mode $ppg_mode",$CONT); }
    
    
    
    my $file=$tunesfilepath."temp.cmd";  # odb command file (temporary) to save parameter data
    
    open OUT, ">$file"  or  die "cannot open file $file for writing: $! \n";
    print OUT "cd \"/Equipment/Fifo_acq/mode parameters/Mode $ppg_mode\"\n";
    print OUT "save $tunefile1\n";
    
    if($needpsm)
    {
	print OUT "cd \"/Equipment/Fifo_acq/frontend/hardware/psm\"\n";
        print OUT "save $tunefile2\n";
    }
    close OUT;

# Save data by executing temporary odb command file
    ($status) = odb_cmd ( "\@$file","" ,"","","") ;
    print_2 ($name,"$name: after odb_cmd, COMMAND=$COMMAND ",$CONT);
    if  ($status){  print_2($name, "executed odb cmd file  $file",$CONT); }
    else 
    { 
        print_3 ($name,"error after executing odb command file $file, ANSWER=$ANSWER ",$MERROR,$CONT ) ;
        set_done($error_code); # error
        return ($FAILURE); 
    }

    
    print_3 ($name,"new tune $tunename has been created for mode $ppg_mode",$MINFO,$CONT);

#  Update the array of tune names (list them)
    find_tune_files($tunesfilepath, $ppg_mode );

    unless($compare_flag)
    {
	if($description)
	{   add_tune_description($descripfilepath, $tunename, $description); } # add or replace;
	set_done($done_code); # done
    }
    else { print_3($name,"not setting done code because compare_flag is set",$MINFO,$CONT);}
    return $SUCCESS;
}
    


sub find_tune_files($$)
{
    my  ($tunesfilepath, $ppg_mode) = @_ ; # get the parameter(s)
    my  $load_tunefile="tune_names";
    my  $tunes_path = "/tunes";
    
    my @files;
    my ($status,$len);
    my $pattern = "*.odb"; 
    my $namefile=$tunesfilepath.$load_tunefile.".odb";  # file to be loaded into odb
    
    print_2 ($name,"\nfind_tune_file is starting with ppg_mode $ppg_mode and tunesfilepath $tunesfilepath",$CONT);
    
    unless ( chdir ("$tunesfilepath"))
    {
	
        print_3 ($name,"FAILURE - cannot change directory to $tunesfilepath",$MINFO,$CONT);
        set_done($error_code); # error
	return ($FAILURE);
    }
    
    @files = glob($pattern); 
    $len = $#files; # array length  (no. files found = len+1
    
    print_2 ($name," len = $len; Number of files found = len+1",$CONT);
    print_2 ($name," files : @files",$CONT);
    if ($len == -1) 
    {   # no tune files
	print_3 ($name," no tune files found for ppg mode $ppg_mode ",$MINFO,$CONT);
	
	# open file tune_names.odb
	open OUT, ">$namefile"  or die "cannot open file $namefile for writing: $! \n";
	
	print OUT "[$tunes_path]\n";
	print OUT "ppg_mode = STRING : [4] $ppg_mode\n"; 
	print OUT "num_tunes = INT : 0\n";
    }
    else
    {   # there are tune files
	my $elem;
	my @tunes;
    
    
	foreach $elem (@files)
	{
	    $elem=~s/.odb//;
	    if($elem=~/_psm/){ next; }
	    if($elem=~/$load_tunefile/){ next; }

	    if($elem=~/^$lasttune/)
	    { 
		print_2 ($name, "found reserved name $lasttune",$CONT);
		next;  # don't save "_last" as a tune name (reserved)
	    }
   
	    if($elem=~/^$savecurrenttune/)
	    { 
		print_2 ($name, "found reserved name $savecurrenttune",$CONT);
		next;  # don't save  $savecurrenttune ("_current") as a tune name (temporary file)
	    }
	    print "elem: $elem\n";
	    push @tunes, $elem;
	    
	}
	
	print_2 ($name,"List of non-reserved Tunes = @tunes ",$CONT);
	my $num_tunes = $#tunes + 1;
	print_2 ($name,"Number of non-reserved tunes found is $num_tunes",$CONT);
	
	
	# open file tune_names.odb
	open OUT, ">$namefile"  or die "cannot open file $namefile for writing: $! \n";
	
	print OUT "[$tunes_path]\n";
	print OUT "ppg_mode = STRING : [4] $ppg_mode\n"; 
	print OUT "num_tunes = INT : $num_tunes\n";
	
	if ($num_tunes < 1)
	{   print OUT "tune_names = STRING : [64] none\n"; }
	elsif ($num_tunes < 2)
	{   print OUT "tune_names = STRING : [64] $tunes[0]\n"; }
	else
	{
	    print OUT "tune_names = STRING[$num_tunes] :\n";
	    foreach $elem (@tunes)
	    {
		print OUT "[64] $elem\n";
	    }
	}
    } # end of tune files present

    close OUT;
    
# Load tune_names file into ODB
    ($status) = odb_cmd ( "load","$namefile" ,"","","") ;
    #print_2 ($name,"$name: after odb_cmd, COMMAND=$COMMAND ",$CONT);
    if  ($status){  print_2($name, "loaded tune_names file $namefile",$CONT); }
    else { print_3 ($name,"$name:error after odb_cmd load $namefile, ANSWER=$ANSWER ",$MERROR,$CONT);}

    print_3 ($name,"list of saved tunes $namefile has been updated for mode $ppg_mode",$MINFO,$CONT);
    set_done($done_code); # done
    return;
}


sub rename_tune($$$$)
{
    my ($tunesfilepath, $tunename, $ppg_mode, $newtunename) = @_;  # get the parameter(s)
   

    my  ($tunefile1, $tunefile2);
    my  ($newtunefile1, $newtunefile2);
    my ($ans,$status);
    
    print_2 ($name,"rename_tune_file is starting with  ppg_mode $ppg_mode, tune name $tunename and new tune name $newtunename",$CONT);
    $tunefile1 = $tunesfilepath.$tunename.".odb";
    $newtunefile1 = $tunesfilepath.$newtunename.".odb";
    print_2($name,"renaming tunefile  $tunefile1 to $newtunefile1",$CONT);
    
# Check tune file exists
    unless (-f $tunefile1)  # includes directory
    { 
	print_3  ($name," tune file $tunefile1 has not been found. Cannot be renamed",$MINFO,$CONT ) ;
        set_done($error_code); # error
	return ($FAILURE);  #No file found
    }
    
# Check new filename does not exist already
    if (-f $newtunefile1)  # includes directory
    { 
	print_3  ($name,"new tune filename $newtunefile1 already exists. Cannot rename $tunefile1",$MINFO,$CONT ) ;
        set_done($error_code); # error
	return ($FAILURE);  #New file found
    }
    
    if($needpsm)
    {
	$tunefile2 = $tunesfilepath.$tunename."_psm.odb";   
        $newtunefile2 = $tunesfilepath.$newtunename."_psm.odb";   
        print_2($name,"renaming psm tunefile $tunefile2 to $newtunefile2",$CONT);
	
	unless (-f $tunefile2)  # includes directory
	{ 
	    print_3  ($name," tune file $tunefile2 has not been found. Cannot be renamed",$MINFO,$CONT ) ;
            set_done($error_code); # error
	    return ($FAILURE);  #No file found   
	}
	
	if (-f $newtunefile2)  # includes directory
	{ 
	    print_3  ($name," tune file $newtunefile2 already exists. Cannot rename $tunefile2",$MINFO,$CONT ) ;
            set_done($error_code); # error
  	    return ($FAILURE);  #No file found   
	}
	
    } # end of needpsm
    
    print "command is  mv  $tunefile1 $newtunefile1\n";
    $ans=`mv $tunefile1 $newtunefile1`;
    if($!)
    { 
	print_3 ($name,"error from renaming $tunefile1 to $newtunefile1 :  $!", $MERROR, $CONT);
        set_done($error_code); #error
	return ($FAILURE);  #No file found
    }
    else
    { print_2($name, "successfully renamed file $tunefile1 to $newtunefile1",$CONT);}
    
    if($needpsm)
    {
	print "command is  mv $tunefile2 $newtunefile2\n";
	$ans=`mv $tunefile2 $newtunefile2`;
        if($!)
        { 
	    print_3 ($name,"error from mv $tunefile2  $newtunefile2 :  $!", $MERROR, $CONT);
           set_done($error_code); #error
      	return ($FAILURE);  #No file found
        }
        else
	{print_2($name, "successfully renamed file $tunefile2 to $newtunefile2",$CONT);}
    }	
    
    
    print_3 ($name,"tune $tunename has been successfully renamed to $newtunename for mode $ppg_mode",$MINFO,$CONT);
    
#  Update the array of tune names 
    print_2($name, "calling find_tune_files",$CONT);
    
    find_tune_files($tunesfilepath, $ppg_mode );


    print "Calling rename_tune_description ";
    rename_tune_description($descripfilepath, $tunename, $newtunename);
    set_done($done_code); # done
    return; # temp
    
}
#=================================================================================================
# These act on file ~/vmic_online/custom/tune_descriptions.js
#
sub remove_tune_description($$)
{
    # called if a tune is deleted. Remove tune and description for file tune_descriptions.js
    my ($descripfilepath, $tunename) = @_;  # get the parameter(s)

    my $my_name="remove_tune_description";
    my $error_message="$my_name: Cannot remove tune $tunename from description file";

    my ($gotit,$done, $linenum);
    my $file1 =$descripfilepath.$descripname.".js";
    my $file2=$descripfilepath.$descripname.".tmp"; 
    
    
    unless (-e $file1)
    {
        print_2 ($my_name,"$file1 does not exist. Cannot remove a Description for this tune",$CONT);
        set_done($error_code); # error
        return; 
    }

    open FILE, "$file1"  or die "$my_name: cannot open file $file1 for reading $! \n";  # open file for reading
    open OUT, ">$file2"  or die "cannot open file $file2 for writing: $! \n";  # open temporary file
    
  
    $gotit=$linenum=$done=0;
    while (<FILE>)
    {
        $linenum++;
	print $_;
        if($done) 
            { 
		print OUT $_;
		next; 
	    }
            unless ($gotit)
            {
		print OUT $_;
		# find the ppg mode (of the form  '{"ppgmode":"2e",...
		if (/'\{"ppgmode":"$ppg_mode"/)
		{
		    print "found string with ppgmode=$ppg_mode at line $linenum in $file1\n";
		    $gotit=1;
                    next;
		}
	    }
            else
            { 
                if (/'\{"ppgmode"/) # looking for the next ppgmode   
                {
		    print_2 ($my_name,"Did not find line for tune $tunename. No existing tune to delete",$CONT);
		    close OUT;
                    close FILE;
                    set_done($done_code); # done
		    return;                
                }
                else
                {
		    # looking for this tune 
		    if (/\{"tuneName":"$tunename"/) 
		    {
			print_2 ($my_name,"Found line containing tune $tunename at line $linenum. Removing this tune and description",$CONT);
                        $done=1;
		    }
                    else
                    {
			print OUT $_;
		    }
		}
	    }	
  

    }
    close FILE;
    close OUT;
    
    unless($gotit)
    {
	print_2($my_name, "no tunes and descriptions found for PPG Mode $ppg_mode ",$CONT);
        set_done($error_code); # error
        return;
    }

    rename_files($descripfilepath, $tunename, $error_message);
    print_2 ($my_name, "successfully removed tune $tunename and description",$CONT);
    set_done($done_code); # done
    return;

}

sub rename_tune_description($$$)
{
    # called if tune is renamed; description is not changed
    my ($descripfilepath, $tunename, $newtunename) = @_;  # get the parameter(s)
    my $my_name="rename_tune_description";
    my $error_message="Cannot rename tune $tunename";

    my ($gotit,$done,$linenum);
    
    my $file1=$descripfilepath.$descripname.".js";
    my $file2=$descripfilepath. $descripname.".tmp";
    
    
    my($string,$newstring);

    print "$my_name starting with params $descripfilepath, $tunename, $newtunename\n";
   
    unless (-e $file1)
    {
        print_3 ($name,"$file1 does not exist. Cannot rename a tune in Description file for this tune",$MERROR, $CONT);
        set_done($error_code); # error
        return; 
    }

    open FILE, "$file1"  or die "$my_name: cannot open file $file1 for reading $! \n";  # open file for reading
    open OUT, ">$file2"  or die "cannot open file $file2 for writing: $! \n";  # open temporary file
 

    $gotit=$linenum=$done=0;
    while (<FILE>)
    {
        $linenum++;
	print $_;
	if($done) 
	{ 
	    print OUT $_;
	    next; 
	}
	unless ($gotit)
	{
	    print OUT $_;
	    # find the ppg mode (of the form  '{"ppgmode":"2e",...
	    if (/'\{"ppgmode":"$ppg_mode"/)
	    {
		print "found string with ppgmode=$ppg_mode at line $linenum in $file1\n";
		$gotit=1;
		next;
	    }
	}
	else
	{ 
	    # now look for this tune and description
	    if (/\{"tuneName":"($tunename)"/) 
	    {
		print_2 ($name,"Found line containing tune $tunename at line $linenum. Renaming to $newtunename",$CONT);
		print_2 ($name,"substituting string $1 ",$CONT);
                s/$1/$newtunename/;
                $done=1;
	    }
	    print OUT $_;
	    
	}
    } # while	
    
    unless ($gotit)
    { 
	print_3 ($name, "Did not find line containing tune $tunename", $MINFO,$CONT);
        set_done($error_code); # error
	return;
    }
    
    close FILE;
    close OUT;
    
    rename_files($descripfilepath, $tunename, $error_message);
    set_done($done_code); # done
    return;
}
    
sub add_tune_description($$$$)
{   # add tune description or replace a previous one
    # tunename is not changed
    # 
    my ($descripfilepath, $tunename, $description, $action) = @_;  # get the parameter(s)
    my $linenum;
    my $my_name="add_tune_description";
    my ($gotit,$done);
    my $error_message="Cannot add or replace tune description";


    my $file1=$descripfilepath.$descripname.".js";
    my $file2=$descripfilepath. $descripname.".tmp";
    my $file3=$descripfilepath. $descripname.".old";

    if($action==7) # clear description
    {	$description=""; }

    unless (-e $file1)
    {
        print_3 ($my_name,"$file1 does not exist. Cannot create a Description for this tune",$MERROR, $CONT);
        set_done($error_code); # error
        return; 
    }

    open FILE, "$file1"  or die "$my_name: cannot open file $file1 for reading $! \n";  # open file for reading
    open OUT, ">$file2"  or die "cannot open file $file2 for writing: $! \n";  # open temporary file
 
 
    
	$gotit=$linenum=$done=0;
	while (<FILE>)
	{
            $linenum++;

            if($done) 
            { 
		print OUT $_;
		next; 
	    }
            
 
            unless($gotit)
	    {
                # First look for the closing of the last ppgmode (penultimate non-blank line of the file)
                # This means ppgmode is not yet defined in description file
		if(/^\s+']}'\+/)  # ']}'+ with 1 or several blanks  Last one has NO COMMA!
		{ 
		    # Add a new ppg_mode
		    # if $ppg_mode is NOT already in the file, neither $gotit nor $done will be true. 

		    print "my_name: done=$done; gotit=$gotit found line $_ \n";
		    print_2 ($my_name,  "found penultimate non-blank line of input file $file1 at linenum $linenum : \"$_\" ",$CONT); 
		    # add a new ppg mode
		    print_2 ($my_name,  "Adding json for new ppgmode $ppg_mode to input file $file1 ",$CONT); 
		    print OUT "        ']},'+ \n";  # with a comma
		    print OUT "\n";
		    print OUT "        '{\"ppgmode\":\"$ppg_mode\",\"tunes\":\[' + \n";
		    print OUT "              '{\"tuneName\":\"$tunename\",\"description\":\"$description\" },' + \n"; # with a comma; there is always a last line
		    print OUT "              '{\"tuneName\":\"last\",\"description\":\"Description for  Tune:last  PPG Mode $ppg_mode\" }' + \n";
		    
		    print OUT $_;  # line ']}'+  with no comma
		    $done=1; # will add the very last line
		    next;
		}
		
		# We have not yet reached the end of file
		print OUT $_;
		# find the ppg mode (of the form  '{"ppgmode":"2e",...
		if (/'\{"ppgmode":"$ppg_mode"/)
		{
		    print "... found string with ppgmode=$ppg_mode at line $linenum in $file1\n";
		    $gotit=1;
                    if($action !=7) # add the new string at the top (7= clear description)
		    {
			print OUT "              '{\"tuneName\":\"$tunename\",\"description\":\"$description\" },' + \n"; # with a comma; there is always a last line
		    }
                    next;
		}
	    } # end of unless($gotit)
            else
            { 
                if (/'\{"ppgmode"/) # looking for the next ppgmode (i.e. end of this ppgmode)   
                {       
		    print_2 ($name,"Did not find a line for tune $tunename. No existing description to delete",$CONT);
		    print OUT $_;
		    $done=1;                   
                }
                else
                {
		    # otherwise look for this tune with a previous description
		    if (/\{"tuneName":"$tunename"/) 
		    {
			print_2 ($name,"Found line containing tune $tunename at line $linenum. Removing existing description for this tune",$CONT);
                        $done=1;
		    }
                    else
                    {
			print OUT $_;
		    }
		}
	    }	
  
	} # while
    close FILE;
    close OUT;

  
    rename_files($descripfilepath, $tunename, $error_message);
    print_2 ($name, "successfully added Description for tune $tunename",$CONT);
    print_2 ($name, "calling set_done with done=$done_code",$CONT);
    set_done($done_code); # done
    return;
}
    
sub compare_tunes($$$$$$)
{

    my($tunesfilepath, $tunename0, $tunename1, $ppg_mode, $save_current, $save_current_index) = @_ ; # get the parameter(s)
    my $my_name="compare_tunes";
    my($ans,$elem);
    my @files;
    my @names;

    print_2 ($my_name,"compare_tunes is starting with ppg_mode=$ppg_mode, tunename0=$tunename0, tunename1=$tunename1, save_current=$save_current",$CONT);
    
  
    if($save_current)
    {
        # Delete previous temporary odb file(s)
	@files=glob( $tunesfilepath.$savecurrenttune."*.odb"); # temporary save file for current mode parameters
	foreach $elem (@files)
	{ 
	    print_2  ($my_name," temporary tune file $elem already exists; deleting it",$CONT);
	    $ans = `rm -rf $elem`;  # old temp file
	    if($!) 
	    { 
		print_3 ($my_name,"error from rm $elem :  $! ", $MERROR,$CONT);
		set_done($error_code); #error
		return ($FAILURE);	
	    }
	    print_2($my_name, "successfully removed previous temporary tunefile $elem",$CONT);
	}

   
	my $status = save_tune($tunesfilepath, $savecurrenttune, $ppg_mode, $descrip, 1  ) ; #compare_flag=1
    
	print_2($my_name, "save_tunes is finished with status $status\n");
	if($status == $SUCCESS){print "success from save_tune\n";}
	else
	{
	    print_3 ($my_name,"error from save_tune() ", $MERROR,$CONT);
	    set_done($error_code); #error
	    return ($FAILURE);
	}

        if( $save_current_index == 1)
	{
	    $tunename1 =  $savecurrenttune;
	}
	else
	{
	    $tunename0 =  $savecurrenttune;
	}
    } # end of $save_current

    
    open (FH, "> $htmlfile") or die $!;  # open html file
    print "Opened $htmlfile\n";
    
    print FH "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
    print FH "<html><head><title>Tune Differences</title>\n";
    print FH "\n<!-- This file created by tunes.pl -->\n";
    print FH "<script type=\"text/javascript\" src=\"mhttpd.js\">\n";
    print FH "</script>\n";

    print FH "<link rel=\"stylesheet\" type=\"text/css\" href=\"mhttpd.css\" > <!-- midas stylesheet (ignores title) --> \n";
    print FH "<link type=\"text/css\" rel=\"stylesheet\" href=\"/CS/styles.css!\" title=\"Stylesheet\"> \n";
    print FH "<link type=\"text/css\" rel=\"stylesheet\" href=\"/CS/common.css!\" title=\"Stylesheet\"> \n";
    print FH "<body>\n";
    print FH "<span class=\"title\">PPG Mode: $ppg_mode</span>\n";

    my $datestring = localtime();
    print "Local date and time $datestring\n";
    print FH "<h3><center>Last Comparison at <span style='color:blue'>$datestring</span></center></h3>\n";
    @names[0]=$tunename0;
    @names[1]=$tunename1;
    for(my $i=0; $i<2; $i++)
    {
	if (@names[$i]=~/^$lasttune/i) { @names[$i]="last run settings";}
	if (@names[$i]=~/^$savecurrenttune/i) { @names[$i]="current settings";}
    }
    print FH "<h3>Differences between tunes <span class=\"italick\">\"@names[0]\"</span> and  <span class=\"italick\">\"@names[1]\"</span>: </h3>\n";
    print FH "<table style=\"width: 100%;\" border=\"1\" cellpadding=\"1\" bgcolor=\"white\">";
  
    my $loops=2; # loop over mode and psm files
    my ($tunefile0, $tunefile1, $tempfile, $type);
    my @compare_class;
    @compare_class=qw(psm_tune1 psm_tune2 tune1 tune2 onef_b common);
    my $index;
    my $dir="";

    while ($loops > 0)
    {
	if($loops == 1)
	{
	    $type = "PSM";
	    $tunefile0 = $tunesfilepath.$tunename0."_psm.odb";  
	    $tunefile1 = $tunesfilepath.$tunename1."_psm.odb";
            $index=0; # @compare_class index 
	}
	elsif ($loops==2)
	{
	    $type = "Mode";
	    $tunefile0 = $tunesfilepath.$tunename0.".odb";  # current mode parameters
	    $tunefile1 = $tunesfilepath.$tunename1.".odb";  # last loaded mode parameters
            $index=2;
	}
	else {last;}

	$tempfile =  $tunesfilepath."diff.tmp";

#	print "tunefile0= $tunefile0 and tunefile1 = $tunefile1   loops=$loops \n";
    
	if (-f $tempfile)  # includes directory
	{ 
	    $ans = `rm -rf $tempfile`;  # old temp file
	    if($!) 
	    { 
		print_3 ($name,"error from rm $tempfile :  $! ", $MERROR,$CONT);
		set_done($error_code); #error
		return ($FAILURE);	
	    }
	    print_2($name, "successfully removed temporary file $tempfile",$CONT);
	}
# Check that both these files exist
	unless (-f $tunefile0)  # includes directory
	{ 
            print " tunefile0 not found; loops=$loops\n";
	   # if($loops==1) { last; }  # psm files are not always present
	   # this should not longer be needed because we have broken from the loop if $needpsm=0 
	    print_2  ($my_name," tune file $tunefile0 does not exist",$CONT);
            print FH "Error : tune file $tunefile0 does not exist\n";
	    set_done($error_code); # error
	    return ($FAILURE); 
	}
	unless (-f $tunefile1)  # includes directory
	{ 
            print " tunefile1 not found; loops=$loops\n";
	    print_2  ($my_name," tune file $tunefile1 does not exist",$CONT);
            print FH "Error : tune file $tunefile1 does not exist\n";
	    set_done($error_code); # error
	    return ($FAILURE); 
	}

	
	
	
# compare the files
        print_2 ($my_name, "command is diff $tunefile0 $tunefile1 --side > $tempfile",$CONT);
        
	$ans = `diff $tunefile0 $tunefile1 --side > $tempfile`;  
	if($!) 
	{ 
	    print_3 ($name,"error from \"diff $tunefile0 $tunefile1 > $tempfile\" :  $! ", $MERROR,$CONT);
	    set_done($error_code); #error
	    return ($FAILURE);	
	}
	
	my $flag =0;
	
	if(-f $tempfile)
	{
	  my $diffs=0; # number of differences found  
	
	  print FH "<tr align=\"center\">";
	  print FH "<td class=\"$compare_class[$loops+3]\" colspan=\"2\" >";
	  print FH "<span class=\"bold\">$type parameters: </span></td></tr>\n";
          if($loops==2)
	  {
	     print FH "<td class=\"$compare_class[$index]\" style=\"font-weight:bold;\">Tune 1: <span class=\"italick\">$tunename</span></td>\n";
	     print FH "<td class=\"$compare_class[$index+1]\" style=\"font-weight:bold;\">Tune 2: <span class=\"italick\">$tunename1</span></td>\n";
	  }
		
	  open FILE, $tempfile or die $!;
	  my @lines = <FILE>;
	  foreach $elem (@lines)
	  {
              if($loops == 1) #psm
              {
		#  if( $elem=~/\[.+one_f.*\](\s+)\[.+one_f.*\]/) # a directory with one_f
                #  { $dir = "one_f/"; 
                #    print "found $dir\n";
                #  }    # don't bother indicating one_f (default)
		  if( $elem=~/\[.+fref.*\](\s+)\[.+fref.*\]/) # a directory with fref
                  { $dir = '<span style="font-size:80%">(fref)&nbsp </span>'; 
                    print "found $dir\n";
		  }
              }
	      if( $elem=~/\[.+\](\s+)\[.+\]/) {next;}  # skip paths
	      
	      chomp $elem;

	      # look for missing parameters marked by     blanks > parameter_name
	      #                                or parameter_name < blank 
	      if ($elem =~/^\s+>/)  # whitespace precedes ">" (assumes ODB key does not start  " > blah")
	      {
		  $diffs++;
		  my @fields = split />/, $elem ;
		  # expect length=1 fields[0]=blank and fields[1]=parameter_name
		  #print "1 found blanks > now  now len= $#fields and fields[0]=\"$fields[0]\"  fields[1]= $fields[1]\n";
		  print FH "<tr><td class=\"$compare_class[$index]\" style=\"color:red;font-style:italic;\">parameter is missing</td>\n";

		  if ($fields[1] =~/(.+)=(.+):(.+)/)  # separate the fields by "=" and ":"
		  {
		      print FH "<td class=\"$compare_class[$index+1]\">$1 : <span style=\"color:red\">$3</span></td></tr>\n";
		  }


	      }
	      
	      elsif ($elem =~ /\<\s*$/)  #  parameter_name "<" followed by whitespace (assumes ODB key does not end "<")
	      {
		  $diffs++;
		  my @fields = split /</, $elem ;   # expect  length=0 fields[0]=parameter_name and fields[1]=blank		   
		  #print "2 found < blanks  now len= $#fields and fields[0]=\"$fields[0]\"  fields[1]= \"$fields[1]\" \n";
		  if ($fields[0] =~/(.+)=(.+):(.+)/)  # separate the fields by "=" and ":"
		  {
		      print FH "<tr><td class=\"$compare_class[$index]\" >$1 : <span style=\"color:blue\">$3</span></td>\n";
		  }
                  print FH "<tr><td class=\"$compare_class[$index]\" style=\"color:red;font-style:italic;\">parameter is missing</td>\n";

	      }

	     elsif ($elem =~ /\|/)  # normal difference   parameter_name |  parameter_name
	     {
                 $diffs++;
		 my @fields = split /\|/, $elem ;
		 if ($fields[0] =~/(.+)=(.+):(.+)/)  # separate the fields by "=" and ":"
		 {
		    print FH "<tr><td class=\"$compare_class[$index]\" >$dir$1 : <span style=\"color:blue\">$3</span></td>\n";
		 }
		 if ($fields[1] =~/(.+)=(.+):(.+)/)  # separate the fields by "=" and ":"
		 {
		    print FH "<td class=\"$compare_class[$index+1]\">$dir$1 : <span style=\"color:red\">$3</span></td></tr>\n";
		 }
	     }
	  }
	  if($diffs ==0) {
	      print FH "<tr><td colspan=2 style=\" text-align: center;\" >No differences found</td></tr>"; }
	 close FILE;

      }
	else	{ print "cannot find differences file $tempfile\n"; }
	$loops--; # decrement while loop counter
        if($needpsm ==0) 
	{
	    print_2($name, "not comparing PSM files because needpsm=$needpsm",$CONT);
	    last;
        }
	print "decremented loops to $loops; now comparing psm files\n";
    } # end of while loop
    print FH "</table><br>\n";
    #print FH "<br><br><input name=\"close_diffs\" value=\"Close\" type=\"button\"  style=\"color:firebrick\" onClick=\"  window.close() ;\">\n";
    print FH "</body></html>\n";
    close FH;
    set_done($done_code); # done
    return;
}


sub rename_files($$$)
{
    my ($descripfilepath, $tunename, $error_message) = @_;  # get the parameter(s)
		     
    my $my_name="rename_files";
    my $ans;
    my $file1=$descripfilepath."tune_descriptions.js";
    my $file2=$descripfilepath."tune_descriptions.tmp"; 
    my $file3=$descripfilepath."tune_descriptions.old"; 
    
    unless(-e $file2)
    {
        print_3 ($my_name," $file2 should exist but does not. $error_message ", $MERROR,$CONT);
        set_done($error_code); #error
	return ($FAILURE);  #No file found
    }
    
    if(-e $file3) 
    { 
	$ans = `rm -rf $file3`;  # old
	if($!) 
	{ 
	    print_3 ($my_name,"error from rm $file3 :  $!  $error_message", $MERROR,$CONT);
	    set_done($error_code); #error
	    return ($FAILURE);  #No file found	
	}
	print_2($my_name, "successfully removed file $file3",$CONT);
	
    }
    
    $ans = `mv $file1 $file3`; # current -> old
    if($!) 
    { 
	print_3 ($my_name,"error from rename $file1 to $file3 :  $!  $error_message", $MERROR,$CONT);
	set_done($error_code); #error
	return ($FAILURE);  #No file found
    }
    print_2($my_name, "successfully renamed $file1 to $file3",$CONT);
    
    $ans = `mv $file2 $file1`; # new -> current
    if($!) 
    { 
	print_3 ($my_name,"error from rename $file2 to $file1 :  $!  $error_message", $MERROR,$CONT);
	set_done($error_code); #error
	return ($FAILURE);  #No file found
    }
    print_2($my_name, "successfully renamed $file2 to $file1",$CONT);
    set_done($done_code); # done
    return;
}   
    
sub set_done($)
{   # 1= in progress  2 = success  3= error 
    my ($value) = @_;  # get the parameter(s)
    my $ans;

  #  print("set_done starting with value= $value\n");

    $ans = `odb -e $expt -c 'set $odbpath $value'`; # set parameter
    if($!) 
    { 
	if($value > 1) { print_3 ($name,"error from odb set command to path $odbpath :  $! ", $MERROR,$DIE ) ;	}
        else { print "error from odb set command to path $odbpath :  $!  \n"; } # print_3 not yet defined
    }
    else
    {
       if($value > 1) {   print_2 ($name, "set $odbpath to $value",$CONT);}
       else {  print ("set $odbpath to $value\n");} # print_2 not yet defined
    }
    get_done($value);
    return;  
}

sub get_done($value)
{
    my ($value) = @_;  # get the parameter(s)
 
    my $string;

    my $ans = `odb  -e $expt -c 'ls -v $odbpath '`; # readback
  
    # our  $in_progress_code=1; $done_code=2; $error_code=3;
    chomp $ans;
    if($ans == $in_progress_code)
    {	$string = "in progress"; }
    elsif  ($ans ==$done_code)
    {	$string = "done (success)"; }
    elsif  ($ans ==$error_code)
    {	$string = "done (error)"; }
    else
    {	$string = "undefined"; }

    if($value == 0)
    { 
        print "get_done: ODB key \"/custom/hidden/perlscript_done\" =$ans\n";
	return $ans;
    }  # no valid value supplied

    if($value > 1) 
    { 
#	print_2("get_done", "starting with value= $value",$CONT);
	print_2 ("get_done", "read back $ans i.e. code= $string",$CONT);
    }
    else
    { 
#	print("get_done : starting with value= $value\n");
	print ("get_done  : read back $ans i.e. code= $string \n");
    }
    return $ans;
}
