#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d
#
use warnings;
use strict;

#    BNMR/BNQR only - monitor beam current
#    Run monitor.pl from BNMR account only. It will monitor bnmr and bnqr if both enabled.
require "/home/bnmr/online/perl/perlmidas.pl";

# prototypes
sub  get_filename_for_shift();
sub read_epics_value($);
#sub send_ssh_cmd(); #replaced by send_odb_command
sub send_odb_command($);
sub write_line($$$$);  # prototype with 4 arguments
sub add_new_exp($$);
sub add_year($);
sub get_digits($);

# input parameters   minimum proton current
#         if $noprint > 0, no output is printed
#   cron job should suppress printing otherwise email will be sent out
#   For cron job,  crontab -e    
#       e.g. monitor.pl  21.45 1
#
#   To stop cron job, comment out monitor.pl line
#
#   To try this file, with printing
#       e.g. monitor.pl 11.4
#
#   
#   For TESTING, 
#                      thresh noprint testing enab_bnmr enab_bnqr
#      e.g. monitor.pl 12.5     0       1        1         0
#                
#         if $testing is true, line in file with have a "*" beside it
#
#            1.   sets $bnmr to $test_bnmr_enabled  ( ignores actual epics switch value)
#            2.   sets $bnqr to $test_bnqr_enabled  ( ignores actual epics switch value)
#            3.   to change expt numbers, change them in the odb of each expt, e.g.
#                 odb -c 'set "/experiment/edit on start/experiment number" 1509'   
#
#
#   
our ($min_proton_current, $noprint, $testing, $test_bnmr_enabled, $test_bnqr_enabled) = @ARGV;# input parameters
my $len = $#ARGV +1; # array length

our $DEBUG = 0;
our $MIDAS_EXPERIMENT;
our $MONITOR_PATH = "/isdaq/data1/bnmr/mon/"; # the current year will be added to this
our $MONITOR_DIR; # this will be $MONITOR_PATH with current year added
our $MONLOG_FH;
our $MIDAS_RN_PATH = "/Runinfo/Run number";
our $EXP_NUM_PATH = "/Experiment/Edit on start/experiment number";
our $DAQ_HOSTNAME = $ENV{"DAQ_HOST"};

my $BNQR_MSERVER_PORT="1177"; # bnqr mserver port

my $logfilename="monitor";
my $EPICS_PROTON_CURRENT="CCS2ISAC:BL2ACURRENT";
my $EPICS_MANUAL_SELECT="BNMR:BNQRSW:MANUALSELECT";
my $EPICS_SWITCHING="BNMR:BNQRSW:REQSWITCHING";

my $EPICS_TEMP="ILE2:BIAS15:RDCUR"; # this is standing in for the proton current which reads 0 currently
my $name="monitor";
my $read_proton_current=0;
my ($filename,$tempfile,$string);
my($gotit,$linenum);
# time
#my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);
my ($dual_channel_mode,$manual_select);
my ($cmd,$status);
my $xm; # AM or PM
my($exp_num,$bnmr_time,$bnqr_time);


# Required information to update the entry
my ($bnmr_exp_num,$bnqr_exp_num); # experiment number
my $proton_uAmps; # current proton current
my $dual_mode; # dual channel mode
my $beam_select; # bnmr/bnqr in single channel mode

$min_proton_current +=0; # input parameter  
$noprint += 0; #input parameter
$testing += 0; #input parameter
$test_bnmr_enabled +=0;
$test_bnqr_enabled +=0;

my $ans;

my $username= $ENV{'USER'}; 
unless ($username=~/bnmr/i)
{ die "$name: run this monitor programme as user \"bnmr\" only"};


# Make sure DAQ_HOSTNAME exists
unless (defined($DAQ_HOSTNAME) &&   $DAQ_HOSTNAME ne "")
{
    print LOG   "$name: environment variable DAQ_HOST must be defined\n";
    die  "$name: environment variable DAQ_HOST must be defined\n";
}

# Make sure directory exists (no year yet)
if (-d $MONITOR_PATH) 
{ 
    unless ($noprint){ print "$name: found existing directory $MONITOR_PATH\n"};
}
else
{
    $ans =  `mkdir $MONITOR_PATH`;
    if( $!)
    {
	print LOG   "$name: failure creating directory \"$MONITOR_PATH\"   : $!\n";
	die  "$name: failure creating directory \"$MONITOR_PATH\"  : $!\n";
    } 
}

my  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(); # get present localtime to find year
$year+=1900; # year since 1900
$MONITOR_DIR = add_year($year); # returns full path with year. Creates directory if absent

my $logfile = $MONITOR_DIR.$logfilename.".log";
# open log file
if(-e $logfile)
{
    open LOG,">> $logfile" or die $!;
    unless($noprint){ print "Opened log file $logfile for append\n";}
}
else
{
    open LOG,"> $logfile" or die $!;
    unless($noprint){ print "Opened new log file $logfile for write\n";}
}
my $datestring =  localtime();
print LOG "\n -----------  $name starting at $datestring ---------------\n";


#print LOG "$name: testing email gets sent\n";
#print "$name: testing email gets sent\n";

MIDAS_env(); # setup for MIDAS BNMR 

# Check if the proton current is above threshold
$read_proton_current = read_epics_value($EPICS_PROTON_CURRENT);
if ($read_proton_current =~ /err/)
{
    print LOG "$name: error from read_epics_value($EPICS_PROTON_CURRENT)\n";
    die  "$name: error from read_epics_value($EPICS_PROTON_CURRENT)\n"; 
}

my $temp = $read_proton_current;
$temp =~ s/\.//; # remove a decimal point if present
if($temp =~ /\D/)
{
    print LOG "$name: error - expect numerical response from read_epics_value($EPICS_PROTON_CURRENT). Found a non-digit\n";
    die  "$name: expect numerical response from read_epics_value($EPICS_PROTON_CURRENT). Found a non-digit\n"; 
  
}   
$read_proton_current +=0; # number
if($read_proton_current < $min_proton_current)
{ 
    unless($noprint){ print "$name: proton current is too low : $read_proton_current < $min_proton_current (min). Nothing to do.\n"; }
    print LOG  "$name: proton current is too low : $read_proton_current < $min_proton_current (min), Nothing to do.\n";
    exit; 
}

#Check if dual mode or bnmr is running


$dual_channel_mode = read_epics_value($EPICS_SWITCHING); # expect "off" or "on"
if ($dual_channel_mode =~ /err/)
{
    print LOG "$name: error from read_epics_value($EPICS_SWITCHING)\n";
    die  "$name: error from read_epics_value($EPICS_SWITCHING)\n"; 
}
my $bnqr=2; # init
my $bnmr=2; # init
if ($dual_channel_mode =~ /on/i)
{ 
    $bnmr=1;
    $bnqr=1;
    unless($noprint){ print "$name: both bnmr and bnqr experiments are selected by dual channel mode\n"; }
    #print LOG "$name: both bnmr and bnqr experiments are selected by dual channel mode\n";
}
elsif  ($dual_channel_mode =~ /off/i)
{ 
    $bnmr=0;
    $bnqr=0;
    unless($noprint){ print "$name: dual channel mode is disabled ($dual_channel_mode)\n"; }
    #print LOG "$name: dual channel mode is disabled ($dual_channel_mode) \n";
}

$bnmr+=0;
$bnqr+=0;

# Read manual_select even if dual channel mode (for checking)
$manual_select = read_epics_value($EPICS_MANUAL_SELECT); # expect "bnmr" "bnqr" or "Osaka" or "none"
if ($manual_select =~ /err/)
{
    print LOG "$name: error from read_epics_value($EPICS_MANUAL_SELECT)\n";
    die "$name: error from read_epics_value($EPICS_MANUAL_SELECT)\n"; 
}


if ($bnmr ==0)  # single channel mode
{
    if ($manual_select =~ /bnmr/i)
    {
	$bnmr=1; # bnmr is selected
	unless($noprint){ print "$name: bnmr experiment is selected by manual switching\n";}
	#print LOG "$name: bnmr experiment is selected by manual switching\n";
    }
    elsif ($manual_select =~ /bnqr/i)
    {
	$bnqr=1; # bnqr is selected
	unless($noprint){ print "$name: bnqr experiment is selected by manual switching\n";}
	#print LOG "$name: bnqr experiment is selected by manual switching\n";
    }
    else
    {
	unless($noprint){ print "$name: neither expt is selected by manual switching. EPICS_MANUAL_SELECT= $manual_select.\n";}
        #print LOG "$name: neither expt is selected by manual switching. EPICS_MANUAL_SELECT= $manual_select.\n";
    }
} # end of single channel mode
 
print LOG "$name: dual channel mode: $dual_channel_mode; manual switching: $manual_select\n";
###  TESTING only ####
if ($testing)
{
    unless($noprint) { print "***  Testing is TRUE .. using test values for bnmr enabled ($test_bnmr_enabled) and bnqr enabled ($test_bnqr_enabled) ***\n";}
    print LOG  "***  Testing is TRUE .. using test values for bnmr enabled ($test_bnmr_enabled) and bnqr enabled ($test_bnqr_enabled) ***\n";
    $bnmr = $test_bnmr_enabled;
    $bnqr = $test_bnqr_enabled;
}

unless (($bnmr == 1) || ($bnqr == 1))
{
    unless($noprint){ print "$name: neither experiment is selected; nothing to do \n";}
    print LOG "$name: neither experiment is selected; nothing to do\n";
    exit;
}

$bnmr_exp_num =0+0; # numerical
$bnqr_exp_num=0+0;
#Determine the experiment number
if($bnmr == 1)
{   # Get the experiment number from ODB
    $bnmr_exp_num = MIDAS_varget($EXP_NUM_PATH);
    unless($noprint){ print "BNMR experiment number is \"$bnmr_exp_num\"\n";}
    $bnmr_exp_num =~ s/ *$//; #remove trailing spaces
    $bnmr_exp_num  =~ s/^ *//; # and leading spaces

    my $num=get_digits($bnmr_exp_num); # returns 0 if no digits found
    $bnmr_exp_num=$num+0; #  numerical (for perl)
   
    unless ($bnmr_exp_num > 0)
    {
	print LOG  "$name: ERROR - illegal or invalid experiment number for bnmr ($bnmr_exp_num)\n";
	die  "$name:  ERROR -  illegal or invalid experiment number for bnmr ($bnmr_exp_num)\n";
    }

}


if($bnqr == 1)
{    # Use ssh to get bnqr's experiment number
   # $bnqr_exp_num = send_ssh_cmd();

    my $hostname= "isdaq01".":".$BNQR_MSERVER_PORT; # hostname:mserver port
    my $odbcmd='ls '.'"'.$EXP_NUM_PATH.'"';
    my $cmd= "odbedit -h $hostname -e bnqr  -c '$odbcmd'";
    #print "cmd=$cmd\n";
    ($status, $bnqr_exp_num)= send_odb_command($cmd); # checks reply
    if ($status)
    {
	print LOG "$name: error reading experiment number for bnqr \n";
	die  "$name: error reading experiment number for bnqr \n";
    }

    
    $bnqr_exp_num += 0; # numerical (for perl)
    unless ($noprint){ print "\nBNQR experiment number is $bnqr_exp_num\n";}  # already checked that response was numerical
    unless ($bnqr_exp_num > 0)
    {
	print LOG  "$name: ERROR - illegal experiment number for bnqr ($bnqr_exp_num)\n";
	die  "$name:  ERROR -  illegal experiment number for bnqr ($bnqr_exp_num)\n";
    }
	
}


print LOG "$name: experiment numbers bnmr:$bnmr_exp_num and bnqr:$bnqr_exp_num  (0=expt not selected)\n";

if($bnmr_exp_num==0 && $bnqr_exp_num==0)
{ # both 0 is illegal
   
    print  LOG "$name: error - experiment numbers for both experiments are 0 \n";
    die "$name: error - experiment numbers for both experiments are  0 \n";
}

# Get the monitor filename
$filename =  get_filename_for_shift();     
$tempfile =  $MONITOR_DIR."tempfile.txt";
#print "filename=$filename and tempfile=$tempfile\n";


#Look for an existing file for this shift
if(-e $filename)
{
    open FIN,"$filename" or die $!;  # open for reading
    unless($noprint){ print "$name: opening existing file $filename for reading\n";}
    print LOG "$name: opening existing file $filename for reading\n";

    open FOUT,"> $tempfile" or die $!;  # open tempfile for writing
    #print LOG "$name: opening temporary file $tempfile for writing\n";
    unless($noprint){ print "$name: opening temporary file $tempfile for writing\n";}


    $gotit=0; # clear flag 
    $linenum=1;
    while (<FIN>)
    {
	unless($noprint)
	{ 
	    print $linenum++;
	    print ": $_";
	}
	if (/^#/)
	{   
	    print FOUT $_ ; # copy comments 
	}
	else
	{
	    if($bnmr_exp_num > 0 || $bnqr_exp_num > 0)
	    {  
	      # Check whether this line is for current experiment number(s) ($gotit flag will be set to 1 if so)
		my @fields=split;
		unless($noprint){ print "Threshold:$fields[0];Expt num:$fields[1];  BNMR time: $fields[2]; BNQR time: $fields[3]; Time: $fields[4]\n";}
		$exp_num=$fields[1]+0; # convert to a number

		$bnmr_time = $fields[2]; # current time for bnmr
		$bnqr_time = $fields[3]; # current time for bnqr

		# This will also handle case if both are running the same experiment
		if ($bnmr_exp_num == $exp_num)
		{
		    $bnmr_time=$fields[2] + 1; # convert to  number, and increment
		    $bnmr_exp_num=0; # clear; we can stop looking for this one.
		    $gotit=1; # set flag
		}
		if ($bnqr_exp_num == $exp_num)
		{
		    $bnqr_time=$fields[3] + 1; # convert to number, and increment
		    $bnqr_exp_num=0;  # clear; we can stop looking for this one.
		    $gotit=1; # set flag
		}

		if($gotit) # flag is set; update this line in the file
		{
		    unless($noprint){  print "$name: updating line $linenum in file for experiment number $exp_num\n";}
		    print LOG "$name: updating line $linenum in file for experiment number $exp_num\n";
		    write_line($exp_num,$bnmr_time,$bnqr_time,$datestring); # change line and write to file
		    $gotit=0; # clear once line is written
		}
		else
		{ 
		    print FOUT $_ ; # copy this line unchanged 
		}
	    }
	    else
	    {
		print FOUT $_ ; # copy this line unchanged 
	    }
	} #else line is not a comment

    } # end of file

    #printf("end of file; exp_num for bnmr is $bnmr_exp_num and for bnqr $bnqr_exp_num\n");

    # Have we found (and dealt with) the experiment numbers?
    if($bnmr_exp_num > 0 || $bnqr_exp_num > 0)
    { 
	add_new_exp($bnmr_exp_num,$bnqr_exp_num); # add new experiment line(s)	# no; write a new line
    }
    close FIN;
    close FOUT;

    
    `rm $filename`; # remove the old file
    if( $!)
    {
	print LOG   "$name: failure deleting old file $filename   : $!\n";
	die  "$name: failure deleting old file $filename  : $!\n";
    } 

    `mv $tempfile $filename`; # replace with the temporary file
    if( $!)
    {
	print LOG   "$name: failure renaming $tempfile to $filename   : $!\n";
	die  "$name: failure renaming $tempfile to $filename : $!\n";
    } 
    
    unless($noprint){ print "$name:  renamed $tempfile to $filename\n"; }
    print LOG "$name: renamed $tempfile to $filename\n";

} # end of existing file
else
{ # new file
    open FOUT,"> $filename" or die $!;  # open for reading
    print LOG "$name: opening new file $filename\n";
    unless($noprint){ print "$name: opening new file $filename\n";}

    print FOUT "# Shift Monitor File first opened at $datestring\n";
    print FOUT "# Threshold(uA)   ExptNum        BNMR(min)    BNQR(min)  Time line written        Testing\n";

    add_new_exp($bnmr_exp_num,$bnqr_exp_num); # add new experiment line(s)
  
    unless($noprint){ print  "$name: successfully written new monitor file $filename\n";}
    print LOG "$name: successfully written new monitor file $filename\n";
    close FOUT;
}

# File format
# Threshold   ExptNum   BNMR(min)    BNQR(min)   Time

exit;

sub get_filename_for_shift()
{
    my @months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
    my @days = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
    my $filename;
    my $now;
#
# Test program trytime.pl tests this routine
#
    $now=time();  # get present unix time 
    my  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($now); # get present localtime
    $year+=1900; # year since 1900

    # Determine if this is AM or PM shift  
    # AM shift starts at 8.30am finishes at 8.29pm the same day
    # PM shift starts at 8.30pm finishes at 8.29am the next day


    $hour +=0; # convert to number
    $min  +=0; # convert to number
    my $mtime="AM";  # default

    # Before 8.30am is the PM shift previous day
    if ( ($hour < 8) or ( ($hour == 8) && ($min < 30)))
    {  # PM shift previous day
	$mtime = "PM";
	my $yesterday = $now - 3600*24; # get date 24 hours ago (i.e. yesterday)
	
	my ($sec2,$min2,$hour2,$mday2,$mon2,$year2,$wday2,$yday2,$isdst2) = localtime($yesterday);
	$year2+=1900; # year since 1900
	
	#print "mday=$mday and mday2=$mday2\n";
	$filename= $MONITOR_DIR.$mday2.$months[$mon2].$year2."_".$mtime.".txt";
	unless($noprint) { print "get_filename_for_shift: $mtime shift previous day;  filename is $filename\n";}
	print LOG "get_filename_for_shift: $mtime shift previous day;  filename is $filename\n";
	return $filename;
    }

    if ($hour > 20) { $mtime="PM";}  # PM same day
    if (($hour == 20) && ($min >= 30)) { $mtime="PM"; } # PM same day 


# assemble filename(s) for this shift
   
    $filename= $MONITOR_DIR.$mday.$months[$mon].$year."_".$mtime.".txt";
    
    unless($noprint) { print "get_filename_for_shift:  $mtime shift same day;  Filename is $filename\n";}
    print LOG "get_filename_for_shift:  $mtime shift same day;   filename is $filename\n";
  
    return $filename;
}


sub add_year($)
{
    # parameter $year
    # adds $year to $MONITOR_PATH and returns full path ( $MONITOR_DIR )

    # make sure there is a directory for this year
    my ($year) = @_;
    my $dir = $MONITOR_PATH.$year."/";
    my $ans;

    if (-d $dir) 
    { 
	unless ($noprint){ print "found existing directory $dir\n"; }
	return $dir;
    }
    else
    {
	`mkdir $dir`;
	if( $!)
	{
	    print LOG   "add_year : failure creating new directory \"$dir\" for year $year   : $!\n";
	    die  "add_year :  failure creating new directory \"$dir\" for year $year  : $!\n";
	}
    }
    return $dir;
    
}

sub read_epics_value($)
{
    my ($epics_path) = @_; # get the parameters
    my ($cmd,$val);
    $cmd = sprintf("caget %s", $epics_path);

    #print LOG "read_epics_value: sending command: $cmd\n";
    unless($noprint){ print "read_epics_value: sending command: $cmd\n";}
    $val=`$cmd`;
    unless( $!)
    {
	chomp $val;
	unless ($val =~/\S/)
	{ 
	    unless($noprint){ print "read_epics_value: reply is empty\n";}
	    return ("err");
	}
	unless ($val =~/$epics_path/)
	{ 
	    unless($noprint){ print "read_epics_value: reply does not contain string $epics_path\n";}
	    return ("err");
	}
	$val =~ s/$epics_path//; #remove epics path
#	print "read_epics_value: $val\n";


	$val =~ s/$epics_path//; #remove epics path
        $val =~ s/ *$//; #remove trailing spaces
        $val =~ s/^ *//; # and leading spaces
	unless($noprint){ print  "read_epics_value: read $epics_path   returning \"$val\"\n";}
	print LOG "read_epics_value: read $epics_path   returning \"$val\"\n";
	return $val;
    }
    else
    {
	unless($noprint){ print  "read_epics_value: Failure after cmd $cmd : $!\n";}
	print LOG  "read_epics_value: Failure after cmd $cmd : $!\n";
    }
    return "err";
}

sub send_ssh_cmd()
{
### REPLACED BY send_odb_cmd()
    
    # a key is supplied to access the bnqr account
    # use environment variable DAQ_HOST (in variable $DAQ_HOSTNAME)
    my $cmd= "ssh bnqr\@$DAQ_HOSTNAME -i  /home/bnmr/.ssh/id_rsa_cron /home/bnqr/online/bnqr/bin/check_exptnum";
    my $num;
    
    unless($noprint){ print "send_ssh_cmd: sending command: \"$cmd\"\n";}
    print LOG "send_ssh_cmd: sending command: \"$cmd\"\n";
    my $val=`$cmd`;
  
    unless( $!)
    {
	chomp $val;
	
	unless ($val =~/\S/) # non-blank character
	{ 
	    unless($noprint){ print "send_ssh_cmd: reply is empty\n";}
	    print LOG "send_ssh_cmd: reply is empty\n";
	    return ("err");
	}
        if  ($val =~ /not found/i)
	{
	    unless($noprint){ print  "send_ssh_cmd:  \"$val\"\n";}
	    print LOG "send_ssh_cmd: cannot read expt num from bnqr -  \"$val\"\n";
	    return ("err");
	}

	if ($val =~ /experiment number/i)
	{
	    $val =~ s/experiment number//;
	    $val =~ s/ *$//; #remove trailing spaces
	    $val =~ s/^ *//; # and leading spaces
#	    print "now val=\"$val\"\n";
	    
	    $num=get_digits($val);
	    $num=$num+0; # numerical
	    if($num == 0) # error from get_digits
	    {
		unless($noprint){ print  "send_ssh_cmd: illegal response for bnqr's experiment number \"$val\"\n";}
		print LOG "send_ssh_cmd: illegal response for bnqr's experiment number \"$val\"\n";
		return ("err");
	    } 
	}
	else
	{
	    unless($noprint){ print  "send_ssh_cmd: illegal reply \"$val\". Could not find string \"experiment number\" \n"; }
	    print  LOG "send_ssh_cmd: illegal reply \"$val\". Could not find string \"experiment number\" \n";
	    return ("err");
	}
    }
    else
    {
	unless($noprint){ print  "send_ssh_cmd: Failure after cmd $cmd : $!\n";}
	print  LOG "send_ssh_cmd: Failure after cmd $cmd : $!\n";
	return ("err");
    }
    return($num); # success
}

sub write_line($$$$)
{
    our $min_proton_current;
    our $testing;
    my $star="*";
    my $nostar="";

    # write a line to the monitor file
    my ($exp_num,$time1,$time2,$datestring) = @_; # get the parameters
    my $format="   %6.3f         %8.8d       %4.4d         %4.4d       %s    %s\n";

    unless($testing) 
    {
	printf FOUT ( $format, $min_proton_current, $exp_num, $time1,$time2, $datestring, $nostar);
	unless($noprint){ printf      ( $format, $min_proton_current, $exp_num, $time1,$time2,$datestring, $nostar);}
    }
    else
    {
	printf FOUT ( $format, $min_proton_current, $exp_num, $time1,$time2, $datestring, $star);
	unless($noprint){ printf      ( $format, $min_proton_current, $exp_num, $time1,$time2,$datestring, $star);}
    }

    return;
}

sub add_new_exp($$)
{   # add new experiment to the monitor file, i.e. add new line(s)
   
    my ($bnmr_exp_num,$bnqr_exp_num) = @_;

    unless($noprint){ print "add_new_exp: starting with experiment number for bnmr = $bnmr_exp_num and bnqr = $bnqr_exp_num\n";}

    if($bnmr_exp_num > 0 || $bnqr_exp_num > 0) # calling program should have checked that at least one is non-zero
    {	  
	if($bnmr_exp_num == $bnqr_exp_num) 
	{  # both sides are running the same expt; 1 minute for each, one line only
	    write_line($bnmr_exp_num,1,1,$datestring);
	}
	else
	{
	    if($bnmr_exp_num > 0)
	    {  # add 1 min for bnmr
		write_line($bnmr_exp_num,1,0,$datestring);
	    }
	    if($bnqr_exp_num > 0)
	    {  # add 1 min for bnqr
		write_line($bnqr_exp_num,0,1,$datestring);
	    }
	}
    }
    return;
}

sub get_digits($)
{
    my $name="get_digits";
    my ($val)=@_;
    our $noprint;
    my @fields;
    my $string = $val; # original string

    if($val =~ /\D/) # any non-digit?
    {
	#print "$name: expect numerical response for experiment number ($val). Found a non-digit\n";
	# an ODB message may be appended to the response; appear to start with a <CR>
	
	@fields = split /\s+/, $val;
	$val=$fields[0];

	#print "after split, first element is \"$val\"\n";
	unless ($val =~/\S/) # non-blank character
	{ 
	    unless($noprint){ print "$name: expect numerical response for experiment number ($string). Found a non-digit\n"; }
	    print LOG "$name: expect numerical response for experiment number ($string). Found a non-digit\n";
	    return 0;
	}
  
	if($val =~ /\D/)
	{
	    unless($noprint){ print "$name: expect numerical response for experiment number (\"$string\"). Found a non-digit after split (\"$val\")\n";} 
	    print LOG "$name: expect numerical response for experiment number (\"$string\"). Found a non-digit after split (\"$val\")\n"; 
	    return 0;
	}
	
    }
    return $val;
    
}

sub send_odb_command($)
{   # this is used for beamline bnqr only
    my ($cmd) = @_;  # input parameter
    my $name="send_command";
    my $fail=1;
    my $success=0;
    my $beamline="bnqr";
    my $expt_num;
    unless($noprint){ print "$name: sending command: \"$cmd\"\n";}
    print LOG "$name: sending command: \"$cmd\"\n";

    my $val=`$cmd`;
 		     
    unless( $!)
    {
        chomp $val;
	unless($noprint){print "$name: reply  val = \"$val\"\n";}
	print LOG "$name: reply val = \"$val\"\n";
	
	unless ($val =~/\S/) # non-blank character
	{ 
	    unless($noprint){ print "$name: reply is empty (beamline=$beamline) \n";}
	    print LOG "$name: reply is empty (beamline=$beamline) \n";
	    return ($fail);
	}
	
	if ($val =~/Cannot connect to remote host/)
	{
	    unless($noprint){ print "$name: mserver may not be running (beamline=$beamline) \n";}
	    print LOG "$name: mserver may not be running (beamline=$beamline)  \n";
	    return ($fail);
	}

	if ($val =~/not found/)
	{
	    unless($noprint){ print "$name: reply \"$val\" contains \"not found\" (beamline=$beamline) \n";}
	    print LOG "$name: illegal reply (beamline=$beamline) $val=\"$val\" \n";
	    return ($fail);
	}
    
	my $pattern= '(experiment +number +)(\w+)';
	if($val=~ /$pattern/i)
	{
	    #print "text matched; $1 and $2  \n";
	    if($2=~/\D/)
	    { 
		print LOG "$name: experiment number for beamline $beamline is not a number ($2)\n";
		unless($noprint){ print "$name: experiment number for beamline $beamline is not a number ($2) \n";}
		return ($fail);
	    }
	    $expt_num=$2+0; # numerical  
	    if($expt_num == 0)
	    {
		unless($noprint){ print  "$name: illegal response for $beamline experiment number ($expt_num)\n";}
		print LOG "$name: illegal response for $beamline experiment number ($expt_num)\n";
		return ($fail);
	    } 
	}
	else
	{
	    print LOG "$name: could not find experiment number for beamline $beamline\n";
	    unless($noprint){ print  "$name: could not find experiment number for beamline $beamline \n";}
	    return ($fail);
	}
	
	return($success,$expt_num);
    }

    print LOG "$name: failure status ($!) from cmd \"$cmd\" for beamline $beamline; reply=$val\n";
    unless($noprint){ print  "$name: failure status ($!) from cmd \"$cmd\" for beamline $beamline; reply=$val \n";}
    return ($fail);
}
