#!/usr/bin/perl
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d
 
## New parameter current run number added for BNM/QR Custom Script
# invoke this script with cmd
#             include  experiment    disable flag    hold   enable   beamline ppg    current
#              path                run_number_check  flag   logging           mode   run number
# toggle.pl     *       bnmr             n            n       y       bnmr    2a       30024
#  * = /home/bnmr/online/mdarc/perl
# toggle.pl     +       bnqr             n            n       y       bnqr    1a
#  + = /home/bnqr/online/mdarc/perl
# toggle.pl     #       musr             n            %       y       m9b     2 ** fixed at 2 **
#  # = /home/musrdaq/musr/mdarc/perl
#  % NOTE: "hold flag"  for MUSR should be linked to /runinfo/state (can be 1,2,3)
#
#
#
# Set a flag in odb (mdarc area) so that mdarc will toggle between real and test runs, deleting
# old run files in the process.
#
# $Log: toggle.pl,v $
# Revision 1.1  2013/01/21 21:47:40  suz
# initial VMIC version for cvs
#
# Revision 1.19  2004/10/19 19:13:19  suz
# some messages become debug messages
#
# Revision 1.18  2004/06/10 18:10:23  suz
# require init_check; use our; last Rev message is bad
#
# Revision 1.17  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.16  2004/04/21 19:06:57  suz
# add a space in search string to fix bug for beamlines m15/m20
#
# Revision 1.15  2004/03/29 18:27:56  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.14  2004/02/12 22:40:20  suz
# check on type 1 comes before check on write data
#
# Revision 1.13  2004/02/09 22:06:25  suz
# add changes for MUSR (BNMR's hold_flag -> MUSR's /runinfo/state)
#
# Revision 1.12  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.11  2002/04/16 20:17:12  suz
# forgot enable logging in a message
#
# Revision 1.10  2002/04/16 20:02:38  suz
# add extra parameter: enable mdarc logging
#
# Revision 1.9  2002/04/15 17:16:35  suz
# add parameter include_path and support for musr pause
#
# Revision 1.8  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.7  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.6  2001/09/14 19:34:19  suz
# add MUSR support and 2 param; toggle not allowed if run on hold; use strict;imsg now msg
#
# Revision 1.5  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.4  2001/04/30 20:03:07  suz
# Add checks on Type 2 and automatic run number flag
#
# Revision 1.3  2001/03/01 19:20:25  suz
# output sent to /var/log/midas rather than /tmp
#
# Revision 1.2  2001/02/23 20:39:21  suz
# use new subroutine open_output_file
#
# Revision 1.1  2001/02/23 18:01:08  suz
# initial version
#
#
use strict;
sub write_clicked($$);
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_PAUSED =2; # Run state is paused
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
###########################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
our ($inc_dir, $expt, $dis_rn_check, $hold_flag, $enable_logging, $beamline, $ppg_mode, $old_rn ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "toggle"; # same as filename
our $nparam = 8; # expect 8 parameters
our $outfile = "toggle.txt";
our $parameter_msg= "include_path; experiment; disable_run_number_check flag; hold flag; enable logging flag; beamline; ppg mode, run number";
################################################################################
my ($transition, $run_state, $path, $key, $status);
my $mdarc_path ;
my ($hold, $eqp_name);
my $hidden_path = "/Custom/hidden/";

$|=1; # flush output buffers

# input parameters:

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";




# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
#
unless ($old_rn)
{
    print_3($name,"FAILURE: current run number not supplied",$MERROR,1);
}
unless ($beamline)
{
    print_3($name,"FAILURE: beamline not supplied",$MERROR,1);
}
# BNM/QR custom page
# clear /custom/hidden/clicked toggle button - perlscript not in action yet until checks are done
write_clicked(0,$beamline);

unless ($dis_rn_check eq "n") 
{
        print FOUT "INFO: Automatic run numbering is DISABLED. Cannot toggle \n"; 
        odb_cmd ( "msg","$MINFO","","$name", "INFO: Automatic run numbering is DISABLED - toggle not allowed " ) ;
        die  "INFO:  Automatic run numbering is DISABLED. Toggle not allowed. \n"; 
}

unless ($ppg_mode)
{
    print_3($name,"FAILURE: ppg_mode not supplied",$MERROR,1);
}
unless ($ppg_mode =~/^[12]/)
{
    print_3 ($name, "FAILURE: invalid ppg mode ($ppg_mode) supplied", $MERROR, 1);
}

unless ( $ppg_mode =~ /^2/  )  # Type 2 experiment including MUSR 
{
        print FOUT "INFO: Toggle is presently supported only for Type 2 experiments \n"; 
        odb_cmd ( "msg","$MINFO","","$name", "INFO: toggle  is supported only for Type 2 experiments" ) ;
        die  "INFO: Toggle  is supported only for Type 2 experiments \n"; 
}
# NOTE: if ever toggling is implemented for Type 1, will have to get do_link to change the link of
#           /script/toggle/write data   to point to /logger/write data for a type 1 run
#
unless ($enable_logging eq "y") 
{
        print FOUT "INFO: Logging is disabled. Cannot toggle \n"; 
        odb_cmd ( "msg","$MINFO","","$name", "INFO: Logging is DISABLED - toggle not allowed " ) ;
        die  "INFO: Logging is DISABLED. Toggle not allowed. \n"; 
}
#
#
#
if( ($beamline =~ /bn[mq]r/i)  )
  {
    # don't allow run on hold 
    #  (problem if the run is later stopped with hold and toggle on - no run file for new run number)  
    unless ($hold_flag eq "n") 
      {
	print FOUT "INFO: Toggle not allowed if run is on HOLD \n"; 
	($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Run must be CONTINUEd before it can be toggled.  " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die  "INFO: Toggle not allowed if run is on HOLD  \n"; 
      }
  }
else
  {
    # MUSR experiments use midas PAUSE; hold_flag is /runinfo/state so will be STATE_RUNNING,STATE_STOPPED or STATE_PAUSED
    # check whether run is in progress
    if ($hold_flag == $STATE_PAUSED)
      {   # Run is paused
	print FOUT "INFO: Toggle not allowed if run is PAUSED \n"; 
	($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Run must be RESUMEd before it can be toggled.  " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die  "INFO: Toggle not allowed if run is PAUSED  \n"; 
      }
  }

#
#      determine equipment name from beamline
#
if( ($beamline =~ /bn[qm]r/i)  )
{
# BNMR experiments use equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";

## New - BNM/QR write "/custom/hidden/clicked ppgmode button" and "remember runnum"
## write from perlscript instead of status page javascript
    ($status, $path, $key) = odb_cmd ( "set","$hidden_path","remember runnum","$old_rn") ;
    unless ($status)
    { 
	print FOUT "$name: Failure from odb_cmd (set); Error setting $hidden_path remember runnum to $old_rn \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting remember runnum to  $old_rn " ) ;
	die "$name: Failure setting $hidden_path remember runnum to $old_rn  ";
    }
    write_clicked(1,$beamline); # set "/custom/hidden/clicked runmode button  (perlscript in progress)
}
else
{
# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
} 
$mdarc_path= "/Equipment/$eqp_name/mdarc/";
print FOUT "mdarc path: $mdarc_path\n";

#
# Check whether mdarc is running (after $EXPERIMENT is set up )
#
unless (mdarc_running() )   # subroutine to check if mdarc is running
{
    # no point in setting toggle

    print FOUT "Mdarc is not running. Toggle not valid. \n";
    ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Toggle valid only when mdarc is running" );     
    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
    write_clicked(0,$beamline); # clear "/custom/hidden/clicked toggle button  (perlscript in progress)
    die      "$name: No toggle action if mdarc is not running";
}

# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state != $STATE_STOPPED)
{   # Run is going

    ($status) = odb_cmd ( "set","$mdarc_path","toggle" ,"y") ;
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error setting toggle.\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting toggle" ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        write_clicked(0,$beamline); # clear "/custom/hidden/clicked toggle button  (perlscript in progress)
        die "$name: Failure setting toggle\n";
    }
    else 
    { 
        print FOUT "$name: Success - toggle bit has been set in odb\n"; 
        ($status)=odb_cmd ( "msg","$MINFO","","$name", "Toggle bit has been set in odb" ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
    }
}
else
{    # run is stopped; no action
    print FOUT "Not running. Toggle has no action. \n";
    ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Run is not in progress. Toggle has no action" );
    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
    print "Not running. Toggle has no action. \n";
}
write_clicked(0,$beamline); # clear "/custom/hidden/clicked toggle button  (perlscript in progress)
exit;


sub write_clicked($$)
{
    my $value = shift;
    my $beamline = shift;
    my $path = $hidden_path."clicked toggle button";
    my ($status,$key);
    ($status, $path, $key) = odb_cmd ( "set","$hidden_path","clicked toggle button","$value") ;

    # write_clicked is for bnm/qr only
    unless ($beamline =~ /bn[qm]r/i) {return;}
   
    unless ($status)
    { 
	print FOUT "$name: Failure from odb_cmd (set); Error setting $hidden_path clicked toggle button true \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting clicked toggle button" ) ;
	die "$name: Failure setting $hidden_path clicked toggle button";
    }
    if($value == 0) { print "$name: cleared $path\n";}
    else { print "$name: set $path\n"; }
    return; 
}

