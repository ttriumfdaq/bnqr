#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
# invoke this script like this:
#                  include path         expt     command     beamline      
# mode_check.pl /home/bnmr/online/perl  bnmr     filename     bnmr
#  e.g. mode_check.pl  /home/bnmr/online/perl  /home/bnmr/$ONLINE/modefiles/1f_defaults.odb bnmr
#
# This perlscript opens and reads a saved modefile, then runs odb cmd "ls" on all the keys
# to make sure they exist. 
# Needed because odb cmd "load" creates keys if they don't exist  
#
# Users run this file by running check_file.pl <expt> <filename> in the modefile directory
#  i.e. /home/<expt>/online/modefiles.  See AAA_README in that area for more info.
#
# $Log: mode_check.pl,v $
# Revision 1.2  2014/01/09 23:36:24  suz
# update example
#
# Revision 1.1  2013/01/21 21:47:40  suz
# initial VMIC version for cvs
#
# Revision 1.3  2012/10/11 20:26:35  suz
# add check on arrays
#
# Revision 1.2  2010/11/08 23:51:49  suz
# add an anchor
#
# Revision 1.1  2005/09/27 20:04:41  suz
# original: checks modefiles
#
#
use strict;
############### G L O B A L S ####################################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#########################################################
# parameters needed by init_check.pl (required code common to perlscripts) :
our ($inc_dir, $expt, $filename, $beamline ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "mode_check"; #same as filename
our $parameter_msg = "include_path, experiment, use path, command,   beamline ";
our $outfile = "mode_check.txt";
our $nparam = 4;  # no. of input parameters

##################################################################

$|=1; # flush output buffers

my ($transition, $run_state, $path, $key, $status);
my $debug=$TRUE;
my @out_array;
my ($lineno,$line);
my ($my_path, $my_key, $tmp);
my $error_count=0;
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
print "mode_check: inc_dir=$inc_dir\n";
require "$inc_dir/odb_access.pl"; 


# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";
print "\n";
print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt;  filename=\"$filename\"  beamline = $beamline \n";
print   "$name starting with parameters:  \n";
print   "Experiment = $expt;  filename=\"$filename\"  beamline = $beamline \n";
print "\n";
# output from the command will be sent to FOUT
# because this is for use with the browser and STDOUT and STDERR get set to null
#

my $array_flag=0;
my $array_cntr=0;

unless (open (FILE, $filename))
{
    print_2($name, "can't open file $filename ($!)",$MERROR);
}
$lineno = 1;
while (<FILE>) 
{
    if($debug)
    {
	print "\nWorking on line ";
	print $lineno++;
    }

    $line=$_;

    chomp $line;
    if($debug){ print ": \"$line\"\n"; }

    unless( /\w/ )
    {
	if($debug) { print ".... skipping empty line...\n"; }  
    }
    else
    {
	if ( /^\[\D/)
	{ # found a directory
	    $line=~ s/[\[\]]//g;  # remove square brackets
	    $my_path = $line;     # it is the path
	    print "\nFound path for the following key(s) : $my_path\n\n";  
	}
	elsif ( /^\[\d+\]/ )
	{ # found an array value
	    unless ($array_flag)
	    { 
		print "Found array value but array_flag is not set; Last key $my_key should be an array\n";
		print "Array length may be wrong !!\n";
		goto err;
	    }
	    $array_cntr--;  # checks array length is as expected 
	    if($debug){ printf ("array_cntr is now $array_cntr\n"); }
	    if ($array_cntr ==0 ){ $array_flag = 0;} # clear flag
	    if($debug){ print "Skipping an array value: $_"; }   	
	}
	else
	{ # found a key
	    if ($line =~  /(\w\[(\d)+\])/)
	    {   # careful of STRING[4] which denotes array of strings,  and STRING : [32] this is a string
		if($debug){print "found a key with square brackets  \"$1\" denoting an array of length $2 in key  in \"$line\" \n";}
		$array_flag = 1;
		$array_cntr = $2;
	    }
	    else
	    { print "found a key in \"$line\n";}

	    ($my_key)=split (/=/,$line); # take the string prior to "="
	    if($debug){ print "my key: $my_key\n"; }
	    
	    ($status) = odb_cmd ( "ls",$my_path,$my_key );
	    unless  ($status) 
	    { 
		print "\n *****  Failure on ls \"$my_path\/$my_key\" from odb_cmd ****\n"; 
		$error_count++;	   
		print FOUT "\n *****  Failure on ls \"$my_path\/$my_key\" from odb_cmd ****\n"; 
	    }
	    
	    if($array_flag)
	    { 
		if($debug){ print "expecting an array...";}
	    }

	    # make sure that both key and answer are arrays or not
	    if ($ANSWER =~  /\n/)
	    {  # this is an array
	
		my @array=split (/\n/,$ANSWER);
		$len = $#array;
		print "this is an array  of length $len\n"; 
		if($len != $array_cntr)
		{
		    print "Expected array length $array_cntr does not agree with array length in ODB ($len)\n";
		    goto err;
		}
	    

		unless ($array_flag)
		{ 
		    print "\n** error - key \"$my_key\" in file is NOT an array, but this key IS an array in ODB\n";
		    goto err;
		}
	    }
	    else
	    {  # key is NOT an array
		if($array_flag)
		{
		    print "\n** error - key \"$my_key\" in file IS an array, but this key is NOT an array in ODB\n";
		    goto err;
		}
	    }

	    
	    
	}
    }
    
}
close FILE;
#
unless($error_count)
{
    print "\n   SUCCESS File \"$filename\" has been checked \n";
    print FOUT "\n   SUCCESS File \"$filename\" has been checked \n";
    print "\ndone\n";
    print FOUT "done\n";
    exit;
}
 err:
    print "\n ***** Check path and key name(s) in file \"$filename\"\n";
print " ***** DO NOT LOAD THIS FILE until the problem is fixed !! \n";
print FOUT "\n ***** Check path and key names in file \"$filename\"\n";
print FOUT " ***** Do not load this file until the problem is fixed!! \n";

print "\ndone\n";
print FOUT "done\n";
exit;
