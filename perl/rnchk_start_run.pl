#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
 
# invoke this script with cmd e.g.
#                             include             exp  disable enable   beam  ppg
#                             path                     runnum  prestart line  mode
#                                                      check   rn check
# rnchk_start_run.pl /home/bnmr/online/mdarc/perl bnmr   n       y       bnmr   1f
# rnchk_start_run.pl /home/bnqr/online/mdarc/perl bnqr   n       y       bnqr   1f
#
#
# calls get_next_run_number.pl to check the next valid run number
# then starts the run
#

# Revision 1.1  2001/04/30 19:53:04  suz
# Initial version
#
#
#
use strict;
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
##########################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
our ($inc_dir, $expt,$dis_rn_check, $enable_pre_rncheck, $beamline, $ppg_mode ) = @ARGV;# input parameters
our $len = $#ARGV; # array length
our $name = "rnchk_start_run"; # same as filename
our $parameter_msg = "include_path, experiment , flag (disable automatic run numbering), flag(enable run number precheck)  beamline, ppg mode";
our $nparam = 6;  # no. of input parameters
our $outfile = "rnchk_start_run.txt";
################################################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my ($mdarc_path, $eqp_name);
my ($cmd);
my $debug=$FALSE;


$|=1; # flush output buffers


# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; Flag to disable run number check = $dis_rn_check; beamline = $beamline \n";
#
#
# automatic run numbering and run number precheck  must be enabled
#

# check whether run is in progress

($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state != $STATE_STOPPED)
{   # Run is going - nothing to do
    print_3($name,"Run is in progress. Can only start run when it is stopped",$MINFO,$DIE);
}

if (($dis_rn_check eq "n")  &&  ($enable_pre_rncheck  eq "y") )
{
    unless ($ppg_mode)
    {
	print_3($name,"FAILURE: beamline not supplied",$MERROR,1);
    }
    unless ($ppg_mode =~/^[12]/)
    {
	print_3 ($name, "FAILURE: invalid ppg mode ($ppg_mode) supplied", $MERROR, 1);
    }
#
#      determine equipment name from beamline
#
    if($beamline =~ /bn[qm]r/i)   
    {
# BNMR experiments use equipment name of FIFO_ACQ
	$eqp_name = "FIFO_ACQ";
    }
    else
    {
# All MUSR experiments use equipment name of MUSR_TD_ACQ
	$eqp_name = "MUSR_TD_ACQ";
    } 
    $mdarc_path= "/Equipment/$eqp_name/mdarc/";
    print FOUT "mdarc path: $mdarc_path\n";
    
    
# now launch get_next_run_number
    print FOUT "Calling get_next_run_number with parameters: $inc_dir  $expt 0 $eqp_name 0 $beamline $ppg_mode\n";
##    $cmd = sprintf("/home/bnmr/online/mdarc/perl/get_next_run_number.pl %s 0 %s 0 %s",$expt,$eqp_name,$beamline,$ppg_mode);
    $cmd = sprintf("$inc_dir/get_next_run_number.pl %s %s 0 %s 0 %s %s",
		   $inc_dir,$expt,$eqp_name,$beamline,$ppg_mode);
    
    print "$name: sending command: $cmd\n";
    print FOUT "$name: sending command: $cmd\n";
    print      "Output from get_next_run_number is in /home/midas/musr/log/$beamline/get_run_number.txt\n";
    print FOUT "Output from get_next_run_number is in /home/midas/musr/log/$beamline/get_run_number.txt\n";
    $status=system "$cmd";
    if( $status == 0)
    {
	print FOUT "Success after system command, status=$status\n";
	print "Success after system command, status=$status\n";
    }
    else
    {
	print FOUT "Failure after system command, status=$status\n";
	print      "Failure after system command, status=$status\n";
	if ($status == -1 )
	{      # there is a message in errno
	    print FOUT "Error message: $!\n"; # errno is put in $! for system cmd
	    print      "Error message: $!\n";
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "Failure return from get_next_run_number due to: $! " ) ;
	    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
	}
	else
	{
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "Failure return from get_next_run_number (status=$status) " ) ;
	    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
	}
	print_3($name,"Failure from run number checker. Correct error and retry.",$MERROR,$DIE);
    }
}


else
{
    print FOUT "INFO: Automatic run numbering is DISABLED or run number pre-check is DISABLED \n"; 
    odb_cmd ( "msg","$MINFO","","$name", "Automatic run number check at start of run is DISABLED" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
}

#   start the run
#print FOUT  "Now attempting to start the run \n";

#($status) = odb_cmd ( "start" ) ;
#unless ($status) { exit_with_message($name); }

#

write_time($name, 3);
exit;








