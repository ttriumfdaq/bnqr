#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
 
# invoke this script with cmd e.g.
#         include                      experiment  ppg    tunename
#         path                                     mode
#
# save_tune.pl /home/bnqr/vmic_online/perl  bnqr        20      my_tune

# Uses template file 20_template.odb

use strict;
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
##########################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
our ($inc_dir, $expt, $ppg_mode, $tune, $nparam, @tune_params ) = @ARGV;# input parameters
our $beamline;
our $len = $#ARGV; # array length
our $name = "test_pl"; # same as filename
our $parameter_msg = "include_path, experiment , ppg mode, tune_name ";
our $nparam = 4;  # no. of input parameters
our $outfile = "save_tune.txt";
################################################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my ($mdarc_path, $eqp_name);
my ($cmd);
my $debug=$FALSE;
my ($tunefile, $template, $lineno);
my @out_array;
my ($lineno,$line);
my ($my_path, $my_key, $tmp);
my $error_count=0;
my $array_flag=0;
my $array_cntr=0;


if($expt =~ /bn[qm]r/i)   
{
    $beamline = $expt;
# BNM/Q R experiments use equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";
}

$|=1; # flush output buffers



# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

print  "$name starting with parameters:  \n";
print   "Experiment = $expt; ppg_mode= $ppg_mode; tune name = $tune \n";

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; ppg_mode= $ppg_mode; tune name = $tune \n";
#
#
print  "parameters are :\n";
print  "inc_dir = $inc_dir, expt = $expt, ppg_mode = $ppg_mode, nparams= $nparam\n";
print  "tune params:  @tune_params\n";

print FOUT "parameters are :\n";
print FOUT "inc_dir = $inc_dir, expt = $expt, ppg_mode = $ppg_mode, nparams= $nparam\n";
print FOUT "tune params:  @tune_params\n";

exit;

# perl module must end with a 1
1; # return a good status
#


