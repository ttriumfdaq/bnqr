# constants used by many of the bnmr/bnqr perlscripts
#
use warnings;
use strict;
######### G L O B A L S ##################
our $EXPERIMENT;
our @ARRAY;
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#########################################################
# Globals needed by  this file and by run_number_check:
our $MAX_TEST = 30499; 
our $MIN_TEST = 30000;

# BNMR values
our $MAX_BNMR = 44499; 
our $MIN_BNMR = 40000;
#other beamlines
our $MIN_M20  =     0;
our $MAX_M20  =  4999;
our $MIN_M15  =  5000;
our $MAX_M15  =  9999;
our $MIN_M9B  = 15000;
our $MAX_M9B  = 19999;
our $MIN_DEV  = 20000;
our $MAX_DEV  = 29999;
our $MIN_BNQR = 45000;
our $MAX_BNQR = 49999;
 
# The following only used if $check_for_holes is true.
our ( $LIST_OF_HOLES, $CHECK_FOR_HOLES);
our $FOUND_HOLE = $FALSE;  # flag indicates if there are holes in the run numbers  
#########################################################################

our  $LOGFILEPATH = "/data1/$BEAMLINE/log";
