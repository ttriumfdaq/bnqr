#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d

use warnings;
use strict;
sub print0($);
sub print1($);
# this routine cycles the log file monitor.log. 
#  monitor.log_5 is deleted, others are shifted up a number, monitor.log-> monitor.log_1
# oldest is monitor.log_5
# newest is monitor.log_1
# current is monitor.log
#
our ($noprint) = @ARGV; # input parameters
$noprint += 0; #input parameter to suppress printing for cron job

my $MONITOR_PATH = "/isdaq/data1/bnmr/mon/"; # the current year will be added to this
my $name="cycle_logfile";

my $now=time();  # get present unix time 
my  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($now);
$year+=1900; # year since 1900
my $MONITOR_DIR = $MONITOR_PATH.$year."/";
my $logfilename="monitor";
my $logfile = $MONITOR_DIR.$logfilename.".log";
my $file;

my $cyclelog =  $MONITOR_DIR."cycle_files.log"; # logfile from this script
my $i;
my @postfix = ("_1","_2","_3","_4","_5");
my $len= $#postfix ;
my $oldest_file=$logfile.$postfix[$len];


# open cycle_files.log file

open LOG,"> $cyclelog" or die $!;
print0 "Opened cycle log file $cyclelog for write\n";



unless (-e $logfile)  # look for monitor.log
{
    print1  "$name: Cannot find $logfile; nothing to do\n";
    exit;
}

if (-e $oldest_file)
{
    print1 "$name: found $oldest_file; removing it\n";
    `rm  $oldest_file`;
    if( $!)
    {
	print LOG  "$name: failure deleting file  $oldest_file  : $!\n";
	die  "$name: failure deleting file  $oldest_file  : $!\n";
    } 
}
$len--;  

while ($len >= 0)
{
    $oldest_file = $logfile.$postfix[$len];
   
    if (-e $oldest_file)
    {
	print1 "found $oldest_file; renaming it to  $logfile$postfix[$len+1]\n";
	`mv $oldest_file  $logfile$postfix[$len+1]`;
	if( $!)
	{
	    print LOG  "$name: failure moving file  $oldest_file to $logfile$postfix[$len+1] : $!\n";
	    die  "$name: failure moving file  $oldest_file to $logfile$postfix[$len+1] : $!\n";
	} 
    }
    else
    {
	print1 "$name: did not find $oldest_file\n";
    }
    $len--;
}
$i=3;
while ($i > 0)
{
    
    if (`/usr/sbin/lsof $logfile`)
    {
	print1 "$name: $logfile is open (count $i)... sleeping 2s\n";
	sleep(2); # sleep 2s 
    }
    else
    {
	`mv $logfile $logfile$postfix[0]`;
	if( $!)
	{
	    print LOG   "$name: failure renaming file  $logfile to  $logfile$postfix[0] : $!\n";
	    die  "$name: failure renaming file  $logfile to  $logfile$postfix[0] : $!\n";
	} 
	print1 "$name: renamed $logfile to  $logfile$postfix[0]\n";
	last;
    }
   
    $i--;
}    
exit;


sub print0($)
{   # print to screen if noprint is false.
    my ($string) = @_;
    our $noprint;
    unless ($noprint) { print "$string\n"; }
    return;
}

sub print1($)
{  # print to LOG and screen if noprint is false.
    my ($string) = @_;
    our $noprint;
    unless ($noprint) { print "$string"; }
    print LOG  "$string";
    return;
}
