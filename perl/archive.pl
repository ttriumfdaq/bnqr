#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
# archive.pl
# archive any  real files that aren't in the archive already;
# if the file is already archived, it checks the sizes are the same.
# Reads archived_dir and saved_dir from odb
#
# NOTE: this script is executed by a user, not by the browser.
# so the include directory is not the first parameter.
#
# invoke this script with cmd:
#             experiment    archive  beamline run number
#                                             start  stop
# archive.pl    bnmr          Y          bnmr   40100  40150
# 
#     Start/stop run number is optional - used if archive_flag = Y
#     If archive_flag = N just produces a list; does not copy any files
#
# outputs:
#   $status        1 for SUCCESS, 0 for FAILURE
#
# based on run_number_check.pl, uses validate
#
# 
# $Log: archive.pl,v $
# Revision 1.2  2013/04/08 22:25:53  suz
# Changes after debugging new VMIC code
#
# Revision 1.1  2013/01/21 21:47:40  suz
# initial VMIC version for cvs
#
# Revision 1.10  2004/06/09 21:53:28  suz
# update, and change use vars to our
#
# Revision 1.9  2004/06/09 21:36:22  suz
# add check on beamline
#
# Revision 1.8  2004/04/21 19:12:46  suz
# add a space in search string to fix bug for beamlines m15/m20
#
# Revision 1.7  2004/03/29 18:26:04  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.6  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.5  2003/01/08 18:39:13  suz
# add polb
#
# Revision 1.4  2002/07/18 19:16:20  suz
# fix bug
#
# Revision 1.3  2002/07/17 00:57:46  suz
# change cpbnmr directory
#
# Revision 1.2  2002/04/16 19:13:38  suz
# many improvements
#
# Revision 1.1  2001/12/07 22:09:57  suz
# original: copies all real msr files to archive unless archived already
#
#
#
use strict;
######### G L O B A L S ##################
our $EXPERIMENT;
our @ARRAY;
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#########################################################
# Globals needed by  this file and by run_number_check:
our $MAX_TEST = 30499; 
our $MIN_TEST = 30000;

# BNMR values
our $MAX_BNMR = 44499; 
our $MIN_BNMR = 40000;
#other beamlines
our $MIN_M20  =     0;
our $MAX_M20  =  4999;
our $MIN_M15  =  5000;
our $MAX_M15  =  9999;
our $MIN_M9B  = 15000;
our $MAX_M9B  = 19999;
our $MIN_DEV  = 20000;
our $MAX_DEV  = 29999;
our $MIN_BNQR = 45000;
our $MAX_BNQR = 49999;
 
# The following only used if $check_for_holes is true.
our ( $LIST_OF_HOLES, $CHECK_FOR_HOLES);
our $FOUND_HOLE = $FALSE;  # flag indicates if there are holes in the run numbers  
#########################################################################

use lib "/home/bnmr/vmic_online/perl";  ####  change this if necessary ####
require "odb_access.pl";
require "run_number_check.pl";
$|=1; # flush output buffers


my ($expt,  $archive_flag, $beamline, $rn_start, $rn_stop)    = @ARGV ; # get the parameters
my ( $saved_dir, $archived_dir);
my $nparam = 3; # no. required input parameters (2 optional)
my $parameter_msg = "experiment, archive (Y or N)  beamline";
my $outfile = "archive.txt";
my $run_type = "REAL";
my ($max_run, $min_run, $max_real, $min_real);
my ($pattern, $pattern2, $message, $status);
my $name = "archive";
my $process_id;
my @files;
my ($last_run,$most_recent_run);
my ($len,$next_run_number,$nextname,$age,$most_recent_run);
my $debug=0;
my ($path,$key,$eqp_name,$mdarc_path);


if ($DEBUG){$debug=1;}   # global debug

# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

# can't check is script is running already unless there's an experiment; can't msg either
unless ($expt) {    die  "$name:  FAILURE -  No experiment supplied \n"; }

# send msg only if $EXPERIMENT is defined
if ($expt) { $EXPERIMENT = $expt; } # for msg

# check if another copy of this perlscript is running
open PS, "ps -efw |";
while (<PS>) 
{
#    print "$_";
    if (/[\s]([\d]+)[\s].*perl.*$name.pl.*$expt/)
    {
	$process_id = $1;
	print "$expt is running  with PID $process_id\n" ;
	if ($$ == $process_id)
	{ 
	    print " this is my process\n";
	}
	else 
	{ 
	    print_3 ($name," FAILURE: a copy of $name is already running with PID: $$",$MERROR,$DIE);
	}
    }
}
close(PS) ;


# Check the parameters:
#
#
$nparam = $nparam + 0;  #make sure it's an integer
$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len >= $nparam ) # need at least $len parameters
{
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 
    print  " Run number start and stop values are optional; default = all run numbers\n";
    print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
    print FOUT "   Supplied parameters: @ARGV\n";
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
    print FOUT " Run number start and stop values are optional; default = all run numbers\n";
 # only msg if there's an experiment
    if ($expt) 
    { 
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }   
    die  "$name:  FAILURE -  Too few parameters supplied ";
}
if ($expt) { $EXPERIMENT = $expt; }
else 
{ 
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  Invalid experiment supplied: $expt \n";
    die  "$name:  FAILURE -  Invalid experiment supplied ";
}

# now open the output file since $beamline is defined
$outfile=sprintf("%s/%s",$beamline,$outfile);

# Output will be sent to file given by $outfile
# now open the output file
open_output_file($name, $outfile, $process_id);

print  " === $name ===\n";
print  "        starting with input parameters:\n";
print  " Expt: $expt;  archive flag = $archive_flag; beamline = $beamline\n";
print FOUT " === $name ===\n";
print FOUT "        starting with input parameters:\n";
print FOUT " Expt: $expt; archive_flag: $archive_flag; beamline = $beamline\n";

unless ($rn_start eq "")
{
    print FOUT " Optional parameters: Run number start: $rn_start";
    unless ($rn_stop eq "") { print FOUT ";Run number stop: $rn_stop\n";}
    else { print FOUT "\n";}
}

#print FOUT "MAX_BNMR = $MAX_BNMR\n";
unless ($beamline) 
{
    print FOUT "$name: FAILURE - no beamline has been supplied\n";
    die "$name: FAILURE - no beamline has been supplied\n";
}
unless ($beamline=~/\D/) 
{ 
    die "$name: Invalid beamline $beamline\n"; # must contain a non-digit
}
if ($archive_flag =~ /y/i  )
{
    unless ($rn_start eq "")
    {
	print " Optional parameters: Run number start: $rn_start";
	if ($rn_start =~ /\D/ ||$rn_start > 49999 ) 
	{ die "\nInvalid start value\n"} 
	
	unless ($rn_stop eq "") 
	{
	    print  "; Run number stop: $rn_stop\n";
	    if ($rn_stop =~ /\D/ ||$rn_stop > 49999 ) 
	    { die "Invalid stop value\n"} 
	    if ($rn_stop < $rn_start) { print "$name: invalid parameters: stop value less than start value\n"; }
	}
	print "\n";
    }
    else
    {
	print "Saved runs will be checked and a list generated; no files will be archived\n";
	print FOUT "Saved runs will be checked and a list generated; no files will be archived\n";
    }
}

$CHECK_FOR_HOLES = 0; 

#
#      determine equipment name from beamline
#
if( ($beamline =~ /bnmr/i)  )
{
# BNMR experiments use equipment name of FIFO_ACQ
$eqp_name = "FIFO_ACQ";
}
else
{
# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
} 

#$mdarc_path= "/Equipment/$eqp_name/mdarc/";
#print FOUT "mdarc path: $mdarc_path\n";

#
#    
#              Read parameters from the odb
#
#
#
# run_number_check checks these dirs are the same for 1 and 2
# 
if( ($EXPERIMENT =~ /bnmr1/i)  )
{
# Type 1 Saved directory 
#   Read the  MLOGGER saved data directory from odb
    ($status, $path, $key) = odb_cmd ( "ls"," /logger",  "data dir " ) ;
    unless ($status) 
    { 
	print FOUT "$name: Error reading saved data directory from for Midas logger odb\n";
	die "$name: Error reading Midas logger saved data directory from odb";
    }
    ($saved_dir, $message) = get_string ($key);
    print FOUT "$name: after get_string, saved_dir=$saved_dir\n";
    unless ($message eq "") { print FOUT "         and message=$message\n"; }
}

else
{
#
# Type 2 Saved directory 
#   Read the  MDARC saved data directory from odb
    ($status, $path, $key) = odb_cmd ( "ls"," /Equipment/$eqp_name/mdarc/",  "saved_data_directory " ) ;
    unless ($status) 
    { 
	print FOUT "$name: Error reading Mdarc saved data directory from odb\n";
	die "$name: Error reading Mdarc saved data directory from odb";
    }
    ($saved_dir,$message) = get_string ( $key);
    print FOUT "$name: after get_string, saved_dir=$saved_dir\n";
    unless ($message eq "") { print FOUT "         and message=$message\n"; }

}

# get the archive dir as well
#   Read the  MDARC archived data directory from odb
    ($status, $path, $key) = odb_cmd ( "ls"," /Equipment/$eqp_name/mdarc/",  "archived_data_directory " ) ;
    unless ($status) 
    { 
	print FOUT "$name: Error reading Mdarc archived data directory from odb\n";
	die "$name: Error reading Mdarc archived data directory from odb";
    }
    ($archived_dir,$message) = get_string ( $key);
    print FOUT "$name: after get_string, archived_dir=$archived_dir\n";
    unless ($message eq "") { print FOUT "         and message=$message\n"; }


unless ( $saved_dir )
{
    print FOUT "$name: FAILURE - saved_dir is an empty string \n";
    die "$name: FAILURE - saved_dir is an empty string\n";
    
}
unless ( $archived_dir )
{
    print FOUT "$name: FAILURE - archived_dir is an empty string \n";
    die "$name: FAILURE - archived_dir is an empty string\n";
    
}
print "Saved_dir: $saved_dir; Archived_dir: $archived_dir\n";
print FOUT "Saved_dir: $saved_dir; Archived_dir: $archived_dir\n";

# easier to chdir for glob 
    unless ( chdir ("$saved_dir"))
    {
        die "$name: FAILURE - cannot change directory to $saved_dir\n";
    }

# get the maximum and minimum run numbers for this beamline
if  ( $beamline =~  (/bnmr/i) )
{
    $max_real  = $MAX_BNMR + 0 ;      # integer
    $min_real  = $MIN_BNMR + 0 ;
    print FOUT "beamline is bnmr, max real run = $max_real, min_run = $min_real\n";
}
elsif (  $beamline =~  (/m20/i) )
{
    $max_real  = $MAX_M20 + 0 ;      # integer
    $min_real = $MIN_M20 + 0 ;            
}
elsif (  $beamline =~  (/m15/i) )
{
    $max_real  = $MAX_M15 + 0 ;      # integer
    $min_real  = $MIN_M15 + 0 ;            
}
elsif (  $beamline =~  (/m9b/i) )
{
    $max_real  = $MAX_M9B + 0 ;      # integer
    $min_real  = $MIN_M9B + 0 ;            
}
elsif (  $beamline =~  (/dev/i) )
{
    $max_real  = $MAX_DEV + 0 ;      # integer
    $min_real = $MIN_DEV + 0 ;            
}
elsif (  $beamline =~  (/bnqr/i) )
{
    $max_real  = $MAX_BNQR + 0 ;      # integer
    $min_real = $MIN_BNQR + 0 ;            
}
else 
{ 
    print FOUT "$name: beamline $beamline is not supported\n";
    die "$name: beamline $beamline is not supported\n";
}
print FOUT "beamline is $beamline, max real run no = $max_real, min real run no = $min_real\n";

if ( $run_type =~ /test/i)
{      # for test  (ignores $beamline)
    $max_run = $MAX_TEST + 0 ; # integer
    $min_run = $MIN_TEST + 0 ; # integer        
}
elsif (  $run_type =~ /real/i)
{     # for real    
    $max_run = $max_real ; # integer
    $min_run = $min_real ; # integer  
}
else
{
    print FOUT "$name: FAILURE - Invalid run type $run_type supplied\n";
    die "$name: FAILURE - Invalid run type $run_type supplied\n";
  
}

if ($max_run <= 0)
{
    print FOUT "$name: FAILURE - Invalid value for maximum run number ($max_run)\n";
    die "$name: FAILURE - Invalid value for maximum run number ($max_run)\n";
}


# as a precaution (!) check for run files that are not of the correct format or are
# out of range for this beamline.
($status, $message) = validate_files($saved_dir, $beamline, $max_real, $min_real ); 
unless ($status) 
{
    print FOUT "$name: error returned by validate_files\n";
    die "$name: error returned by validate_files\n";
}
    

unless ($beamline =~ /m20/i)
{
    $pattern = sprintf ("%06d.msr* ",$min_run,$min_run); # includes .msr files only
    $pattern =~s/0+\./*\./g; # substitute e.g. "00." by "*.", all occurences

    $pattern2=   sprintf ("%06d.msr* %06d.mid",$min_run,$min_run); # includes .msr and .mid files
    $pattern2 =~s/0+\./*\./g; # substitute e.g. "00." by "*.", all occurences
}
else
{  # m20: minimum value is 0 - eliminate test runs by requiring two leading zeroes
    $pattern = sprintf"00*.msr*",$min_run,$min_run ;
    $pattern2 = sprintf"$saved_dir/00*.msr* $saved_dir/00*.mid",$min_run,$min_run ;
}

print FOUT  "$name:  search pattern for glob = $pattern2\n";
if ($debug) {print   "$name:  search pattern for glob = $pattern2\n";}
@files = glob($pattern2); 
$len = $#files; # array length  ( Note: no. of files found = len + 1 )
#print FOUT "$name: INFO - no. of saved files found len = $len\n";

if($len < 0)     
{  
    print FOUT "$name: INFO - no saved files were found for $run_type runs in $saved_dir\n"; 
    return ($SUCCESS, $min_run); # return minumum run number
}

else 
{
    # produce a list that shows status of saved files (e.g. whether  mid files have all been converted to msr) 
    $status  = check_files_converted( $saved_dir, $archived_dir, @files);
}
unless ($archive_flag =~ /y/i  )
{ 
    print "Archive flag is off; ending\n";
    print FOUT "Archive flag is off; ending\n";
    exit;
 }

# Archive any files missing from archive
print FOUT  "$name:  search pattern for glob = $pattern\n";
if ($debug) {print   "$name:  search pattern for glob = $pattern\n";}

@files = glob($pattern); 
$len = $#files; # array length  ( Note: no. of files found = len + 1 )
#print FOUT "$name: INFO - no. of saved files found len = $len\n";

if($len < 0)     
{  
    print FOUT "$name: INFO - no saved files were found for $run_type runs in $saved_dir\n"; 
    die "$name: INFO - no saved files were found for $run_type runs in $saved_dir\n"; 
}

else 
{
    # archive files goes through the files, archiving any that are not in the archive
    $status  = archive_files($min_run, $archived_dir, $rn_start, $rn_stop, @files);
    unless ($status){ print "$name: Failure returned by archive_files\n";}
}

exit;
#############################################################################

sub archive_files( $min_run, $archived_dir,  $rn_start, $rn_stop, @files )

##############################################################################
{
#  Checks all files are in archive; archives any that aren't
#  Inputs
#     min run       minimum run (needed for check for holes)
#     archived_dir   archive directory
#     @files        sorted array of run files
#     
### Note: if > 1 parameter,  array comes last in parameter list otherwise have 
#   to pass by reference 
###
#  
#  Outputs
#     $status


    my  ($min_run,  $archived_dir, $rn_start, $rn_stop, @files ) = @_ ; # get the parameter(s)
    my (@sorted_files,  $temp);
    my ($len, $first_run, $last_run);
    my $name = "archive_files";
    my ($most_recent_run);
    my $debug=0;
    my $cmd;
    my ($index,$choice,$type,$size,$size2,$saved_file,$archived_file);
    my ($end,$k, $flag1,$flag2);

    print FOUT  " $name starting with min run=$min_run  archived_dir: $archived_dir\n";
    print FOUT  "   and rn_start =$rn_start, rn_stop =$rn_stop \n";
    print FOUT  " and with Array :  @files \n"; 
    print    " $name starting with min run=$min_run  archived_dir: $archived_dir\n";
    print    "   and rn_start =$rn_start, rn_stop =$rn_stop \n";
    
   
    if ($DEBUG){$debug=1;}   # global debug
    
# Find first and last run numbers
    
    @sorted_files = sort {$a <=> $b } @files;
    if($debug)
    {
        print FOUT  "\n$name:  list of sorted files:\n";
        print FOUT  "@sorted_files\n";
    }
    $len = $#sorted_files; # array length   ( Note: no. of files found = len + 1 )
    
    
    $_ =  $sorted_files[$len];
    ($temp) = split /\./;
    $last_run = $temp+0 ; # make sure it's integer not string
    
    $_ =  $sorted_files[0];
    ($temp) = split /\./;
    $first_run = $temp +0 ; # make sure it's integer not string
    print FOUT  "$name: first & last saved runs are $first_run & $last_run\n";

    print FOUT "$name: CHECK_FOR_HOLES = $CHECK_FOR_HOLES\n";
    ($most_recent_run) = check_date (@sorted_files);
    if ($CHECK_FOR_HOLES)  
    {
        print FOUT "$name: calling check_for_holes\n";
        if ($first_run = $last_run ) { $first_run = $min_run;} # if only one run make sure there is no gap
        check_for_holes( $first_run, $last_run, @sorted_files );
    }
    if ($FOUND_HOLE)
    {
	print    "$name: ** WARNING: Run_number_check has found holes in the sequence of saved files for REAL run numbers  ** \n"; 
	print FOUT   "$name: ** WARNING: Run_number_check has found holes in the sequence of saved files for REAL run numbers  ** \n";
	print "$name: $LIST_OF_HOLES\n";
	print FOUT "$name: $LIST_OF_HOLES\n";
    }
    if($debug) { print ("archived_dir $archived_dir\n"); }
#   see if archived directory exists
    unless (-d $archived_dir)
    {
	print FOUT  "$name: archived directory $archived_dir does not exist or is not a directory \n";
	print  "$name: archived directory $archived_dir does not exist or is not a directory \n";
	return ($FAILURE);
    }


#   set defaults for while loop
    $index=0; 
    $end=$len;
    $flag1=$flag2=0;

# look for $rn_start & $rn_stop  (if non-zero) in array sorted_files
    unless ($rn_start eq "")
    {   
	
	$k=0;
	# loop over all files 
	while ($k < $len )   # $len = total no. of files
	{
	    unless ($rn_start eq "")
	    { # look for first file
		if ($sorted_files[$k] =~ /$rn_start/) 
		{
		     print "found $rn_start in $sorted_files[$k] at index $k\n";
		    $flag1=1;
		    $index = $k; 
		}
	    }
	    unless ($rn_stop eq "")
	    {         # look for last file
		if ($sorted_files[$k] =~ /$rn_stop/) 
		{ 
		    $end = $k; 
		    print "found $rn_stop in $sorted_files[$k] at index $k\n";
		    $flag2=1;
		    last;
		}
	    }
	    else { last;}
	    $k++;
	}
	# check we have found start and stop runs if defined
	if($debug){ print "Start index $index ; stop index  $end\n"; }
	$end++; # add one as loop is " < $end"
	unless($flag1) 
	{ 
	    print FOUT "Start run $rn_start not found\n";
	    die "Start run $rn_start not found\n";
	}
	unless  ($rn_stop eq "")
	{
	    unless($flag2) 
	    { 
		print FOUT "End run $rn_stop not found\n";
		die "End run $rn_stop not found\n";
	    }
	}

	
    } #$rn_start eq 0


    if($debug){ print "Start index $index ; stop index  $end\n"; }

    # main loop over all (selected) files 
    while ($index < $end)   # (defaults $index=0 $end=$len) 
    {
	

# see if there is already a file archived for this run
	
	$archived_file = sprintf("%s/%s",$archived_dir,$sorted_files[$index]);
	$archived_file =~ s/\/\//\//;  # remove any // in path
	if ($debug){print ("archived_file: $archived_file\n");}
	
	$saved_file = sprintf("%s/%s",$saved_dir,$sorted_files[$index]);
	$saved_file =~ s/\/\//\//; # remove any // in path
	$size = (-s $saved_file);
	if($debug){print ("saved_file $saved_file has size $size\n");}
	
	if (-d $archived_file)
	{
	    print FOUT  "found existing archived file $archived_file is a directory; archiving aborted for this file \n";
	}
	elsif (-e $archived_file)
	{    # file exists
	    $size2 = (-s $archived_file);
	    unless ($size2 == $size) 
	    { 
	    print FOUT "$name: Warning - file sizes of saved ($size) and archived ($size2) files do not match\n";
	    print  "$name: Warning - file sizes of saved ($size) and archived ($size2) files do not match\n";
	}
	    else
	    {
		print FOUT  "$name: file $sorted_files[$index]  (size $size) is already archived \n";
		print   "$name:  file $sorted_files[$index]  (size $size) is already archived \n";
	    }
	}
	else
	{
#
#                   ARCHIVE the file  
#
	    print "Copy file $saved_dir/$sorted_files[$index] to $archived_dir <y/n> ? ";
	    my $choice = <STDIN>;
	    chomp $choice;
	    if ( $choice =~/^y/i)
	    {
		print FOUT "$name:Archiving file $sorted_files[$index] \n";
		if ($debug){ print "$name:Calling cpbnmr with parameters:  $saved_dir, $sorted_files[$index], $archived_dir\n";}
		$cmd = sprintf("/home/musrdaq/musr/mdarc/bin/cpbnmr %s/%s  %s ", $saved_dir, $sorted_files[$index], $archived_dir );
		
		if ($debug) { print  "$name: cmd=\"$cmd\"\n"; }
		print FOUT  "$name: cmd=\"$cmd\"\n";
		
	    
		#$status=system "$cmd";
		if( $status == 0)
		{
		    #print FOUT "$name: Success after system command, status=$status\n";
		    print FOUT "$name: Successfully archived file $sorted_files[$index]\n";
		    print "$name: Successfully archived file $sorted_files[$index]\n";
		}
		else
		{
		    print FOUT "Failure after system command, status=$status\n";
		    print      "Failed to archive file  $sorted_files[$index]\n";
		    print FOUT "Failed to archive file  $sorted_files[$index]\n";
		    if ($status == -1 )
		    {      # there is a message in errno
			print FOUT "Error message: $!\n"; # errno is put in $! for system cmd
			print      "Error message: $!\n";
			return($FAILURE);
		    }
		}
	    } # end of $choice
	}
	$index++;
    } # while loop
    
    
return ($SUCCESS) ;

}
    
  #############################################################################

    sub check_files_converted( $saved_dir, $archived_dir, @files )

##############################################################################
{
#  Check run files for any holes
#  Inputs
#     min run       minimum run (needed for check for holes)
#     @files        array of run files (mid and msr)
#     
### Note: if > 1 parameter,  array comes last in parameter list otherwise have 
#   to pass by reference 
###
#  
#  Outputs


    my  ( $saved_dir, $archived_dir, @files ) = @_ ; # get the parameter(s)
    my (@sorted_files,  $temp);
    my ($len, $first_run, $last_run);
    my $name = "check_files_converted";
    my ($index,$k,$this_run,$nf,$type);
    my ($message,$message2,$saved_file, $archived_file, $mid,$mud, $size, $size2);
    my $debug=0;
    

    print FOUT  " $name starting  \n"; 

    
# Sort
    
    @sorted_files = sort {$a <=> $b } @files;
    if($debug)
    {
        print FOUT  "\n$name:  sorted files:\n";
        print FOUT  "@sorted_files\n";
    }
    $len = $#sorted_files; # array length   ( Note: no. of files found = len + 1 )
    
    
    $_ =  $sorted_files[$len];
    ($temp) = split /\./;
    $last_run = $temp+0 ; # make sure it's integer not string

    
    $_ =  $sorted_files[0];
    ($temp) = split /\./;
    $first_run = $temp +0 ; # make sure it's integer not string

    $k=0;
    $this_run = $first_run;
    
    print  FOUT "\nRun   mid msr  Type  Archived   Message\n";
    print  "\nRun   mid msr  Type  Archived   Message\n";
    # loop over all files 
    while ($k < $len )   # $len = total no. of files
    {
	$nf= 0;
	$mud="N"; $mid="N";
	while ($sorted_files[$k] =~ /$this_run/)
	{
	    $_ = $sorted_files[$k]; 
	    $nf++; # count no. of files found
	 #   print   "$_";
	    $k++; # next array element
	    if(/msr/)
	    { 
		$mud="Y";  # set a flag; we have a .msr file for this run
		$size = (-s $_);
	    }
	    elsif(/mid/){ $mid="Y";}
	}
	print "$this_run   $mid  $mud ";
	print FOUT "$this_run   $mid  $mud ";

	if ($nf > 0)    # we have found files for this run
	{
	    $message=$message2="";
#	$index=$k-1; # point to last run number
	    $_ = $sorted_files[$k-1]; 
	    # find out if this run is Type 1 or Type 2
	    $saved_file = sprintf("%s/0%s.odb",$saved_dir,$this_run);
	    $saved_file =~ s/\/\//\//; # remove any // in path
	 #   print "odb file = $saved_file\n";

	    if($nf==2) { $type=1; } # two files - must be type 1
	    elsif (/msr/) { $type=2; }
	    elsif (/mid/) { $type=1; }
	    else { $type=0 };
	    
#	print "saved_file:$saved_file, $this_run\n";
	    if (-e $saved_file) # look for .odb file 
	    {
		
	    # open the file
		unless( open FILE, $saved_file)
		{
		    $message= "(can't open file $saved_file)";
		}
		while (<FILE>  )
		{
		    if ( /Programs\/fe1_BNMR/)     # type 1
		    {   
			#if($debug){ print "found string /Programs/fe1_BNMR\n";}
			unless ($type eq 1) 
			{
			    $message= "Types disagree between saved odb (1) and run files ($type) for run $this_run";
			    $type="?";
			}
			else 
			{
			    $saved_file =~ s/odb/mid/;
			    #print "mid file = $saved_file\n";
			    unless (-e $saved_file) { $message= "Missing mid file $saved_file\n" ;}
			}
			last;
		    }
		    elsif (  /Programs\/feBNMR2/)  # type 2
		    {
			#if($debug){ print "found string /Programs/feBNMR2";}
			unless ($type eq 2)
			{ 
			    $type="?";
			    $message= "Types disagree between saved odb (2) and run files ($type) for run $this_run";
			}
			last;
		    }
		} # file open
		close FILE;
	    } # -e $saved_file (odb)
	    else
	    {  # no odb file
		$message = "(no odb file found)";
	    }
	    print      "    $type   " ;
	    print FOUT "    $type   " ;

	} # end of found files for this run ($nf > 0)
	else { print "    ?   "; print FOUT "    ?   "} # print spaces and type
	# 
	# look for an archived file for this run even though no saved files
	#
	$archived_file = sprintf("%s/0%s.msr",$archived_dir, $this_run );
	$archived_file =~ s/\/\//\//;  # remove any // in path
	#print (" archived_file: $archived_file");


#  look for archived (msr) file
	if (-d $archived_file)
	{
	    print "?";
	    print FOUT "?";
	    $message2="(file $archived_file is a directory)";
	}
	elsif (-e $archived_file)
	{    # file exists
	    print "Y"; print FOUT "Y"; 
	    if ($mud=="Y")
	    {   # if we have a mud file, check sizes
		$size2 = (-s $archived_file);
		unless ($size2 == $size) { $message2="(mismatch between sizes of saved ($size) and archived ($size2) files)"; }
	    }
	}
	else 
	{
	    print "N"; print FOUT "N"; 
	} # no archived file
	
	print " $message $message2"; print FOUT " $message $message2";
	$this_run++;
	print "\n"; print FOUT "\n";
  #    if($this_run > 40010) {last;}
}
    print FOUT  "Run   mid msr  Type  Archived   Message\n";
    print  "Run   mid msr  Type  Archived   Message\n\n";

    return ;
    
}
  

# this 1 is needed at the end of the file so require returns a true value   
1;   









