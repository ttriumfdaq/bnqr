#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from hold button
# 
# invoke this script with cmd
#           include                    experiment      hold   toggle  beamline ppg
#              path                                    flag   flag             mode
# hold.pl /home/bnmr/online/mdarc/perl    bnmr           n      y       bnmr    2a
# hold.pl /home/musrdaq/musr/mdarc/perl   musr           n      y       m20     2 ** set to 2 for musr ***
#
# Sets a flag in odb (frontend area) so that frontend will stop sending the histograms, but otherwise
#   nothing changes. 
#
# $Log: hold.pl,v $
# Revision 1.1  2013/01/21 21:47:40  suz
# initial VMIC version for cvs
#
# Revision 1.16  2004/06/10 18:18:37  suz
# require init_check; use our; last Rev message is bad
#
# Revision 1.15  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.14  2004/03/29 18:26:42  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.13  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.12  2003/01/08 18:35:56  suz
# add polb
#
# Revision 1.11  2002/04/12 20:32:02  suz
# add parameter include_path
#
# Revision 1.10  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.9  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.8  2001/09/14 19:19:53  suz
# add MUSR support and 1 param; use strict;imsg now msg
#
# Revision 1.7  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.6  2001/05/03 21:33:52  suz
# support BNMR1
#
# Revision 1.5  2001/05/01 23:28:52  suz
# add message for bnmr1
#
# Revision 1.4  2001/03/01 19:20:35  suz
# output sent to /var/log/midas rather than /tmp
#
# Revision 1.3  2001/02/23 20:39:21  suz
# use new subroutine open_output_file
#
# Revision 1.2  2001/02/23 19:55:39  suz
# add toggle as a parameter. Change file permissions on output file so world can write
#
# Revision 1.1  2001/02/23 18:01:57  suz
#  initial version
#
#
use strict;
######### G L O B A L S ##################

our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ($inc_dir, $expt, $hold_flag, $toggle_flag, $beamline, $ppg_mode ) = @ARGV;
our $name = "hold"; # same as filename
our $len =  $#ARGV; # array length
our $nparam=6;
our $outfile = "hold.txt";
our $parameter_msg = "include_path, experiment; hold flag; toggle flag; beamline; ppg mode\n";
#########################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my ($flags_path, $eqp_name);
my $debug=$FALSE;
#########################################################

$|=1; # flush output buffers

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; hold flag = $hold_flag; toggle flag = $toggle_flag; beamline = $beamline\n";
#

unless ($ppg_mode)
{
    print_3($name,"FAILURE: ppg mode not supplied","MERROR",1);
}
unless ($ppg_mode =~/^[12]/)
{
    print_3 ($name, "FAILURE: invalid ppg mode ($ppg_mode) supplied", $MERROR, 1);
}


if ($hold_flag eq "y") { $hold_flag = $TRUE;} 
else { $hold_flag = $FALSE ; }

if ($toggle_flag eq "y") { $toggle_flag = $TRUE;} 
else { $toggle_flag = $FALSE ; }
unless ($beamline)
{
    print FOUT "FAILURE: beamline not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: beamline not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure: beamline not supplied \n"; } 
        die  "FAILURE:  beamline  not supplied \n";
}
#
#      determine equipment name from beamline
#
if( ($beamline =~ /bn[qm]r/i)  )
{
# BNMR/BNQR experiments use equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";
    $flags_path = "/Equipment/$eqp_name/frontend/flags/";
}
else
{
# All MUSR experiments use equipment name of MUSR_TD_ACQ
    $eqp_name = "MUSR_TD_ACQ";
    $flags_path = "/Equipment/$eqp_name/v680/flags/";
} 
print FOUT "flags path: $flags_path\n";


if( $ppg_mode =~ /^1/i)
{
    # Toggle not implemented yet for Type 1 experiments. Make sure it's FALSE
    print FOUT "$name: note - setting toggle false\n";
    $toggle_flag = $FALSE;
}

# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state == $STATE_RUNNING)
{          #      Running

# check state of toggle flag  (supplied as a parameter)
    if ($toggle_flag) 
    { 
        print FOUT "Toggle flag is set. Run cannot be put on hold until toggle is complete.\n";
        ($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING - Toggle flag is set. Hold not allowed" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        die "Toggle flag is set. Run cannot be put on hold";
    }


    # check state of hold flag (supplied as a parameter)
    if ($hold_flag) 
    {
        odb_cmd ( "msg","$MINFO","","$name", "INFO: hold flag is already set" ) ;
        print FOUT  "$name: hold flag is already set  \n";
        die "$name: Hold flag is already set";
    }
    
    ($status) = odb_cmd ( "set","$flags_path","hold" ,"y") ;
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error setting hold flag.\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting hold flag" ) ;
        die "$name: Failure setting hold flag";
    }
    print FOUT "INFO: Success - Hold flag has been set \n";
    odb_cmd ( "msg","$MINFO","","$name", "INFO: Hold flag has been set " );
    print "Success - Hold flag has been set \n";
}
else
{    # not running; no action
    print FOUT "Not running. Hold has no action. \n";
    odb_cmd ( "msg","$MINFO","","$name", "INFO: Run is not in progress. Hold has no action" );
    print "Not running. Hold has no action. \n";
}
exit;




