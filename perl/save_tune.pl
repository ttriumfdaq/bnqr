#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
 
# invoke this script with cmd e.g.
#         include                      experiment  ppg    tunename
#         path                                     mode
#
# save_tune.pl /home/bnqr/online/perl  bnqr        20      my_tune

# Uses template file 20_template.odb

use strict;
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
##########################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
our ($inc_dir, $expt, $ppg_mode, $tune ) = @ARGV;# input parameters
our $beamline;
our $len = $#ARGV; # array length
our $name = "save_tune"; # same as filename
our $parameter_msg = "include_path, experiment , ppg mode, tune_name ";
our $nparam = 4;  # no. of input parameters
our $outfile = "save_tune.txt";
################################################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my ($mdarc_path, $eqp_name);
my ($cmd);
my $debug=$FALSE;
my ($tunefile, $template, $lineno);
my @out_array;
my ($lineno,$line);
my ($my_path, $my_key, $tmp);
my $error_count=0;
my $array_flag=0;
my $array_cntr=0;


if($expt =~ /bn[qm]r/i)   
{
    $beamline = $expt;
# BNM/Q R experiments use equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";
}

$|=1; # flush output buffers



# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; ppg_mode= $ppg_mode; tune name = $tune \n";
#
#







$template = sprintf("/home/%s/%s/tunes/%s/%s_template.odb",$expt, $ENV{'ONLINE'},$ppg_mode,$ppg_mode);
print "template file = $template\n";


$tunefile = sprintf("/home/%s/%s/tunes/%s/$tune.odb",$expt, $ENV{'ONLINE'},$ppg_mode,$tune);
unless (-e $template)
{
    print_2 ($name, "ERROR: template $template does not exist",$MERROR,$DIE);
}
unless (open (FILE,"$template"))
	{
	  print_2 ($name, "ERROR: FAILURE: Could not open template file $template ",$MERROR,$CONT ) ;
	  print_2 ($name, "      Check beamline is correct.",$MERROR,$DIE);
	}
if(0)
{
if (-e $tunefile)
{
    print_2 ($name, "ERROR: there is already a tune file for name $tune",$MERROR,$DIE);
}

unless (open (FOUT,">$tunefile"))
	{
	  print_2 ($name, "ERROR: FAILURE: Could not open tune file $tunefile ",$MERROR,$CONT ) ;
	  print_2 ($name, "      Check path is correct.",$MERROR,$DIE);
	}
}
$lineno = 1;

while (<FILE>) 
{
    if($debug)
    {
	print "\nWorking on line ";
	print $lineno++;
    }
    $line=$_;

    chomp $line;
    if($debug){ print ": \"$line\"\n"; }

    unless( /\w/ )
    {
	if($debug) { print ".... skipping empty line...\n"; }  
    }
    else
    {
	if ( /^\[\D/)
	{ # found a directory
	    $line=~ s/[\[\]]//g;  # remove square brackets
	    $my_path = $line;     # it is the path
	    print "\nFound path for the following key(s) : $my_path\n\n";  
	}
	elsif ( /^\[\d+\]/ )
	{ # found an array value
	    unless ($array_flag)
	    { 
		print "Found array value but array_flag is not set; Last key $my_key should be an array\n";
		print "Array length may be wrong !!\n";
		goto err;
	    }
	    $array_cntr--;  # checks array length is as expected 
	    if($debug){ printf ("array_cntr is now $array_cntr\n"); }
	    if ($array_cntr ==0 ){ $array_flag = 0;} # clear flag
	    if($debug){ print "Skipping an array value: $_"; }   	
	}
	else
	{ # found a key
	    if ($line =~  /(\w\[(\d)+\])/)
	    {   # careful of STRING[4] which denotes array of strings,  and STRING : [32] this is a string
		if($debug){print "found a key with square brackets  \"$1\" denoting an array of length $2 in key  in \"$line\" \n";}
		$array_flag = 1;
		$array_cntr = $2;
	    }
	    else
	    { print "found a key in \"$line\n";}

	    ($my_key)=split (/=/,$line); # take the string prior to "="
	    if($debug){ print "my key: $my_key\n"; }

# Find the key type
            my ($type,$size,$value,$error);
	    ($error, $type,$size,$value)=get_key_type($my_path,$my_key);
          #  unless ($error) 
{ 
print "get_key_type returns  $type $size $value\n"; 
}

           
	    ($status) = odb_cmd ( "ls",$my_path,$my_key );
	    unless  ($status) 
	    { 
		print "\n *****  Failure on ls \"$my_path\/$my_key\" from odb_cmd ****\n"; 
		$error_count++;	   
		print FOUT "\n *****  Failure on ls \"$my_path\/$my_key\" from odb_cmd ****\n"; 
	    }
	    
	    if($array_flag)
	    { 
		if($debug){ print "expecting an array...";}
	    }

	    # make sure that both key and answer are arrays or not
	    if ($ANSWER =~  /\n/)
	    {  # this is an array
	
		my @array=split (/\n/,$ANSWER);
		$len = $#array;
		print "this is an array  of length $len\n"; 
		if($len != $array_cntr)
		{
		    print "Expected array length $array_cntr does not agree with array length in ODB ($len)\n";
		    goto err;
		}
	    

		unless ($array_flag)
		{ 
		    print "\n** error - key \"$my_key\" in file is NOT an array, but this key IS an array in ODB\n";
		    goto err;
		}
	    }
	    else
	    {  # key is NOT an array
		if($array_flag)
		{
		    print "\n** error - key \"$my_key\" in file IS an array, but this key is NOT an array in ODB\n";
		    goto err;
		}
		# get the answer
		

	    }

	    
	    
	}
    }
    
}
close FILE;
#
unless($error_count)
{
    print "\n   SUCCESS File \"$template\" has been checked \n";
    print FOUT "\n   SUCCESS File \"$template\" has been checked \n";
    print "\ndone\n";
    print FOUT "done\n";
    exit;
}
 err:
    print "\n ***** Check path and key name(s) in file \"$template\"\n";
print " ***** DO NOT LOAD THIS FILE until the problem is fixed !! \n";
print FOUT "\n ***** Check path and key names in file \"$template\"\n";
print FOUT " ***** Do not load this file until the problem is fixed!! \n";

print "\ndone\n";
print FOUT "done\n";
exit;




sub get_key_type($$)
{
     # inputs:   $path $key

  my ($path, $key) = @_;
  my $status;

  ($status) = odb_cmd ( "ls -lt",$my_path,$my_key );
  unless  ($status) 
  { 
      print "\n *****  Failure on ls -lt \"$my_path\/$my_key\" from odb_cmd ****\n"; 	   
      print FOUT "\n *****  Failure on ls -lt \"$my_path\/$my_key\" from odb_cmd ****\n"; 
return (0);
  }
  print "Answer= $ANSWER\n";
  chomp $ANSWER;
  $_=$ANSWER;
  s/-+/\@/;
  print "Now answer=$_\n";
  my @xxx = split /@/, $_;
  $_=@xxx[1];
  s/\W//;
  my $key2=$key;
  $key2 =~ tr/\(\)/./;
  
  if(/^$key2/){ print "found key in the string\n";}
  else { print "didn't find key \"$key\" in string \"$_\"\n" ; return 0; }
  s/^$key2// ;
  my $string=$_;

  s/\s+/ /g;
  
  print "Now string=$_\n";


  @xxx=split / /, $_;
  print "array xxx = @xxx \n";
  if($xxx[2] > 1) 
  { 
      my @array;
      my $len=$#xxx;
      my $size=$xxx[2];
      my $type=$xxx[1];
      print "Type = $xxx[1] size = $xxx[2] ... this is an array... length=$len\n";
      my $i=7;
      my $j=0;
      while ($i < ($len+1) )
      {
	  print "$xxx[$i]   $xxx[$i+1] \n";
          if ($xxx[$i]=~/[\d]/) { $array[$j] =  $xxx[$i+1]; $j++; }
          else { print "expect an array element instead of $xxx[$i]\n"; }
          $i+=2;        
      }
      
      print "array= @array\n";

      
     return (1, $xxx[1], $xxx[2], "array");
  }
  else
  { 
     print "Type = $xxx[1] size = $xxx[2] and value = $xxx[7]\n";
     return (1, $xxx[1], $xxx[2], $xxx[7]);
  }
}


# perl module must end with a 1
1; # return a good status
#
