#!/usr/bin/perl  
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d
#
#    IMUSR only - set renormalize
#
#   Normally invoked from renorm button
# 
# invoke this script with cmd
#                  include                        experiment   renorm  beamline
#                   path                                         flag
# imusr_renorm.pl  /home/online/musrdaq/mdarc/perl    imusr        y      dev      
#
# Sets the renorm flag in odb (imusr area).
# renormalize is hot linked 
# 
#
# $Log: imusr_renorm.pl,v $
# Revision 1.1  2013/01/21 21:47:40  suz
# initial VMIC version for cvs
#
# Revision 1.5  2004/06/10 18:09:07  suz
# require init_check; use our; last Rev message is bad
#
# Revision 1.4  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.3  2004/04/21 19:11:54  suz
# add a space in search string to fix bug for beamlines m15/m20; add missing ','
#
# Revision 1.2  2004/04/08 17:47:24  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.1  2004/02/09 22:30:18  suz
# copy here from musr_mdarc
#
#

use strict;


##################### G L O B A L S ####################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#########################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ($inc_dir, $expt, $renorm_flag, $run_state, $beamline ) = @ARGV;
our $len =  $#ARGV; # array length
our $name = "imusr_renorm";  # same as filename
our $nparam=5;
our $outfile = "imusr_renorm.txt";
our $parameter_msg = "include_path, experiment; renormalize flag; run state;  beamline\n";
#######################################################################
# local variables
my ($transition, $path, $key, $status);
my ($input_path) ;
my $debug=$FALSE;
my $eqp_name;
#
$|=1; # flush output buffers



# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 


# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null


print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; renorm flag = $renorm_flag\n";
#
if ($renorm_flag eq "y") 
  {
    # flag already set
    print_3($name, "Renormalize flag is already set...experiment is already renormalizing",$MINFO,$DIE);
  }
if ($run_state == $STATE_STOPPED)
  {
    print_3($name, "Run is stopped. Cannot renormalize",$MINFO,$DIE);
  }


# set renorm flag (program clears it after renormalization)
$renorm_flag = $TRUE ; 

#
#      determine equipment name from beamline
#
if( ($beamline =~ /bn[mq]r/i)  ||  ($beamline =~ /pol/i) )
  {
    # Not supported for BNMR/BNQR/POL experiments
    print_3 ($name,"No support for beamline \"$beamline\" ",$MERROR,$DIE);
  }

# All IMUSR experiments use equipment name of MUSR_I_ACQ
$eqp_name = "MUSR_I_ACQ";
$input_path = "/Equipment/$eqp_name/settings/input/";

print FOUT "input_path = $input_path\n";

($status) = odb_cmd ( "set","$input_path","renormalize" ,"$renorm_flag") ;
unless($status)
  { 
    print_3 ($name,"Failure writing to renormalize flag",$MERROR,$DIE);
  }
print_3 ($name, "Success - Renormalize flag has been set",$MINFO,$CONT);
exit;

