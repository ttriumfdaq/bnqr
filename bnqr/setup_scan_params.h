/*   setup_scan_params.h

*/
#ifndef _setup_scan_params_
#define _setup_scan_params_

/* Function prototypes */
INT mode10_set_scan_params(INT *ninc);
INT random_set_scan_params(INT *ninc);
INT  RF_set_scan_params(INT *ninc);

#ifdef CAMP_ACCESS
INT camp_set_scan_params(INT *ninc);
#endif

#ifdef EPICS_ACCESS
INT epics_set_scan_params(INT *ninc);
#endif

#endif //  _setup_scan_params_
