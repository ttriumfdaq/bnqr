/* this file is included into febnmr.c. 
   Used by PPG Modes 1a/1b/2a/2e with randomized frequencies */

/*
CVS log information:
$Log: trandom.c,v $
Revision 1.3  2013/05/08 00:37:53  suz
change a message

Revision 1.2  2013/05/01 20:19:16  suz
remove ifdef PSM;use full filepath; add check on number freq values read from file

Revision 1.1  2013/01/21 20:58:10  suz
initial VMIC version

Revision 1.6  2008/04/14 22:21:32  suz
remove spurious code

Revision 1.5  2007/09/26 18:25:53  suz
get rid of warnings

Revision 1.4  2005/08/04 00:52:32  suz
add 2e

Revision 1.3  2005/05/17 23:38:04  suz
add support for randomized 2a

Revision 1.2  2005/02/16 20:47:37  suz
add another debug flag (dd[16]) to reduce output messages

Revision 1.1  2005/01/25 23:43:42  suz
Original: ppg modes 1a/1b with random freq steps

Example
pfreq ia   prandom_freq  pseqf
100   0    500             4
200   1    100             0
300   2    300             2
400   3    200             1
500   4    600             5
600   5    400             3
*/

INT read_freq_file(char* ppg_mode, INT num_frequency_steps)
{
  /*  Called if type 1a,1b with randomized frequency steps 
      i.e. random_flag is true.

  input parameter: num_frequency_steps

  allocates the space for randomizing the frequency values
  opens the file of frequency steps and copies it into an array
  */

  INT i;
  FILE  *fileinput;
  char frqfile[64];

  INT  *ptr; /* temorary pointer */

 
  if(num_frequency_steps < 1)
    {
      cm_msg(MERROR,"read_freq_file","invalid number of frequency steps supplied=%d\n",num_frequency_steps);
      return DB_INVALID_PARAM;
    }
  else if (num_frequency_steps < 2)
    {
      cm_msg(MERROR,"read_freq_file","cannot randomize with only one frequency step\n");
      return DB_INVALID_PARAM;
    }
      
  /* open the frequency file */
  sprintf(frqfile,"%s/%s.psm",fs.input.cfg_path,ppg_mode);
  
  if(dd[16])printf("opening frequency file %s\n",frqfile);

  if(pfreq != NULL)
    {
      free (pfreq);
      pfreq=NULL;
    }

  fileinput = fopen(frqfile,"r");
  ss_sleep(50);
  if(fileinput == NULL)
    {
      cm_msg(MERROR,"read_freq_file","Frequency table file %s could not be opened", frqfile); 
      return FE_ERR_HW;    
    }

  /* allocate space for the frequency array */
  pfreq = (INT *) calloc(num_frequency_steps, sizeof(INT));
  if (pfreq == NULL)
    {
      cm_msg(MERROR,"read_freq_file","Could not allocate memory for freq array (%d * %d)bytes ",
	     num_frequency_steps, sizeof(INT) ); 
      return FE_ERR_HW; 
    }
  ptr = pfreq;
  /* read the file  */
  i=0;

  if(strncmp("2",ppg_mode,1)==0) /* look for type 2 */
    {
      /* for PSM  Type 2, freq values are in hex format */
      while (fscanf(fileinput,"%x", ptr) != EOF)
	{ 
	  if(dd[16]) 
	    printf("read_freq_file:   i=%d  frequency value = 0x%x \n",i,*ptr);      
	  i++;
	  if( i > num_frequency_steps)
	    {
              cm_msg(MERROR,"read_freq_file",
		     "ERROR frequency file \"%s\" seems to have more frequency steps than expected (num freq steps=%d)",
		     frqfile,num_frequency_steps ); 
	      printf("read_freq_file: counter i=%d num_frequency_steps=%d; too many steps in file\n",i,num_frequency_steps);
	      return FE_ERR_HW;
	    }
	  ptr++;
	}
    }
  else
    {
      /* for PSM Type 1, freq values are in Hz */
      while (fscanf(fileinput,"%d", ptr) != EOF)
	{ 
	  if(dd[16]) 
	    printf("i=%d  frequency value = %u Hz\n",i,*ptr);      
	  i++;
	  if(i>num_frequency_steps)
            {
              printf("read_freq_file:   Error i=%d num_frequency_steps=%d\n",i,num_frequency_steps);
              return FE_ERR_HW;
            }

	  ptr++;
	}
    }
 

  if(dd[16])printf("Num freq steps from the file is i=%d \n",i);
  if (i != fs.output.num_frequency_steps)
    {
      cm_msg(MERROR,"read_freq_file","number of freq steps in file %s (%d) does not agree with expected odb value .../output/num frequency steps (%d)",
	     frqfile,  i,num_frequency_steps);
      return FE_ERR_HW;
    }
  
  if(dd[16])
    {
      if(strncmp("2",ppg_mode,1)==0) /* look for type 2 */
	{ /* type 2 */
	  for (i=0; i<num_frequency_steps; i++)
	    printf("i=%d frequency value = 0x%x  \n",i,pfreq[i]);
	}
      else
	{
	  for (i=0; i<num_frequency_steps; i++)
	    printf("i=%d frequency value = %d Hz \n",i,pfreq[i]);
	}
 
    }

  /* clear random memory and realloc */
  if(prandom_freq != NULL)
    {
      free (prandom_freq);
      prandom_freq=NULL;
    }
  
  prandom_freq = (INT *) calloc( num_frequency_steps, sizeof(INT) );
  if (prandom_freq == NULL)
    {
      cm_msg(MERROR,"read_freq_file","Could not allocate memory for random freq array (%d * %d)bytes ",
	     num_frequency_steps, sizeof(INT) ); 
      return FE_ERR_HW; 
    }
  if(dd[16])
    {  /* fill with known values */
      for (i=0; i<num_frequency_steps; i++)
	{
	  prandom_freq[i]=i+5;
	  //printf(prandom_freq[%d] = %d \n",i,prandom_freq[i]);
	}
    }
  
  if(pindex != NULL)
    {
      free (pindex);
      pindex=NULL;
    }

  pindex = (INT *) calloc( num_frequency_steps, sizeof(INT) );
  if (pindex == NULL)
    {
      cm_msg(MERROR,"read_freq_file","Could not allocate memory for index array (%d * %d)bytes ",
	     num_frequency_steps, sizeof(INT) ); 
      return FE_ERR_HW; 
    }

  if(pseqf != NULL)
    {
      free (pseqf);
      pseqf=NULL;
    }

  pseqf = (INT *) calloc( num_frequency_steps, sizeof(INT) );
  if (pseqf == NULL)
    {
      cm_msg(MERROR,"read_freq_file","Could not allocate memory for index to original freq array (%d * %d)bytes ",
	     num_frequency_steps, sizeof(INT) ); 
      return FE_ERR_HW; 
    }

  return SUCCESS;
}

INT randomize_freq_values(char* ppg_mode, INT num_freq_val)
{
  /* randomize the frequency values
     
  called after read_freq_file and at each helicity flip (after each complete sweep) 
  
  uses globals  *pfreq          pointer to initial frequency array (ordered)
  *prandom_freq   pointer to randomized frequency array 
  *pindex         pointer to index array 
  *pseqf        pointer to original freq array (to fill histo data correctly in type 2) 
  num_freq_vals =  number of frequency values in the array 
  
  uses pindex to keep track of indexes into frequency array that have not yet been selected.
  Each time a frequency is selected it is copied into the prandom_freq array. The index array
  is squashed down each time so the number of values in it (nval) becomes one less. A random number
  between 0 and the number of index values left (nval) is selected. That is used as the index into
  the index array. The contents of that is the index into the frequency array.
  */
  
  INT ia; /* pointer into INT pfreq array */
  INT ik; /* pointer into prandom_freq array */
  INT ii; /* pointer into index array */
  INT my_freq;
  INT iran,nval,i;
  // INT ran;
  
  if(num_freq_val < 2 )
    {
      cm_msg(MERROR,"randomize_freq_values", "Error or too few frequency values (%d) to randomize",num_freq_val);
      return DB_INVALID_PARAM;
    }
  if(pindex==NULL || prandom_freq==NULL  || pfreq == NULL || pseqf == NULL)
    { 
      /* read_freq_file must be called prior to this routine to allocate space */
      cm_msg(MERROR,"randomize_freq_values",
	     "array pointer(s) are null. Space has not been allocated");
      return DB_INVALID_PARAM;
    }
  
  for (i=0; i<num_freq_val; i++)
    {
      pindex[i]=i;  /* initialize index array */
      prandom_freq[i]=0; /* clear random array */
      pseqf[i]=-1; /* initialize pointer array for filling histo data */
      if(dd[16])
	{
	  if(strncmp("2",ppg_mode,1)==0) /* look for PSM type 2 */
	    {  /* PSM type 2 */
	      printf("i=%d pindex[%d]=%d freq[%d] 0x%x  \n",
		     i,i,pindex[i],i,pfreq[i]);
	    }
	  else
	    {  
	      printf("i=%d pindex[%d]=%d freq[%d] %d Hz \n",
		     i,i,pindex[i],i,pfreq[i]);
	    }
	 
	}
    }
  ik=0;
  nval = num_freq_val;
  
  while (nval > 1)
    {
      iran=get_random_index(nval); /* get a random number, iran is an integer between 0 & nval-1 */
      if(dd[16])
	printf("nval = %d  random no. iran (between 0 and %d) = %d\n",nval,(nval-1),iran);
      ia=pindex[iran];
      my_freq = pfreq[ia];
      pseqf[ik]=ia;  /* remember where this for histogramming type 2 */
      if(dd[16])
	{
	  printf("Picked out freq value %d  at freq array index ia=%d \n", 
		 my_freq, ia);
	  /*printf("   using iran=%d as index of index_array whose contents was ia=%d \n",iran,ia); */
	}
      prandom_freq[ik]=my_freq;
      ik++;
      nval--;
      /* printf("nval is now is %d, ik is now %d\n",nval,ik); */
      
      /* shift down the index array */
      for (ii=iran; ii<nval; ii++)
	{
	  pindex[ii]=pindex[ii+1];
	}
      
      if(dd[16])
	{
	  for (ii=0; ii<nval; ii++)
	    printf("index array: i=%d contents=%d\n",ii,pindex[ii]); 
	}
      
      if(dd[16])
	{
	  for (i=0; i<num_freq_val; i++)
	    printf("Random freq array: ik=%d contents=%d \n",
		   i,prandom_freq[i]); 
	}
    } /* while */
  /* now get the last value */
  if (nval == 1)
    {
      if(dd[16])printf("getting last value, nval=%d, ik=%d\n",nval,ik);
      ia=pindex[0];
      my_freq = pfreq[ia];
      pseqf[ik]=ia;  /* remember where this */
      if(dd[16])printf("Picked out freq value %d at freq array index ia=%d \n", 
		   my_freq, ia);
      prandom_freq[ik]=my_freq;
    }
  else
    {
      /* this is just a test... shouldn't happen */
      printf("randomize_freq_values: Error... nval must be 1 after end of while loop (nval=%d\n",nval);
      return DB_INVALID_PARAM;
    }

  if(dd[16])
    {

      if(strncmp("2",ppg_mode,1)==0) /* look for PSM type 2 */
	{  /* PSM type 2 */
	  printf("Index     Frequency     Randomized Freq  index to orig\n");
	  for (i=0; i<num_freq_val; i++)
	    printf("%d           0x%x            0x%x       %d\n",i,pfreq[i], prandom_freq[i], pseqf[i]);
	}
      else

	{
	  printf("Index     Frequency(Hz)     Randomized Freq (Hz)  index to orig\n");
	  for (i=0; i<num_freq_val; i++)
	    printf("%d           %d              %d        %d\n",i,pfreq[i], prandom_freq[i], pseqf[i]);
	}
      
      if(dd[16])
	{
	  printf("pointers pfreq=%p, prandom_freq=%p, pindex=%p pseqf=%p\n",pfreq,prandom_freq,pindex,pseqf);
	  printf("read_freq_file: returns success\n");
	}
    }
  return SUCCESS;
}

void  free_freq_pntrs(void)
{
  if(prandom_freq != NULL)
    free (prandom_freq);
  if(pindex != NULL)
  free (pindex);  
  if(pfreq != NULL)
  free (pfreq);
  if(pseqf != NULL)
  free (pseqf);
  pfreq=pindex=prandom_freq=pseqf=NULL;
}
  
INT get_random_index( INT nval)
{
  /* returns a random integer between 0 and nval-1
     this will be used as the index of the index array
     index array contains the indexes of the frequency array
 */
  INT i;
  float fran,ftmp;
  long int lseed;
  lseed = (long int)seed; /* to get rid of warnings */

  fran = ran0(&lseed);
  seed = (DWORD)lseed;
  ftmp = fran * (float)nval;
  i = (INT)ftmp; /* truncate fractional part .. integers go from 0 to nint-1 */
  if(dd[16])
    printf("get_random_index:  random no. = %f ftmp=%f -> %d\n",fran,ftmp,i);
  return i;
}

float ran0(long *idum)
/* 
   Minimal  random number generator of Park and Miller. Returns a uniform
   random deviate between 0.0 and 1.0. Set or reset idum to any integer value
   (except the unlikely value MASK) to initialize the sequence; idum must not
   be altered between calls for successive deviates in a sequence. */
{
  long k;
  float ans;
  
  *idum ^= MASK;
  k=(*idum)/IQ;
  *idum=IA*(*idum-k*IQ)-IR*k;
  if (*idum < 0) *idum += IM; ans=AM*(*idum);
  *idum ^= MASK;
  return ans;
}

