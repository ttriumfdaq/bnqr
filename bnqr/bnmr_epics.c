/* bnmr_epics,c

Frontend code to scan the epics devives (NaCell,Laser)

   $Log: bnmr_epics.c,v $
   Revision 1.4  2016/05/24 21:43:06  suz
   higher NaCell Voltage requested (2700V)

   Revision 1.3  2015/06/02 23:36:42  suz
   increased NaCell maximum from 999 to 1500V. Algorithm needs to be tested at higher voltages

   Revision 1.2  2014/03/21 20:12:25  suz
   Removed helicity switch check code (it was GONE before as moved to rf_config)

   Revision 1.1  2013/01/21 20:58:10  suz
   initial VMIC version

   Revision 1.29  2008/05/12 22:47:23  suz
   Helicity_Switch now is GONE; check moved to rf_config

   Revision 1.28  2008/05/01 22:51:57  suz
   add helicity_switch

   Revision 1.27  2008/04/17 22:09:26  suz
   change NaCell params; remove ifdef GONE stuff

   Revision 1.26  2008/04/14 22:30:15  suz
   add LaserPower_init

   Revision 1.25  2007/10/04 20:51:47  suz
   further adjust offsets

   Revision 1.24  2007/09/18 21:23:23  suz
   NaCell voltage params have changed

   Revision 1.23  2006/06/22 21:26:51  suz
   add a message about access permissions when cannot write to Epics device

   Revision 1.22  2005/10/11 23:00:40  suz
   reduced retrys for epics_reconnect

   Revision 1.21  2005/08/13 01:36:38  suz
   move check on epics dual channel params to rf_config

   Revision 1.20  2005/07/19 21:31:40  suz
   support Epics HV bias for POL; helicity routines now renamed & used for bias as well

   Revision 1.19  2005/04/21 20:02:53  suz
   add read of EPICS single/dual channel switch

   Revision 1.18  2005/04/08 17:58:27  suz
   make 2 printf statements debugs

   Revision 1.17  2005/04/08 17:20:21  suz
   set a long watchdog before reading helicity

   Revision 1.16  2005/02/16 20:53:03  suz
   removed opening HEL Off; not needed

   Revision 1.15  2005/02/14 20:37:12  suz
   add flip to init_helicity; switch must be set to DAQ if flip is enabled

   Revision 1.14  2005/02/04 22:18:28  suz
   changes to Helicity_init

   Revision 1.13  2005/02/03 22:46:14  suz
   add read of helicity switch and helOff in helicity_init

   Revision 1.12  2005/01/26 21:56:23  suz
   add helicity watchdog

   Revision 1.11  2005/01/25 23:29:25  suz
   direct channel access for helicity read added

   Revision 1.10  2004/11/24 01:40:23  suz
   add variable typical_offset for NaCell, tries with typical_offset before trying with maximum_offset

   Revision 1.9  2004/11/17 18:26:46  suz
   fixes for scan jump

   Revision 1.8  2004/11/17 00:09:38  suz
   first version of Pol's jump scan

   Revision 1.7  2004/10/25 17:23:34  suz
   add flag d11, silent mode for POL

   Revision 1.6  2004/05/03 18:25:53  suz
   changes to Field scan

   Revision 1.5  2004/03/31 00:05:45  suz
   generalize epics device name (Xx); extensive changes

   Revision 1.4  2003/12/01 19:52:34  suz
   add support for reconnect

   Revision 1.3  2003/06/26 21:32:24  suz
   correct bug in Field params; add Epics_units so setpoint no longer always in Volts

   Revision 1.2  2003/06/24 23:21:41  suz
   add Field scan; not tested for Field, NaCell still works

   Revision 1.1  2003/05/01 21:23:50  suz
   initial bnmr version; identical to bnmr1 Rev1.7

   Revision 1.7  2003/01/13 18:28:31  suz
   Fix problems when EPICS_ACCESS is disabled

   Revision 1.6  2003/01/09 21:31:34  suz
   change algorithm for repeat; add EpicsCheck


   Revision 1.5  2003/01/08 19:22:25  suz
   change Nacell stability constant

   Revision 1.4  2002/11/26 23:00:16  suz
   laser and NaVolt params moved to input from hardware in odb

   Revision 1.3  2002/06/07 18:30:57  suz
   replace conn.h by connect.h

   Revision 1.2  2002/06/05 00:45:41  suz
   add cvs header

*/

#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "experim.h"
#include "cadef.h"
#include "connect.h"
#include "bnmr_epics.h"
#include "EpicsStep.h"
#include "debug.h" 
extern FIFO_ACQ_FRONTEND fs;

INT iwait(INT millisec); // in frontend
#ifndef POL
/* Read Helicity directly (BNMR/BNQR)
---------------------------------------------------------------------------
  Read  Helicity 
----------------------------------------------------------------------------
*/
//Epics names of Helicity variables 

char RhelOn[]="ILE2:POLSW2:STATON";
char RhelOff[]="ILE2:POLSW2:STATOFF";
//char RhelSwitch[]="ILE2:POLSW2:STATLOC";/* 0=DAQ has control 1=Epics has control */

/*
------------------------------------------------ 
  Read Laser Power (for threshold) BNMR/BNQR 
-----------------------------------------------
*/
char RlaserPower[]="ILE2:LAS:RDPOWER"; /* for laser power */

/* 
--------------------------------------------
EPICS names of dual channel switching modes
--------------------------------------------
*/
char RbnmrPeriod[]="BNMR:BNQRSW:BNMRPERIOD";
char RbnqrPeriod[]="BNMR:BNQRSW:BNQRPERIOD";
char RbnmrDelay[]="BNMR:BNQRSW:BNMRDELAY";
char RbnqrDelay[]="BNMR:BNQRSW:BNQRDELAY";
char REnableSwitching[]="BNMR:BNQRSW:ENBSWITCHING";

#else /* end of BNMR/BNQR only */

/* Read HV bias directly (POL)
---------------------------------------------------------------------------
  Read  HV bias 
----------------------------------------------------------------------------
*/ 
//Epics names of HV bias variables */ 

char R_ITW[]="ITW:BIAS:RDVOL";
char R_ITE[]="ITE:BIAS:RDVOL";
char R_IOS[]="IOS:BIAS:RDVOL";
/* one of these will be copied into R_bias depending on odb param */


#endif /* POL only */


/*
------------------------------------------------------------------------------

   Sodium Cell :     CONSTANTS
  
------------------------------------------------------------------------------
*/
/* Epics names of Na/Rb Cell variables */ 
char NaRead_name[]="ILE2:BIAS15:RDVOL";
char NaWrite_name[]="ILE2:BIAS15:VOL";
/* parameters - used at begin run to calculate Diff and stability
   Absolute values!!  */
//const float   Nadiff_min=0.05; /* Diff set to  Nadiff_min if the increment fabs (Epics_inc) < Nainc_limit */
//const float   Nadiff_max=0.2;  /*      else   set to  Nadiff_max */
const float   Nadiff_min=0.05; /* Diff set to  Nadiff_min if the increment fabs (Epics_inc) < Nainc_limit */
const float   Nadiff_max=0.2;  /*      else   set to  Nadiff_max */

const float   Nainc_limit=1.5;/* below this value, Diff = Nadiff_min */
const float   Nainc_min=0.5;   /* minimum increment user is allowed to specify (was 0.3 before offset increased)
                                    then 0.5, now 1.0 */
/* note also that stability is calculated using above values */
//const float   Na_max=999; /* maximum value that user can set on NaCell (not reliable higher) */
const float Na_max=2700;  // higher maximum requested (was 1500) 
const float   Na_min=0.5; /* minimum value that user can set on NaCell (not reliable lower)  */
const float   Na_max_offset = -1.8; /* maximum offset (read-set) for NaCell (more accurate offset value will be calculated */
const float   Na_typical_stability = 0.4;
char NaUnits[]="V"; /* set points are in volts */
/*      end of NaCell constants        */
/*
------------------------------------------------------------------------------

   DYE LASER  :     CONSTANTS

------------------------------------------------------------------------------
*/
/* Epics names of Dye Laser  variables */ 
char LaRead_name[]="ILE2:RING:FREQ:RDVOL";
char LaWrite_name[]="ILE2:RING:FREQ:VOL";

/* parameters - used at begin run to calculate Diff and stability  */
const float   Ladiff_min= 0.001; /* Diff set to  Ladiff_min if increment fabs (Epics_inc) < Nainc_limit */
const float   Ladiff_max= 0.005;  /*      else   set to  Ladiff_max */
const float   Lainc_limit=0.010;/* below this value, Diff = Nadiff_min */
const float   Lainc_min=  0.003;   /* minimum increment user is allowed to specify */
/* note also that stability is calculated using above values */
const float   La_max=5.0; /* maximum value that user can set on Laser  */
const float   La_min=-5.0; /* minimum value that user can set on Laser   */
const float   La_max_offset = 0.004; /* maximum offset for laser (more accurate offset value will be calculated */
const float   La_typical_stability = 0.002;
char LaUnits[]="V"; /* set points are in volts */
/*      end of Laser constants        */
/*-------------------------------------------------------------------------------------
*/
/*
------------------------------------------------------------------------------

   FIELD  :     CONSTANTS

------------------------------------------------------------------------------
*/
/* Epics names of Field  variables */ 
char FdWrite_name[]="ILE2A1:HH3:CUR";
char FdRead_name[]="ILE2A1:HH3:RDCUR";

/* parameters - used at begin run to calculate Diff and stability

   All parameters are in AMPS     */
const float   Fddiff_min= 0.1; /* Diff set to  Fddiff_min if increment fabs (Epics_inc) < Fdinc_limit */
const float   Fddiff_max= 0.5;  /*      else   set to  Fddiff_max */
const float   Fdinc_limit=1.5;/* below this value, Diff = Fddiff_min */
const float   Fdinc_min=  0.2;   /* minimum increment user is allowed to specify */
/* note also that stability is calculated using above values */
const float   Fd_max=91.0; /* maximum value that user can set  */ //  BMS - TODO Change this to 200 Gauss
const float   Fd_min=-0.0001; /* minimum value that user can set (0 is allowed)  */
const float   Fd_max_offset = -0.2; /* maximum offset for Field (more accurate offset value will be calculated */
const float   Fd_typical_stability = 0.1;
char FdUnits[]="Amps"; /* set points are in Amps */
/*      end of Field constants        */

/* globals within this file 

    Note: offset = difference between read and write values
          diff   = difference between read and write increments, i.e.
          increment = difference between value now and value at last set point
*/

static float   Diff;     /* value has changed if (abs(difference between read & write increments) < Diff)*/
static float   Offset;   /* current offset value  */
static float   Epics_stop;

/* structure defined in EpicsStep.h */
EPICS_CONSTANTS epics_constants = { -1, -1, 0, 0, 0, 0 };

/* Field conversion constants */
/* old values
  const float B=-0.08;  Gauss */
/*const float A=2.228;  Gauss/Amp */
const float B=-0.175; /* Gauss */
const float A=2.2131; /* Gauss/Amp */


/************************************************************************************/
INT EpicsInit(BOOL flip, EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /* Initialize Epics Scan device - calls NaCell_init / Laser_init / Field_init */
  INT status;
  //INT icount = 0;


  
 if(dd[5])printf("EpicsInit starting\n");
  if(p_epics_params->NaCell_flag) 
      status = NaCell_init(flip, p_epics_params );
  else if (p_epics_params->Laser_flag)
    status = Laser_init(flip, p_epics_params );
  else if (p_epics_params->Field_flag)
    status = Field_init(flip, p_epics_params );

  else
    {
      printf("Epics_init: Invalid Epics Scan device %s\n", fs.output.e1n_epics_device);
      return ( FE_ERR_HW );
    }

  if(dd[5])printf("EpicsInit returning with status = %d  (last_offset =%.4f) \n",
	       status, p_epics_params->Epics_last_offset);
  return (status);
}



/*********************************************************************************/
INT Helicity_init(BOOL flip, INT *Rchid, char * Rname)
/*********************************************************************************/
{
  INT status,i;
  float value;

  /* Get ID for  STATUS  HEL ON, (also check HEL OFF)
       Set the Midas Watchdog to a long time before calling !   

       Fills output params Rchid, Rname 
*/

#ifdef EPICS_ACCESS

  if(*Rchid > 0)
    {
      /* Should have been cleared at end of last run */
      printf("Helicity_init: channel ID for HelOn is already defined; clearing the channel...\n");
      ca_clear_channel((chid)*Rchid );

    }
  /* get the ID for Hel On */
 
  Rchid_helOn=*Rchid=-1;
  status = caGetSingleId(RhelOn, &Rchid_helOn );
  if(status==-1)
    {
      printf("Helicity_init: Bad status after caGetID for status of HEL On \n");
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }
  /* store the chid and name in these output parameters */
  sprintf(Rname,"%s",RhelOn);
  *Rchid = Rchid_helOn;



  if(dd[7])printf("Calling read_Epics_chan for helOn\n");
  status = read_Epics_chan(Rchid_helOn,  &value);
  i=(INT)value;
  if(status != SUCCESS)
    printf("helicity_init: failure from read_Epics_chan for Hel On\n");
  else
    if(dd[7])printf("helicity_init: Helicity On reads %d\n",i);
#else
  printf("Helicity_init: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif 
  return SUCCESS;
}




/*********************************************************************************/
INT LaserPower_init(INT *Rchid, char * Rname)
/*********************************************************************************/
{
  INT status;
  float value;
  /* Get ID for Laser Power 
       Set the Midas Watchdog to a long time before calling !   

       Fills output params Rchid, Rname 


*/

#ifdef EPICS_ACCESS
  if(*Rchid > 0)
    {
      /* Should have been cleared at end of last run */
      printf("LaserPower_init: channel ID for LaserPower is already defined; clearing the channel...\n");
      ca_clear_channel((chid)*Rchid);
    }
  /* get the ID for Laser Power */


   Rchid_LaserPower=*Rchid=-1;
    status = caGetSingleId(RlaserPower, &Rchid_LaserPower );
  if(status==-1)
    {
      printf("LaserPower_init: Bad status after caGetID for status of Laser Power \n");
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }
  /* store the chid and name in these output parameters */
  sprintf(Rname,"%s",RlaserPower);
  *Rchid =  Rchid_LaserPower;


  if(dd[7])printf("Calling read_Epics_chan for Laser Power\n");
  status = read_Epics_chan(Rchid_LaserPower,  &value);

  if(status != SUCCESS)
    printf("LaserPower_init: failure from read_Epics_chan for Laser Power\n");
  else
    if(dd[7])printf("LaserPower_init: Laser Power reads %f\n",value);
#else
  printf("LaserPower_init: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif 
  return SUCCESS;
}


 
/************************************************************************************/
INT read_Epics_chan(INT chan_id,  float * pvalue)
/************************************************************************************/
{
  /* read an Epics channel directly using channel access
     
  (this routine is used to read the helicity; set the midas watchdog before calling!)
  */
  
  INT status;

#ifdef EPICS_ACCESS
  float fval;
  
  if(chan_id == -1)
  {
    printf("read_Epics_chan: channel has disconnected\n");
    return(FE_ERR_HW);
  }

  if(dd[7])printf("read_Epics_chan: calling caRead with chan_id=%d\n",chan_id);
  status=caRead(chan_id,&fval);
  if(status==-1)
 {
   printf("read_Epics_chan: Bad status after caRead for %s\n", ca_name((chid)chan_id));
    return(FE_ERR_HW);
  }
  else
    if(dd[7])printf("read_Epics_chan: Read value for %s as %8.3f\n", ca_name((chid)chan_id),fval);

  *pvalue=fval;
  if(dd[7])printf("read_Epics_chan for %s returning value = %f \n",ca_name((chid)chan_id),*pvalue);

#else
  printf("read_Epics_chan: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif 
  return(DB_SUCCESS);
}




/************************************************************************************/
INT EpicsRead(float* pvalue, EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{

  /* Returns SUCCESS  and value read in pvalue. 
     or -1 for failure

     Calls caRead to read appropriate epics device 

     caRead returns -1 for failure and 0 for success
  
         prototype is   int caRead(int chan_id, float *pval);
*/
  INT status;

#ifdef EPICS_ACCESS
  status =  caRead(p_epics_params->XxRchid , pvalue);
  if(status != -1)
    status = SUCCESS;
#else
  printf("EpicsRead: Epics Access is not defined -> DUMMY access to Epics in this code\n");
  status = SUCCESS;
#endif
  return(status);
}

void EpicsDisconnect(EPICS_PARAMS *p_epics_params)
{
  /* disconnect from the channels */
  if(dd[5])printf("EpicsDisconnect: starting\n");
  if(epics_constants.Rchan_id != -1)
    ca_clear_channel((chid)epics_constants.Rchan_id);
  if(epics_constants.Wchan_id != -1)
    ca_clear_channel((chid)epics_constants.Wchan_id);
  if(dd[5])printf("Cleared channels %d and %d\n",epics_constants.Rchan_id, epics_constants.Wchan_id);
  epics_constants.Rchan_id=epics_constants.Wchan_id=-1;
  p_epics_params->XxRchid=p_epics_params->XxWchid=-1;	  
  /* set to -1 to indicate these channels are disconnected */
  return;
}


INT EpicsReconnect(EPICS_PARAMS *p_epics_params)
{
  INT status;
  INT max_err=3;
  INT icount;
  BOOL connected;


  printf("\nEpicsReconnect: ***  reconnecting to Epics device .....\n");
  printf(" = Currently we only have 50 percent connectivity with EPICS. \n");
  printf(" = Establishing a connection may take some minutes. \n");
  printf(" = Please be patient....may have to retry \n");
  printf(" = If connection does fail, please restart run to try again. \n");
  printf(" =========================================================== \n");


  icount=0;

  connected=FALSE;

  while ( ! connected && icount < max_err )
    {
      /* Clear old channels if open */
      if(p_epics_params->XxRchid != -1)
	ca_clear_channel((chid)p_epics_params->XxRchid);
      if(p_epics_params->XxWchid != -1)
	ca_clear_channel((chid)p_epics_params->XxWchid);
	  
	  
      /* GetIDs for Epics scan ; opens only channels for enabled device  */
      status = EpicsGetIds( p_epics_params );
      if(dd[5])printf("After EpicsGetIds, status = %d XxRchid=%d XxWchid=%d\n",
	     status, p_epics_params->XxRchid, p_epics_params->XxRchid);
      if( status == SUCCESS) 
	{
	  epics_constants.Rchan_id = p_epics_params->XxRchid ;
	  epics_constants.Wchan_id = p_epics_params->XxWchid ;
	  connected=TRUE;
	}
      else if (status == -2)
	return(-1); /* no valid device specified */


      if(connected) 
	break;
      icount++;
      printf("EpicsReconnect: could not connect to Epics channels.....waiting 10s and retrying (%d)\n",icount);
      iwait(10000); /* wait 10s before retrying; include cm_yield every second */
    }
  
  if(connected) 
    return SUCCESS;
  
  cm_msg(MERROR,"EpicsReconnect","Could not reconnect to Epics. Epics system may not be available ");
  return (-1);
}

/************************************************************************************/
INT EpicsWatchdog(EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{
  INT status;
  float read_val; /* local variable */
  BOOL flag;
  /* note: EpicsReconnect should be called before calling this routine, otherwise
     epics_constants.Rchan_id will not be filled */
  
#ifdef EPICS_ACCESS  
  /* Maintain EPICS live */
  /* check that channel(s) have not disconnected */
  
  /* just make sure we can access fs */
  if (dd[7])printf("Epics device selected for scan = \"%s\"\n",fs.output.e1n_epics_device);


  flag=FALSE;
  /* 
     check the channels 
  */
 if(dd[7]) printf("EpicsWatchdog: XxRchid=%d,XxWchid=%d\n",
	       p_epics_params->XxRchid,p_epics_params->XxWchid);
  if ( !caCheck(epics_constants.Rchan_id)) 
    {
      printf("EpicsWatchdog: channel  (chid %d) has disconnected... attempting to reconnect \n",
	     epics_constants.Rchan_id);
      status = EpicsReconnect(p_epics_params);
       if(status != SUCCESS)
	 {
	   cm_msg(MERROR, "EpicsWatchdog", "error reconnecting to Epics (%d)",status);
	  printf("EpicsWatchdog: ERROR: bad status after attempting to reconnect to Epics (%d) Rchan_id=%d\n",
		 status,epics_constants.Rchan_id);
	  return(status);
	 }
    }
  if(dd[7])printf("Watchdog: calling caRead\n"); 
  status=caRead(epics_constants.Rchan_id , &read_val);
  if(status)
    {
      cm_msg(MERROR, "EpicsWatchdog", "error  reading epics voltage (status=%d)",status);
      printf("EpicsWatchdog: ERROR: bad status after calling caRead (%d), chid=%d\n",
	     status,epics_constants.Rchan_id);	  
      return(status);
    }
  else
    if(dd[7])printf("Watchdog read :%f [%d]\n", read_val, status);

      
  return (SUCCESS);
  
#else
  printf("EpicsWatchdog: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif
  return(SUCCESS);
  
}

/*---------------------------------------------------------------------------*/
INT EpicsGetIds( EPICS_PARAMS *p_epics_params )
/*---------------------------------------------------------------------------*/
{
  INT status;
  p_epics_params->XxRchid=p_epics_params->XxWchid= -1; /* initialize to a bad value */

#ifdef EPICS_ACCESS

  /* Get Epics IDs */
  if(p_epics_params->NaCell_flag)
    {
      printf("EpicsGetIds: getting epics channel ID for access to Na/Rb Cell ....\n");
      status=caGetId (NaRead_name, 
		      NaWrite_name, 
		      &p_epics_params->XxRchid, 
		      &p_epics_params->XxWchid);
    }
  else if(p_epics_params->Laser_flag)
    {
      printf("EpicsGetIds: getting epics channel ID for access to Dye Laser ....\n");
      status=caGetId (LaRead_name, 
		      LaWrite_name, 
		      &p_epics_params->XxRchid, 
		      &p_epics_params->XxWchid);
    }
  else  if(p_epics_params->Field_flag)
    {
      if(dd[5])printf("EpicsGetIds: getting epics channel ID for access to Field ....\n");
      status=caGetId (FdRead_name, 
		      FdWrite_name, 
		      &p_epics_params->XxRchid, 
		      &p_epics_params->XxWchid);
      
    }
  else
    {
      cm_msg(MERROR,"EpicsGetIds","No valid epics device has been specified");
      return -2;
    }

  if(status==-1)
    {
      cm_msg(MERROR,"EpicsGetIds","Cannot get epics channel ID; EPICS system may  not be available");
      return(status);
    }
  else
    {
      if(dd[5])printf("EpicsGetIds: caGetId returns channel ID for read: %d and write: %d\n",
	     p_epics_params->XxRchid,p_epics_params->XxWchid);
      if (caCheck(p_epics_params->XxRchid) && caCheck(p_epics_params->XxWchid))
	{ if(dd[5])printf("EpicsGetIds: Info: Epics channels are  connected \n"); }
      else
	printf("EpicsGetIds: Info: One or both of the Epics channels are not connected \n");
    }
#else
  printf("EpicsGetIds: EPICS_ACCESS is not defined -> DUMMY access only in this code\n");
#endif 
  
  return SUCCESS;
}



/************************************************************************************/
INT EpicsIncr( EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{
  /* called by cycle_start to increment Epics value 

     returns 
     SUCCESS 
     -1 failure returned  from setting/reading Epics device; may no longer have a connection 
     -2 invalid input parameters; may no longer have a connection
     
     -3  read increment and write increment do not agree
     -4  offset between read and set values is outside allowed  range

 */
  
  INT status;
  float check_offset;
  float typical_offset;
  float stability;

  epics_constants.debug=dd[6];  /* update debugging for EpicsStep */
  if(dd[5])
    {
      printf("\nEpicsIncr starting\n");
      
      /* Epics constants should have been set up by EpicsInit */
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }
  
#ifdef EPICS_ACCESS 

  /*
    call EpicsSet_step to set the next setpoint
  */
  
 if(dd[5])printf("EpicsIncr: calling EpicsSet_step to set %s value to %.4f%s\n",
	       fs.output.e1n_epics_device,p_epics_params->Epics_val, p_epics_params->Epics_units);


  /* derive offset limit from last offset value i.e. widen offset by the stability */
 if(dd[6])printf("EpicsIncr: prior to deriving offset, Epics_last_offset = %f\n",
	p_epics_params->Epics_last_offset);

  if (p_epics_params->Epics_last_offset < 0)
    stability =  p_epics_params->Epics_stability * -1; /* stability is positive */
  else
    stability =  p_epics_params->Epics_stability;


  
  check_offset = p_epics_params->Epics_last_offset + stability; 
  if(dd[6])printf("EpicsIncr: calling EpicsSet_step with  check_offset=%f  Epics_last_offset=%f stability=%f\n",
	 check_offset , p_epics_params->Epics_last_offset , stability); 

  if(p_epics_params->NaCell_flag) 
    typical_offset = NaCell_get_typical_offset (p_epics_params->Epics_val) * 1.1 ; /* variable offset + 10% */
  else
    typical_offset = 0.7 *   epics_constants.max_offset;  /* constant offset */

  if(dd[6])printf(" and with typical offset = %f\n",typical_offset);

  status = EpicsSet_step(  
			 p_epics_params->Epics_val,
			 check_offset, 
			 typical_offset,
			 p_epics_params->Epics_inc, 
			 Diff, 
			 &p_epics_params->Epics_read, 
			 &Offset,
			 epics_constants);
  
  if(dd[5])
    {
      printf("EpicsIncr: EpicsSet_step returns with status =%d\n",status);
      printf("      and Read value=%.4f, offset = %.4f\n",p_epics_params->Epics_read,Offset);
    }
  switch(status)
    {
    case(0):
      if(dd[5])
	printf("EpicsIncr: Success - Accepting set point %.4f%s (read %.4f%s)\n",
	       p_epics_params->Epics_val, p_epics_params->Epics_units, p_epics_params->Epics_read, p_epics_params->Epics_units);
      else /* do not remove these spaces or line is overwritten and cannot be read */
	{	
	  if(!dd[11])printf("\r                                             Set %.4f%s (Read  %.4f%s)",
		 p_epics_params->Epics_val, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units );
	}
      p_epics_params->Epics_last_offset = Offset;  /* remember this good value */
      return SUCCESS;
      break;
    case (-1):
      /* Cannot get a connection to Epics or error from caread/write; need to stop the run   */  
      return (-1);
    case(-2):
      return (-1); /* error from caread/write or  may not longer have a connection */
      break;
    case (-3):
      printf("EpicsIncr: Info: accepting set point  %.4f%s (read %.4f%s) since read/write values within allowed offset (%.4f%s)\n",
	     p_epics_params->Epics_val, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units,
	     check_offset, p_epics_params->Epics_units );
      p_epics_params->Epics_last_offset = Offset;  /* remember this value */
      return SUCCESS;
      break;
    case(-4):
      printf("EpicsIncr: %s not responding at set point %.4f%s (read %.4f%s) actual offset=%.4f, allowed offset is %.4f%s \n", 
	     fs.output.e1n_epics_device, p_epics_params->Epics_val, p_epics_params->Epics_units, p_epics_params->Epics_read,
	     p_epics_params->Epics_units, Offset, check_offset, p_epics_params->Epics_units );
      /* if callbacks are working, we should not need to repeat ..... 
	    -  if callbacks are NOT working, repeating just queues up set values
	        may repeat once after a long delay */
      cm_msg(MERROR,"EpicsIncr","Cannot change %s value at set value = %.4f%s (read %.4f%s)",
	     fs.output.e1n_epics_device, p_epics_params->Epics_val, p_epics_params->Epics_units,
	     p_epics_params->Epics_read, p_epics_params->Epics_units);
      return status; 
      break;
    case(-5):
      printf("EpicsIncr: %s is still at old setpoint (read %.4f%s); new setpoint= %.4f%s (actual offset=%.4f, allowed offset=%.4f) \n",
	     fs.output.e1n_epics_device, p_epics_params->Epics_read, p_epics_params->Epics_units,
	     p_epics_params->Epics_val, p_epics_params->Epics_units, Offset, check_offset);
      return (-5); /* value has not changed */
      break;
    default:
      printf("EpicsIncr: Unknown return code %d from EpicsSet_step\n",status);
      return status;
      break;
    } /* end of switch for EpicsSet_step */
  
#else
  printf("EpicsIncr: EPICS_ACCESS not defined; would set  %s value to %.4f%s \n",
	 fs.output.e1n_epics_device,p_epics_params->Epics_val, p_epics_params->Epics_units);
#endif
  return (SUCCESS);
  
  
}  /* end of EpicsIncr */



/************************************************************************************/
INT EpicsCheck( EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{
  /* called by cycle_start to check Epics device  is at the setpoint
     if not, calls EpicsSet 
  */
  
  INT status;
  float ftemp,rtemp,stemp;
  float typical_offset;
  
  epics_constants.debug=dd[6];  /* update debugging for EpicsStep */

  if(dd[5])
    {
      printf("\nEpicsCheck starting\n");
      
      /* Epics constants should have been set up by Laser_init or NaCell_init */
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }
  
#ifdef EPICS_ACCESS
  
  if(dd[5])printf("EpicsCheck: calling EpicsRead\n");
  /* check if Epics is  set to the correct value */
  status = EpicsRead( &ftemp, p_epics_params);
  if(status!=SUCCESS)
    {	    
      printf("EpicsCheck: Bad status returned from EpicsRead for (%d)\n",status);
      return FE_ERR_HW;
    }
  
  
  printf("EpicsCheck: set value %.4f%s, just read back = %.4f%s Epics_last_offset=%f\n",
	 p_epics_params->Epics_val, p_epics_params->Epics_units,ftemp, p_epics_params->Epics_units,p_epics_params->Epics_last_offset);
  if ( fabs (p_epics_params->Epics_val - ftemp) <= fabs(p_epics_params->Epics_last_offset))
    {
      p_epics_params->Epics_read = ftemp;      
      printf("EpicsCheck: Success: set value (%.4f%s) and read value (%.4f%s) are within last offset of %.4f\n",
	     p_epics_params->Epics_val, p_epics_params->Epics_units, p_epics_params->Epics_read ,
	     p_epics_params->Epics_units, p_epics_params->Epics_last_offset);
      return(SUCCESS);
    }
    
  /* Unless last offset was zero (may not have set even the first value)
     let's widen the offset by adding the stability */
  if (p_epics_params->Epics_last_offset != 0)
    {
      stemp = fabs(p_epics_params->Epics_last_offset) + fabs (p_epics_params-> Epics_stability); 
      if(stemp >= fabs(p_epics_params->Epics_inc))
	{   /* make sure it's less than the increment */
	  rtemp = ( fabs(p_epics_params->Epics_inc) - fabs (p_epics_params-> Epics_stability));
	  if (rtemp >  fabs(p_epics_params->Epics_last_offset) )
	    stemp = rtemp;
	}
    }
  else
    stemp =  fabs(p_epics_params->Epics_last_offset);

   if(p_epics_params->NaCell_flag) 
    typical_offset = NaCell_get_typical_offset (p_epics_params->Epics_val); /* variable offset */
  else
    typical_offset = 0.7 *   epics_constants.max_offset;  /* constant offset */
   /* typical_offset and stemp are the absolute value */
   if(stemp> typical_offset)stemp=typical_offset;

  printf("EpicsCheck: Read back Epics value as %.4f%s; attempting to set to %.4f%s using acceptable offset=%.4f (device typical offset=%.4f) \n",
	 ftemp, p_epics_params->Epics_units, p_epics_params->Epics_val, p_epics_params->Epics_units, stemp, typical_offset);
  
 
  /* EpicsSet2 sets and reads back Epics value  */
  status=EpicsSet2(
		   p_epics_params->Epics_val,
		   stemp,/* last offset value to check against */
		   typical_offset, /* typical offset at this setpoint */
		   &p_epics_params->Epics_read,
		   &Offset,
		   epics_constants);
  if(dd[5])
    {
      printf("EpicsCheck: EpicsSet2 returns with status =%d\n",status);
      printf("      and Read value=%.4f%s, offset = %.4f\n",p_epics_params->Epics_read, p_epics_params->Epics_units,Offset );
    }
  switch(status)
    {
    case(0):
      printf("EpicsCheck: Success after EpicsSet2; now at setpoint = %.4f%s, read back %.4f%s, offset %.4f \n",
	     p_epics_params->Epics_val , p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units, Offset);
      p_epics_params->Epics_bad_flag=0;  /* initialize global counter */
      p_epics_params->Epics_last_offset=Offset;
      return (SUCCESS);
      break;
    case(-1):
      printf("EpicsCheck: cannot access Epics devices\n");
      return (-1);
    case(-2):
      printf("EpicsCheck: EpicsSet2 detects invalid parameters\n");
      return (-2);
    case(-4):
      printf("EpicsCheck: %s value not responding at setpoint %.4f%s (read %.4f%s) last acceptable offset=%.4f device typical offset=%.4f\n",
	     fs.output.e1n_epics_device, p_epics_params->Epics_val, p_epics_params->Epics_units,
	     p_epics_params->Epics_read, p_epics_params->Epics_units,
	     p_epics_params->Epics_last_offset, typical_offset);
      
      printf("EpicsCheck: Failure from EpicsSet2 (repeat) at set point %.4f%s (%d)\n",p_epics_params->Epics_val, 
	     p_epics_params->Epics_units, status);
      /*      printf("EpicsCheck: Check if %s is ON\n",fs.output.e1n_epics_device, p_epics_params->Epics_val, status);      
	      cm_msg(MERROR,"EpicsCheck","Cannot change %s value at set value = %.4f (read %.4f)",
	      fs.output.e1n_epics_device, p_epics_params->Epics_val, p_epics_params->Epics_read);
	      cm_msg(MERROR,"EpicsCheck","Check status of %s then restart",fs.output.e1n_epics_device);
	      cm_msg(MTALK,"EpicsCheck","Can't set %s device value",fs.output.e1n_epics_device); */

      p_epics_params->Epics_bad_flag++; /* used to stop trying to set the Epics after Epics_bad_flag = 5  */

      return FE_ERR_HW; /* return error */
      break;
    default:
      printf("EpicsCheck: Invalid return status from EpicsSet2 (%d)\n",status);
      return (FE_ERR_HW);
      break;
    }	/* end of switch for EpicsSet2 */
  
  
  /*   S U C C E S S  */
  /* Epics value has been set successfully */
  
#else
  printf("EpicsCheck: DUMMY access to Epics Device  %s\n",fs.output.e1n_epics_device);
#endif
  return (SUCCESS);
}


/************************************************************************************/
INT  EpicsNewScan(EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /* called by cycle_start at each new scan 

     - sets value to p_epics_params->Epics_val (unless at Begin_Of_Run, as device has 
     already been set to starting point)

     at begin of a scan, usually set Epics device to 1 increment different from scan start.
  */

  INT status;
  float Value_tmp;
  float typical_offset;

  epics_constants.debug=dd[6];  /* update debugging for EpicsStep */

  if(dd[5])
    {
      printf("\nEpicsNewScan starting\n");

      /* epics_constants was filled in Laser_init or NaCell_init */
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f, debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }
#ifdef EPICS_ACCESS	  
  
  /* set Epics value to this value (1 incr. away from start value) now for two reasons:
     1. So that the read_inc will be within range for the check later on
     2. To give the Epics device a chance to reach the start value if there is a large sweep */
  
  Value_tmp = p_epics_params->Epics_val;
  /* make sure we are within range - if not get within range by adding/subtracting 2 increments */
  if(Value_tmp > epics_constants.max_set_value ) Value_tmp = p_epics_params->Epics_val - 2* fabs(p_epics_params->Epics_inc) ;
  if(Value_tmp < epics_constants.min_set_value ) Value_tmp = p_epics_params->Epics_val + 2* fabs(p_epics_params->Epics_inc)  ;
  
    if(p_epics_params->NaCell_flag) 
      typical_offset = NaCell_get_typical_offset(Value_tmp); /* variable offset */
    else
      typical_offset = 0.7 *   epics_constants.max_offset;  /* constant offset */

    /* EpicsSet sets & reads back  & checks  Epics value & returns the offset */
    if(dd[5])printf("\n EpicsNewScan: NEW SCAN -  calling EpicsSet with pre-start value of %.4f%s and typical offset of %.4f\n",
		 Value_tmp, p_epics_params->Epics_units,typical_offset);
    status=EpicsSet( Value_tmp,
		     typical_offset,
		     &p_epics_params->Epics_read,
		     &Offset,
		     epics_constants);
    if(dd[5])
    {
      printf("EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.4f%s, offset = %.4f\n",p_epics_params->Epics_read, p_epics_params->Epics_units,Offset);
    }
      switch(status)
	{
	case(0):
	  if(dd[5])printf("Success after EpicsSet at setpoint = %.4f%s, read back %.4f%s (new scan)\n",
		       Value_tmp, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units);
	  p_epics_params->Epics_last_offset=Offset; /* remember this acceptable offset */
	  break;
	case(-1):
	  printf("EpicsNewScan: EpicsSet indicates may have lost connection to Epics Device %s\n",p_epics_params->Epics_units);
	  return (-1);
	case(-2):
	  printf("EpicsNewScan: EpicsSet indicates invalid input parameters supplied for Epics Device at %.4f%s\n",Value_tmp, p_epics_params->Epics_units);

	  return (-2); /* bad params */
	case(-4):
	  printf("EpicsNewScan: %s value not responding at setpoint %.4f%s (read back %.4f%s)\n",
		 fs.output.e1n_epics_device, Value_tmp, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units);
	default:
	  printf("EpicsNewScan: Failure from EpicsSet (new scan) at setpoint %.4f%s (%d)\n",Value_tmp, p_epics_params->Epics_units,status);
	  cm_msg(MERROR, "EpicsNewScan", "cannot set %s value to %.4f%s (read %.4f%s)",
		 fs.output.e1n_epics_device,Value_tmp, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units);
	  cm_msg(MTALK,"EpicsNewScan","can't set %s value",fs.output.e1n_epics_device);
	  return DB_NO_ACCESS;
	}  
      p_epics_params->Epics_last_value = p_epics_params->Epics_read;  /* new scan:  set this for Epics_stability check later */
#else
      printf("EpicsNewScan: DUMMY access to %s\n",fs.output.e1n_epics_device);
#endif
  return(SUCCESS); /* end of new scan */
}



/*****************************************************************************************/
INT  NaCell_init(BOOL flip, EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /*   Initialize Na/Rb Cell
       Called from begin of run 
  */ 
  
  float NaVolt_tmp;
   
  INT status;
  float Na_typical_offset; /* not a constant for NaCell */

  /* Check we have channel IDs for NaCell access */
#ifdef EPICS_ACCESS
  if (p_epics_params->XxRchid != -1)
    if (!caCheck(p_epics_params->XxRchid)) p_epics_params->XxRchid=-1;
  
  if (p_epics_params->XxWchid != -1)
    if (!caCheck(p_epics_params->XxWchid)) p_epics_params->XxWchid=-1;
  
  
  if (p_epics_params->XxRchid == -1 || p_epics_params->XxWchid == -1)  /* no access */
    {   /* try again for access to NaCell */      
      printf("NaCell_init: attempting to get epics channel ID for access to Na/Rb Cell ....\n");
      p_epics_params->XxRchid=p_epics_params->XxWchid= -1; /* initialize both channels to a bad value */
      
      status = EpicsReconnect(p_epics_params);
      if(status!=SUCCESS)
	{
	  printf("NaCell_init: Cannot get epics channel ID; access to Na/Rb Cell is not possible\n");
	  cm_msg(MERROR,"NaCell_init","Cannot get epics channel ID; access to Na/Rb Cell is not possible");
	  return (DB_NO_ACCESS);
	}
      printf("NaCell_init: caGetId returns channel ID for read: %d and write: %d\n",
	     p_epics_params->XxRchid,p_epics_params->XxWchid);
    }
#else
  printf("NaCell_init: DUMMY access to Na/Rb Cell in this code\n");
#endif
  
  sprintf(p_epics_params->Epics_units,"%s",NaUnits);
  p_epics_params->Epics_start = fs.input.navolt_start;
  Epics_stop = fs.input.navolt_stop;
  p_epics_params->Epics_inc = fs.input.navolt_inc;
  if( p_epics_params->Epics_inc == 0 )
    {
      cm_msg(MERROR,"NaCell_init","Invalid increment value: %f volt(s)",
	     p_epics_params->Epics_inc);
      return FE_ERR_HW;
    }
  
  if(    p_epics_params->Epics_start < Na_min  || Epics_stop < Na_min || p_epics_params->Epics_start > Na_max || Epics_stop > Na_max)
    {
      cm_msg(MERROR,"NaCell_init","NaVolt start (%.2f) or stop (%.2f) parameter(s) out of range. Allowed range is between %.2f and %.2f volts",
	     p_epics_params->Epics_start, Epics_stop,  Na_min, Na_max);
      return FE_ERR_HW;
    }
  
  if( ((p_epics_params->Epics_inc > 0) && (Epics_stop < p_epics_params->Epics_start)) ||
      ((p_epics_params->Epics_inc < 0) && (p_epics_params->Epics_start < Epics_stop)) )
    p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_ninc = ((Epics_stop - p_epics_params->Epics_start)/p_epics_params->Epics_inc + 1.); 
  if( p_epics_params->Epics_ninc < 1)
    {
      cm_msg(MERROR,"NaCell_init","Invalid number of voltage steps: %d",
	     p_epics_params->Epics_ninc);
      return FE_ERR_HW;
    }
  


  printf("NaCell_init: Selected %d increments starting at %.2f volt(s) by steps of %.2f volt(s)\n",
	 p_epics_params->Epics_ninc,p_epics_params->Epics_start,p_epics_params->Epics_inc);


  printf("NaCell_init: flip = %d\n",flip);
  if(flip) p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_val = p_epics_params->Epics_start;
  
  

  if (fabs (p_epics_params->Epics_inc) < Nainc_min )  /* use a minimum increment of currently 0.3V */
    {
      cm_msg(MERROR,"NaCell_init","increment (%.2f) must be at least %.2f",
	     p_epics_params->Epics_inc,Nainc_min);
      return FE_ERR_HW;
    }
  else if  (fabs (p_epics_params->Epics_inc) < Nainc_limit )
    {
      Diff = Nadiff_min;
      p_epics_params->Epics_stability = Nadiff_min *2 ;
    } 
  else
    {
      Diff = Nadiff_max; /* use default constant  value for comparison */
      p_epics_params->Epics_stability = Nadiff_min + 0.15 ;
    }
  if(dd[5])printf("NaCell_init: Calculated Diff = %.2fV & Epics_stability = %.2f V\n",Diff,p_epics_params->Epics_stability);
  
  
  
  /* 
     check whether we can access the NaCell 
  */
  NaVolt_tmp = p_epics_params->Epics_start - p_epics_params->Epics_inc; /* set a value one incr. away from Epics_start */
  /* make sure this is within range  */
  if(NaVolt_tmp > Na_max || NaVolt_tmp < Na_min ) 
    NaVolt_tmp =  p_epics_params->Epics_start + p_epics_params->Epics_inc ; 
  
  printf("NaCell_init: Initial setpoint (%.2f volts) will be 1 increment away from start value\n",
	 NaVolt_tmp);
  
  /* load up some constants needed by EpicsSet and EpicsSet_Step; these are now independent of device (NaCell or NaCell) */
  epics_constants.Rchan_id = p_epics_params->XxRchid ;
  epics_constants.Wchan_id = p_epics_params->XxWchid ;
  epics_constants.max_offset = Na_max_offset;  /* use an initial large value */
  epics_constants.min_set_value = Na_min;
  epics_constants.max_set_value=  Na_max;
  epics_constants.stability = Na_typical_stability;
  epics_constants.debug=dd[6];  /* debug EpicsStep */
  Na_typical_offset = NaCell_get_typical_offset(NaVolt_tmp) ;


  if(dd[5])
    {
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.2f  min_set_value=%.2f max_set_value=%.2f stability=%.2f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }

#ifdef EPICS_ACCESS
  /* EpicsSet sets and reads back value to check access */
  status=EpicsSet(	NaVolt_tmp,
			Na_typical_offset,
		&p_epics_params->Epics_read,
		&Offset,
		epics_constants);
  if(dd[5])
    {
      printf("NaCell_init: EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.2f, offset = %.2f\n",p_epics_params->Epics_read,Offset);
    }
  switch(status)
    {
    case(0):
      printf("NaCell_init: Success after EpicsSet at setpoint = %.2f, read back %.2f\n",
	     NaVolt_tmp, p_epics_params->Epics_read);
      p_epics_params->Epics_last_offset=Offset; /* remember last offset */
      break;
    case(-2):
      printf("NaCell_init: EpicsSet detects invalid parameters\n");
    case(-4):
      printf("NaCell_init: Na/Rb Cell voltage not responding (could be switched off)\n");
      printf("              at setpoint = %.2f, read back %.2f\n",NaVolt_tmp,p_epics_params->Epics_read);
    default: 
      printf("NaCell_init: Failure from EpicsSet (%d)\n",status);
     cm_msg(MERROR, "NaCell_init", "cannot set Na/Rb Cell value. Check BNQR expt is granted Epics write access");
      cm_msg(MERROR, "NaCell_init", " and that Na/Rb Cell is switched on (see EPICS Main Control Page) ");
      return DB_NO_ACCESS;
    }
#else
  printf("NaCell_init: Info - DUMMY - would have set Na/Rb Cell to setpoint = %.2f \n", NaVolt_tmp);
#endif
  return(SUCCESS);

} /* end of NaCell_init */







/*****************************************************************************************/
INT  Laser_init(BOOL flip, EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /*   Initialize Laser
       Called from begin of run 
   */ 

  float LaVolt_tmp; 
  float La_typical_offset;
  INT status;

      /* Check we have channel IDs for Laser access */
#ifdef EPICS_ACCESS
  if (p_epics_params->XxRchid != -1)
      if (!caCheck(p_epics_params->XxRchid)) p_epics_params->XxRchid=-1;

  if (p_epics_params->XxWchid != -1)
      if (!caCheck(p_epics_params->XxWchid)) p_epics_params->XxWchid=-1;


  if (p_epics_params->XxRchid == -1 || p_epics_params->XxWchid == -1)  /* no access */
    {   /* try again for access to Laser */      
      printf("Laser_init: attempting to get epics channel ID for access to Laser ....\n");
      p_epics_params->XxRchid=p_epics_params->XxWchid= -1; /* initialize both channels to a bad value */
      status = EpicsReconnect(p_epics_params);
      if(status !=SUCCESS)
	{
	  printf("Laser_init: Cannot get epics channel ID; access to Laser is not possible\n");
	  cm_msg(MERROR,"Laser_init","Cannot get epics channel ID; access to Laser is not possible");
	  return (DB_NO_ACCESS);
	}
      printf("Laser_init: caGetId returns channel ID for read: %d and write: %d\n",
	     p_epics_params->XxRchid,p_epics_params->XxWchid);
    }
#else
  printf("Laser_init: DUMMY access to Laser in this code\n");
#endif

  sprintf(p_epics_params->Epics_units,"%s",LaUnits);

  p_epics_params->Epics_start = fs.input.laser_start;
  Epics_stop = fs.input.laser_stop;
  p_epics_params->Epics_inc = fs.input.laser_inc;
  if( p_epics_params->Epics_inc == 0 )
    {
      cm_msg(MERROR,"Laser_init","Invalid increment value: %f volt(s)",
	     p_epics_params->Epics_inc);
      return FE_ERR_HW;
    }
  
  if(    p_epics_params->Epics_start < La_min  || Epics_stop < La_min || p_epics_params->Epics_start > La_max || Epics_stop > La_max)
    {
      cm_msg(MERROR,"Laser_init","LaVolt start (%.4f) or stop (%.4f) parameter(s) out of range. Allowed range is between %.4f and %.4f volts",
	     p_epics_params->Epics_start, Epics_stop,  La_min, La_max);
      return FE_ERR_HW;
    }
  
  if( ((p_epics_params->Epics_inc > 0) && (Epics_stop < p_epics_params->Epics_start)) ||
      ((p_epics_params->Epics_inc < 0) && (p_epics_params->Epics_start < Epics_stop)) )
    p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_ninc = ((Epics_stop - p_epics_params->Epics_start)/p_epics_params->Epics_inc + 1.); 
  if( p_epics_params->Epics_ninc < 1)
    {
      cm_msg(MERROR,"Laser_init","Invalid number of Voltage steps: %d",
	     p_epics_params->Epics_ninc);
      return FE_ERR_HW;
    }

  printf("Laser_init: Selected %d increments starting at %.4f volt(s) by steps of %.4f volt(s)\n",
	 p_epics_params->Epics_ninc,p_epics_params->Epics_start,p_epics_params->Epics_inc);


  printf("Laser_init: flip = %d\n",flip);
  if(flip) p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_val = p_epics_params->Epics_start;
  
  if (fabs (p_epics_params->Epics_inc) < Lainc_min )  /* use a minimum increment of currently 0.3V */
    {
      cm_msg(MERROR,"Laser_init","LaVolt increment (%.4f) must be at least %.4f",
	     p_epics_params->Epics_inc,Lainc_min);
      return FE_ERR_HW;
    }
  else if  (fabs (p_epics_params->Epics_inc) < Lainc_limit )
    {
      Diff = Ladiff_min;
      p_epics_params->Epics_stability = Ladiff_min *2 ;
    } 
  else
    {
      Diff = Ladiff_max; /* use default constant  value for comparison */
      p_epics_params->Epics_stability = Ladiff_min + 0.1 ;
    }
  if(dd[5])printf("Laser_init: Calculated Diff = %.4fV & Epics_stability = %.4f V\n",Diff,p_epics_params->Epics_stability);
  
  
      /* Epics_ninc = number of SuperCycles needed for a scan */   
      /* fs.hardware.num_cycles = Epics_ninc*NSCAN*Ncycle_per_supercycle */

      /*  JUN 2001 - cannot reliably stop a run ; comment this out for now
	  if (N_Scans_wanted > 0)
	  {
	  fs.hardware.num_cycles = Epics_ninc * N_Scans_wanted;
	  if(flip)fs.hardware.num_cycles *= 2.0 ;
	  } else {
	  fs.hardware.num_cycles = 0;
	  }*/
  /* don't do this within this function as hDB,hFS are not defined here */
  //  fs.hardware.num_cycles = 0; /* make sure num_cycles is set to zero Jun 2001 */
  //  db_set_value(hDB, hFS, "hardware/num cycles", &fs.hardware.num_cycles, 4, 1, TID_DWORD);
   
  
  /* 
     check whether we can access the Laser 
  */
  LaVolt_tmp = p_epics_params->Epics_start - p_epics_params->Epics_inc; /* set a value one incr. away from Epics_start */
  /* make sure this is within range  */
  if(LaVolt_tmp > La_max || LaVolt_tmp < La_min ) 
    LaVolt_tmp =  p_epics_params->Epics_start + p_epics_params->Epics_inc ; 
  
  printf("Laser_init: Initial setpoint (%.4f volts) will be 1 increment away from start value\n",
	 LaVolt_tmp);
#ifdef EPICS_ACCESS
  /* If XxWchid or XxRchid are -1, we have no access */
  if(p_epics_params->XxWchid < 0 || p_epics_params->XxRchid < 0 )
    {
      printf("Invalid channel ID for Laser direct access Write=%d, Read=%d\n",
	     p_epics_params->XxWchid,p_epics_params->XxRchid);
      cm_msg(MERROR, "Laser_init", "Invalid chID for Laser - cannot access La Cell");
      return DB_NO_ACCESS;
    }
#endif
  /* load up some constants needed by EpicsSet and EpicsSet_Step; these are now independent of device (Laser or NaCell) */
  epics_constants.Rchan_id = p_epics_params->XxRchid ;
  epics_constants.Wchan_id = p_epics_params->XxWchid ;
  epics_constants.max_offset = La_max_offset;  /* use an initial large value */
  epics_constants.min_set_value = La_min;
  epics_constants.max_set_value=  La_max;
  epics_constants.stability = La_typical_stability;
  epics_constants.debug=dd[6]; /* debug EpicsStep */
  La_typical_offset = 0.7 * La_max_offset ; /* constant offset value for Laser */


  if(dd[5])
    {
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }

#ifdef EPICS_ACCESS
  /* EpicsSet sets and reads back Laser voltage to check access */
  status=EpicsSet(	LaVolt_tmp,
			La_typical_offset,
		&p_epics_params->Epics_read,
		&Offset,epics_constants );
  if(dd[5])
    {
      printf("Laser_init: EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.4f, offset = %.4f\n",p_epics_params->Epics_read,Offset);
    }
  switch(status)
    {
    case(0):
      printf("Laser_init: Success after EpicsSet at setpoint = %.4f, read back %.4f\n",
	     LaVolt_tmp, p_epics_params->Epics_read);
      p_epics_params->Epics_last_offset=Offset; /* remember good value */
      break;
    case(-2):
      printf("Laser_init: EpicsSet detects invalid parameters\n");
    case(-4):
      printf("Laser_init: Laser voltage not responding (could be switched off)\n");
      printf("              at setpoint = %.4f, read back %.4f\n",LaVolt_tmp,p_epics_params->Epics_read);
    default: 
      printf("Laser_init: Failure from EpicsSet (%d)\n",status);
      cm_msg(MERROR, "Laser_init", "cannot set Lsser value. Check BNQR expt is granted Epics write access");
      cm_msg(MERROR, "Laser_init", " and that Laser is switched on (see EPICS Main Control Page) ");
      return DB_NO_ACCESS;
    }
#else
  printf("Laser_init: Info - DUMMY - would set  Laser to setpoint = %.4f \n", LaVolt_tmp);
#endif
  return SUCCESS;
} /* end of Laser_init */





/*****************************************************************************************/
INT  Field_init(BOOL flip, EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /*   Initialize Field
       Called from begin of run 
   */ 

  float FdCurr_tmp;
  float start_amps; /* input params are in Gauss */
  float stop_amps;
  float inc_amps;
  float Fd_min_G,Fd_max_G,Fdinc_min_G;
  float Fd_typical_offset;
  

  INT status;

  /* We need to convert  Gauss to Amps */ 
  start_amps =  convert_to_amps (fs.input.field_start);
  stop_amps =  convert_to_amps (fs.input.field_stop);
  inc_amps =  convert_to_amps (fs.input.field_inc);

  /* and Amps to Gauss for messages */
  Fd_min_G =  convert_to_gauss (Fd_min);
  Fd_max_G =  convert_to_gauss (Fd_max);
  Fdinc_min_G = convert_to_gauss (Fdinc_min);
  /* if(dd[5]) */
    {
      printf("scan start %.2f Gauss;  stop %.2f Gauss; inc %.2f Gauss \n",
	     fs.input.field_start, fs.input.field_stop,  fs.input.field_inc);
      printf("scan start %.2f Amps;   stop %.2f Amps;  inc %.2f Amps\n",
	     start_amps, stop_amps, inc_amps);

      printf("Permitted range is %.2fA to %.2fA, min step=%.2fA ; or %.2fG to %.2fG, min step=%.2fG\n",
	     Fd_min,Fd_max,Fdinc_min, Fd_min_G,Fd_max_G, Fdinc_min_G);
    }
  sprintf(p_epics_params->Epics_units,"%s",FdUnits); /* scan units */
  p_epics_params->Epics_start = start_amps;
  Epics_stop = stop_amps;
  p_epics_params->Epics_inc = inc_amps;
  if( p_epics_params->Epics_inc == 0 )
    {
      cm_msg(MERROR,"Field_init","Invalid increment value: %f amps (%.2f Gauss)",
	     p_epics_params->Epics_inc, fs.input.field_inc);
      return FE_ERR_HW;
    }

  /* Check the start parameter is in range */  
  if(p_epics_params->Epics_start < Fd_min  || p_epics_params->Epics_start > Fd_max)
    {
      printf("Field_init: Field scan start parameter (%.2fA)  is out of range. Allowed range is between %.2f and %.2f amps\n", p_epics_params->Epics_start, Fd_min, Fd_max);
      /* convert to gauss for the user */
      
      cm_msg(MERROR,"Field_init","Field start parameter ( %.2f G )  is out of range. Allowed range is between %.2f and %.2f Gauss",
	     fs.input.field_start,  Fd_min_G, Fd_max_G);
      return FE_ERR_HW;
    }
  
  /* Check the stop parameter is in range */
  
  if(Epics_stop < Fd_min  || Epics_stop > Fd_max)
    {
      printf("Field_init: Field scan stop parameter (%.2fA)  is out of range. Allowed range is between %.2f and %.2f amps\n", Epics_stop, Fd_min, Fd_max);
      /* convert to gauss for the user */
      cm_msg(MERROR,"Field_init","Field stop parameter ( %.2f G )  is out of range. Allowed range is between %.2f and %.2f Gauss",
	     fs.input.field_stop, Fd_min_G, Fd_max_G);
      return FE_ERR_HW;
    }

  /* Check the increment parameter is in range */  
  if(p_epics_params->Epics_inc < Fdinc_min)
    {
      printf("Field_init: Field scan increment parameter (%.2fA) is too small. Minimum increment is %.2fA\n", 
	     p_epics_params->Epics_inc, Fdinc_min);
      /* convert to gauss for the user */
      
      cm_msg(MERROR,"Field_init","Field start parameter (%.2fG) is too small. Minimum increment is %.2fG",
	     fs.input.field_start,  Fdinc_min_G);
      return FE_ERR_HW;
    }
  
  /* Check number of increments */
  if( ((p_epics_params->Epics_inc > 0) && (Epics_stop < p_epics_params->Epics_start)) ||
      ((p_epics_params->Epics_inc < 0) && (p_epics_params->Epics_start < Epics_stop)) )
    p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_ninc = ((Epics_stop - p_epics_params->Epics_start)/p_epics_params->Epics_inc + 1.); 
  if( p_epics_params->Epics_ninc < 1)
    {
      cm_msg(MERROR,"Field_init","Invalid number of  steps: %d",
	     p_epics_params->Epics_ninc);
      return FE_ERR_HW;
    }
  printf("Field_init: Selected %d increments starting at %.2fG (%.2fA) in steps of %.2fG (%.2fA)\n",
	 p_epics_params->Epics_ninc,
	 fs.input.field_start, p_epics_params->Epics_start,
	 fs.input.field_inc,  p_epics_params->Epics_inc);
  if(dd[5])printf("Field_init: flip = %d\n",flip);
  if(flip) p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_val = p_epics_params->Epics_start;
  
  
 
  if (fabs (p_epics_params->Epics_inc) < Fdinc_min )  /* use a minimum increment of currently 0.3V */
    {
      cm_msg(MERROR,"Field_init","increment %.2f G (%.2fA) must be at least %.2f G (%.2f A)",
	     convert_to_gauss (p_epics_params->Epics_inc),p_epics_params->Epics_inc ,
	     convert_to_gauss (Fdinc_min), Fdinc_min);
      return FE_ERR_HW;
    }
  else if  (fabs (p_epics_params->Epics_inc) < Fdinc_limit )
    {
      Diff = Fddiff_min;
      p_epics_params->Epics_stability = Fddiff_min *2 ;
    } 
  else
    {
      Diff = Fddiff_max; /* use default constant  value for comparison */
      p_epics_params->Epics_stability = Fddiff_min + 0.1 ;
    }
  if(dd[5])printf("Field_init: Calculated Diff = %.2fA & Epics_stability = %.2f A\n",Diff,p_epics_params->Epics_stability);
  
  FdCurr_tmp = p_epics_params->Epics_start - p_epics_params->Epics_inc; /* set a value one incr. away from Epics_start */
  /* make sure this is within range  */
  if(FdCurr_tmp > Fd_max || FdCurr_tmp < Fd_min ) 
    FdCurr_tmp =  p_epics_params->Epics_start + p_epics_params->Epics_inc ; 
  
  if(dd[5])printf("Field_init: Initial setpoint (%.2f amps) will be 1 increment away from start value\n",
	 FdCurr_tmp);

#ifdef EPICS_ACCESS
  /* If XxWchid or XxRchid are -1, we have no access */
  if (p_epics_params->XxRchid != -1)
      if (!caCheck(p_epics_params->XxRchid)) p_epics_params->XxWchid=-1;

  if (p_epics_params->XxWchid != -1)
      if (!caCheck(p_epics_params->XxWchid)) p_epics_params->XxWchid=-1;


  if (p_epics_params->XxRchid == -1 || p_epics_params->XxWchid == -1)  /* no access */
    {   /* try again for access to NaCell */      
      printf("Field_init: attempting to get epics channel ID for access to Field ....\n");

      status = EpicsReconnect(p_epics_params);
      if(status!=SUCCESS)
	{
	  printf("Field_init: Cannot get epics channel ID; access to Field is not possible\n");
	  cm_msg(MERROR,"Field_init","Cannot get epics channel ID; access to Field is not possible");
	  return (DB_NO_ACCESS);
	}
      printf("Field_init: caGetId returns channel ID for read: %d and write: %d\n",
	     p_epics_params->XxRchid,p_epics_params->XxWchid);
    }
#else
  printf("Field_init: DUMMY access to NaCell in this code\n");
#endif

  

  /* load up some constants needed by EpicsSet and EpicsSet_Step; these are now independent of device (Field or Field) */
  epics_constants.Rchan_id = p_epics_params->XxRchid ;
  epics_constants.Wchan_id = p_epics_params->XxWchid ;
  epics_constants.max_offset = Fd_max_offset;  /* use an initial large value */
  epics_constants.min_set_value = Fd_min;
  epics_constants.max_set_value=  Fd_max;
  epics_constants.stability = Fd_typical_stability;
  epics_constants.debug=dd[6]; /* debug EpicsStep */

  Fd_typical_offset = 0.7 * Fd_max_offset ; /* constant offset value for Field */

  if(dd[5])
    {
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.2f  min_set_value=%.2f A max_set_value=%.2f A stability=%.2f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }

#ifdef EPICS_ACCESS
  /* EpicsSet sets and reads back Field value to check access */
  status=EpicsSet(	FdCurr_tmp,
			Fd_typical_offset,
		&p_epics_params->Epics_read,
		&Offset,
		epics_constants);
  if(dd[5])
    {
      printf("Field_init: EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.2f, offset = %.2f\n",p_epics_params->Epics_read,Offset);
    }
  switch(status)
    {
    case(0):
      printf("Field_init: Success after EpicsSet at setpoint = %.2f%s, read back %.2f%s\n",
	     FdCurr_tmp,p_epics_params->Epics_units, p_epics_params->Epics_read,p_epics_params->Epics_units );
      p_epics_params->Epics_last_offset=Offset; /* remember good value */
      break;
    case(-2):
      printf("Field_init: EpicsSet detects invalid parameters\n");
    case(-4):
      printf("Field_init: Field value not responding (could be switched off)\n");
      printf("              at setpoint = %.2f A, read back %.2f A\n",FdCurr_tmp,p_epics_params->Epics_read);
    default: 
      printf("Field_init: Failure from EpicsSet (%d)\n",status);
      cm_msg(MERROR, "Field_init", "cannot set Field value. Check BNQR expt is granted Epics write access");
      cm_msg(MERROR, "Field_init", " and that Field is switched on (see EPICS Main Control Page) ");
      return DB_NO_ACCESS;
    }
#else
  printf("Field_init: Info - DUMMY - would have set Field to setpoint = %.2f A \n", FdCurr_tmp);
#endif
  return(SUCCESS);

} /* end of Field_init */


/* Conversion routines for FIELD  

   Zaher's formula for conversion:
H= A*I +B

H=field (gauss)
A=2.228 G/Amp
B=-0.08 G

Now A=2.2131 B= 0.175 
*/ 
float convert_to_gauss (float I)
{

  float H;

  H =  A * I + B;
  return (H);
}

float convert_to_amps (float H)
{

  float I;

  I = ( H - B ) / A ;
  return (I);
}
  

float NaCell_get_typical_offset (  float set_point )
{
  float typical_offset;
  /* The NaCell readback offset varies according
     to the voltage. 
     All offsets are in Volts.
  */
  /* Aug 2007 offset seem to have changed!
  if(set_point < 50)       typical_offset=0.3;
  else if(set_point < 75)  typical_offset=0.45;
  else if(set_point < 100) typical_offset=0.4;
  else if(set_point < 175) typical_offset=0.45;
  else if(set_point < 250) typical_offset=0.5;
  else if(set_point < 325) typical_offset=0.55;
  else if(set_point < 400) typical_offset=0.6;
  else if(set_point < 500) typical_offset=0.65;
  else if(set_point < 600) typical_offset=0.7;
  else if(set_point < 700) typical_offset=0.75;
  else                     typical_offset=0.8;
  
  Offset = read - set                    
e.g wrote 20 read 19.2264  -> offset ~ -0.78  */
  /*  if(set_point < 50)       typical_offset=-0.85;
  else if(set_point < 75)  typical_offset=-0.85;
  else if(set_point < 100) typical_offset=-0.8;
  else if(set_point < 175) typical_offset=-0.75;
  else if(set_point < 250) typical_offset=-0.7;
  else if(set_point < 325) typical_offset=-0.6;
  else if(set_point < 400) typical_offset=-0.5;
  else if(set_point < 500) typical_offset=-0.45;
  else if(set_point < 600) typical_offset=-0.45;
  else if(set_point < 700) typical_offset=-0.45;
  else                     typical_offset=-0.4;
 
 
   April 2008 Offset increased again...
e.g wrote 20 read  18.8602   -> offset ~ -1.14  */
  if(set_point < 50)       typical_offset=-1.25;
  else if(set_point < 75)  typical_offset=-1.36;
  else if(set_point < 100) typical_offset=-1.32;
  else if(set_point < 200) typical_offset=-1.25;
  else if(set_point < 250) typical_offset=-1.15;
  else if(set_point < 325) typical_offset=-1.10;
  else if(set_point < 400) typical_offset=-1.10;
  else if(set_point < 500) typical_offset=-1.00;
  else if(set_point < 600) typical_offset=-0.95;
  else if(set_point < 700) typical_offset=-0.85;
  else if(set_point < 900) typical_offset=-0.75;

    else typical_offset=-0.60;

  
  
  if(dd[5])
    printf("get_typical_offset: typical offset at set_point=%.2f is %.2f\n",
	   set_point,typical_offset);
  return(typical_offset);
}


/*********************************************************************************/
INT ChannelReconnect(char *Rname)
/*********************************************************************************/
{
  INT status;

#ifdef EPICS_ACCESS
  INT max_err=1;
  INT icount;
  BOOL connected;
  INT chid;
  
  printf("\nChannelReconnect: ***  reconnecting to Epics device \"%s\" .....\n",Rname);
  
  icount=0;
  
  connected=FALSE;

  while ( ! connected && icount < max_err )
    {
      /* Clear old channel if open */
      chid = -1;
      status =caGetSingleId(Rname, &chid );
      if(status==0)
	connected=TRUE;
      
      if(connected) 
	break;
      icount++;
      printf("ChannelReconnect: could not connect to Epics channel \"%s\".....waiting 10s and retrying (%d)\n",
	     Rname,icount);
      if(icount < max_err)
	iwait(10000); /* wait 10s before retrying... includes cm_yield every second */
    }
  if(connected)
    {
      printf("ChannelReconnect: successfully reconnected to Epics channel \"%s\" \n",Rname);
      return chid;
    }
  
  cm_msg(MERROR,"ChannelReconnect",
	 "Could not reconnect to Epics channel\"%s\". Epics system may not be available ",Rname);
  return (-1);

#else
  printf("ChannelReconnect: Epics Access is not defined -> DUMMY access to Epics in this code\n");
  return SUCCESS;
#endif 
}

/*********************************************************************************/
INT ChannelWatchdog(char *Rname, INT *Rchid)
/************************************************************************************/
{
  INT status;
  float read_val; /* local variable */
  BOOL flag;
  INT chid;



  /* NOTE:  ChannelReconnect should be called for initialization before calling this routine 
        If reconnects, returns new value of Rchid 
  */
  
#ifdef EPICS_ACCESS  
  /* Maintain EPICS channel live */
  /* check that channel has not disconnected */
  
  chid = *Rchid; 
  flag=FALSE;
  /* 
     check the channel 
  */
  if(dd[7]) printf("ChannelWatchdog:  chid %d\n",chid);
  if ( !caCheck(chid) )
    {
      printf("ChannelWatchdog: channel  (chid %d) has disconnected... attempting to reconnect \n",
	   chid  );
      chid = *Rchid = -1; /* init to -1 */
      chid = ChannelReconnect(Rname);
       if(chid == -1)
	 {
	   cm_msg(MERROR, "ChannelWatchdog", "error reconnecting %s to Epics",Rname);
	   return(-1);
	 }
       else
	 {
	   printf("ChannelWatchdog: channel id is now %d\n",chid);
	   *Rchid=chid; /* return the new value */
	 }
    }
  if(dd[7])printf("ChannelWatchdog: calling caRead\n"); 
  status=caRead(chid , &read_val);
  if(status)
    {
      cm_msg(MERROR, "ChannelWatchdog", "error  reading epics value chid=%d (status=%d)",chid,status);
      return(-1);
    }
  else
    if(dd[7])printf("ChannelWatchdog read :%f [status=%d]\n", read_val, status);
#else
  printf("ChannelWatchdog: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif      
  return SUCCESS;
}


/*********************************************************************************/
void ChannelDisconnect(INT *Rchid)
/*********************************************************************************/
{
  if(*Rchid != -1)  
    ca_clear_channel((chid)*Rchid);
  *Rchid=-1;
  return;
}












