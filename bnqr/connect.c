/*   connect.c

Routines to access an Epics channel using callbacks 
   - from conn.c but all NaCell specific routines moved out into
     EpicsStep.c

Based on ca_test by Jeff Hill 
and Channel Access Client Library Tutorial Routines by Philip Stanley
 *
 * Modification Log:
 * -----------------
CVS log information:
$Log: connect.c,v $
Revision 1.3  2015/12/01 22:44:22  suz
reduce waiting time

Revision 1.2  2014/01/08 23:08:09  suz
printf

Revision 1.1  2013/01/21 20:58:10  suz
initial VMIC version

Revision 1.11  2007/11/20 23:37:20  suz
small fix

Revision 1.10  2006/06/19 17:13:41  suz
reduce time before giving up; include time.h

Revision 1.9  2005/10/11 22:57:45  suz
bug fix by Konstantin; missing if() statement

Revision 1.8  2005/07/19 21:30:24  suz
support linux (ifdef OS_LINUX) for epics test program

Revision 1.7  2005/04/08 17:19:43  suz
change a message

Revision 1.6  2004/12/02 18:02:36  suz
wait 1 min not 3 min before giving up

Revision 1.5  2004/05/03 18:24:23  suz
set a message as debug

Revision 1.4  2003/12/02 18:53:22  suz
disable some debug statements

Revision 1.3  2003/12/01 19:45:20  suz
changes to support reconnect of Epics devices

Revision 1.2  2003/12/01 18:47:08  suz
do not define d6

Revision 1.1  2003/05/05 19:30:24  suz
initial bnmr version; identical to bnmr1 v1.3

Revision 1.3  2003/01/08 19:21:25  suz
add caGetSingleId

Revision 1.2  2002/06/07 18:31:58  suz
replace conn.h by connect.h

Revision 1.1  2002/06/05 17:22:05  suz
general connect routines from conn.c


 */
#include <string.h>
#include <stdio.h>
#include <math.h>



#ifndef LOCAL
#define LOCAL static
#endif
#ifndef OS_LINUX /* VXWORKS */
#include <alarmString.h> /* EPICS header files */
#endif
#include "cadef.h"
#ifndef OS_LINUX /* VXWORKS */
#include "time.h" /* add for new epics */
#include "epicsTime.h"
#endif
#include "connect.h"
int caGetSingleId(char* pSname, int *pSchan_ID);
/* global */
int d4=0; /* debug ca routines and event handlers etc. */ 

/* Aug 2003 - the variable d6 is defined as global in file EpicsStep.c
   it should not be defined here also */
/* int d6=0; */ /* debug  in NaSet and NaSet_step */

/* Prototype for event handler */
 void read_event_handler(struct event_handler_args args);
 void write_event_handler(struct event_handler_args args);
 void connection_handler(struct connection_handler_args);
LOCAL void print_returned(chtype type, const void *pbuffer, unsigned count);

 /* Flag used to communicate between main() and event_handlers() */
int PEND_FLAG;
int CONN_FLAG;

/* global needed by print_returned to pass value read  */
float Read_value;

int caInit( void )
{

  /* Initialize Channel Access.
     
     Call once before anything else. If not called,
     it will be called automatically 
  */
  int status;
 
  status = ca_task_initialize();
  SEVCHK(status,  ca_message(status));
  if (status != ECA_NORMAL)
    {
      printf("caInit: Unable to initialize EPICS channel access");
      return(-1);
    }
  return 0;
}


int caGetId(char* pRname, char* pWname, int *pRchan_ID,  int *pWchan_ID)
{ 
  
  /* 
   *	convert name to chan id 
   */
  int status,i;
  chid chan_id;

  *pRchan_ID = *pWchan_ID = -1;
  printf("caGetId: starting with names:%s (read) and %s (write) \n",pRname,pWname);

  /* 
   *	convert name to chan id 
   */
  

  CONN_FLAG=1;

  /* for read access */    
  status = ca_search_and_connect(pRname, &chan_id, connection_handler, NULL);
#ifdef OS_LINUX
  SEVCHK(status, (char *)  ca_message(status));
#else
  if(d4)SEVCHK(status,  ca_message(status));
#endif
  if(status != ECA_NORMAL)
    { 
      printf("ca_GetId: bad status after ca_search_and_connect for %s\n",pRname);
      return -1;
    }


  /* Flush request and block for 0.25 seconds waiting for event handler to finish */

  i=0;
 
  while(CONN_FLAG)
    {
      i++;
      status=ca_pend_event(0.25);
      /* print a message if we have to wait a long time */
      if(i==10) printf("caGetID: Waiting for callback from search_and_connect for %s ",pRname);
      if(i%10 ==0)printf(".");
      if (i==60) /* wait for 15s before giving up (was 240 or 1min) */
	{
	  printf("\ncaGetID: timed out waiting for callback \n");
	  printf("caGetID: couldn't establish connection  to %s\n", pRname);
	  caExit();
	  return -1;
	}
    }
  if(d4)
    printf("\nca_GetID: Callback received from search_and_connect after %d loops\n",i);
  else if(i>9)printf("\n");
  
  if (status != ECA_TIMEOUT && status != ECA_NORMAL)
    {
#ifdef OS_LINUX 
      SEVCHK(status, (char *) ca_message(status));
#else
      if(d4)SEVCHK(status, ca_message(status));
#endif
      printf("ca_GetID: bad status after ca_pend_event: %s\n", ca_message(status));
      printf("ca_GetID: couldn't establish connection  to %s\n", pRname);
      return -1;
    }
  else if (ca_state(chan_id) != cs_conn)
    {  /* not connected */
      printf("ca_GetID: No connection to channel for %s\n", pRname);
      return -1;
    }
    
  printf("sizeof(chan_id)=%d and sizeof(int)=%d\n",sizeof(chan_id), sizeof(status));
  
  *pRchan_ID= (int)chan_id; /* cast to integer */
  

  if(d4)
    {
      printf("name:\t%s\n", ca_name(chan_id));
      printf("native type:\t%d\n", ca_field_type(chan_id));
      printf("native count:\t%lu\n", ca_element_count(chan_id));
    }

  /* for write access  */
  CONN_FLAG = 1;

  status = ca_search_and_connect(pWname, &chan_id, connection_handler, NULL);
#ifdef OS_LINUX
 SEVCHK(status,  (char *) ca_message(status));
#else  /* VXWORKS */
  if(d4)SEVCHK(status,  ca_message(status));
#endif

  if (status != ECA_TIMEOUT && status != ECA_NORMAL)
    { 
      printf("ca_GetId: bad status after ca_search_and_connect for %s",pWname);
      return -1;
    } 
 
 /* Flush request and block for 0.25 seconds waiting for event handler to finish */
  i=0;
  while(CONN_FLAG)
    {
      i++;
      status=ca_pend_event(0.25);
      if(i==10)   printf("ca_GetID: waiting for callback from search_and_connect for %s ",pWname);
      if(i%10 ==0)printf(".");
      if (i==480) /* wait for 2 minutes before giving up */
	{
	  printf("\nca_GetID: timed out waiting for callback \n");
	  printf("ca_GetID: couldn't establish connection  to %s\n", pWname);
	  return -1;
	}
    }
  if(d4)
  printf("\nca_GetID: Callback received from search_and_connect after %d loops\n",i);
  else if(i>9)printf("\n");

  if (status != ECA_TIMEOUT && status != ECA_NORMAL)
    {
      printf("ca_GetID: bad status after ca_pend_event: %s\n", ca_message(status));
      printf("ca_GetID: couldn't establish connection to %s\n", pWname);
      return -1;
    }
  else if (ca_state(chan_id) != cs_conn)
    {  /* not connected */
      printf("ca_GetID: No connection to channel for %s\n", pWname);
      /* status=ca_clear_channel(chan_id);
#ifdef OS_LINUX
      SEVCHK(status, (char *) ca_message(status) );
#else
      SEVCHK(status, ca_message(status) );
#endif
      if (status != ECA_NORMAL)
	  printf("ca_GetID: Unable to clear channel %s\n", pWname);
      status = ca_pend_io(1.0);
#ifdef OS_LINUX
      SEVCHK(status, (char *) ca_message(status) );
#else
      SEVCHK(status, ca_message(status) );
#endif */
      return -1;    /* calling pgm should call caExit */
    }

  

 *pWchan_ID= (int)chan_id; /* cast to integer */
 if(d4)
   {
     printf("name:\t%s\n", ca_name(chan_id));
     printf("native type:\t%d\n", ca_field_type(chan_id));
     printf("native count:\t%lu\n", ca_element_count(chan_id));
   }
 /* success */
 return 0;
}

void connection_handler(struct connection_handler_args connect_args)
{
 

  if(d4) 
    printf("connection_handler starting\n");

  if(connect_args.op == CA_OP_CONN_UP)
    printf("Connect_handler: %s IS reconnected \n",  ca_name(connect_args.chid) );
  else 
    printf("Connect_handler: %s is DISCONNECTED \n", ca_name(connect_args.chid));
  CONN_FLAG = 0; /* clear flag */
  return;
}

int caRead(int ichan_id, float *pval)
{
  int status,i;

  chid chan_id;
  chtype type; 
  unsigned nelem;
  
  
  chan_id=(chid)ichan_id;
  if(d4)printf("caRead: starting with chan_id=%d name: %s \n",ichan_id, ca_name(chan_id) );
  
  /* Get native data type.  */
  type = ca_field_type(chan_id);
    /* Get number of elements of PV */
  nelem = ca_element_count(chan_id);
  
  if(d4)printf("caRead: native type:/t%d and count::\t%lu\n", ca_field_type(chan_id), ca_element_count(chan_id) );

  /* Make callback request */
 
  PEND_FLAG = 1;
  
  /*  status = ca_array_get_callback(dbf_type_to_DBR_STS(type), */
   status = ca_array_get_callback(DBR_STS_FLOAT, 
				 nelem,                                  /* number of elements of database field */
				 chan_id,                                   /* channel identifier or chid */
				 read_event_handler,  /* name of callback event handler */
				 NULL);

  if(status != ECA_NORMAL)
    {
      printf("caRead: bad status after ca_array_get_callback : %s\n", ca_message(status));
      return -1;
    }  
  /* Flush request and block for 0.1 seconds waiting for event */
  
  /* Put in a counter so we can time out after a certain time.... */
  
  i=0;
  while (PEND_FLAG)
    {
      i++;
      status = ca_pend_event(0.1);
      if(i==10) printf("caRead: Waiting for callback from read_event_handler ");
      if(i%10 ==0)printf(".");
      if (i==300)
	{
	  printf("\ncaRead: timed out waiting for ca_pend_event\n");
	  return -1;
	}
    }
  if(d4)printf("\ncaRead: Callback received from read_event_handler after %d loops\n",i);
  else if(i>9)printf("\n");

  if (status != ECA_TIMEOUT && status != ECA_NORMAL)
    {
      printf("caRead: timeout after ca_pend_event: %s\n", ca_message(status));
    return -1;
    }
  else
    if(d4)printf("caRead: Read_value=%.2f\n",Read_value);
 
  *pval = Read_value;
  return(0);
}


/* callback function: informs user of success or failure of   */
/* get operation and prints the channel's value along with */
/* status and severity.                                                                                                                 */
void read_event_handler(struct event_handler_args args)
{
  if(d4)printf("read_event_handler starting\n");

  Read_value=-1; /* set an invalid value to global Read_value to start with */
  if (args.status == ECA_NORMAL) 
    {
      print_returned(
		     args.type,
		     args.dbr,
		     args.count);   /* fills global Read_value */
    }
  /* if get operation failed, print channel name and message */
  else
    {

      printf("read_event_handler: get operation failed on channel %s\n", ca_name(args.chid));
#ifdef OS_LINUX
      SEVCHK(args.status, (char *) ca_message(args.status) );
#else
      if(d4)SEVCHK(args.status, ca_message(args.status) );
#endif
    }
  PEND_FLAG = 0; 
  return;
}
/*
 * PRINT_RETURNED
 *
 * print out the values in a database access interface structure
 *
 * switches over the range of CA data types and reports the value
 */
LOCAL void print_returned(chtype type, const void *pbuffer, unsigned count)
{
  /*  unsigned	i; */
  
  
  
  if(d4)printf("print_returned starting with data type = %s (%d)\n",dbr_text[type],(int)type);

  switch(type)
    {
    
    case (DBR_STS_FLOAT):
      {
	
	struct dbr_sts_float *pvalue
	  = (struct dbr_sts_float *)pbuffer;
	dbr_float_t *pfloat = &pvalue->value;
	/*      printf("%2d %2d",pvalue->status,pvalue->severity); 
		if(count==1) printf("Value: ");
		for (i = 0; i < count; i++,pfloat++){
		if(count!=1 && (i%10 == 0)) printf("\n");
		printf("%6.4f ",*pfloat); */
	Read_value = *pfloat;  /* count=1 for NaCell */  
	
	if(d4)printf("print_returned: Read_value=%.2f, type is DBR_STS_FLOAT \n",Read_value);
	
	break;
      }
    default:
      printf("print_returned: type not supported \n");

    } 
}

int caWrite(int ichan_id, float *pvalue)
{
  int i,status;
  chid chan_id;
  chtype type; 
  unsigned nelem;
  

   if(d4) 
   printf("\n==== caWrite: starting, with ichan_id=%d, writing value=%8.3f === \n",ichan_id,*pvalue);
  chan_id=(chid)ichan_id;
 
  /* Get native data type.  */
  type = ca_field_type(chan_id);
  
  /* Get number of elements of PV */
  nelem = ca_element_count(chan_id);
  

  if(d4)
    {
      printf("name:\t%s\n", ca_name(chan_id));
      printf("native type:\t%d\n", ca_field_type(chan_id));
      printf("native count:\t%lu\n", ca_element_count(chan_id));
      printf("data to write: %.2f\n",*pvalue);
    }

  PEND_FLAG = 1;  /* set flag (cleared by callback) */

 /* Make callback request */
  
  /*  status = ca_array_get_callback(dbf_type_to_DBR_STS(type), */
  status = ca_array_put_callback( DBR_FLOAT,   /*DBR_STS_FLOAT,*/ 
				 nelem,                                  /* number of elements of database field */
				 chan_id,                                   /* channel identifier or chid */
				  pvalue,                                     /* value to write */
				 write_event_handler,  /* name of callback event handler */
				 NULL);
#ifdef OS_LINUX
  SEVCHK(status,  (char *)ca_message(status)); 
#else
  if(d4)SEVCHK(status, ca_message(status)); 
#endif
  if(status != ECA_NORMAL)
    {
      printf("caWrite: bad status after call to ca_array_put_callback\n");
      return -1;
    }  

  /* Flush request and block for 0.1 seconds waiting for event */

  i=0;
  while (PEND_FLAG)
    {
      i++;
      status = ca_pend_event(0.1);
      if(i==10) printf("caWrite: Waiting for callback from write_event_handler ");
      if(i%10 ==0)printf(".");
      if (i==300)  /* wait up to 30s (was  one minute) for callback to complete */
	{
	   printf("\ncaWrite: timed out waiting for write_event_handler \n");
	   return -1;
	}
    }
   if(d4) 
    printf("\ncaWrite: Callback received from write_event_handler after %d loops\n",i);
    if(i>9)printf("\n");

  if (status != ECA_TIMEOUT && status != ECA_NORMAL)
    {
      printf("caWrite: bad status after ca_pend_event:\n %s\n", ca_message(status));
      return -1;
    }

  if(d4)printf("caWrite: Successfully set value to =%.2f\n",*pvalue);
  return(0);
}


/* callback function: informs user of success or failure of   */
/* get operation and prints the channel's value along with */
/* status and severity. */                          
    

void write_event_handler(struct event_handler_args handler_args)
    {
      
      if(d4)printf("write_event_handler: starting\n");
      if (handler_args.status != ECA_NORMAL)
	{
	  printf("write_event_handler: channel %s: put operation FAILED due to:\n",
		 ca_name(handler_args.chid));
#ifdef OS_LINUX
	  SEVCHK(handler_args.status, (char *) ca_message(handler_args.status) );
#else
	  if(d4)SEVCHK(handler_args.status, ca_message(handler_args.status) );
#endif
	}
      else
	 if(d4) 
	  printf("write_event_handler: put operation completed on channel %s.\n", ca_name(handler_args.chid));
      PEND_FLAG=0;
      return;
     
    }

void caExit()
{
  int status;
  printf("caExit: disconnecting all channels by calling ca_task_exit\n");

  status=ca_task_exit();
  if (status != ECA_NORMAL)    
    {
      printf("caExit: failure from ca_task_exit() %d",status);
#ifdef OS_LINUX
      SEVCHK(status,  (char *)  ca_message(status));
#else
      SEVCHK(status,  ca_message(status));
#endif

    }

  printf("caExit: Ignore message: Warning: \"Virtual circuit disconnect\")  status(%d)\n",status);
  
  if(d4)printf("caExit: returning after calling ca_task_exit \n");
  return;
}

int caCheck(int ichan_id)
{
  chid chan_id;
  
  chan_id = (chid) ichan_id;
  if(d4)printf("caCheck: starting with chan_id=%d name: %s \n",ichan_id, ca_name(chan_id) );
  
  if (ca_state(chan_id) == cs_conn)
    { /* connected */
      if(d4)printf("caCheck: channel %s is connected\n", ca_name(chan_id) );
      return 1; /* true */
    }
  else
    {  /* not connected */
     if(d4)printf("caCheck: No connection to channel for %s\n", ca_name(chan_id)  );
     return 0; /* false */
    }
}




int caGetSingleId(char* pSname, int *pSchan_ID)
{ 
  
  /* 
   *	convert name to chan id 
   */
  int status,i;
  chid chan_id;


  *pSchan_ID = -1;
  printf("caGetSingleId: starting with name:%s  \n",pSname);



/* Initialize Channel Access. */
  status = ca_task_initialize();
  if(d4)SEVCHK(status,  ca_message(status));
  if (status != ECA_NORMAL)
    {
      printf("caGetSingleId: Unable to initialize EPICS channel access");
      return(-1);
    }
  else 
    printf("caGetSingleId: initialized EPICS channel access");

    
  printf("caGetSingleId: pSname=%s\n",pSname);

  /* 
   *	convert name to chan id 
   */
  

  CONN_FLAG=1;

  /* for read access */    
  status = ca_search_and_connect(pSname, &chan_id, connection_handler, NULL);
   printf("caGetSingleId: after ca_search_and_connect\n");
  if(d4)
    SEVCHK(status,  ca_message(status));
  if(status != ECA_NORMAL)
    { 
      printf("caGetSingleId: bad status after ca_search_and_connect for %s\n",pSname);
      return -1;
    }
  else
    printf("caGetSingleId: success from ca_search_and_connect for %s\n",pSname);


  /* Flush request and block for 0.25 seconds waiting for event handler to finish */

  i=0;
  while(CONN_FLAG)
    {
      i++;
      status=ca_pend_event(0.25);
      /* print a message if we have to wait a long time */
      if(i==10) printf("caGetSingleID: Waiting for callback from search_and_connect for %s ",pSname);
      if(i%10 ==0)printf(".");
      if(i%25 ==0)printf("\ncaGetSingleID: still waiting for callback from search_and_connect for %s",pSname);
      if (i==50) /* give up (was 100) */
	{
	  printf("\ncaGetSingleId: timed out waiting for callback \n");
	  printf("caGetSingleId: couldn't establish connection  to %s\n", pSname);
	  //	  caExit();
	  return -1;
	}
    }
  if(d4)
  printf("\ncaGetSingleId: Callback received from search_and_connect after %d loops\n",i);
  else if(i>9)printf("\n"); 

  if (status != ECA_TIMEOUT && status != ECA_NORMAL)
    {
      if(d4)SEVCHK(status, ca_message(status));
      printf("caGetSingleId: bad status after ca_pend_event: %s\n", ca_message(status));
      printf("caGetSingleId: couldn't establish connection  to %s\n", pSname);
      return -1;
    }
  else if (ca_state(chan_id) != cs_conn)
    {  /* not connected */
      printf("caGetSingleId: No connection to channel for %s\n", pSname);
      return -1;
    }
    
  printf("caGetSingleId: connected\n");
  *pSchan_ID= (int)chan_id; /* cast to integer */
  
  /*if(d4)*/
    {
      printf("name:\t%s\n", ca_name(chan_id));
      printf("native type:\t%d\n", ca_field_type(chan_id));
      printf("native count:\t%lu\n", ca_element_count(chan_id));
    }
 /* success */
 return 0;
}


/*
int ca_array_put_callback (
                 chtype                  TYPE,
                 unsigned long                   COUNT,
                 chid                    CHID,
                 void                    *PVALUE,
                 void                    (*PFUNC)(struct event_handler_args ARGS),
                 void                    *USERARG,
 );


int ca_array_get_callback (
                 chtype                  TYPE,
                 unsigned long                   COUNT,
                 chid                    CHID,
                 void                    (*USERFUNC)(struct event_handler_args ARGS),
                 void                    *USERARG
 );
*/












