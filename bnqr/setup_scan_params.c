/*  Included by febnmr.c

CVS log information:
$Log: setup_scan_params.c,v $
Revision 1.2  2015/06/22 22:40:03  suz
added mode 2f

Revision 1.1  2013/01/21 20:58:10  suz
initial VMIC version

Revision 1.11  2008/04/19 06:29:07  suz
revert to opening and closing epics scan iocs each run

Revision 1.10  2008/04/17 17:31:09  suz
do not reopen epics scan channels if already open

Revision 1.9  2005/08/13 01:35:34  suz
change a comment

Revision 1.8  2005/08/04 00:53:59  suz
call for 2a/2e non-random to fill pseqf array

Revision 1.7  2005/05/18 00:09:11  suz
frequency is unsigned int (needed for PSMII)

Revision 1.6  2005/03/16 19:04:11  suz
add a printf

Revision 1.5  2005/02/28 21:45:54  suz
change format for pol

Revision 1.4  2005/02/22 23:18:15  suz
fix bugs for POL version

Revision 1.3  2005/01/26 21:52:14  suz
add status

Revision 1.2  2005/01/25 23:27:40  suz
epics_reconnect now has a parameter for helicity CA

Revision 1.1  2005/01/24 17:57:02  suz
initial version: simplify begin_of_run by moving code to subroutines


*/
#include "setup_scan_params.h"

INT mode10_set_scan_params(INT *ninc)
{
  /* called from begin of run to set up scan parameters */

  printf(" mode10_set_scan_params: setting up for Mode 10 (DUMMY Frequency scan) \n");
	  /* mode 10 -  frequency is not actually scanned,  so use some standard values  */
  freq_start = 335000; freq_stop  = 336000;  freq_inc   = 100; /* gives 20 frequency steps */
  freq_ninc = ( abs(freq_stop - freq_start)/ abs(freq_inc) ) +1;
  printf(" begin_of_run: Selected %d DUMMY frequency cycles \n",freq_ninc);
  gbl_scan_flag = 4;      /* set scan flag for dummy frequency scan  */
  *ninc = freq_ninc;
  return SUCCESS;
}	

#ifdef EPICS_ACCESS
INT epics_set_scan_params(INT *ninc)
{
  INT status;
  INT openflag;
  /* called from begin of run to set up scan parameters */

  printf("\n ***** epics_set_scan_params: starting \n");
  openflag=0;
  /*  
      Epics Scan : Scanning Laser ?  
  */
  if (strcmp (fs.output.e1n_epics_device, "Laser")==0)
    {
      printf("epics_set_scan_params: EPICS Laser scan detected\n");
      epics_params.Laser_flag = TRUE; /* LASER scan is selected */
      gbl_scan_flag = 2;   /* Laser (value = 2 ) */

    }
  /*
    Epics Scan : Scanning Field ?  
  */
  else if (strcmp (fs.output.e1n_epics_device, "Field")==0)
    {
      printf("epics_set_scan_params: EPICS Field scan detected\n");  /* 1e */
      epics_params.Field_flag = TRUE;  /* FIELD scan is selected */
      gbl_scan_flag = 3;     /* Field (value = 3) */
    }

  /*
    Epics Scan : Scanning NaCell ?  
  */    
  else if (strcmp (fs.output.e1n_epics_device, "NaCell")==0)
    {
      printf("epics_set_scan_params: EPICS Na/Rb Cell scan detected\n");
      epics_params.NaCell_flag = TRUE; /* Na/Rb Cell scan is selected */
      gbl_scan_flag = 1;  /* NaCell (value = 1) */
    }
  else
    {
      cm_msg(MERROR,"epics_set_scan_params","Unknown Epics device : %s", fs.output.e1n_epics_device);	  
      return(DB_NO_ACCESS);
    }
  /* GetIDs for Epics scan */
  status = epics_reconnect(FALSE); 
  if (status != SUCCESS)
    {
      /* Epics device not accessible  ....
	 mdarc should stop the run since client flag will not be set to success */
      return  (FE_ERR_HW);    
    }
  


#ifdef GONE   /* try keeping NaCell open  */
  /*
    Epics Scan : Scanning NaCell ?  
  */
  else if (strcmp (fs.output.e1n_epics_device, "NaCell")==0)
    { 
      printf("epics_set_scan_params: EPICS Na/Rb Cell scan detected\n");
      if (gbl_epics_scan == 1)
	{
	  printf("epics_set_scan_params: epics scan has been done previously; epics channels should be open\n");
	  if(epics_params.NaCell_flag != TRUE) /* Na Cell scan should be selected */
	    {
	      printf("epics_set_scan_params: error expect epics_params.NaCell_flag to be set TRUE\n");
	     
	    }

	  printf("epics_params.NaCell_flag=%d epics_params.XxWchid =%d  epics_params.XxRchid = %d\n",
	epics_params.NaCell_flag,	 epics_params.XxWchid, epics_params.XxRchid);
 
	  status = epics_watchdog ( &epics_params, FALSE);
	  if (status != SUCCESS)
	    {
	      printf("epics_set_scan_params: error from epics_watchdog \n");
	      /* Epics device not accessible  ....
		 mdarc should stop the run since client flag will not be set to success */
	      return  (FE_ERR_HW);    
	    }
	  openflag=1;
	}
      else if (gbl_epics_scan != 0)
	printf("epics_set_scan_params; changing epics scan\n");
      else
	printf("epics_set_scan_params: this is the first epics scan; epics channels should be closed\n");
	  
     
      epics_params.NaCell_flag = TRUE; /* Na Cell scan is selected */
      gbl_scan_flag = 1;  /* NaCell (value = 1) */
    }
  else
    {
      cm_msg(MERROR,"epics_set_scan_params","Unknown Epics device : %s", fs.output.e1n_epics_device);	  
      return(DB_NO_ACCESS);
    }
  
  /* should these Epics channels already be open ? */
  if(!openflag)
    {
      /* GetIDs for Epics scan */
      status = epics_reconnect(FALSE); 
      if (status != SUCCESS)
	{
	  /* Epics device not accessible  ....
	     mdarc should stop the run since client flag will not be set to success */
	  return  (FE_ERR_HW);    
	}
    }
#endif /* GONE */


  gbl_epics_scan = gbl_scan_flag;
  
  
  /* Time for watchdog on direct access to Epics channels */
  Epics_last_time = ss_time(); 
  
  status = EpicsInit( flip, &epics_params ); /* Initialize direct access to Epics device */
  if (status != SUCCESS) 
    {
      /* Epics device cannot be initialized  or the parameters were invalid ....
	 mdarc will stop the run since client flag will still show failure */
      return  (FE_ERR_HW);  
    }
  gbl_epics_live = TRUE;
  gbl_inc_cntr = epics_params.Epics_ninc; /* set to max to satisfy cycle_start routine */      
#ifdef POL
  /* Jump in scan? */
  if( fs.input.enable_scan_jump)
    {  /* EpicsInit calls find_jump, which filled these output params */
      gbl_jinc1= fs.output.scan_jump_inc1;
      gbl_jinc2= fs.output.scan_jump_inc2;
      if(dj)printf("epics_set_scan_params: epics gbl_jinc1=%d gbl_jinc2=%d\n",gbl_jinc1,gbl_jinc2);
    }
#endif
  *ninc = gbl_inc_cntr;
  return(SUCCESS);
}
#endif     /* EPICS ACCESS */

#ifdef CAMP_ACCESS
INT camp_set_scan_params(INT *ninc)
{
  float ftmp;
  /* called from begin_of_run */
  if(dc)printf("camp_set_scan_params: Camp scan detected\n");
  camp_start = fs.input.e1c_camp_start;   /* all these are defined in odb (even if #ifdef CAMP_ACCESS is false */
  camp_stop = fs.input.e1c_camp_stop;
  camp_inc = fs.input.e1c_camp_inc;
  
  if( camp_inc  == 0 )
    {
      cm_msg(MERROR,"camp_set_scan_params","Invalid camp scan increment value: %f",
	     camp_inc);
      return FE_ERR_HW;
    }
  
  
  if ( fabs(camp_inc) > fabs(camp_stop - camp_start)) 
    {
      cm_msg(MERROR,"camp_set_scan_params","Invalid camp scan increment value: %f",
	     camp_inc);
      return FE_ERR_HW;
    }
  
  if( ((camp_inc > 0) && (camp_stop < camp_start)) ||
      ((camp_inc < 0) && (camp_start < camp_stop)) )
    camp_inc *= -1;
  ftmp =  ( fabs(camp_stop - camp_start)/ fabs(camp_inc) ) +1;
  camp_ninc = (INT)ftmp;
  
  printf("ninc %f -> integer %d  diff %f camp_inc = %f\n",ftmp,camp_ninc,(camp_stop-camp_start),camp_inc);
  if( camp_ninc < 1)
    {
      cm_msg(MERROR,"camp_set_scan_params","Invalid number of sweep steps: %d",
	     camp_ninc);
      return FE_ERR_HW;
    }
  if(flip) camp_inc *= -1;
  gbl_FREQ_n = camp_ninc; /* set to max to satisfy cycle_start routine */
  set_camp_val = camp_start;
  printf("camp_set_scan_params: camp device will be set to =%f\n",set_camp_val);
  printf(" Selected %d CAMP scan increments starting with %f %s by steps of %f\n",
	 camp_ninc,camp_start, camp_params.units,camp_inc);


  /*  Note    gbl_scan_flag will be set by camp_update_params */
  *ninc = camp_ninc;
  return SUCCESS;
}
#endif /* CAMP */


INT random_set_scan_params(INT *ninc)
{
  INT i,status;

  /* called from Begin_of_run; randomized values for 1a/1b/2a/2c/2e/2f/1f 
     also called for non-random 2a,2e,2f to fill pseqf */

  /* frequency step values are read from the file generated by rf_config.
     returns the number of frequency increments in *ninc.

  The frequency step values are read out from the file into an array. 
  For Type 1 the freq steps are then randomized, and the random frequency values are stored in an array
     pointed to by "prandom_freq"
  
  For Type 2, randomization is done every cycle, and the random values are then downloaded into the PPG.
  For Type 2e non-random pseq is used. This routine is called but pseq is then filled for non-random data 
  */
	  
#ifdef TEST_JUMP /* POL only TEMP for testing scan JUMP with a frequency scan (type 1 only)   */
  if( fs.input.enable_scan_jump)
    {
      cm_msg(MERROR,"random_set_scan_params","jump in scan not supported for randomized frequencies");
      return DB_INVALID_PARAM;
    }
#endif
  
  /* seed is defined in trandom.h */
  seed = fs.output.last_seed__for_random_freqs_; /* use last seed from odb to seed a new sequence of random nos. */
  if(random_flag)
    printf("random_set_scan_params:  frequency values will be randomized; initial seed=%d \n",seed);
  
  freq_ninc = fs.output.num_frequency_steps;
  
  status = read_freq_file(ppg_mode, freq_ninc); /* read the frequency file and allocate memory for arrays */
  if(status != SUCCESS) /* error message sent by read_freq_file */
    return status;

 *ninc = freq_ninc;


  /* For type 2, only 2a/2e/2f are currently supported */
  if( e2a_flag || e2e_flag || e2f_flag )
    {
    if(random_flag)/* no need to randomize freqs now, since it is done each cycle for Type 2 */
      return status;
    else
      {  /* for non-random data */
	if(pindex==NULL || prandom_freq==NULL  || pfreq == NULL || pseqf == NULL)
	  { 
	    /* read_freq_file must have been called prior to this routine to allocate space */
	    cm_msg(MERROR,"random_set_scan_params",
		   "array pointer(s) are null. Space has not been allocated");
	    return DB_INVALID_PARAM;
	  }
	/* fill pseqf, so we can point to non-random frequencies */
	for (i=0; i<freq_ninc; i++)
	  {
	    pindex[i]=i;  /* initialize index array */
	    prandom_freq[i]=pfreq[i]; /* random array = frequency array */
	    pseqf[i]=i; /* initialize pointer array for filling histo data */
	  }
	return status;
      }
    } /* end of Type 2 */


 /* Type 1 only */
  status = randomize_freq_values(ppg_mode, freq_ninc); /* fill prandom_freq with the random freq values */
  if(status != SUCCESS) /* error message sent by randomize_freq_values */
    return status;
 
  gbl_FREQ_n = freq_ninc; /* set to max to satisfy cycle_start routine */
  freq_val = (DWORD)prandom_freq[0];  /* the first value to set */
  
  printf(" Selected %d frequency cycles; frequency range is %u to %u, step size =%d\n",
	 freq_ninc, (DWORD)fs.input.frequency_start__hz_, 
	 (DWORD)fs.input.frequency_stop__hz_, fs.input.frequency_increment__hz_);
  printf(" Frequency steps are randomized; first value will be %u hz\n", freq_val);
  gbl_scan_flag = 0x800;
  return SUCCESS;
}




INT  RF_set_scan_params(INT *ninc)
{

#ifdef HAVE_PSM
  DWORD itmp;
#endif
  /* called from begin_of_run to set up frequency for Type 1 modes
     (1f/1g/10) and 1a/1b (not randomized)  
  */
  if(debug)
    printf("RF_set_scan_params: Frequency scan detected \n");
  freq_start= (DWORD)fs.input.frequency_start__hz_;
  freq_stop = (DWORD)fs.input.frequency_stop__hz_;
  freq_inc = (INT)fs.input.frequency_increment__hz_;
  
  if( freq_inc == 0 )
    {
      cm_msg(MERROR,"RF_set_scan_params","Invalid frequency scan increment value: %d",
	     freq_inc);
      return FE_ERR_HW;
    }
  
  
  if ( abs(freq_inc) > abs(freq_stop - freq_start)) 
    {
      cm_msg(MERROR,"RF_set_scan_params","Invalid frequency increment value: %d",
	     freq_inc);
      return FE_ERR_HW;
    }
  
#ifdef HAVE_PSM
  /* check the values for psm */
  itmp = get_hex(freq_stop);
  if(itmp == 0)
    {
      cm_msg(MERROR,"RF_set_scan_params","Frequency stop %uHz is outside maximum for psm\n",freq_stop);
      return FE_ERR_HW;
    }
  itmp = get_hex(freq_start);
  if(itmp == 0)
    {
      cm_msg(MERROR,"RF_set_scan_params","Frequency start %uHz is outside maximum for psm\n",freq_start);
      return FE_ERR_HW;
    }
#endif	 // HAVE_PSM	
  
  if( ((freq_inc > 0) && (freq_stop < freq_start)) ||
      ((freq_inc < 0) && (freq_start < freq_stop)) )
    freq_inc *= -1;
  freq_ninc = ( abs(freq_stop - freq_start)/ abs(freq_inc) ) +1;
  printf("ninc %d diff %d freq_inc = %d\n",freq_ninc,(freq_stop-freq_start),freq_inc);
  if( freq_ninc < 1)
    {
      
      cm_msg(MERROR,"RF_set_scan_params","Invalid number of freq steps: %d",
	     freq_ninc);
      return FE_ERR_HW;
    }
#ifdef TEST_JUMP /* TEMP for testing scan JUMP with a frequency scan   */

  if( fs.input.enable_scan_jump)
    {
      if (mode1g && flip)
	{
	  /* adds unnecessary complication */
	  cm_msg(MERROR,"RF_set_scan_params","jump in scan not supported for mode 1g with helicity flipping");
	  return DB_INVALID_PARAM;
	}
	      /* freq_start, freq_stop are unsigned INTEGERS; scan_jump_start and stop are float */
      float fstart,fstop, finc;
      INT status;

      fstart = (float)freq_start;
      fstop  = (float)freq_stop;
      finc   = (float)freq_inc;
      
      if(dj)
	{
	  printf("freq_start=%u freq_stop=%u freq_inc=%d; fstart=%f fstop=%f finc=%f\n",
		 freq_start,freq_stop,freq_inc,fstart,fstop,finc);
	  
	  printf("calling find_jump with start=%f stop=%f inc=%f ninc=%d jstart=%f jstop=%f\n",
		 fstart, fstop, finc, freq_ninc, 
		 fs.input.scan_jump_start, fs.input.scan_jump_stop);
	}
      status = find_jump(fstart, fstop, finc, freq_ninc, 
			 fs.input.scan_jump_start, fs.input.scan_jump_stop,
			 dj,&gbl_jinc1, &gbl_jinc2);
      if(status != SUCCESS)
	return (DB_INVALID_PARAM);
    }
#endif
  if(flip) freq_inc *= -1;  /* do this after find_jump */
  freq_val = freq_start;
  gbl_FREQ_n = freq_ninc; /* set to max to satisfy cycle_start routine */
  if(mode1g_flag && flip)
    {
      printf(" Selected %d freq cycles starting with %u hz by steps of %d hz (cycle repeats at each helicity)\n",
	     freq_ninc,freq_start,freq_inc);
    }
  else
    printf(" Selected %d frequency cycles starting with %u hz by steps of %d hz\n",
	   freq_ninc,freq_start,freq_inc);
#ifdef TEST_JUMP	  
  if( fs.input.enable_scan_jump)
    {
      printf("with a jump selected between %.2f Hz and %.2f Hz (between increments %d and %d)\n", 
	     fs.input.scan_jump_start,  fs.input.scan_jump_stop,
	     gbl_jinc1, gbl_jinc2);
      
      printf("actual values for jump will be %.2f Hz and %.2f Hz \n",
	     fs.output.scan_jump_value1,  fs.output.scan_jump_value2);
    }
  /* end of TEMP for testing JUMP */
#endif
  if (mode1g_flag && flip)
    gbl_scan_flag = 0x1000; /* 1g with flip ... may not need a special value */
  else
    gbl_scan_flag = 0;  /* frequency etc (value = 0) 1f,1g (no flip), 1a/1b not random  */
  *ninc = freq_ninc;
  return SUCCESS;
}	  /* end of freq scan modes (1f,1g,1a,1b) */


