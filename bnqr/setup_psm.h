/* setup_psm.h

Function prototypes

*/
#ifdef HAVE_PSM

#define MAX_PROFILES 4  /* 1f 3f 5f fREF */

INT psm_setone(MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq_Hz);
INT psm_loadFreqFile(MVME_INTERFACE *mvme, DWORD base_addr, char* ppg_mode, BOOL random_flag);
INT psm_loadIQFile(MVME_INTERFACE *mvme, DWORD base_addr, char* ppg_mode, char *profile);
INT psm_loadIQIdle(MVME_INTERFACE *mvme, DWORD base_addr, char *profile);
INT disable_psm(MVME_INTERFACE *mvme, DWORD base_addr);
INT init_psm(MVME_INTERFACE *mvme, DWORD base_addr);
INT setup_psm(MVME_INTERFACE *mvme, DWORD base_addr);
INT load_iq_file(MVME_INTERFACE *mvme, DWORD base_addr);
INT load_tuning_freq(MVME_INTERFACE *mvme, DWORD base_addr);
INT get_index(char *p_profile_code, INT *pindex);
char *get_profile(INT n);
INT  write_RF_trip_threshold( MVME_INTERFACE *mvme, DWORD base_addr);
INT get_profile_index(char *p_profile_code, INT *pindex);
INT update_Ncmx(MVME_INTERFACE *mvme, DWORD base_addr);
INT psmReadFreqDM( MVME_INTERFACE *mvme, DWORD base_addr, DWORD num_values);
#endif
