/* camp_acq.c 

   based on imusr_camp_acq.c from VAX 

   
 $Log: camp_acq.c,v $
 Revision 1.4  2015/12/02 03:16:38  suz
 changes so this works with post-Ted camp library

 Revision 1.3  2013/05/10 21:34:30  suz
 rearrange commented out lines to get rid of warnings

 Revision 1.2  2013/04/26 01:32:47  suz
 got rid of some warnings

 Revision 1.1  2013/01/21 20:58:10  suz
 initial VMIC version

 Revision 1.9  2004/10/25 19:15:53  suz
 add a comment: pol no longer uses camp

 Revision 1.8  2004/05/20 18:42:12  suz
 add support for POL

 Revision 1.7  2004/03/09 22:15:33  suz
 changes for Midas 1.9.3

 Revision 1.6  2004/01/14 20:02:35  suz
 change error return code

 Revision 1.5  2003/10/15 19:07:44  suz
 add support for imusr

 Revision 1.4  2003/09/30 19:50:20  suz
 added support for imusr

 Revision 1.3  2003/08/14 20:24:22  suz
 add support for POL's Camp DAC device

 Revision 1.2  2003/06/24 23:24:57  suz
 do not set instr. offline then online if already online

 Revision 1.1  2002/06/05 00:50:15  suz
 routines for direct camp access by febnmr1.c


*/
#include <stdio.h>
#include <math.h>
#include <ctype.h>

#include "camp_clnt.h"
/* to avoid conflict with midas defines, undefine these
   - they will be redefined by midas the same as camp defined them */
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#ifdef INLINE
#undef INLINE
#endif

#include "midas.h"
#include "camp_acq.h"


extern BOOL dc; /* TRUE for debug */

#define failure_status CAMP_FAILURE
#define IO_RETRIES 15

/* function prototypes  
     are in camp_acq.h
 */


/* globals */
static BOOL haveCamp = FALSE;
INT status;

/* parameters */
/* structure CAMP_PARAMS camp_params is defined in camp_acq.h */



/*   Usage of these routines :

A.    rf_config will
  1. Fill camp_params 
  2. Full Initialize  -> call  init_sweep_device

B.   frontend will
  1. Fill camp_params.InsPath and setPath  only
  2. Partial initialize -> call initPath
  3. Write setpoints   -> call  set_sweep_device
*/
/* -------------------------------------------------------------------------------------------*/
INT
camp_init(  CAMP_PARAMS camp_params )

/* -------------------------------------------------------------------------------------------*/
{
  /* Initialize the camp connection

      called from init_sweep_device
             and  initPath
   */
  // char *msg;
  int len;
  long itime=20;

  
  len =strlen(camp_params.serverName);
  if(dc)
    {
      printf("camp_init: starting with:" );
      printf("hostname : %s length %d   dc(debug) =%d \n",camp_params.serverName, len, dc);
    }
/* until tested on BNMR/BNQR : */
#ifdef IMUSR
#ifdef OS_VXWORKS
  if(dc)printf("camp_init: calling rpcInit....\n");
  rpcInit( );
  if(dc)printf("camp_init: calling rpcTaskInit....\n");
  rpcTaskInit( );
  if(dc)printf("camp_init:  rpcTaskInit done\n");
#endif 
#endif

  haveCamp=FALSE;
  status = camp_clntInit( camp_params.serverName, itime ); 
  if(dc)
    printf("status after camp_clntInit = %d\n",status);
  
  if( _failure(status) ) 
    {
      printf( "camp_init: failed camp_clntInit (%d)\n",status );
      cm_msg(MERROR,"camp_init"," failed camp_clntInit");
      sendMsg("camp_init");
      return( status );
    }

  status = camp_clntUpdate();
  if (status != CAMP_SUCCESS ) 
    {
        printf( "camp_init: failed camp_clntUpdate (%d)\n",status );
	cm_msg(MERROR,"camp_init"," failed camp_clntUpdate");
	sendMsg("camp_init");
        return( status );
    }

    haveCamp = TRUE;
    return( CAMP_SUCCESS );
}


/* -------------------------------------------------------------------------------------------*/
INT
camp_end( void )

/* -------------------------------------------------------------------------------------------*/
{
  /* Close the camp connection

     Called by an external program
  */
    if( !haveCamp ) return( CAMP_SUCCESS );
    camp_clntEnd();
    haveCamp = FALSE;
    printf("camp_end: setting haveCamp FALSE\n");
    return( CAMP_SUCCESS );
}


/* -------------------------------------------------------------------------------------------*/
INT 
set_sweep_device( float setpoint, CAMP_PARAMS camp_params )

/* -------------------------------------------------------------------------------------------*/
{
  /* Set the sweep device to a value

     Called by an external program

     Device must have been previously initialized 
         by a call to init_sweep_device   
              or      init_path            
  */
  int wait_msec;
  // double wait_sec;
  // char wait_str[32];
#ifdef POL
  float tmp;
#endif
  if(dc)
    {
      printf("set_sweep_device: starting with setpoint=%f\n",setpoint);
      printf("sweep_device %s, dc(debug) =%d\n",camp_params.SweepDevice,dc);
    }
  if( !haveCamp ) 
    {
      printf("set_sweep_device: haveCamp is false\n");
      cm_msg(MERROR,"set_sweep_device","Camp access flag (haveCamp) is false\n");
      return( -1 ); /* don't have camp */
    }
  /* check the value against the maximum and minimum allowed */
  if (setpoint < camp_params.minimum_value || setpoint >  camp_params.maximum_value)
    {
      cm_msg(MERROR,"set_sweep_device","Camp set value (%f) is outside maximum (%f) or minimum (%f) allowed values in %s\n",
	   setpoint, camp_params.minimum_value,  camp_params.maximum_value,  camp_params.units   );
      return( CAMP_FAILURE );
    }
#ifdef POL
  /* note: POL no longer uses a CAMP DAC, and is no longer compiled with CAMP_ACCESSS
     So this code will not be used. Leave it in for now in case of future use */
  /* conversion */
  if (strncmp(camp_params.SweepDevice, "DAC", 3)==0)
    {
      tmp = setpoint;
      setpoint = (INT) (setpoint * 204.7);
      if(dc)printf("\nDAC device: converting %f volts to  %f counts\n",tmp,setpoint);
    }
#endif
  /*
   *  Get time within tolerance
   */
  wait_msec = 20.0; /* delay in millisecs */
  

  status = camp_setVal(setpoint, camp_params);
  if(status != CAMP_SUCCESS ) 
    {
      if(dc)printf("set_sweep_device: failure return from camp_setVal\n");
      return( status );
    }
  
  ss_sleep(wait_msec);
  return( CAMP_SUCCESS );
}

/* -------------------------------------------------------------------------------------------*/
INT 
read_sweep_device( float* setpoint, CAMP_PARAMS camp_params )

/* -------------------------------------------------------------------------------------------*/
{
  /* Read the setpoint back from the sweep device

     Called by an external program

     Device must have been previously initialized 
         by a call to init_sweep_device   
              (or      init_path)            
  */
  int wait_msec;
  //double wait_sec;
  // char wait_str[32];
  float value=0;
#ifdef POL
  float counts;
#endif
  if(dc)printf("read_sweep_device: starting \n");
  if( !haveCamp ) 
    {
      if(dc)printf("read_sweep_device: Error - haveCamp is FALSE\n");
      return( -1 ); /* don't have camp */
    }

  if(dc)printf("read_sweep_device: calling campSrv_varGet \n");
  status = campSrv_varGet(camp_params.setPath, CAMP_XDR_NO_CHILD);
  if( status != CAMP_SUCCESS  ) 
    {
      printf( "read_sweep_device: failed campSrv_varGet (%d)\n",status );
      sendMsg("read_sweep_device");
      return( status );
    }
  
  /*
   *  Get time within tolerance
   */
  wait_msec = 20.0; /* delay in millisecs */
  
  status = camp_getVal(&value, camp_params);
  if(status != CAMP_SUCCESS ) 
    {
      if(dc)printf("read_sweep_device: failure return from camp_getVal\n");
      return( status );
    }
  else
    {
#ifdef POL
      /* conversion for DAC */
      if (strncmp(camp_params.SweepDevice, "DAC", 3)==0)
	{
	  if(dc)printf("read_sweep_device: success from camp_getVal; read %f\n",value);
	  counts = value;
	  value = value / 204.7;
	  if(dc)printf("\nDAC device:  converted  %f counts to  %f volts for DAC read \n",counts,value);
	}
      else
#endif
	if(dc)printf("read_sweep_device: success from camp_getVal; read %f\n",value);
    }

  *setpoint = value;
  ss_sleep(wait_msec);
  return( CAMP_SUCCESS );
}

/* -------------------------------------------------------------------------------------------*/
INT
camp_readDevDepParam_MG (float *param, CAMP_PARAMS camp_params)
/* -------------------------------------------------------------------------------------------*/
{
  /* this is for the CAMP magnet MG */
  //char str[80];
  int wait_msec,i;
  // double wait_sec;
  // char wait_str[32];
  double value=-1;
  
  *param = -99;

  /* check path exists */
  if( strncmp( camp_params.SweepDevice, "MG", 2 ) != 0 )
    {
      printf("camp_readDevDepParam_MG: invalid sweep device (%s); valid for MG device only\n",camp_params.SweepDevice);
      cm_msg( MERROR, "camp_readDevDepParam_MG","Invalid device (%s); valid for MG device only",camp_params.SweepDevice);
      sendMsg("camp_readDevDepParam_MG");
      return( -1 );
    }

  if( strcmp( camp_params.DevDepPath, "" ) == 0 )
    {
      printf("camp_readDevDepParam_MG: no device dependent path is supplied for %s\n",camp_params.SweepDevice);
      cm_msg( MERROR, "camp_readDevDepParam_MG","no device dependent path is supplied for %s",camp_params.SweepDevice);
      sendMsg("camp_readDevDepParam_MG");
      return( -1 );
    }  

  if(dc)printf("camp_readDevDepParam_MG: now reading device dependent path \"%s\" for sweep device %s\n",
	       camp_params.DevDepPath,camp_params.SweepDevice);
  if( !haveCamp ) 
    {
      if(dc)printf("camp_readDevDepParam_MG: Error - haveCamp is FALSE\n");
      return( -1 ); /* don't have camp */
    }
  
  if(dc)printf("camp_readDevDepParam_MG: calling campSrv_varGet with path %s \n",camp_params.DevDepPath);
  /* note: changed first param from camp_parames.setPath (for DAC) - should make no difference to MG */
  status = campSrv_varGet(camp_params.DevDepPath, CAMP_XDR_NO_CHILD);
  if( status != CAMP_SUCCESS  ) 
    {
      printf( "camp_readDevDepParam_MG: failed campSrv_varGet (%d)\n",status );
      sendMsg("camp_readDevDepParam_MG");
      return( status );
    }


  /*
   *  Get time within tolerance
   */
  wait_msec = 20.0; /* delay in millisecs */
  
  for( i = 1;; i++ )
    {
      status = camp_varNumGetVal( camp_params.DevDepPath, &value );
      if( status == CAMP_SUCCESS  ) break;
      else if( i >= IO_RETRIES ) 
	{
	  printf( "camp_readDevDepParam_MG: failed to get value from CAMP path \"%s\" after %d retries (%d)\n",
		  camp_params.DevDepPath,IO_RETRIES,status );
	  cm_msg( MERROR, "camp_readDevDepParam_MG","failed to get value from CAMP path \"%s\" ", camp_params.DevDepPath);
	  sendMsg("camp_readDevDepParam_MG");
	  return( status );
	}
      ss_sleep(wait_msec);
    }
  if(dc)printf("camp_readDevDepParam_MG: success returning value of %f\n",value);
  *param = (float)value;
  return (CAMP_SUCCESS);
}


#ifdef POL
/* -------------------------------------------------------------------------------------------*/
INT
camp_readDevDepParam_DAC (float *param, CAMP_PARAMS camp_params)
/* -------------------------------------------------------------------------------------------*/
{
  char str[80];
  int wait_msec,i;
  double wait_sec;
  char wait_str[32];
  double value=-1;
  char path[20]="/"; /* make path begin with a slash */
  char *p;

  *param = -99;
  printf("camp_params.SweepDevice=\"%s\" \n",camp_params.SweepDevice);

    if( strncmp( camp_params.SweepDevice, "DAC", 3 ) != 0 ) 
    {
      printf("camp_readDevDepParam_DAC: invalid sweep device (%s); valid for DAC device only\n",camp_params.SweepDevice);
      cm_msg( MERROR, "camp_readDevDepParam_MG","Invalid device (%s); valid for DAC device only",camp_params.SweepDevice);
      sendMsg("camp_readDevDepParam_DAC");
      return( CAMP_SUCCESS );
    }

  /* check path exists */
  if( strcmp( camp_params.DevDepPath, "" ) == 0 )
    {
      printf("camp_readDevDepParam_DAC: No device dependent path supplied\n",camp_params.DevDepPath);
      cm_msg( MERROR, "camp_readDevDepParam_DAC","Invalid device-dependent path supplied (%s)",camp_params.DevDepPath);
      return(-1 );
    }

  if(dc)printf("camp_readDevDepParam_DAC: now reading device dependent parameter for path %s\n",camp_params.DevDepPath);
  if( !haveCamp ) 
    {
      if(dc)printf("camp_readDevDepParam_DAC: Error - haveCamp is FALSE\n");
      return( -1 ); /* don't have camp */
    }

  
  if(dc)printf("read_sweep_device: calling campSrv_varRead with path %s\n",camp_params.DevDepPath);
  status = campSrv_varRead(camp_params.DevDepPath); 
  if( status != CAMP_SUCCESS  ) 
    {
      printf( "read_sweep_device: failed campSrv_varRead at path %s (%d)\n",camp_params.DevDepPath,status );
      sendMsg("read_sweep_device");
      return( status );
    }
  status = campSrv_varGet(camp_params.DevDepPath, CAMP_XDR_NO_CHILD);
  if( status != CAMP_SUCCESS  ) 
    {
      printf( "read_sweep_device: failed campSrv_varGet at path %s (%d)\n",camp_params.DevDepPath,status );
      sendMsg("read_sweep_device");
      return( status );
    }
  
  /*
   *  Get time within tolerance
   */
  wait_msec = 20.0; /* delay in millisecs */
  value=-99;  
  /* printf(" waiting for camp value ...\n"); */ 
   for( i = 1;; i++ )
    {
      if(dc) 
	printf("camp_readDevDepParam_DAC: calling camp_varNumGetVal with path %s \n",camp_params.DevDepPath);
      status =  camp_varNumGetVal(camp_params.DevDepPath, &value); 
      if( status == CAMP_SUCCESS  ) break;
      else if ( i >= IO_RETRIES ) 
	{
	  printf( "camp_readDevDepParam_DAC: failed campSrv_varNumGetVal for path %s (%d)\n",camp_params.DevDepPath,status );
	  sendMsg("camp_readDevDepParam_DAC");
	  return( status );
	}
      ss_sleep(wait_msec);
    }
   if(dc)printf("camp_readDevDepParam_DAC: success returning value of %f\n",value);
   *param = (float)value;
   return (CAMP_SUCCESS);

}
#endif

/* -------------------------------------------------------------------------------------------*/
INT
camp_setVal(float setpoint, CAMP_PARAMS camp_params)

/* -------------------------------------------------------------------------------------------*/
{
  /* 
     Set a setpoint for the Camp device
     
     Called from set_sweep_device

  */

    INT i;
    double setp;


    if(dc)printf("camp_setVal: using path %s to set camp device to %f %s, dc(debug)=%d\n",
		 camp_params.setPath, setpoint, camp_params.units ,dc);

    setp = (double)setpoint;

    /*
     *  Ramp to new setting
     */
    for( i = 1;; i++ )
    {
	status = campSrv_varNumSetVal( camp_params.setPath, setp );
	if( status == CAMP_SUCCESS  ) break;
	else if( i >= IO_RETRIES ) 
	{
	    printf( "camp_setVal: failed to set CAMP device after %d retries (%d)\n",IO_RETRIES,status );
	    cm_msg( MERROR, "camp_setVal","failed to set CAMP device to %f\n",setp);
	    sendMsg("camp_setVal");
	    return( status );
	}
    }

    return( CAMP_SUCCESS );
}


/* -------------------------------------------------------------------------------------------*/
INT
camp_getVal(float* value, CAMP_PARAMS camp_params )

/* -------------------------------------------------------------------------------------------*/
{
  /* 
     Read value from the Frequency Generator
     
     Called from read_sweep_device

  */

    INT i;
    double setp;

    if(dc)printf("camp_getVal: using path %s to read value; dc(debug)=%d\n",
	   camp_params.setPath,dc);



    for( i = 1;; i++ )
    {
	status = camp_varNumGetVal( camp_params.setPath, &setp );
	if( status == CAMP_SUCCESS  ) break;
	else if( i >= IO_RETRIES ) 
	{
	    printf( "camp_getVal: failed to get value from CAMP device after %d retries (%d)\n",IO_RETRIES,status );
	    cm_msg( MERROR, "camp_getVal","failed to get value from CAMP device\n");
	    sendMsg("camp_getVal");
	    return( status );
	}
    }
    *value = (float)setp;
    return( CAMP_SUCCESS );
}


/* -------------------------------------------------------------------------------------------*/
void sendMsg(char* name)

/* -------------------------------------------------------------------------------------------*/
{
  /* Get any error message from CAMP and send it out */
  char *msg;
  msg = camp_getMsg();
  if( *msg != '\0' ) 
    {
      if(dc)printf("sendMsg: called from %s\n",name);
      printf( "CAMP error msg is \"%s\"\n", msg );
      cm_msg(MERROR,"sendMsg","Camp error message:\"%s\"",msg);
    }
}

/* -------------------------------------------------------------------------------------------*/
INT
#ifdef IMUSR
camp_initPath(CAMP_PARAMS camp_params, BOOL *camp_active)
#else
camp_initPath(CAMP_PARAMS camp_params)
#endif
/* -------------------------------------------------------------------------------------------*/

{
  /* Does minimal initialization

     initializes and checks Instrument path - called from frontend.
     Assumes that full initialization and checks have already been done 
     e.g. by rf_config with a call to init_sweep_device 
  

     IMUSR version returns param camp_active (haveCamp) 
*/


  CAMP_VAR* pVar;
  float temp;
  
    if(dc)
    {
      printf("camp_initPath: starting with Instrument path=%s and dc(debug) %d\n",camp_params.InsPath,dc);
      printf("camp parameter settings:\n");
      printf("host: %s\n",camp_params.serverName);
      printf("sweep device: %s\n",camp_params.SweepDevice);
      printf("InsPath: %s\n",camp_params.InsPath);
      printf("InsType: %s\n",camp_params.InsType);
      printf("IfMod: %s\n",camp_params.IfMod);
      printf("setPath: %s\n",camp_params.setPath);
    }
#ifdef IMUSR
    haveCamp=FALSE;
#endif
  status = camp_init(camp_params);
  if( status != CAMP_SUCCESS  )
    {  /* camp_init sends its own message */
      printf( "camp_initPath: failed camp_init i.e. failed to initialize as CAMP client (%d)\n",status );
      return( status );
    }
  
  /*
   *  Update of CAMP data has just been got by camp_init
   */
  
  pVar = camp_varGetp( camp_params.InsPath );
  
  if( pVar == NULL )
    {
      printf("camp_initPath: failed camp_varGetp; bad path \"%s\"\n",camp_params.InsPath);
      cm_msg(MERROR,"camp_initPath","failed camp_varGetp; bad path \"%s\"",camp_params.InsPath);
      return (CAMP_FAILURE);
    }
  /* read the sweep device */
  status = read_sweep_device( &temp, camp_params );
  if (status == CAMP_SUCCESS)
    printf("camp_initPath: read value of %f %s from sweep device\n",temp, camp_params.units);
  else
    printf("camp_initPath: error from read_sweep_device (%d)\n",status);
#ifdef IMUSR
  printf("campPath: haveCamp=%d\n",haveCamp);
  *camp_active = haveCamp;
#endif
  return (status);
}

/* -------------------------------------------------------------------------------------------*/
INT 
init_sweep_device( CAMP_PARAMS camp_params )

/* -------------------------------------------------------------------------------------------*/
{
  /* 
     Initialize  sweep device  (full initialize )  
     
  */
  INT i,j;
  //char buf[256];
  CAMP_VAR* pVar;
  //  double max,min;
  char str[80];
  char units[15];
  bool_t online;

  if(dc)
    {
      printf("init_sweep_device: starting with serverName=%s, sweepDevice=%s\n",
	     camp_params.serverName  ,camp_params.SweepDevice );
      printf("   and Instrument path=%s, InsType=%s, IfMod=%s \n",
	     camp_params.InsPath  , camp_params.InsType ,camp_params.IfMod );
      printf("   and setPath= %s , dc(debug)=%d\n",camp_params.setPath,dc);
    }
  
  status = camp_init( camp_params);
  if( status != CAMP_SUCCESS  )
    { /* camp_init sends its own messages */
      printf( "init_sweep_device: failed camp_init i.e. failed to initialize as CAMP client (%d)\n",status );
      return( status );
    }
  
  /*
   *  Get update of CAMP data
   */
  
  if(dc)
    {
      printf("init_sweep_device starting with Instrument path %s and dc(debug) %d\n",camp_params.InsPath,dc);
      printf("camp parameter settings:\n");
      printf("host: %s\n",camp_params.serverName);
      printf("sweep device: %s\n",camp_params.SweepDevice);
      printf("InsPath: %s\n",camp_params.InsPath);
      printf("InsType: %s\n",camp_params.InsType);
      printf("IfMod: %s\n",camp_params.IfMod);
      printf("setPath: %s\n",camp_params.setPath);
    }
  
  status = camp_clntUpdate();
  if( status != CAMP_SUCCESS  ) 
    {
      printf( "init_sweep_device: failed camp_clntUpdate (%d)\n",status );
      cm_msg( MERROR,"init_sweep_device","failed camp_clntUpdate" );
      sendMsg("init_sweep_device");
      return( status );
    }
  if(dc)printf("calling camp_varGetp with path \"%s\"\n",camp_params.InsPath);
  pVar = camp_varGetp( camp_params.InsPath );
  
  if( pVar == NULL )
    {
      printf( "init_sweep_device: adding instrument %s to CAMP...\n",camp_params.InsPath );
    }
  else if( !streq( camp_params.InsType, pVar->spec.CAMP_VAR_SPEC_u.pIns->typeIdent ) )
    {
      printf( "init_sweep_device: replacing instrument '%s' in CAMP...\n",camp_params.InsPath );
      if(dc)printf("comparison of InsType: ours = \"%s\", camp's existing = \"%s\"\n",
		   camp_params.InsType, pVar->spec.CAMP_VAR_SPEC_u.pIns->typeIdent); 
      
      status = campSrv_insDel(camp_params.InsPath);
      if( status !=CAMP_SUCCESS ) 
	{
	  printf( "init_sweep_device: failed to remove old '%s' from CAMP\n",camp_params.InsPath );
	  cm_msg( MERROR,"init_sweep_device","failed to remove old '%s' from CAMP",camp_params.InsPath );
	  sendMsg("init_sweep_device");
	  return( status );
	}
      
      pVar = NULL;
    }
  
  if( pVar == NULL ) 
    {
      /*
       *  Add instrument.
       */
      status = campSrv_insAdd( camp_params.InsType, camp_params.InsPath );
      if( status != CAMP_SUCCESS ) 
	{
	  printf( "init_sweep_device: failed to add '%s' (type %s) to CAMP\n",
		  camp_params.InsPath, camp_params.InsType );
	  cm_msg(MERROR,"init_sweep_device","failed to add '%s' to CAMP",
		 camp_params.InsPath );
	  sendMsg("init_sweep_device");
	  return( status );
	}
      
      status = camp_clntUpdate();
      if( status != CAMP_SUCCESS ) 
	{
	  printf( "Init_Sweep_Device: failed camp_clntUpdate\n" );
	  cm_msg( MERROR,"Init_Sweep_Device","failed camp_clntUpdate" );
	  sendMsg("init_sweep_device");
	  return( status );
	}
      
      pVar = camp_varGetp(camp_params.InsPath  );
      if( pVar == NULL ) 
	{
	  printf( "Init_Sweep_Device: instrument '%s' doesn't exist\n",camp_params.InsPath );
	  cm_msg( MERROR,"Init_Sweep_Device","instrument '%s' doesn't exist",camp_params.InsPath );
	  return( CAMP_INVAL_INS );
	}
      else
	{
	  printf( "Init_Sweep_Device: instrument '%s'  is ready\n",camp_params.InsPath );
	}
    }
  
  /*
   *  Make sure there's a default interface definition
   *  that we will modify.
   */
  if( pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL )
    {
      printf( "Init_Sweep_Device: CAMP instrument '%s' has an invalid\n",camp_params.InsPath );
      printf( "        default interface definition.  Get help!\n" );
      cm_msg(MERROR,"Init_Sweep_Device","CAMP instrument '%s' has invalid default interface definition",
	     camp_params.InsPath );
      return( CAMP_INVAL_INS );
    }
  
  status = camp_insGetLine(camp_params.InsPath,&online);
  printf("after camp_insgetline, online flag=%d, status=%d\n",online,status);
  if ( !online)
    {
      
      /* do not turn the instrument offline when it's already online) */
      /*    printf("Init_Sweep_Device: Turning instrument offline\n"); */
	       /*
		*  Make sure it's offline
		*/
      /*campSrv_insLine( camp_params.InsPath, FALSE ); */
	
	/*
	 *  Set the port
	 */
      /*
	sprintf( buf, "insSet %s -if_mod %s", camp_params.InsPath,camp_params.IfMod );
	status = campSrv_cmd( buf );
	if( _failure( status ) ) 
	{
	printf( 
	"Init_Sweep_Device: failed to set the interface for '%s'\n",camp_params.InsPath );
	cm_msg(MERROR, 
	"Init_Sweep_Device","failed to set the interface for '%s'",camp_params.InsPath );
	sendMsg("camp_setSD");
	return( status );
	}
      */
      
      printf("Init_Sweep_Device: Now turning instrument online\n");
      /*
       *  Turn it online
       */
      for( i = 1; i <= IO_RETRIES; i++ )
	{
	  status = campSrv_insLine( camp_params.InsPath, TRUE );
	  if( _success( status ) ) break;
	  else if( i == IO_RETRIES ) 
	    {
	      printf( "Init_Sweep_Device: failed to set instrument '%s' online\n",camp_params.InsPath );
	      cm_msg( MERROR,"Init_Sweep_Device","failed to set instrument '%s' online",camp_params.InsPath );
	      sendMsg("init_sweep_device");
	      return( status );
	    }
	}
    } /* end of instrument offline */
  
  /* make sure that the set path actually contains the path */ 
  {
    char temp1[20],temp2[20];
    camp_pathGetFirst(camp_params.setPath, temp1, sizeof(temp1));
    camp_pathGetFirst(camp_params.InsPath, temp2, sizeof(temp2));
    if(! camp_pathCompare(temp1,temp2))
      {
	printf("Init_Sweep_Device: set path (\"%s\" does not contain instrument path (\"%s\")\n",
	       camp_params.setPath,camp_params.InsPath );
	cm_msg(MERROR,"Init_Sweep_Device"," set path (\"%s\" does not contain instrument path (\"%s\")",
	       camp_params.setPath,camp_params.InsPath );
	return(CAMP_FAILURE);
	
      }
  }
  /* Check the units for the sweep */
  if(dc)printf("Init_Sweep_Device: reading the units for the sweep\n");
  for( i = 1;;i++ )
    {
      status = camp_varNumGetUnits( camp_params.setPath, str, sizeof(str) );
      if( _success( status ) ) break;
      else if( i >= IO_RETRIES ) 
	{
	  printf( "Init_Sweep_Device: failed to get units from CAMP device after %d retries (%d)\n",IO_RETRIES,status );
	  cm_msg( MERROR, "Init_Sweep_Device","failed to get units from CAMP device\n");
	  sendMsg("Init_Sweep_Device"); 
	  /*  return( status );  */
	  sprintf(str,"X"); /* non-fatal error */
	  break;
	}
    }
  printf("Units from CAMP : %s\n",str);


    /* Check the units for the sweep */
    if(dc)printf("Init_Sweep_Device: reading the units for the sweep\n");
    for( i = 1;;i++ )
      {
	status = camp_varNumGetUnits( camp_params.setPath, str,  sizeof(str));
	if( _success( status ) ) break;
	else if( i >= IO_RETRIES ) 
	  {
	    printf( "Init_Sweep_Device: failed to get units from CAMP device after %d retries (%d)\n",IO_RETRIES,status );
	    cm_msg( MERROR, "Init_Sweep_Device","failed to get units from CAMP device\n");
	    sendMsg("Init_Sweep_Device"); 
	    return( status );
	  }
      }
    printf("Units from CAMP : %s\n",str);

    sprintf(units,"%s",camp_params.units);
    for  (j=0; j< strlen(units) ; j++)
      units[j] = tolower (units[j]); /* convert to lower case */
     for  (j=0; j< strlen(str) ; j++)
      str[j] = tolower (str[j]); /* convert to lower case */
     if (strncmp(units,str,strlen(str)) == 0)
       printf("units %s are identical\n",camp_params.units);
     else
       printf("units are NOT identical, %s and %s from camp\n",camp_params.units,str);
    


    /* IMUSR Device-dependent initialization - will be handled in a script */

#ifndef IMUSR

    /* Device-dependent initialization for BNMR/BNQR  */
    stoupper( camp_params.SweepDevice );
    if( strncmp( camp_params.SweepDevice, "FG", 2 ) == 0 )
    {
      if(dc)printf("Init_Sweep_Device: device dependent initialization for AFG :none at present\n");
    }
  else if( strncmp( camp_params.SweepDevice, "MG", 2 ) == 0 )
    {
      if(dc)printf("Init_Sweep_Device: device dependent initialization for Magnet :none at present\n");
      
      /* this was the max/min current not field so don't read it
	 if(dc)printf("Init_Sweep_Device: device dependent initialization for Magnet\n");
	 sprintf(str,"%s/setup/mag_max",camp_params.InsPath);
	 for( i = 1;;i++ )
	 {
	 status = camp_varNumGetVal( str, &max );
	 if( _success( status ) ) break;
	 else if( i >= IO_RETRIES ) 
	 {
	 printf( "Init_Sweep_Device: failed to get max value from CAMP device after %d retries (%d)\n",IO_RETRIES,status );
	 cm_msg( MERROR, "Init_Sweep_Device","failed to get value from CAMP device\n");
	 sendMsg("Init_Sweep_Device"); 
	 return( status );
	 }
	 }
	 sprintf(str,"%s/setup/mag_min",camp_params.InsPath);
	 for( i = 1;;i++ )
	 {
	 status = camp_varNumGetVal( str, &min );
	 if( _success( status ) ) break;
	 else if( i >= IO_RETRIES ) 
	 {
	 printf( "Init_Sweep_Device: failed to get min value from CAMP device after %d retries (%d)\n",IO_RETRIES,status );
	 cm_msg( MERROR, "Init_Sweep_Device","failed to get value from CAMP device\n");
	 sendMsg("Init_Sweep_Device"); 
	 return( status );
	 }
	 }
	 printf("max=%f min=%f\n",max,min);
	 if (max < camp_params.maximum_value) camp_params.maximum_value = max; 
	 if (min > camp_params.minimum_value) camp_params.minimum_value = min;
      */	 
	 
	 /*
	  *  Set to non-persistant mode
	  */
      /*    for( i = 1; i <= IO_RETRIES; i++ )
	    {
	      status = campSrv_varSelSetVal( "/imusr_mag/setup/ramp_mode", 0 );
	      if( _success( status ) ) break;
	      else if( i == IO_RETRIES )
	      {
	      printf( "I-MuSR: failed to set 'imusr_mag' ramp mode\n" );
	      return( status );
	      }
	      }
      */
      
    }
  else
      if(dc)printf("Init_Sweep_Device: No device dependent initialization for %s \n", camp_params.SweepDevice );
    

	/* this was the max/min current not field so don't read it
	   if(dc)printf("Init_Sweep_Device: device dependent initialization for Magnet\n");
	   sprintf(str,"%s/setup/mag_max",camp_params.InsPath);
	   for( i = 1;;i++ )
	   {
	   status = camp_varNumGetVal( str, &max );
	   if( _success( status ) ) break;
	   else if( i >= IO_RETRIES ) 
	   {
	   printf( "Init_Sweep_Device: failed to get max value from CAMP device after %d retries (%d)\n",IO_RETRIES,status );
	   cm_msg( MERROR, "Init_Sweep_Device","failed to get value from CAMP device\n");
	   sendMsg("Init_Sweep_Device"); 
	   return( status );
	   }
	   }
	   sprintf(str,"%s/setup/mag_min",camp_params.InsPath);
	   for( i = 1;;i++ )
	   {
	   status = camp_varNumGetVal( str, &min );
	   if( _success( status ) ) break;
	   else if( i >= IO_RETRIES ) 
	   {
	   printf( "Init_Sweep_Device: failed to get min value from CAMP device after %d retries (%d)\n",IO_RETRIES,status );
	   cm_msg( MERROR, "Init_Sweep_Device","failed to get value from CAMP device\n");
	   sendMsg("Init_Sweep_Device"); 
	   return( status );
	   }
	   }
	   printf("max=%f min=%f\n",max,min);
	   if (max < camp_params.maximum_value) camp_params.maximum_value = max; 
	   if (min > camp_params.minimum_value) camp_params.minimum_value = min;
	*/

    /*
     *  Set to non-persistant mode
     */
	/*    for( i = 1; i <= IO_RETRIES; i++ )
	      {
		status = campSrv_varSelSetVal( "/imusr_mag/setup/ramp_mode", 0 );
		if( _success( status ) ) break;
		else if( i == IO_RETRIES )
		{
		printf( "I-MuSR: failed to set 'imusr_mag' ramp mode\n" );
		return( status );
		}
		}
	*/

#endif  // end of ifndef IMUSR
    return( CAMP_SUCCESS );
}



























































