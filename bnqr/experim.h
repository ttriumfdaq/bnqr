/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Thu Sep 26 12:16:04 2019

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  INT       run_mode;
  char      comment[32];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) const char *_name[] = {\
"[.]",\
"run mode = INT : 0",\
"comment = STRING : [32] VMIC Version",\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  char      run_title[128];
  DWORD     experiment_number;
  char      experimenter[128];
  char      sample[128];
  char      orientation[128];
  char      temperature[80];
  char      field[80];
  INT       run_mode;
  INT       number_of_scans;
  BOOL      write_data;
  BOOL      edit_run_number;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) const char *_name[] = {\
"[.]",\
"run_title = STRING : [128] test",\
"experiment number = DWORD : 0",\
"experimenter = STRING : [128] sd",\
"sample = STRING : [128] ",\
"orientation = STRING : [128] ",\
"temperature = STRING : [80] 292.549(0.002)K",\
"field = STRING : [80] 120G",\
"run mode = LINK : [36] /Experiment/run parameters/run mode",\
"Number of scans = LINK : [48] /Equipment/FIFO_acq/frontend/hardware/num scans",\
"write data = LINK : [35] /Logger/Channels/0/Settings/Active",\
"Edit run number = BOOL : n",\
"",\
NULL }

#ifndef EXCL_FIFO_ACQ

#define FIFO_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} FIFO_ACQ_COMMON;

#define FIFO_ACQ_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 5",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnqr.triumf.ca",\
"Frontend name = STRING : [32] feBNQR_VMIC",\
"Frontend file name = STRING : [256] /home/bnqr/online/bnqr_alpha/febnqr_vmic.c",\
"Status = STRING : [256] feBNQR_VMIC@lxbnqr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define FIFO_ACQ_MDARC_DEFINED

typedef struct {
  struct {
    struct {
      double    histogram_totals[29];
      double    total_saved;
    } output;
    struct {
      BOOL      use_defaults;
      INT       number_of_regions;
      INT       first_time_bin[5];
      char      range_label[5][12];
      INT       last_time_bin[5];
      INT       start_freq__hz_;
      INT       end_freq__hz_;
    } midbnmr;
    INT       number_defined;
    INT       num_bins;
    float     dwell_time__ms_;
    INT       resolution_code;
    char      titles[29][32];
    INT       bin_zero[29];
    INT       first_good_bin[29];
    INT       last_good_bin[29];
    INT       first_background_bin[29];
    INT       last_background_bin[29];
    char      type1_titles[25][32];
    char      sampleref_titles[8][32];
    char      alpha_titles[8][32];
    char      type2_default_titles[10][32];
  } histograms;
  char      time_of_last_save[32];
  char      last_saved_filename[128];
  char      saved_data_directory[128];
  DWORD     num_versions_before_purge;
  BOOL      endrun_purge_and_archive;
  BOOL      suppress_save_temporarily;
  DWORD     save_interval_sec_;
  char      archiver_task[80];
  char      archived_data_directory[128];
  char      last_archived_filename[128];
  BOOL      toggle;
  BOOL      disable_run_number_check;
  char      run_type[5];
  char      perlscript_path[50];
  BOOL      enable_mdarc_logging;
  struct {
    char      camp_hostname[128];
    char      temperature_variable[128];
    char      field_variable[128];
    char      rf_power_variable[128];
    BOOL      enable_weighted_averaging;
    BOOL      record_temperature_error;
    BOOL      record_field_error;
  } camp;
  BOOL      enable_prestart_rn_check;
} FIFO_ACQ_MDARC;

#define FIFO_ACQ_MDARC_STR(_name) const char *_name[] = {\
"[histograms/output]",\
"histogram totals = DOUBLE[29] :",\
"[0] 2463283712",\
"[1] 492656768",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"total saved = DOUBLE : 2955940572",\
"",\
"[histograms/midbnmr]",\
"use defaults = BOOL : y",\
"number of regions = INT : 1",\
"first time bin = INT[5] :",\
"[0] 1",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"range label = STRING[5] :",\
"[12] ",\
"[12] ",\
"[12] ",\
"[12] ",\
"[12] ",\
"last time bin = INT[5] :",\
"[0] 101",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"start freq (Hz) = INT : 232000",\
"end freq (Hz) = INT : 234000",\
"",\
"[histograms]",\
"number defined = INT : 10",\
"num bins = INT : 69",\
"dwell time (ms) = FLOAT : 119",\
"resolution code = INT : -1",\
"titles = STRING[29] :",\
"[32] Const",\
"[32] FluM2",\
"[32] L+",\
"[32] R+",\
"[32] L-",\
"[32] R-",\
"[32] NBMB+",\
"[32] NBMF+",\
"[32] NBMB-",\
"[32] NBMF-",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] 31593I",\
"[32] ",\
"bin zero = INT[29] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"first good bin = INT[29] :",\
"[0] 10",\
"[1] 10",\
"[2] 10",\
"[3] 10",\
"[4] 10",\
"[5] 10",\
"[6] 10",\
"[7] 10",\
"[8] 10",\
"[9] 10",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"last good bin = INT[29] :",\
"[0] 64",\
"[1] 64",\
"[2] 64",\
"[3] 64",\
"[4] 64",\
"[5] 64",\
"[6] 64",\
"[7] 64",\
"[8] 64",\
"[9] 64",\
"[10] 1700",\
"[11] 1700",\
"[12] 1700",\
"[13] 1700",\
"[14] 1700",\
"[15] 1700",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"first background bin = INT[29] :",\
"[0] 65",\
"[1] 65",\
"[2] 65",\
"[3] 65",\
"[4] 65",\
"[5] 65",\
"[6] 65",\
"[7] 65",\
"[8] 65",\
"[9] 65",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"last background bin = INT[29] :",\
"[0] 68",\
"[1] 68",\
"[2] 68",\
"[3] 68",\
"[4] 68",\
"[5] 68",\
"[6] 68",\
"[7] 68",\
"[8] 68",\
"[9] 68",\
"[10] 100",\
"[11] 100",\
"[12] 100",\
"[13] 100",\
"[14] 100",\
"[15] 100",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"type1_titles = STRING[25] :",\
"[32] B+",\
"[32] F+",\
"[32] B-",\
"[32] F-",\
"[32] Const",\
"[32] FluM2",\
"[32] NBMB+",\
"[32] NBMF+",\
"[32] NBMB-",\
"[32] NBMF-",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"sampleref titles = STRING[8] :",\
"[32] SL+",\
"[32] SR+",\
"[32] SL-",\
"[32] SR-",\
"[32] NBSB+",\
"[32] NBSF+",\
"[32] NBSB-",\
"[32] NBSF-",\
"alpha titles = STRING[8] :",\
"[32] AL0+",\
"[32] AL1+",\
"[32] AL2+",\
"[32] AL3+",\
"[32] AL0-",\
"[32] AL1-",\
"[32] AL2-",\
"[32] AL3-",\
"type2_default_titles = STRING[10] :",\
"[32] Const",\
"[32] FluM2",\
"[32] L+",\
"[32] R+",\
"[32] L-",\
"[32] R-",\
"[32] NBMB+",\
"[32] NBMF+",\
"[32] NBMB-",\
"[32] NBMF-",\
"",\
"[.]",\
"time_of_last_save = STRING : [32] Wed Aug 14 13:06:35 2019",\
"last_saved_filename = STRING : [128] /isdaq/data1/bnqr/dlog/current/030082.msr",\
"saved_data_directory = STRING : [128] /isdaq/data1/bnqr/dlog/current",\
"num_versions_before_purge = DWORD : 45",\
"endrun_purge_and_archive = BOOL : y",\
"suppress_save_temporarily = BOOL : n",\
"save_interval(sec) = DWORD : 60",\
"archiver task = STRING : [80] scp -B -i /home/bnqr/bnqr_archiver_id",\
"archived_data_directory = STRING : [128] musrarc@cmms:dlog",\
"last_archived_filename = STRING : [128] musrarc@cmms:dlog/045288.msr",\
"toggle = BOOL : n",\
"disable run number check = BOOL : n",\
"run type = STRING : [5] test",\
"perlscript path = STRING : [50] /home/bnqr/online/perl",\
"enable mdarc logging = BOOL : n",\
"",\
"[camp]",\
"camp hostname = STRING : [128] polvw.triumf.ca",\
"temperature variable = STRING : [128] /Sample/read_B",\
"field variable = STRING : [128] ",\
"RF power variable = STRING : [128] ",\
"enable weighted averaging = BOOL : n",\
"record temperature error = BOOL : y",\
"record field error = BOOL : n",\
"",\
"[.]",\
"enable prestart rn check = BOOL : n",\
"",\
NULL }

#define FIFO_ACQ_FRONTEND_DEFINED

typedef struct {
  struct {
    DWORD     num_cycles;
    DWORD     fluor_monitor_thr;
    float     cycle_thr1;
    float     cycle_thr2;
    float     cycle_thr3;
    INT       diagnostic_channel_num;
    BOOL      re_reference;
    BOOL      out_of_tol_repeat_scan;
    DWORD     skip_ncycles_out_of_tol;
    DWORD     num_polarization_cycles;
    DWORD     polarization_switch_delay;
    BOOL      enable_sis_ref_ch1_scaler_a;
    BOOL      enable_sis_ref_ch1_scaler_b;
    BOOL      enable_helicity_flipping;
    INT       helicity_flip_sleep__ms_;
    BOOL      enable_all_hel_checks;
    BOOL      enable_dual_channel_mode;
    BOOL      enable_epics_switch_checks;
    BOOL      check_rf_trip;
    INT       num_scans;
    float     rf_trip_threshold__0_5v_;
    DWORD     skip_ncycles_laser_tol;
    BOOL      disable_laser_thr_check;
    BOOL      enable_sampleref_mode;
    BOOL      daq_drives_sampleref;
    BOOL      enable_alpha_mode;
    INT       dummy;
    struct {
      INT       sis_mode;
      DWORD     input_mode;
      DWORD     output_mode;
      struct {
        DWORD     enable_channels_mask;
      } module_a;
      struct {
        DWORD     enable_channels_mask;
      } module_b;
      BOOL      send_mcs_bank;
      BOOL      hot_debug;
      BOOL      discard_first_bin;
    } sis3820;
    struct {
      DWORD     idle_freq__hz_;
      BOOL      freq_end_sweep_jump_to_idle;
      BOOL      freq_sweep_load_1st_val_in_idle;
      DWORD     fref_tuning_freq__hz_;
      struct {
        BOOL      profile_enabled;
        BOOL      simulate_single_tone_mode;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__max_plus_or_minus_511_;
          INT       idle_q__max_plus_or_minus_511_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__max_plus_or_minus_511_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__max_plus_or_minus_511_;
        } iq_modulation;
        INT       gate_control;
      } one_f;
      struct {
        BOOL      profile_enabled;
        BOOL      simulate_single_tone_mode;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__max_plus_or_minus_511_;
          INT       idle_q__max_plus_or_minus_511_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__max_plus_or_minus_511_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__max_plus_or_minus_511_;
        } iq_modulation;
        INT       gate_control;
      } fref;
    } psm;
  } hardware;
  struct {
    struct {
      BOOL      test_mode;
      DWORD     num_bins;
      float     dwell_time__ms_;
      INT       test_setup_mode_number;
    } sis3801;
    struct {
      INT       lne_prescale_factor;
      INT       num_bins_requested;
      struct {
        DWORD     test_pulse_mask;
      } module_a;
      struct {
        DWORD     test_pulse_mask;
      } module_b;
    } sis3820;
  } sis_test_mode;
  struct {
    BOOL      hold;
  } flags;
  struct {
    char      experiment_name[32];
    char      cfg_path[128];
    char      ppg_path[128];
    float     beam_off_time__ms_;
    DWORD     num_beam_precycles;
    DWORD     num_beam_acq_cycles;
    DWORD     frequency_start__hz_;
    DWORD     frequency_stop__hz_;
    DWORD     frequency_increment__hz_;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    DWORD     num_rf_on_delays__dwell_times_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    float     mcs_max_delay__ms_;
    float     daq_service_time__ms_;
    float     minimal_delay__ms_;
    float     time_slice__ms_;
    float     e1b_dwell_time__ms_;
    float     rf_delay__ms_;
    float     bg_delay__ms_;
    DWORD     num_rf_cycles;
    INT       e2b_num_beam_on_dwell_times;
    DWORD     e1f_num_dwell_times;
    BOOL      check_recent_compiled_file;
    DWORD     freq_single_slice_width__hz_;
    DWORD     num_freq_slices;
    float     prebeam_on_time__ms_;
    float     flip_360_delay__ms_;
    float     flip_180_delay__ms_;
    float     f_slice_internal_delay__ms_;
    char      beam_mode;
    char      counting_mode;
    float     f_select_pulselength__ms_;
    DWORD     e00_prebeam_dwelltimes;
    DWORD     rfon_dwelltime;
    DWORD     e00_beam_on_dwelltimes;
    DWORD     e00_beam_off_dwelltimes;
    INT       rfon_duration__dwelltimes_;
    INT       num_type1_frontend_histograms;
    INT       num_type1_fe_histos_alpha_mode;
    INT       num_type2_frontend_histograms;
    INT       num_type2_fe_histos_sr_mode;
    INT       num_type2_fe_histos_alpha_mode;
    char      e1a_and_e1b_pulse_pairs[4];
    char      e1a_and_e1b_freq_mode[3];
    BOOL      randomize_freq_values;
    float     e2c_beam_on_time__ms_;
    INT       num_cycles_per_supercycle;
    float     navolt_start;
    float     navolt_stop;
    float     navolt_inc;
    float     laser_start;
    float     laser_stop;
    float     laser_inc;
    float     e1c_camp_start;
    float     e1c_camp_stop;
    float     e1c_camp_inc;
    float     field_start;
    float     field_stop;
    float     field_inc;
    BOOL      e2a_pulse_pairs;
    BOOL      e2a_180;
    char      e2a_ubit1_action[10];
    DWORD     e2e_num_dwelltimes_per_freq;
    DWORD     e2e_num_rf_dwelltimes_per_freq;
    DWORD     e2e_num_postrfbeamon_dwelltimes;
    BOOL      e1f_const_time_between_cycles;
    float     e1f_min_daq_service__ms_;
    BOOL      e1f_enable_alpha_histos;
  } input;
  struct {
    char      ppg_loadfile[128];
    char      ppg_template[20];
    float     beam_on_time__ms_;
    float     frequency_stop__hz_;
    DWORD     num_frequency_steps;
    DWORD     e1_number_of_incr__ninc_;
    DWORD     e1_number_of_cycles_per_scan;
    float     dwell_time__ms_;
    DWORD     num_dwell_times;
    float     ppg_cycle_time__ms_;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    DWORD     compiled_file_time__binary_;
    char      compiled_file_time[32];
    char      e1c_camp_path[32];
    char      e1c_camp_ifmod[10];
    char      e1c_camp_instr_type[32];
    char      e1n_epics_device[32];
    INT       psm_num_iq_pairs__niq_;
    INT       psm_cic_interpoln_rate__ncic_;
    float     psm_bandwidth__rad_per_sec_;
    float     psm_pulse_width__msec_;
    INT       psm_num_cycles_iq_pairs__nc_;
    INT       psm_max_cycles_iq_pairs__ncmx_;
    float     psm_linewidth_mod_param__a_;
    float     psm_amplitude_mod_param__alpha_;
    DWORD     last_seed__for_random_freqs_;
    BOOL      epics_dual_channel_switch;
    DWORD     e2a_pulse_pairs_mode;
    DWORD     e1b_bg_delay__dwelltimes_;
    DWORD     e1b_rf_delay__dwelltimes_;
    DWORD     e1b_rf_on__dwelltimes_;
    DWORD     e1b_rf_off__dwelltimes_;
    BOOL      vme_beam_control;
    float     epics_bnmr_beam_period;
    float     epics_bnqr_beam_period;
    float     epics_bnmr_beam_delay;
    float     epics_bnqr_beam_delay;
    DWORD     e2e_num_histo_bins_per_ch;
    DWORD     e2e_histo_ntuple_width__bins_;
    DWORD     e2e_num_post_ntuple_bins;
    DWORD     e2e_ntuple_depth__bins_;
    DWORD     e2e_num_beam_off_dwelltimes;
    DWORD     e2e_scaler_ntuple_width__bins_;
    DWORD     e2f_num_beam_on_dwelltimes;
    float     epics_bnmr_period;
    float     epics_bnmr_delay;
    float     epics_bnqr_period;
    float     epics_bnqr_delay;
    struct {
      INT       lne_frequency__hz_;
      DWORD     lne_preset;
      DWORD     modid_a;
      DWORD     modid_b;
    } sis3820;
    struct {
      BOOL      device_disabled;
      INT       max_cycles_iq_pairs__ncmx_;
      INT       num_iq_pairs__niq_[4];
      INT       cic_interpoln_rate__ncic_[4];
      float     bandwidth__rad_per_sec_[4];
      float     pulse_width__msec_[4];
      INT       num_cycles_iq_pairs__nc_[4];
      float     linewidth_mod_param__a_[4];
      float     amplitude_mod_param__alpha_[4];
    } psm;
    struct {
      DWORD     modid_a;
      DWORD     modid_b;
      DWORD     lne_preset;
    } sis3801;
  } output;
  struct {
    BOOL      debug;
    BOOL      dppg;
    BOOL      dcamp;
    BOOL      dpsm;
    BOOL      manual_ppg_start;
    BOOL      dsis;
    INT       hot_debug_level___1_clr_;
    INT       hot_threshold_trip;
  } debug;
} FIFO_ACQ_FRONTEND;

#define FIFO_ACQ_FRONTEND_STR(_name) const char *_name[] = {\
"[Hardware]",\
"num cycles = DWORD : 0",\
"Fluor monitor thr = DWORD : 0",\
"Cycle thr1 = FLOAT : 0",\
"Cycle thr2 = FLOAT : 0",\
"Cycle thr3 = FLOAT : 0",\
"Diagnostic channel num = INT : 10",\
"Re-reference = BOOL : n",\
"out-of-tol repeat scan = BOOL : n",\
"skip ncycles out-of-tol = DWORD : 2",\
"num polarization cycles = DWORD : 0",\
"polarization switch delay = DWORD : 0",\
"Enable SIS ref ch1 scaler A = BOOL : n",\
"Enable SIS ref ch1 scaler B = BOOL : y",\
"Enable helicity flipping = BOOL : n",\
"helicity flip sleep (ms) = INT : 4000",\
"enable all hel checks = BOOL : n",\
"Enable dual channel mode = BOOL : n",\
"enable epics switch checks = BOOL : y",\
"Check RF trip = BOOL : n",\
"Num scans = INT : 0",\
"RF trip threshold (0-5V) = FLOAT : 0",\
"skip ncycles laser-tol = DWORD : 1",\
"disable Laser thr check = BOOL : y",\
"enable SampleRef mode = BOOL : n",\
"DAQ drives SampleRef = BOOL : y",\
"enable Alpha mode = BOOL : n",\
"dummy = INT : 0",\
"",\
"[Hardware/sis3820]",\
"SIS mode = INT : 2",\
"Input Mode = DWORD : 3",\
"Output Mode = DWORD : 2",\
"",\
"[Hardware/sis3820/module_A]",\
"Enable Channels mask = DWORD : 0",\
"",\
"[Hardware/sis3820/module_B]",\
"Enable Channels mask = DWORD : 0",\
"",\
"[Hardware/sis3820]",\
"send mcs bank = BOOL : n",\
"hot debug = BOOL : y",\
"Discard first bin = BOOL : n",\
"",\
"[Hardware/PSM]",\
"Idle freq (Hz) = DWORD : 63299",\
"freq end sweep jump to idle = BOOL : n",\
"freq sweep load 1st val in idle = BOOL : n",\
"fREF Tuning freq (Hz) = DWORD : 75000",\
"",\
"[Hardware/PSM/one_f]",\
"profile enabled = BOOL : n",\
"simulate single tone mode = BOOL : n",\
"quadrature modulation mode = BOOL : y",\
"scale factor (def 181 max 255) = INT : 255",\
"",\
"[Hardware/PSM/one_f/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] ln-sech",\
"requested bandwidth (Hz) = INT : 50",\
"jump to idle IQ = BOOL : n",\
"load first val in idle = BOOL : n",\
"idle i (max plus or minus 511) = INT : 0",\
"idle q (max plus or minus 511) = INT : 0",\
"load i,q pairs file = BOOL : y",\
"set constant i value in file = BOOL : n",\
"const i (max plus or minus 511) = INT : 255",\
"set constant q value in file = BOOL : n",\
"const q (max plus or minus 511) = INT : 0",\
"",\
"[Hardware/PSM/one_f]",\
"gate control = INT : 1",\
"",\
"[Hardware/PSM/fref]",\
"profile enabled = BOOL : n",\
"simulate single tone mode = BOOL : n",\
"quadrature modulation mode = BOOL : n",\
"scale factor (def 181 max 255) = INT : 1",\
"",\
"[Hardware/PSM/fref/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] Hermite",\
"requested bandwidth (Hz) = INT : 50",\
"jump to idle IQ = BOOL : y",\
"load first val in idle = BOOL : n",\
"idle i (max plus or minus 511) = INT : 511",\
"idle q (max plus or minus 511) = INT : 0",\
"load i,q pairs file = BOOL : n",\
"set constant i value in file = BOOL : n",\
"const i (max plus or minus 511) = INT : 255",\
"set constant q value in file = BOOL : n",\
"const q (max plus or minus 511) = INT : 0",\
"",\
"[Hardware/PSM/fref]",\
"gate control = INT : 1",\
"",\
"[sis test mode/sis3801]",\
"test mode = BOOL : n",\
"num bins = DWORD : 100",\
"dwell time (ms) = FLOAT : 1",\
"Test Setup Mode Number = INT : 2",\
"",\
"[sis test mode/sis3820]",\
"LNE prescale factor = INT : 1000",\
"num bins requested = INT : 500",\
"",\
"[sis test mode/sis3820/module_a]",\
"test pulse mask = DWORD : 0",\
"",\
"[sis test mode/sis3820/module_b]",\
"test pulse mask = DWORD : 4161798143",\
"",\
"[flags]",\
"hold = BOOL : n",\
"",\
"[Input]",\
"Experiment name = STRING : [32] 1e",\
"CFG path = STRING : [128] /home/bnqr/online/bnqr/ppgload",\
"PPG path = STRING : [128] /home/bnqr/online/ppg-templates",\
"beam off time (ms) = FLOAT : 0",\
"num beam PreCycles = DWORD : 0",\
"num_beam_acq_cycles = DWORD : 0",\
"frequency start (Hz) = DWORD : 4000",\
"frequency stop (Hz) = DWORD : 4500",\
"frequency increment (Hz) = DWORD : 50",\
"RF on time (ms) = FLOAT : 119",\
"RF off time (ms) = FLOAT : 0.0005",\
"num RF on delays (dwell times) = DWORD : 10",\
"MCS enable delay (ms) = FLOAT : 0.001",\
"MCS enable gate (ms) = FLOAT : 10",\
"MCS max delay (ms) = FLOAT : 1",\
"DAQ service time (ms) = FLOAT : 0",\
"Minimal delay (ms) = FLOAT : 0.0005",\
"Time slice (ms) = FLOAT : 0.0001",\
"E1B Dwell time (ms) = FLOAT : 1",\
"RF delay (ms) = FLOAT : 32",\
"Bg delay (ms) = FLOAT : 0",\
"Num RF cycles = DWORD : 1",\
"E2B Num beam on dwell times = INT : 500",\
"e1f num dwell times = DWORD : 100",\
"Check recent compiled file = BOOL : y",\
"freq single slice width (Hz) = DWORD : 10",\
"Num freq slices = DWORD : 1",\
"prebeam on time (ms) = FLOAT : 1",\
"flip 360 delay (ms) = FLOAT : 10",\
"flip 180 delay (ms) = FLOAT : 40",\
"f slice internal delay (ms) = FLOAT : 10",\
"beam_mode = CHAR : C",\
"counting_mode = CHAR : 1",\
"f select pulselength (ms) = FLOAT : 10",\
"e00 prebeam dwelltimes = DWORD : 100",\
"RFon dwelltime = DWORD : 0",\
"e00 beam on dwelltimes = DWORD : 400",\
"e00 beam off dwelltimes = DWORD : 1200",\
"rfon duration (dwelltimes) = INT : 0",\
"num type1 frontend histograms = INT : 7",\
"num type1 fe histos alpha mode = INT : 11",\
"num type2 frontend histograms = INT : 10",\
"num type2 fe histos SR mode = INT : 18",\
"num type2 fe histos alpha mode = INT : 16",\
"e1a and e1b pulse pairs = STRING : [4] 000",\
"e1a and e1b freq mode = STRING : [3] F0",\
"Randomize freq values = BOOL : y",\
"e2c beam on time (ms) = FLOAT : 500",\
"num cycles per supercycle = INT : 1",\
"NaVolt start = FLOAT : 120",\
"NaVolt stop = FLOAT : 150",\
"NaVolt inc = FLOAT : 1",\
"Laser start = FLOAT : -2",\
"Laser stop = FLOAT : 2",\
"Laser inc = FLOAT : -0.005",\
"e1c Camp Start = FLOAT : 8",\
"e1c Camp Stop = FLOAT : 8.15",\
"e1c Camp Inc = FLOAT : 0.001",\
"Field start = FLOAT : 10",\
"Field stop = FLOAT : 100",\
"Field inc = FLOAT : 5",\
"e2a pulse pairs = BOOL : n",\
"e2a 180 = BOOL : n",\
"e2a ubit1 action = STRING : [10] diff",\
"e2e num dwelltimes per freq = DWORD : 3",\
"e2e num RF dwelltimes per freq = DWORD : 0",\
"e2e num postRFbeamOn dwelltimes = DWORD : 4",\
"e1f const time between cycles = BOOL : n",\
"e1f min daq service (ms) = FLOAT : 90",\
"e1f enable alpha histos = BOOL : n",\
"",\
"[Output]",\
"PPG_loadfile = STRING : [128] 2e.ppg",\
"PPG template = STRING : [20] 2e_C2.ppg",\
"beam on time (ms) = FLOAT : 5831",\
"frequency stop (Hz) = FLOAT : 4500",\
"num frequency steps = DWORD : 11",\
"e1 number of incr (ninc) = DWORD : 21",\
"e1 number of cycles per scan = DWORD : 21",\
"dwell time (ms) = FLOAT : 119",\
"num dwell times = DWORD : 49",\
"ppg cycle time (ms) = FLOAT : 5831",\
"RF on time (ms) = FLOAT : 119",\
"RF off time (ms) = FLOAT : 0",\
"compiled file time (binary) = DWORD : 1565813145",\
"compiled file time = STRING : [32] Wed Aug 14 13:05:45 2019",\
"e1c Camp Path = STRING : [32] /biasV",\
"e1c Camp IfMod = STRING : [10] /tyCo/1",\
"e1c Camp Instr Type = STRING : [32] galil_rio47xxx",\
"e1n Epics device = STRING : [32] none",\
"PSM num IQ pairs (Niq) = INT : 1579",\
"PSM CIC interpoln rate (Ncic) = INT : 63",\
"PSM bandwidth (rad per sec) = FLOAT : 314.143",\
"PSM pulse width (msec) = FLOAT : 318.3264",\
"PSM num cycles iq pairs (Nc) = INT : 32",\
"PSM max cycles iq pairs (Ncmx) = INT : 32",\
"PSM linewidth mod param (A) = FLOAT : 0.1",\
"PSM amplitude mod param (alpha) = FLOAT : 5",\
"last seed (for random freqs) = DWORD : 730337032",\
"epics dual channel switch = BOOL : n",\
"e2a pulse pairs mode = DWORD : 9",\
"e1b Bg delay (dwelltimes) = DWORD : 0",\
"e1b RF delay (dwelltimes) = DWORD : 32",\
"e1b RF on (dwelltimes) = DWORD : 1000",\
"e1b RF off (dwelltimes) = DWORD : 0",\
"VME beam control = BOOL : y",\
"epics bnmr beam period = FLOAT : 0",\
"epics bnqr beam period = FLOAT : 0",\
"epics bnmr beam delay = FLOAT : 0",\
"epics bnqr beam delay = FLOAT : 0",\
"e2e num histo bins per ch = DWORD : 69",\
"e2e histo ntuple width (bins) = DWORD : 5",\
"e2e num post ntuple bins = DWORD : 4",\
"e2e ntuple depth (bins) = DWORD : 2",\
"e2e num beam off dwelltimes = DWORD : 0",\
"e2e scaler ntuple width (bins) = DWORD : 3",\
"e2f num beam on dwelltimes = DWORD : 17",\
"epics bnmr period = FLOAT : 0",\
"epics bnmr delay = FLOAT : 0",\
"epics bnqr period = FLOAT : 0",\
"epics bnqr delay = FLOAT : 0",\
"",\
"[Output/sis3820]",\
"LNE frequency (Hz) = INT : 0",\
"LNE preset = DWORD : 49",\
"Modid_A = DWORD : 0",\
"Modid_B = DWORD : 941621516",\
"",\
"[Output/PSM]",\
"device disabled = BOOL : y",\
"max cycles iq pairs (Ncmx) = INT : 32",\
"num IQ pairs (Niq) = INT[4] :",\
"[0] 1579",\
"[1] 1974",\
"[2] 1974",\
"[3] 1400",\
"CIC interpoln rate (Ncic) = INT[4] :",\
"[0] 63",\
"[1] 63",\
"[2] 63",\
"[3] 63",\
"bandwidth (rad per sec) = FLOAT[4] :",\
"[0] 314.143",\
"[1] 2512.825",\
"[2] 2512.825",\
"[3] 314.0367",\
"pulse width (msec) = FLOAT[4] :",\
"[0] 318.3264",\
"[1] 39.79584",\
"[2] 39.79584",\
"[3] 35.28",\
"num cycles iq pairs (Nc) = INT[4] :",\
"[0] 32",\
"[1] 16",\
"[2] 16",\
"[3] 4",\
"linewidth mod param (A) = FLOAT[4] :",\
"[0] 0.1",\
"[1] 0.1",\
"[2] 0.1",\
"[3] 0.39714",\
"amplitude mod param (alpha) = FLOAT[4] :",\
"[0] 5",\
"[1] 5",\
"[2] 5",\
"[3] 2.2",\
"",\
"[Output/sis3801]",\
"Modid_A = DWORD : 939646976",\
"Modid_B = DWORD : 0",\
"LNE preset = DWORD : 0",\
"",\
"[debug]",\
"debug = BOOL : y",\
"dppg = BOOL : n",\
"dcamp = BOOL : n",\
"dpsm = BOOL : y",\
"manual_ppg_start = BOOL : n",\
"dsis = BOOL : y",\
"hot_debug_level (-1=clr) = INT : 18",\
"hot threshold trip = INT : 2",\
"",\
NULL }

#define FIFO_ACQ_CLIENT_FLAGS_DEFINED

typedef struct {
  BOOL      mdarc;
  BOOL      rf_config;
  BOOL      mheader;
  BOOL      frontend;
  BOOL      fe_epics;
  BOOL      enable_client_check;
  INT       client_alarm;
  INT       epics_access_alarm;
  DWORD     error_code_bitpat;
  char      error_code_from[30];
  BOOL      logging_camp;
  BOOL      logging_epics;
  BOOL      got_event;
  INT       elog_alarm;
  INT       epicslog_alarm;
  INT       mh_on_hold;
  INT       last_run_campok;
  INT       last_run_epicsok;
  BOOL      prestart_failure;
  BOOL      start_abort;
  BOOL      hel_mismatch;
} FIFO_ACQ_CLIENT_FLAGS;

#define FIFO_ACQ_CLIENT_FLAGS_STR(_name) const char *_name[] = {\
"[.]",\
"mdarc = BOOL : y",\
"rf_config = BOOL : y",\
"mheader = BOOL : y",\
"frontend = BOOL : y",\
"fe_epics = BOOL : y",\
"enable client check = BOOL : y",\
"client alarm = INT : 0",\
"epics access alarm = INT : 0",\
"error code bitpat = DWORD : 0",\
"error code from = STRING : [30] mdarc",\
"logging camp = BOOL : y",\
"logging epics = BOOL : y",\
"got_event = BOOL : y",\
"elog alarm = INT : 0",\
"epicslog alarm = INT : 0",\
"mh_on_hold = INT : 0",\
"last_run_campok = INT : 0",\
"last_run_epicsok = INT : 0",\
"prestart_failure = BOOL : n",\
"start_abort = BOOL : n",\
"hel_mismatch = BOOL : n",\
"",\
NULL }

#define FIFO_ACQ_MODE_PARAMETERS_DEFINED

typedef struct {
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_10;
  struct {
    char      beam_mode;
    char      pulse_pairs[4];
    char      frequency_mode[3];
    float     background_delay__ms_;
    float     rf_delay__ms_;
    DWORD     number_of_rf_cycles;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_1a;
  struct {
    char      beam_mode;
    char      pulse_pairs[4];
    char      frequency_mode[3];
    float     background_delay__ms_;
    float     rf_delay__ms_;
    DWORD     number_of_rf_cycles;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    float     dwell_time__ms_;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_1b;
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    float     camp_start_scan;
    float     camp_stop_scan;
    float     camp_increment;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    char      camp_path[32];
    char      gpib_port_or_rs232_portname[10];
    char      instrument_type[32];
    char      camp_scan_path[80];
    char      scan_units[10];
    float     maximum;
    float     minimum;
    INT       integer_conversion_factor;
  } mode_1c;
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    float     start_field_scan__gauss_;
    float     stop_field_scan__gauss_;
    float     field_increment__gauss_;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_1e;
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    BOOL      const__time_between_cycles;
    BOOL      enable_alpha_histograms;
    float     daq_service_time_ms_;
  } mode_1f;
  struct {
    float     mcs_enable_gate__ms_;
    DWORD     number_of_prebeam_dwelltimes;
    DWORD     number_of_beam_on_dwelltimes;
    DWORD     number_of_beam_off_dwelltimes;
    DWORD     rfon_delay__dwelltimes_;
    INT       rfon_duration__dwelltimes_;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_1g;
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    BOOL      const__time_between_cycles;
    float     daq_service_time_ms_;
  } mode_1h;
  struct {
    float     mcs_enable_gate__ms_;
    float     camp_start_scan;
    float     camp_stop_scan;
    float     camp_increment;
    DWORD     number_of_prebeam_dwelltimes;
    DWORD     number_of_beam_on_dwelltimes;
    DWORD     number_of_beam_off_dwelltimes;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    char      camp_path[32];
    char      gpib_port_or_rs232_portname[10];
    char      instrument_type[32];
    char      camp_scan_path[80];
    char      scan_units[10];
    float     maximum;
    float     minimum;
    INT       integer_conversion_factor;
  } mode_1j;
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    float     start_nacell_scan__volts_;
    float     stop_nacell_scan__volts_;
    float     nacell_increment__volts_;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_1n;
  struct {
    float     mcs_enable_gate__ms_;
    DWORD     number_of_prebeam_dwelltimes;
    DWORD     number_of_beam_on_dwelltimes;
    DWORD     number_of_beam_off_dwelltimes;
    DWORD     rfon_delay__dwelltimes_;
    INT       rfon_duration__dwelltimes_;
    BOOL      enable_sample_reference_mode;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_20;
  struct {
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    DWORD     number_of_beam_precycles;
    BOOL      enable_180;
    BOOL      enable_pulse_pairs;
    char      bin_parameter[10];
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2a;
  struct {
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    DWORD     number_of_beam_precycles;
    INT       number_of_beam_on_dwelltimes;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2b;
  struct {
    char      beam_mode;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    float     prebeam_on_time__ms_;
    float     beam_on_time__ms_;
    float     rf_on_time__ms_;
    DWORD     number_of_frequency_slices;
    DWORD     freq_single_slice_width__hz_;
    float     freq_single_slice_int_delay_ms_;
    float     flip_180_delay__ms_;
    float     flip_360_delay__ms_;
    char      counting_mode;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2c;
  struct {
    char      beam_mode;
    char      pulse_pairs[4];
    char      frequency_mode[3];
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     background_delay__ms_;
    float     rf_delay__ms_;
    DWORD     number_of_rf_cycles;
    float     dwell_time__ms_;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2d;
  struct {
    float     rf_on_time__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    DWORD     num_dwelltimes_per_freq;
    DWORD     num_post_rfbeamon_dwelltimes;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    BOOL      dual_channel_mode;
  } mode_2e;
  struct {
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    DWORD     number_of_beam_off_dwelltimes;
    DWORD     num_post_rfbeamon_dwelltimes;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2f;
  struct {
    float     mcs_enable_gate__ms_;
    DWORD     number_of_prebeam_dwelltimes;
    DWORD     number_of_beam_on_dwelltimes;
    DWORD     number_of_beam_off_dwelltimes;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2g;
  struct {
    float     mcs_enable_gate__ms_;
    DWORD     number_of_prebeam_dwelltimes;
    DWORD     number_of_beam_on_dwelltimes;
    DWORD     number_of_beam_off_dwelltimes;
    DWORD     rfon_delay__dwelltimes_;
    DWORD     rfon_duration__dwelltimes_;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2h;
} FIFO_ACQ_MODE_PARAMETERS;

#define FIFO_ACQ_MODE_PARAMETERS_STR(_name) const char *_name[] = {\
"[Mode 10]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of bins = INT : 200",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 1a]",\
"Beam mode = CHAR : C",\
"Pulse pairs = STRING : [4] 000",\
"Frequency mode = STRING : [3] F0",\
"Background delay (ms) = FLOAT : 0",\
"RF delay (ms) = FLOAT : 32",\
"Number of RF cycles = DWORD : 1",\
"RF On Time (ms) = FLOAT : 75",\
"RF Off Time (ms) = FLOAT : 2000",\
"MCS Enable Delay (ms) = FLOAT : 4",\
"MCS Enable Gate (ms) = FLOAT : 10",\
"Frequency Scan Start (Hz) = DWORD : 127500",\
"Frequency Scan Stop (Hz) = DWORD : 130500",\
"Frequency Scan Increment (Hz) = DWORD : 25",\
"Randomize Freq Scan Increments = BOOL : n",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 1b]",\
"Beam mode = CHAR : C",\
"Pulse pairs = STRING : [4] 000",\
"Frequency mode = STRING : [3] F0",\
"Background delay (ms) = FLOAT : 0",\
"RF delay (ms) = FLOAT : 10",\
"Number of RF cycles = DWORD : 1",\
"RF On Time (ms) = FLOAT : 40",\
"RF Off Time (ms) = FLOAT : 20",\
"MCS Enable Delay (ms) = FLOAT : 0.01",\
"MCS Enable Gate (ms) = FLOAT : 10",\
"Dwell Time (ms) = FLOAT : 2",\
"Frequency Scan Start (Hz) = DWORD : 41220000",\
"Frequency Scan Stop (Hz) = DWORD : 41340000",\
"Frequency Scan Increment (Hz) = DWORD : 2000",\
"Randomize Freq Scan Increments = BOOL : n",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 1c]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of bins = INT : 100",\
"Camp Start scan = FLOAT : -1.205",\
"Camp Stop scan = FLOAT : -1.165",\
"Camp Increment = FLOAT : 0.0005",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 4000",\
"Camp path = STRING : [32] /biasV",\
"GPIB port or rs232 portname = STRING : [10] 0",\
"Instrument Type = STRING : [32] galil_rio47xxx",\
"Camp scan path = STRING : [80] /biasV/output1",\
"Scan units = STRING : [10] volts",\
"maximum = FLOAT : 10",\
"minimum = FLOAT : -10",\
"integer conversion factor = INT : 10000",\
"",\
"[Mode 1e]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of bins = INT : 100",\
"Start Field scan (Gauss) = FLOAT : 120",\
"Stop Field scan (Gauss) = FLOAT : 150",\
"Field Increment (Gauss) = FLOAT : 1",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 1f]",\
"MCS enable gate (ms) = FLOAT : 200",\
"Number of bins = INT : 100",\
"Frequency Scan Start (Hz) = DWORD : 232000",\
"Frequency Scan Stop (Hz) = DWORD : 234000",\
"Frequency Scan Increment (Hz) = DWORD : 100",\
"Randomize Freq Scan Increments = BOOL : n",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 4000",\
"Const. time between cycles = BOOL : y",\
"Enable Alpha histograms = BOOL : n",\
"DAQ service time(ms) = FLOAT : 150",\
"",\
"[Mode 1g]",\
"MCS enable gate (ms) = FLOAT : 6",\
"Number of Prebeam dwelltimes = DWORD : 5",\
"Number of Beam On dwelltimes = DWORD : 400",\
"Number of Beam Off dwelltimes = DWORD : 1200",\
"RFon Delay (dwelltimes) = DWORD : 50",\
"RFon Duration (dwelltimes) = INT : 10",\
"Frequency Scan Start (Hz) = DWORD : 41220000",\
"Frequency Scan Stop (Hz) = DWORD : 41340000",\
"Frequency Scan Increment (Hz) = DWORD : 2000",\
"Randomize Freq Scan Increments = BOOL : n",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 1h]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of bins = INT : 200",\
"Frequency Scan Start (Hz) = DWORD : 10000",\
"Frequency Scan Stop (Hz) = DWORD : 200000",\
"Frequency Scan Increment (Hz) = DWORD : 4000",\
"Randomize Freq Scan Increments = BOOL : n",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 4000",\
"Const. time between cycles = BOOL : y",\
"DAQ service time(ms) = FLOAT : 160",\
"",\
"[Mode 1j]",\
"MCS enable gate (ms) = FLOAT : 6",\
"Camp Start scan = FLOAT : 1.5",\
"Camp Stop scan = FLOAT : 8.5",\
"Camp Increment = FLOAT : 1",\
"Number of Prebeam dwelltimes = DWORD : 5",\
"Number of Beam On dwelltimes = DWORD : 400",\
"Number of Beam Off dwelltimes = DWORD : 1200",\
"Num cycles per scan incr = INT : 0",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 3000",\
"Camp path = STRING : [32] /biasV",\
"GPIB port or rs232 portname = STRING : [10] 0",\
"Instrument Type = STRING : [32] galil_rio47xxx",\
"Camp scan path = STRING : [80] /biasV/output1",\
"Scan units = STRING : [10] volts",\
"maximum = FLOAT : 10",\
"minimum = FLOAT : -10",\
"integer conversion factor = INT : 10000",\
"",\
"[Mode 1n]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of bins = INT : 100",\
"Start NaCell scan (Volts) = FLOAT : 120",\
"Stop NaCell scan (Volts) = FLOAT : 150",\
"NaCell Increment (Volts) = FLOAT : 1",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 20]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of Prebeam dwelltimes = DWORD : 10",\
"Number of Beam On dwelltimes = DWORD : 40",\
"Number of Beam Off dwelltimes = DWORD : 60",\
"RFon Delay (dwelltimes) = DWORD : 16",\
"RFon Duration (dwelltimes) = INT : 0",\
"Enable Sample Reference mode = BOOL : n",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 2a]",\
"RF On Time (ms) = FLOAT : 75",\
"RF Off Time (ms) = FLOAT : 2000",\
"MCS Enable Delay (ms) = FLOAT : 4",\
"MCS Enable Gate (ms) = FLOAT : 10",\
"Num RF On Delays (Dwelltimes) = DWORD : 5",\
"Beam Off time (ms) = FLOAT : 8000",\
"Number of Beam precycles = DWORD : 0",\
"Enable 180 = BOOL : n",\
"Enable Pulse Pairs = BOOL : n",\
"Bin parameter = STRING : [10] diff",\
"Frequency Scan Start (Hz) = DWORD : 127500",\
"Frequency Scan Stop (Hz) = DWORD : 130500",\
"Frequency Scan Increment (Hz) = DWORD : 25",\
"Randomize Freq Scan Increments = BOOL : n",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 4000",\
"",\
"[Mode 2b]",\
"RF On Time (ms) = FLOAT : 75",\
"RF Off Time (ms) = FLOAT : 2000",\
"MCS Enable Delay (ms) = FLOAT : 4",\
"MCS Enable Gate (ms) = FLOAT : 10",\
"Num RF on Delays (dwelltimes) = DWORD : 5",\
"Beam Off time (ms) = FLOAT : 8000",\
"Number of Beam precycles = DWORD : 0",\
"Number of Beam On dwelltimes = INT : 500",\
"Frequency Scan Start (Hz) = DWORD : 127500",\
"Frequency Scan Stop (Hz) = DWORD : 130500",\
"Frequency Scan Increment (Hz) = DWORD : 25",\
"Randomize Freq Scan Increments = BOOL : n",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 4000",\
"",\
"[Mode 2c]",\
"Beam mode = CHAR : C",\
"MCS Enable Delay (ms) = FLOAT : 4",\
"MCS Enable Gate (ms) = FLOAT : 10",\
"Num RF On Delays (Dwelltimes) = DWORD : 5",\
"Beam Off time (ms) = FLOAT : 8000",\
"Prebeam On time (ms) = FLOAT : 1",\
"Beam On time (ms) = FLOAT : 500",\
"RF On time (ms) = FLOAT : 10",\
"Number of frequency slices = DWORD : 1",\
"Freq single slice width (Hz) = DWORD : 10",\
"Freq single slice int delay(ms) = FLOAT : 10",\
"Flip 180 delay (ms) = FLOAT : 40",\
"Flip 360 delay (ms) = FLOAT : 10",\
"Counting mode = CHAR : 1",\
"Frequency Scan Start (Hz) = DWORD : 127500",\
"Frequency Scan Stop (Hz) = DWORD : 130500",\
"Frequency Scan Increment (Hz) = DWORD : 25",\
"Randomize Freq Scan Increments = BOOL : n",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 4000",\
"",\
"[Mode 2d]",\
"Beam mode = CHAR : C",\
"Pulse pairs = STRING : [4] 000",\
"Frequency mode = STRING : [3] F0",\
"RF On Time (ms) = FLOAT : 40",\
"RF Off Time (ms) = FLOAT : 20",\
"Background delay (ms) = FLOAT : 0",\
"RF delay (ms) = FLOAT : 10",\
"Number of RF cycles = DWORD : 1",\
"Dwell Time (ms) = FLOAT : 4",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 0",\
"",\
"[Mode 2e]",\
"RF On Time (ms) = FLOAT : 119",\
"Num RF on Delays (dwelltimes) = DWORD : 10",\
"Beam Off time (ms) = FLOAT : 0",\
"Num dwelltimes per freq = DWORD : 3",\
"Num post RFbeamOn dwelltimes = DWORD : 4",\
"Frequency Scan Start (Hz) = DWORD : 4000",\
"Frequency Scan Stop (Hz) = DWORD : 4500",\
"Frequency Scan Increment (Hz) = DWORD : 50",\
"Randomize Freq Scan Increments = BOOL : y",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 4000",\
"Dual channel mode = BOOL : n",\
"",\
"[Mode 2f]",\
"RF On Time (ms) = FLOAT : 40",\
"RF Off Time (ms) = FLOAT : 0.0005",\
"MCS Enable Delay (ms) = FLOAT : 0.001",\
"MCS enable gate (ms) = FLOAT : 39.009",\
"Num RF on Delays (dwelltimes) = DWORD : 5",\
"Beam Off time (ms) = FLOAT : 1000",\
"Number of Beam Off dwelltimes = DWORD : 20",\
"Num post RFbeamOn dwelltimes = DWORD : 10",\
"Frequency Scan Start (Hz) = DWORD : 4000000",\
"Frequency Scan Stop (Hz) = DWORD : 4100000",\
"Frequency Scan Increment (Hz) = DWORD : 50000",\
"Randomize Freq Scan Increments = BOOL : y",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 5000",\
"",\
"[Mode 2g]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of Prebeam dwelltimes = DWORD : 20",\
"Number of Beam On dwelltimes = DWORD : 100",\
"Number of Beam Off dwelltimes = DWORD : 1200",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 2h]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of Prebeam dwelltimes = DWORD : 100",\
"Number of Beam On dwelltimes = DWORD : 400",\
"Number of Beam Off dwelltimes = DWORD : 1200",\
"RFon Delay (dwelltimes) = DWORD : 0",\
"RFon Duration (dwelltimes) = DWORD : 0",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 4000",\
"",\
NULL }

#define FIFO_ACQ_CAMP_SWEEP_DEVICE_DEFINED

typedef struct {
  char      camp_path[32];
  char      gpib_port_or_rs232_portname[10];
  char      instrument_type[32];
  char      camp_scan_path[80];
  char      scan_units[10];
  float     maximum;
  float     minimum;
  char      camp_device_dependent_path[80];
  INT       integer_conversion_factor;
} FIFO_ACQ_CAMP_SWEEP_DEVICE;

#define FIFO_ACQ_CAMP_SWEEP_DEVICE_STR(_name) const char *_name[] = {\
"[.]",\
"Camp path = STRING : [32] /biasV",\
"GPIB port or rs232 portname = STRING : [10] 0",\
"Instrument Type = STRING : [32] galil_rio47xxx",\
"Camp scan path = STRING : [80] /biasV/output1",\
"Scan units = STRING : [10] volts",\
"maximum = FLOAT : 10",\
"minimum = FLOAT : -10",\
"Camp device dependent path = STRING : [80] ",\
"integer conversion factor = INT : 10000",\
"",\
NULL }

#define FIFO_ACQ_CAMP_SWEEP_DEVICES_DEFINED

typedef struct {
  char      camp_path[32];
  char      gpib_port_or_rs232_portname[10];
  char      instrument_type[32];
  char      camp_scan_path[80];
  char      scan_units[10];
  float     maximum;
  float     minimum;
  char      camp_device_dependent_path[80];
  INT       integer_conversion_factor;
} FIFO_ACQ_CAMP_SWEEP_DEVICES;

#define FIFO_ACQ_CAMP_SWEEP_DEVICES_STR(_name) const char *_name[] = {\
"[.]",\
"Camp path = STRING : [32] /biasV",\
"GPIB port or rs232 portname = STRING : [10] 0",\
"Instrument Type = STRING : [32] galil_rio47xxx",\
"Camp scan path = STRING : [80] /biasV/output1",\
"Scan units = STRING : [10] volts",\
"maximum = FLOAT : 10",\
"minimum = FLOAT : -10",\
"Camp device dependent path = STRING : [80] ",\
"integer conversion factor = INT : 10000",\
"",\
NULL }

#endif

#ifndef EXCL_CYCLE_SCALERS

#define CYCLE_SCALERS_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} CYCLE_SCALERS_COMMON;

#define CYCLE_SCALERS_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnqr.triumf.ca",\
"Frontend name = STRING : [32] feBNQR_VMIC",\
"Frontend file name = STRING : [256] /home/bnqr/online/bnqr_alpha/febnqr_vmic.c",\
"Status = STRING : [256] feBNQR_VMIC@lxbnqr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define CYCLE_SCALERS_NAMES_3801_DEFINED

typedef struct {
} CYCLE_SCALERS_NAMES_3801;

#define CYCLE_SCALERS_NAMES_3801_STR(_name) const char *_name[] = {\
"names_3801 = STRING[16] :",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
NULL }

#define CYCLE_SCALERS_NAMES_3820_DEFINED

typedef struct {
} CYCLE_SCALERS_NAMES_3820;

#define CYCLE_SCALERS_NAMES_3820_STR(_name) const char *_name[] = {\
"names_3820 = STRING[14] :",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam Back",\
"[32] Scaler_B%Neutral Beam Front",\
"[32] Scaler_B%Alpha 0",\
"[32] Scaler_B%Alpha 1",\
"[32] Scaler_B%Alpha 2",\
"[32] Scaler_B%Alpha 3",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
NULL }

#define CYCLE_SCALERS_SETTINGS_DEFINED

typedef struct {
  char      names[14][32];
} CYCLE_SCALERS_SETTINGS;

#define CYCLE_SCALERS_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[14] :",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam Back",\
"[32] Scaler_B%Neutral Beam Front",\
"[32] Scaler_B%Alpha 0",\
"[32] Scaler_B%Alpha 1",\
"[32] Scaler_B%Alpha 2",\
"[32] Scaler_B%Alpha 3",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"",\
NULL }

#endif

#ifndef EXCL_HISTO

#define HISTO_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} HISTO_COMMON;

#define HISTO_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 17",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnqr.triumf.ca",\
"Frontend name = STRING : [32] feBNQR_VMIC",\
"Frontend file name = STRING : [256] /home/bnqr/online/bnqr_alpha/febnqr_vmic.c",\
"Status = STRING : [256] feBNQR_VMIC@lxbnqr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define HISTO_SETTINGS_DEFINED

typedef struct {
  char      names[16][32];
} HISTO_SETTINGS;

#define HISTO_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[16] :",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"",\
NULL }

#endif

#ifndef EXCL_INFO_ODB

#define INFO_ODB_EVENT_DEFINED

typedef struct {
  DWORD     helicity_set;
  DWORD     current_cycle;
  DWORD     cancelled_cycle;
  DWORD     current_scan;
  double    ref_helup_thr;
  double    ref_heldown_thr;
  double    current_helup_thr;
  double    current_heldown_thr;
  double    prev_helup_thr;
  double    prev_heldown_thr;
  DWORD     rf_state;
  DWORD     fluor_monitor_counts;
  float     epicsdev_set_v_;
  float     epicsdev_read_v_;
  float     campdev_set;
  float     campdev_read;
  float     laser_power_v_;
  DWORD     last_failed_thr_test;
  DWORD     cycle_when_last_failed_thr;
  DWORD     last_good_hel;
  DWORD     ncycle_sk_tol;
  DWORD     helicity_read;
  INT       const1f_ppg_cb_cntr;
  INT       const1f_cb_cntr;
  float     const1f_percent;
  float     dummy;
  double    const1f_max_cb__ms_;
  double    const1f_av_cb__ms_;
  double    const1f_min_cb__ms_;
  double    dummy3;
  INT       wrong_hel_cntr;
  INT       microwave_set;
  INT       microwave_read;
  INT       rf_set_1f;
} INFO_ODB_EVENT;

#define INFO_ODB_EVENT_STR(_name) const char *_name[] = {\
"[.]",\
"helicity set = DWORD : 0",\
"current cycle = DWORD : 7",\
"cancelled cycle = DWORD : 1",\
"current scan = DWORD : 0",\
"Ref HelUp thr = DOUBLE : 0",\
"Ref HelDown thr = DOUBLE : 0",\
"Current HelUp thr = DOUBLE : 0",\
"Current HelDown thr = DOUBLE : 0",\
"Prev HelUp thr = DOUBLE : 0",\
"Prev HelDown thr = DOUBLE : 0",\
"RF state = DWORD : 0",\
"Fluor monitor counts = DWORD : 58309617",\
"EpicsDev Set(V) = FLOAT : 0",\
"EpicsDev Read(V) = FLOAT : 0",\
"Campdev set = FLOAT : 0",\
"Campdev read = FLOAT : 0",\
"Laser Power(V) = FLOAT : 0",\
"last failed thr test = DWORD : 0",\
"cycle when last failed thr = DWORD : 0",\
"last good hel = DWORD : 0",\
"ncycle sk tol = DWORD : 0",\
"helicity read = DWORD : 1",\
"Const1f ppg_cb_cntr = INT : 0",\
"Const1f cb_cntr = INT : 0",\
"Const1f perCent = FLOAT : 0",\
"dummy = FLOAT : 0",\
"Const1f max cb (ms) = DOUBLE : 0",\
"Const1f av cb (ms) = DOUBLE : 0",\
"Const1f min cb (ms) = DOUBLE : 0",\
"dummy3 = DOUBLE : 0",\
"Wrong hel cntr = INT : 0",\
"microwave set = INT : 0",\
"microwave read = INT : 0",\
"rf_set_1f = INT : 0",\
"",\
NULL }

#define INFO_ODB_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} INFO_ODB_COMMON;

#define INFO_ODB_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 10",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] ",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] FIXED",\
"Enabled = BOOL : y",\
"Read on = INT : 273",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnqr.triumf.ca",\
"Frontend name = STRING : [32] feBNQR_VMIC",\
"Frontend file name = STRING : [256] /home/bnqr/online/bnqr_alpha/febnqr_vmic.c",\
"Status = STRING : [256] feBNQR_VMIC@lxbnqr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_CAMP

#define CAMP_SETTINGS_DEFINED

typedef struct {
  INT       camp_ok;
  char      perl_script[80];
  INT       n_var_logged;
  char      var_path[16][132];
  INT       polling_interval[16];
  INT       cvar_event_period__s_;
} CAMP_SETTINGS;

#define CAMP_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"camp OK = INT : 0",\
"perl_script = STRING : [80] camp.pl",\
"n_var_logged = INT : 11",\
"var_path = STRING[16] :",\
"[132] /cryo_lift/set_position",\
"[132] /cryo_lift/read_position",\
"[132] /He_flow/read_flow",\
"[132] /He_flow/set_flow",\
"[132] /needle-valve/set_position",\
"[132] /needle-valve/read_position",\
"[132] /DAC/dac_set",\
"[132] /Sample/read_A",\
"[132] /Sample/read_B",\
"[132] /Sample/setpoint_1",\
"[132] /Sample/current_read_1",\
"[132] ",\
"[132] ",\
"[132] ",\
"[132] ",\
"[132] ",\
"polling_interval = INT[16] :",\
"[0] 10",\
"[1] 10",\
"[2] 10",\
"[3] 10",\
"[4] 10",\
"[5] 10",\
"[6] 10",\
"[7] 10",\
"[8] 10",\
"[9] 10",\
"[10] 10",\
"[11] 10",\
"[12] 10",\
"[13] 10",\
"[14] 10",\
"[15] 10",\
"cvar event period (s) = INT : 5",\
"",\
NULL }

#define CAMP_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} CAMP_COMMON;

#define CAMP_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 13",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 5000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isdaq01.triumf.ca",\
"Frontend name = STRING : [32] mheader",\
"Frontend file name = STRING : [256] src/mheaderBnmr.c",\
"Status = STRING : [256] mheader@isdaq01.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_HEADER

#define HEADER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} HEADER_COMMON;

#define HEADER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 14",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 17",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isdaq01.triumf.ca",\
"Frontend name = STRING : [32] mheader",\
"Frontend file name = STRING : [256] src/mheaderBnmr.c",\
"Status = STRING : [256] mheader@isdaq01.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_HDIAGNOSIS

#define HDIAGNOSIS_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} HDIAGNOSIS_COMMON;

#define HDIAGNOSIS_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 4",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 1",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnqr.triumf.ca",\
"Frontend name = STRING : [32] feBNQR_VMIC",\
"Frontend file name = STRING : [256] /home/bnqr/online/bnqr_alpha/febnqr_vmic.c",\
"Status = STRING : [256] feBNQR_VMIC@lxbnqr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define HDIAGNOSIS_SETTINGS_DEFINED

typedef struct {
  char      names[16][32];
} HDIAGNOSIS_SETTINGS;

#define HDIAGNOSIS_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[16] :",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"",\
NULL }

#endif

#ifndef EXCL_EPICSLOG

#define EPICSLOG_SETTINGS_DEFINED

typedef struct {
  INT       epics_ok;
  INT       n_epics_logged;
  char      epics_path[8][32];
  struct {
    INT       num_log_ioc;
    char      epics_log_ioc[8][32];
  } output;
  INT       evar_event_period__s_;
} EPICSLOG_SETTINGS;

#define EPICSLOG_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"epics OK = INT : 1",\
"n_epics_logged = INT : 5",\
"epics_path = STRING[8] :",\
"[32] BNQR:HVBIAS:RDVOL",\
"[32] ITE:BIAS:RDVOL",\
"[32] ILE2:BIAS15:RDVOL",\
"[32] ILE2A1:HH3:RDCUR",\
"[32] ILE2:LAS:RDPOWER",\
"[32] ",\
"[32] ",\
"[32] ",\
"",\
"[output]",\
"num_log_ioc = INT : 5",\
"epics_log_ioc = STRING[8] :",\
"[32] BNQR:HVBIAS:RDVOL",\
"[32] ITE:BIAS:RDVOL",\
"[32] ILE2:BIAS15:RDVOL",\
"[32] ILE2A1:HH3:RDCUR",\
"[32] ILE2:LAS:RDPOWER",\
"[32] ",\
"[32] ",\
"[32] ",\
"",\
"[.]",\
"evar event period (s) = INT : 5",\
"",\
NULL }

#define EPICSLOG_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} EPICSLOG_COMMON;

#define EPICSLOG_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 19",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 5000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isdaq01.triumf.ca",\
"Frontend name = STRING : [32] mheader",\
"Frontend file name = STRING : [256] src/mheaderBnmr.c",\
"Status = STRING : [256] mheader@isdaq01.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_EPICSALIVE

#define EPICSALIVE_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} EPICSALIVE_COMMON;

#define EPICSALIVE_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 12",\
"Trigger mask = WORD : 4096",\
"Buffer = STRING : [32] ",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 30000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnqr.triumf.ca",\
"Frontend name = STRING : [32] feBNQR_VMIC",\
"Frontend file name = STRING : [256] /home/bnqr/online/bnqr_alpha/febnqr_vmic.c",\
"Status = STRING : [256] feBNQR_VMIC@lxbnqr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_EPICS

#define EPICS_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} EPICS_COMMON;

#define EPICS_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 9",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 121",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isdaq01.triumf.ca",\
"Frontend name = STRING : [32] feEpics",\
"Frontend file name = STRING : [256] frontend.c",\
"Status = STRING : [256] Ok",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define EPICS_SETTINGS_DEFINED

typedef struct {
  struct {
    INT       epics;
  } channels;
  struct {
    struct {
      char      channel_name[20][32];
      BOOL      enabled;
    } epics;
  } devices;
  char      names[20][32];
  float     update_threshold_measured[20];
} EPICS_SETTINGS;

#define EPICS_SETTINGS_STR(_name) const char *_name[] = {\
"[Channels]",\
"Epics = INT : 24",\
"",\
"[Devices/Epics]",\
"Channel name = STRING[20] :",\
"[32] BNQR:VAR1",\
"[32] BNQR:VAR2",\
"[32] BNQR:VAR3",\
"[32] BNQR:VAR4",\
"[32] BNQR:VAR5",\
"[32] BNQR:VAR6",\
"[32] BNQR:VAR7",\
"[32] BNQR:VAR8",\
"[32] BNQR:VAR9",\
"[32] BNQR:VAR10",\
"[32] BNQR:VAR11",\
"[32] BNQR:VAR12",\
"[32] BNQR:VAR13",\
"[32] BNQR:VAR14",\
"[32] BNQR:VAR15",\
"[32] BNQR:VAR16",\
"[32] BNQR:VAR17",\
"[32] BNQR:VAR18",\
"[32] BNQR:VAR19",\
"[32] BNQR:VAR20",\
"Enabled = BOOL : y",\
"",\
"[.]",\
"Names = STRING[20] :",\
"[32] Pol Left",\
"[32] Pol Right",\
"[32] Pol. Cycle Sum",\
"[32] Pol. Cycle Asym",\
"[32] NeutBeam Back Sum",\
"[32] NeutBeam Front Sum",\
"[32] NeutBeam B+F Sum",\
"[32] NeutBeam Asym",\
"[32] not used",\
"[32] Cycle #",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"[32] Watchdog",\
"Update Threshold Measured = FLOAT[20] :",\
"[0] 2",\
"[1] 2",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 2",\
"[6] 2",\
"[7] 2",\
"[8] 2",\
"[9] 2",\
"[10] 2",\
"[11] 2",\
"[12] 2",\
"[13] 2",\
"[14] 2",\
"[15] 2",\
"[16] 2",\
"[17] 2",\
"[18] 2",\
"[19] 2",\
"",\
NULL }

#endif

#ifndef EXCL_HELICITY

#define HELICITY_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} HELICITY_COMMON;

#define HELICITY_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 11",\
"Trigger mask = WORD : 4096",\
"Buffer = STRING : [32] ",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 1",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] lxbnqr.triumf.ca",\
"Frontend name = STRING : [32] feBNQR_VMIC",\
"Frontend file name = STRING : [256] febnqr_vmic.c",\
"Status = STRING : [256] feBNQR_VMIC@lxbnqr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : y",\
"",\
NULL }

#define HELICITY_SETTINGS_DEFINED

typedef struct {
  char      names[6][32];
} HELICITY_SETTINGS;

#define HELICITY_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[6] :",\
"[32] PPG Set Value",\
"[32] Readback (Latched if DCM)",\
"[32] Enable Dual Channel Mode",\
"[32] Current Hel (DCM only)",\
"[32] max",\
"[32] min",\
"",\
NULL }

#endif

