/* setup_psm.c

This file included by febnmr.c if PSM is defined
$Log: setup_psm.c,v $
Revision 1.5  2015/02/23 23:49:21  suz
minor change to get rid of warning

Revision 1.4  2013/05/06 20:04:32  suz
add some debugging

Revision 1.3  2013/05/01 20:10:33  suz
add full path to freq filename

Revision 1.2  2013/04/26 01:30:27  suz
add a message about RF trip

Revision 1.1  2013/01/21 20:58:10  suz
initial VMIC version

Revision 1.10  2007/10/03 17:49:14  suz
remove setting scale fact to 0 from disable; instead use device disabled flag

Revision 1.9  2007/10/01 22:02:27  suz
disable_psm now also sets scale factor to 0

Revision 1.8  2007/09/26 18:33:40  suz
remove psmVMEReset from init_psm; takes a long time and was causing problems

Revision 1.7  2005/05/18 05:04:22  suz
finally get psmFreqWriteEndSweepMode name correct

Revision 1.6  2005/05/18 02:01:57  suz
correct bug

Revision 1.4  2005/05/16 19:35:05  suz
odb changed from setting values all zero to setting to a constant, needed for PSMII

Revision 1.3  2005/03/16 19:03:24  suz
add disable_psm

Revision 1.2  2005/03/08 18:04:05  suz
add writing scale factor

Revision 1.1  2004/12/13 22:36:51  suz
initial version


Initialize and setup the PSM

*/
#ifdef HAVE_PSM
INT psm_setone(MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq_Hz)
{
  /* set one frequency for use in Type 1
     Load freq into idle and strobe

     Note: init_freq_* should have been called before calling this routine.
       init_freq_* call init_psm.

       freq_Hz is unsigned long (DWORD)
 */
  INT status;

  if(dpsm)
    printf("\npsm_setone: now loading frequency %uHz into IDLE\n",freq_Hz);
  status = psmLoadIdleFreq_Hz (mvme, base_addr,freq_Hz);
  if(status == -1)
    {
      cm_msg(MERROR,"psm_setone","error writing idle frequency");
      return(status);
    }
  else
    {  
      if(dpsm)
	printf("psm_setone: read back Idle frequency as %dHz\n",status);
      if(status != freq_Hz)
	{
	  cm_msg(MERROR,"psm_setone","read back %dHz for Idle, but set value was %dHz",
		 status,freq_Hz);
	  return FE_ERR_HW;
	}
    }
   
  if(dpsm)
    printf("\nNow resetting mem addr pointer & loading IDLE  (psmFreqSweepMemAddrReset)\n");
  /* load the IDLE freq into DDS device */
  status = psmFreqSweepMemAddrReset(mvme, base_addr);

  if(dpsm)
    printf("\nNow writing internal strobe register\n");
  status = psmFreqSweepStrobe (mvme, base_addr);
  return status;
}




INT psm_loadFreqFile(MVME_INTERFACE *mvme, DWORD base_addr,  char* ppg_mode, BOOL random_flag)
{
  DWORD status;
  DWORD nfreq;
  char frqfile[64];
  DWORD first_frequency=-1;
  DWORD idle;
  
  idle=0;
  /* Load a frequency file into the PSM if random_flag is false.
     
     If random_flag is TRUE, everything else is done except the file is NOT loaded.
     Instead randomized frequency values will be loaded from an array by a call to psm_loadFreqData
     
     called by begin_of_run for Type 2 runs
     
     Note:  init_psm must be called before calling this routine; 
     this checks "all" profiles enabled, sets into quad mode */
  
  sprintf(frqfile,"%s/%s.psm",fs.input.cfg_path,ppg_mode);
        
  status = load_tuning_freq(mvme, base_addr); /* load tuning frequency if fREF profile is enabled */

  if(dpsm)
    printf("\npsm_loadFreqFile:Now loading frequency file %s (random flag is %d)\n",frqfile,random_flag);

  if(random_flag)
    {
      if(dpsm || dd[13] )printf("psm_loadFreqFile: calling LoadFreqDM_ptr with prandom_freq=%p \n",prandom_freq);
      status = psmLoadFreqDM_ptr(mvme, base_addr, prandom_freq,  fs.output.num_frequency_steps, &first_frequency);
    }
  else
    status = psmLoadFreqDM(mvme, base_addr,frqfile, &nfreq, &first_frequency);
  if(status == -1)
    {
      if(random_flag)
	cm_msg(MERROR,"psm_loadFreqFile","Error: frequency values at pointer %p are not loaded", prandom_freq);
      else
	cm_msg(MERROR,"psm_loadFreqFile","Error: frequency file %s is not loaded", frqfile);
      return(FE_ERR_HW);
    }
   
  
  printf("psm_loadFreqFile: successfully downloaded frequency values into PSM\n");
  if(dpsm)
    {
      // Reading back the frequency data
      psmReadFreqDM(mvme, base_addr, nfreq);  // read the data back
    }



  /* Jump to idle at end of sweep or stay at last value (for all enabled profiles) */
  if(fs.hardware.psm.freq_end_sweep_jump_to_idle)
    { 
      /* jump to idle; load initial freq value into idle? */
      if(fs.hardware.psm.freq_sweep_load_1st_val_in_idle)
	{
	  /*  first value will be loaded into Idle */
	  idle = psmLoadIdleFreq(mvme,base_addr,first_frequency); /* hex value to load */
	  if(idle == -1) 
	    {
	      cm_msg(MERROR,"psm_loadFreqFile","error writing idle frequency as 0x%x",
		     first_frequency);
	      return(FE_ERR_HW);
	    }
	  printf("psm_loadFreqFile:set Idle frequency to 0x%x\n",idle);	  
	} 
      else
	{
	  /* load fs.hardware.psm.idle_freq__hz_  into Idle */
	  idle = psmLoadIdleFreq_Hz (mvme,base_addr,fs.hardware.psm.idle_freq__hz_);
	  if(idle == -1) 	  
	    {
	      cm_msg(MERROR,"psm_loadFreqFile","error writing idle frequency as 0x%x",
		     fs.hardware.psm.idle_freq__hz_);
	      return(FE_ERR_HW);
	    }
	  printf("psm_loadFreqFile: set Idle frequency to %dHz\n",idle);
	}
    }  

  
  /*  PSM is now waiting for external strobe */
  printf("\nTEMPORARY writing internal strobe register\n");
  status = psmFreqSweepStrobe(mvme, base_addr);
  return SUCCESS;
}



/* ============================================================
 */

INT psm_loadIQFile( MVME_INTERFACE *mvme, DWORD base_addr, char* ppg_mode, char *profile)
{
  INT status;
  //BOOL load_idle;
  //DWORD nfreq;
  char IQfile[64];
  // DWORD first_frequency=-1;
  DWORD npairs,i,q,I,Q;
  DWORD first_i,first_q,last_i,last_q;
  const DWORD memadd=0;
  BOOL diff;

  INT Ncic,Niq;
  BOOL jump_to_idle_iq,load_first_val_in_idle;
  INT idle_i,idle_q;
  INT pro_index;

  /* Load an IQ pairs file into the PSM (file is 2's compliment data)
     
     called if quadrature mode is enabled

     Note:  init_psm must be called before calling this routine 
   */


  status = get_index(profile,&pro_index);

  if(status == -1)
  {
    cm_msg(MERROR,"psm_loadIQFile","illegal profile %s",profile);
    return -1;
  }
  printf(" psm_loadIQFile starting with profile \"%s\" and profile index %d\n",profile,pro_index);

 
  switch (pro_index)
    {
    case 0:
      Ncic =fs.output.psm.cic_interpoln_rate__ncic_[pro_index];
      Niq = fs.output.psm.num_iq_pairs__niq_[pro_index];

      jump_to_idle_iq =fs.hardware.psm.one_f.iq_modulation.jump_to_idle_iq;
      load_first_val_in_idle = fs.hardware.psm.one_f.iq_modulation.load_first_val_in_idle;
      idle_i =fs.hardware.psm.one_f.iq_modulation.idle_i__max_plus_or_minus_511_;
      idle_q =fs.hardware.psm.one_f.iq_modulation.idle_q__max_plus_or_minus_511_;
      break;
    case 3:
      Ncic =fs.output.psm.cic_interpoln_rate__ncic_[pro_index];
      Niq = fs.output.psm.num_iq_pairs__niq_[pro_index];

      jump_to_idle_iq =fs.hardware.psm.fref.iq_modulation.jump_to_idle_iq;
      load_first_val_in_idle = fs.hardware.psm.fref.iq_modulation.load_first_val_in_idle;
      idle_i =fs.hardware.psm.fref.iq_modulation.idle_i__max_plus_or_minus_511_;
      idle_q =fs.hardware.psm.fref.iq_modulation.idle_q__max_plus_or_minus_511_;
      break;
    default:
      cm_msg(MERROR," psm_loadIQFile","Error: profile %d i.e. \"%s\" is not supported",pro_index,profile);
      return(FE_ERR_HW);
    }
 
  sprintf(IQfile,"%s/%s_iq_%s.psm", fs.input.cfg_path,ppg_mode,profile);
  
  /*if(dpsm)*/
    printf("\nNow loading IQ file %s\n",IQfile);
  /* Note: PSM and PSMII use a different psmLoadIQ_DM_all (ifdefs) */
  /* status = psmLoadIQ_DM_all (mvme, base_addr, IQfile, &npairs,&first_i, &first_q); */
    status = psmLoadIQ_DM(mvme, base_addr, IQfile, profile,  &npairs,  &first_i, &first_q); 
  if(status != SUCCESS)
    {
      cm_msg(MERROR," psm_loadIQFile","Error: IQ file %s is not loaded", IQfile);
      return(FE_ERR_HW);
    }
  
  if(dpsm)printf(" psm_loadIQFile: loaded IQ file; first I,Q pair (2's comp) = %d,%d, no. pairs=%d\n",
	 first_i,first_q,npairs);
  
  if(npairs !=  Niq)
    {
      cm_msg(MINFO,"psm_loadIQFile",
	     "No. IQ pairs in file (%d) disagrees with odb value (%d); using file's value",
	     npairs,  Niq);      
    }

  if(dpsm)printf("psm_loadIQFile: setting IQ length register to %d \n", Niq);
  status = psmSetIQlen (mvme, base_addr, profile, Niq);
  if(status == -1)
    {
      cm_msg(MERROR,"psm_loadIQFile","Error setting IQ length register");
      return FE_ERR_HW;
    }


  /* read back to check

     check against the file; don't print the data; start reading at memadd=0  */
  diff=FALSE;
  status=psmReadIQ_DM(mvme, base_addr ,profile,&last_i,&last_q,memadd,npairs,&diff,FALSE, IQfile);

  if(status == -2)
    {
      cm_msg(MERROR,"psm_loadIQFile","failure checking IQ data memory against loadfile");
      return(FE_ERR_HW);
    }
  if(status == -3)
    {
      cm_msg(MERROR,"psm_loadIQFile",
	     "data length register contents does not agree with no. pairs in loadfile\n");
      return(FE_ERR_HW);
    }
  else if (status < 0)
    {
      cm_msg(MERROR,"psm_loadIQFile","failure return after reading back IQ data memory");
      return(FE_ERR_HW);
    }

  printf("psm_loadIQFile: successfully loaded I,Q pairs file %s\n", IQfile);

  /* Write the CIC value to the PSM */
  status = psmSetCIC_Rate(mvme, base_addr, profile, Ncic);
  if(status == -1)
    {
      cm_msg(MERROR,"psm_loadIQFile","Error setting CIC Rate register to %d ",
	     Ncic);
      return(FE_ERR_HW);
    }
 

  /* Check Jump to Idle for IQ pairs
     
  Jump to idle at end of sweep or stay at last value ?
  */
  if(jump_to_idle_iq)
    { 
      /* jump to idle; load initial IQ pair into idle? */
      if(load_first_val_in_idle)
	{
	  /*  first value will be loaded into Idle */
	  I=first_i;  /* these are in 2s complement */
	  Q=first_q;
	  printf("psm_loadIQFile: first pair will be loaded into Idle (I,Q=%d,%d in 2\'s comp)\n",I,Q); 
	  status = psmLoadIdleIQ(mvme, base_addr, "all",I,Q, TRUE); /* 2's compl. IQ pair to load */
	  if(status == -1)
	    {
	      cm_msg(MERROR,"psm_loadIQFile","error writing idle I,Q pair as %d,%d (in 2\'s compliment)",I,Q);
	      return(FE_ERR_HW);
	    }
	  else
	    {
	      psmReadIdleIQ(mvme, base_addr, "all",&i,&q, TRUE); /* read back in 2's comp */
	    }
	} /* end of loading first value into idle */
      else
	{
	  /* load fs.hardware.psm.iq_modulation  idle I,Q pair  into Idle */
	  I= idle_i;
	  Q= idle_q;
	  printf("psm_loadIQFile: supplied I,Q pair will be loaded into Idle (I,Q=%d,%d NOT in 2\'s comp)\n",I,Q); 
	  status = psmLoadIdleIQ(mvme, base_addr, "all",I,Q, FALSE); /* non 2's compl. IQ pair to load */
	  if(status == -1)
	    {
	      cm_msg(MERROR,"psm_loadIQFile","error writing supplied idle I,Q pair as %d,%d",I,Q);
	      return(FE_ERR_HW);
	    }
	  else
	    {
	      psmReadIdleIQ(mvme, base_addr,"all",&i,&q, FALSE);
	      if(dpsm)
		printf("psm_loadIQFile: read i,q Idle pair = %d,%d (NOT in 2\'s comp)\n",i,q);
	    }
	} 
      /* now check the value read back */
      if(i != I || q != Q)
	{
	  cm_msg(MERROR,"psm_loadIQFile",
		 "Error; read idle IQ pair as %d,%d, but wrote %d,%d",
		 i,q,I,Q);
	  return (FE_ERR_HW);
	}
      
    }
  return SUCCESS;
}



INT psm_loadIQIdle( MVME_INTERFACE *mvme, DWORD base_addr, char *profile)
{
  /* called in a loop on all profiles by load_iq_file

    writes the IQ idle pair, and sets length to zero 
       NOT called if loading an I,Q file
  */

  INT status;
  INT pro_index;
  INT idle_i, idle_q;

  if(get_index(profile,&pro_index) == -1)
    {
      cm_msg(MERROR,"psm_loadIQIdle","illegal profile %s",profile);
      return -1;
    }

  switch (pro_index)
    {
    case 0:
      idle_i = fs.hardware.psm.one_f.iq_modulation.idle_i__max_plus_or_minus_511_;
      idle_q = fs.hardware.psm.one_f.iq_modulation.idle_q__max_plus_or_minus_511_;
      break;
    case 3:
      idle_i = fs.hardware.psm.fref.iq_modulation.idle_i__max_plus_or_minus_511_;
      idle_q = fs.hardware.psm.fref.iq_modulation.idle_q__max_plus_or_minus_511_;
      break;
    default:
      cm_msg(MERROR," psm_loadIQIdle","Error: profile %d i.e. \"%s\" is not supported",pro_index,profile);
      return(FE_ERR_HW);
    }

  printf("psm_loadIQIdle called with profile %s\n",profile);
  status = psmLoadIdleIQ (mvme, base_addr, profile, 
			  idle_i, idle_q, 
			  FALSE); /* i,q, pair values NOT in 2s compliment */
  if(status == -1)
    {
      cm_msg(MERROR,"psm_loadIQIdle","error loading PSM IQ Idle pair (i=%d q=%d) for profile \"%s\"",
	     idle_i, 
	     idle_q,
	     profile);
      return(FE_ERR_HW);
    }
  /* psmloadIdleIQ reads values back to check */


  /* set the IQ length to zero for the Idle pair */
  status = psmSetIQlen (mvme, base_addr, profile, 1); /* reads back and checks value */
  if(status == -1)
    {
      cm_msg(MERROR,"psm_loadIQIdle","error while setting IQ length to 0");
      return FE_ERR_HW;
    }
  
  printf("psm_loadIQIdle: Idle IQ pair has been written as (%d,%d). Length set to 0\n",
	 idle_i, idle_q);
  
 
  return SUCCESS;
}

/* ==================================================*/
INT disable_psm(MVME_INTERFACE *mvme, DWORD base_addr)
{
  /* we will not be using the psm so disable it */
  INT status,size;
  char str[128];
  INT temp;

  /* Disable by setting all profiles into full sleep mode and single tone mode; 
     default value of this register is 0   */
  temp = FULL_SLEEP_MODE | SINGLE_TONE_MODE ;
  if(dpsm)printf("\ndisable_psm: setting all profiles into full sleep mode & single mode (writing 0x%2.2x)\n",temp);

  status = psmWriteProfileReg (mvme, base_addr, "all", PROFILE_REG2, temp);

  psmWriteGateControl (mvme, base_addr,"all",0); /* disable external gates; ppg sends spurious gates when loaded */
  printf("frontend_init: clearing PSM RF Trip \n");
  psmClearRFpowerTrip(mvme, base_addr);

  psmSetScaleFactor (mvme, base_addr,"all",0);  /* set amplitude to zero */

  if(dpsm)printf("\ndisable_psm: setting device disabled flag in ODB\n"); 
  sprintf(str,"/Equipment/FIFO_acq/frontend/output/PSM/device disabled");
  fs.output.psm.device_disabled=1; /* set flag */
  size=sizeof(BOOL);
  status = db_set_value(hDB,0, str, &fs.output.psm.device_disabled, size,1,TID_BOOL);
  if(status != SUCCESS)
    printf("disable_psm: error writing to  %s (%d) \n",str,status);

  return status; 
}
 

/* ==================================================
 */
INT init_psm( MVME_INTERFACE *mvme, DWORD base_addr)
{
  INT temp,status,size;
  char str[128];


  //dpsm = 1; // TEMP
  /* Remove hardware reset as it takes a long time  */
 if(dpsm) printf("init_psm: NOT resetting PSM  \n");
 //  if(dpsm) printf("init_psm: resetting PSM and waiting 1s \n");
 // psmVMEReset (mvme, base_addr);
 //  ss_sleep(5000); /* allow 5s for reset */
 /*  printf("Waiting 5s for reset...\n");*/


 printf("\n init_psm: starting\n");

#ifdef BNMR
 status = write_RF_trip_threshold(mvme, base_addr);
  if(status == -1)
    { 
      cm_msg(MERROR,"init_psm","error return from write_RF_trip_threshold");
      return FE_ERR_HW;
    } 
  ss_sleep(500);
#endif

  printf("init_psm: clearing PSM RF Trip \n");
  psmClearRFpowerTrip(mvme, base_addr);

  /* rf_config checked that at least one profiles is enabled */

  /* Initially, set all profiles into full sleep mode and single tone mode; 
     default value of this register is 0   */
  temp = FULL_SLEEP_MODE | SINGLE_TONE_MODE ;
  
  if(dpsm)printf("\nSetting all profiles into full sleep mode & single mode (writing 0x%2.2x)\n",temp);
  status = psmWriteProfileReg (mvme, base_addr, "all", PROFILE_REG2, temp);
  if(status == -1)
    {
      cm_msg(MERROR,"init_psm","error return from psmWriteProfileReg");
      return(status);
    }
  if(dpsm)printf("after writing 0x%2.2x to all profiles , read back  = 0x%x\n",temp,status);
  

  /* write freq end sweep mode (into Freq End Sweep Control Reg bit 4) */
  if(dpsm)printf("\nSetting freq EndSweepMode to %d  \n",
		 fs.hardware.psm.freq_end_sweep_jump_to_idle);
  status = psmFreqWriteEndSweepMode(mvme, base_addr, fs.hardware.psm.freq_end_sweep_jump_to_idle);
  if(status == -1)
    {
      cm_msg(MERROR,"init_psm","error return from psmFreqWriteEndSweepMode");
      return(status);
    }
  if(dpsm)printf("after writing freq end sweep mode, read back  = 0x%x\n",status);
    

  /* For each enabled profile, setup_psm sets up gate,scale factor, quad mode */
  if(setup_psm(mvme, base_addr) == -1) 
    return(-1);

  printf("\ninit_psm: clearing psm device disabled flag in ODB\n"); 

  sprintf(str,"/Equipment/FIFO_acq/frontend/output/PSM/device disabled");
  fs.output.psm.device_disabled=0; /* clear flag */
  size=sizeof(BOOL);
  status = db_set_value(hDB,0, str, &fs.output.psm.device_disabled, size,1,TID_BOOL);

  if(status != SUCCESS)
    printf("init_psm: error writing to  %s (%d) \n",str,status);


  return SUCCESS;
}
     
INT   setup_psm( MVME_INTERFACE *mvme, DWORD base_addr)
{
  /*  Parameters: VMIC mvme structure, PSM base

 For each enabled profile  (1f,3f,5f,fREF)
       set up any profile-dependent stuff read from ODB 
      
 
       Returns -1 for FAILURE
 */

  INT status,temp;
  BOOL quad_mode,jump_to_idle_iq, gate_control;
  INT bits, Nc, scale_factor;
  INT index;
  char profile[5];
  BOOL enabled;
  enabled=quad_mode=jump_to_idle_iq=bits=Nc=scale_factor=temp=0;
  gate_control=1;

  for (index=0; index <MAX_PROFILES; index++)
    {
      switch (index)
	{
	case 0:
	  if(fs.hardware.psm.one_f.profile_enabled)
	    {
	      enabled=1;
	      sprintf(profile, get_profile(index));
	      printf("setup_psm: setting up profile=%s (profile index %d)\n",profile,index);
	      
	      Nc = fs.output.psm.num_cycles_iq_pairs__nc_[index];

	      scale_factor = fs.hardware.psm.one_f.scale_factor__def_181_max_255_;
	      quad_mode = fs.hardware.psm.one_f.quadrature_modulation_mode;
	      jump_to_idle_iq = fs.hardware.psm.one_f.iq_modulation.jump_to_idle_iq;
              gate_control = fs.hardware.psm.one_f.gate_control;
 
	    }
	  break;

	case 3:
	  if(fs.hardware.psm.fref.profile_enabled)
	    {
	      enabled=1;
	      sprintf(profile, get_profile(index));
	      printf("setup_psm: setting up profile=%s (profile index %d)\n",profile,index);
	      Nc = fs.output.psm.num_cycles_iq_pairs__nc_[index];

	      scale_factor = fs.hardware.psm.fref.scale_factor__def_181_max_255_;
	      quad_mode = fs.hardware.psm.fref.quadrature_modulation_mode;
	      jump_to_idle_iq = fs.hardware.psm.fref.iq_modulation.jump_to_idle_iq;
              gate_control = fs.hardware.psm.one_f.gate_control;
	    }
	  break;
	    
	default:
	  enabled=0;
	} // switch


      if(dpsm)
	{
	  printf("\nProfile index %d :\n",index);
	  printf("Nc = %d\n",Nc);
	  printf("Scale Factor = %d\n",scale_factor);
	  printf("Quad Mode = %d\n",quad_mode);
	  printf("Jump to Idle IQ = %d\n",jump_to_idle_iq);
	  printf("Gate control =  %d\n\n",gate_control );
	}


      if(enabled)
	{
	  switch (gate_control)
	    {
	    case FP_GATE_DISABLED:
	      cm_msg(MERROR,"setup_psm","WARNING: Front Panel Gate is Disabled. Use only for testing");
	      printf("setup_psm: WARNING: Front panel gate is disabled\n");
	      break;
	    case  GATE_NORMAL_MODE:
	      printf("setup_psm: Front panel gate is enabled (default mode)\n");
	      break;
	    case  GATE_PULSE_INVERTED:
	      cm_msg(MERROR,"setup_psm","WARNING: Front Panel Gate Pulse Inverted. Use only for testing");
	      printf("setup_psm: WARNING:Front panel gate pulse inverted\n");
	      break;
 	    case   INTERNAL_GATE:
	      cm_msg(MERROR,"setup_psm","WARNING: Front Panel Gate Pulse Ignored and Gates are ALWAYS ON. Use only for testing");
	      printf("setup_psm: WARNING Front panel gate pulse ignored. Gate are ALWAYS ON\n");
	      break;
            default:
	      cm_msg(MERROR,"setup_psm","WARNING: Illegal gate parameter. Using default mode");
	      printf("setup_psm:  Illegal gate parameter. Using default mode\n");
              gate_control=1;
	    } // switch
 
	  /* write gate control for this profile */
	
	  
	  if(dpsm)printf("\nNow writing gate control = %d for profile \"%s\"   \n",gate_control,profile);
	  status = psmWriteGateControl (mvme, base_addr, profile,  gate_control);
	  if(status == -1)
	    {
	      cm_msg(MERROR,"setup_psm","error return from psmWriteGateControl for profile \"%s\" ",profile);
	      return(status);
	    }
	  
	  
	  if(dpsm)printf("after writing gate control to \"%s\", read back  = 0x%x\n",profile,status);
	  
	  
	  /* Write the scale factor  */
	  
	  if (scale_factor < 0 || scale_factor > 255)
	    {
	      cm_msg(MERROR,"setup_psm","Illegal scale factor (%d) for profile \"%s\"; it should be between 0 and 255",
		     scale_factor,profile);
	      return -1;
	    }
	  
	  status = psmSetScaleFactor(mvme, base_addr, profile, scale_factor);
	  if(status == -1)
	    {
	      cm_msg(MERROR,"setup_psm","error return from psmSetScaleFactor");
	      return status;
	    }
	  
	  if(!quad_mode)
	    {
	      if(dpsm)printf("\nsetup_psm: setting profile \"%s\" into Single Tone mode and out of full sleep mode",profile);
	      temp = SINGLE_TONE_MODE ;
	    }
	  else
	    { /* quadrature mode */
	      
	      /* for quadrature  mode  */
	      status = psmWriteProfileBufFactor(mvme, base_addr, profile, Nc);
	      if(status !=  Nc) 
		{
		  cm_msg(MERROR,"setup_psm","error writing Nc=%d to Buffer Factor profile \"%s\" register\n ",
			 Nc,profile);
		  return -1; 
		}
	      
	      /* write IQ end sweep mode (into  End Sweep Control Reg bits 0-3) */
	      if(dpsm)printf("\nSetting IQ EndSweepMode to %d  \n",
			     jump_to_idle_iq);
	      status = psmIQWriteEndSweepMode(mvme, base_addr,profile,jump_to_idle_iq); /* write bit */
	      if(status == -1)
		{
		  cm_msg(MERROR,"setup_psm","error return from psmIQWriteEndSweepMode");
		  return(status);
		}
	      if(dpsm)printf("setup_psm:after writing IQ end sweep mode (%d) to end sweep reg, read back  = 0x%x\n",
			     jump_to_idle_iq, status);
	      
	      
	      
	      if(dpsm)printf("\nsetup_psm: now setting profile \"%s\" into Quadrature Modulation mode",profile);	
	      if(dpsm)printf(" & out of full sleep mode by writing 0x%2.2x or %d to Control Reg 2\n",temp,temp);      
	      temp = 0;
	    }
	  
	  status = psmWriteProfileReg (mvme, base_addr, profile, PROFILE_REG2, temp);
	  if(status == -1)
	    {
	      cm_msg(MERROR,"setup_psm","error return from psmWriteProfileReg");
	      return status;
	    }

	} /* end of this profile enabled */
    } /* end of for */
  return(SUCCESS);
}


/*---------------------------------------------------------*/
INT load_iq_file(MVME_INTERFACE *mvme, DWORD base_addr)
/*---------------------------------------------------------*/
  /* load the IQ pair file(s) for quadrature mode (also loads Idle)
     for enabled profile(s)
  */
{
#ifdef HAVE_PSM
  INT n,status; 
  char my_profile[5];
  BOOL first,enabled;
  BOOL quad,load_iq_file;

  first=1;

  for (n=0; n< MAX_PROFILES; n++) /* cycle through profiles */ 
    {
      enabled=quad=load_iq_file=0;
    
      switch (n)
	{
	case 0:    
	  if(fs.hardware.psm.one_f.profile_enabled)
	    {
	      sprintf(my_profile, get_profile(n));
	      enabled=1;
	      quad = fs.hardware.psm.one_f.quadrature_modulation_mode;
	      load_iq_file =  fs.hardware.psm.one_f.iq_modulation.load_i_q_pairs_file;
	    }
	  break;

	case 3:    
	  if(fs.hardware.psm.fref.profile_enabled)
	    {
	      sprintf(my_profile, get_profile(n));
	      enabled=1;
	      quad = fs.hardware.psm.fref.quadrature_modulation_mode;
	      load_iq_file =  fs.hardware.psm.fref.iq_modulation.load_i_q_pairs_file;
	    }
	  break;
	default:
	  enabled=0;
	} // switch

      if(enabled)
	{
	  if(quad)
	    {
	      if( load_iq_file)
		{
		  if(first) /* check this once only */
		    {
		      if ( strncmp(fs.input.experiment_name,"20",2) == 0)
			sprintf(ppg_mode,"00"); /* filename is 00 not 20; change ppg_mode (global) */
		      first=0;
		    }

		  /* load the IQ pair file for quadrature mode (also loads Idle)*/
		  printf("load_iq_file: Quadrature mode enabled for profile \"%s\", loading I,Q pairs file\n",
			 my_profile);
		  status = psm_loadIQFile(mvme, base_addr, ppg_mode, my_profile);
		  if(status != SUCCESS)
		    {
		      printf("load_iq_file: error loading PSM IQ pairs file for profile \"%s\" and ppg_mode %s\n",
			     my_profile,ppg_mode);
		      return(status);
		    }
		  else
		    printf("load_iq_file: successfully loaded PSM IQ pairs file for profile \"%s\" and ppg_mode %s\n",
			   my_profile,ppg_mode);
		}
	      else
		{  /* don't load i,q, pairs file; load the i,q idle values */
		  printf("load_iq_file calling psm_loadIQIdle with my_profile %s\n",my_profile) ; 
		  status = psm_loadIQIdle(mvme, base_addr, my_profile);
		  
		  if(status != SUCCESS)
		    {
		      printf("load_iq_file: error 3 loading PSM IQ Idle for profile \"%s\" and ppg_mode %s\n",
			     my_profile,ppg_mode);
		      return(status);
		    }
		  else
		    printf("load_iq_file: successfully loaded PSM IQ Idle for profile \"%s\" and ppg_mode %s\n",
			   my_profile,ppg_mode);
		}
	    } /* end of quad modulation mode */
	  else
	    {  /* single tone mode
                  Hubert says load IQ idle with either I or Q set to 0x3FF (511)*/
	      printf("load_iq_file: adding Hubert's fix...\n");
	      printf("psm_loadIQIdle called with profile %s\n",my_profile);
	      status = psmLoadIdleIQ (mvme, base_addr, my_profile, 
				      511 , 0, FALSE); /* i,q, pair values NOT in 2s compliment */
	      if(status == -1)
		{
		  cm_msg(MERROR,"psm_loadIQIdle",
			 "error loading PSM IQ Idle pair (i=511 q=0) for profile \"%s\" (single tone mode)",
			 my_profile);
		  return(FE_ERR_HW);
		}
	    }
	} /* end of this profile enabled */
    } /* end of for loop */
#endif
  return SUCCESS;
}

/*---------------------------------------------------------*/
INT load_tuning_freq(MVME_INTERFACE *mvme, DWORD base_addr)
/*---------------------------------------------------------*/
{
#ifdef HAVE_PSM
  INT status;

  /* If fREF profile is enabled ... */
  if(fs.hardware.psm.fref.profile_enabled)
    {
      /* Load "reference tuning frequency (Hz)" into fREF  */
      printf("\n load_tuning_freq: loading PSM frequency %dHz into fref reference tuning reg\n",
	     fs.hardware.psm.fref_tuning_freq__hz_);
      status = psmWrite_fREF_freq_Hz (mvme, base_addr,  fs.hardware.psm.fref_tuning_freq__hz_);
      if(status != -1) 
	printf("load_tuning_freq:read back PSM fref tuning frequency as %dHz\n",status);
      else
	{
	  cm_msg(MERROR,"load_tuning_freq","error writing PSM fref tuning frequency of %dHz",
		 fs.hardware.psm.fref_tuning_freq__hz_);
	  return(FE_ERR_HW);
	}
    }
  else
    printf("load_tuning_freq: tuning frequency NOT loaded as profile fREF is disabled\n");
  printf("load_tuning_freq returns success\n");
#endif
  return SUCCESS;

}


#ifdef GONE
INT get_index(char *p_profile_code, INT *pindex)
{
  /* get the array index depending on profile (1f,3f,5f or fREF) 
   */
  p_profile_code[0]=tolower(p_profile_code[0]);
  /* printf("get_index: starting with my_profile_code=%s\n",my_profile_code); */
  switch (p_profile_code[0])
    {
    case '1':
      *pindex=0;
      break;
    case '3':
      *pindex=1;
      break;
    case '5':
      *pindex=2;
      break;            
    case 'f':
      *pindex=3;
      break;
    default:
      printf("Register profile (%s) must be one of 1f,3f,5f or fREF\n",p_profile_code);
      return (-1);
    }
  return(SUCCESS);
}
#endif

char *get_profile(INT n)
{
  char *profile_name[]={"1f","3f","5f","fREF","unknown"};
 
  return (n<0 || n>MAX_PROFILES) ? profile_name[4] : profile_name[n];
}


INT update_Ncmx(MVME_INTERFACE *mvme, DWORD base_addr)
{
  INT status,Ncmx;
  char str[128];
  /* read Ncmx from the PSM and write it into the odb for rf_config to use */
  Ncmx = psmReadMaxBufFactor(mvme, base_addr );
  sprintf(str,"/Equipment/FIFO_acq/frontend/output/psm/max cycles iq pairs (Ncmx)");
  if(dpsm)
    { 
      printf(" init_freq_module: read Ncmx from PSM as %d\n",Ncmx);
      printf("    now writing it to \"%s\" for rf_config\n",str);
    }
  status = db_set_value(hDB,0,str,&Ncmx,sizeof(Ncmx),1,TID_INT);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR," init_freq_module","Error writing Ncmx=%d to ODB key\"%s\"; cannot setup PSM (%d)",
	     Ncmx, str,status);
      return FE_ERR_HW;
    }
  return SUCCESS;
}


#ifdef BNMR
INT  write_RF_trip_threshold( MVME_INTERFACE *mvme, DWORD base_addr)
{
  float ftemp;
  INT status;
  
  /* Write the RF Trip Threshold  */
  ftemp = fs.hardware.rf_trip_threshold__0_5v_;
  if(dpsm)printf("write_RF_trip_threshold:starting with ftemp=%f\n",ftemp);
  if (ftemp < 0 || ftemp > 5)
    {
      cm_msg(MERROR,"write_RF_trip_threshold","Illegal RF Trip threshold (%.2f); it should be between 0 and 5",ftemp);
      return -1;
    }
  
  
  status = psmWriteRFpowerTripThresh(mvme, base_addr,ftemp);
  if(status == -1)
    {
      cm_msg(MERROR,"write_RF_trip_threshold","error return from psmWriteRfpowerTripThresh");
      return status;
    }
  else 
    if(dpsm)printf("psm_write_RF_trip_threshold: read back 0x%2.2x (%d)\n",status,status); /* data */
  
  return SUCCESS;
}
#endif /* BNMR */


#endif  /* ifdef HAVE_PSM */
  
