/* bnmr_epics.h

   $Log: bnmr_epics.h,v $
   Revision 1.1  2013/01/21 20:58:10  suz
   initial VMIC version

   Revision 1.12  2008/05/12 22:45:54  suz
   add gbl_helswitch prototype

   Revision 1.11  2008/04/14 22:29:20  suz
   add Laser Power variable

   Revision 1.10  2005/07/19 21:29:26  suz
   support Epics HV bias for POL

   Revision 1.9  2005/04/21 20:03:09  suz
   add EPICS single/dual channel switch

   Revision 1.8  2005/02/03 22:45:33  suz
   add epics channels for helicity switch ( & helOff for testing)

   Revision 1.7  2005/01/26 21:55:04  suz
   add channel id for direct Epics channel access for helicity read

   Revision 1.6  2005/01/25 23:31:03  suz
   direct channel access for helicity read added

   Revision 1.5  2004/03/31 00:06:20  suz
   generalize epics device name (Xx)

   Revision 1.4  2003/12/01 19:51:23  suz
   add support for reconnect

   Revision 1.3  2003/06/26 21:31:33  suz
   add Epics_units so setpoint no longer always in Volts

   Revision 1.2  2003/06/24 23:23:00  suz
   add Epics Field scan

   Revision 1.1  2003/05/01 21:27:46  suz
   initial bnmr version; identical to bnmr1 Rev1.2

   Revision 1.2  2003/01/09 21:13:03  suz
   add EpicsCheck

   Revision 1.1  2002/06/05 00:48:32  suz
   defines structure for globals


 */

/* Define structure EPICS_PARAMS

     contains globals needed by Main program  AND  
     epics routines in bnmr_epics.c  

*/

#ifndef _bnmr_epics_
#define _bnmr_epics_

typedef struct {
  BOOL    NaCell_flag; /* True if we scanning NaCell voltage (i.e. ppg_mode=1n) */
  BOOL    Laser_flag;
  BOOL    Field_flag;
  /* EPICS channel IDs */
  int XxRchid; /* ID for read;  set to -1 if no access */
  int XxWchid; /* ID for write;  set to -1 if no access */
  /* other globals */
  float   Epics_stability; /* value used for stability check (histo process) & NaSet */
  float   Epics_val;      /* Na cell voltage to request */
  float   Epics_inc;
  DWORD   Epics_ninc;     /* number of increments */
  float   Epics_read;     /* value read back */
  INT     Epics_bad_flag; /* counter increments if cannot set NaCell voltage */
  float   Epics_last_value;  /* value as read and accepted by histo_process */
  float   Epics_start;
  float   Epics_last_offset; /* last good offset value */
  char    Epics_units[6]; /* set point units */
} EPICS_PARAMS;

#ifdef POL
int Rchid_Bias; /* channel access Bias read */
char Rbias[32];
#else  /* BNMR/QR only: direct epics channel access to read helicity  */
int Rchid_helOn; /* channel access helicity read */
int Rchid_helSwitch, Rchid_helOff; /* these are used in helicity_init */
int Rchid_LaserPower; /* read laser power for threshold check */
INT gbl_helswitch; /* 1 if switch is local, 0 if DAQ */
/* Epics dual channel switching params */
int Rchid_BnmrPeriod;
int Rchid_BnqrPeriod;
int Rchid_BnmrDelay;
int Rchid_BnqrDelay;
int Rchid_EnableSwitching;
#endif

/* function prototypes */

/* Functions for Epics devices used for scanning
   i.e. NaCell, Laser, Field
*/

INT EpicsReconnect(EPICS_PARAMS *p_epics_params);
void EpicsDisconnect(EPICS_PARAMS *p_epics_params);
INT  EpicsWatchdog  (EPICS_PARAMS *pEpics_params);
INT  EpicsIncr      (EPICS_PARAMS *pEpics_params);
INT  EpicsGetIds   (EPICS_PARAMS *pEpics_params);
INT  EpicsInit     (BOOL flip,  EPICS_PARAMS *pEpics_params);
INT  EpicsNewScan (EPICS_PARAMS *pEpics_params);
INT  EpicsRead      (float* value, EPICS_PARAMS *pEpics_params);
INT  EpicsCheck( EPICS_PARAMS *p_epics_params);

/* Function prototypes in bnmr_epics.c */
 INT   NaCell_init(BOOL flip, EPICS_PARAMS *p_epics_params) ;
 INT   Laser_init(BOOL flip, EPICS_PARAMS *p_epics_params);
 INT   Field_init(BOOL flip, EPICS_PARAMS *p_epics_params);
 float convert_to_gauss(float amps);
 float convert_to_amps(float gauss);
 float NaCell_get_typical_offset (  float set_point );
 INT   Helicity_init(BOOL flip, INT *Rchid, char * Rname);
 INT   LaserPower_init(INT *Rchid, char * Rname);
 INT   read_Epics_chan(INT chan_id,  float * pvalue);
 INT   ChannelReconnect(char *Rname);
 void  ChannelDisconnect(INT *Rchid);
 INT   ChannelWatchdog(char *Rname, INT *Rchid); /* watchdog on single channel */


#endif // _bnmr_epics_





















