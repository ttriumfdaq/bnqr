#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
# $Log: kill_frontend.pl,v $
# Revision 1.4  2016/07/06 22:17:56  suz
# call check-host-vmic
#
# Revision 1.3  2016/07/06 21:33:44  suz
# remove vmic_online
#
# Revision 1.2  2014/11/24 21:38:40  suz
# now uses environment variable DAQ_HOST for DAQ hostname
#
# Revision 1.1  2013/04/10 19:44:55  suz
# new to cvs; supports bnmr and bnqr
#
# VMIC version
#
use strict;

# invoke this script with cmd 
#                         Parameter
# perl kill_frontend.pl        0 = kill task first in odb then kill program if still running
#                              1 = just kill the program if running

sub kill_frontend_task(); # prototype
#
#
our $MIDAS_EXPT_NAME = $ENV{'MIDAS_EXPT_NAME'};
our $HOME = $ENV{'HOME'};
our $ONLINE = $ENV{'ONLINE'};
our $EXPERIM_DIR = $ENV{'EXPERIM_DIR'};
print "kill_frontend: environment variables: selected MIDAS_EXPT_NAME  $MIDAS_EXPT_NAME, HOME is $HOME and ONLINE is $ONLINE \n";

#  check-host-vmic looks for host $DAQ_HOST username  "bn[qm]r"
my $reply = `$EXPERIM_DIR/bin/check-host-vmic `;
print "$reply\n";
if ( "$?" != "0" ) {
    print "kill_frontend: failure from check-host-vmic\n";
    exit;
}

#

my ($param) = @ARGV;  # need the brackets
print "input parameter =$param\n";

unless ($param)
{  # skip this if param=1 (i.e. called from kill-all where mserver has been shut down)

    my $done=0;
    my $count=0;
    while ( ! $done && $count < 3 )
    {
	$count++;  # if feBN[QM]R_VMIC can't be killed avoid infinite loop
	$reply = `odb -c scl -e $MIDAS_EXPT_NAME` ;
#$reply = $reply . "feBNQR_VMIC      lxbnqr\n";
	print "kill_frontend: active Midas clients are \n $reply \n" ;
	if ($reply =~/(feBN[QM]R_VMIC\d?)/i)
	{  
	    print "found $1 \n";
	    print "shutting down $1\n";
	    `odb -c 'sh $1' `;  
	}
	else
	{ 
	    $done=1;
	}
    }
    sleep 2;
} # end of unless $param
kill_frontend_task();
exit;


sub kill_frontend_task()
{
# make sure task is stopped
    my $task;
    $task="fe".$MIDAS_EXPT_NAME."_vmic.exe";
    print "checking for task $task\n";
    `killall $task`;
    return;
}


