#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#   
use strict;
sub check_epics_string($$);

# invoke this script with cmd e.g.
#                  include_path          experiment   parameter (optional)   
# at_start_run.pl  /home/bn[mq]r/$ONLINE/perl     bn[mq]r    0,1,2 
#
#  Optional input parameter
#  0 default 
#  1 (for redo camp from custom page) to stop statistics being zeroed
#  2 (redo epics from custom page)
#
#
# previously, /programs/execute on start run was
#    /home/bnmr/online/bn[mq]r/bin/at_start_run.csh > /home/midas/musr/log/bn[mq]r/at_start.txt
# now it is
#   /home/bn[mq]r/$ONLINE/bn[mq]r/bin/at_start_run.pl  /home/bn[mq]r/$ONLINE/perl bnmr
#
#   message file is /data1/bn[mq]r/log/at_start_run_pl.txt
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);

our $ONLINE=$ENV{'ONLINE'}; # ONLINE environment variable 
#######################################################################
#  parameters needed by init2_check.pl (required code common to perlscripts)
#
# input parameters :
our ($inc_dir, $expt, $my_parameter ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "at_start_run" ; # same as filename
our $outfile = "at_start_run_pl.txt"; # path will be added by file open
our $parameter_msg = "include path , experiment, [ optional parameter 0=all 1=CAMP only 2=EPICS only ]";
our $nparam = 2;  # no. of required input parameters
our $beamline = $expt; # beamline is not supplied. Same as $expt for bnm/qr, pol
############################################################################

# paths and constants
our $client_flags_path = "/equipment/fifo_acq/client flags/";
our $camp_settings = "/equipment/camp/settings/";
our $camp_mdarc = "/equipment/FIFO_acq/mdarc/camp/";
our $epicslog_settings = "/equipment/epicslog/settings/";
our $epicslog_alarm = "/Alarms/Alarms/epicslog/";
our $elog_alarm = "/Alarms/Alarms/elog/";
our $input = "/Equipment/FIFO_acq/frontend/Input";
our $ppgmode;

our $MAX_CAMP_LOGGED=16;
our $MAX_EPICS_LOGGED=8;   # maximum number of epics paths allowed 
# local variables:
my ($transition, $run_state, $path, $key, $status);
my $logfile_path ="/home/midas/musr/log/vmic_$expt/";

#--------------------------------------------------------
$|=1; # flush output buffers



# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}

$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of this script running
#   no. of input parameters is correct
#   opens output file:
#
require "$inc_dir/init_check.pl";
# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt \n";
unless ($expt =~ /bn[qm]r/i  )
{
    print FOUT "FAILURE: experiment $expt not supported\n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  experiment $expt not supported " ) ;
    unless ($status) { print FOUT "$name: Failure return after msg \n"; } 
    die  "FAILURE:  experiment $expt not supported\n";
}
unless (check_online($inc_dir) == $SUCCESS) #check ONLINE and input param include dir are consistent
{
    print_3($name, "Error from check_online. Environment variable \"online\" does not agree with supplied include_dir $inc_dir",$MERROR,$CONT);
    die "FAILURE: Environment variable \"online\" does not agree with supplied include_dir $inc_dir\n";
}
print "len=$len\n";
if($len == 2) 
{
    $my_parameter=0; # check both CAMP and EPICS
}
print_2 ($name, "optional input parameter is $my_parameter",$CONT);

# NOTE: msg [type] [user] <msg> - compose user message
#
#  odb msg 1 -> error msg  (in black/red)  
#  odb msg 2 -> info msg   (in black/white)
#  odb -e  $MIDAS_EXPT_NAME -c 'msg script "this is yet another test" ' -> [script]this is another test
#  odb -e  $MIDAS_EXPT_NAME -c 'msg 1 script  "this is yet another test" ' ->  [ODBEdit1] this is yet another test
#  odb -e  $MIDAS_EXPT_NAME -c 'msg 2 script  "this is yet another test" ' ->  [ODBEdit1] [odbedit.c:2424:scr] this is yet another test
#

# clear elog (camp log) alarm
($status) = odb_cmd ( "set","$client_flags_path","elog alarm" ,0) ;
unless($status)
{ 
    print_3 ($name, "Failure from odb_cmd (set); Error setting $client_flags_path elog alarm to 0",$MERROR,$DIE);
  
}

# clear epicslog alarm
($status) = odb_cmd ( "set","$client_flags_path","epicslog alarm" ,0) ;
unless($status)
{ 
    print_3 ($name, "Failure from odb_cmd (set); Error setting $client_flags_path/epicslog alarm",$MERROR,$DIE);
  
}

if ($my_parameter==1)   # redo CAMP only
{  
    ($status) = odb_cmd ( "set","$camp_settings","camp ok" ,4) ;
    unless($status)
    { 
	print_3 ($name, "Failure from odb_cmd (set); Error setting $camp_settings/camp ok to 4",$MERROR,$DIE);
	
    }

    print_2($name, "(at start) sets camp ok to 4 (redoing CAMP only)", $CONT);
}

elsif ($my_parameter==2)   # redo EPICS only
{ 
    ($status) = odb_cmd ( "set","$epicslog_settings","epics ok" ,4) ;
    unless($status) { 
	print_3 ($name, "Failure from odb_cmd (set); Error setting $epicslog_settings/epics ok to 4",$MERROR,$DIE); }

    print_2($name, "(at start) sets epics ok to 4 (redoing EPICS only)", $CONT);
}

else #redoing EPICS and CAMP
{ 
    $my_parameter=0; # set to zero for do_camp.csh
    ($status) = odb_cmd ( "set","$camp_settings","camp ok" ,2) ;
    unless($status) {
	print_3 ($name, "Failure from odb_cmd (set); Error setting $camp_settings/camp ok to 2",$MERROR,$DIE); }
    print_2($name, "(at start) sets camp ok to 2 (redoing CAMP)", $CONT);

    ($status) = odb_cmd ( "set","$epicslog_settings","epics ok" ,2) ;
    unless($status) { 
	print_3 ($name, "Failure from odb_cmd (set); Error setting $epicslog_settings/epics ok to 2",$MERROR,$DIE); }	
    print_2($name, "(at start) sets epics ok to 2 (redoing EPICS)", $CONT);
}

if ($my_parameter == 0 || $my_parameter == 2)
{
# 
#  Check epics logged variables
#
    print_3($name, "calling check_epics",$MINFO,$CONT);
    
    $status =check_epics();
    if($status != $SUCCESS)
    { 
	print_3($name, "error return from check_epics",$MERROR,$CONT); 
	($status) = odb_cmd ( "set","$epicslog_settings","epics ok" ,0) ; # FAILURE
	unless($status) { 
	    print_3 ($name, "Failure from odb_cmd (set); Error setting $epicslog_settings/epics ok to 0",$MERROR,$DIE); }
    }
    else
    { 
	print_3($name, "successful return from check_epics",$MINFO,$CONT); 
	($status) = odb_cmd ( "set","$epicslog_settings","epics ok" ,1) ; # SUCCESS
	unless($status) { 
	    print_3 ($name, "Failure from odb_cmd (set); Error setting $epicslog_settings/epics ok to 1",$MERROR,$DIE); }
    }
}
else
{
    print_3($name, "NOT calling check_epics (start parameter=$my_parameter)",$MINFO,$CONT);
}

if ($my_parameter == 2) { exit; }  # do not check CAMP

# 
#     Check CAMP
#
print_2($name, "now checking camp: param=$my_parameter",$CONT);
# $MAX_CAMP_LOGGED

# reset the following in case of failure from this script

# do not reset; due to possible delay in completion of script, leave last run's value
#($status) = odb_cmd ( "set","$camp_settings","n_var_logged" ,0) ;
#unless($status) {
#    print_3 ($name, "Failure from odb_cmd (set); Error setting $camp_settings n_var_logged to 0",$MERROR,$DIE); }

print "calling check_camp\n";
$status =check_camp();
print FOUT ($name,"status after check_camp=$status \n done",$CONT);
exit;








sub check_epics()
{

    my $stat;
    my $i;
    my $retry=5;
    my $n_epics_logged;
    my($my_path,$good_path,$icount);
    my $alarm_msg;
    my ($ans,$tmp,$length);
    my @paths;
    my $name="check_epics";
    
    $i=$retry; # midas message may get mixed up with epics variable string
    while ($i) # retry if this happens
    {
	
	($status,$path,$key) = odb_cmd ( "ls","$epicslog_settings","n_epics_logged" ) ;
	unless($status) {
	    print_3 ($name, "Failure from odb_cmd (ls); Error reading $epicslog_settings n_epics_logged",$MERROR,$DIE);	}
        
	print_2($name, "calling check_epics_string.pl with parameters:n_epics_logged and $ANSWER", $CONT);
	$stat = check_epics_string("n_epics_logged",$ANSWER);
        if($stat) { last; }
        $i--;
    }

    if($i<= 0)
    {
	print_3($name, "cannot read number of epics variables correctly after $retry tries",$MINFO,$CONT);
	return $FAILURE;
    }

    $n_epics_logged = get_int ( $key); #also uses global $ANSWER
    if ($n_epics_logged < 0  ){  $n_epics_logged = 0; }
    elsif ($n_epics_logged >  $MAX_EPICS_LOGGED ) { $n_epics_logged =  $MAX_EPICS_LOGGED; }

    print_3($name, "number of epics paths to be logged=$n_epics_logged",$MINFO, $CONT);
    if ($n_epics_logged == 0)
    {
	print_2($name, "writing n_epics_logged=0 to output/num_log_ioc and clearing paths",$CONT);
	
	($status) = odb_cmd ( "set","$epicslog_settings","output/num_log_ioc" ,0) ;
	unless($status) { 
	    print_3 ($name, "Failure from odb_cmd (set); Error setting $epicslog_settings/output/num_log_ioc to 0",$MERROR,$DIE); }
	$icount=0;
	$good_path="";
        # clear the epics paths
	while($icount <  $MAX_EPICS_LOGGED)
	{
	    $my_path="output/epics_log_ioc[$icount]";
	    ($status) = odb_cmd ( "set","$epicslog_settings","$my_path" ,$good_path) ;
	    unless($status){ 
		print_3 ($name, "Failure from odb_cmd (set); Error setting $epicslog_settings/$my_path to 0",$MERROR,$DIE); }
	    $icount++; 
	} # end of while 
	return $SUCCESS;
    } # end of n_epics_logged=0;

# read epics paths

    $i=$retry; # midas message may get mixed up with epics variable string
    while ($i) # retry if this happens
    {
	($status,$path,$key) = odb_cmd ( "ls","$epicslog_settings","epics_path" ) ;
	unless($status) { 
	    print_3 ($name, "Failure from odb_cmd (ls); Error reading $epicslog_settings/epics_path",$MERROR,$DIE);}
        
	print_2($name, "calling check_epics_string.pl with parameters:$key and $ANSWER", $CONT);
	$stat = check_epics_string($key,$ANSWER);
        if($stat) { last; }
        $i--;
    }
    if ($i <= 0) {
	print_3($name, "cannot read epics paths correctly after $retry tries; stripping off offending msg",$MINFO,$CONT); }
 

    ($ans,$tmp)=fix_answer($key); # strip off any extra message after the value we want
    
    print "ans=$ans\n";
    @paths=split / /,$ans;
    $length=$#paths;
    print_2($name, "n_epics_logged=$n_epics_logged; length=$length",$CONT);
    $tmp=$length+1;
    if($n_epics_logged > $tmp)
    {
	print_3($name,"Not enough epics paths listed (expect $n_epics_logged). Using the $tmp paths listed)",$MINFO,$CONT);
	$n_epics_logged=$tmp; # use the number of paths we found
    }

    $i=$icount=0;
    while ($i < $n_epics_logged)
    {
	$my_path = shift @paths;
        print_2($name,"Working on path $my_path",$CONT);
	system("caget" ,"$my_path");
        $status=$?;
        print "status after caget $my_path is $?\n";
        unless($status)
	{
	    $good_path = $my_path;
	    $my_path="output/epics_log_ioc[$icount]";
	    ($status) = odb_cmd ( "set","$epicslog_settings","$my_path" ,$good_path) ;
	    unless($status) { 
		print_3 ($name, "Failure from odb_cmd (set); Error setting $epicslog_settings/$my_path to $good_path",$MERROR,$DIE); }
	    $icount++; # counts good paths
	}
	else
	{
	    print_3 ($name, "Epics device $my_path is not responding. Skipping this device",$MINFO,$CONT); 
	    # Send Alarm Message
	    $alarm_msg="cannot access $my_path; will not be logged";
            $tmp=length($alarm_msg);
            # Limit length of Alarm Message in odb  or get record size mismatch error
            if($tmp > 79 ) 
	    {    
		$alarm_msg = "Invalid EPICS parameters. Click on Messages button for details";
		print_2($name,"Alarm Message has had to be truncated. Number of characters now is $tmp",$CONT);
	    }
	    
	    ($status) = odb_cmd ( "set","$epicslog_alarm","Alarm Message" ,$alarm_msg) ;
	    unless($status) {
		print_3 ($name, "Failure from odb_cmd (set); Error setting $epicslog_alarm/Alarm Message to $alarm_msg",$MERROR,$CONT);	}
	    # trigger odb alarm /equipment/fifo_acq/client flags/epicslog alarm 
	    print_2($name, "setting epicslog alarm to 1 ",$CONT);
	    ($status) = odb_cmd ( "set","$client_flags_path","epicslog alarm" ,1) ;
	    unless($status) { 
		print_3 ($name, "Failure from odb_cmd (set); Error setting $client_flags_path epicslog alarm to 1",$MERROR,$CONT);	}
	} # end of bad epics path 
	$i++;  # counts paths for while loop
    } # end of while
    
    print_3($name, "writing icount=$icount to output/num_log_ioc ",$MINFO,$CONT);
    
    ($status) = odb_cmd ( "set","$epicslog_settings","output/num_log_ioc" ,$icount) ;
    unless($status) { 
	print_3 ($name, "Failure from odb_cmd (set); Error setting $epicslog_settings/output/num_log_ioc to 0",$MERROR,$DIE);}

# clear empty paths to avoid confusion
    $good_path = "";
    while($icount < $MAX_EPICS_LOGGED )
    {
	$my_path="output/epics_log_ioc[$icount]";
	($status) = odb_cmd ( "set","$epicslog_settings","$my_path" ,$good_path) ;
	unless($status) {
	    print_3 ($name, "Failure from odb_cmd (set); Error clearing $epicslog_settings/$my_path ",$MERROR,$DIE); }	
	$icount++;  # counter
    }
    return $SUCCESS;
}


sub check_epics_string($$)
{
# input parameters :
    my $param = shift;
    my $string = shift;
    my $name="check_epics_string";

# check on odb response from the odb -c command
# parameter should be parameter read (unless "none" for -v flag)
# string should be string read back by odb -c

print_2 ($name,"check_epics_string starting with param=\"$param\" and string=\"$string\"",$CONT);

$_=$string;
if (/(\d\d:\d\d:\d\d)/)
{ 
    print_3 ($name, "Info - found date string $1 (Midas Message mixed in)",$MINFO,$CONT);
    print_2 ($name,"string was: $string",$CONT);
    return $FAILURE;
}

if($param eq "none") 
{ 
   print "check_epics_string: success\n";
   return $SUCCESS;
}

if (/^$param/)
{
    s/$param//;
}
else
{ 
    print_2($name, "Could not find expected leading parameter \"$param\"",$CONT);
    return $FAILURE;
}

if($param eq "n_epics_logged")
{
    s/ //g;

    if (/(\D+)/)  # should only be digit(s)
    { 
	print_2($name, "Found unexpected non-digit(s) \"$1\" in string \"$_\" ",$CONT);
	return $FAILURE;
    }
    unless (/(\d+)/)  # should be some digit(s)
    { 
	print_2($name,  "Found no digit(s) \"$1\" in string \"$_\" ",$CONT);
	return $FAILURE;
    }
}

elsif($param eq "epics_path")
{
#epics paths are all upper case
    if (/([a-z])/)
    {
		print_2($name,  "Found unexpected lower case in epics_path \"$1\"",$CONT);
		return $FAILURE;
    }
}

print "check_epics_string: returning success\n";
return $SUCCESS;
}


sub check_camp_string($$)
{
# input parameters :
    my $param = shift;
    my $string = shift;
    my $message;
    my $name="check_camp_string";

    print_2 ($name,"starting with param=\"$param\" and string \"$string\" ",$CONT);
    
    $_=$string;
    if (/(\d\d:\d\d:\d\d)/)
    { 
	print_3 ($name, "Info - found date string $1 (Midas Message mixed in)",$MINFO,$CONT);
	print_2 ($name,"string was: $string",$CONT);
	return $FAILURE;
    }
    
    if($param eq "none") 
    { 
	print "$name: success\n";
	return $SUCCESS;
    }
    
    if (/^$param/)
    {
	s/$param//;
    }
    if($param eq "camp hostname")
    {
#camp hostname has no digits
	if (/(\d)/)
	{
	    print_2($name,  "Found unexpected digit in camp hostname \"$1\"",$CONT);
	    return $FAILURE;
	}
    }
    print "$name: success\n";
    return $SUCCESS;
}


sub check_camp()
{
# input parameters :
    my $stat;
    my ($i,$item,$var);
    my $retry=5;
    my $n_epics_logged;
    my($my_path,$good_path,$icount);
    my $alarm_msg;
    my ($ans,$tmp,$length);
    my @paths;
    my ($camp_hostname,$env_camp_hostname);
    my $skip_item;
    my @automatic_headers = ("temperature variable","RF power variable", "field variable");
    my $ppgmode;
    
    my $name="check_camp";


    $status = read_and_check("ls",$camp_mdarc,"camp hostname");
    #print "read_and_check for camp hostname returns status $status\n";
    ($camp_hostname, $tmp)= get_string ("camp hostname"); #also uses global $ANSWER
    print "camp_hostname=\"$camp_hostname\" msg=$tmp ANSWER=$ANSWER\n";
    
    $env_camp_hostname=$ENV{"CAMP_HOST"};
    if ($env_camp_hostname ne $camp_hostname)
    {
	print_3($name,"odb camp hostname $camp_hostname does not agree with environment variable CAMP_HOST $env_camp_hostname",$DIE);
	#print "odb camp hostname $camp_hostname does not agree with environment variable CAMP_HOST $env_camp_hostname\n";
    }

# Check automatic header variables for existence, polling, and logging.
    print "$name   array is @automatic_headers \n";
    foreach $item (@automatic_headers )
    { 
	
	print_2($name, "\n  Working on automatic header item \"$item\"",$CONT);
	$skip_item=0;


	$status = read_and_check("ls -v","/alias/Auto Headers&/","$item");

	if($status == $SUCCESS)
	{

	    ($var,$tmp)=get_string ( $item); #also uses global $ANSWER
	    # strips off any extra message after the value we want
	
	    my $len = length($var);
	    print "Automatic header item is \"$var\" of length $len\n";
	    if($len > 0)
	    {	
		print "Setting up automatic header $item: \"$var\"\n";
		$ANSWER=`/home/musrdaq/musr/camp/linux/camp_cmd "setupLoggedVar $var"`;  
		$status=$?;
		if($status != 0)
		{  # error response
		    chomp $ANSWER;  # strip trailing linefeed
		    print_3($name, "Invalid autoheader item $var: $ANSWER",$MERROR,$CONT);
		    
		    # Send Alarm Message
		    $alarm_msg = "Invalid autoheader item \"$var\": $ANSWER";
		    $tmp=length($alarm_msg);
		    # Limit length of Alarm Message in odb  or get record size mismatch error
		    if($tmp > 79 ) 
		    {    
			$alarm_msg="Non-fatal warning from CAMP auto header. Click on Messages button for details";
			print_2($name,"Alarm Message has had to be truncated. Number of characters now is $tmp",$CONT);
		    }
		    
		    ($status) = odb_cmd ( "set","$elog_alarm","Alarm Message" ,$alarm_msg) ;
		    unless($status) {
			print_3 ($name, "Failure from odb_cmd (set); Error setting $elog_alarm/Alarm Message to $alarm_msg",$MERROR,$CONT);	}
		    # trigger odb alarm /equipment/fifo_acq/client flags/elog alarm 
		    print_2($name, "setting elog alarm to 1 ",$CONT);
		    ($status) = odb_cmd ( "set","$client_flags_path","elog alarm" ,1) ;
		    unless($status) { 
			print_3 ($name, "Failure from odb_cmd (set); Error setting $client_flags_path elog alarm to 1",$MERROR,$CONT);	}
		} # end of error response
		else {
		    print_2($name,"camp \"$var\" setupLoggedVar OK $status $ANSWER",$CONT); }
	    }
	    else {
		print_2($name,"automatic variable \"$item\"is empty",$CONT);}
	}
	else {
	print "** read_and_check: $ANSWER\n";
	    print_2($name,"automatic variable \"$item\" does not exist in odb",$CONT);}  
    
	print "\n";
    } # next item
    
    
    $status = read_and_check("ls -v",$input,"experiment name");
    print "read_and_check returns status $status\n";
    ($ppgmode, $tmp)= get_string ("experiment name" ); #also uses global $ANSWER
    print_3 ($name, "after get_string, ppgmode=$ppgmode msg='$tmp' ANSWER=$ANSWER",$MINFO,$CONT);
    
    unless( $ppgmode=~/^1[0a-z]/ || $ppgmode=~/^2[0a-z]/ )
    { print_3 ($name,"illegal ppg mode: $ppgmode ",$MERROR,$DIE); }
    
#
    my $filename="$logfile_path/do_camp.tmp";  # temporary file written by do_camp.csh
    print_2 ($name,"do_camp.csh writes temporary output file $filename",$CONT);

    if (-e $filename) 
    { 
	system ("rm", "-f","$filename");  
	print_2 ($name,"removed temporary file  $filename",$CONT);
    }
    print_2($name,"calling do_camp.csh with ppgmode=$ppgmode ",$CONT);
#
    $ANSWER=system("/home/$expt/$ONLINE/$expt/bin/do_camp.csh","$my_parameter","$ppgmode", "$logfile_path" );
    $status=$?;
    if($status != 0)
    {  # error response
	chomp $ANSWER;  # strip trailing linefeed
	print_3($name, "Error return from do_camp.csh",$MERROR,$CONT);
        $status=$FAILURE;
    }
    $status =  $SUCCESS;

    if (-e $filename) 
    {
	$/=undef;
	open FTEMP, "$filename" ;
	my $file_contents = <FTEMP>;
	print FOUT "\n contents of file $filename :\n";
	print FOUT $file_contents;
	print FOUT "\n";
	close FTEMP;
    }
    else { print FOUT "\n file $filename not found\n"; }
    return $status;
}
    

sub read_and_check($$$$)
{
    my $i;
    my $retry=5;
    my $my_path;
    my($status,$path,$key);

    my $cmd=shift;          # must be "ls" or "ls -v"
    my $input_path=shift;   # path without key name
    my $key_name=shift;     # key name
    
    my $param;        
    my $name="read_and_check";
    unless ( $cmd=~/^ls/)
    {
	die "$name : illegal command $cmd; expect ls\n";
    }
    unless ($cmd=~/ -v/ ) { $param = $key_name; }
    else { $param = "none"; }
    
    print "$name : command is $cmd; param is \"$param\"\n";
    $i=$retry; # midas message may get mixed up with epics variable string
    while ($i) # retry if this happens
    {
	$my_path=$input_path . $key_name;
	($status,$path,$key) = odb_cmd ( $cmd,$input_path,$key_name) ;
	unless($status) {
	    print_2 ($name, "Failure from odb_cmd $cmd; Error reading $my_path",$CONT);
	    print_3($name, "$ANSWER",$MINFO,$CONT);
	    return $FAILURE;
}
        
	print_2($name, "calling check_camp_string.pl with parameters:$param and $ANSWER", $CONT);
	$status = check_camp_string($param,$ANSWER); 
	if($status) { last; }
	$i--;
    }
    if($i<= 0)
    {
	print_3($name, "cannot read $key_name correctly after $retry tries",$MINFO,$CONT);
	return ($FAILURE);
    }
    
    return $SUCCESS;
}
