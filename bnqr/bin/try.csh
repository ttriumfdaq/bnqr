#!/bin/csh

echo  $HOST > ~/temp
grep --silent $DAQ_HOST ~/temp
if ( "$?" != "0" )  then 
   echo "This is being run as DAQ_HOST ($DAQ_HOST)  (NOT $HOST)"
   ssh $DAQ_HOST  /home/midas/packages/elog/elog -h $HOST  -p $port -l $logbook -u $MIDAS_EXPT_NAME $pwd  -a author=$author -a Type="Automatic Elog" -a System="PPG$exp_type" -a Subject="$sample"  -m $fin
endif
rm ~/temp




