#!/bin/csh

#   $Log: at_end_run.csh,v $
#   Revision 1.2  2018/06/12 23:55:59  suz
#   clear_elog_alarm.csh run from bin/
#
#   Revision 1.1  2013/02/01 02:13:44  suz
#   initial version for vmic daq
#
#   Revision 1.3  2008/05/08 20:06:52  suz
#   clear the elog and epics alarms at end-of-run using script
#
#   Revision 1.2  2008/05/01 22:30:35  suz
#   make common for bnmr/bnqr
#
echo "expt: $MIDAS_EXPT_NAME"

cd ~/$ONLINE/$MIDAS_EXPT_NAME/bin

# Just run this script for me and keep a log for the errors
./elog_every_run.csh >& elog.log

# run script 
./clear_elog_alarm.csh
exit
