#!/bin/csh 
#   (-x for debug)
#
#   $Log: check_epics.csh,v $
#   Revision 1.2  2008/04/17 21:07:38  suz
#   clear unused array elements in output to avoid confusion
#
#   Revision 1.1  2008/04/14 22:26:17  suz
#   initial version
#
#
# This script is executed at start of every run to check list of epics paths to log
# in /Equipment/EpicsLog/Settings
# and to write the list of valid paths to  /Equipment/EpicsLog/Settings/output
#
# Called by at_start_run.csh
#
set max = 8
set num = `odb -e $MIDAS_EXPT_NAME -c "ls '/Equipment/EpicsLog/Settings/n_epics_logged'"` 
#echo "number of epics variables to be logged: $num "
#echo "$num[1] $num[2]"
set n_epics_logged=$num[2] # strip off "n_epics_logged" from the number
echo "number of epics paths to be logged = $n_epics_logged"

if ($n_epics_logged < 0  ) then
   set n_epics_logged = ( 0 )
else if ($n_epics_logged > $max || ) then
   set n_epics_logged = ( $max )
endif

#odb -e $MIDAS_EXPT_NAME -c "msg   'check_epics' 'num epics paths to be logged is $n_epics_logged (input)'"


if ($n_epics_logged == 0) then
   echo "writing n_epics_logged=0 to output/num_log_ioc and clearing paths"
   odb -e $MIDAS_EXPT_NAME -c "set '/Equipment/EpicsLog/Settings/output/num_log_ioc' 0"

   set icount = ( 0 )
   set good_path = ""
   while($icount < $max)
       odb -e $MIDAS_EXPT_NAME -c "set '/Equipment/EpicsLog/Settings/output/epics_log_ioc[$icount]' $good_path"
       @ icount ++  # count the valid paths
      end
   exit    
endif

set paths =  `odb -e $MIDAS_EXPT_NAME -c "ls '/Equipment/EpicsLog/Settings/epics_path'"`
echo  "epics paths:  $paths "
#odb -e $MIDAS_EXPT_NAME -c "msg   'check_epics' 'input epics paths: $paths'"


set idx = ( 0 )
set icount = ( 0 )
set good_path = ""

foreach my_path ($paths)
   echo "icount=$icount  working on path $my_path"
#   odb -e $MIDAS_EXPT_NAME -c "msg   'check_epics' 'working on path $my_path'"

   if  ($icount >= $n_epics_logged) then
      echo "breaking with icount=$icount and n_epics_logged=$n_epics_logged"
      #odb -e $MIDAS_EXPT_NAME -c "msg   'check_epics' 'breaking with icount=$icount and n_epics_logged=$n_epics_logged'"

      break
   endif

# the first two values are "epics_path"
   if ($my_path != "epics_path") then
       #echo "checking path $my_path"
       caget "$my_path"
       set my_status=$status
       # echo $my_status
 
       if($my_status == 0) then
          #echo "incrementing icount to $icount, saving $my_path"
	  set good_path = "$good_path $my_path"
          #echo $good_path 
      # odb -e $MIDAS_EXPT_NAME -c "msg   'check_epics' 'setting output path to $my_path (icount=$icount)'"
          odb -e $MIDAS_EXPT_NAME -c "set '/Equipment/EpicsLog/Settings/output/epics_log_ioc[$icount]' $my_path"
          @ icount ++  # count the valid paths
      
       else
#         echo "bad status $my_status"
          echo "path $my_path failed; will not be logged"

	  set message = "cannot access $my_path; will not be logged"
          odb -e $MIDAS_EXPT_NAME -c "msg '1'  'check_epics' '$message'"
          set nchar = $%message
       
       
# Limit length of Alarm Message in odb  or get record size mismatch error
          if($nchar > 79 ) then    
             set message = "Invalid EPICS parameters. Click on Messages button for details"
             echo "Alarm Message has had to be truncated. Number of characters now is $%message"
          endif
      
          odb -e $MIDAS_EXPT_NAME -c "set '/Alarms/Alarms/epicslog/Alarm Message' '$message'"

          # trigger odb alarm /equipment/fifo_acq/client flags/epicslog alarm
          echo "setting epicslog alarm to 1 "
          odb -e $MIDAS_EXPT_NAME -c "set '/equipment/fifo_acq/client flags/epicslog alarm' 1" 
 
          endif

       endif # path is not "epics_path"

end # end of foreach loop


echo "writing icount=$icount to output/num_log_ioc"
odb -e $MIDAS_EXPT_NAME -c "msg  'check_epics' 'writing $icount to /equipment/epicslog/settings/output/num_log_ioc'"
odb -e $MIDAS_EXPT_NAME -c "set '/Equipment/EpicsLog/Settings/output/num_log_ioc' $icount" 
echo "Valid epics paths: $good_path"


# clear empty paths to avoid confusion
   set good_path = ""
   while($icount < $max)
       odb -e $MIDAS_EXPT_NAME -c "set '/Equipment/EpicsLog/Settings/output/epics_log_ioc[$icount]' $good_path"
       @ icount ++  # counter
   end
exit    

