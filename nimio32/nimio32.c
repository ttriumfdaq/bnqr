/*********************************************************************

  Name:         nimio.c
  Created by:   Pierre-Andre Amaudruz

  Contents:      Routines for accessing the VMEIO Triumf board
                
  
   $Log: nimio32.c,v $
   Revision 1.5  2018/09/05 22:16:36  suz
   add function nimio_Reset

   Revision 1.4  2015/04/28 20:18:41  suz
   change simulator output 5 to output 3

   Revision 1.3  2014/03/24 22:49:10  suz
   add more bnmr hel up/down testing

   Revision 1.2  2014/03/21 20:16:31  suz
   add support for latched helicity readback modification to firmware

*********************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <signal.h>
#include "VMENIMIO32.h"

#ifdef MAIN_ENABLE
#define HEL_DOWN                0         /* helicity down */
#define HEL_UP                  1         /* helicity up */
#define INPUT0              0      /* VMEIO Input ch 0  Beam */
#define INPUT1              1        /* VMEIO Input ch 1  Hel Down */
#define INPUT2              2        /* VMEIO Input ch 2  Hel Up   */
#define INPUT3              3        /* VMEIO Input ch 3  Latched Hel DOWN (connected to OUTPUT 0) */
#define INPUT4              4        /* VMEIO Input ch 4  Latched Hel UP (connected to OUTPUT 1 */

typedef long int INT;
typedef DWORD BOOL;

void bnmr_simulator(MVME_INTERFACE *myvme, DWORD base);
void bnmrcmd(void);
INT hel_read_ioreg(MVME_INTERFACE *myvme, DWORD base, BOOL dual_channel_mode, BOOL write);
char *helicity_state(INT n);

#endif // MAIN_ENABLE

/********************************************************************/

uint32_t nimio_CommandReg(MVME_INTERFACE *myvme, DWORD base, DWORD data)
{
  uint32_t my_data;
  //data = 0 (no-op) 1 (reset) etc.
   nimio_write32(myvme,  base, IO32_COMMAND ,  data & 0xF);
   my_data = nimio_read32(myvme, base,  IO32_COMMAND);
   return (my_data & 0xF);
}
uint32_t nimio_Reset(MVME_INTERFACE *myvme, DWORD base)
{
  uint32_t data;
  data = nimio_CommandReg(myvme, base, IO32_CMD_RESET ); // General Reset = 1
  return data;
}

void nimio_OutputSet(MVME_INTERFACE *myvme, DWORD base, DWORD data)
{
  DWORD data1,data2;
  data2 =  data & 0xFFFF; // get just lower bits 0-16 NIM outputs

  data1 =  nimio_read32(myvme,  base, IO32_NIMOUT); // read whole word 32 bits
  data1 = data1 & 0xFFFF0000; // get upper output control bits 16-31

  printf("nimio_OutputSet: currently output control bits (16-31) are 0x%4.4x\n",data1);
  data1 = data2 | data1; // OR
  printf("nimio_OutputSet: writing NIM output word as  0x%8.8x\n",data1);
   nimio_write32(myvme,  base, IO32_NIMOUT ,  data1);
}

uint32_t nimio_OutputRead(MVME_INTERFACE *myvme, DWORD base)
{
  uint32_t data;
  data = nimio_read32(myvme,  base, IO32_NIMOUT);
  return (data & 0xFFFF);
}

uint32_t nimio_OutputControlRead(MVME_INTERFACE *myvme, DWORD base)
{
  uint32_t data;
  data = nimio_read32(myvme,  base, IO32_NIMOUT);
  return ((data & 0xFFFF0000) >> 16);
}

void nimio_OutputControlSet(MVME_INTERFACE *myvme, DWORD base, DWORD data )
{
  uint32_t my_data,data1;
  my_data = (data & 0xFFFF) << 16;

  data1 =  nimio_read32(myvme,  base, IO32_NIMOUT); // read whole word
  data1 = data1 & 0xFFFF; // get lower bits NIM outputs 0-15
  printf("Currently NIM outputs 0-15 are 0x%4.4x\n",data1);
  data1 = my_data | data1; // OR
printf("Writing 0x%x to NIM output reg\n",data1);
   nimio_write32(myvme,  base, IO32_NIMOUT ,  data1);
   
}

void nimio_SetOneOutput(MVME_INTERFACE *myvme, DWORD base, DWORD bit )
{
  uint32_t data;

  data =  nimio_read32(myvme,  base, IO32_NIMOUT);
  data = nimio_bitset(bit, data);
  nimio_write32(myvme,  base, IO32_NIMOUT, data);
}

void nimio_ClrOneOutput(MVME_INTERFACE *myvme, DWORD base, DWORD bit )
{
  uint32_t data;

  data =  nimio_read32(myvme,  base, IO32_NIMOUT);
  data = nimio_bitclear(bit, data);
  nimio_write32(myvme,  base, IO32_NIMOUT, data);
}

uint32_t nimio_ReadOneInput(MVME_INTERFACE *myvme, DWORD base, DWORD bit)
{
  uint32_t data;
  uint32_t ival;
  data = nimio_read32(myvme,  base, IO32_NIMIN);
  ival = 1 << bit;
  return (data & ival);
}

uint32_t nimio_ReadOneLatchedInput(MVME_INTERFACE *myvme, DWORD base, DWORD bit)
{
  uint32_t data;
  uint32_t ival;
  data = nimio_read32(myvme,  base, IO32_NIMIN);
  ival = 1 << bit;
  data= data >> 16;  // latched bits in upper 16 bits;
  return (data & ival);
}



void nimio_ResetLatch(MVME_INTERFACE *myvme, DWORD base, DWORD data)
{
  // mvme_write_value(myvme, base+IO32_NIMOUT, data & 0xFFFFFF);
   nimio_write32(myvme,  base, IO32_NIMIN ,  data & 0xFFFF);
}

uint32_t nimio_InputRead(MVME_INTERFACE *myvme, DWORD base)
{
  uint32_t data;
  data = nimio_read32(myvme,  base, IO32_NIMIN);
  return data;
}

uint32_t nimio_FirmwareRead(MVME_INTERFACE *myvme, DWORD base)
{
  uint32_t data;
  data = nimio_read32(myvme,  base, IO32_REVISION);
  return data;
}


/********************************************************************/


/********************************************************************/
/**
Read the NIMIO32
@param myvme vme structure
@param base  VMEIO base address
@return register value
*/

uint32_t nimio_read32(MVME_INTERFACE *myvme, DWORD base, DWORD offset)
{
  mvme_set_am(myvme, MVME_AM_A24);
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  return mvme_read_value(myvme, base+offset);
}

void nimio_write32(MVME_INTERFACE *myvme, DWORD base, DWORD offset, uint32_t data)
{
  mvme_set_am(myvme, MVME_AM_A24);
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  mvme_write_value(myvme, base + offset, data);
}

uint32_t nimio_bitset(int bit, uint32_t data)
{
  uint32_t ival;
  ival = 1<<bit;
  data=data | ival;
  return data;
}

uint32_t nimio_bitclear(int bit, uint32_t data)
{
  uint32_t ival;
  ival = 1<<bit;
  data = data & ~ival;
  return data;
}

void read_bnmrinputs(MVME_INTERFACE *myvme, DWORD base)
{
  uint32_t data,data1,data2;
  // Input 0 BEAM
  //       1 HEL DWN
  //       2 HEL UP
  //       3 HEL DWN Latched by beam
  //       4 HEL UP Latched by beam 

  int beam, held, helu, lheld, lhelu;
  data =  nimio_InputRead(myvme, base);
  data1= data & 0xFFFF0000;
  data2= data & 0x0000FFFF;
  printf(" data=0x%8.8x; Latched NIM inputs 0x%4.4x, NIM inputs=0x%4.4x\n",
	  data, (data1 >> 16), data2);

  beam=held=helu=lheld=lhelu=0;
  if( data & 1)
    beam=1;
  if(data & 2)
    held=1;
  if(data & 4)
    helu=1;
  if(data & 8)
    lheld =1;
  if(data & 16)
    lhelu =1;

  printf("Beam %d   hel down = %d hel up=%d    -->  latched  hel down = %d hel up = %d \n",
	 beam, held, helu, lheld, lhelu);
  return;
}

#ifdef MAIN_ENABLE

void bnmr_simulator(MVME_INTERFACE *myvme, DWORD base)
{
  /*  Simulator :
      When real signals are not available, functionality of IOREG can be checked using the simulator
      Outputs 2,3,4 are used for the simulator. 
      Make sure output control bits are set so that Outputs 0-3 are regular outputs (see command 'K')
      Make the following connections: 
      BEAM is controlled by output 2 (connected to input 0)
      Hel Down              output 3 (connected to input 1)
      Hel Up                output 4 (connected to input 2)
  */
 char cmd[]="hallo";
 int s;
 uint32_t      data;
 int helup=4;   // Output 4  bitpat 0x10
 int heldown=3; // Output 3  bitpat 0x8
 int beam=2;    // Output 2
 int hu,hd,bm;
 int state;

 
   printf("BNMR Simulator - used when real signals are not available\n");
   printf("Outputs 2,3,4 are used for the simulator.\n"); 
   printf("Make sure output control bits are set so that Outputs 0-3 are regular outputs (see command 'K')\n"); 
   printf("   Make the following connections: \n"); 
   printf("   BEAM is controlled by output 2 (connected to input 0)\n"); 
   printf("   Hel Down              output 3 (connected to input 1)\n"); 
   printf("   Hel Up                output 4 (connected to input 2)\n"); 

  while ( isalpha(cmd[0]) )
    {
      printf("\nEnter simulator command (A-Y) X to exit?  ");
      scanf("%s",cmd);
      printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];
      
      switch(s)
	{  // set base
	case ('A'):
	  printf("Reading BNMR inputs\n");
	  read_bnmrinputs(myvme, base);
	  break;
	case ('B'):
	  printf("Setting Beam On\n");
	  nimio_SetOneOutput(myvme,base,beam);  // beam on
	  break;
	case ('C'):
	  printf("Setting Beam Off\n");
	  nimio_ClrOneOutput(myvme,base,beam);  // beam off
	  break;
	case ('D'):
	  printf("Setting Hel Up\n");
	  nimio_ClrOneOutput(myvme,base,heldown);  // clear hel down
	  nimio_SetOneOutput(myvme,base,helup);  // set hel up
	  break;
	case ('E'):
	  printf("Setting Hel Down\n");
	  nimio_ClrOneOutput(myvme,base,helup);  // clear hel up
	  nimio_SetOneOutput(myvme,base,heldown);  // set hel down
	  break;
	case ('F'):
	  printf("Setting Hel Down and Up\n");
	  nimio_SetOneOutput(myvme,base,heldown);  // set hel down
	  nimio_SetOneOutput(myvme,base,helup);  // set hel up
	  break;
	case ('G'):
	  printf("Clearing Hel Down and Up \n");
	  nimio_ClrOneOutput(myvme,base,helup);  // clear hel up
	  nimio_ClrOneOutput(myvme,base,heldown);  // clear hel down
	  break;
	case ('H'):
	  printf("Readback OUTPUT bits \n");
	  data =nimio_OutputRead(myvme, base);
	  data = data & 0xFFFC; // mask away outputs 0,1 (under bnmr latch control)
          hu=bm=hd=0;
          if(data & 4)  // output 2
	    bm=1;
          if(data & 8) // output 3
	    hd=1;
	  if(data & 0x10) // output 4
	    hu=1;
	  printf("Readback masked OUTPUT word as 0x%x\n",data);
	  printf("Beam: %d  Hel Up: %d   Hel Down: %d\n",bm,hu,hd);
	  break;
	case ('I'):  // test translation to HEL_UP and HEL_DOWN

	  state=hel_read_ioreg(myvme,base,0,1);
	  printf("state is %d or %s\n ",state, helicity_state(state));
	  break;


	case ('S'):  // Simulate
	  printf("Simulator starting\n");
	  // Beam Off
	  nimio_ClrOneOutput(myvme,base,beam);  // beam off
	  printf("\n== Beam Off ==\n");
	  
	  sleep(1); // sleep
	  
	  printf("\n-- Hel Down --\n");
	  
	  // set Hel Down
	  nimio_ClrOneOutput(myvme,base,helup);  // clear hel up
	  nimio_SetOneOutput(myvme,base,heldown);  // set hel down
	  
	  // Read helicity state
	  printf("\nBeam Off, Hel Down\n");
	  read_bnmrinputs(myvme, base);
	  
	  
	  // Beam On
	  nimio_SetOneOutput(myvme,base,beam);  // beam on
	  printf("\n** Beam On **\n");
	  sleep(1); // sleep
	  printf("\nBeam On, Hel Down\n");
	  read_bnmrinputs(myvme, base);
	  printf ("\n");
      
	  // Beam Off
	  nimio_ClrOneOutput(myvme,base,beam);  // beam off
	  printf("\n== Beam Off ==\n");
	  sleep(1); // sleep
	  
	  // Read helicity state - should be latched 
	  printf("\nBeam Off, Hel Down\n");
	  read_bnmrinputs(myvme, base);
	  
	  
	  printf("\n++ Hel up ++\n");
	  // set Hel up
	  nimio_ClrOneOutput(myvme,base,heldown);  // clear hel up
	  nimio_SetOneOutput(myvme,base,helup);  // set hel down
	  
	  printf("\nBeam Off, Hel up\n");
	  // Read helicity state
	  read_bnmrinputs(myvme, base);
	  
          // Beam On
	  nimio_SetOneOutput(myvme,base,beam);  // beam on
	  printf("\n** Beam On **\n");
	  sleep(1); // sleep
	  
	  printf("\nBeam On, Hel up\n");
	  read_bnmrinputs(myvme, base);
	  
	  
	  // Beam Off
	  nimio_ClrOneOutput(myvme,base,beam);  // beam off
	  printf("\n== Beam Off ==\n");
	  sleep(1); // sleep
	  
	  read_bnmrinputs(myvme, base);
	  break;

	case ('P'): // Print command list
	  bnmrcmd();
	  break;

	case ('X'):
	case('Q'):
	  return;
	  
	
	default:
	  printf("Unknown command\n");
	  bnmrcmd();
	}
    }
}

void bnmrcmd(void)
{
   printf(" BNMR simulator command list\n");
   printf("A Read BNMR inputs\n");
   printf("B Beam On            C Beam Off\n");        
   printf("D Hel Up             E Hel Down \n");
   printf("F Hel Up and Down    G Hel neither\n");
   printf("H Read Outputs       I hel_read_io\n");
   printf("S Simulate changing hel\n");
   printf("P Print command list Q Quit\n");
}

// Make sure this routine is working correctly

INT hel_read_ioreg(MVME_INTERFACE *myvme, DWORD base, BOOL dual_channel_mode, BOOL write)
{
  /* returns  helicity state (latched if dual channel mode is true)
     0 = HEL DOWN
     1 = HEL UP
     2 = Waveplate split in half (in both states) or no io module in crate
     3 = Waveplate in transit (in neither state)
     4 = unexpected error
  */

  INT state, held, helu;

  if(write)
    read_bnmrinputs(myvme,  base); // Write out states for debug

  held=helu=0; // clear

  if(dual_channel_mode)
    { // read latched values 
      if(nimio_ReadOneInput(myvme,  base, INPUT3)) // latched hel down
	 held=1;
      if(nimio_ReadOneInput(myvme,  base, INPUT4))  // latched hel up
	helu=1;
    }
  else
    {
      // read unlatched values 
      if( nimio_ReadOneInput(myvme,  base, INPUT1)) // hel down
	held=1;
      if(nimio_ReadOneInput(myvme,  base, INPUT2)) // hel up
	helu=1;
    }

  if(held == helu) // error; both are the same 
    {
      if(held ==1)  // both on (shouldn't get this)
	state = 3; // error...  both states are true
      else
	state = 2; // both states are false (waveplate in transit)
    }

  else if (held == 1 )
    state =  HEL_DOWN; 
  else if (helu == 1 )
    state =  HEL_UP; 
  else
    state = 4; // peculiar error

  printf(" hel_read_ioreg: hel down=%d hel up=%d state=%s\n",
	 held,helu, helicity_state(state));

  return state;
}



char *helicity_state(INT n)
{
  static char *name[]= {
    "HEL DOWN","HEL UP","IN TRANSIT" ,"PLATE SPLIT","ERROR"
  };
  return ( n < 0 || n > 4) ? name[4] : name[n] ;
}









void nimio(void)
{
   printf("       NIMIO32 command list\n");
   printf("A Read BNMR inputs       B Show BaseAddr\n");
   printf("C Set One Output         D Set OutputBitPat \n");
   printf("E Read OutputBitPat      F Read Firmware\n");
   printf("G Read Input bits        H Reset Latch on Inputs\n");
   printf("I Read OutputCntrlBits   J Read whole output word (32 bits)\n");
   printf("K Write OutputCntrlBits  L Set Gate Delay values\n");
   printf("M Read Gate Delay values N Pulse one output \n");
   printf("O Read one input         Q Read one latched input\n");
   printf("P print this list        R Reset \n");
   printf("S bnmr simulator\n");
   printf("X exit \n");
   printf("\n");
}


int main () 
{

#define NUM_OUTPUTS           8        /*  number of output LEMOS fitted  */
#define NUM_INPUTS            8        /*  number of input LEMOS fitted  */
  MVME_INTERFACE *myvme;
  DWORD NIMIO32_BASE1 = 0x00100000 ;
  DWORD NIMIO32_BASE2 = 0x00200000 ;
  DWORD base[2];
 

  int status, csr;
  uint32_t      data,data1,data2;
  char cmd[]="hallo";
  int i,s;
  int ival, bit;

  base[0]= NIMIO32_BASE1; // default
  base[1]= NIMIO32_BASE2; 

 int nmod=1; // default one module

  // Test under vmic   
  status = mvme_open(&myvme, 0);
  printf("\n");
  csr = nimio_FirmwareRead(myvme,NIMIO32_BASE1);
  printf("Firmware for Module 1: 0x%x\n", csr);
  csr = nimio_FirmwareRead(myvme,NIMIO32_BASE2);
  printf("Firmware for Module 2: 0x%x\n", csr);
  
  for(i=0; i<nmod; i++)
    printf("Base[%d]=0x%x\n",i,base[i]);
  
  while ( isalpha(cmd[0]) )
    {
      printf("\nEnter command (A-Y) X to exit?  ");
      scanf("%s",cmd);
      printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];
      
      switch(s)
	{  // switch
	case ('A'):
#ifdef GONE
	   printf("Select Module to write (1 = Base 0x%x, 2 = Base 0x%x) 3 for both or 0 for other :",
	  	 NIMIO32_BASE1,	NIMIO32_BASE2 );
	   scanf("%d",&i);
	   nmod=1;
	  switch(i)
	    {
            case(0):
	      printf("Enter base 0x");
	      scanf("%x",&base[0]);
	      break;
	    case(2):
	      base[0]=NIMIO32_BASE2;
	      break;
	    case(3):
	      nmod=2;
	    default:
	      base[0]=NIMIO32_BASE1;
	    }
#endif
	      printf("Interpreting BNMR Inputs\n");
	      for(i=0; i<nmod; i++)
		read_bnmrinputs(myvme,  base[i]);
	      break;
	    

	case ('B'): // Query base
	  if(nmod > 1)
	    printf("BOTH modules are selected, i.e. Module 1 at base  0x%x and Module 2 at base  0x%x \n", base[0],base[1]);
	  
	  else if(base[0] == NIMIO32_BASE1)
	    printf("Base is 0x%x (Module 1)\n",base[0]);
	      
	  else if(base[0] == NIMIO32_BASE2)
	      printf("Base is 0x%x (Module 2)\n",base[0]);
	      
	  else
	    printf("Base is 0x%x \n",base[0]);

	  break;

	case ('C'): 
	  bit=-1;
          while( bit < 0 || bit >= NUM_OUTPUTS) // NUM_OUTPUTS = number of outputs
	    {
	      printf("Enter Output (0-%d) :",(NUM_OUTPUTS-1)); //outputs numbers from 0
	      scanf("%d",&bit);
	    }
	  ival = -1;
	  while (ival < 0 || ival > 1)
	    {
	      printf("Enter Set (1) or Clear (0) :"); //0-1
	      scanf("%d",&ival);
	    }
 
	  for(i=0; i<nmod; i++)
	    {
	      data =  nimio_read32(myvme,  base[i], IO32_NIMOUT);
	      printf("Module %d: read complete output word is   0x%8.8x\n",(i+1),data);
              if(ival==1)
		data = nimio_bitset(bit, data);
	      else if(ival == 0)
		data = nimio_bitclear(bit, data);
	      nimio_write32(myvme,  base[i], IO32_NIMOUT, data);
	      printf("Module %d:  now complete output word is   0x%8.8x\n",(i+1),data);
	    }
	  break;
	  
	case ('D'):  // Set output bits 0-15
	  
	  for(i=0; i<nmod; i++)
	    {
	      printf("Module %d: enter Output bitpattern to write (bits 0-15) :0x",(i+1));
	      scanf("%lx",&data);
	      nimio_OutputSet(myvme, base[i], data);
	    }
	  break;

	case ('E'): // Read output bits 0-15
	  for(i=0; i<nmod; i++)
	    {
	      data =nimio_OutputRead(myvme, base[i]);
	      printf("Module %d: data= 0x%4.4x\n",(i+1),data);
	    }
	  break;

	case ('F'): // Read Firmware
	  for(i=0; i<nmod; i++)
	    {
	      data=nimio_FirmwareRead(myvme, base[i]);
	      printf("Module %d: firmware : 0x%8.8x\n",(i+1), data);
	    }
	  break;

	case ('G'): // Read input bits 0-31
	  for(i=0; i<nmod; i++)
	    {
	      data =  nimio_InputRead(myvme, base[i]);
              data1= data & 0xFFFF0000;
	      data2= data & 0x0000FFFF;
	      printf("Module %d: data=0x%8.8x; Latched NIM inputs 0x%4.4x, NIM inputs=0x%4.4x\n",
		     (i+1), data, (data1 >> 16), data2);

	    }
	  break;

	case ('H'): // Reset Latch (data will be shifted to top bits)
	  for(i=0; i<nmod; i++)
	    {
	      printf("Module %d: enter Reset Latch bitpattern e.g. 0xFFFF :0x",(i+1));
	      scanf("%lx",&data);
	      nimio_ResetLatch(myvme, base[i], data);
	    }
	  break;


	case ('I'): // Read Output Control bits
	  for(i=0; i<nmod; i++)
	    {
	      data = nimio_OutputControlRead(myvme, base[i]);
	      printf("Module %d: output Control bits are 0x%4.4x\n",(i+1),data);
	    }
	  break;

	case ('J'): // Read Output Word (32 bits)
	  for(i=0; i<nmod; i++)
	    {
	      data =  nimio_read32(myvme,  base[i], IO32_NIMOUT);
	      printf("Module %d: complete output word is   0x%8.8x\n",(i+1),data);
	  }
	  break;


	case ('K'): // Write Output Control bits
	  printf("Examples: \n"); 
	  printf("Set  0x0040 for NIMOUT 0-3 regular outputs \n");
 	  printf("Set       0 for NIMOUT 3  to be 40MHz clock \n");
          printf("Set  0x0080 for delay generator using NIMIN3 and NIMOUT3 (use command L to program) \n");
	  printf("Set  0x0001 for NIMOUT 0 to be 20MHz clock \n");

	  for(i=0; i<nmod; i++)
	    {
	      printf("Module %d: enter Output control bitpattern e.g. 0xFFFF :0x",(i+1));
	      scanf("%lx",&data);
	      nimio_OutputControlSet(myvme, base[i], data);
	    }
	  break;
	  
	case ('L'):
          //  Set Gate Delay values in Register 48
          //  NIM input 3 and output 3 function 2
          printf("Use command \'K\' to set NIM output 3 into GateDelay mode (uses input 3)\n");
	  printf("Enter Gate Pulse delay in us (100ns to 650us) Max 0xFFFF :0x"); //0-15
	  scanf("%lx",&data1);
	  printf("Enter Gate Pulse width in us (20ns to 650us) Max 0xFFFF :0x");//16-31
	  scanf("%lx",&data2);
	  data1=data1 & 0xFFFF;
	  data = (data2 << 16) | data1;
	  printf("Writing data 0x%x to Register 48 (delay generator)\n",data);

	  for(i=0; i<nmod; i++)
	    nimio_write32(myvme, base[i], IO32_DELAY,data);
	  break;
	  
	  
	case ('M'):
	  //  Read Delay values in Register 48
	  for(i=0; i<nmod; i++)
	    {
	      data =  nimio_read32(myvme,  base[i], IO32_DELAY);
	      printf("Module %d: Gate delay register = 0x%x\n",i,data);
              data1=data & 0xFFFF;
              printf("Module %d: Gate Pulse delay in  us (100ns to 650us) is 0x%x\n",i,data1);
	      data2 = data >> 16;
	      printf("Module %d: Gate Pulse width in  us (20ns to 650us) is 0x%x\n",i,data2);
	    }


	case ('N'):
	  //  Pulse an output
	  bit=-1;
          while( bit < 0 || bit >= NUM_OUTPUTS) // NUM_OUTPUTS = number of outputs
	    {
	      printf("Enter Output to pulse (0-%d) :",(NUM_OUTPUTS-1)); //outputs numbers from 0
	      scanf("%d",&bit);
	    }

	  for(i=0; i<nmod; i++)
	    {
	      nimio_SetOneOutput(myvme, base[i], bit);
	      nimio_ClrOneOutput(myvme, base[i], bit);
	      break;
	    }

	case ('O'): // read one input line
	case ('Q'): // read one latched input line
	 
	  bit=-1;
          while( bit < 0 || bit >= NUM_INPUTS) // NUM_INPUTS = number of input lemos
	    {
	      printf("Enter Input to read (0-%d) :",(NUM_INPUTS-1)); //inputs numbers from 0
	      scanf("%d",&bit);
	    }

	  for(i=0; i<nmod; i++)
	    {
	      printf("Module %d: ",i);
	      if (s=='Q')
		{
		  data = nimio_ReadOneLatchedInput(myvme, base[i], bit);
		  printf("Latched ");
		}
	      else
		data = nimio_ReadOneInput(myvme, base[i], bit);
	      if(data)
		printf("Input bit %d is SET\n",bit);
	      else
		printf("Input bit %d is CLEAR\n",bit);
	    }
	  break;


	  //	case ('B'):
	  // printf("Enter Mask value to write :0x");
	  //  scanf("%lx",&pol);
	  //  break;

	case ('P'): // print command list
	  nimio();
	  break;

	case ('R'): // Reset
	  for(i=0; i<nmod; i++)
	    {
              data = nimio_Reset(myvme, base[i]);
	      printf("Module %d : read back 0x%1.1x\n",(i+1), data);
	    }
	  break;

	case ('S'): // Bnmr Simulator
	  for(i=0; i<nmod; i++)
	      bnmr_simulator(myvme, base[i]);
	  break;


	case ('X'): // exit
	  goto exit;
	  break;

	default:
	  printf("Illegal cmd\n");
	}
    }




 

 exit:
  status = mvme_close(myvme);
  return 1;
}	
#endif
