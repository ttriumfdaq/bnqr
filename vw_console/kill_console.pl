#!/usr/bin/perl
#
#  $Log: kill_console.pl,v $

# called by kill-all to kill the MVME162 console window
#
use strict;
# input parameters:
#  beamline  bnqr/pol  -> polcon1 
#      or     bnmr      -> bnmrcon1
#
#  port :   blank         both consoles
#     or    2001         ppc console
#     or    2002         mvme162 console
#
# e.g.  kill_console.pl bnmr      kills both consoles for bnmr 
#       kill_console.pl pol 2002  kills mvme162 console for pol
#
my ($beamline, $port ) = @ARGV;# input parameters
my $len = $#ARGV; # array length
my $nparam = 2;  # max no. of input parameters
my $process_id;
my $debug=0;
my $name="blank";
my $username="none";
my $killed=0;
my $port_msg;

$nparam = $nparam + 0;  #make sure it's an integer
$len += 1; # no. of supplied parameters
if($len == 0)
{
    die "Beamline must be supplied as a parameter\n ";
}

#print  "No. parameters suppled = $len\n";
if ($len == $nparam-1)
{ 
    print "Port is not supplied; both consoles for $beamline will be killed \n";
    $port = "*";
    $port_msg = "";
}
else
{
#    print "Port $port is supplied; one console for $beamline will be killed \n";
    $port_msg = " $port";
}

if ( $beamline =~ /bnmr/i ) { $name = "bnmrcon1"; }
else { $name = "polcon1"; }

print "Looking for \"$name$port_msg\" to kill console window(s) for $beamline...\n";

open PS, "ps -efw |";
while (<PS>) 
{
#    print "$_";
    if(/([a-z]+)[\s]+([\d]+) [\s].*$name.$port/) # look for character string (i.e. username) followed by
#                                              spaces, then a space +1 or more digits
#                                              first set of brackets  ->backreference $1 (username)
#                                              second set of brackets ->backreference $2 (pid)
    {
	$username = $1;
	$process_id = $2;
#	print "$_";
	print "found $name $port_msg running  with PID $process_id and username $username\n" ;
	if( $username eq $beamline)
	{
	    print "killing $process_id\n";
	    open KI," kill $process_id |";
	    while (<KI>) 
	    {
		print "$_";
	    }
	    close (KI);
	    $killed=1;
	}
	else
	{	    
	    print "Warning: username $username not same as $beamline\n";
	    print "\n *** ERROR  Another user ($username) is using your console ... \n";
	    die " *** that user must kill process $process_id before $beamline can start the console. \n\n";
	}
    }
}
close(PS) ;
unless ($killed)
{
    print "\"$name$port_msg\" not found; console window(s) not running for $beamline\n";
}
exit;


