#####################################################################
#
#  Name:         Makefile
#  Created by:   Stefan Ritt
#
#  Contents:     UNIX Makefile for MIDAS EPICS slow control frontend
#
#  $Id: Makefile,v 1.1 2015/12/03 23:47:15 suz Exp $
#
#####################################################################

#--------------------------------------------------------------------
# The following lines contain specific switches for different UNIX
# systems. Find the one which matches your OS and outcomment the 
# lines below.

# This is for Linux ----------------
LIBS = -lbsd -lm -lutil -lpthread -lrt
OSFLAGS = -DOS_LINUX

# This is for OSF1 -----------------
#LIBS = -lbsd
#OSFLAGS = -DOS_OSF1

# This is for Ultrix ---------------
#LIBS =
#OSFLAGS = -DOS_ULTRIX -Dextname

# This is for FreeBSD --------------
#LIBS = -lbsd -lcompat
#OSFLAGS = -DOS_FREEBSD

#-------------------------------------------------------------------
# The following lines define directories. Adjust if necessary
#                 
INC_DIR 	= $(MIDASSYS)/include
LIB_DIR 	= $(MIDASSYS)/linux/lib
DRV_DIR		= $(MIDASSYS)/drivers
EPICSSYS        = /home/midas/packages/epics-base/
EPICS_DIR       = $(EPICSSYS)
EPICS_LIBDIR    = $(EPICSSYS)/lib/linux-x86_64
DEST_DIR        = ./linux
#-------------------------------------------------------------------
# Drivers needed by the frontend program
#                 
DRIVERS         = $(DEST_DIR)/generic.o $(DEST_DIR)/epics_ca.o

####################################################################
# Lines below here should not be edited
####################################################################

LIB = $(LIB_DIR)/libmidas.a $(EPICS_LIBDIR)/libca.a $(EPICS_LIBDIR)/libCom.a -lreadline -ldl

# compiler
CC = gcc
CCXX = g++
CFLAGS = -g  -D_POSIX_C_SOURCE=199506L -D_POSIX_THREADS -D_XOPEN_SOURCE=500 \
	-DOSITHREAD_USE_DEFAULT_STACK -D_X86_64_ -DUNIX -D_BSD_SOURCE -Dlinux \
	-I. -I$(INC_DIR) -I$(DRV_DIR) -I$(EPICS_DIR)/include -I$(EPICS_DIR)/include/os/Linux/
LDFLAGS =

all: $(DEST_DIR) $(DEST_DIR)/fe_epics

# create output directory
#
$(DEST_DIR):
	@if [ ! -d  $(DEST_DIR) ] ; then \
	 echo "Making directory $(DEST_DIR)" ; \
	mkdir $(DEST_DIR); \
	fi;

$(DEST_DIR)/fe_epics:  $(LIB) $(LIB_DIR)/mfe.o $(DEST_DIR)/frontend.o $(DRIVERS)
	$(CCXX) $(CFLAGS) -o $(DEST_DIR)/fe_epics $(DEST_DIR)/frontend.o $(LIB_DIR)/mfe.o $(DRIVERS) $(LIB) $(LDFLAGS) $(LIBS)

$(DEST_DIR)/frontend.o: frontend.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@

$(DEST_DIR)/generic.o: $(DRV_DIR)/class/generic.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@

$(DEST_DIR)/epics_ca.o: $(DRV_DIR)/device/epics_ca.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@

#.c.o:
#	$(CC) $(CFLAGS) $(OSFLAGS) -c $<

$(DEST_DIR)/%.o:%.c
	$(CC) -c $(CFLAGS) $(OSFLAGS) -o $@ $<

clean:
	rm -f $(DEST_DIR)/* *~ \#*
