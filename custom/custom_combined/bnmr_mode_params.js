// Specific parameters for bnmr used by cs_functions.js

// Used in Custom_Status page
//
// mode1Names includes combined modes

var mode1Names=new Array("Scalers","1a","1b","Camp","Freq","NaCell","CW","LCR","Fast");
var mode1Info=[
		 "Mode 10; nothing is scanned","","",
                 "Mode 1c: CAMP variable scanned",
                 "Mode 1f: RF scanned",
                 "Mode 1n: NaCell voltage scanned",
                 "Mode 1w: CW mode freq(s) scanned using functions(x)",
                 "Mode 1j: combination of Modes 20 and 1c (CAMP scan)",
                 "Mode 1g: combination of Modes 20 and 1f (RF scan)"
	       ];
var num_type1_modes=7;  // 2 modes (LCR,FAST) are combined modes

var mode2Names=new Array("SLR","2a","2b","2c","2d","2e","2f","2SE","Wurst");   // BNMR
var mode2Info=[
		 "Mode 20: Select no RF or fixed freq","","","","",
                 "RF scanned (loads frequency table)",
                 "not tested; do not use",
                 "Mode 2s: Spin Excitation with PSMIII f0ch1,f0ch2,f0ch3,f0ch4 phased 90deg apart",
                 "Mode 2w: Wurst modulation mode with PSMIII channels phased 90deg apart",
	       ];

// Used in parameters page
// Hardware
var  psm_module="PSMIII";
var have_psm3=1;
var have_psm2=0;

// Modes for BNMR
// only those PPG Modes with special names are listed
var ppg_mode_array = ["10","1c","1d","1f","1g","1n","1j","1w","20","2g","2s","2w"];
var ppg_mode_names = ["Scalers","Camp Scan","Laser Scan","Frequency Scan","Fast","NaCell Scan","LCR","CW","SLR","SampleRef","SpinEcho","Wurst"];

// Mode 1w BNMR only
var evalPeriod=1000; //ms  poll time for evaluate perlscript (mode 1w only)
var evalTimerId=0;   // timerID for evaluate perlscript  (mode 1w only)
var remember_evalfreq;
var cw_eval_path = hidden_path + "/cw eval status";

// Mode 2s BNMR only
var e2s_rf_pi_on_path,  e2s_rf_halfpi_on_path, e2s_select_mode_se2_path;
var e2s_zdwell_path, e2s_xdwell_path, e2s_nfid_path, e2s_enable_pi_pulse_path;
var e2s_pi_pulse_cycle_num_path,  e2s_xy_flip_half_pulse_path, e2w_central_freq_path;

var e2w_freq_sweep_path, e2w_li_precession_path,  e2w_tot_freq_scans_path, e2w_tp_path;
var e2s_qpc3_path, e2s_qpc5_path, e2s_qpcf_path, e1w_f1_function_path, e1w_f2_function_path;
var e1w_f3_function_path, e1w_f4_function_path, e1w_x_start_path, e1w_x_stop_path, e1w_x_incr_path;
