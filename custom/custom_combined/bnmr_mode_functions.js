// bnmr_functions.js
// Mode-specific functions for bnmr

// These modes do not exist in bnqr
// bnmr_mode_params.js

//-------------------------------------------------------------------------
// The following  mode parameters are only used by 2s, 2w, 1w (bnmr only) 

function get_exp_specific_param_paths(ppg_mode_path)
{
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="get_exp_specific_param_path starting with ppg_mode_path="+ppg_mode_path;

    //    alert('get_exp_specific_param_paths: starting with ppg_mode_path='+ppg_mode_path);

// Mode 2s BNMR only
  e2s_rf_pi_on_path = ppg_mode_path+"/pi pulse length (ms)";  //  input_path + "/e2sRF pi on (ms)"
  e2s_rf_halfpi_on_path = ppg_mode_path+"/half-pi pulse length (ms)";  //  input_path + "/e2s RF half-pi on (ms)"
  e2s_select_mode_se2_path = ppg_mode_path+ "/Mode SE2 selected"  ; // input_path + "/e2s select mode" NOTE TRUE value is 1 or 2
                                                                       //                                   while mode param is boolean
  e2s_zdwell_path = ppg_mode_path+ "/Z dwell time (ms)"   ; // input_path + "/e2s Z dwell time (ms)"
  e2s_xdwell_path = ppg_mode_path+ "/X dwell time (ms)"   ; // input_path + "/e2s X dwell time (ms)"
  e2s_nfid_path =  ppg_mode_path+ "/Num FID cycles" ;       // input_path + "/e2s num FID cycles"
  e2s_enable_pi_pulse_path =  ppg_mode_path+ "/Enable pi pulse"; // input_path + "/e2s enable pi pulse"
  e2s_pi_pulse_cycle_num_path =  ppg_mode_path+ "/Pi pulse cycle num"; // input_path + "/e2s pi pulse cycle num"
  e2s_xy_flip_half_pulse_path =  ppg_mode_path+ "/xy flip half pulse (ms)"; 

  e2w_central_freq_path = ppg_mode_path+ "/central freq f0 (khz)";  // input_path + "/e2w central freq f0 (kHz)"
  e2w_freq_sweep_path =  ppg_mode_path+ "/Frequency sweep (kHz)";
  e2w_li_precession_path = ppg_mode_path+ "/Li precession f1 (kHz)";
  e2w_n_path =  ppg_mode_path+ "/N (40 or 80)";
  e2w_q0_path =  ppg_mode_path+ "/Q0 (default 5)";
  e2w_freq_res_path =  ppg_mode_path + "/freq resolution Nf";
  e2w_tot_freq_scans_path =  ppg_mode_path+ "/Total number of frequency scans";
  e2w_tp_path  =  ppg_mode_path+ "/Tp (ms)";

     // For e2s assign phase correction paths
  e2s_qpc3_path = psm_path +"/phase/e2s 3f phase correction QPC3";  // true path
  e2s_qpc5_path = psm_path +"/phase/e2s 5f phase correction QPC5" ; // true path
  e2s_qpcf_path = psm_path +"/phase/e2s fREF phase correction QPCF";  // true path

     // For e1w assign paths
  e1w_f1_function_path = ppg_mode_path+ "/f1 frequency function"; // input_path + "e1w f1 function"
  e1w_f2_function_path = ppg_mode_path+ "/f2 frequency function"; // input_path + "e1w f2 function"
  e1w_f3_function_path = ppg_mode_path+ "/f3 frequency function"; // input_path + "e1w f3 function"
  e1w_f4_function_path = ppg_mode_path+ "/f4 frequency function"; // input_path + "e1w f4 function"
  e1w_x_start_path =     ppg_mode_path+ "/parameter X start";     // input_path + "e1w x start"
  e1w_x_stop_path  =     ppg_mode_path+ "/parameter X stop";      // input_path + "e1w x stop"
  e1w_x_incr_path  =     ppg_mode_path+ "/parameter X incr";      // input_path + "e1w x incr"

 if(gbl_debug)document.getElementById('gdebug').innerHTML+="ending (get_exp_specific_param_paths)";
  return;
}

function write_exp_specific_params(debug,ppg_mode)
{
    // Mode 2s 12 items  "e2s z dwell time (ms)",  "e2s x dwell time (ms)", "e2s num fid cycles"
    //                  "e2s  rf pi on (ms)", "e2s rf half-pi on (ms)" and  "e2s select mode"  
    //                  "e2s enable pi pulse", "e2s pi pulse cycle number" "e2s xy flip half pulse (ms)"
    //                   3f, 5f and fREF phase corrections (in PSM area)
    //                 
    //           

    text="";
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="write_exp_specific_params starting with ppg_mode="+ppg_mode;

    // alert('get_exp_specific_param_paths: starting with ppg_mode='+ppg_mode);
    pattern2 = /2s/;
    if (pattern2.test(ppg_mode)  )
	{
	    
	    // 1.  z dwell time ms
	    //        e2s_zdwell_ms is global
	    
	    e2s_zdwell_ms = parseFloat(CustomData.hidden.current_mode["z dwell time (ms)"] );
	    if(e2s_zdwell_ms  == undefined)
		alert(' e2s_zdwell_ms is undefined ( CustomData.hidden.current_mode["z dwell time (ms)"] ) ');
	    
	    text+= '<td class="param_float">Z dwell time (ms)</td>';
			
	    text+= '<td>';
	    if(rstate==state_stopped)
		{
		    text+= '<a href="#" onclick="myODBEdit(e2s_zdwell_path, e2s_zdwell_ms, update_ppg)" >';
		    text+= roundup(e2s_zdwell_ms , 4); // show 4 dec places
		    text+= '</a>';
		    text+= '' ; 
		}
	    else
		text+=e2s_zdwell_ms ;
	    text+= '</td>'
		if(debug)
		    text = add_truevalue(text,  e2s_zdwell_ms  ,  EqData.frontend.input["e2s z dwell time (ms)"] ); // true_mcs_en_gate_path)
	    text += '</tr>'  ; 
	    

	    // 2.  X dwell time ms
	    //           e2s_xdwell_ms is global 
	    e2s_xdwell_ms  = parseFloat(CustomData.hidden.current_mode["x dwell time (ms)"] );
	    if(  e2s_xdwell_ms  == undefined)
		alert(' e2s_xdwell_ms   is undefined ( CustomData.hidden.current_mode["x dwell time (ms)"] ) ');
	    
	    text+= '<tr>';
	    text+= '<td class="param_float">X dwell Time/2 (ms)</td>';
	    
	    text+= '<td>';
	    if(rstate==state_stopped)
		{
		    text+= '<a href="#" onclick="myODBEdit( e2s_xdwell_path,  e2s_xdwell_ms , update_ppg )" >';
		    text+= roundup(e2s_xdwell_ms , 5); // show 5 dec places
		    text+= '</a>';
		    text+= '' ; 
		}
	    else
		text+= e2s_xdwell_ms  ;
	    text+= '</td>';
	    if(debug)
		text = add_truevalue(text,   e2s_xdwell_ms    , EqData.frontend.input["e2s x dwell time (ms)"]); // true_e1b_dwell_time_path)
	    text += '</tr>';   
   
	    // 3. Half Pi ms       e2s_rf_half_pi_on_ms is  global 
	    e2s_rf_half_pi_on_ms  =  parseFloat( CustomData.hidden.current_mode["half-pi pulse length (ms)"]);  

	    if(e2s_rf_half_pi_on_ms == undefined)
		alert('e2s_rf_half_pi_on_ms is undefined ( CustomData.hidden.current_mode["half-pi pulse length (ms)"] ) ');
            
	    text+= '<tr>';
	    text+= '<td  class="param_float">Enter half-pi pulse length (ms)';
	    text+='</td>';


	    text+= '<td>';
	    if(rstate==state_stopped)
		{
		    text+= '<a href="#" onclick="myODBEdit(e2s_rf_halfpi_on_path,  e2s_rf_half_pi_on_ms, update_ppg)" >';
		    text+=  roundup( e2s_rf_half_pi_on_ms,5);
		    text+= '</a>';
		    text+= '' ; 
		}
	    else
		text +=e2s_rf_half_pi_on_ms ;
	    text+= '</td>';
	    if(debug)
		text = add_truevalue(text, e2s_rf_half_pi_on_ms  , EqData.frontend.input["e2s rf half-pi on (ms)"] ); // true_rf_on_time_path
	    text += '</tr>';   

	    // 4. e2s select mode  
	    //  e2s_select_mode_se2 is global
	    e2s_select_mode_se2  = get_bool(  CustomData.hidden.current_mode["mode se2 selected"]); // current mode parameter
 
	    //   alert('write_all_params:   e2s_select_mode_se2='+e2s_select_mode_se2);
	    if( e2s_select_mode_se2 == undefined)
		alert('e2s_select_mode_se2 is undefined  (CustomData.hidden.current_mode["mode 2se select (1 or 2)"]) ');

	    text+= '<tr>'; 
	    text+= '<td class="param">Enable Mode 2</td>'; 
	    
	    text+= '<td id=se2>'; 
	    if(rstate==state_stopped)
		{
		    //  text+= '<a href="#" onclick="myODBEdit( e2s_select_mode_se2_path  ,  e2s_select_mode_se2 , update_ppg)" >' 
		    text+= '<input  name="box7"  type="checkbox"  onClick="set_se2( this.checked?\'1\':\'0\')">';
		    // text+=   e2s_select_mode_se2;
		    text+= '</a>' ;
		    text+= ''  ; 
		}
	    else
		text+= e2s_select_mode_se2 ;
	    text+= '</td>' ;
	    if(debug)
		text = add_truevalue(text, e2s_select_mode_se2 , EqData.frontend.input["e2s select mode"]); // true value of this parameter)
	    text += '</tr>';
	    
	    // 5. Enable pi pulse
         // e2s_enable_pi_pulse is global

	    e2s_enable_pi_pulse  = get_bool(  CustomData.hidden.current_mode["enable pi pulse"]); // current mode parameter
 
	    //   alert('write_all_params: e2s_enable_pi_pulse ='+e2s_enable_pi_pulse);
	    if( e2s_enable_pi_pulse == undefined)
		alert('e2s_enable_pi_pulse is undefined  (CustomData.hidden.current_mode["enable pi pulse"]) ');

	    text+= '<tr>' ;
	    text+= '<td class="param">Enable pi pulse? </td>'; 

	    text+= '<td id=enpi>'; 
	    if(rstate==state_stopped)
		{
		    text+= '<input  name="box8"  type="checkbox"  onClick="enable_pi( this.checked?\'1\':\'0\')">';
		    text+= '</a>' ;
		    text+= ''  ; 
		}
	    else
		text+=  e2s_enable_pi_pulse  ;
	    text+= '</td>' ;
	    if(debug)
		text = add_truevalue(text,  e2s_enable_pi_pulse  , EqData.frontend.input["e2s enable pi pulse"]); // true value of this parameter)
	    text += '</tr>';
	    
	    
	    if(e2s_enable_pi_pulse)
		{
		    // enable pi pulse mode only
		    
		    // 6.  Pi On ms   e2s  rf pi on (ms)  NOT USED if  e2s_select_mode_se2 is true
		    // e2s_rf_pi_on_ms is global
		    e2s_rf_pi_on_ms  = parseFloat( CustomData.hidden.current_mode["pi pulse length (ms)"]  );
		    if( e2s_rf_pi_on_ms == undefined)
			alert('e2s_rf_pi_on_ms is undefined  (CustomData.hidden.current_mode["pi pulse length (ms)"]) ');
		    
		    text+= '<tr>'; 
		    text+= '<td  class="param_float">Enter Pi pulse length in ms</td>'; 
		    
		    text+= '<td>' ;
		    if(rstate==state_stopped)
			{
			    text+= '<a href="#" onclick="myODBEdit(  e2s_rf_pi_on_path ,  e2s_rf_pi_on_ms, update_ppg )" >' ;
			    text+=   e2s_rf_pi_on_ms ;
			    text+= '</a>' ;
			    text+= ''  ; 
			}
		    else
			text+= e2s_rf_pi_on_ms  ;
		    text+= '</td>'; 
		    if(debug)
			text = add_truevalue(text, e2s_rf_pi_on_ms , EqData.frontend.input["e2s rf pi on (ms)"]); // true value of this parameter)
		    text += '</tr>';
      
		} // end enable pi pulse

	    // 7.  number of FID cycles
	    
	    //  e2s_nfid_cycles is global
	    // alert('num fid cycles= '+EqData["mode parameters"]["mode 2s"]["num fid cycles"])
	    e2s_nfid_cycles  = parseInt( CustomData.hidden.current_mode["num fid cycles"]) // ODBGet(num_rf_cycles_path)
		
		if( e2s_nfid_cycles == undefined)
		    alert(' e2s_nfid_cycles is undefined ( CustomData.hidden.current_mode["number of fid cycles"] ) ');


	    text+= '<tr>';
	    text+= '<td class="param">Number of FID cycles (min 2) </td>';

	    text+= '<td>';
	    if(rstate==state_stopped)
		{
		    text+= '<a href="#" onclick="myODBEdit(e2s_nfid_path,   e2s_nfid_cycles, update_ppg)" >';
		    text+= e2s_nfid_cycles ;
		    text+= '</a>';
		    text+= '' ; 
		}
	    else
		text+=  e2s_nfid_cycles;
	    text+= '</td>';
	    if(debug)
		text = add_truevalue(text,   e2s_nfid_cycles  ,  EqData.frontend.input["e2s num fid cycles"] ) ;
	    text += '</tr>';   


 
   // 8. pi pulse cycle num

      //  e2s_pi_pulse_cycle_num is global
      // alert('num fid cycles= '+EqData["mode parameters"]["mode 2s"]["pi pulse cycle num"])
	    e2s_pi_pulse_cycle_num  = parseInt( CustomData.hidden.current_mode["pi pulse cycle num"]); 

      if( e2s_pi_pulse_cycle_num == undefined)
	  alert(' e2s_pi_pulse_cycle_num is undefined ( CustomData.hidden.current_mode["pi pulse cycle num"] ) ');


      text+= '<tr>';
      text+= '<td class="param">Enter pi pulse cycle number (min 3)</td>';
      text+= '<td>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e2s_pi_pulse_cycle_num_path,   e2s_pi_pulse_cycle_num, update_ppg)" >';
	      text+=   e2s_pi_pulse_cycle_num ;
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e2s_pi_pulse_cycle_num;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text,   e2s_pi_pulse_cycle_num  ,  EqData.frontend.input["e2s pi pulse cycle num"] ) ;
      text += '</tr>';   


      // 12. xy flip half pulse ms

      //  e2s_xy_flip_half_pulse_ms is global
      
      e2s_xy_flip_half_pulse_ms  = parseFloat( CustomData.hidden.current_mode["xy flip half pulse (ms)"]); 

      if(  e2s_xy_flip_half_pulse_ms == undefined)
	  alert(' e2s_xy_flip_half_pulse_ms  is undefined ( CustomData.hidden.current_mode["xy flip half pulse (ms)"] ) ');


      text+= '<tr>';
      text+= '<td class="param_float">Enter xy flip pulse length/2 (ms)</td>';
      text+= '<td>';
      if(rstate==state_stopped)
      {
	  text+= '<a href="#" onclick="myODBEdit( e2s_xy_flip_half_pulse_path,  e2s_xy_flip_half_pulse_ms , update_ppg)" >';
         text+=   e2s_xy_flip_half_pulse_ms  ;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+=  e2s_xy_flip_half_pulse_ms ;
      text+= '</td>';
    if(debug)
	text = add_truevalue(text,     e2s_xy_flip_half_pulse_ms ,  EqData.frontend.input["e2s xy flip half pulse (ms)"] ) ;
	   text += '</tr>';   



 /* not needed for PSMIII */
    if(!have_psm3)
    {
      // 9. PSM Three-f Phase correction  e2s_qpc3
       e2s_qpc3 = EqData.frontend.hardware.psm.phase["e2s 3f phase correction qpc3"];
       text+= '<tr>';
       text+= '<td class="param_float">PSM 3f phase correction (degrees)</td>';
       text+= '<td>';
       if(rstate==state_stopped)
         {
	     text+= '<a href="#" onclick="myODBEdit(e2s_qpc3_path,  e2s_qpc3, update_ppg);">';
          text+= e2s_qpc3;
          text+= '</a>';
          text+= '' ;
         }
       else
          text+= e2s_qpc3
	      text+= '</td>';
       if(debug)
           text+= '<td class="debug" style="background-color: '+debug_bgcol[0]+'">true</td>'; // this is the true value
       text+= '</tr>';
 


   // 10. PSM Five-f Phase correction  e2s_qpc5
       e2s_qpc5 = EqData.frontend.hardware.psm.phase["e2s 5f phase correction qpc5"];
       text+= '<tr>';
       text+= '<td class="param_float">PSM 3f phase correction (degrees)</td>';
       text+= '<td>';
       if(rstate==state_stopped)
         {
	     text+= '<a href="#" onclick="myODBEdit(e2s_qpc5_path,  e2s_qpc5, update_ppg);">';
          text+= e2s_qpc5;
          text+= '</a>';
          text+= '' ;
         }
       else
	   text+= e2s_qpc5;
       text+= '</td>';
       if(debug)
           text+= '<td class="debug" style="background-color: '+debug_bgcol[0]+'">true</td>'; // this is the true value
       text+= '</tr>';
 



   // 11. PSM Fref Phase correction  e2s_qpcf
    
      if(e2s_select_mode_se2)
        {
	  // ES2 mode only
  
          e2s_qpcf = EqData.frontend.hardware.psm.phase["e2s fref phase correction qpcf"];
          text+= '<tr>';
          text+= '<td class="param_float">PSM fREF phase correction (degrees)</td>';
          text+= '<td>';
          if(rstate==state_stopped)
          {
	      text+= '<a href="#" onclick="myODBEdit(e2s_qpcf_path,  e2s_qpcf, update_ppg);">';
             text+= e2s_qpcf;
             text+= '</a>';
             text+= '' ;
          }
          else
	      text+= e2s_qpcf;
          text+= '</td>'
          if(debug)
             text+= '<td class="debug" style="background-color: '+debug_bgcol[0]+'">true</td>'; // this is the true value
          text+= '</tr>';
        }  // end of e2s_select_mode 2 set
     }  // end of !have_psm3

  } // end of Mode 2s
 


  // Mode 2w x items
  //                 
  //            e2w_central_freq_f0, e2w_freq_sweep,e2w_li_precession_f1, e2w_n, e2w_q0, e2w_freq_res_nf,     e2w_tot_freq_scans  

  pattern2 = /2w/;
  if (pattern2.test(ppg_mode)  )
  {

     // 1.  central freq
      //       e2w_central_freq_f0   is global
   

      //  alert('e2w_central_freq_path or my_path =  "'+my_path+'"' );
      if(0)
	  { //NOT USED  // this is PSM Frequency 
	      e2w_central_freq_f0 = parseInt(CustomData.hidden.current_mode["central freq f0 (khz)"] );
	      if( e2w_central_freq_f0 == undefined)
	  alert(' e2w_central_freq_f0 is undefined ( CustomData.hidden.current_mode["central freq f0 (khz)"] ) ');
    
	      text+= '<td class="param_int">Central Frequency f0 (kHz)</td>';
        
	      text+= '<td>';
	      if(rstate==state_stopped)
		  {
		      text+= '<a href="#" onclick="myODBEdit(e2w_central_freq_path, e2w_central_freq_f0 , update_ppg)" >';
		      text+=e2w_central_freq_f0;
		      text+= '</a>';
		      text+= '' ; 
		  }
	      else
		  text+=e2w_central_freq_f0 ;
	      text+= '</td>';
	      if(debug)
		  text = add_truevalue(text, e2w_central_freq_f0   ,  EqData.frontend.input["e2w central freq f0 (khz)"] ) ;
	      text += '</tr>'    ;


	      // 2.  freq sweep
	      //       e2w_freq_sweep   is global
	      // not needed (bandwidth  )
	      e2w_freq_sweep = parseInt(CustomData.hidden.current_mode["frequency sweep (khz)"] );
	      if( e2w_freq_sweep  == undefined)
		  alert(' e2w_freq_sweep  is undefined ( CustomData.hidden.current_mode["frequency sweep (khz)"] ) ');
	      
	      text+= '<td class="param_int">Frequency sweep (kHz)</td>';
	      
	      text+= '<td>';
	      if(rstate==state_stopped)
		  {
		      text+= '<a href="#" onclick="myODBEdit(e2w_freq_sweep_path,  e2w_freq_sweep, update_ppg)" >';
		      text+= e2w_freq_sweep;
		      text+= '</a>';
		      text+= '' ; 
		  }
	      else
		  text+= e2w_freq_sweep ;
	      text+= '</td>';
	      if(debug)
		  text = add_truevalue(text,   e2w_freq_sweep  ,  EqData.frontend.input["e2w frequency sweep (khz)"] ) ;
	      text += '</tr>'    ;
	      
	  } // if(0) NOT USED
      
      // 2.  Tp (ms)
      //       e2w_tp__ms_   is global
    
      e2w_tp = parseFloat(CustomData.hidden.current_mode["tp (ms)"] );
      if( e2w_tp  == undefined)
	  alert(' e2w_tp  is undefined ( CustomData.hidden.current_mode["tp (ms)"] ) ');
    
      text+= '<td class="param_float">Pulse length of modulation function Tp (ms)</td>';
      
      text+= '<td>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e2w_tp_path,  e2w_tp, update_ppg)" >';
	      
	      text+=  roundup(e2w_tp,3);  // show 3 dec places
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+= e2w_tp ;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text,   e2w_tp  ,  EqData.frontend.input["e2w tp (ms)"] ) ;
      text += '</tr>'    ;
      
      
      // 3.  Li precession f1
      //        e2w_li_precession_f1 is global  and FLOAT
      
      
      e2w_li_precession_f1 = parseFloat(CustomData.hidden.current_mode["li precession f1 (khz)"] );
      if(  e2w_li_precession_f1  == undefined)
	  alert(' e2w_li_precession_f1 is undefined ( CustomData.hidden.current_mode["li precession (khz)"] ) ');
      
      text+= '<td class="param_float">Li Precession Frequency  f1 (kHz)</td>';
      
      text+= '<td>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit( e2w_li_precession_path,   e2w_li_precession_f1, update_ppg)" >';
	      text+=  roundup(e2w_li_precession_f1,3);  // show 3 dec places
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e2w_li_precession_f1 ;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text,   e2w_li_precession_f1  ,  EqData.frontend.input["e2w li precession f1 (khz)"] ) ;
      text += '</tr>'    ;

      
      
      // 4.  N (40 or 80)
      //        e2w_n  is global
      
      
      e2w_n = parseInt(CustomData.hidden.current_mode["n (40 or 80)"] );
      
      if( e2w_n  == undefined)
	  alert('e2w_n is undefined ( CustomData.hidden.current_mode["n (40 or 80)"] ) ');
      
      text+= '<td class="param_int">N (40 or 80)</td>';
      
      text+= '<td>';
      if(rstate==state_stopped)
	  {
	      //  text+= '<a href="#" onclick="myODBEdit(e2w_n_path,  e2w_n, update_ppg)" >';
	      //  text+=  e2w_n;
	      // text+= '</a>';
	      text +='<input type="radio" name="n_radiogroup" value=0  onClick="cs_odbset(e2w_n_path,40)">';
	      text +='<span class="it">40 </span>';
	      text +='<input type="radio" name="n_radiogroup" value =1  onClick="cs_odbset(e2w_n_path,80)">';
	      text +='<span class="it">80 </span>';
	      text +='</td>';
	      
	      
	  }
      else
         text+= e2w_n ;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text,  e2w_n  ,  EqData.frontend.input["e2w n (40 or 80)"] ) ;
      text += '</tr>'    ;
      
      
      
      // 5.  Q0 (default 5)
      //      e2w_q0   is global
      
      
      e2w_q0 = parseInt(CustomData.hidden.current_mode["q0 (default 5)"] );
      if( e2w_q0  == undefined)
	  alert('e2w_q0 is undefined ( CustomData.hidden.current_mode["q0 (default 5)"] ) ');
      
      text+= '<td class="param_int">Q0 (default=5)</td>';
      
      text+= '<td>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit( e2w_q0_path,  e2w_q0, update_ppg)" >';
	      text+= e2w_q0 ;
         text+= '</a>';
         text+= '' ; 
	  }
      else
	  text+= e2w_q0 ;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text,  e2w_q0  ,  EqData.frontend.input["e2w q0 (default 5)"] ) ;
      text += '</tr>'    ;
      
      
      // 6.  Freq resolution Nf
      //       e2w_freq_res_nf   is global
      
      
      e2w_freq_res_n = parseInt(CustomData.hidden.current_mode["freq resolution nf"] );
      if(  e2w_freq_res_n  == undefined)
	  alert('e2w_freq_res_nf is undefined ( CustomData.hidden.current_mode["freq resolution nf"] ) ');
      
      text+= '<td class="param_int">Number of frequencies Nf </td>';
      
      text+= '<td>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e2w_freq_res_path,   e2w_freq_res_n, update_ppg)" >';
	      text+=  e2w_freq_res_n ;
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e2w_freq_res_n ;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text,   e2w_freq_res_n  ,  EqData.frontend.input["e2w freq resolution nf"] ) ;
      text += '</tr>'    ;
      
      
      // 7.  Total number frequency scans
      //       e2w_freq_res_nf   is global
      
      
      e2w_tot_freq_scans = parseInt(CustomData.hidden.current_mode["total number of frequency scans"] );
      if(  e2w_tot_freq_scans  == undefined)
	  alert('e2w_tot_freq_scans is undefined ( CustomData.hidden.current_mode["total number of frequency scans"] ) ');
      
      text+= '<td class="param_int">Total number of frequency scans </td>';
      
      text+= '<td>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e2w_tot_freq_scans_path,   e2w_tot_freq_scans, update_ppg)" >';
	      text+=  e2w_tot_freq_scans ;
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e2w_tot_freq_scans ;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text,   e2w_tot_freq_scans  ,  EqData.frontend.input["e2w total num freq scans"] ) ;
      text += '</tr>'    ;
      
      
  } // end of Mode 2w
  
  // NO LONGER Add daq service time for modes 2s,2w
  if(0)
      { // NOT USED
	  pattern2 = /2[sw]/;
	  if (pattern2.test(ppg_mode)  )
	      {
		  daq_service_time = CustomData.hidden.current_mode["daq service time(ms)"]  // ODBGet( daq_service_time_path)
		      if( daq_service_time   == undefined)
			  alert(' daq_service_time  is undefined ( CustomData.hidden.current_mode["daq service time(ms)"] ) ')
			      
			      var my_class;
		  
		  text+= '<tr>'; 
		  
		  my_class="param_float"  
		      text+= '<td class="'+my_class+'">DAQ service time (ms) </td>'
		      
		      text+= '<td>' 
		      if(rstate==state_stopped)
			  {
			      text+= '<a href="#" onclick="myODBEdit( daq_service_time_path, daq_service_time , update_ppg)" >' 
			      text+=   daq_service_time   ;
			      text+= '</a>' ;
			      text+= ''  ; 
			  }
		      else
			  text+= daq_service_time   ;
		  text+= '</td>' 
		      if(debug)
			  text = add_truevalue(text,  daq_service_time  , EqData.frontend.input["daq service time (ms)"]) // true_daq_service_time_path)
			      
			      text += '</tr>';
	      }
      } // end of if (0)

 


  // Mode  1w   7  items
  pattern1 = /1w/;
  // 1w globals    e1w_f1_function, e1w_f2_function, e1w_f3_function, e1w_f4_function, e1w_x_start, e1w_x_stop, e1w_x_incr;
  if (pattern1.test(ppg_mode)  )
  {
      //1. e1w  Param X start
      e1w_x_start =  CustomData.hidden.current_mode["parameter x start"];
      if(e1w_x_start == undefined)
	  alert('e1w_x_start is undefined ( CustomData.hidden.current_mode["parameter x start"] ) ');
      else
	  e1w_x_start = parseInt(  e1w_x_start);
      text+= '<tr>';
      text+= '<td class="param">Parameter x start</td>';
      text+= '<td>';

      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e1w_x_start_path, e1w_x_start, update_ppg)" >';
	      text+=  e1w_x_start; 
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e1w_x_start;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text, e1w_x_start ,  EqData.frontend.input["e1w x start"]); //"true" path)
      text += '</tr>' ;
      
      
      //2. e1w  Param X stop
      e1w_x_stop =  CustomData.hidden.current_mode["parameter x stop"];
      if(e1w_x_stop == undefined)
	  alert('e1w_x_stop is undefined ( CustomData.hidden.current_mode["parameter x stop"] ) ');
      else
	  e1w_x_stop = parseInt(  e1w_x_stop);
      
      text+= '<tr>';
      text+= '<td class="param">Parameter x stop</td>';
      text+= '<td>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e1w_x_stop_path, e1w_x_stop, update_ppg)" >';
	      text+=  e1w_x_stop; 
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e1w_x_stop;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text, e1w_x_stop ,  EqData.frontend.input["e1w x stop"]); //"true" path)
      text += '</tr>' ;
      
      //3. e1w  Param X incr
      e1w_x_incr =  CustomData.hidden.current_mode["parameter x incr"];
      if(e1w_x_incr == undefined)
	  alert('e1w_x_incr is undefined ( CustomData.hidden.current_mode["parameter x incr"] ) ');
      else
	  e1w_x_incr = parseInt(  e1w_x_incr);
      
      text+= '<tr>';
      text+= '<td class="param">Parameter x increment</td>';
      text+= '<td>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e1w_x_incr_path, e1w_x_incr, update_ppg)" >';
	      text+= e1w_x_incr;
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e1w_x_incr;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text, e1w_x_incr ,  EqData.frontend.input["e1w x incr"]); //"true" path)
      text += '</tr>' ;
      
      
      //4. e1w f1 function
      e1w_f1_function =  CustomData.hidden.current_mode["f1 frequency function"];
      if(e1w_f1_function == undefined)
	  alert('e1w_f1_function is undefined ( CustomData.hidden.current_mode["f1 frequency function"] ) ');
      
      text+= '<tr>';
      text+= '<td class="param">f0ch1 frequency function(x)</td>';
      text+= '<td id=f1func>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e1w_f1_function_path, e1w_f1_function, update_ppg)" >';
	      if(e1w_f1_function == "") e1w_f1_function="empty";
	      text+=  e1w_f1_function;
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e1w_f1_function;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text, e1w_f1_function ,  EqData.frontend.input["e1w f1 function"]); //"true" path)
      text += '</tr>' ;
      
      //5. e1w f2 function
      e1w_f2_function =  CustomData.hidden.current_mode["f2 frequency function"];
      if(e1w_f2_function == undefined)
	  alert('e1w_f2_function is undefined ( CustomData.hidden.current_mode["f2 frequency function"] ) ');
	      
      text+= '<tr>';
      text+= '<td class="param">f0ch2 frequency function(x)</td>';
      text+= '<td id=f2func>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e1w_f2_function_path, e1w_f2_function, update_ppg)" >';
	      if(e1w_f2_function == "") e1w_f2_function="empty";
	      text+=  e1w_f2_function;
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e1w_f2_function;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text, e1w_f2_function ,  EqData.frontend.input["e1w f2 function"]); //"true" path)
      text += '</tr>' ;


      //6. e1w f3 function
      e1w_f3_function =  CustomData.hidden.current_mode["f3 frequency function"];
      if(e1w_f3_function == undefined)
	  alert('e1w_f3_function is undefined ( CustomData.hidden.current_mode["f3 frequency function"] ) ');
      
      text+= '<tr>';
      text+= '<td class="param">f0ch3 frequency function(x)</td>';
      text+= '<td id=f3func>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e1w_f3_function_path, e1w_f3_function, update_ppg)" >';
	      if(e1w_f3_function == "") e1w_f3_function="empty";
	      text+=  e1w_f3_function;
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e1w_f3_function;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text, e1w_f3_function ,  EqData.frontend.input["e1w f3 function"]); //"true" path)
      text += '</tr>' ;
      
      //7. e1w f4 function
      e1w_f4_function =  CustomData.hidden.current_mode["f4 frequency function"];
      if(e1w_f4_function == undefined)
	  alert('e1w_f4_function is undefined ( CustomData.hidden.current_mode["f4 frequency function"] ) ');
      
      text+= '<tr>';
      text+= '<td class="param">f0ch4 frequency function(x)</td>';
      text+= '<td id=f4func>';
      if(rstate==state_stopped)
	  {
	      text+= '<a href="#" onclick="myODBEdit(e1w_f4_function_path, e1w_f4_function, update_ppg)" >';
	      if(e1w_f4_function == "") e1w_f4_function="empty";
	      text+=  e1w_f4_function; 
	      text+= '</a>';
	      text+= '' ; 
	  }
      else
	  text+=  e1w_f4_function;
      text+= '</td>';
      if(debug)
	  text = add_truevalue(text, e1w_f4_function ,  EqData.frontend.input["e1w f4 function"]); //"true" path)
      text += '</tr>'; 
  } // end of Mode 1w


 document.getElementById("PPGparams").innerHTML+=text; 

 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (write_exp_specific_params)"
} // end of function


function init_exp_specific_params(ppg_mode)
{
    // called from initialize_ppg_params
    // e2s select se2 mode 

    if(rstate == state_stopped)
	{
	    if(typeof(document.form1.box7) !='undefined')
		{
		    //  alert('initialize_ppg_params:  at box7   e2s_select_mode_se2 (bool) ='+  e2s_select_mode_se2)
		    document.form1.box7.checked=  e2s_select_mode_se2;  // initialize to the correct value
		    
		} 
	    
	    // e2s enable pi pulse
	    if(typeof(document.form1.box8) !='undefined')
		{
		    //  alert('initialize_ppg_params:  at box8  e2s_enable_pi_pulse (bool) ='+ e2s_enable_pi_pulse )
		    document.form1.box8.checked=  e2s_enable_pi_pulse;  // initialize to the correct value
		    
		} 
	    // e2w N=40 or 80
	    if(typeof(document.form1.n_radiogroup)  !='undefined')
		{ 
		    // e2w "N" can be 40 or 80
		    if(parseInt(e2w_n) == 40)
			document.form1.n_radiogroup[0].checked= 1;
		    else if (parseInt(e2w_n) == 80)
			document.form1.n_radiogroup[1].checked= 1;
		    else
			{
			    alert('Illegal value of N ('+e2w_n+'). Setting N to 40');
			    cs_odbset(e2w_n_path,40);
			    document.form1.n_radiogroup[0].checked= 1;
			}
		}
	}
    else
	{ // running
	    if(document.getElementById("se2") != undefined)   
		{
		    if(  e2s_select_mode_se2)
			document.getElementById("se2").innerHTML = "SE2 selected" ;
		    else
			document.getElementById("se2").innerHTML = "SE1 selected"; 
		}
	}
    
    return;
}

//-------------------------------------------------------------------------
// The following  functions only used in BNMR

function set_se2(val)
{
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_se2</b>: starting "

    val=parseInt(val);
    document.form1.box7.checked=val; // initialize to correct value
    gbl_code=1; // for update
    cs_odbset(e2s_select_mode_se2_path , val) // and call update() to build_ppg_params();
 
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (set_se2) with a call to update (1) "
 update();
}

function enable_pi(val)
{
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>enable_pi</b>: starting "

    val=parseInt(val);
    document.form1.box8.checked=val; // initialize to correct value
    gbl_code=1; // for update
    cs_odbset(e2s_enable_pi_pulse_path , val) // and call update() to build_ppg_params();
 
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (enable_pi) with a call to update (1) "
 update();
}

