// Specific parameters for bnqr used by cs_functions.js, parameter_functions.js

// Used in Custom_Status page
//
// mode1Names includes combined modes

var mode1Names=new Array("Scalers","1a","1b","Camp","Freq","NaCell","LCR","Fast");
var mode1Info=[
                 "Mode 10; nothing is scanned","","",
                 "Mode 1c: CAMP variable scanned",
                 "Mode 1f: RF scanned",
                 "Mode 1n: NaCell voltage scanned",
                 "Mode 1h: 1f mode with Alpha counters",
                 "Mode 1j: combination of Modes 20 and 1c (CAMP scan)",
                 "Mode 1g: combination of Modes 20 and 1f (RF scan)"
                ];
var num_type1_modes=6;  // 2 modes (LCR,FAST) are combined modes. Add new type1 modes in list before LCR,Fast

var mode2Names=new Array("SLR","2a","2b","2c","2d","2e","2f","Alpha2"); // Alpha is 2h

var mode2Info=[
                 "Mode 20: Select no RF or fixed freq","","","","",
                 "RF scanned (loads frequency table)",
                 "not tested; do not use",
                 "Mode 20 with ALPHA inputs histogrammed"
                 ];
// End of custom_status page modes


// Used in parameters page
// BNQR Hardware
var  psm_module="PSM";
var have_psm3=0;
var have_psm2=0;

// Modes for BNQR
// only those PPG Modes with special names are listed
var ppg_mode_array = ["10","1c","1d","1f","1g","1n","1j","1h","20","2h"];
var ppg_mode_names = ["Scalers","Camp Scan","Laser Scan","Frequency Scan","Fast","NaCell Scan","LCR","Alpha1","SLR","Alpha2"];

// BNQR mode-specific
var  e1f_alpha_histos_path;
var e1f_alpha_histos;