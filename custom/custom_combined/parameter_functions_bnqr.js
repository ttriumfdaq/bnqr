// parameter_functions.js
//
// calls functions from params_common.js  i.e. poll(). set_poll_timer(),  set_load_timer(), check_perlscript_done().

// json data from ODB
var EqData;  //  //  /Equipment/FIFO_acq/   
var CustomData;  //  /Custom/
var TunesData;   //  /tunes/
var CustomScriptData; // /Customscript/
var RunInfoData; //  /Runinfo
var ExperimData; // Experiment
var PSM;  // psm subdir 

var progressFlag=0
    var updatePeriod = 5000; // partial update time in msec  (default)
    //  var updatePeriod = 10000; // in msec  (default)
// var updatePeriod=0; // do not update
var updateTimerId = 0;
var update_time_s = 0; // page update time is now set by updatePeriod in parameter_functions.js

// gbl_code can be set to one of :
var update_none=0; // load() rebuilds neither; user has not pressed a button
var update_ppg=1;  // load() rebuilds ppg parameters ; ppg params have changed
var update_psm=2;  // load() rebuilds psm parameters ; psm params have changed
var update_all=3;  // load() rebuilds both psm and ppg parameters; both have changed

var progress_last=100
var progress_last_index=0
var remember_progress;
var progress_init=0
var progress_callback=1 // waiting for callback (read data + others)
var progress_got_callback=11 // got callback (read data + others)
var progress_build_psm=2  // build_psm
var progress_build_psm_done=12; // build_psm_done
var progress_build_ppg=4   // build_ppg_params was progress_load_all
var progress_build_ppg_done=14;  // build_ppg_params finished 
var progress_load=5; // load_all or load()
var progress_load_done=15; // load_all or load finished


var progressFlag=progress_init;

var progress_msg_callback=3; // write last message waiting for callback
var progress_msg_got_callback=13; // got callback from write last message

var progress_poll_callback=6; // waiting for poll callback
var progress_poll_got_callback=16; // got poll callback

var progress_write_callback=7; // waiting for callback from db_paste
var progress_write_got_callback=17; // got  callback from db_paste

var progressPeriod = 250; // in msec  default -> set to ODB value update_time_s
var progressTimerId = 0;

var beam_mode, pulse_pairs, freq_mode, camp_dev;
var dual_channel_mode, hel_flip,randomize;
var e2a_bin_param,  hel_sleep_ms , num_cps;
var min_daq_service_time,  daq_service_time;
var gbl_code=3; // default - rebuild all in load
var na_start_v, na_stop_v, na_inc_v, e1f_num_bins, camp_start,  camp_stop,  camp_inc;
var camp_dev, camp_port, camp_ins, camp_scan, camp_units, camp_max, camp_min, camp_factor;
var e1b_dwell_time_ms, mcs_en_gate_ms, mcs_en_delay_ms, rf_off_time_ms, num_rf_cycles, rf_delay_ms;
    var  e00_prebeam_dt,  e00_beamOn_dt,  e00_beamOff_dt,  RFon_delay_dt,  num_rf_delays,  beam_off_time_ms;
    var  num_beam_precycles,  e2a_en_180,  e2a_en_pulse_pairs, beam_on_dt, e2c_prebeam_on_ms,  e2c_beam_on_ms;
    var  e2c_num_freq,  e2c_ss_width_hz,  e2c_fslice_delay_ms,  e2c_flip_180_delay_ms,  e2c_flip_360_delay_ms;
    var bg_delay_ms,  e2c_counting_mode,  e2e_postrf_dt, e2e_dt_per_freq;
    var  freq_start_hz, freq_stop_hz, freq_inc_hz;

var gbl_callback_flag=0; // set non-zero if callback needed from async_odbset

var debug_bgcol=["blanchedalmond","yellow", "pink","red"]; // for true_value debug. pink not used.




function main()
{
    //alert('main: starting ')

      var my_date= new Date;
      var s=my_date.toString();
      s = s.replace(/ GMT.+/,""); // strip off time zone info
      //  document.getElementById('prld').innerHTML = "Last full reload " + s;

      clearTimeout(updateTimerId);
      if(gbl_debug)document.getElementById('gdebug').innerHTML='<br><span style="color:green">main:starting';

      update_page_time = 0; // clear timer for complete page refresh

      if (updatePeriod > 0)
      {
	 progress();
	 update_time_s = updatePeriod/1000; // convert to seconds
      }
      else
          update_time_s = 0;  // will update once

      update();  // will call load_all as init is false
      write_last_message(); // last midas message
  if(gbl_debug)document.getElementById('gdebug').innerHTML='<br><span style="color:green">main:  ending';
}

function suspend_updates()
{
   clearTimeout(progressTimerId);
   clearTimeout(updateTimerId);

   var my_date= new Date;
   var s=my_date.toString();
   s = s.replace(/ GMT.+/,""); // strip off time zone info
   document.getElementById('suspended').innerHTML='<span style="color:red">Update suspended at '+s+'</span>'
   updateTimerId = setTimeout('updates_suspended()', updatePeriod);
}
function updates_suspended()
{ // updates are suspended, but keep the timer going
  update_page_time += update_time_s; // total time since last update
  if (updatePeriod > 0)  // this will be the short one
    updateTimerId = setTimeout('updates_suspended()', updatePeriod);
}
function restore_updates()
{
    // alert('restoring page updates:  update_page_time='+update_page_time)
    clearTimeout(updateTimerId);
    progress(); // restart progress
    gbl_code=3; // default load psm,ppg
    update(); // restart update
   
}

function wait_then_reload(time_s)
{
    var time_ms=time_s * 1000; // ms
    progress();
    updateTimerId = setTimeout('reload()', time_ms); // full page load in time_s seconds
}

function update()
{
    // gbl_code=0 load() rebuilds neither; user has not pressed a button
    // gbl_code=1 load() rebuilds ppg parameters ; ppg params have changed
    // gbl_code=2 load() rebuilds psm parameters ; psm params have changed
    // gbl_code=3 load() rebuilds both ppg and psm
   
    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:cyan; font-weight:bold">update: starting; gbl_code='+gbl_code+'</span>'


    var my_date= new Date;
    var s=my_date.toString();
    s = s.replace(/ GMT.+/,""); // strip off time zone info

    var n =update.arguments.length
 

    update_page_time += update_time_s; // total time
    if(update_page_time >= update_page_s)  // time for page reload
    {
	reload(); // reload page
    }
    document.getElementById("since").innerHTML = 'Time since last full reload: '+update_page_time+' seconds';

    clearTimeout(updateTimerId);
 clear_click_msg() // clear message for users if interface is slow
  
        //   alert('update: gbl_code='+gbl_code ) 
        read_data();
    
   document.getElementById('LastUpdated').innerHTML = "Last update " + s
   if (updatePeriod > 0)  // this will be the short one
   updateTimerId = setTimeout('update()', updatePeriod);

  if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:cyan; font-weight:bold">update: ending with gbl_code='+gbl_code
}


function progress()
{
    var colour = new Array("black","red",   "blue", "slateblue", "brown", "purple",  "grey",  "fuchsia","black","black");
  //                        0        1        2        3          4        5          6         7         8       9
  //                      init     read      build    write      build    load      poll      write   
  //                               waitfor   psm      msg        ppg      load_all  waitfor   waitfor   
  //                               callback          callback                      callback   callback
  //             
  //                                11       12       13           14      15        16        17
  //                               read got  build   msg         done      done     poll      write 
  //                               callback  psm     got                            got       got
  //                                         done   callback                       callback   callback
    var rlp=new String();
    var color = new Array("silver","black");
  clearTimeout(progressTimerId);

  if(progressPeriod > 0)
        progressTimerId = setTimeout('progress()', progressPeriod);

  if(progressFlag < 10)
      {
     document.getElementById('myProgress').innerHTML +='<span style="color:'+colour[progressFlag]+'">'+progressFlag+'</span>'
     remember_progress=document.getElementById('myProgress').innerHTML;
      }
     else
	 {
	    
             if(progressFlag==progress_last)
	     {
                 progress_last_index++;
                 if(progress_last_index > 1) progress_last_index=0;
                 document.getElementById('myProgress').innerHTML =remember_progress+'<span style="color:'+color[progress_last_index]+'">'+(progressFlag-10) +'</span>'
               
	     }
	     else
	     { 
                remember_progress=document.getElementById('myProgress').innerHTML;
                progress_last_index=0; // black
                document.getElementById('myProgress').innerHTML +='<span style="color:black">'+(progressFlag-10)+'</span>'
	     }
	 }
  progress_last=progressFlag;
}

function read_data()
{
    var paths=[
     "/Equipment/FIFO_acq/",
     "/Custom/",
     "/Tunes/",
     "/Customscript/",
     "/Runinfo",
     "/Experiment",
     ];
  if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:blue;font-weight:bold">read_data: starting</span> '
   progressFlag=progress_callback  // read_data waiting for callback

  var my_date= new Date;
    var s=my_date.toString();
    
     // alert(pattern_date.test(s))
     s = s.replace(/ GMT.+/,""); // strip off time zone info
    
    document.getElementById('LastUpdated').innerHTML = "Last update " + s
   
      
    mjsonrpc_db_get_values(paths).then(function(rpc) {
        document.getElementById('readStatus').innerHTML = 'read_data: status='+rpc.result.status
        document.getElementById('writeStatus').innerHTML = 'read_data:processing data at '+s
        var i;
        var len=rpc.result.status.length
       
      //  document.getElementById('mytest').innerHTML=' get_data:id='+rpc.id
        for ( i=0; i<len; i++)
        {  // check individual status
           if(rpc.result.status[i] != 1) 
              alert('read_data: status error at index ='+i+' path='+paths[i])
        }
      data_callback(rpc); // success
      }).catch(function(error) {
        mjsonrpc_error_alert(error);
       })
 if(gbl_debug)document.getElementById('gdebug').innerHTML+='<span style="color:blue;font-weight:bold"> ...and ending (read_data) <span>'
}

function data_callback(rpc)
{
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>data_callback</b>: starting "
  progressFlag=  progress_got_callback
      //    alert('data_callback')
      // var obj= JSON.parse(data);
    
   EqData=  rpc.result.data[0];
   CustomData=  rpc.result.data[1];
   TunesData=  rpc.result.data[2];
   CustomScriptData=  rpc.result.data[3];
   RunInfoData =  rpc.result.data[4];
   ExperimData =  rpc.result.data[5];
   if(have_psm3)
      PSM = EqData.frontend.hardware.psm;

   get_globals();
   
   if(have_psm3)
       get_enabled_channels();
   else
       get_profile_enabled();
   //alert ('data_callback: calling load')

   load(); // reload the page
  
   // my_test();
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>data_callback</b>: ending "
}



function set_flash_timer(time_s)
{
  if(flash_timerID)
         clearTimeout(flash_timerID);
  flash_timerID =  setTimeout ('flash()', (time_s * 1000));
  return;
}
function flash()
{
  if(document.getElementById('interdiag').className == "inter") 
     document.getElementById('interdiag').className = "retin";
  else if  (document.getElementById('interdiag').className == "retin") 
     document.getElementById('interdiag').className = "inter";
  set_flash_timer(flash_time_s);
  return;
}

function myODBGetMsg(facility, start, n, callback)
{

  var url = ODBUrlBase + '?cmd=jmsg&f='+facility+'&t=' + start+'&n=' + n;
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>myODBGetMsg</b>: starting and ending"
  return ODBCall(url, msg_callback);

}
function write_last_message()
{
  progress_Flag=progress_msg_callback;
  myODBGetMsg("midas",0,1,msg_callback);
}

function msg_callback(msg)
{
 // latest MIDAS (August 2015) - clean off unwanted information
    var pattern=/^\d+ (\d\d:\d\d:\d\d)\.\d+( .+)/
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>msg_callback</b>: starting "
    msg=msg.replace(pattern,"$1 $2"); 
    document.getElementById('lastmsg').innerHTML = msg;
    progressFlag= progress_msg_got_callback // all done
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="... and ending ( msg_callback) "
}

function load()
{

    // alert('load: starting ')
    var done=2;  // values for perlscript_done
    var in_progress=1;
    var error=3;
    var clear=4;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:red; font-weight:bold">load</span>: starting; gbl_code= '+gbl_code

    progressFlag =  progress_load;	    

    clearTimeout(flash_timerID);
    // set_load_timer(update_time_s);  // timer now done in update()
    set_flash_timer(flash_time_s);
 
  

    polling_on_perl = 0; 
    if(rstate == state_stopped)
    {
	document.form1.pcounter.value ++;
	//polling_on_perl= parseInt(ODBGet(poll_on_perl_path)); // make sure it's an integer
          polling_on_perl= parseInt(CustomData.hidden.poll_on_perl_path); // make sure it's an integer
	//  alert('load: pcounter='+  document.form1.pcounter.value+ ' polling_on_perl='+  polling_on_perl);
	  if(polling_on_perl == undefined)
	      alert("polling_on_perl is undefined");
	if(polling_on_perl == 1)
	    {
		//alert("setting poll_timer");
            	document.getElementById("perl").innerHTML="perlscript started!";
                document.getElementById("perl").style.color="green";
		set_poll_timer(poll_time_s);
	    }

    }
    //  alert(' load: init,rstate_changed,ppg_mode_changed= '+ init+rstate_changed+ppg_mode_changed) // does not exist
    if(!init || rstate_changed || ppg_mode_changed)
    {
	//	alert(' load: calling load_all')
	load_all();
        if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:red; font-weight:bold">load: returning after calling load_all'
        return;
    }
    
    if(tip_changed)
    {
	//	alert('tip_changed= '+tip_changed)
       if(tip == 0)  // transition in progress
      	  document.getElementById("tip").innerHTML='';
       else
       {
	   document.getElementById("tip").innerHTML=remember_tip;
           document.getElementById("tip").className="transition";
       }
       if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:red; font-weight:bold">load:returning after tip changed</span>'
    
       return;
    }
    //document.getElementById("tmpmsg").innerHTML+=' ... load has nothing to update';
	
    // this will update page e.g after myODBEdit has been called to change a psm or ppg parameter
     if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:red; font-weight:bold">load</span>: gbl_code= '+gbl_code
     if ((gbl_code==1) || (gbl_code==3))
          build_ppg_params();
     if((gbl_code==2)  || (gbl_code==3))
      	  build_psm_params();
    if(gbl_code > 0)
       check_consistency();
    else
	gbl_code=0; // do nothing further

      progressFlag =  progress_load_done;
 if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:red; font-weight:bold">load</span>:ending'+progressFlag
    return;
}


function load_all()
{ 

    var temp, text;
    var tune_ppgmode;
    var my_color;

    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:maroon; font-weight:bold">load_all: starting</span> '
    progressFlag =  progress_load;
    //   alert('load_all: init= '+init);
    
    if(!init)
        initialize();

	
    setup_ppgmode(); // must be called before everything else   
    setup_rf(); // depends on ppgmode

    write_buttons();
    write_runstate();  // write line beginning run state
   

      write_tunes(); // calls build_tunes  SLOW
   

    write_titles(); // writes ppgmode in title line and parameters line

  
    build_ppg_params();
   
    //alert( 'typeof( document.getElementById("RFonDT") ) =' +typeof(  document.getElementById("RFonDT")));
  
    //  alert('load_all:  calling build_psm_params' ); 
       build_psm_params(); /// SLOW
       //alert("load_all: calling check_consistency")
       check_consistency();
        write_date();
        //update_page_time=0;
    progressFlag =  progress_load_done;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:maroon; font-weight:bold">load_all: ending</span> '
    return;
}

function check_mode()
{
    // set table_driven flag for table driven modes
    var pattern_noload_freqtable=/2[0s]/; // modes do not load a freq table
    var pattern_type1=/1/; // and all Type 1 modes
    if(pattern_noload_freqtable.test(ppg_mode) || pattern_type1.test(ppg_mode))
	table_driven=0;
    else
	table_driven=1;
}


function write_titles()
{ // writes ppgmode in title line and PPG Parameters line
    var text;
    var my_colour;
    // document.getElementById("ppgmode").innerHTML='PPG Mode '+ppg_mode+ppg_name
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>write_titles</b>: starting "
    if(ppgtype==1)
       my_colour="purple"
    else
       my_colour="blue"
    
    text ='&nbsp &nbsp  PPG Mode: &nbsp'
    text +=' &nbsp  <span style=" color:'+my_colour+';">'+ppg_mode+ppg_name+'&nbsp</span> '

    document.getElementById("hdr").innerHTML =  remember_title + text;

 
    if(ppg_mode =="2s")  // supported by BNMR only
	{ 
	    // add calculate button for this mode
         text ='&nbsp &nbsp &nbsp &nbsp'
         text +='<input name="calc2s" value="Calculate" type="button"  style="color:firebrick" onClick="calculate_2s()">';

	}
    // ppgtype
    document.getElementById("ppgmode").innerHTML= remember_ppg_title + ' for PPG Mode '+ppg_mode+ppg_name+text
    if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending  write_titles"
    return;
}


function write_buttons()
{
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>write_buttons</b>: starting "
   if(rstate== state_stopped)
    {
        text  = '<input name="cmd" value="Start" type="submit" onClick="write_click();" >'+remember_buttons
        text += '<input name="customscript" value="rf_config" type="submit" style="color:firebrick" title="click to check parameters; check Messages for result" onClick="write_click();">'
	document.getElementById("buttons").innerHTML=text;
    }
  else // running
     document.getElementById("buttons").innerHTML='<input name="cmd" value="Stop" type="submit"  onClick="write_click();">'+remember_buttons
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending  write_buttons"
   return;
}

function write_runstate()
{
    // also "Show tunes checkbox"
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>write_runstate</b>: starting "
  if(rstate== state_stopped)
  {
	document.getElementById("runstate").innerHTML='Stopped';
        document.getElementById("runstate").className="stopped";
        document.getElementById("showtunes").innerHTML=remember_showtunes;
        document.form1.tunebox0.checked= get_bool(TunesData["hide tunes"]); // get_bool(ODBGet(hide_tunes_path)); 
   }  
   else
   {   // running

	document.getElementById("runstate").innerHTML='Running';
        document.getElementById("runstate").className="running";
        document.getElementById("showtunes").innerHTML=''
   }

   if(tip == 0)  // transition in progress
      	document.getElementById("tip").innerHTML='';
    else
    {
	document.getElementById("tip").innerHTML=remember_tip;
        document.getElementById("tip").className="transition";
    }
   var runnumber=RunInfoData["run number"];
   document.getElementById("runnum").innerHTML=runnumber;
   if (runnumber < 40000)
       document.getElementById("runnum").style.backgroundColor="yellow";
   else
      document.getElementById("runnum").style.backgroundColor="lime";

   document.getElementById("zaher").innerHTML=remember_zaher;
    // ppgmode
    if(hr != undefined)
       document.getElementById("timingdiag").innerHTML='<a href='+hr+' target=\"_blank\">Timing Diag</a>&nbsp';
    else
        document.getElementById("timingdiag").innerHTML="";
    if(hi != undefined)
	{
	    var my_pattern=/2[0h]/;
	    if(my_pattern.test(ppg_mode))
		my_text="Setup"; // sliders
	    else
                my_text="Diagram"; // 2e,2f
	    my_elem='<button type="button" onclick=\"document.location.href='+hi+';">Interactive Timing '+my_text+'</button>'
		//	alert('my_elem='+my_elem);
		// document.getElementById("interdiag").innerHTML='<button type="button" onclick=\"document.location.href='+hi+';">name</button>'
	    document.getElementById("interdiag").innerHTML='Enter parameters using '+my_elem;
            //alert('classname='+ document.getElementById('interdiag').className)
  
	    document.getElementById("2ecalc").innerHTML='Mode '+ppg_mode+' '+my_elem;
	    // alert(' element id 2ecalc= '+    document.getElementById("2ecalc").innerHTML);

	    my_pattern=/2[e0h]/;
	    if(!my_pattern.test(ppg_mode))
       	       document.getElementById("2ecalc").innerHTML="";

	    document.getElementById("20calc").innerHTML=""; // not used


	}
    else
        document.getElementById("interdiag").innerHTML="";
    //    alert(' element id interdiag= '+  document.getElementById("interdiag").innerHTML)
  
   // rf (depends on ppgmode)
    if(have_psm)
    {
       document.form1.gbox0.checked= show_gate_params;  // initialize to the correct value
       document.form1.tbox0.checked= enable_psm_debug_params;  // initialize to the correct value
        var my_pattern=/2e/;
	if(my_pattern.test(ppg_mode))
	    document.getElementById("2ecalc").innerHTML=remember_2ecalc;
        else
	    document.getElementById("2ecalc").innerHTML="";
       my_pattern=/20/;
	if(my_pattern.test(ppg_mode))
	    document.getElementById("20calc").innerHTML=remember_20calc;
        else
	    document.getElementById("20calc").innerHTML="";

    }
    else
       document.getElementById("zaher").innerHTML=''; 

 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (write_runstate)"
    return;
}    



function write_tunes()
{
    // use W3C standard document.getElementById() 
    

    //  var rname_val,cname_val;
    //  var default_rname_val = "NewTuneName"
    //  var default_cname_val = "MyTuneName"

    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>write_tunes</b>: starting "
	if(poll_on_done != 0)   // global
	{   // may be > 1 only when run is stopped 
	    document.getElementById("tuneline1").innerHTML="";
	    document.getElementById("tuneline2").innerHTML="";
	    // alert('write_tunes:cleared tunelines as poll_on_done= '+poll_on_done);
            if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (write_tunes)"
	    return;
	}

    // alert(' document.getElementById("rname").value= '+ document.getElementById("rname").value)
    
    // these will be null if no tunes are defined
    //  if( document.getElementById("rname") != null) 
    //   rname_val= document.getElementById("rname").value; // was  =form1.rname.value incompatible with newer browsers
                                                           // set id=rname rather than name="rname"

    //  if(rname_val === undefined)
    //	    rname_val= default_rname_val;
          
 
     // alert('rname_val='+rname_val+' cname_val= '+cname_val);
  if(rstate== state_stopped)
  {
      //alert('write_tunes: document.form1.tunebox0.checked='+document.form1.tunebox0.checked);

     if (!document.form1.tunebox0.checked)
     {  // Show tune lines
	 

            document.getElementById("tuneline1").innerHTML=remember_tuneline1;
	    document.getElementById("tuneline2").innerHTML=remember_tuneline2;

	    read_tunes();

            tune_ppgmode = TunesData.ppg_mode; //  ODBGet(tunes_ppgmode_path);
            if(tune_ppgmode == undefined)
	       alert(' tune_ppgmode is undefined ( TunesData.ppg_mode)')
            tune_ppgmode=tune_ppgmode.replace(/\n/,""); // strip off extra carriage return

           
            build_tunes(tune_ppgmode);
 
            // if user had typed something in these, restore them
           
	    // alert('document.getElementById("rname")'+ document.getElementById("rname"));
            // these will be null if no tunes are defined
	    //  if( document.getElementById("rname") != null) 
	    //  {
	    //    document.getElementById("rname").value = rname_val // form1.rname.value= rname_val;
	      
		      //  }
     }
     else
     {   // hide tunelines
         
	 // document.getElementById("tuneshdr").innerHTML="";
         document.getElementById("tuneline1").innerHTML="";
         document.getElementById("tuneline2").innerHTML="";
     } 
        
  } // end of stopped  
  else   // running
  {
      document.getElementById("tuneline1").innerHTML=''
      document.getElementById("tuneline2").innerHTML=''
      document.getElementById("showtunes").innerHTML=''
   }
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (write_tunes)"
return;
}    

function write_click()
{ // message for users if interface is slow
 document.getElementById('click').innerHTML='Button clicked! Please wait...'
}

function clear_click_msg()
{   // clear message for users 
    document.getElementById('click').innerHTML='';
}


function myODBEdit_orig(path, code, value)
{
   
   suspend_updates();
   var new_value = prompt('Please enter new value', value);
   if (new_value != undefined) {
        restore_updates();
        ODBSet(path, new_value);
        gbl_code=code; // for load
        update()
        return;
   } 
   restore_updates();
}

function myODBEdit(path, value, code)
{
    var pattern_psm=/psm/i;
    var pattern_ppg=/mode/i;

    //   alert('myODBEdit path='+path+' value= '+value+' code= '+code);
    if(code == undefined)
	{
	    if( pattern_psm.test(path) )
		gbl_code=2; // rebuild psm params
	    else if (pattern_ppg.test(path))
		gbl_code=1; // rebuild ppg params
	    else
		gbl_code=3; // rebuild neither
	}
    else
	gbl_code=code;
    // alert('myODBEdit starting with path='+path+' and value= '+value+' and gbl_code='+gbl_code);

    suspend_updates();
 var new_value = prompt('Please enter new value', value);	

   if (new_value != undefined) {
        restore_updates();

       cs_odbset(path,new_value,update); // update() is a callback

	update();		 
        return;
   } 
   restore_updates();
   return;
}



function setup_ppgmode()
{
    
    // called when ppg_mode has changed (or on load page)

    var i;
    hr = diag[ppg_mode]; // timing diagram
    hi = interdiag[ppg_mode]; // interactive timing diagram
    //  alert('hr='+hr+' hi='+hi)
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>setup_ppgmode</b>: starting ";
    // alert('setup_ppgmode: ppg_mode '+ppg_mode)
    ppg_mode_path='/Equipment/FIFO_acq/mode parameters/Mode '+ppg_mode;  // Input parameters path

    // There should be a link "current_mode" pointing to this.
    // Make sure this link exists by checking for /Equipment/FIFO_acq/mode parameters/current_mode/Flip helicity in EqData

    var fh= CustomData.hidden.current_mode["flip helicity"];// all modes have this
  //  var fh = EqData["mode parameters"].current_mode["Flip helicity"] // all modes have this
  //  Note: had to change current_mode link from above because experim.h saved the directory the link points to
  //  This is not the same size for all modes and caused rf_config to fail on check_record

    if(fh==undefined)
	alert('Problem with CustomData.hidden.current_mode . Cannot find ..current_mode/Flip helicity ');
	    //else
	    //	alert('fh= '+fh)
	    // tune_filepath=tune_path+ppg_mode+'/';
    ppg_name="   ";

      var len1=ppg_mode_array.length;
      var len2=ppg_mode_names.length;
      if(len1 != len2)
	  {
	     alert('setup_ppgmode: Warning... ppg_mode_array and ppg_mode_names  arrays should be the same length!');
             if(len1 > len2) len1=len2;
          }
    // See if this  ppg mode has a special name
    for (i=0; i<len1; i++)
    {
      if (ppg_mode == ppg_mode_array[i])
      {
          ppg_name = ' ('+ppg_mode_names[i]+')';
          break;
      }
    }

    
    if(pattern_1.test(ppg_mode)) 
	ppgtype=1;
    else if(pattern_2.test(ppg_mode)) 
	ppgtype=2;
    // alert ('ppgtype '+ppgtype)

    // assign variables that depend on ppg_mode_path

    // Mode parameter path         equivalent to            Input parameter path
    //------------------------------------------------------------------------------------------------------

    hel_flip_path = ppg_mode_path + "/flip helicity";        //   hel_flip_path = hardware_path + "/Enable helicity flipping"; // checkbox box0
    hel_sleep_path = ppg_mode_path+"/helicity sleep time (ms)"; //  hel_sleep_path = hardware_path+"/helicity flip sleep (ms)"
    cps_path = ppg_mode_path +"/Num cycles per scan incr";      //  cps_path = input_path +"/num cycles per supercycle"

    freq_start_path = ppg_mode_path + "/Frequency Scan Start (Hz)";     //  freq_start_path =input_path + "/frequency start (Hz)"
    freq_stop_path = ppg_mode_path + "/Frequency Scan Stop (Hz)";       //  freq_stop_path =input_path + "/frequency stop (Hz)"
    freq_inc_path = ppg_mode_path + "/Frequency Scan Increment (Hz)";   //  freq_inc_path =input_path + "/frequency increment (Hz)"	
    randomize_path = ppg_mode_path + "/Randomize Freq Scan Increments"; //  randomize_path = input_path + "/randomize freq values"  // checkbox box3

    beam_mode_path =  ppg_mode_path + "/beam mode";             //  beam_mode_path = input_path + "/beam_mode"
    pulse_pairs_path = ppg_mode_path + "/pulse pairs";          //  pulse_pairs_path = input_path + "/e1a and e1b pulse pairs"
    freq_mode_path = ppg_mode_path + "/frequency mode";         //  freq_mode_path = input_path + "/e1a and e1b freq mode"
    bg_delay_path =  ppg_mode_path + "/Background delay (ms)";  //   bg_delay_path = input_path + "/Bg delay (ms)" 
    rf_delay_path = ppg_mode_path + "/RF delay (ms)";           //  rf_delay_path = input_path+ "/RF delay (ms)"
    num_rf_cycles_path = ppg_mode_path+"/Number of RF cycles";  //  num_rf_cycles_path = input_path+"/Num RF cycles"
    rf_on_time_path = ppg_mode_path+"/RF on time (ms)";         //  rf_on_time_path = input_path+"/RF on time (ms)"
    rf_off_time_path = ppg_mode_path+"/RF off time (ms)" ;       //  rf_off_time_path = input_path+"/RF off time (ms)"
    mcs_en_delay_path = ppg_mode_path+"/MCS enable delay (ms)";  //  mcs_en_delay_path = input_path+"/MCS enable delay (ms)"

//      Dwell time/bin width/enable mcs gate 
    mcs_en_gate_path = ppg_mode_path +"/MCS enable gate (ms)";   //  mcs_en_gate_path = input_path +"/MCS enable gate (ms)" // (bin width/dwelltime)

    e1b_dwell_time_path = ppg_mode_path+"/Dwell time (ms)";      //  e1b_dwell_time_path = input_path+"/E1B Dwell time (ms)"
    na_start_path =  ppg_mode_path +"/Start NaCell scan (Volts)";//  na_start_path = input_path +"/NaVolt start"
    na_stop_path =  ppg_mode_path +"/Stop NaCell scan (Volts)";  //  na_stop_path = input_path +"/NaVolt stop"
    na_inc_path =  ppg_mode_path +"/NaCell Increment (Volts)";  //  na_inc_path = input_path +"/NaVolt inc"
    e1f_num_bins_path = ppg_mode_path +"/Number of bins";        //  e1f_num_bins_path = input_path +"/e1f num dwell times"

    camp_start_path =ppg_mode_path+"/Camp start scan";   //  camp_start_path =input_path+"/e1c Camp start"
    camp_stop_path =ppg_mode_path+"/Camp stop scan";     //  camp_stop_path =input_path+"/e1c Camp stop"
    camp_inc_path =ppg_mode_path+"/Camp increment";      //  camp_inc_path =input_path+"/e1c Camp inc"

    camp_dev_path = ppg_mode_path+"/Camp path";              // camp_inc_path=  camp_sweep_device_path+"/Camp path";
    camp_port_path = ppg_mode_path+"/GPIB port or rs232 portname"; //camp_port_path =  camp_sweep_device_path+"/GPIB port or rs232 portname"; 
    camp_ins_path =  ppg_mode_path+"/Instrument Type";  //camp_ins_path =  camp_sweep_device_path+"/GPIB port or rs232 portname";
    camp_scan_path =  ppg_mode_path+"/Camp scan path";  //camp_scan_path =  camp_sweep_device_path+"/Camp scan path";
    camp_units_path = ppg_mode_path+"/Scan units";       //camp_units_path =  camp_sweep_device_path+"/Scan units";
    camp_max_path = ppg_mode_path+"/maximum";            //camp_max_path =  camp_sweep_device_path+"/maximum";
    camp_min_path = ppg_mode_path+"/minimum";            //camp_min_path =  camp_sweep_device_path+"/minimum";
    camp_factor_path = ppg_mode_path+"/integer conversion factor"; //camp_factor_path =  camp_sweep_device_path+"/integer conversion factor";


    e00_prebeam_dt_path =ppg_mode_path+"/Number of prebeam dwelltimes";  //  e00_prebeam_dt_path =input_path+"/e00 prebeam dwelltimes";
    e00_beamOn_dt_path =ppg_mode_path+"/Number of beam on dwelltimes";   //  e00_beamOn_dt_path =input_path+"/e00 beam on dwelltimes";
    e00_beamOff_dt_path =ppg_mode_path+"/Number of beam off dwelltimes"; //  e00_beamOff_dt_path =input_path+"/e00 beam off dwelltimes";

    RFon_delay_dt_path =ppg_mode_path+"/RFon Delay (dwelltimes)";       //  RFon_delay_dt_path =input_path+"/RFon dwelltime";
    RFon_duration_dt_path=ppg_mode_path+"/RFon duration (dwelltimes)";  //  RFon_duration_dt_path=input_path+"/rfon duration (dwelltimes)";
    num_rf_delays_path =ppg_mode_path+"/Num RF On Delays (Dwelltimes)";  //   num_rf_delays_path =input_path+"/num RF on delays (dwell times)"
    beam_off_time_path=ppg_mode_path+"/beam off time (ms)" ;            //   beam_off_time_path=input_path+"/beam off time (ms)"
    num_beam_precycles_path=ppg_mode_path+"/Number of Beam precycles";  //   num_beam_precycles_path=input_path+"/num beam precycles"

    e2a_en_180_path =ppg_mode_path+"/enable 180" ;                      //   e2a_en_180_path =input_path+"/e2a 180"
    e2a_en_pulse_pairs_path =ppg_mode_path+"/Enable Pulse Pairs" ;      //   e2a_en_pulse_pairs_path =input_path+"/e2a pulse pairs"
    e2a_bin_param_path=ppg_mode_path+"/Bin parameter" ;                 //   e2a_bin_param_path=input_path+"/e2a ubit1 action"
    beam_on_dt_path=ppg_mode_path+"/number of beam on dwelltimes";      //   beam_on_dt_path=input_path+"/e2b num beam on dwell times"
	 //e2b_num_postRF_dt_path=ppg_mode_path+"/Num postRF dwelltimes" //   e2b_num_postRF_dwelltimes_path=  input_path+"/e2b num postRF dwelltimes" //NEW PARAM
    prebeam_on_path =ppg_mode_path+"/prebeam on time (ms)" ;        //   e2c_prebeam_on_path =input_path+"/prebeam on time (ms)"
    e2c_beam_on_path =ppg_mode_path+"/beam on time (ms)"  ;             //   e2c_beam_on_path =input_path+"/e2c beam on time (ms)"
    e2c_rf_on_path =ppg_mode_path+"/RF On time (ms)";                  //   e2c_rf_on_path =input_path+"/f select pulselength (ms)"
    e2c_num_freq_path =ppg_mode_path+"/Number of frequency slices";          //   e2c_num_freq_path =input_path+"/Num freq slices"
    e2c_ss_width_path =ppg_mode_path+"/freq single slice width (Hz)" ;       //   e2c_ss_width_path =input_path+"/freq single slice width (Hz)"
     e2c_fslice_delay_path =ppg_mode_path+"/freq single slice int delay(ms)" //   e2c_fslice_delay_path =input_path+"/f slice internal delay (ms)"
	 e2c_flip_180_delay_path =ppg_mode_path+"/flip 180 delay (ms)";           //   e2c_flip_180_delay_path =input_path+"/flip 180 delay (ms)"
     e2c_flip_360_delay_path =ppg_mode_path+"/flip 360 delay (ms)" ;          //   e2c_flip_360_delay_path =input_path+"/flip 360 delay (ms)"
     e2c_counting_mode_path =ppg_mode_path+"/counting mode" ;                 //   e2c_counting_mode_path =input_path+"/counting_mode"
     e2e_dt_per_freq_path =ppg_mode_path+"/num dwelltimes per freq";          //   e2e_dt_per_freq_path =input_path+"/e2e num dwelltimes per freq"
     e2e_postrf_dt_path =ppg_mode_path+"/num post RFbeamOn dwelltimes"       //   e2e_postrf_dt_path =input_path+"/e2e num postRFbeamOn dwelltimes"
     e1f_const_cycle_path = ppg_mode_path +"/const. time between cycles"  // checkbox5 //   input_path +"/e1f const time between cycles"


     daq_service_time_path =  ppg_mode_path + "/DAQ service time(ms)";
     sampleref_mode_path = ppg_mode_path + "/Enable Sample Reference mode";// hardware_path + "/enable SampleRef mode"; 

   if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (setup_ppg_mode) "		      
}



function setup_rf()
{
   // RF used by modes  1a 1b 1f 1g 1w 20 2a 2b 2c 2d 2e 2h 2s 2w
   pattern1 = /1[abfgw]/;
   pattern2 = /2[0abcdefhsw]/;
   have_psm=0; // default no rf

   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>setup_rf</b>: starting "
   if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode)  )
   {   
      have_psm = 1;
       
      // enable_psm_debug_params, show_gate_params are globals

      enable_psm_debug_params=get_bool(CustomData.hidden["enable test parameters"]); // tpath = "/custom/hidden/enable test parameters";
    
      show_gate_params=get_bool(CustomData.hidden["show gate parameters"]); // gpath = "/custom/hidden/show gate parameters";
   }
   if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (setup_rf)"
}


function isEmpty(obj){
return (Object.getOwnPropertyNames(obj).length === 0);
}


function build_ppg_params()
{
    var my_pattern1,my_pattern2;
   var ppg_debug =  ppg_debug=document.form1.pbox0.checked; 
   progressFlag=progress_build_ppg;
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>build_ppg_params</b>: starting "

   
   document.getElementById("PPGparams").innerHTML =""; // clear to start with

   if(ppg_debug)
   {
      text= '<tr id="debug_pt" class="debug_r">'
      text+= '<th>Parameter Name</th><th>Mode value</th><th>True value'
      text+= '<input name="trueinfo" value="Notes" type="button"  style="color:firebrick" onClick=" show_notes();">'

      text+= ' </th></tr>' // PPG Debug Title 
     document.getElementById("PPGparams").innerHTML= text;
   }

   write_all_params(ppg_debug, ppg_mode); // checks mode before writing
 
    if(bnqr_expt)
	{     // BNQR-specific parameters for Modes 2h.. (bnqr only) 
	    my_pattern1=/??/;
	    my_pattern2=/2[h]/;
	    if( my_pattern1.test(ppg_mode) || my_pattern2.test(ppg_mode))
		{
		    // alert('build_ppg_params:  bnqr_expt='+bnqr_expt+ ' calling get_exp_specific_params and paths');
		    get_exp_specific_param_paths(ppg_mode_path);
		    write_exp_specific_params(ppg_debug,ppg_mode); // mode params for bnqr only, in bnqr_mode_params.js
		}
	}
   write_freq_scan_params(ppg_debug, ppg_mode);
   write_common_params(ppg_debug);
   write_type1_common_params(ppg_debug, ppgtype);  // checks mode is type 1 before writing

   initialize_ppg_params(ppg_debug);

  // Add a warning for Mode 2b using "interdiag" element
   my_pattern2=/2b/;
   if(my_pattern2.test(ppg_mode))
      document.getElementById("interdiag").innerHTML="WARNING This mode (2b) does not appear to be supported properly by the frontend";
 
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>build_ppg_params<b> ending "
    progressFlag=progress_build_ppg_done
}

function initialize_ppg_params(ppg_debug)
{
   var pattern_p=/P/;  
   var pattern_c=/C/;
   var pattern_000=/000/;  
   var pattern_180=/180/;
   var pattern_FR=/FR/;
   var pattern_F0=/F0/;

   var pattern_GE=/GE/; // general camp device

   var pattern_pairs=/pairs/;  
   var pattern_1st=/1st/;
   var pattern_2nd=/2nd/;
   var pattern_diff=/diff/;

 
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>initialize_ppg_params</b>: starting "
   //alert('initialize_ppg_params starting with hel_flip '+hel_flip);

   if(rstate == state_stopped)
   {
       // Helicity Flip
       if(typeof(document.form1.box0) !='undefined') // doesn't like a space i.e. = 'undefined'
      {
            document.form1.box0.checked= hel_flip;  // hel_flip is boolean initialize to the correct value

            // helicity sleep time ignored in dual channel mode
            if ( dual_channel_mode  ||  ! document.form1.box0.checked)
            {
                remember_hel_sleep = document.getElementById("hel_sleep").innerHTML;
                document.getElementById("hel_sleep").innerHTML="";
             }
      }

      // Dual Channel Mode
      if(typeof(document.form1.box1) !='undefined')
	  {
		document.form1.box1.checked= dual_channel_mode;   // initialize to the correct value
          }

      if(0)  // GONE  

      {  // use default midbnmr params
         if(typeof(document.form1.box2) !='undefined')
            document.form1.box2.checked= get_bool(ODBGet(def_midbnmr_path));  // initialize to the correct value
      }


      // Randomize frequency values
      if(typeof(document.form1.box3) !='undefined')
         document.form1.box3.checked= get_bool(randomize);  // initialize to the correct value

       // Beam Mode
    
      if(typeof(document.form1.bmradiogroup) !='undefined')
      {
	  //alert('typeof(document.form1.bmradiogroup)='+typeof(document.form1.bmradiogroup))
  	  // beam_mode is now global
        document.form1.bmradiogroup[0].checked=  pattern_p.test(beam_mode)
        document.form1.bmradiogroup[1].checked=  pattern_c.test(beam_mode)
     }
     // Pulse Pairs
     if(typeof(document.form1.ppradiogroup) !='undefined')
     {
       //  pulse_pairs    global
        document.form1.ppradiogroup[0].checked=  pattern_000.test(pulse_pairs)
        document.form1.ppradiogroup[1].checked=  pattern_180.test(pulse_pairs)
     }

     // Frequency Mode
     if(typeof(document.form1.fmradiogroup) !='undefined')
     {
        // freq_mode global
        document.form1.fmradiogroup[0].checked=  pattern_FR.test(freq_mode)
        document.form1.fmradiogroup[1].checked=  pattern_F0.test(freq_mode)
     }

     // e1f_external_start   not used
   //  if(typeof(document.form1.box4) !='undefined')
    //     document.form1.box4.checked= get_bool(ODBGet(e1f_ext_start_path));  // initialize to the correct value


     // e1f_constant_cycle   
     if(typeof(document.form1.box5) !='undefined')
         document.form1.box5.checked= get_bool(e1f_const_cycle);  // initialize to the correct value

  // e1f alpha histos 
     if(typeof(document.form1.box9) !='undefined')
        document.form1.box9.checked= get_bool(e1f_alpha_histos); // initialize to the correct value
     // alert('initialize_ppg_params:	sampleref_mode='+sampleref_mode)



    // e20 sample/reference mode 
     if(typeof(document.form1.box6) !='undefined')
	 {
     document.form1.box6.checked=sampleref_mode ;  // initialize to the correct value
     // alert('initialize_ppg_params:	sampleref_mode='+sampleref_mode)
 }    
     // Bin Params
    

     if(typeof(document.form1.bpradiogroup) !='undefined')
     {
        // e2a_bin_param is global
        document.form1.bpradiogroup[0].checked=  pattern_pairs.test(e2a_bin_param)
        document.form1.bpradiogroup[1].checked=  pattern_1st.test(e2a_bin_param)
        document.form1.bpradiogroup[2].checked=  pattern_2nd.test(e2a_bin_param)
        document.form1.bpradiogroup[3].checked=  pattern_diff.test(e2a_bin_param)
      }
   }
   else
   {  //  running

      // Dual Channel Mode
     
       //  if( typeof(document.getElementById("dualchan")) != undefined)
       if( document.getElementById("dualchan") != undefined)
	  document.getElementById("dualchan").innerHTML=dual_channel_mode;

     // Beam Mode
       if(document.getElementById("bm") != undefined)
     {
        //  beam_mode is now global
        if(pattern_c.test(beam_mode))
            document.getElementById("bm").innerHTML= 'Continuous'
         else
            document.getElementById("bm").innerHTML= 'Pulsed'
     }

     // Pulse Pairs
     if(document.getElementById("pp") != undefined)
     {
        //  pulse_pairs  global
        if(pattern_000.test(pulse_pairs))
             document.getElementById("pp").innerHTML= '000' 
         else if(pattern_180.test(pulse_pairs))
             document.getElementById("pp").innerHTML= '180'  
         else
             document.getElementById("pp").innerHTML= '?' 
      }

     // Frequency Mode
     if(document.getElementById("fm") != undefined)
     {
         // freq_mode global
	 document.getElementById("fm").innerHTML = freq_mode;
     }

     // CAMP device
     if(document.getElementById("camp") != undefined)
	 
	 document.getElementById("camp").innerHTML=camp_dev; // ODBGet( camp_devcode_path);
	      //  else alert(" element camp undefined");    // for 1c mode
 
     // Bin Params
     if(document.getElementById("bp") != undefined)
     {
        document.getElementById("bp").innerHTML =  e2a_bin_param.toLowerCase();
     }

     // helicity sleep time not shown if helicity flip is disabled
      // helicity sleep time ignored in dual channel mode 
     if (dual_channel_mode  ||  ! hel_flip )
     {
           remember_hel_sleep = document.getElementById("hel_sleep").innerHTML;
           document.getElementById("hel_sleep").innerHTML="";
     }

 }    // running

 init_exp_specific_params(); // BNMR: box7,8 used for type 2s
 
   //document.form1.box4.checked= get_bool(ODBGet(e1f_ext_start_path));  // initialize to the correct value
   if(typeof(document.form1.box3) !='undefined')
       document.form1.box3.checked= randomize;  // initialize to the correct value


   // if(ppg_debug)
   //     document.getElementById ("pstar").innerHTML= remember_ppg_dbg_notes
   //  else
   //     document.getElementById ("pstar").innerHTML="";
   
  
    if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending  (initialize_ppg_params)"
}

function initialize()
{
   var pattern1f = /1f/;  // PPG Mode "1f"
   init=1;
 
   bnmr_expt = pattern_bnmr.test(my_expt);  // globals
   bnqr_expt = pattern_bnqr.test(my_expt);

   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>initialize</b>: starting "
 
   if(bnmr_expt)
      document.getElementById("hdr").innerHTML='Run Parameters for &beta;-nmr ' 
   else if (bnqr_expt)
      document.getElementById("hdr").innerHTML='Run Parameters for &beta;-nqr '    

    //alert( 'typeof( document.getElementById("tip") ) =' +typeof(  document.getElementById("tip")));

    remember_tip       = document.getElementById("tip").innerHTML;
    remember_ppg_title = document.getElementById("ppgmode").innerHTML;
    remember_showtunes = document.getElementById("showtunes").innerHTML;
    remember_tuneline1 = document.getElementById("tuneline1").innerHTML;
    remember_tuneline2 = document.getElementById("tuneline2").innerHTML;
    remember_buttons =  document.getElementById("buttons").innerHTML;
    remember_zaher =  document.getElementById("zaher").innerHTML;
    remember_2ecalc =  document.getElementById("2ecalc").innerHTML;
    remember_20calc =  document.getElementById("20calc").innerHTML;
    remember_ldbtn =  document.getElementById("ldbtn").innerHTML;
    remember_rtl =  document.getElementById("rtl").innerHTML;
   
    remember_cdbtn =  document.getElementById("cdbtn").innerHTML; 
    remember_chngd =  document.getElementById("chngd").innerHTML; 
    remember_llt =  document.getElementById("llt").innerHTML; 
    remember_showall =  document.getElementById("showall").innerHTML; 
    remember_PSMparams = document.getElementById("PSMparams").innerHTML;
    // remember_ppg_dbg_notes = document.getElementById("pstar").innerHTML;
    remember_tdebug =  document.getElementById('tdebug').innerHTML;

    remember_title = document.getElementById("hdr").innerHTML;
    //  alert('pdpath '+pdpath)
    //  var temp= ODBGet( pdpath);
    //  alert('temp '+temp)

    var ppg_debug = get_bool( CustomData.hidden["ppg debug"]);
    if(ppg_debug == undefined)
	alert('ppg_debug ( CustomData.hidden["ppg debug"] ) is undefined')
    document.form1.pbox0.checked  = ppg_debug // get_bool( ODBGet( pdpath));  // PPG debug checked?  pdpath ="/custom/hidden/ppg debug";

    gbl_debug = get_bool( CustomData.hidden["param debug"]);
    if(gbl_debug == undefined)
    {
	alert('gdebug ( CustomData.hidden["param debug"] ) is undefined')
	gbl_debug=0;
    }
    document.form1.gdbbox.checked  = gbl_debug;

    document.form1.pcounter.value= 0;
    if(!gbl_debug)
    {
      document.getElementById('tdebug').innerHTML=""; // clear title
      document.getElementById('gdebug').innerHTML=""; // clear area
    }

 
    if(have_psm3)
    {
	document.getElementById('param_table').style.width="40%"; // reduce the size of the PPG param table
	document.getElementById('psmhl').style.width="60%"; // increase the size of the PSM param table

	if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>initialize</b>: calling  assign_channel_paths()";
	assign_channel_paths(); // includes  get_psm_path_arrays() and get_psm_quad_path_arrays
    }
    else
    {

	if(pattern1f.test(ppg_mode) || ppg_mode=="2s")
	    {
		document.getElementById('param_table').style.width="40%"; // reduce the size of the PPG param table
		document.getElementById('psmhl').style.width="60%"; // increase the size of the PSM param table
		show_fref=1; // global  fref profile is shown and may be enabled
	    }
	if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>initialize</b>: calling  assign_profile_paths()";
	assign_profile_paths();
	if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>initialize</b>: calling  get_psm_path_arrays() ";
	get_psm_path_arrays(); // uses show_fref
    }
    

    document.getElementById("update").innerHTML='Page fully reloaded every: '+update_page_s+' seconds'

    document.form1.gdbbox.checked=gbl_debug;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>initialize</b>: ending "
}

function get_globals()
{
    //alert('get_globals')
    // Fill some globals used elsewhere
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_globals</b>: starting "

    my_expt = ExperimData.name; // global
    rstate = RunInfoData.state
    if(rstate==undefined)
       alert('rstate is undefined ( RunInfoData.state)')

    if(rstate != document.form1.rstate.value)
       rstate_changed=1;
    else
       rstate_changed=0;
    document.form1.rstate.value=rstate;

    tip = RunInfoData["transition in progress"]
    if(tip == undefined)
       alert('tip is undefined ( RunInfoData["transition in progress"] )')

    if(tip != document.form1.tip.value)
	tip_changed=1;
    else
        tip_changed=0;
   document.form1.tip.value=tip;

    ppg_mode = EqData.frontend.input["experiment name"];  // global
    if(ppg_mode  == undefined)
       alert('ppg_mode is undefined ( EqData.frontend.input["experiment name"])')

    ppg_mode = ppg_mode.replace(/\n/,""); // strip off any extra carriage return
    ppg_mode = ppg_mode.toLowerCase();
    if(ppg_mode != document.form1.ppg_mode.value)
       ppg_mode_changed=1;
    else
       ppg_mode_changed=0;
    document.form1.ppg_mode.value=ppg_mode;
    // alert('get_globals: ppg_mode= '+ppg_mode)
 
    num_tunes =  parseInt(TunesData.num_tunes) //  ODBGet(num_tunes_path);
    if(num_tunes  == undefined)
       alert('get_globals: num_tunes is undefined ( TunesData.num_tunes)')

     sampleref_mode =  get_bool(CustomData.hidden.current_mode["enable sample reference mode"]);
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="... and ending (get_globals)"
}


function write_common_params(debug)
{ // dual channel mode,  helicity flip  and helicity sleep time  in Table id PPGcommon
 
 var pattern1 = /1[fnd]/;
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>write_common_params</b>: starting "
 // dual_channel_mode, hel_flip global
 dual_channel_mode= get_bool (EqData.frontend.hardware["enable dual channel mode"]);  //  ODBGet(dual_channel_path);
 hel_flip = get_bool(CustomData.hidden.current_mode["flip helicity"]);  //ODBGet(hel_flip_path);
 //alert('hel_flip='+hel_flip+'dual_chan='+dual_channel_mode)
 var temp,tmp,index;
 var text="";

 
 text = '<tr>'
 text+= '<td class="common">Flip helicity</td>'
 text+= '<td>'

 if(rstate==state_stopped)
      text+= '<input  name="box0"  type="checkbox"  onClick="set_flip( this.checked?\'1\':\'0\')">'
 else
   text+= hel_flip

 text+= '</td>'
 if(debug)
    text = add_truevalue(text,hel_flip, EqData.frontend.hardware["enable helicity flipping"]) // true_hel_flip_path);

 text+= '</tr>'
 ;

  

 // Helicity sleep time (global)
  hel_sleep_ms = CustomData.hidden.current_mode["helicity sleep time (ms)"] //  ODBGet(hel_sleep_path);
      //    alert(' hel_sleep_ms= '+ hel_sleep_ms)
  text+= '<tr id="hel_sleep">'
  text+= '<td class="common">Helicity flip sleep (ms)</td>'
  text+= '<td id="hs">'
  if(rstate==state_stopped)
    {
     text+= '<a href="#" onclick="myODBEdit(hel_sleep_path, hel_sleep_ms, update_ppg);">'
     text+= hel_sleep_ms;
     text+= '</a>';
     text+= '' ;
    }
  else
       text+= hel_sleep_ms
  text+= '</td>'
  if(debug)
      text = add_truevalue(text, hel_sleep_ms,  EqData.frontend.hardware["helicity flip sleep (ms)"])// true_hel_sleep_path)
  text+= '</tr>'
  ;
 

   // Dual Channel Mode

   if(ppgtype == 1  &&  dual_channel_mode)
       {
          cs_odbset(dual_channel_path,0) 
	   //  ODBSet(dual_channel_path,0);   // Turn it OFF //  text+= '<td class="error">Dual Channel Mode</td>'
       }

   if(ppgtype == 2)  //  ppgtype == 2
   {
      text+= '<tr>'
      text+= '<td class="dual">Dual Channel Mode</td>'
	  var my_colspan=1;
     
      if(debug)
	  my_colspan=2;   // not a mode param (a "true" value already)

      text+= '<td colspan='+my_colspan+' id="dualchan">'
      if(rstate==state_stopped)
      text+= '<input  name="box1"  type="checkbox"  onClick="set_dual_chan_mode(this.checked?\'1\':\'0\')">'
	        else
	          text+= dual_channel_mode
      text+= '</td>'
      text+= '</tr>'
    
      
   }

   //alert(' write_common_params: text= '+text)
  document.getElementById("PPGparams").innerHTML += text;
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="... and ending (write_common_params) "
 
 return;
}

function set_dual_chan_mode(val)
{
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_dual_chan_mode</b>: starting "
   val=parseInt(val);
   var paths=new Array(); var values=new Array();
   paths[0]=dual_channel_path;
   values[0]=val;
   // ODBSet(dual_channel_path,val); update(1)
   document.form1.box1.checked=val; // initialize to correct value
   gbl_code=1;
   cs_odbset(dual_channel_path,val) // set and call update() -> load() to  call build_ppg_params()
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_dual_chan_mode</b>: ending "
}

function set_random(val)
{
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_random</b>: starting "
   val=parseInt(val);

   // ODBSet(randomize_path,val); update(1)
   document.form1.box3.checked=val; // initialize to correct value
   gbl_code=1;
   cs_odbset(randomize_path,val) // calls update() to call build_ppg_params()
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (set_random) "
}

function set_constant_time(val)
{

 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_constant_time</b>: starting "
   val=parseInt(val);
 //alert('set_constant_time: e1f_const_cycle_path="'+e1f_const_cycle_path+'" and val='+val);
  
   //  ODBSet( e1f_const_cycle_path,val); update(1);
   document.form1.box5.checked=val; // initialize to correct value
   gbl_code=1; // for update
   cs_odbset (e1f_const_cycle_path,val); // update() will call build_ppg_params()

 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (set_constant_time) "
}

function set_sample_ref(val)
{

    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:fuschia"><b>set_sample_ref</b>: starting </span>'
   val=parseInt(val);
   document.form1.box6.checked=val; // initialize to correct value
   gbl_code=1; // for update
   //  alert('set_sample_ref val='+val)
   cs_odbset (sampleref_mode_path,val); // update() will call build_ppg_params()
 

 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (set_sample_ref) "
}



function set_flip(val)
{
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_flip</b>: starting "

    val=parseInt(val);
    // ODBSet(hel_flip_path, val);  call update(1)
    document.form1.box0.checked=val; // initialize to correct value
    gbl_code=1; // for update
    cs_odbset(hel_flip_path, val) // and call update() to build_ppg_params();
 
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (set_flip) with a call to update (1) "
 update();
}

function set_radio_jump(val)
{

 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_radio_jump</b>: starting "
    val=parseInt(val);

    // ODBSet(jump_to_idle_path, val); update(2);
    // alert('set profile_path_array['+i+'] to '+val)
   document.form1.jiradiogroup[0].checked=  val
   document.form1.jiradiogroup[1].checked=  !val
   gbl_code=2; // for update
   cs_odbset(jump_to_idle_path, val); // and update();

    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_radio_jump</b>: ending with a call to update (2) "
  update()
}

function cs_odbset(one_path, one_value)
{   // cs_odbset   convert single values to array then call async_odbset
    //            update_code (update_code) may not be supplied
    var Paths=new Array(); var Values=new Array();
    
    Paths[0]=one_path;
    Values[0]=one_value;
    // alert('cs_odbset:  Paths='+Paths[0]+' and Values='+Values[0])

    
    if(gbl_debug) 
	{
       document.getElementById('gdebug').innerHTML+='<br><span style="color:orange"> cs_odbset: starting with one_path='+one_path+'; one_value='+one_value+'</span>'
       }
   
   if( cs_odbset.arguments.length < 2)
   {
       alert("cs_odbset: error returning because too few arguments");
      if(gbl_debug) 
	    document.getElementById('gdebug').innerHTML+='<br><span style="color:orange"> cs_odbset: returning because too few args</span>'
     return;
   }

 
  
     if(gbl_debug) 
         document.getElementById('gdebug').innerHTML+='<br><span style="color:orange"> cs_odbset: calling async_odbset with paths '+Paths+' and values '+Values+'</span>'

	       async_odbset(Paths,Values);
 
 if(gbl_debug) 
         document.getElementById('gdebug').innerHTML+='<br><span style="color:orange"> cs_odbset: returning</span>'
}

function remove_blanks(paths,values)
{
    var len=paths.length;
    var i;
    var myPaths=new Array();
    var myValues=new Array();
    var splitexp=/[%+]/;
    var pattern=/&&+/;

  // Remove any empty elements that may have appeared
  var stringversion=paths.join("&");
  if(pattern.test(stringversion))
      { // there are empty elements
          if(gbl_debug)
	      {
                  document.getElementById('gdebug').innerHTML+='<br><span style="color:red">remove_blanks: paths contains blank elements</span>'
		  document.getElementById('gdebug').innerHTML+='<br>length='+paths.length+'   paths='+paths+' and values= '+values;
	      }
	  stringversion=stringversion.replace(/&+/g,"%");
	  //alert('async_odbset: paths stringversion='+ stringversion);
	  myPaths=stringversion.split(splitexp);

	  stringversion=values.join("&");
	  stringversion=stringversion.replace(/&+/g,"%");
	  // alert('async_odbset: values stringversion='+ stringversion);
	  myValues=stringversion.split(splitexp);
          for(i=0;i<myPaths.length; i++)
	      {
	         paths[i]=myPaths[i];
                 values[i]=myValues[i];
              }
          paths.length=values.length=i;
	  // alert('remove_blanks: now len='+paths.length+' paths='+paths+' values= '+values);
        if(gbl_debug)
	       document.getElementById('gdebug').innerHTML+='<br>remove_blanks: now length='+paths.length+'   paths='+paths+' and values= '+values;
      }
    return;
}
function async_odbset(paths,values)
{
   // called from cs_odbset or directly
   // expects parameters paths, values are ARRAYS
   // if gbl_callback_flag > 0, does a callback
    var i,len;
    var myPaths=new Array();
    var myValues=new Array();
    var splitexp=/[%+]/


    //	 alert('async_odbset: len='+paths.length+' paths='+paths+'values='+values);
	if(paths.length != values.length)
	{
	    alert('async_odbset: ERROR: length of paths and values arrays are not equal. Not writing to ODB')
            alert('async_odbset: path length='+paths.length+' paths='+paths+' values length='+values.length+' values='+values);
	    return;
        }
    remove_blanks(paths,values); // make sure there are no blank values
   

    // alert('async_odbset: after check_blanks, length='+paths.length+' paths='+paths+'values='+values);

    progressFlag= progress_write_callback
    if(gbl_debug) 
         document.getElementById('gdebug').innerHTML+='<br><span style="color:thistle">async_odbset: writing  paths '+paths+' and values '+values+'</span>'
	     //    alert('async_odbset: writing  paths '+paths+' and values '+values)

    mjsonrpc_db_paste(paths,values).then(function(rpc) {
        progressFlag= progress_write_got_callback;
        document.getElementById('readStatus').innerHTML = 'async_odbset:  status= '+rpc.result.status
        len=rpc.result.status.length // get status array length
	//	alert('async_odbset: length of rpc.result.status='+len+' update_code='+update_code)
        for (i=0; i<len; i++)
        {
           if(rpc.result.status[i] != 1) 
	       alert(' async_odbset: status error '+rpc.result.status[i]+' at index '+i+'; paths='+paths+' and values= '+values)
        } 
        document.getElementById('writeStatus').innerHTML='async_odbset: writing paths '+paths+' and values '+values
        if(gbl_debug) 
         document.getElementById('gdebug').innerHTML+='<br><span style="color:thistle">async_odbset: wrote paths '+paths+' and values '+values+'</span>'
	     //     alert('async_odbset: wrote  paths '+paths+' and values '+values)
	     if(gbl_callback_flag)     async_odbset_callback();
    }).catch(function(error)
          { 
          mjsonrpc_error_alert(error); });
 }

function async_odbset_callback()
{
    // async_odbset callback comes here if gbl_callback_flag is > 0
    // can use value of gbl_callback_flag to call something else

    var flag = gbl_callback_flag;
    gbl_callback_flag=0; // clear flag

    //alert('async_odbset_callback: flag='+flag);
    if(flag==1)
        eval_callback();// 1 = eval Mode 1w(CW) frequency table
	   
    return;	
}




function write_type1_common_params( debug, ppgtype)
{  // 
 
  if (ppgtype != 1)  // Type 1 ?
     return;

 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>write_type1_common_params</b>: starting "
  
   var text;
   // num_cps global
   num_cps = parseInt( CustomData.hidden.current_mode["num cycles per scan incr"]) //  ODBGet(cps_path)
   if(num_cps == undefined)
        alert('num_cps is undefined ( CustomData.hidden.current_mode["num cycles per scan incr"] ) ')
   text = '<tr>'
   if (get_bool( e1f_const_cycle) && num_cps > 1)
          text+= '<td class="error">Number cycles per scan increment</td>'
   else
      text+= '<td class="common">Number cycles per scan increment</td>'
   text+= '<td>'

   if(rstate==state_stopped)
   {
       text+= '<a href="#" onclick="myODBEdit(cps_path, num_cps, update_ppg)" >';
     text+= num_cps;
     text+= '</a>';
     text+= '' ;    
   }
   else
     text+= num_cps

   text+= '</td>'
   if(debug)
      text = add_truevalue(text, num_cps, EqData.frontend.input["num cycles per supercycle"]  ) // true_cps_path)
   text += '</tr>'
   ;
		     

   if(0)
       {   // don't show these as experimenters don't seem to know what it means

   var num_midbnmr = ODBGet(num_midbnmr_path);   // if(0)
   text+= '<tr>'
   text+= '<td class="common">Number of midbnmr regions  </td>'
   text+= '<td>'
   if(rstate==state_stopped)
   {
     text+= '<a href="#" onclick="myODBEdit(num_midbnmr_path,num_midbnmr,update_ppg)";  >'
     text+= num_midbnmr;
     text+= '</a>';
     text+= '' ; 
   }
   else
     text+= num_midbnmr
   text+= '</td></tr>'
   ;


   var def_midbnmr = ODBGet(def_midbnmr_path); // if(0)
   text+= '<tr>'
   text+= '<td class="common">Use default midbnmr parameters </td>'
   text+= '<td>'
   if(rstate==state_stopped)  
   {   
       text+= '<input  name="box2"  type="checkbox"  onClick="ODBSet(def_midbnmr_path, this.checked?\'1\':\'0\'); build_ppg_params() ">' // if(0)
      
   }
   else
     text+= def_midbnmr
   text+= '</td></tr>'
   ;

   } // if (0)

  document.getElementById("PPGparams").innerHTML += text;

   if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending ( write_type1_common_params)"
   return;
}

function check_mode(ppg_mode)
{
  // All modes 2 except 20,2d,2g,2h,2s are frequency table-driven
   var pattern2 =/2[abcef]/;
  if (  pattern2.test(ppg_mode) )
      return 1;  // true
   else
      return 0;
}


function write_freq_scan_params( debug, ppg_mode, col)
{  // 
 // alert ('ppgtype '+ppgtype)
 // Modes  2e 2f 2c 2b 2a 1g 1f 1a 1b  ... three items used by this combination freq_start_hz, freq_stop_hz , freq_inc_hz
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>write_freq_scan_params</b>: starting "
   var pattern1 = /1[abgf]/;
   var pattern2 = /2[abcef]/;
   var text="";
   if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode) )
   { 
    
 //  1.  frequency start (Hz)  used by modes  2e 2c 2b 2a 2f 1g 1f 1a 1b
       //  freq_start_hz global
      freq_start_hz = CustomData.hidden.current_mode["frequency scan start (hz)"] // ODBGet(freq_start_path)
      if( freq_start_hz == undefined)
        alert(' freq_start_hz is undefined ( CustomData.hidden.current_mode["frequency scan start (hz)"] ) ')
      else
	  freq_start_hz= parseInt( freq_start_hz)
      text= '<tr>'
      text+= '<td class="scan" colspan='+col+'>Frequency scan start (Hz)</td>'
      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(freq_start_path,freq_start_hz,update_ppg)" >'
         text+= freq_start_hz;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= freq_start_hz;
      text+= '</td>'
  
      if(debug)
         text = add_truevalue(text, freq_start_hz ,  EqData.frontend.input["frequency start (hz)"]  )// true_freq_start_path)
      text += '</tr>'
     // ;
        cntr_start++;  // psm params lines


 //  2.  frequency stop (Hz)  used by modes  2e 2c 2b 2a 2f 1g 1f 1a 1b

      // freq_stop_hz global
      freq_stop_hz = CustomData.hidden.current_mode["frequency scan stop (hz)"] // ODBGet(freq_stop_path)
      if( freq_stop_hz == undefined)
        alert(' freq_stop_hz is undefined ( CustomData.hidden.current_mode["frequency scan stop (hz)"] ) ')
      else
	  freq_stop_hz= parseInt( freq_stop_hz)
      text+= '<tr>'
      text+= '<td class="scan">Frequency scan stop (Hz)</td>'
      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(freq_stop_path, freq_stop_hz, update_ppg)" >'
         text+= freq_stop_hz;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= freq_stop_hz;
      text+= '</td>'
      if(debug)
          text = add_truevalue(text, freq_stop_hz , EqData.frontend.input["frequency stop (hz)"]  )// true_freq_stop_path)
      text += '</tr>'
    //   ;
       cntr_start++

 //  3.  frequency incr (Hz)  used by modes  2e 2c 2b 2a 2f 1g 1f 1a 1b

     // freq_inc_hz global
      freq_inc_hz = CustomData.hidden.current_mode["frequency scan increment (hz)"] // ODBGet(freq_inc_path)
      if( freq_inc_hz == undefined)
        alert(' freq_inc_hz is undefined ( CustomData.hidden.current_mode["frequency scan increment (hz)"] ) ')
     else
	  freq_inc_hz= parseInt( freq_inc_hz)

      text+= '<tr>'
      text+= '<td class="scan">Frequency Scan Increment (Hz)</td>'
      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(freq_inc_path,freq_inc_hz,update_ppg)" >'
         text+= freq_inc_hz;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= freq_inc_hz;
      text+= '</td>'
      if(debug)
          text = add_truevalue(text, freq_inc_hz , EqData.frontend.input["frequency increment (hz)"]  )// true_freq_inc_path)
    
      text += '</tr>'
     // ;
       cntr_start++

    }  // end of modes  2e 2c 2b 2a 1g 1f 1a 1b  



 // Randomize frequency incr  used by modes 2e 2a 2f 1a 1b 2f
 // Note: 1f does not support randomize any more
   pattern1 = /1[ab]/;
   pattern2 = /2[aef]/;

   if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode) )
   {
      // rnadomize is global
       randomize = get_bool( CustomData.hidden.current_mode["randomize freq scan increments"]) // ODBGet(randomize_path)
      if( randomize == undefined)
        alert(' randomize is undefined ( CustomData.hidden.current_mode["randomize freq scan increments"] ) ')
      text+= '<tr>'  // restart text
      text+= '<td class="scan">Randomize Frequency Scan Increments</td>'
      text+= '<td>'
      if(rstate==state_stopped)
       text+= '<input  name="box3"  type="checkbox"  onClick="set_random(this.checked?\'1\':\'0\')">'
      else
         text+= randomize;
      text+= '</td>'  
      if(debug)
         text = add_truevalue(text, randomize  , EqData.frontend.input["randomize freq values"]) // true_randomize_path)

      text += '</tr>'
     // ;
     cntr_start++;
     
  }
  document.getElementById("PPGparams").innerHTML+=text; 
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (write_freq_scan_params)"
} // end of function  write_freq_scan_params




function write_all_params(debug, ppg_mode)
{ 
 var pattern1,pattern2;
 var text;

 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>write_all_params</b>: starting "
 // write beam mode  needed by 2d 2c 1a 1b

 // debug - if running show ppg param against real path (copied by rf_config, should be the same)
 pattern1 = /1[ab]/;
 pattern2 = /2[cd]/;
 text="";

 if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode) )
 { 
 var pattern_p=/P/;  
   var pattern_c=/C/;

     // beam_mode is global
     beam_mode = CustomData.hidden.current_mode["beam mode"] //  ODBGet(beam_mode_path)
     if(beam_mode == undefined)
        alert('beam_mode is undefined ( CustomData.hidden.current_mode["beam mode"] ) ')
    text='<tr>'
  
    text+='<td colspan=1 class="param">Beam Mode</td>'
    text+='<td id="bm">'

                     //  (<span class="em">P</span><span class="small">ulsed </span>')
                    // text+='or <span class="em">C</span><span class="small">ontinuous</span>)</td>'
    if(rstate == state_stopped)
      {
        
	  // text+= '<input type="radio" name="bmradiogroup" value=0  onClick="ODBSet(beam_mode_path,Pp)">'
        text+= '<input type="radio" name="bmradiogroup" value=0  onClick="cs_odbset(beam_mode_path,Pp)">'
        text+= '<span class="it">Pulsed </span>'
        //text+= '<input type="radio" name="bmradiogroup" value=1  onClick="ODBSet(beam_mode_path,Cc)">'
        text+= '<input type="radio" name="bmradiogroup" value=1  onClick="cs_odbset(beam_mode_path,Cc)">'
        text+= '<span class="it">Continuous</span>'
       
       // alert('C '+pattern_c.test(beam_mode)+' and P '+ pattern_p.test(beam_mode) )
       
      }
     // else
     // {
     //    if(pattern_c.test(beam_mode))
     //       text+= 'Continuous'
     //    else
     //      text+= 'Pulsed'
     //  }
     text+= '</td>'

                  //  true_beam_mode_path = input_path + "/beam_mode"
     if(debug)
         text = add_truevalue(text, beam_mode  , EqData.frontend.input.beam_mode )
     text += '</tr>'  
     ; 
 }


//  Modes 2d 1a 1b   this combination  5 items : pulse_pairs,  freq_mode, bg_delay_ms, rf_delay_ms, num_rf_cycles

//  1. write pulse pairs needed by 2d 1a 1b

 pattern1 = /1[ab]/;
 pattern2 = /2d/;

 if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode) )
 { 
   
    // pulse_pairs is global
     pulse_pairs = CustomData.hidden.current_mode["pulse pairs"] //  pulse_pairs_path
     if(pulse_pairs == undefined)
        alert('pulse_pairs is undefined ( CustomData.hidden.current_mode["pulse pairs"] ) ')

    text+= '<tr>' 
  

    text+= '<td class="param">Pulse Pairs' 

    text+= '<td id="pp">' 
 
    if(rstate==state_stopped)
      { 

	  // text+= '<input type="radio" name="ppradiogroup" value=0  onClick="ODBSet(pulse_pairs_path,Zero)">' 
        text+= '<input type="radio" name="ppradiogroup" value=0  onClick="cs_odbset(pulse_pairs_path,Zero)">' 
        text+= '<span class="it">000</span>' 
	// text+= '<input type="radio" name="ppradiogroup" value=1  onClick="ODBSet(pulse_pairs_path,One80)">' 
        text+= '<input type="radio" name="ppradiogroup" value=1  onClick="cs_odbset(pulse_pairs_path,One80)">' 
        text+= '<span class="it">180</span>' 
 
      }
  
    text+= '</td>'
     if(debug)
         text = add_truevalue(text, pulse_pairs , EqData.frontend.input["e1a and e1b pulse pairs"] ) //  true_pulse_pairs_path 

    text+='</tr>'    
    ;
     
    // if( (pulse_pairs != "000") &&  (pulse_pairs != "180"))
    //    alert('Illegal pulse pairs ');
    

  

 //   2. write frequency mode needed by 2d 1a 1b

   
     
      // freq_mode is global
      freq_mode = CustomData.hidden.current_mode["frequency mode"] //    freq_mode_path 
     if(freq_mode == undefined)
        alert('freq_mode is undefined ( CustomData.hidden.current_mode["frequency mode"] ) ')

   
    text+= '<tr>'
    //text+= '<td class="param">Frequency Mode (<span class="em">FR</span><span class="small"> or </span><span class="em">F0</span>)</td>')
    text+= '<td class="param">Frequency Mode'
    text+= '<td id="fm">'
    if(rstate==state_stopped)
      {
	  //text+= '<input type="radio" name="fmradiogroup" value=0  onClick="ODBSet(freq_mode_path,FR)">'
        text+= '<input type="radio" name="fmradiogroup" value=0  onClick="cs_odbset(freq_mode_path,FR)">'

        text+= '<span class="it">FR</span>' 
	// text+= '<input type="radio" name="fmradiogroup" value=1  onClick="ODBSet(freq_mode_path,F0)">'
        text+= '<input type="radio" name="fmradiogroup" value=1  onClick="cs_odbset(freq_mode_path,F0)">'
        text+= '<span class="it">F0</span>' 
 
      }
    //  else
    //  text+= freq_mode;
    text+= '</td>'
    if(debug)
         text = add_truevalue(text, freq_mode , EqData.frontend.input["e1a and e1b freq mode"] ) //  true_freq_mode_path

    text+='</tr>'   
    ;


    // if( (freq_mode != "FR") &&  (freq_mode != "F0")   )
    //   alert('Illegal Frequency Mode '+freq_mode);

 // 3. Background delay (ms) used by modes 2d 1a 1b

      // bg_delay_ms is global

       bg_delay_ms =  CustomData.hidden.current_mode["background delay (ms)"] // ODBGet(bg_delay_path)
      if(bg_delay_ms == undefined)
        alert('bg_delay_ms is undefined ( CustomData.hidden.current_mode["background delay (ms)"] ) ')

      text+= '<tr>'
      text+= '<td class="param_float">Background delay (ms)</td>'

      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(bg_delay_path,bg_delay_ms,update_ppg )" >'
         text+= bg_delay_ms;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= bg_delay_ms;

    text+= '</td>'    
    if(debug)
       text = add_truevalue(text,  bg_delay_ms ,  EqData.frontend.input["bg delay (ms)"] )//  true_bg_delay_path

    text += '</tr>'   
    ;  

 // 4.  RF delay (ms) used by modes 2d 1a 1b

     // rf_delay_ms is global
      rf_delay_ms =  CustomData.hidden.current_mode["rf delay (ms)"] // ODBGet(rf_delay_path)

      if(rf_delay_ms == undefined)
        alert('rf_delay_ms is undefined ( CustomData.hidden.current_mode["rf delay (ms)"] ) ')

      text+= '<tr>'
      text+= '<td class="param_float">RF delay (ms)</td>'

      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(rf_delay_path,rf_delay_ms,update_ppg)" >'
         text+=  rf_delay_ms;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= rf_delay_ms;
    text+= '</td>'
    if(debug)
       text = add_truevalue(text,  rf_delay_ms ,  EqData.frontend.input["rf delay (ms)"] ) // true_rf_delay_path
    text += '</tr>'   

    ;

  // 5. Num RF cycles


      //  num_rf_cycles is global
       num_rf_cycles =  CustomData.hidden.current_mode["number of rf cycles"] // ODBGet(num_rf_cycles_path)

      if(num_rf_cycles == undefined)
        alert('num_rf_cycles is undefined ( CustomData.hidden.current_mode["number of rf cycles"] ) ')


      text+= '<tr>'
      text+= '<td class="param">Number of RF cycles</td>'



      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(num_rf_cycles_path,num_rf_cycles, update_ppg)" >'
         text+=  num_rf_cycles ;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= num_rf_cycles;
    text+= '</td>'
    if(debug)
       text = add_truevalue(text,   num_rf_cycles ,  EqData.frontend.input["num rf cycles"] ) //true_num_rf_cycles_path
    text += '</tr>'   


   

  } // end of combination 2d 1a 1b  (5 items)




 //     RF on time (ms) used by modes 2a 2e 2d 2b 2f 2a 1a 1b
    pattern1 = /1[ab]/;
    pattern2 = /2[abdef]/;
    pattern3 = /2e/;
 
 if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode) )
 { 

     //  rf_on_time_ms is  global
      rf_on_time_ms =   CustomData.hidden.current_mode["rf on time (ms)"]  //ODBGet(rf_on_time_path)  

      if(rf_on_time_ms == undefined)
        alert('rf_on_time_ms is undefined ( CustomData.hidden.current_mode["rf on time (ms)"] ) ')
     
       
      text+= '<tr>'
      text+= '<td id="RFonms" class="param_float">RF On Time (ms)'
      if (  pattern3.test(ppg_mode))
	  text+='<br><span class="note">dwell time=RFOn Time</span>'
      text+='</td>'


      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(rf_on_time_path,rf_on_time_ms, update_ppg)" >'
         text+=  rf_on_time_ms;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text += rf_on_time_ms;
    text+= '</td>'
    if(debug)
       text = add_truevalue(text,  rf_on_time_ms , EqData.frontend.input["rf on time (ms)"] ) // true_rf_on_time_path
    text += '</tr>'   

    ;

  }


 //     RF off time (ms) used by modes 2d 2b 2f 2a 1a 1b
    pattern1 = /1[ab]/;
    pattern2 = /2[abdf]/;
  
 if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode) )
 { 
   //  rf_off_time_ms is  global
      rf_off_time_ms =   CustomData.hidden.current_mode["rf off time (ms)"]  //ODBGet(rf_off_time_path )

      if(rf_off_time_ms == undefined)
        alert('rf_off_time_ms is undefined ( CustomData.hidden.current_mode["rf off time (ms)"] ) ')
      else
	  rf_off_time_ms = parseFloat(  rf_off_time_ms);

      text+= '<tr>'
      text+= '<td class="param_float">RF Off Time (ms)</td>'



      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(rf_off_time_path, rf_off_time_ms, update_ppg)" >'
         text+=  roundup( rf_off_time_ms, 4); // show 4 dp
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+=  rf_off_time_ms;
      text+= '</td>'
      if(debug)
         text = add_truevalue(text,  rf_off_time_ms ,  EqData.frontend.input["rf off time (ms)"]) //true_rf_off_time_path)
      text += '</tr>'   

      ;
 }


 // MCS Enable Delay (ms) used by modes 2c 2b 2f 2a 1a 1b

    pattern1 = /1[ab]/;
    pattern2 = /2[abcf]/;

 if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode) )
 { 
   //  mcs_en_delay_ms is global
   mcs_en_delay_ms = CustomData.hidden.current_mode["mcs enable delay (ms)"] // ODBGet( mcs_en_delay_path)
  if( mcs_en_delay_ms == undefined)
        alert(' mcs_en_delay_ms is undefined ( CustomData.hidden.current_mode["mcs enable delay (ms)"] ) ')
     
      text+= '<tr>'
      text+= '<td class="param_float">MCS Enable Delay (ms)</td>'

      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( mcs_en_delay_path,mcs_en_delay_ms, update_ppg)" >'
         text+= mcs_en_delay_ms  ;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= mcs_en_delay_ms;
    text+= '</td>'
    if(debug)
       text = add_truevalue(text,  mcs_en_delay_ms  ,  EqData.frontend.input["mcs enable delay (ms)"] ) // true_mcs_en_delay_path)
    text += '</tr>'   

    ;
   
  }

 // MCS Enable Gate  (ms) used by modes 20 2a 2c 2b 2f 2g 2h  1a 1b 1j 1g  AND 10 1c 1d 1n 1f 1w called Bin Width
 // Mode 1b ALSO uses e1b_dwell_time.  Mode 2d ONLY uses e1b_dwell_time  Mode 2e does not use this
    pattern1 = /1[0abcdnfjgw]/;
    pattern2 = /2[0abcfgh]/;  // called mcs enable gate
    pattern3 = /1[0cdnfw]/; // called bin width
    pattern4 = /1[jg]/;  //   called dwell time
    pattern5 = /2[0g]/;     //   called dwell time
  

    if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode) )  // all type 1 and Type 2 except 2d
    { 
      //  mcs_en_gate_ms is global
    
      mcs_en_gate_ms = CustomData.hidden.current_mode["mcs enable gate (ms)"] // ODBGet(mcs_en_gate_path);
      if( mcs_en_gate_ms == undefined)
        alert(' mcs_en_gate_ms is undefined ( CustomData.hidden.current_mode["mcs enable gate (ms)"] ) ')
      else
	  mcs_en_gate_ms = parseFloat( mcs_en_gate_ms);
    

      text+= '<tr>'
      if (  pattern3.test(ppg_mode))
         text+= '<td class="param_float">Bin Width (ms)</td>'
      
      else if (  pattern4.test(ppg_mode) || pattern5.test(ppg_mode ))
         text+= '<td class="param_float">Dwell Time (ms)</td>'
      
      else
         text+= '<td class="param_float">MCS Enable Gate (ms)</td>'
        

      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(mcs_en_gate_path, mcs_en_gate_ms, update_ppg)" >'
         text+= roundup( mcs_en_gate_ms, 4); // show 4 dec places
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= mcs_en_gate_ms ;
    text+= '</td>'
    if(debug)
       text = add_truevalue(text,  mcs_en_gate_ms   ,  EqData.frontend.input["mcs enable gate (ms)"] ) // true_mcs_en_gate_path)
    text += '</tr>'   

    ;
   
  }




 //  E1B Dwell time (ms) used by modes 2d 1b 
   pattern1 = /1b/;
   pattern2 = /2d/;

 if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode) )
 { 
     // e1b_dwell_time_ms is global
     e1b_dwell_time_ms = CustomData.hidden.current_mode["dwell time (ms)"]  // ODBGet( e1b_dwell_time_path )
     if(  e1b_dwell_time_ms == undefined)
        alert(' e1b_dwell_time_ms  is undefined ( CustomData.hidden.current_mode["dwell time (ms)"] ) ')
 
      text+= '<tr>'
      text+= '<td class="param_float">Dwell Time (ms)</td>'



      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( e1b_dwell_time_path, e1b_dwell_time_ms, update_ppg )" >'
         text+= e1b_dwell_time_ms ;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= e1b_dwell_time_ms ;
    text+= '</td>'
    if(debug)
       text = add_truevalue(text,  e1b_dwell_time_ms    , EqData.frontend.input["e1b dwell time (ms)"]) // true_e1b_dwell_time_path)
    text += '</tr>'   

    ;
  }

 //  Mode 1n  ... 3 items na_start_v, na_end_v , na_inc_v

   pattern1 = /1n/;
   
 // 1. NaVolt start used by 1n

 if (  pattern1.test(ppg_mode) )
 { 
     // na_start_v is global (for myODBEdit)
      na_start_v = CustomData.hidden.current_mode["start nacell scan (volts)"] //ODBGet(na_start_path )
     if( na_start_v  == undefined)
        alert(' na_start_v  is undefined ( CustomData.hidden.current_mode["start nacell scan (volts)"] ) ')
     else
	na_start_v=parseFloat( na_start_v)
	  
      text+= '<tr>'
      text+= '<td class="param_float">Start NaCell scan (Volts)</td>'


      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(na_start_path, na_start_v, update_ppg)" >'
         text+= na_start_v ;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= na_start_v;
    text+= '</td>'
    if(debug)
       text = add_truevalue(text,    na_start_v   ,  EqData.frontend.input["navolt start"]) //true_na_start_path)
    text += '</tr>'   

    ;

 // 2. NaVolt stop used by 1n

      // na_stop_v is global
     na_stop_v = CustomData.hidden.current_mode["stop nacell scan (volts)"] //ODBGet( na_stop_path)
     if( na_stop_v  == undefined)
        alert(' na_stop_v  is undefined ( CustomData.hidden.current_mode["stop nacell scan (volts)"] ) ')
 
      text+= '<tr>'
      text+= '<td class="param_float">Stop NaCell scan (Volts)</td>'

      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( na_stop_path, na_stop_v, update_ppg)" >'
         text+=  na_stop_v;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= na_stop_v;
    text+= '</td>'
    if(debug)
       text = add_truevalue(text,    na_stop_v   , EqData.frontend.input["navolt stop"]) // true_na_stop_path)
    text += '</tr>'   

    ;


 // 3. NaVolt incr used by 1n

      // na_inc_v is global
     na_inc_v = CustomData.hidden.current_mode["nacell increment (volts)"] //ODBGet( na_inc_path)
     if( na_inc_v  == undefined)
        alert(' na_inc_v  is undefined ( CustomData.hidden.current_mode["nacell increment (volts)"] ) ')
 
      text+= '<tr>'
      text+= '<td class="param_float">NaCell Increment (Volts)</td>'


      text+= '<td>'
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( na_inc_path, na_inc_v, update_ppg)" >'
         text+=  na_inc_v;
         text+= '</a>';
         text+= '' ; 
      }
      else
         text+= na_inc_v;
    text+= '</td>'
    if(debug)
       text = add_truevalue(text,    na_inc_v   , EqData.frontend.input["navolt inc"]) // true_na_inc_path)
    text += '</tr>'   

    ;
 
  } // end of mode 1n



  // e1f num bins  modes 10 1c 1d 1f 1n 1w

   pattern1 = /1[0cdfnw]/;
   
 if (  pattern1.test(ppg_mode) )
 { 
     //  e1f_num_bins is global
     e1f_num_bins = CustomData.hidden.current_mode["number of bins"] // ODBGet(e1f_num_bins_path)
     if(  e1f_num_bins == undefined)
        alert(' e1f_num_bins  is undefined ( CustomData.hidden.current_mode["number of bins"] ) ')
    
      text+= '<tr>' 
      text+= '<td class="param">Number of bins</td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(e1f_num_bins_path,  e1f_num_bins, update_ppg )" >' 
         text+=  e1f_num_bins;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+= e1f_num_bins ;
    text+= '</td>' 
    if(debug)
       text = add_truevalue(text, e1f_num_bins    , EqData.frontend.input["e1f num dwell times"]) // true_e1f_num_bins_path)
    text += '</tr>'   

    ;
   
  }
 

 //  Mode 1d  ... 3 items la_start_v, la_end_v , la_inc_v

// Mode 1d  not supported at present

   pattern1 = /1d/;
   
 // 1. Laser start used by 1d

 //  1d IS NOT SHOWN ON STATUS PAGE
 if (  pattern1.test(ppg_mode) )
 { 

 
     var la_start_v = ODBGet( la_start_path)
  
      text+= '<tr>' 
      text+= '<td class="scan">Start Laser scan (Volts)</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( la_start_path,  la_start_v, update_ppg)" >' 
         text+= la_start_v  ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+= la_start_v ;
    text+= '</td>' 
    if(debug)
       text = add_truevalue(text,   la_start_v , EqData.frontend.input["laser start"]) // true_la_start_path)
    text += '</tr>'   

    ;

 // 2. Laser stop used by 1d


      var la_stop_v = //ODBGet(la_stop_path)
   
       text+= '<tr>' 
      text+= '<td class="scan">Stop Laser scan (Volts)</td>' 



      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(la_stop_path, la_stop_v), update_ppg" >' 
         text+= la_stop_v  ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+= la_stop_v ;
    text+= '</td>' 
    if(debug)
       text = add_truevalue(text,   la_stop_v , EqData.frontend.input["laser stop"]) // true_la_stop_path)

    text += '</tr>'   

    ;


 // 3. Laser incr used by 1d

 
      var la_inc_v =   ODBGet( la_inc_v)
   
       text+= '<tr>' 
      text+= '<td class="scan">Laser Increment (Volts)</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( la_inc_path, la_inc_v, update_ppg)" >' 
         text+=   la_inc_v ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+= la_inc_v ;
    text+= '</td>' 
    if(debug)
       text = add_truevalue(text,   la_inc_v ,  EqData.frontend.input["laser inc"]) //true_la_inc_path)
    text += '</tr>'   

    ;

  } // end of mode 1d


 //  Modes 1c  1j  ... 4 items  camp_dev, camp_start, camp_end, camp_inc 

   pattern1 = /1[cj]/;
     // when we have ODBGet we can read the devices, the code, the limits
     // for now, hardcode

 if (  pattern1.test(ppg_mode) )
 {

     // General CAMP Device  

     // Get camp_units now so they can be displayed
     //  camp_units is global
    camp_units = CustomData.hidden.current_mode["scan units"] 
    if( camp_units  == undefined)
    {
        alert(' camp_units is undefined ( CustomData.hidden.current_mode["scan units"] ) ')
        camp_units="?";
    }

 // 1. Camp start Modes 1c  1j

    //  camp_start is global
     camp_start = CustomData.hidden.current_mode["camp start scan"]  // ODBGet(camp_start_path)
     if( camp_start  == undefined)
        alert('  camp_start is undefined ( CustomData.hidden.current_mode["camp start scan"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="scan_float">Camp Start Scan ('+camp_units+')</td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_start_path, camp_start, update_ppg )" >' 
         text+=   camp_start  ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=   camp_start  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_start  , EqData.frontend.input["e1c camp start"]) // true_camp_start_path)
    text += '</tr>'   

    ;

 // 2. Camp stop Modes 1c  1j

    //  camp_stop is global
       camp_stop = CustomData.hidden.current_mode["camp stop scan"]  //ODBGet( camp_stop_path)
     if(  camp_stop == undefined)
        alert(' camp_stop  is undefined ( CustomData.hidden.current_mode["camp stop scan"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="scan_float">Camp Stop Scan ('+camp_units+')</td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_stop_path, camp_stop, update_ppg )" >' 
         text+=  camp_stop  ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=camp_stop  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_stop  , EqData.frontend.input["e1c camp stop"]) // true_camp_stop_path)
  
    text += '</tr>'   

    ;


 // 3. Camp incr Modes 1c  1j

    //  camp_inc is global
       camp_inc = CustomData.hidden.current_mode["camp increment"]  //ODBGet( camp_inc_path)
     if( camp_inc  == undefined)
        alert(' camp_inc  is undefined ( CustomData.hidden.current_mode["camp increment"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="scan_float">Camp Scan Increment ('+camp_units+')</td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_inc_path, camp_inc , update_ppg)" >' 
         text+= roundup( camp_inc, 5)   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
          text+=  roundup( camp_inc, 5)   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_inc  ,  EqData.frontend.input["e1c camp inc"]) //true_camp_inc_path)

    text += '</tr>'   

    ;

 // 4. Camp path (as listed in CAMP)  Modes 1c  1j

    //  camp_dev is global
	camp_dev = CustomData.hidden.current_mode["camp path"];
     if( camp_dev  == undefined)
        alert(' camp_dev  is undefined ( CustomData.hidden.current_mode["camp path"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">Camp path</td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_dev_path, camp_dev , update_ppg)" >' 
         text+=  camp_dev   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
          text+=  camp_dev   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_dev  ,  EqData["camp sweep device"]["camp path"]) //true_camp_dev_path)

    text += '</tr>'   

   // 5. Camp Port Modes 1c  1j

    //  camp_port is global
       camp_port = CustomData.hidden.current_mode["gpib port or rs232 portname"] 
     if( camp_port  == undefined)
        alert(' camp_port is undefined ( CustomData.hidden.current_mode["gpib port or rs232 portname"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">GPIB port or RS232 portname)</td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_port_path, camp_port , update_ppg)" >' 
         text+=  camp_port   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
          text+=  camp_port   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_port  ,  EqData["camp sweep device"]["gpib port or rs232 portname"]); //true_camp_inc_path)

    text += '</tr>'   ;


  // 6. Camp Instrument Type  Modes 1c  1j

    //  camp_ins is global
       camp_ins = CustomData.hidden.current_mode["instrument type"] 
     if( camp_ins  == undefined)
        alert(' camp_ins is undefined ( CustomData.hidden.current_mode["instrument type"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">Instrument Type</td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_ins_path, camp_ins , update_ppg)" >' 
         text+=  camp_ins   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
          text+=  camp_ins   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_ins  ,  EqData["camp sweep device"]["instrument type"]); 

    text += '</tr>'   


  // 7. Camp scan path  Modes 1c  1j

    //  camp_scan is global
       camp_scan = CustomData.hidden.current_mode["camp scan path"] 
     if( camp_scan  == undefined)
        alert(' camp_scan is undefined ( CustomData.hidden.current_mode["camp scan path"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">Camp path to scan </td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_scan_path, camp_scan , update_ppg)" >' 
         text+=  camp_scan   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
          text+=  camp_scan  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_scan  ,  EqData["camp sweep device"]["camp scan path"]); 

    text += '</tr>'   


  // 8. Camp scan units  Modes 1c  1j

    /*  camp_units is global
      camp_units was assigned above
    */
      text+= '<tr>' 
      text+= '<td class="param">Scan units </td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_units_path, camp_units , update_ppg)" >' 
         text+=  camp_units   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
          text+=  camp_units  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_units  ,  EqData["camp sweep device"]["scan units"]); 

    text += '</tr>'   

  // 9. Camp instrument maximum Modes 1c  1j

    //  camp_max is global
       camp_max = CustomData.hidden.current_mode["maximum"] 
     if( camp_max  == undefined)
        alert(' camp_scan is undefined ( CustomData.hidden.current_mode["maximum"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param_float">Instrument range: maximum </td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_max_path, camp_max , update_ppg)" >' 
         text+=  camp_max   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
          text+=  camp_max  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_max  ,  EqData["camp sweep device"]["maximum"]); 

    text += '</tr>'   

 // 10. Camp instrument minimum Modes 1c  1j

    //  camp_min is global
       camp_min = CustomData.hidden.current_mode["minimum"] 
     if( camp_min  == undefined)
        alert(' camp_scan is undefined ( CustomData.hidden.current_mode["minimum"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param_float">Instrument range: minimum </td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_min_path, camp_min , update_ppg)" >' 
         text+=  camp_min   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
          text+=  camp_min  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_min  ,  EqData["camp sweep device"]["minimum"]); 

    text += '</tr>'   
   

 // 11. Camp instrument conversion factor       Modes 1c  1j

    //  camp_factor is global
       camp_factor = CustomData.hidden.current_mode["integer conversion factor"] 
     if( camp_factor == undefined)
        alert(' camp_scan is undefined ( CustomData.hidden.current_mode["integer conversion factor"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">Instrument integer conversion factor </td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( camp_factor_path, camp_factor , update_ppg)" >' 
         text+=  camp_factor   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
          text+=  camp_factor  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, camp_factor  ,  EqData["camp sweep device"]["integer conversion factor"]); 

    text += '</tr>'   
  

  } // end of modes 1c 1j (camp)


 //  Modes 1j 1g 20 2g 2h 2w ... 2 items  e00_prebeam_dt,  e00_beamOn_dt

   pattern1 = /1[jg]/;
   pattern2 = /2[0ghw]/;

 if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode)  )
 {

 // 1. e00 prebeam dwelltimes   modes  1j 1g 20 2g 2h 2w

     //  e00_prebeam_dt is global
      e00_prebeam_dt = parseInt(CustomData.hidden.current_mode["number of prebeam dwelltimes"])  // DWORD ODBGet( e00_prebeam_dt_path );
     if( e00_prebeam_dt   == undefined)
        alert(' e00_prebeam_dt   is undefined ( CustomData.hidden.current_mode["number of prebeam dwelltimes"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">Number of Prebeam dwelltimes</td>' 


      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( e00_prebeam_dt_path, e00_prebeam_dt, update_ppg )" >' 
         text+=   e00_prebeam_dt  ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=   e00_prebeam_dt  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, e00_prebeam_dt  , EqData.frontend.input["e00 prebeam dwelltimes"]) // true_e00_prebeam_dt_path)
    text += '</tr>'   

    ;



 // 2. e00 beam on dwelltimes   modes  1j 1g 20 2g 2h 2w

    //  e00_beamOn_dt is global
    e00_beamOn_dt = parseInt(CustomData.hidden.current_mode["number of beam on dwelltimes"]);  // DWORD ODBGet( e00_beamOn_dt_path )
      if( e00_beamOn_dt  == undefined)
	  alert(' e00_beamOn_dt  is undefined ( CustomData.hidden.current_mode["number of beam on dwelltimes"] ) ');
      text+= '<tr>'; 
	    text+= '<td class="param">Number of Beam On dwelltimes </td>'; 



      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( e00_beamOn_dt_path, e00_beamOn_dt , update_ppg )" >' 
         text+=  e00_beamOn_dt   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=   e00_beamOn_dt  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,  e00_beamOn_dt  ,  EqData.frontend.input["e00 beam on dwelltimes"]) //true_e00_beamOn_dt_path)
    text += '</tr>'   

    ;


  } // end of 2 items for modes  1j 1g 20 2g 2h 2w


 // One item for Modes 1j 1g 20 2f 2g 2h  e00_beamOff_dt
   pattern1 = /1[jg]/;
   pattern2 = /2[0fgh]/;

   if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode)  )
   {

    // 1. e00 beam off dwelltimes   modes  1j 1g 20 2f 2g 2h

       //  e00_beamOff_dt is global
       e00_beamOff_dt = parseInt(CustomData.hidden.current_mode["number of beam off dwelltimes"])  //DWORD  ODBGet(e00_beamOff_dt_path)
     if(  e00_beamOff_dt == undefined)
        alert('  e00_beamOff_dt is undefined ( CustomData.hidden.current_mode["number of beam off dwelltimes"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">number of beam off dwelltimes</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(e00_beamOff_dt_path, e00_beamOff_dt, update_ppg )" >' 
         text+=  e00_beamOff_dt  ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e00_beamOff_dt  ;
    text+= '</td>' 
    if(debug)
 	text = add_truevalue(text,  e00_beamOff_dt  , EqData.frontend.input["e00 beam off dwelltimes"]) // true_e00_beamOff_dt_path)
    text += '</tr>'  

  } // end of 1 items for modes  1j 1g 20 2f 2g 2h



 //  Modes 1g  20 2h ... 2 items  RFon_delay_dt,  RFon_duration_dt,  

   pattern1 = /1g/;
   pattern2 = /2[0h]/;

 if (  pattern1.test(ppg_mode) || pattern2.test(ppg_mode)  )
 {

 // 1. RF On delay (dwelltimes)  modes  1g  20 2h

     //  RFon_delay_dt is global
     RFon_delay_dt = parseInt(CustomData.hidden.current_mode["rfon delay (dwelltimes)"])  //ODBGet (  RFon_delay_dt_path)
     if( RFon_delay_dt  == undefined)
        alert(' RFon_delay_dt  is undefined ( CustomData.hidden.current_mode["rfon delay (dwelltimes)"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">RFon Delay (dwelltimes)</td>' 




      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(  RFon_delay_dt_path, RFon_delay_dt, update_ppg )" >' 
         text+=  RFon_delay_dt   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=   RFon_delay_dt  ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,  RFon_delay_dt    , EqData.frontend.input["rfon dwelltime"]) // true_RFon_delay_dt_path)
    text += '</tr>'   

    ;

 // 2. RF On duration (dwelltimes)  modes  1g  20 2h


    RFon_duration_dt  =  parseInt(CustomData.hidden.current_mode["rfon duration (dwelltimes)"])  // INT ODBGet( RFon_duration_dt_path)  // global
     if(  RFon_duration_dt == undefined)
        alert(' RFon_duration_dt  is undefined ( CustomData.hidden.current_mode["rfon duration (dwelltimes)"] ) ')
 
      if ( RFon_duration_dt > 0)
      {
	  if(pattern2.test(ppg_mode))
             hole_burning_flag=1;
      }
     

      text+= '<tr>' 

      if( RFon_duration_dt == 0)
      {
          if  (pattern1.test(ppg_mode) )
             text+= '<td  id="RFonDT" class="error">RFon duration (dwelltimes)' ;   //  1g requires the RF to be on
          else
             text+= '<td id="RFonDT"  class="param">RFon duration (dwelltimes)' ;
      }
      else
	  text+= '<td  id="RFonDT" class="param">RFon duration (dwelltimes)' ;

      if(pattern2.test(ppg_mode)) //20,2h RF may be on or off . 
	  {
	      if(hole_burning_flag)
                 text+='<p style="text-align:center; font-weight: bold;">Press <input name="tz"  value="'+ppg_mode+'_RFsetup" type="button" title="Show slider custom page" onClick=\"document.location.href=\'/CS/slider20&\'\"> to enter Beam and RF parameters using slider page </p>'
              else
	         text+= '<br><span id="hbm" class="small">Set non-zero for "hole burning" mode & enable RF. </span>'
	  }
   
      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( RFon_duration_dt_path, RFon_duration_dt , update_ppg)" >' 
         text+=   RFon_duration_dt  ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  RFon_duration_dt   ;
    text+= '</td>' 
    if(debug)
      text = add_truevalue(text, RFon_duration_dt  , EqData.frontend.input["rfon duration (dwelltimes)"]) //true_RFon_duration_dt_path)
    text += '</tr>' 
    ;

  } // end of Modes 1g 20 2h



 //  Modes 2e 2c 2b 2f 2a  ... 1 items    num_rf_delays

   pattern2 = /2[abcef]/;

 if (pattern2.test(ppg_mode)  )
 {

 //  RF On delay (dwelltimes)  modes  2e 2c 2b 2a
    
     //  num_rf_delays is global
       num_rf_delays =  CustomData.hidden.current_mode["num rf on delays (dwelltimes)"]  // ODBGet( num_rf_delays_path)
      if(  num_rf_delays == undefined)
        alert(' num_rf_delays  is undefined ( CustomData.hidden.current_mode["num rf on delays (dwelltimes)"] ) ')
      else
	 num_rf_delays= parseInt(num_rf_delays)

	  //  alert('num_rf_delays='+num_rf_delays)
 
      text+= '<tr>' 
      text+= '<td class="param">Number of RF On Delays (dwelltimes)</td>'

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( num_rf_delays_path,  num_rf_delays, update_ppg )" >' 
         text+=  num_rf_delays   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  num_rf_delays   ;
    text+= '</td>' 

    if(debug)
       text = add_truevalue(text, num_rf_delays  , EqData.frontend.input["num rf on delays (dwell times)"]) // true_num_rf_delays_path)
    text += '</tr>' ;
 }   // end modes 2e 2c 2b 2a 2f



 //  Modes 2e 2c 2a 2b  ... 1 items    Beam Off time (ms)

  pattern2 = /2[abce]/;

  if (pattern2.test(ppg_mode)  )
  {
    // Beam off time  (ms)  modes  2e 2c 2a 2b

    //  beam_off_time_ms is global
      beam_off_time_ms= CustomData.hidden.current_mode["beam off time (ms)"]  // ODBGet( beam_off_time_path)
     if(  beam_off_time_ms == undefined)
        alert(' beam_off_time_ms  is undefined ( CustomData.hidden.current_mode["beam off time (ms)"] ) ')
     else
	    beam_off_time_ms=parseFloat(  beam_off_time_ms)
      text+= '<tr>' 
      text+= '<td class="param_float">Beam Off time (ms)</td>' 



      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( beam_off_time_path ,  beam_off_time_ms, update_ppg)" >' 
         text+=  beam_off_time_ms   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  beam_off_time_ms   ;
    text+= '</td>' 
    if(debug)
    text = add_truevalue(text, beam_off_time_ms   , EqData.frontend.input["beam off time (ms)"]) // true_beam_off_time_path)
    text += '</tr>' 
    ;

  } // end modes 2e 2c 2b 2a


  //  num beam PreCycles  2b 2a
  
  pattern2 = /2[ab]/;

  if (pattern2.test(ppg_mode)  )
  {
      //  num_beam_precycles is global
    num_beam_precycles= CustomData.hidden.current_mode["number of beam precycles"]  // ODBGet( num_beam_precycles_path)
     if(  num_beam_precycles  == undefined)
        alert('  num_beam_precycles  is undefined ( CustomData.hidden.current_mode["number of beam precycles"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">Number of Beam precycles</td>' 




      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( num_beam_precycles_path, num_beam_precycles , update_ppg)" >' 
         text+=  num_beam_precycles   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=   num_beam_precycles  ;
    text+= '</td>' 
    if(debug)
    text = add_truevalue(text, num_beam_precycles   , EqData.frontend.input["num beam precycles"]) // true_num_beam_precycles_path)
    text += '</tr>' 
    ;
   }



  //  Mode  2a ... 3 items  e2a_en_180, e2a_en_pulse_pairs, e2a_bin_param
  pattern2 = /2a/;

  if (pattern2.test(ppg_mode)  )
  {
   // 1. e2a  Enable 180   Mode 2a

      //   e2a_en_180 is global
   e2a_en_180= CustomData.hidden.current_mode["enable 180"]  // ODBGet(e2a_en_180_path)
     if(  e2a_en_180 == undefined)
        alert('  e2a_en_180 is undefined ( CustomData.hidden.current_mode["enable 180"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">Enable 180</td>' 

      text+= '<td>' 

      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(e2a_en_180_path,  e2a_en_180 , update_ppg)" >' 
         text+=  e2a_en_180   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e2a_en_180   ;
    text+= '</td>' 
    if(debug)
    text = add_truevalue(text,  e2a_en_180  , EqData.frontend.input["e2a 180"]) // true_e2a_en_180_path)
    text += '</tr>' 
    ;


  //  2. e2a pulse pairs  Mode 2a

    //  e2a_en_pulse_pairs is global
    e2a_en_pulse_pairs= CustomData.hidden.current_mode["enable pulse pairs"]  //ODBGet(e2a_en_pulse_pairs_path)
     if( e2a_en_pulse_pairs  == undefined)
        alert(' e2a_en_pulse_pairs  is undefined ( CustomData.hidden.current_mode["enable pulse pairs"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">Enable Pulse Pairs</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( e2a_en_pulse_pairs_path,  e2a_en_pulse_pairs, update_ppg)" >' 
         text+= e2a_en_pulse_pairs   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+= e2a_en_pulse_pairs   ;
    text+= '</td>' 
    if(debug)
    text = add_truevalue(text,  e2a_en_pulse_pairs   , EqData.frontend.input["e2a pulse pairs"]) // true_e2a_en_pulse_pairs_path)
    text += '</tr>' 
    ;


 //  3. e2a bin param  Mode 2a

      //   e2a_bin_param is global
      e2a_bin_param=  CustomData.hidden.current_mode["bin parameter"] ;
     if( e2a_bin_param  == undefined)
        alert('  e2a_bin_param is undefined ( CustomData.hidden.current_mode["bin parameter"] ) ')
 

      text+= '<tr>' 
     // text+= '<td class="param">Bin param (<span class="em">pairs,1st,2nd,diff</span>)</td>' 
      text+= '<td class="param">Bin parameter' ;
      text+= '<td>' 
      if(rstate==state_stopped)
      {
	  //text+= '<input type="radio" name="bpradiogroup" value=0  onClick="ODBSet( e2a_bin_param_path,Pairs)">' 
	  text+= '<input type="radio" name="bpradiogroup" value=0  onClick="cs_odbset( e2a_bin_param_path,Pairs)">' 
	  text+= '<span class="small">Pairs</span>'
	  //text+= '<input type="radio" name="bpradiogroup" value=1  onClick="ODBSet( e2a_bin_param_path,First)">'
	  text+= '<input type="radio" name="bpradiogroup" value=1  onClick="cs_odbset( e2a_bin_param_path,First)">'
          text+= '<span class="small">First</span>'
	  //text+= '<input type="radio" name="bpradiogroup" value=1  onClick="ODBSet( e2a_bin_param_path,Second)">'
	  text+= '<input type="radio" name="bpradiogroup" value=1  onClick="cs_odbset( e2a_bin_param_path,Second)">'
          text+= '<span class="small">Second</span>'
          //text+= '<input type="radio" name="bpradiogroup" value=1  onClick="ODBSet( e2a_bin_param_path,Diff)">'
	  text+= '<input type="radio" name="bpradiogroup" value=1  onClick="cs_odbset( e2a_bin_param_path,Diff)">'
          text+= '<span class="small">Diff</span>'
 

      
        //alert(e2a_bin_param + pattern_diff.test(e2a_bin_param))
       
       

       //  text+= '<a href="#" onclick="ODBEdit( e2a_bin_param_path )" >' 
       //  text+=  e2a_bin_param   ;
       //  text+= '</a>' ;
       //  text+= ''  ; 
      }
      // else
      //   text+=  e2a_bin_param   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,  e2a_bin_param, EqData.frontend.input["e2a ubit1 action"]) //  , true_e2a_bin_param_path)
    text += '</tr>'
    ;

  
   } // end of Mode 2a


  // Mode 2b 

  // num beam on dwelltimes  Mode 2b
  
  pattern2 = /2b/;

  if (pattern2.test(ppg_mode)  )
  {
      //  beam_on_dt is global
     beam_on_dt=  CustomData.hidden.current_mode["number of beam on dwelltimes"]  //ODBGet( beam_on_dt_path)
    if( beam_on_dt  == undefined)
        alert(' beam_on_dt  is undefined ( CustomData.hidden.current_mode["number of beam on dwelltimes"] ) ')
 
      text+= '<tr>' 
      text+= '<td class="param">Number of Beam On dwelltimes</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( beam_on_dt_path, beam_on_dt, update_ppg )" >' 
         text+=  beam_on_dt   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  beam_on_dt   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,  beam_on_dt  , EqData.frontend.input["e2b num beam on dwell times"]) // true_beam_on_dt_path)
    text += '</tr>';

  
   } // end of Mode 2b


  // Input parameters "prebeam on time (ms)" and  "e2c beam on time (ms)" used by  2c 2s 
  
  pattern2 = /2[cs]/;   // 2 items

  if (pattern2.test(ppg_mode)  )   
  {
  // 1. e2c beam on time (ms)    Mode 2c

      //  e2c_beam_on_ms global
      e2c_beam_on_ms =  CustomData.hidden.current_mode["beam on time (ms)"]  //ODBGet(e2c_beam_on_path)
      if(  e2c_beam_on_ms  == undefined)
        alert('  e2c_beam_on_ms is undefined ( CustomData.hidden.current_mode["beam on time (ms)"] ) ')
      text+= '<tr>' 
      text+= '<td class="param_float">Beam On time (ms)</td>' 
      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(e2c_beam_on_path,  e2c_beam_on_ms, update_ppg)" >' 
         text+=  e2c_beam_on_ms   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e2c_beam_on_ms   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,  e2c_beam_on_ms  , EqData.frontend.input["e2c beam on time (ms)"]) // true_e2c_beam_on_path)
    text += '</tr>'
    ;


 // 2. prebeam on time  (ms)   Mode 2c
  
      //  prebeam_on_ms global  but called by a different name between 2c and 2s
   
     prebeam_on_ms = CustomData.hidden.current_mode["prebeam on time (ms)"]  //ODBGet(prebeam_on_path)
     if(  prebeam_on_ms  == undefined)
	 alert(' prebeam_on_ms  is undefined ( CustomData.hidden.current_mode["prebeam on time (ms)"] ) ')
      text+= '<tr>' 
      text+= '<td class="param_float">Prebeam time (ms)</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(  prebeam_on_path, prebeam_on_ms, update_ppg)" >' 
         text+=  prebeam_on_ms   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  prebeam_on_ms   ;
      text+= '</td>'    
    if(debug)
	text = add_truevalue(text,  prebeam_on_ms  , EqData.frontend.input["prebeam on time (ms)"]) // true_prebeam_on_path)
    text += '</tr>';

  }

   // Mode 2c ...  7 items prebeam_on_ms, e2c_beam_on_ms, e2c_rf_on_ms,  e2c_num_freq, e2c_ss_width_hz ,
                           e2c_fslice_delay_ms,  e2c_flip_180_delay_ms, e2c_flip_360_delay_ms, e2c_counting_mode
  
  pattern2 = /2c/;

  if (pattern2.test(ppg_mode)  )
  {

 
// 1. e2c RF on time (ms)    Mode 2c

 
      e2c_rf_on_ms = CustomData.hidden.current_mode["rf on time (ms)"]  // ODBGet( e2c_rf_on_path)   // global
      if(  e2c_rf_on_ms  == undefined)
        alert(' e2c_rf_on_ms  is undefined ( CustomData.hidden.current_mode["rf on time (ms)"] ) ')
      text+= '<tr>' 
      text+= '<td id="e2cRFon" class="param_float">RF On time (ms)</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( e2c_rf_on_path,  e2c_rf_on_ms , update_ppg)" >' 
         text+=  e2c_rf_on_ms   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e2c_rf_on_ms   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,  e2c_rf_on_ms  , EqData.frontend.input["f select pulselength (ms)"]) // true_e2c_rf_on_path)
    text += '</tr>'
    ;

// 2. e2c num freq slices     Mode 2c

//  e2c_num_freq global
      e2c_num_freq =  CustomData.hidden.current_mode["number of frequency slices"]  //ODBGet(e2c_num_freq_path)
  if(  e2c_num_freq  == undefined)
        alert(' e2c_num_freq  is undefined ( CustomData.hidden.current_mode["number of frequency slices"] ) ')
      text+= '<tr>' 
      text+= '<td class="param">Number of frequency slices</td>' 




      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(e2c_num_freq_path,  e2c_num_freq, update_ppg )" >' 
         text+= e2c_num_freq   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+= e2c_num_freq   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,  e2c_num_freq   , EqData.frontend.input["num freq slices"]) // true_e2c_num_freq_path)
    text += '</tr>'
    ;


// 3. e2c single slice width (Hz)    Mode 2c

    //  e2c_ss_width_hz global
       e2c_ss_width_hz = CustomData.hidden.current_mode["freq single slice width (hz)"]  // ODBGet( e2c_ss_width_path)
  if(   e2c_ss_width_hz  == undefined)
        alert('  e2c_ss_width_hz  is undefined ( CustomData.hidden.current_mode["freq single slice width (hz)"] ) ')
      text+= '<tr>' 
      text+= '<td class="param">Frequency single slice width (Hz)</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(e2c_ss_width_path,  e2c_ss_width_hz, update_ppg )" >' 
         text+=  e2c_ss_width_hz   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e2c_ss_width_hz   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, e2c_ss_width_hz    , EqData.frontend.input["freq single slice width (hz)"]) // true_e2c_ss_width_path)
    text += '</tr>'
    ;


// 4. e2c   f slice internal delay (ms)   Mode 2c

    //   e2c_fslice_delay_ms global
       e2c_fslice_delay_ms = CustomData.hidden.current_mode["freq single slice int delay(ms)"]  // ODBGet(e2c_fslice_delay_path)
  if(   e2c_fslice_delay_ms  == undefined)
        alert('  e2c_fslice_delay_ms  is undefined ( CustomData.hidden.current_mode["freq single slice int delay(ms)"] ) ')
      text+= '<tr>' 
      text+= '<td class="param_float">Frequency slice internal delay (ms)</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(e2c_fslice_delay_path,  e2c_fslice_delay_ms , update_ppg)" >' 
         text+=  e2c_fslice_delay_ms   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e2c_fslice_delay_ms   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,  e2c_fslice_delay_ms   , EqData.frontend.input["f slice internal delay (ms)"]) // true_e2c_fslice_delay_path)
    text += '</tr>'
    ;

// 5. e2c   flip 180 delay (ms)   Mode 2c

    //  e2c_flip_180_delay_ms global
      e2c_flip_180_delay_ms = CustomData.hidden.current_mode["flip 180 delay (ms)"] //  ODBGet( e2c_flip_180_delay_path )
  if(  e2c_flip_180_delay_ms  == undefined)
        alert('  e2c_flip_180_delay_ms is undefined ( CustomData.hidden.current_mode["flip 180 delay (ms)"] ) ')
      text+= '<tr>' 
      text+= '<td class="param_float">Flip 180 delay (ms)</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( e2c_flip_180_delay_path,  e2c_flip_180_delay_ms, update_ppg  )" >' 
         text+= e2c_flip_180_delay_ms   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+= e2c_flip_180_delay_ms   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, e2c_flip_180_delay_ms    , EqData.frontend.input["flip 180 delay (ms)"]) // true_e2c_flip_180_delay_path)
    text += '</tr>'
    ;

// 6. e2c   flip 360 delay (ms)   Mode 2c

    //   e2c_flip_360_delay_ms global
       e2c_flip_360_delay_ms = CustomData.hidden.current_mode["flip 360 delay (ms)"]  // ODBGet( e2c_flip_360_delay_path)
  if(  e2c_flip_360_delay_ms  == undefined)
        alert(' e2c_flip_360_delay_ms  is undefined ( CustomData.hidden.current_mode["flip 360 delay (ms)"] ) ')
      text+= '<tr>' 
      text+= '<td class="param_float">Flip 360 delay (ms)</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( e2c_flip_360_delay_path,  e2c_flip_360_delay_ms , update_ppg)" >' 
         text+=   e2c_flip_360_delay_ms  ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e2c_flip_360_delay_ms   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text, e2c_flip_360_delay_ms    , EqData.frontend.input["flip 360 delay (ms)"]) // true_e2c_flip_360_delay_path)
    text += '</tr>'
    ;


// 7. e2c   counting mode   Mode 2c

//  e2c_counting_mode global
       e2c_counting_mode = CustomData.hidden.current_mode["counting mode"]  //ODBGet(e2c_counting_mode_path)
     if(  e2c_counting_mode   == undefined)
        alert('  e2c_counting_mode  is undefined ( CustomData.hidden.current_mode["counting mode"] ) ')
      text+= '<tr>' 
      text+= '<td class="param">Count After (1=1st 180, 2=2nd 180 or 3=both))</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(e2c_counting_mode_path, e2c_counting_mode, update_ppg)" >' 
         text+=  e2c_counting_mode   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e2c_counting_mode   ;
      text+= '</td>'    
     if(debug)
	text = add_truevalue(text,  e2c_counting_mode   , EqData.frontend.input.counting_mode) // true_e2c_counting_mode_path)
    text += '</tr>'
      ;


   } // end of Mode 2c

 // Mode 2e and 2f ...  1 item  e2e_postrf_dt

    pattern2 = /2[ef]/;
    if (pattern2.test(ppg_mode)  )
    {
       //  e2e  num post RF beamOn dwelltimes  Mode 2e and 2f

	// var  e2e_postrf_dt global
       e2e_postrf_dt = CustomData.hidden.current_mode["num post rfbeamon dwelltimes"]  // ODBGet(e2e_postrf_dt_path)
    if( e2e_postrf_dt  == undefined)
        alert(' e2e_postrf_dt  is undefined ( CustomData.hidden.current_mode["num post rfbeamon dwelltimes"] ) ')
    else
         e2e_postrf_dt= parseInt(  e2e_postrf_dt)
      text+= '<tr>' 
      text+= '<td class="param">Number of post RF BeamOn dwelltimes</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit(e2e_postrf_dt_path, e2e_postrf_dt, update_ppg )" >' 
         text+=  e2e_postrf_dt   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e2e_postrf_dt   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,e2e_postrf_dt   , EqData.frontend.input["e2e num postrfbeamon dwelltimes"]) // true_e2e_postrf_dt_path)
    text += '</tr>'

    }
 // Mode 20  Sample/Reference; may be added to other modes
 pattern2 = /20/;
 if (pattern2.test(ppg_mode)  )
  {

      // sampleref_mode =  get_bool(CustomData.hidden.current_mode["enable sample reference mode"]); get_global
      if( sampleref_mode == undefined)
	  alert('sampleref_mode is undefined  (CustomData.hidden.current_mode["enable sample reference mode"]) ');

      text+= '<tr>' 
      text+= '<td class="param">Enable Sample/Reference mode?' 
      txt=""
      if( sampleref_mode)
	  txt='style="color:red"';
      text+='<br><span class="small"'+txt+'>Set <b>FALSE</b> unless running Sarah\'s experiment</span></td>' 
	  //  text+= '<br><span class="small">Set <b>FALSE</b> unless running Sarah\'s experiment</span></td>' 
      text+= '<td>' 
      if(rstate==state_stopped)
      {
	   text+= '<input  name="box6"  type="checkbox"  onClick="set_sample_ref( this.checked?\'1\':\'0\')">' 
	
      }
      else
	  text+=  sampleref_mode  ;
      text+= '</td>' 
    if(debug)
	text = add_truevalue(text, sampleref_mode , EqData.frontend.hardware["enable sampleref mode"]) //  true_sampleref_mode_path
    text += '</tr>'
      ;


  }
 // Mode 2e ...  1 item   e2e_dt_per_freq

  pattern2 = /2e/;

  if (pattern2.test(ppg_mode)  )
  {

  //  e2e  Number dwelltimes per freq  Mode 2e

      //  e2e_dt_per_freq global
      e2e_dt_per_freq = CustomData.hidden.current_mode["num dwelltimes per freq"]  // ODBGet(e2e_dt_per_freq_path )
    if( e2e_dt_per_freq  == undefined)
        alert(' e2e_dt_per_freq  is undefined ( CustomData.hidden.current_mode["num dwelltimes per freq"] ) ')
    else
        e2e_dt_per_freq= parseInt( e2e_dt_per_freq)
      text+= '<tr>' 
      text+= '<td class="param">Number of dwelltimes per frequency' 
      text+='<br><span class="note">Num pre (& post) dwelltimes per frequency ='
	 if(rstate==state_stopped)
            text+='<br>&nbsp&nbsp&nbsp&nbspnumber of dwelltimes per frequency - 1'
	 else
	 {
	     var j=  parseInt(e2e_dt_per_freq) -1;
             text+=j;
         } 
      text+='</span></td>'

      text+= '<td>' 
      if(rstate==state_stopped)
      {
         text+= '<a href="#" onclick="myODBEdit( e2e_dt_per_freq_path,  e2e_dt_per_freq , update_ppg )" >' 
         text+=  e2e_dt_per_freq   ;
         text+= '</a>' ;
         text+= ''  ; 
      }
      else
         text+=  e2e_dt_per_freq   ;
    text+= '</td>' 
    if(debug)
	text = add_truevalue(text,  e2e_dt_per_freq    ,  EqData.frontend.input["e2e num dwelltimes per freq"]) //true_e2e_dt_per_freq_path)
    text += '</tr>'
 
   } // end Mode 2e


  // Mode  1f,1w   2 items
  pattern1 = /1[fw]/;
  
  if (pattern1.test(ppg_mode)  )
  {
      
  // 1. e1f  const time between cycles

      // e1f_const_cycle is global
      e1f_const_cycle =  get_bool(CustomData.hidden.current_mode["const. time between cycles"]);  // ODBGet( e1f_const_cycle_path
      if( e1f_const_cycle == undefined)
	  alert('e1f_const_cycle is undefined  (CustomData.hidden.current_mode["const. time between cycles"]) ');

      text+= '<tr>' 
      text+= '<td class="param">Enable constant time between cycles</td>' 

      text+= '<td>' 
      if(rstate==state_stopped)
      {
	   text+= '<input  name="box5"  type="checkbox"  onClick="set_constant_time( this.checked?\'1\':\'0\')">' 
	  
	  //  text+= '<a href="#" onclick="ODBEdit(  e1f_const_cycle_path  )" >')
	  // text+=   e1f_const_cycle  );
	  // text+= '</a>' ;
	  // text+= ''  ; 
      }
      else
	  text+=  e1f_const_cycle ;
      text+= '</td>'; 
      if(debug)
	  text = add_truevalue(text, e1f_const_cycle  , EqData.frontend.input["e1f const time between cycles"]); // true_e1f_const_cycle_path)
      text += '</tr>';
      



// 2. DAQ service time
      
      if (get_bool( e1f_const_cycle))
      {
	  // daq_service_time global
          daq_service_time = CustomData.hidden.current_mode["daq service time(ms)"]  // ODBGet( daq_service_time_path)
         if( daq_service_time   == undefined)
        alert(' daq_service_time  is undefined ( CustomData.hidden.current_mode["daq service time(ms)"] ) ')

        // min_daq_service_time (global)   NOT a mode parameter
	 min_daq_service_time =  EqData.frontend.input["e1f min daq service (ms)"]// ODBGet( min_daq_service_time_path);
        if(min_daq_service_time   == undefined)
        alert('min_daq_service_time  is undefined ( EqData.frontend.input["e1f min daq service (ms)"]  ) ')

         //alert(' min_daq_service_time_path='+ min_daq_service_time_path);
	 var my_class;
         var err=0;
         text+= '<tr>' 
	     if(parseFloat(daq_service_time) < parseFloat( min_daq_service_time) )
            my_class="error"
         else
            my_class="param_float"  
         text+= '<td class="'+my_class+'">DAQ service time (ms) (see '
         text+= '<input name="trueinfo" value="Notes" type="button" title="show info re const1f"  style="color:firebrick" onClick=" show_notes_const1f();">'
         text+= ')<br><small>Minimum value '+min_daq_service_time+'ms '
         if(err)
            text+= 'will be used '
	 text+= '</small></td>'


         text+= '<td>' 
         if(rstate==state_stopped)
         {
            text+= '<a href="#" onclick="myODBEdit( daq_service_time_path, daq_service_time , update_ppg)" >' 
            text+=   daq_service_time   ;
            text+= '</a>' ;
            text+= ''  ; 
         }
         else
            text+= daq_service_time   ;
         text+= '</td>' 
         if(debug)
  	     text = add_truevalue(text,  daq_service_time  , EqData.frontend.input["daq service time (ms)"]) // true_daq_service_time_path)

         text += '</tr>';

      } // const cycle time mode

  } // end Mode 1f 1w
  
  document.getElementById("PPGparams").innerHTML+=text; 

 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending  (write_all_params)"
  return;
} // end of function




function init_mode_files(ppg_mode)
{
   // later with ODBGet, display mode filenames
}   

function convert_all_quad(i)
{
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>convert_all_quad</b>: starting "
    // convert all boolean quad mode parameters
    jump_idle_iq[i]=get_bool(jump_idle_iq [i] );
    load_first_iq[i]=get_bool(load_first_iq[i] );
    load_iq_file[i]=get_bool(load_iq_file[i] );
    set_const_i[i]=get_bool( set_const_i[i] );
    set_const_q[i]=get_bool(set_const_q[i] );
   // alert('convert_all_quad  set_const_q is '+set_const_q[i])
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (convert_all_quad)"
}


function reload()
{
      if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>reload</b>: starting.. full reload "
   // also called from poll() in params_common.js
   window.location.reload();
}
function clear_frame()
{   // function needed by compare_tunes.html only
    reload(); // just reload as not using frames on parameters page
}

function get_bool(jval)
{
  var ival;
  if(jval == undefined)
      return jval;
  if(pattern_y.test(jval))
    ival=1;
  else
    ival=0;
  return ival;
}

function unbool(jval)
{
    if(jval)
	return ("y");
    else
	return ("n");
}

function radio_quad(val,index)
{
   var my_path;   // PSMII only (BNMR)
   var paths=new Array();
   var values=new Array();
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>radio_quad</b>: starting "
		   //  alert(' radio_quad: val='+val+' psm_module='+psm_module)

   if(val==0)
       document.getElementById('smb').style.backgroundColor="navy"; // signifies clicked
   else
      document.getElementById('qmb').style.backgroundColor="azure"; // signifies clicked
   
   
   if(psm_module=="PSM") // PSM Module    psm quad val=1
   {
       // alert('radio_quad  setting '+quadrature_path[index]+' with val= '+val+'  i = '+i)
 
       //ODBSet( quadrature_path[index], val);
       paths[0]=quadrature_path[index];   values[0]=val;  // set quad mode
   }

 //  simulate_single_tone_mode()  WAS a separate function called from radio_quad()
   else if(psm_module=="PSMII") // PSM Module
   {   
          paths[0]=quadrature_path[index];   values[0]=1;  // set quad mode
          paths[1]=load_iq_file_path[index]; values[1]=1; // load i,q file
          paths[2]=set_const_i_path[index]; 
          paths[3]=set_const_q_path[index]; 
          paths[4]=idle_i_path[index];      
          paths[5]=idle_q_path[index];      
          paths[6]=jump_to_idle_iq_path[index]; 
          paths[7]=simulate_single_tone_path[index];

      if(val==0)
      {
         values[2]=1; //set constant i value in file path
         values[3]=1; //set constant q value in file path
         values[4]=511; //idle i path
         values[5]=0; //  idle q path
         values[6]=0; // jump_to_idle_iq_path
         values[7]=1; //simulate_single_tone_path

         paths[8]=const_i_path[index];      values[8]=511; //set constant i value to 511  
         paths[9]=const_q_path[index];      values[9]=0; // set constant i value to 0
       
        

	  // alert ('Simulating Single Tone Mode by setting Quad Mode with Idle i,q pair= (511,0)')
	  // Set quad mode, idle i=511 idle q=0, load an iq file and set values to 0,511
         //Paths/Values
         //Index
         // 0  ODBSet(quadrature_path[index],1);  // set quad mode
         // 1  ODBSet(load_iq_file_path[index], 1); // load i,q file
	 // 2  ODBSet(set_const_i_path[index],1); //set constant i value in file
	 // 3  ODBSet(set_const_q_path[index],1); //set constant q value in file
	 // 8  ODBSet(const_i_path[index], 511); // set constant i value to 511  
	 // 9  ODBSet(const_q_path[index], 0);  // set constant i value to 0
          // alert('path = '+idle_i_path[index])
	 // 4  ODBSet(idle_i_path[index], 511);  // just in case
	 // 5  ODBSet(idle_q_path[index], 0);
	 // 6  ODBSet(jump_to_idle_iq_path[index], 0);
	 // 7  ODBSet(simulate_single_tone_path[index],1); // set flag 
   
      }
      else
      {
	  // alert ('Reverting to true Quadrature Mode from simulated single tone mode')
          // Set quad mode

         values[2]=0; //set constant i value in file path
         values[3]=0; //set constant q value in file path
         values[4]=0; //idle i path
         values[5]=0; //  idle q path
         values[6]=1; // jump_to_idle_iq_path
         values[7]=0; //simulate_single_tone_path

         //Paths/Values
         //Index
         // 0  ODBSet(quadrature_path[index],1);  // set quad mode
         // 1  ODBSet(load_iq_file_path[index], 1); // load i,q file
         // 2  ODBSet(set_const_i_path[index],0); //do not set constant i value in file 
         // 3  ODBSet(set_const_q_path[index],0); //do not set constant q value in file 
         // 4  ODBSet(jump_to_idle_iq_path[index], 1);
         // 5  ODBSet(idle_i_path[index], 0); 
         // 6  ODBSet(idle_q_path[index], 0);
         // 7  ODBSet(simulate_single_tone_path[index],0); // clear flag 
       }
   }
   else
   {
       alert(" radio_quad: unknown psm_module: "+psm_module)
       return;
   }
   //   alert('radio_quad: val='+val+' index= '+index+'  paths= '+paths);
   progressFlag= progress_write_callback;
   mjsonrpc_db_paste(paths,values).then(function(rpc) {
   var i,len;
   document.getElementById('readStatus').innerHTML = 'radio_quad:  status= '+rpc.result.status
   len=rpc.result.status.length // get status array length
   // alert('radio_quad: length of rpc.result.status='+len)

   for (i=0; i<len; i++)
   {
      if(rpc.result.status[i] != 1) 
                   alert('radio_quad: status error '+rpc.result.status[i]+' at index '+i)
   } 
   document.getElementById('writeStatus').innerHTML='radio_quad: writing paths '+paths+' and values '+values
   progressFlag= progress_write_got_callback;
      gbl_code=2; // for update
      update() // rebuild psm table
   }).catch(function(error)
   { 
          mjsonrpc_error_alert(error); });

  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>radio_quad</b>: ending "
   return
}    
   


function set_psmii_quad_mode()
{
  var paths=new Array(); 
  var values=new Array();
  var index=0;
  //alert('set_psmii_quad_mode: setting quad mode for all valid profiles')
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_psmii_quad_mode</b>: starting "
  for(i=0; i<4; i++)
  {
      if(profile_enabled[i] && profile_path[i].length > 5) // quadrature_path depends on profile_path
      {
	  //alert('set_psmii_quad_mode: profile_path['+i+']='+profile_path[i]+' length='+profile_path[i].length)
    
	  paths[index]=quadrature_path[i];
          values[index]=1;
          index++;
      }
  }
  gbl_code=2; // for psm table update
  async_odbset(paths,values); // called with ARRAYS; 
 
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_psmii_quad_mode</b>: ending "
}



function openPop(gifname)
{
  var path = '/custom/images/'+gifname
  configWin = open (path,'config','height=800,width=800,scrollbars=yes,menubar=yes,toolbar=yes');
}

function openPop1(ppg_mode)
{
    //var path = '/custom/images/'+gifname
  Pop1Win = window.open ('','','height=800,width=800,scrollbars=yes,menubar=yes,toolbar=yes');
  if(Pop1Win != null)
      {
	  document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">')
	  document.write('<html><head><title>Pop1Win</title>');
	  document.write('<script type="text/javascript" src="mhttpd.js"></script> ');
          var tunepath="/customscript/tune"+ppg_mode+"/tune";  // tune name

      }
}

function openPopZ()
{
    if (typeof PopZWin !='undefined')
    {
       if(PopZWin && !PopZWin.closed)
	   PopZWin.close();
    }
    PopZWin = window.open ('http://bnmr.triumf.ca/?file=Tp2BW_Convertor','','height=800,width=800,scrollbars=yes,menubar=yes',1);

}

function openPopCmp()
{
    if (typeof PopCmpWin !='undefined')
    {
       if(PopCmpWin && !PopCmpWin.closed)
	   PopCmpWin.close();
    }
    PopCmpWin = window.open ('./cmp&','','height=800,width=1000,scrollbars=yes,menubar=yes',1);

}



function show_tune_differences()
{
   var path = '/custom/tune diffs'

   if (typeof PopDWin !='undefined')
   {	  
       //alert('PopDWin= '+PopDWin);
       if(PopDWin && !PopDWin.closed)
	   PopDWin.close();
    }  
    PopDWin =  open (path,'config','height=800,width=800,scrollbars=yes,menubar=yes,toolbar=yes');
    if(PopDWin == null)
    {
        alert('Cannot open popup window')
        return;
    }
}

// Tune functions

function read_tunes()
{   
    // tune_names is a global array
  
     if (num_tunes > 0)
     {
       if (num_tunes == 1)
       {
	   tune_names_path = tunes_path+'/tune_names';
           tune_names[0] =    TunesData.tune_names; // if there is only one tune, make sure tune_names is still an array
       }
       else
       {
          tune_names_path = tunes_path+'/tune_names[*]';
          tune_names=TunesData.tune_names;
       }
   }
   else
       tune_names[0]=""; // no tunes are defined

   if( tune_names  == undefined)
	alert( 'read_tunes: tune_names are undefined ( TunesData.tune_names) ')
	    //alert('read_tunes: tune_names '+tune_names);
}



function ppgDebug(i)
{
    var debug_ppg = parseInt(i);
    // alert('ppgDebug: i='+i+' document.form1.pbox0.checked='+document.form1.pbox0.checked);

    cs_odbset( pdpath, debug_ppg); // ODBSet( pdpath, debug_ppg)
    build_ppg_params();

    // hide RefreshTuneList button unless debug_ppg is set
    if(debug_ppg==1)
      document.getElementById("rtl").innerHTML= remember_rtl;
    else
      document.getElementById("rtl").innerHTML= "";
}









function write_date()
{
  var text;
  var mydate=new Date()
  var time_now = mydate.getTime() /1000 ; // present time in seconds
  var year=mydate.getYear()
  if (year < 1000)
   year+=1900
  var day=mydate.getDay()
  var month=mydate.getMonth()
  var daym=mydate.getDate()
  var hour=mydate.getHours()
  var min=mydate.getMinutes()
  var sec=mydate.getSeconds()
  if (daym<10)
   daym="0"+daym
  if (hour<10)
   hour="0"+hour
  if (min<10)
   min="0"+min
  if (sec<10)
   sec="0"+sec
  var dayarray=new Array("Sun","Mon","Tue","Wed","Thur","Fri","Sat")
  var montharray=new Array("Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec")
  text = 'Page last fully reloaded at '
  text+= dayarray[day]+ " "+ montharray[month]+ " "+ daym+" "+  hour+ ":"+ min+ ":"+ sec+" ", year
  document.getElementById("time").innerHTML = text;
}


function add_truevalue(text, mode_value, true_value)
  {
      var index;
      var pattern_X = /^0x/;
	 
      if(pattern_X.test(true_value)) // hex
	  true_value=parseInt(true_value)

      if(true_value == mode_value)
	  index=0;   // blanchedalmond if values are the same
      else
	  index=rstate; // red if running, yellow if stopped

     
     text +='<td class="debug"  style="background-color: '+debug_bgcol[index]+'">'+true_value+'</td>' 
	 return text;
  }




function get_tune_description()
{
    // get JSON tune description from file ~/online/custom/bn[m/q]r/tune_descriptions.js
    // json_txt defined in above file

   var obj = JSON.parse(json_txt);
   var i,j;
   //  selected_tune = tune_names[document.form1.select_tunes.selectedIndex];
   //  alert('tuneName and description for 2a tune try55 is '+obj.modes[0].tunes[0].tuneName + obj.modes[0].tunes[0].description);

   // alert('length= '+obj.modes.length)
   for(i=0; i<obj.modes.length; i++) // look for this ppg_mode
       {
	   if(obj.modes[i].ppgmode == ppg_mode)
	   {
	       //  alert('found '+ppg_mode+ ' at index '+i+'; now looking for tune '+selected_tune );
	       for(j=0; j<obj.modes[i].tunes.length; j++) // look for this tune
	       {
		   if(obj.modes[i].tunes[j].tuneName == selected_tune)
		   {
		       //  alert('get_tune_description: found  tune '+selected_tune+  ' at index '+j+' Description is '+obj.modes[i].tunes[j].description );
                      return (obj.modes[i].tunes[j].description);
                   }
                   
		       
               }
              
	   }
       }
   return("");
}

function show_notes()
{
   if (typeof Pop3Win !='undefined')
   {	  
       //alert('Pop3Win= '+Pop3Win);
       if(Pop3Win && !Pop3Win.closed)
	   Pop3Win.close();
    }  
    Pop3Win = window.open ('','','height=600,width=1500,scrollbars="yes",menubar=no,toolbar=no',true);
    if(Pop3Win == null)
    {
        alert('Cannot open popup window')
        return;
    }
    Pop3Win.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">')
    Pop3Win.document.write('<html><head><title>Notes about Mode Params '+ppg_mode+'</title>');
    Pop3Win.document.write('<h2>Notes on internals of PPG Mode Parameters for BNM/QR</h2>')
    Pop3Win.document.write('<ul>')
    Pop3Win.document.write('<li>The PPG <b>Mode</b> Parameters shown on the Run Parameters Custom page are stored in the ODB (at Key <i>/equipment/FIFO_acq/mode parameters/Mode...</i>)</li>')
    Pop3Win.document.write('<li>Each PPG Mode has its own unique set of <b>Mode</b> Parameters</li>')
    Pop3Win.document.write('<li>The user may edit the  <b>Mode</b> Parameters, or reload stored sets of values (i.e. <b>Tunes</b>) while the run is stopped.</li>')
    Pop3Win.document.write('<br>')
    Pop3Win.document.write('<ul>')
    Pop3Win.document.write('<li>The list of <b>Tunes</b> is in the ODB (at Key <i>/tunes</i>). The Tune files are stored on disk.</li>');
    Pop3Win.document.write('<li>The Run Parameters Custom page uses the perlscript <i>tunes.pl</i> to list, load, create, delete and rename the  <b>Tunes</b></li>');
    Pop3Win.document.write('<li>A short description may be associated with each tune (also handled by <i>tunes.pl</i>). </li>')
    Pop3Win.document.write('<li>The tune descriptions are stored in JSON format in a file on disk</li>')
    Pop3Win.document.write('</ul>')
    Pop3Win.document.write('<br>')
    Pop3Win.document.write('<li>The <b>Mode</b> Parameter values are copied to <b>"true"</b> values at begin-of-run by client <i>rf_config</i>.</li>')
    Pop3Win.document.write('<li>The  <b>"true"</b> values are common to several PPG Modes, and are located in the ODB (most at Key <i>/equipment/FIFO_acq/frontend/input</i>)</li>')
    Pop3Win.document.write('<li>When running, <b>Mode</b> values and  <b>"true"</b> values should therefore be identical.</li>')
    Pop3Win.document.write('<li>The DAQ clients (frontend, mdarc etc.) use only the  <b>"true"</b> values to run the experiment.</li>')
    Pop3Win.document.write('<li>When <i>rf_config</i> runs at begin-of-run, the current Mode Parameters are stored as a hidden Tune named "last".&nbsp');
    Pop3Win.document.write('This allows the last set of valid run parameters to be automatically loaded by default when a particular PPG Mode is selected.</li>');

    Pop3Win.document.write('</ul>')
 
    Pop3Win.document.write('<h3>PSM Parameters:</h3>')
    Pop3Win.document.write('<ul>')
    Pop3Win.document.write('<li>PSM Parameter values are stored with the Mode Parameters for each PPG Mode that use the RF</li>') 
    Pop3Win.document.write('<li>A  <b>Tune</b> consists of a set of stored Mode and PSM Parameters for PPG Modes that use RF</li>');
    Pop3Win.document.write('<li>There is no separated "Mode" area for the PSM Parameters, which are always <b>"true"</b> values</li>');
    Pop3Win.document.write('</ul>')
    Pop3Win.document.write('<br>')
    Pop3Win.document.write('&nbsp**&nbsp </span>"Dual Channel Mode" (when shown) is not saved as a Mode Parameter') 
    Pop3Win.document.write('<br>')

// </th></tr>	    
    Pop3Win.document.write('<input name="true_info" value="Close" type="button"  style="color:firebrick" onClick="  window.close() ;">')
    Pop3Win.document.write('</body></html>')
   return;
   
}
 
function show_notes_const1f_wiki()
{
    // decided not to use for now
    alert (" show_notes_const1f()");
  if (typeof Pop3Win !='undefined')
   {	  
       //alert('Pop3Win= '+Pop3Win);
       if(Pop3Win && !Pop3Win.closed)
	   Pop3Win.close();
    }  
    Pop3Win = window.open ('https://www.triumf.info/wiki/DAQwiki/index.php/BNMR:PPG_MODE_1f#Constant_time_between_PPG_cycles','','height=900,width=2050,scrollbars=yes,menubar=no,toolbar=no',true);
    if(Pop3Win == null)
    {
        alert('Cannot open popup window')
    }
  
    return;

}
function show_notes_const1f()
{
   if (typeof Pop3Win !='undefined')
   {	  
       //alert('Pop3Win= '+Pop3Win);
       if(Pop3Win && !Pop3Win.closed)
	   Pop3Win.close();
    }  
    Pop3Win = window.open ('','','height=900,width=1050,scrollbars="yes",menubar=no,toolbar=no',true);
    if(Pop3Win == null)
    {
        alert('Cannot open popup window')
        return;
    }
    Pop3Win.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">')
    Pop3Win.document.write('<html><head><title>Notes about Mode '+ppg_mode+'(const1f)</title>');
    Pop3Win.document.write('<h2>Constant time between cycles ("const1f" mode)</h2>')
	Pop3Win.document.write('<small>')
	Pop3Win.document.write('<p style="font-size:120%;font-weight:bold; color:blue;text-align:left;">Supported by Modes 1f and 1w</p>');
    Pop3Win.document.write('<p>For all other modes, the PPG runs one cycle and stops. The data are read from the scaler, ')
    Pop3Win.document.write('the histograms sent out, and the PPG restarted. The DAQ Service Time is not used (i.e. set to zero).');

    Pop3Win.document.write('<h3>Const1f mode</h3>')
    Pop3Win.document.write('<p>In this mode, the PPG is <b>NOT</b> restarted after each cycle. Instead, it runs continuously in a loop') 
    Pop3Win.document.write(' (except when restarted as described below). At the end of each cycle, the PPG delays for the ')
    Pop3Win.document.write(' DAQ Service time before restarting the next cycle. ');  
    Pop3Win.document.write('The scaler data are read out during the DAQ Service time, and threshold and other checks are run. If these fail, the cycle ')
    Pop3Win.document.write('is repeated using the "step back procedure" (see below). Otherwise the frequency is incremented.')
    Pop3Win.document.write('Histogrammed data are sent out while the scaler is collecting data for the next PPG cycle.')
    Pop3Win.document.write('<h3>DAQ Service Time</h3>')
    Pop3Win.document.write('<p>The time taken by the frontend code at the end of each cycle to read the data and perform ')
    Pop3Win.document.write('  any system functions is variable, ')
    Pop3Win.document.write('and will sometimes exceed the DAQ service time programmed into the PPG. ')
    Pop3Win.document.write('To recover, the cycle will be considered to be out-of-tolerance, and the out-of-tolerance procedure ')
    Pop3Win.document.write('(see below) will automatically commence. ');
    Pop3Win.document.write('Adjust the PPG DAQ service time to reduce the number of times this occurs (i.e the number of repeated cycles) to  ')
    Pop3Win.document.write('an acceptable level ')
    Pop3Win.document.write('(e.g. < 0.5 % of all PPG cycles) while maintaining a reasonably short dead time. ')
    Pop3Win.document.write('<br><b>The actual service time taken by the software likely depends on factors such as</b>')
    Pop3Win.document.write('<ul><li>polling/housekeeping by the midas frontend,</li><li>the network activity,</li> <li>number of bins enabled,</li> ')
    Pop3Win.document.write('<li>number of channels of the scalers being read etc.</li></ul> ')
	Pop3Win.document.write('  <b>120-180ms</b> has been found to be reasonable depending on the conditions. The minimum time is  '+min_daq_service_time+'ms ')
	Pop3Win.document.write('which is a constant set in the ODB. Any values less than this will automatically be set to the minimum value. ') 
    Pop3Win.document.write('Users may monitor the number of repeats and the average/maximum/minimum time taken by the software by observing <b>DAQ Busy statistics</b> on the main status page.')
    
    Pop3Win.document.write('<h3>Out-of-tolerance procedure</h3>')
    Pop3Win.document.write('<p>Out-of-tolerance procedure is to return to the <b>previous</b> frequency, and repeat the cycle ') 
    Pop3Win.document.write('until return to tolerance. Data from the repeated in-tolerance cycle is discarded, ')
    Pop3Win.document.write('frequency is then incremented, and the frequency scan proceeds as usual')
    Pop3Win.document.write('<p> Constant time between cycles cannot be maintained when the helicity is flipped due to helicity flip sleep time. ')
    Pop3Win.document.write('Therefore, the PPG is stopped and restarted for each helicity flip.')
    Pop3Win.document.write('<p>Supercycles are not supported in this mode, i.e. number cycles/scan increment must be 1.')
    Pop3Win.document.write('<p>A VMEIO32 board must be present with Input 5 connected to PPG output "Daq Service Time". ')
    Pop3Win.document.write('Two output signals from the VMEIO32 board are provided for debugging (on an oscilloscope): ')
    Pop3Win.document.write('<ul><li>Output 5 is set when the histograms are being sent out') 
    Pop3Win.document.write('<li>Output 6 is set when frontend is running in cycle (waiting for PPG to step through the time bins). ')
    Pop3Win.document.write('<li>Output 7 is set when frontend is busy (reading scalers, filling histograms). ')
    Pop3Win.document.write('</ul><br></span>')
	Pop3Win.document.write('<p><center>')
   
    Pop3Win.document.write('<input name="1finfo" value="Close" type="button"  style="color:firebrick" onClick="  window.close() ;">')
    	Pop3Win.document.write('</center>')
    Pop3Win.document.write('</body></html>')


   return;
   
}





function show_tune_descriptions()
{
    // show JSON tune descriptions from file ~/online/custom/bn[m/q]r/tune_descriptions.js
    // json_txt defined in above file

   var obj = JSON.parse(json_txt);
   var i,j;
   var my_experiment;
   var table_flag=0;
   //  selected_tune = tune_names[document.form1.select_tunes.selectedIndex];
   //  alert('tuneName and description for 2a tune try55 is '+obj.modes[0].tunes[0].tuneName + obj.modes[0].tunes[0].description);

   // open popup window
   if (typeof Pop2Win !='undefined')
   {	  
      if(Pop2Win && !Pop2Win.closed)
        Pop2Win.close();
   }  
   Pop2Win = window.open ('','','height=400,width=800,scrollbars=yes,menubar=no,toolbar=no',true);
   if(Pop2Win == null)
   {
      alert('Cannot open popup window')
      return;
   }

           
   Pop2Win.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">')
   Pop2Win.document.write('<html><head><title>Tune descriptions for PPG Mode '+ppg_mode+'</title>');
	 
   Pop2Win.document.write('<style type="text/css">')
       // alert('bnmr_expt='+bnmr_expt)
   if (bnmr_expt) // true if bnmr
   {
       my_experiment = "&beta;-nmr"
       Pop2Win.document.write('.header { color: white ; background-color: MediumSlateBlue; font-weight: bold; font-size: 150%; }')
       Pop2Win.document.write('.title  { color: black; background-color: LightSteelBlue; font-weight: bold; }')
   }
   else
   {
       my_experiment = "&beta;-nqr"
       Pop2Win.document.write('.header { color: white ; background-color:lightseagreen; font-weight: bold; font-size: 150%; }')
       Pop2Win.document.write('.title { color: black ; background-color: aquamarine; font-weight: bold; }')
   }
   Pop2Win.document.write('</style>')
   Pop2Win.document.write('</head><body>')


   //alert('show_tune_descriptions:  obj.modes.length= '+obj.modes.length)
   for(i=0; i<obj.modes.length; i++) // look for this ppg_mode
   {
       if(obj.modes[i].ppgmode == ppg_mode)
       {
	   // alert('show_tune_descriptions: found '+ppg_mode+ ' at index '+i)
	     
           table_flag=1; // table is opened
	   Pop2Win.document.write('<table style=" text-align: left; width: 100%;" border="1" cellpadding="2" cellspacing="4">')
	   Pop2Win.document.write('<tbody><tr>')
           Pop2Win.document.write('<td colspan=2 class=header>')
	   Pop2Win.document.write('Tune descriptions for '+my_experiment+'  PPG Mode '+ppg_mode+'<td></tr>')
           Pop2Win.document.write('<tr><td class= "title">Tunename</td>')
	   Pop2Win.document.write('<td class="title"> Description</td></tr>')
		   

	   for(j=0; j<obj.modes[i].tunes.length; j++) // list the tunes except "last" which is a place marker
	   {
               if(obj.modes[i].tunes[j].tuneName == "last") continue;
	       Pop2Win.document.write ('<tr><td>'+ obj.modes[i].tunes[j].tuneName +'</td>')
	       Pop2Win.document.write ('    <td>'+ obj.modes[i].tunes[j].description +'</td></tr>')
		   
	       //  alert('show_tune_descriptions: found  tune '+obj.modes[i].tunes[j].tuneName+ ' at index '+j+' Description is '+obj.modes[i].tunes[j].description );
	   } // end of for list loop
       }
   } // for loop on ppg_modes
   
   if(table_flag)
       Pop2Win.document.write('</table>')
   else
       Pop2Win.document.write('<br><h2>No Tune descriptions are defined for this mode</h2><br>');

   Pop2Win.document.write('<center><input type="button" value="close window" onclick="self.close();"></center>') 
   Pop2Win.document.write('</body></html>')
	  
   return;
}


function ODBcs(cmd)
{
   var value, request, url;
   
   var request = XMLHttpRequestGeneric();
    url = ODBUrlBase
   if(redir_path!=undefined)
    url +=  '?redir='+redir_path
 //url += ODBUrlBase + '?redir=CustomStatus&&customscript='+ encodeURIComponent(cmd);
    url+='&customscript='+ encodeURIComponent(cmd);
    //  alert('url='+url)
   request.open('GET', url, true); // async

   request.send(null);
   // alert('url='+url+'request.status='+request.status)
 //      if (request.status != 200 || request.responseText != 'OK') 
//        alert('ODBcs error:\nHTTP Status: '+request.status+'\nMessage: '+request.responseText+'\n'+document.location) ;
//   if (request.status != 200)
//       alert('ODBcs Error calling Custom Script "'+cmd+'" :\nHTTP Status: '+request.status)
}

function set_global_debug(val)
{
    gbl_debug=get_bool(val);
    // alert('val='+val+' document.form1.gdbbox.checked='+ document.form1.gdbbox.checked)
      
    document.form1.gdbbox.checked = gbl_debug
    cs_odbset(gbl_debug_path, gbl_debug);
    if(gbl_debug)
	document.getElementById('tdebug').innerHTML=  remember_tdebug; // title
    else
    {
        document.getElementById('gdebug').innerHTML="";  // clear area
	if(!enable_psm_debug_params)
	    document.getElementById('tdebug').innerHTML=""; // clear title
    }
    
}

// end of functions

