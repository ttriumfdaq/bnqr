// This code for BNQR using PSM
// 
//  Only profiles 1f (and fref for a test) are used; profiles 3f,5f not shown
//  This code for use on BNQR only - BNMR now uses PSMIII
//

// PSM parameter constants

// These functions for PSM or PSMII
var profile_path = new Array ();
var num_profiles_enabled=0;// default
var profiles=["one-freq","three-freq","five-freq","freq REF"];
var profile=["one_f","three_f","five_f","fref"];
// PSM Arrays
var profile_enabled = new Array ();
var profile_path_array   = new Array ();   // psmbox0, psmbox1  profile_enabled checkboxes
var profile_enabled_path = new Array (); // paths for profile enabled

// PSM profile colour arrays
var profile_bgcolours=["lightblue","mediumpurple", "pink","turquoise"]
var profile_colours=["black","white","black","black"]
var profile_class= ["onef","threef","fivef","fref"];
var profile_class_iq=["onef_iq","threef_iq","fivef_iq","fref_iq"];
var profile_class_b=["onef_b","threef_b","fivef_b","fref_b"];
var profile_class_s=["onef_s","threef_s","fivef_s","fref_s"];
var profile_class_g=["onef_g","threef_g","fivef_g","fref_g"];


// ======================================================================================
//   P S M  TABLE
// ======================================================================================
function build_psm_params()
{
    var i;
    var pattern1 = /1f/;  // PPG Mode "1f"
    var text="";
    var cols;
    var paths=new Array(); var values=new Array(); //  arrays for async_odbset
    var index=0; //  index for async_odbset arrays (paths,values)
    progressFlag= progress_build_psm;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:lime;font-weight:bold"> build_psm_params: starting</span>'   

    num_profiles_enabled=0;
    if(!have_psm)
    {
	document.getElementById("PSMparams").innerHTML='<tr><td class="allpsmtbg">This mode does not use RF and this is a spacer</td></tr>';
        document.getElementById("PSMparams").className="psmtbg";
        document.getElementById("dbg").innerHTML=""; // debug params

        if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> build_psm_params: and ending "       
	progressFlag=progress_build_psm_done;
   	return;
    }

   // P S M  
    //  alert(' document.getElementById("PSMparams").innerHTML='+ document.getElementById("PSMparams").innerHTML);
  
    //  alert('remember_PSMparams = '+remember_PSMparams)  
    if( document.getElementById("PSMparams").className=="white")
        document.getElementById("PSMparams").className=="psm_title"
	// document.getElementById("PSMparams").innerHTML=remember_PSMparams;

       
  
   // All PPG Modes which use RF  use only one PSM profile (one-f) except ppg_mode "1f" which may use fREF as well (i=3)
   // PSM profiles three_f and five_f are not shown

     text = '<tbody>'
     text += '<tr align="center">'
     text += '<th class="psm_title" id="psm_title" colspan="4">RF Module  ('+psm_module+') Parameters</th>'
     text +='</tr>'
  

	    // assign_profile_paths();   move to initialize
	    // get_profile_enabled();    move to initialize
	    //  alert(' build_psm_table:  profile_enabled array is '+profile_enabled)
    
    // Only show fREF profile (i==3) if  ppg_mode == "1f")
    show_fref=0;
    
    // alert('ppg_mode='+ppg_mode+'  pattern1.test(ppg_mode)= '+ pattern1.test(ppg_mode))
    if (  pattern1.test(ppg_mode))
	show_fref=1;
  
    // alert('ppg_mode "'+ppg_mode+'" show fref '+show_fref+ ' index='+index)
  
    
    cntr_start=0; // initialize line counter
    
    // get paths and information whether enabled or not
    // get_psm_path_arrays();  // get path arrays parameters   (calls  get_psm_quad_path_arrays )  move to initialize
    get_psm_data_arrays(); // get the psm data into its arrays
    if ( !profile_enabled[0] && !profile_enabled[1] && !profile_enabled[2]   && !profile_enabled[3] )
    {   // none are enabled
       num_profiles_enabled = 0;
       text +='<tr>'
       for (i=0; i<4; i++)
       {
         if (i==0 || ( i==3 && show_fref) )
	 {
	    text += '<th  style="vertical-align:top">'
	    text += '<table id="profile'+i+'" style="width: 100%;" border="0" cellpadding="1" bgcolor="white">'  // Table for each profile
				// alert('opened psm table for profile '+i)
            text +='<tbody>'
            text +='<tr align="center">'
            text +='<th style="background-color: '+profile_bgcolours[i]+'; color: '+profile_colours[i]+'; " colspan="2">Profile <i>"'+profiles[i]+'"</i></th>'
            text +='</tr>'

 
            text +='<tr> <td id= "psm_on'+i+'"  class="'+profile_class[i]+'"> Enabled</td>'
            if (rstate == state_stopped) // stopped
            {
               text +='<td class="'+profile_class[i]+'">'
               // use checkboxes psmbox0, psmbox1
              
               if(i==0) 
	           text +='<input  name="psmbox0"  type="checkbox"  onClick="enable_profile('+i+', this.checked?\'1\':\'0\')">'

                if(i==3 && show_fref )
       	            text +='<input  name="psmbox1"  type="checkbox"  onClick="enable_profile('+i+', this.checked?\'1\':\'0\')">'
                text +='</td>';
             }
	     else // running
                 text +='<td class="'+profile_class[i]+'">'+profile_enabled[i]+'</td>';
             text +='</tr></tbody></table> ' // profile table ends
             text +='</th>'
              //alert('closed psm table for profile '+i)
	  }  //  end of  if (i==0 || ( i==3 && show_fref) )

       }  // end of for loop

       text +='</tr> ' // PSM row ends
       //alert('closed psm table')

    } // end of no profiles enabled
    else
    { // at least one profile is enabled
       
        if(psm_module=="PSMII")
           set_psmii_quad_mode(); // psmii does not work in single tone mode; has to be quad mode always
 
        idle_freq_path = psm_path + "/Idle freq (hz)"
	    idle_freq_hz = EqData.frontend.hardware.psm["idle freq (hz)"] // DWORD ODBGet( idle_freq_path)
 
	if( idle_freq_hz == undefined)
	    alert(' idle_freq_hz is undefined (  idle_freq_path EqData.frontend.hardware.psm["idle freq (hz)"]) ')
	else
		idle_freq_hz  = parseInt(idle_freq_hz);
        table_driven = check_mode(ppg_mode);  // freq table-driven for all mode 2 except 20,2d,2g,2h
        //alert('mode '+ppg_mode+' is frequency table-driven')
        if(table_driven)
	    {    // jump_to_idle is global 
		jump_to_idle = get_bool( EqData.frontend.hardware.psm["freq end sweep jump to idle"]) // ODBGet( jump_to_idle_path)   psm_path + "/freq end sweep jump to idle";
		if(jump_to_idle==undefined)
		    alert('jump_to_idle is undefined ( EqData.frontend.hardware.psm["freq end sweep jump to idle"]) ')
                
            text +='<tr>'
            var col=2; // default 

            text +='<td class=param colspan='+col+'>';

            // TABLE for Idle Freq radio buttons 
            //alert('opened table for idle freq  for profile '+i)
            text +='<table id="tifreq" style="width: 100%;" border="0" cellpadding="1">'
            text+= '<tbody>'
            text +='<tr align="left">'
            text +='<td  class=psm_info colspan=5>This mode loads a table containing the frequency scan values into the PSM<br>&nbsp</td></tr>'

        
            text +='<td id="1star" class=param>After last frequency scan value ...  <span class=asterisk>*</span> </td>'
            text +='<td class="param">'
            if(rstate==state_stopped)
            {
               text +='<input type="radio" name="jiradiogroup" value=0  onClick="set_radio_jump(1)">'
               text +='<span class="it">Jump to idle frequency</span>'
               text +='</td><td class=param>'
               text +='<input type="radio" name="jiradiogroup" value=1  onClick="set_radio_jump(0)">'
               text +='<span class="it">Stay at final frequency</span>'
            }
            else
	    { // running
              // if(pattern_y.test(jump_to_idle))
		if(jump_to_idle) // bool
                  text +='Jump to idle frequency'
               else
                  text +='Stay at final frequency'
	    }
        
	
            text +='</td></tr>'
            text +='</tbody></table>'  // END OF IDLE FREQ TABLE  tifreq
            text +='</td></tr>'
      
		// if (pattern_y.test(jump_to_idle))
		if(jump_to_idle) // bool
            {
	       // load_first is global
		load_first =  get_bool( EqData.frontend.hardware.psm["freq sweep load 1st val in idle"]) // ODBGet( load_first_path) // global
		if(load_first==undefined)
		    alert('load_first is undefined ( EqData.frontend.hardware.psm["freq sweep load 1st val in idle"]) ')
              
               text +='<tr>'

               var col=2; // default

               text +='<td class=param>Use the first frequency scan value as the idle frequency?</td>';
               text +='<td class=param>'

               if(rstate==state_stopped)
                       text +='<input  name="psmbox12"  type="checkbox"  onClick="gbl_code=2; cs_odbset(load_first_path, this.checked?\'1\':\'0\')">'
		   //  text +='<input  name="psmbox12"  type="checkbox"  onClick="ODBSet(load_first_path, this.checked?\'1\':\'0\');update(2)">'
               else
                  text += load_first
               text +='</td></tr>'


               if (pattern_n.test(load_first))
               {

                  // show idle freq
                  text +='<tr>'
                  text +='<td class=param>Idle frequency (Hz)</td>';
                  text +='<td class=param>'

                  if(rstate==state_stopped)
                  {
                     text +='<a href="#" onclick="myODBEdit(idle_freq_path, 2, idle_freq_hz  )" >'
                     text +=idle_freq_hz;
                     text +='</a>';
                     text +='' ;
                   }
                   else
                      text +=idle_freq_hz
                   text +='</td></tr>'
	        } //  end of not load_first
        
	      } // end of jump-to-idle

       } // end of  freq table-driven
       else
       { // NOT freq table-driven

          col=2; // default
          text +='<tr>'
          pattern_scan = /1[abfg]/;  // Type 1 with a frequency scan
          if(pattern_scan.test(ppg_mode))
          {
             if(show_fref)
             {
                col=4; // fref as well
                text +='<td class=param colspan='+col+'><i>'
                text +='<ul>'
                text +='<li>PPG Parameters for Frequency Scan (Stop/Start/Incr) control the frequency range</li>';
                text +='<li>If required, enter Reference (Fixed) frequency under Profile fREF below</li>';
                text +='</i></td></tr>';
              } 
	   } // end of modes with freq scan
           else
	   {  // no freq scan
               text +='<td class=param>Frequency (Hz)</td>';
               text +='<td class=param>'
          
               if(rstate==state_stopped)
               {
                 text +='<a href="#" onclick="myODBEdit(idle_freq_path, 2,	idle_freq_hz )" >'
                 text +=idle_freq_hz;
                 text +='</a>';
                 text +='' ;
               }
               else
                  text +=idle_freq_hz
               text +='</td></tr>'
	    } // end of no freq scan ppg modes
       
       } // end of NOT frequency table-driven
  
     
    
       // note - line_cntr removed. Replaced by <th  style="vertical-align:top"><table... >

       // alert(cntr_start)   cntr_start incremented in  write_freq_scan_params
       fref_en = 0;
       if (profile_enabled[3] ) // Fref
       {
          cntr_start++; // fREF tuning freq
          fref_en = 1; // flag
       }
      
     
       for (i=0; i<4; i++)
       {  // for loop
          if(i==0 || (i==3 && show_fref) )
          {  // calculate 2 profiles only      
	
             if (profile_enabled[i] )
             {
		num_profiles_enabled++;
                convert_all_quad(i);  // convert y/n to 1/0 for all Quad boolean parameters for index i

             } // profile enabled
          } // 2 profiles only
       } // end of for loop

 

       text +='<tr>'  // two profiles are side-by-side
       // TABLE for each profile

       for (i=0; i<4; i++)   //   MAIN LOOP on PROFILES
       {
          if (i==0 || (i==3 && show_fref))  // only Max of TWO profiles are shown
          {

             text +='<th colspan="2"   style="vertical-align:top">'
              
             text +='<table id="profile'+i+'" style="width: 100%;" border="0" cellpadding="1" bgcolor="white">'  // Table for each profile
             text +='<tbody>'
             text +='<tr align="center">'
             text +='<th style="background-color: '+profile_bgcolours[i]+'; color: '+profile_colours[i]+';" colspan="2">Profile <i>"'+profiles[i]+'"</i></th>'
             text +='</tr>'
	      

             text +='<tr>'
             if(rstate==state_stopped)       
             {
		 //  if(!hole_burning_flag && ppg_mode=="20")
		 //  text +='<td class="error"> Enabled</td>'
		 //  else
	
                 text +='<td  id="psm_on'+i+'" class="'+profile_class[i]+'"> Enabled</td>'
                
                 text +='<td class="'+profile_class[i]+'">'
                 // use checkboxes psmbox0, psmbox1

		
		    
                 if(i==0)
                     text +='<input  name="psmbox0"  type="checkbox"  onClick="enable_profile('+i+', this.checked?\'1\':\'0\')">'

		     //  text +='<input  name="psmbox0"  type="checkbox"  onClick="ODBSet(profile_path_array['+i+'], this.checked?\'1\':\'0\');build_psm_params(); check_consistency();">'


                 if(i==3 && show_fref )
		    text +='<input  name="psmbox1"  type="checkbox"   onClick="enable_profile('+i+', this.checked?\'1\':\'0\')">'
                     
 
                 text +='</td>';           
          
             }
             else  // running
             {
	        if  ( profile_enabled[i]) 
                    text +='<td class="'+profile_class[i]+'" align="center" colspan=2 > Enabled </td>'
                else
                    text +='<td class="'+profile_class[i]+'"  align="center"colspan=2 > Disabled </td>'
             }

             text +='</tr>'
    

             if (profile_enabled[i] )
             { 

                if (profile_enabled[3]) // fref is enabled so we need an extra row
                {
                   if(i==3 && show_fref)
	           {  // fref Tuning FReq

		      fref_tuning_freq_path = psm_path +"/fREF Tuning freq (hz)"
		      fref_tuning_freq = EqData.frontend.hardware.psm["fref tuning freq (hz)"]//DWORD  ODBGet( fref_tuning_freq_path);
		      if( fref_tuning_freq == undefined)
			  alert('fref_tuning_freq is undefined ( EqData.frontend.hardware.psm["fref tuning freq (hz)"])  ')
		      else
			  fref_tuning_freq = parseInt( fref_tuning_freq)
                      text +='<tr>'    // tuning frequency
                      text +='<td class="fref_tun">Reference (fixed) Frequency (Hz)</td>'
                      text +='<td class="'+profile_class[i]+'">'
                      if(rstate==state_stopped)
                      {
                         text +='<a href="#" onclick="myODBEdit( fref_tuning_freq_path,   2,  fref_tuning_freq )" >'
                         text += fref_tuning_freq;
                         text +='</a>';
                         text +='' ;
                      }
                      else // running
                         text += fref_tuning_freq
                      text +='</td></tr>'
                     } // end of (i==3 && show_fref)

		   else // blank this line
                      text +='<tr><td class="allwhite" colspan="2">&nbsptuning freq </td></tr>'

		}  // end of profile_enabled[3]
	   

// ================================= ROW A ==================================================

          
             text +='<tr>'    // Amplitude row A
             // check value of Amplitude
             if(amplitude[i] > 255 )
             {
		alert('Maximum value of amplitude is 255');
                paths[index]=amplitude_path[i];  values[index]=255;  //ODBSet(amplitude_path[i], 255)
                index++;
                amplitude[i]=255;
             }
             else if(amplitude[i] < 0 )
             {
		alert('Minimum value of amplitude is 0 ');
                paths[index]=amplitude_path[i]; values[index]=0;  //ODBSet(amplitude_path[i], 0)
                index++;
                amplitude[i]=0;
             }


	     if(amplitude[i]==0 )   // enabled with zero amplitude... no RF
		       my_class="error";
	      else
                my_class=profile_class[i];
            
             
             text +='<td  class="'+my_class+'">Amplitude (0-255) </td>'
             if(rstate==state_stopped)
             {  

                        // document.write ('<td class="'+profile_class[i]+'">'
                text +='<td class="'+my_class+'">'

                text +='<a href="#" onclick="myODBEdit(amplitude_path['+i+'],  2, amplitude[i])"> '
                text += amplitude[i];
                text +='</a>';
                text +='' ;
            }
            else  // running
               text +='<td class="'+profile_class[i]+'">'+amplitude[i];
            text +='</td></tr>'
      

// ================================= ROW B ==================================================
  
             text +='<tr>'  // Quad Mode row B

            

             if(rstate==state_stopped)
             {
                text +='<td colspan=2  class="'+profile_class_b[i]+'">'

               // TABLE for QUAD/SINGLE TONE 
                //alert('opened  table for quad/single tone profile '+i)
                text +='<table id="qst'+i+'" style="width: 100%;" border="1" cellpadding="1">'
                text +='<tbody>'
                text +='<tr align="left">'

                if(psm_module=="PSM") //  true single tone mode is supported 
                {    
                   //Radio quad buttons
                   text +='<td class="'+profile_class_b[i]+'">Enable Quadrature Mode? </td>'
                   text +='<td id="qmb" class="'+profile_class_b[i]+'">'

                   text +='<input type="radio" name="quadradiogroup'+i+'" id="Quad"  onClick="radio_quad(1,'+i+');"></td>'
                   text +='</td>'

                   text +='<td class="'+profile_class_s[i]+'">Enable Single Tone Mode?</td>'
                   text +='<td id="smb" class="'+profile_class_s[i]+'">'
                   text +='<input type="radio" name="quadradiogroup'+i+'" id="SingleTone"  onClick="radio_quad(0,'+i+');"></td>'

	         
		  
                }  // end of PSM

 
                else // PSMII    single tone mode must be simulated 
                { 
                     //Radio quad buttons
                    
                     text +='<td class="'+profile_class_b[i]+'">Quadrature Mode? </td>'
                     text +='<td id="qmb" class="'+profile_class_b[i]+'">'

                     text +='<input type="radio" name="quadradiogroup'+i+'" id="Quad"  onClick="radio_quad(1,'+i+');"></td>'

                     text +='<td id="two_stars" class="'+profile_class_s[i]+'">Single Tone Mode? <span class=asterisk>**</span></td>'
                     text +='<td id="smb" class="'+profile_class_s[i]+'">'
                     text +='<input type="radio" name="quadradiogroup'+i+'" id="SingleTone"  onClick="radio_quad(0,'+i+');"></td>'
                     
		      
                   } // end  PSMII

                   //alert('closing  table for quad/single tone profile '+i)
		text +='</tr></tbody></table>'  // table id="qst"
 
                   // ENDED  TABLE for QUAD/SINGLE TONE  

              }
              else  // running
              {  
                 if(psm_module=="PSM") //  true single tone mode is supported 
                 {
                    text +='<td class="'+profile_class_b[i]+'">Enable quadrature mode?</td>'
                    text +='<td class="'+profile_class_b[i]+'">'+enable_quad[i];  
                 }
                 else // PSMII
                 { 
                    text +='<td class="'+profile_class_b[i]+'">Simulated single tone mode?</td>'
                    text +='<td class="'+profile_class_b[i]+'">'+simulate_single_tone[i];  
                 }
              } 
                  
             text +='</td></tr>'
  
             sstm_flag = 0 ; // simulate single tone mode flag
             
             if(psm_module=="PSMII")  
             {
                if(simulate_single_tone[i])
                   sstm_flag=1;
             }  
	 
// ================================= ROW C ==================================================

            // do not show quadrature mode params for simulated single channel mode
       
             if(enable_quad[i]  &&  !sstm_flag )
	     {         
              

	        text +='<tr>'  // Modulation Mode  row C
                lc_moduln_mode = moduln_mode[i].toLowerCase();
                var pattern_mod_mode= /ln-sech|hermite/; // either
      
                if (  pattern_mod_mode.test(lc_moduln_mode) )
                   my_class=profile_class[i];
                else   // shouldn't happen with radiogroup
                   my_class="error";
                     //text +='<td class="'+profile_class[i]+'">Modulation Mode</td>'
                   text +='<td class="'+my_class+'">Modulation Mode</td>'

                if(rstate==state_stopped)
                { 
                  text +='<td class="'+profile_class[i]+'">'
		  text +='<input type="radio" name="mmradiogroup'+i+'" value=0  onClick="cs_odbset(moduln_mode_path['+i+'],ln_sech_string)">'
                  text +='<span class="it">ln-sech </span>'

                  text +='<input type="radio" name="mmradiogroup'+i+'" value=1  onClick="cs_odbset(moduln_mode_path['+i+'],hermite_string)">'
                  text +='<span class="it">Hermite</span>'
               }
                else // running
                   text +='<td>'+moduln_mode[i]+'</td>';  

                text +='</tr>'


// ================================= ROW D ==================================================

 

                text +='<tr>'  // Requested Bandwidth  row D

                text +='<td class="'+profile_class[i]+'">Requested bandwidth (Hz)</td>'

                if(rstate==state_stopped)
                { 
                   text +='<td class="'+profile_class[i]+'">'

                   text +='<a href="#" onclick="myODBEdit(bandwidth_path['+i+'], 2, bandwidth[i])" >'
                   text +=bandwidth[i]
                   text +='</a>';
                   text +='' ;
                   text +='</td>';
                 }
                 else // running
                    text +='<td class="'+profile_class[i]+'">'+bandwidth[i]+'</td>';  
                text +='</tr>'


// ================================= ROW E ==================================================

                 text +='<tr>'  // Load i,q pairs file  row # E

                 text +='<td class="'+profile_class_b[i]+'">Load i,q pairs file?</td>'


                 // uses checkboxes  psmbox6,7

                 if(rstate==state_stopped)
                 {  
                    text +='<td class="'+profile_class_b[i]+'">'
                    if(i==0)
		        text +='<input  name="psmbox6"  type="checkbox"  onClick="set_psm_cb_param(load_iq_file_path['+i+'], this.checked?\'1\':\'0\')">'

                   if(i==3 && show_fref )		     
		        text +='<input  name="psmbox7"  type="checkbox"  onClick="set_psm_cb_param(load_iq_file_path['+i+'], this.checked?\'1\':\'0\')">' 
                 }
                 else  // running
		     text +='<td class="'+profile_class_b[i]+'">'+unbool(load_iq_file[i]);  
 
                 text +='</td></tr>'


// ================================= ROW F ==================================================

 
                if (load_iq_file[i])
                {
                    text +='<tr>'  // Jump to Idle  row # F
                    text +='<td colspan=3 class="'+profile_class_b[i]+'">'

                    // TABLE for Jump to Idle IQ radio buttons
                    //alert('opened  table for Jump to Idle IQ radio buttons  profile '+i)
                    text +='<table id="jiiq'+i+'"  style="width: 100%;" border="0" cellpadding="1">'
                    text +='<tbody>'
                    text +='<tr align="left">'
                    text +='<td id="one_star"  class="'+profile_class_b[i]+'">After last i,q pair ...  <span class=asterisk>*</span> </td>'
                    text +='<td class="'+profile_class_b[i]+'">'

                    // Jump to  Idle i,q pair?</td>'
            

                // uses jiqradiogroup0

                if(rstate==state_stopped)
                {   
                 //  text +='<td class="'+profile_class_b[i]+'">'  
                   if(i==0)
		     {		 
                          text +='<input type="radio" name="jiqradiogroup0" value=0  onClick="set_psm_cb_param(jump_to_idle_iq_path['+i+'], 1)">'
                          text +='<span class="small">Jump to idle i,q pair</span>'
                          text +='</td><td class="'+profile_class_b[i]+'">'
                          text +='<input type="radio" name="jiqradiogroup0" value=1  onClick="set_psm_cb_param(jump_to_idle_iq_path['+i+'], 0);">'
                          text +='<span class="small">Stay at final i,q pair</span>'
   
                      
                        //  alert(jump_idle_iq[i] )
                         

                     } // end  if(i==0) 
                   if(i==3 && show_fref )
		     {
                          text +='<input type="radio" name="jiqradiogroup3" value=0  onClick="set_psm_cb_param(jump_to_idle_iq_path['+i+'], 1);">'
		         // text +='<input type="radio" name="jiqradiogroup3" value=0  onClick="ODBSet(jump_to_idle_iq_path['+i+'], 1); update(2);">'
                          text +='<span class="small">Jump to idle i,q pair</span>'
                          text +='</td><td class="'+profile_class_b[i]+'">'
                          text +='<input type="radio" name="jiqradiogroup3" value=1  onClick="set_psm_cb_param(jump_to_idle_iq_path['+i+'], 0 );">'
		       // text +='<input type="radio" name="jiqradiogroup3" value=1  onClick="ODBSet(jump_to_idle_iq_path['+i+'], 0);  update(2);">'
                          text +='<span class="small">Stay at final i,q pair</span>'
   
                         // alert(jump_idle_iq[i] )
                         
                     }   // end if(i==3 && show_fref )

                }
                else // running
                {
                  // text +='<td class="'+profile_class_b[i]+'">'+jump_idle_iq[i]+'</td>'; 
                   if(pattern_y.test(jump_to_idle_iq_path[i]))
                      text +='jump to idle i,q pair'
                   else
                      text +='stay at final i,q pair'
                }
                text +='</td></tr>'
                text +='</tbody></table>'  // END OF TABLE  jiiq

 //alert('closed  table for Jump to Idle IQ radio buttons  profile '+i)
                text +='</td></tr>'

                // end of jump to idle i,q

               } // end of load_iq_file[i]


 // ================================= ROW G ==================================================


                if ( (load_iq_file[i] && jump_idle_iq[i] ) || !(load_iq_file[i]))
                {
                text +='<tr>'  // Idle i row # G

                // Check value
                if(idle_i[i] > 511 )
                {
		   alert('Maximum  i value is 511'); 
	           //ODBSet(idle_i_path[i], 511)
                   idle_i[i] =511;
                   paths[index]=idle_i_path[i]; values[index]=511;
                   index++;
                }
                else if(idle_i[i] < 0 )
                {
		   alert('Minimum  i value is 0'); 
		   // ODBSet(idle_i_path[i], 0)
                   idle_i[i] =0;
                   paths[index]=idle_i_path[i];
                   values[index]=0;
                   index++;
                }

                text +='<td class="'+profile_class[i]+'">&nbsp&nbsp Idle i (0-511)</td>'
                   
                if(rstate==state_stopped)
                {       
                   text +='<td class="'+profile_class[i]+'">'
           
                   text +='<a href="#" onclick="myODBEdit( idle_i_path['+i+'], 2, idle_i[i])" >'
                   text +=idle_i[i]
                   text +='</a>';
                   text +='' ;
                   text +='</td>';
                 }
                 else // running
                   text +='<td class="'+profile_class[i]+'">'+idle_i[i]+'</td>';  
                 text +='</tr>'


// ================================= ROW H ==================================================


                 text +='<tr>'  // Idle q row # H

                 text +='<td class="'+profile_class[i]+'">&nbsp&nbsp Idle q (0-511)</td>'

                 // Check value
                 if(idle_q[i] > 511 )
                 {
		     // ODBSet(idle_q_path[i], 511)
                   idle_q[i] =511;
                   paths[index]=idle_q_path[i];
                   values[index]=511;
                   index++;
                 }
                 else if(idle_q[i] < 0 )
                 {
		   alert('Minimum  q value is 0'); 
		   // ODBSet(idle_q_path[i], 0)
                   idle_q[i] =0;
                   paths[index]=idle_q_path[i];
                   values[index]=0;
                   index++;
                 }


                 if(rstate==state_stopped)
                 { 
                    text +='<td class="'+profile_class[i]+'">'

                    text +='<a href="#" onclick="myODBEdit( idle_q_path['+i+'], 2, idle_q[i])" >'
                    text +=idle_q[i]
                    text +='</a>';
                    text +='' ;
                  
                   text +='</td>';
                    
                 }
                 else // running
                    text +='<td class="'+profile_class[i]+'">'+idle_q[i]+'</td>';  
                 text +='</tr>'

	    } // end of  if ( (load_iq_file[i] && jump_idle_iq[i] ) || !(load_iq_file[i]))


// ================================= ROW I ==================================================

                if (load_iq_file[i])
                {
                 
                 text +='<tr>'  // Set constant i in file  row # I
                 text +='<td class="'+profile_class_iq[i]+'">&nbsp&nbsp Set constant i value in file?</td>'

                  // uses checkboxes  psmbox8,9


                   if(rstate==state_stopped)
                   {   
                       text +='<td class="'+profile_class_iq[i]+'">'
                       if(i==0)	
	                   text +='<input  name="psmbox8"  type="checkbox"  onClick="set_psm_cb_param( set_const_i_path['+i+'], this.checked?\'1\':\'0\')">'		         
			   // text +='<input  name="psmbox8"  type="checkbox"  onClick="ODBSet( set_const_i_path['+i+'], this.checked?\'1\':\'0\');update(2)">'		         

                        
                        if(i==3 && show_fref )	
	                   text +='<input  name="psmbox9"  type="checkbox"  onClick="set_psm_cb_param( set_const_i_path['+i+'], this.checked?\'1\':\'0\')">'		           
			    // text +='<input  name="psmbox9"  type="checkbox"  onClick="ODBSet( set_const_i_path['+i+'], this.checked?\'1\':\'0\');update(2)">'		                                   
                    }
                    else // running
                       text +='<td>'+unbool(set_const_i[i]);  
                    text +='</td></tr>'

                       if(set_const_i[i])
                       {
                          text +='<tr>'  // constant i  row # I
                          text +='<td class="'+profile_class[i]+'">&nbsp&nbsp&nbsp&nbsp Constant i (0-511)</td>'

                          // Check value
                          if(const_i[i] > 511 )
                          {
			     alert('Maximum i value is 511')
				 //ODBSet(const_i_path[i], 511)
                             const_i[i] =511;
                             paths[index]=const_i_path[i];
                             values[index]=511;
                             index++;
                           }
			  else if(const_i[i] <0 )
                          {
			     alert('Minimum  i value is 0')
				 //ODBSet(const_i_path[i], 0)
                             const_i[i] =0;
                             paths[index]=const_i_path[i];
                             values[index]=0;
                             index++;
                           }

                      
                          if(rstate==state_stopped)
                          {       
                             text +='<td class="'+profile_class[i]+'">'
                          
                              text +='<a href="#" onclick="myODBEdit(const_i_path['+i+'], 2, const_i[i] )" >'
                              text += const_i[i]
                              text +='</a>';
                              text +='' ;
                              text +='</td>';    
                           }
                           else
                             text +='<td class="'+profile_class[i]+'">'+const_i[i]+'</td>';  
                           text +='</tr>'
                    } // end of set const i
                
// ================================= ROW J ==================================================
                    text +='<tr>'  // Set constant q in file  row # J

                    text +='<td class="'+profile_class_iq[i]+'">&nbsp&nbsp Set constant q value in file?</td>'
                 
                    if(rstate==state_stopped)
                    {        
                        text +='<td class="'+profile_class_iq[i]+'">'
                        if(i==0)
                           text +='<input  name="psmbox10"  type="checkbox"  onClick="set_psm_cb_param(set_const_q_path['+i+'], this.checked?\'1\':\'0\')">'
			// text +='<input  name="psmbox10"  type="checkbox"  onClick="ODBSet(set_const_q_path['+i+'], this.checked?\'1\':\'0\');update(2)">'
   
                        if(i==3 && show_fref )	
	                   text +='<input  name="psmbox11"  type="checkbox"  onClick="set_psm_cb_param(set_const_q_path['+i+'], this.checked?\'1\':\'0\')">'
		      // text +='<input  name="psmbox11"  type="checkbox"  onClick="ODBSet(set_const_q_path['+i+'], this.checked?\'1\':\'0\');update(2)">'
                                         
                       }
                       else // running
                          text +='<td class="'+profile_class_iq[i]+'">'+unbool(set_const_q[i]);  
                    text +='</td></tr>'


// ================================= ROW K ==================================================

                    if(set_const_q[i])
                    {
                       text +='<tr>'  // constant q  row # K

                       text +='<td class="'+profile_class[i]+'">&nbsp&nbsp&nbsp&nbsp Constant q (0-511)</td>'
                     
                       // Check value
                       if(const_q[i] > 511 )
                       {
			     alert('Maximum q value is 511')
				 // ODBSet(const_q_path[i], 511)
                             const_q[i] =511;
                             paths[index]=const_q_path[i];
                             values[index]=511;
                             index++;
                        }
                       else  if(const_q[i] < 0 )
                        {
			     alert('Minimum q value is 0')
				 //ODBSet(const_q_path[i], 0)
                             const_q[i] =0;
                             paths[index]=const_q_path[i];
                             values[index]=0;
                             index++;
                        }
                        if(rstate==state_stopped)
                        {   
                            text +='<td class="'+profile_class[i]+'">'
                            text +='<a href="#" onclick="myODBEdit( const_q_path['+i+'], 2, const_q[i])" >'
                            text += const_q[i]
                            text +='</a>';
                            text +='' ;
                           }
                           else
                              text +='<td class="'+profile_class[i]+'">'+const_q[i];  
                           text +='</td></tr>'
                     } // end set const q

                  } // end load_iq_file

	     } // end iq_modulation mode enabled
	 
  
       // GATES 
             text +='<tr id="psmGates'+i+'"><td>gate control '+i+'</td>'
             text +='</tr>'
 
     } // end profile i enabled
	  
      //alert('closed  table for  profile '+i)
      text +='</tbody></table>'  // closed table id="profile.."
      text +='</th>'

      } // if i==0 or 3
   } // for loop

   text +='</tr> ' // PSM row ends

  } // one or more profiles enabled

  if(num_profiles_enabled > 0)
  {
     text += '<tr  id="footnotes">'
	 text += '<th colspan="5"></th></tr>' // my_footnotes
  }

   text +='<tr><th colspan=2 class="normal"  id="psmerr"></th></tr>'
   text +='</tbody>'

       // alert('build_psm_params:   Final text is '+text);

	  document.getElementById("PSMparams").innerHTML=text;  // replace <table id="PSMparams" > by text
   // alert('build_psm_params:   Final text is '+text);
  
	 
   // alert('build_psm_params: show_gate_params='+show_gate_params)
        display_gate_control( show_gate_params); 
       
       initialize_psm_params();
  
        show_psm_debug_params();


   if(index > 0) 
       {
	   // alert('build_psm_params: calling async_odbset with index='+index+'; paths='+paths+'; values='+values)
	   async_odbset(paths,values);  // write to odb if needed
       }

   //  alert('smb='+ document.getElementById("smb").innerHTML)
   if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:lime;"> build_psm_params: ending</span>'
   progressFlag=progress_build_psm_done
return;

}


function  initialize_psm_params()
{
    var i,idx;
    var index; 

    //alert('initialize_psm_params starting')

    if( num_profiles_enabled == 0)
    {
          if(typeof(document.form1.psmbox0) !='undefined')
             document.form1.psmbox0.checked= 0;  // initialize "profile enabled" 1f
          return;
    }
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> initialize_psm_params: starting "
    if(rstate==state_stopped)
    {

       if(typeof(document.form1.psmbox12) !='undefined')
	     document.form1.psmbox12.checked= load_first;  // initialize to the correct value freq sweep load 1st val in idle
         
 if(typeof(document.form1.jiradiogroup) =='undefined')
     //  alert('(typeof(document.form1.jiradiogroup) '+typeof(document.form1.jiradiogroup))
     i=0; // do nothing
  else  
	       // if(typeof(document.form1.jiradiogroup) !='undefined')  #this throws an error
          {
	   
	     document.form1.jiradiogroup[0].checked=  pattern_y.test(jump_to_idle ) 
             document.form1.jiradiogroup[1].checked=  pattern_n.test(jump_to_idle)
          }
	   
       for (i=0; i<4; i++) 
       {
          if(i==1 || i==2 )
	      continue;  // profiles 3f,5f 
    
          if(i==0)
          {
             if(typeof(document.form1.psmbox0) !='undefined')
		 document.form1.psmbox0.checked= profile_enabled[i] //get_bool(ODBGet(profile_path_array[i]));  // initialize "profile enabled" 1f
          }
          else if(i==3  && show_fref )
          {
             if(typeof(document.form1.psmbox1) !='undefined')
		 document.form1.psmbox1.checked=  profile_enabled[i] //get_bool(ODBGet(profile_path_array[i]));  // initialize "profile enabled" fREF
          }



          if(psm_module=="PSM") //  true single tone mode is supported 
          { 
             if(enable_quad[i])   // quad mode
                index=0;
             else                      // single tone
                index=1;
          }
          else // PSMII
          {
             if(simulate_single_tone[i])
                index=1;
             else
                index=0; // true quad mode
          }        
       
          if(i==0)
          {

             if(typeof(document.form1.quadradiogroup0) !='undefined')
		 document.form1.quadradiogroup0[index].checked=1; // initialize  Enable Quad mode   1f
          }


           else if(i==3  && show_fref )
           { 
               if(typeof(document.form1.quadradiogroup3) !='undefined')
                   document.form1.quadradiogroup3[index].checked=1;  // initialize  Enable Quad mode   fREF
           }
             
       
           if(i == 0)
           {
	      if(enable_quad[i]  &&  !sstm_flag )   // ROW C
	      {
                 if(typeof(document.form1.mmradiogroup0) !='undefined')
                 {                                                       // initialize  modulation mode
                     if( pattern_ln_sech.test(lc_moduln_mode) )
			 document.form1.mmradiogroup0[0].checked=  1;
                     else if (  pattern_hermite.test(lc_moduln_mode))
			 document.form1.mmradiogroup0[1].checked= 1;
                 }
	      
                 if(typeof(document.form1.psmbox6) !='undefined') // ROW E
		     document.form1.psmbox6.checked= load_iq_file[i]; //  get_bool(ODBGet(load_iq_file_path[i]));  // initialize load i,q pairs file


                 if (load_iq_file[i])        
                 {
                     if(typeof(document.form1.jiqradiogroup0) !='undefined')    // ROW F
                     {  
                        if(jump_idle_iq[i])
		           document.form1.jiqradiogroup0[0].checked= 1; // initialize jump to idle i,q pair  1f
                        else
		           document.form1.jiqradiogroup0[1].checked= 1; // initialize stay at final i,q pair  1f
                      }


  
                      if(typeof(document.form1.psmbox8) !='undefined')
			  document.form1.psmbox8.checked= set_const_i[i] // get_bool(ODBGet(set_const_i_path[i]));  //  initialize set constant i in file
                      if(typeof(document.form1.psmbox10) !='undefined')
			  document.form1.psmbox10.checked= set_const_q[i] // get_bool(ODBGet(set_const_q_path[i]));  // initialize set constant q in file 
	 	  } // end of  if(load_iq_file[i]
	       } // end of if(enable_quad[i]  &&  !sstm_flag )  
             
               if(show_gate_params)
               {
                  idx = gate_control[i];
                  idx=parseInt(idx);
               
                  if(typeof(document.form1.gateradiogroup0) !='undefined')
                     document.form1.gateradiogroup0[idx].checked = 1;
               }
           } // end of i==0

           else if(i==3 && show_fref)
           {
              if(enable_quad[i]  &&  !sstm_flag )   // ROW C
	      {
                  if(typeof(document.form1.mmradiogroup3) !='undefined')
                  {                                               // initialize modulation mode
                      if( pattern_ln_sech.test(lc_moduln_mode))
			  document.form1.mmradiogroup3[0].checked= 1;
                      else if ( pattern_hermite.test(lc_moduln_mode))
                          document.form1.mmradiogroup3[1].checked= 1;
                  }

                  if(typeof(document.form1.psmbox7) !='undefined') // ROW E
		      document.form1.psmbox7.checked= load_iq_file[i] // get_bool(ODBGet(load_iq_file_path[i]));  // initialize load i,q pairs file

                  if (load_iq_file[i])        
                  {
                     if(typeof(document.form1.jiqradiogroup3) !='undefined') // ROW F
	             {    
                        if(jump_idle_iq[i])
                           document.form1.jiqradiogroup3[0].checked= 1; // initialize jump to idle i,q pair  fref
                        else
                           document.form1.jiqradiogroup3[1].checked= 1; // initialize stay at final i,q pair  fref
		     }

                     if(typeof(document.form1.psmbox9) !='undefined')
			 document.form1.psmbox9.checked= set_const_i[i] // get_bool(ODBGet(set_const_i_path[i]));  // initialize set constant i in file
                     if(typeof(document.form1.psmbox11) !='undefined')
			 document.form1.psmbox11.checked= set_const_q[i] // get_bool(ODBGet(set_const_q_path[i]));  //  initialize set constant q in file
	 	  } // end of  if(load_iq_file[i]
	       } // end of if(enable_quad[i]  &&  !sstm_flag )  

               if(show_gate_params)
               {
                  idx = gate_control[i];
                  idx=parseInt(idx);
               
                  if(typeof(document.form1.gateradiogroup3) !='undefined')
                     document.form1.gateradiogroup3[idx].checked = 1;
               }
          } //  end of (i==3 && show_fref)

      } // for loop
   } // state stopped


    //  alert('Footnotes: ');
   // PSM Footnotes
    if ( profile_enabled[0] || profile_enabled[1] || profile_enabled[2]   || profile_enabled[3] )
    { // at least one profile is enabled
       text ='<td class=footnote colspan=5>'

	   // alert('document.getElementById("1star")='+document.getElementById("one_star"))
	   if((document.getElementById("one_star") != undefined) || (document.getElementById("1star") != undefined))
       {
          text +='<span class=allwhite><br>hi</span>' // spacer
          text +='<span class=asterisk>*</span> if the number of strobes or the gate exceeds the number of values in the table.'
       }
	
       if(document.getElementById("two_stars") != undefined)
       {
          text +='<span class=allwhite><br>hi</span>' // spacer
          text +='<span class=asterisk>**</span> &nbsp&nbspThe PSMII module does not support single tone mode. '
	      text +='<br><span class=allwhite>hi**</span> &nbsp&nbsp' // spacer
          text +='Single tone mode will be simulated by using Quadrature Mode with Idle i,q pair=(511,0)'
       }
       text +='</td>' // footnotes
       document.getElementById("footnotes").innerHTML=text;

     }
     // if no profiles enabled, no footnotes needed; id="footnotes" not written
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="... and ending "
   return;
}

function set_psm_cb_param(path, val)
{   // set a psm parameter at path (called by a checkbox)

    var i=parseInt(val); // value to set (0/1)

    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_psm_cb_param</b>: starting with path="+path+" and value= "+i
    cs_odbset(path, val) 

    gbl_code=2; // for update (build_psm_params)
    if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending with call to update with gbl_code set to "+gbl_code
    update();
}

function display_gate_control(param)
{
  var idx;
  var text;
  param = get_bool(param);

 
  show_gate_params = param; // get_bool(ODBGet(gpath));
  
  if(!have_psm)
     return;

  if(num_profiles_enabled == 0)
      return; // no profiles enabled, no Gates
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> display_gate_control: starting "

  // GATES 
  for(i=0; i<4; i++)
  {
      if(!profile_enabled[0])
          continue;

      if(i==0 || ( i==3 && show_fref) ) 
      {
         if(show_gate_params)
         {
	    text="";
            idx =parseInt( gate_control[i]);
   
            if (rstate != state_stopped) // running
            {
               text +='<td colspan=1 class="'+profile_class_g[i]+'"><big><b>Gate Control:</b>  front panel gate input...</big></td>'
               text +='<td colspan=1 class="'+profile_class_g[i]+'"><big>'+gate_states[idx]+'</big></td></tr>'
            }
            else // stopped
            {
               text +='<td colspan=2 class="'+profile_class_g[i]+'"><big><b>Gate Control:</b>  front panel gate input...</big>'
               text +='<table style="width: 100%;" border="0" cellpadding="5" bgcolor="white">'  // GATE table
               text +='<tbody>'

               text +='<tr>'


               text +='<td  colspan=1 class="'+profile_class_g[i]+'">'
               text +='<input name="gateradiogroup'+i+'"  type="radio" value=0 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],0);">'
	       // text +='<input name="gateradiogroup'+i+'"  type="radio" value=0 onclick="ODBSet(gate_control_path['+i+'],0);update(2);">'
               // gate_control = 0 (disabled);
               if(idx==0) // selected state
                  text +='<span class="error">'+gate_states[0]+'</span><br>'
               else
                  text +=gate_states[0]+'<br>'
               text +='<input name="gateradiogroup'+i+'"  type="radio" value=1 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],1);">'
	       //text +='<input name="gateradiogroup'+i+'"  type="radio" value=1 onclick="ODBSet(gate_control_path['+i+'],1);update(2);">'
               // gate_control = 1 enabled (default)
               text +='<span style="color: green">'+gate_states[1]+'</span></td>'


               text +='<td  colspan=1 class="'+profile_class_g[i]+'">'
               text +='<input name="gateradiogroup'+i+'"  type="radio" value=2 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],2);">'
	       // text +='<input name="gateradiogroup'+i+'"  type="radio" value=2 onclick="ODBSet(gate_control_path['+i+'],2);update(2);">'
               // gate_control = 2 (pulse inverted);
               if(idx==2) 
                 text +='<span class="error">'+gate_states[2]+'</span><br>'
               else
                 text +=gate_states[2]+'<br>'

	       text +='<input name="gateradiogroup'+i+'"  type="radio" value=3 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],3);">'
	       // text +='<input name="gateradiogroup'+i+'"  type="radio" value=3 onclick="ODBSet(gate_control_path['+i+'],3);update(2);">'
               // gate_control = 3 // ignored (int gate always on)
               if(idx==3)
                  text +='<span class="error">'+gate_states[3]+'</span>';
               else
                  text +=gate_states[3]

               text +='</td></tr>'
               text +='</table>';  // end of GATE table
     
               text +='</td>'
               text +='</tr>'
	    }  // stopped
	    // alert('i='+i+'show_fref '+show_fref);
            if(i==0)
               document.getElementById("psmGates0").innerHTML=text;
            else if (i==3  && show_fref) 
	    {
	        
                document.getElementById("psmGates3").innerHTML=text;
	     }
         } // if(show_gate_params)
         else
	 {
	    if(typeof( document.getElementById("psmGates0")) !=undefined)
	    {
	       if(document.getElementById("psmGates0") !=null)   
                  document.getElementById("psmGates0").innerHTML="";
            }
           
	    if(typeof( document.getElementById("psmGates3")) !=undefined)
	    {
	        if(document.getElementById("psmGates3") !=null)      
		   document.getElementById("psmGates3").innerHTML="";
	    }
         }

        
      } // if i==0 ...
  } // for loop

  if(show_gate_params)
      initialize_gate_params();
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> display_gate_control: ending "
  return;
}


function initialize_gate_params()
{
    var idx, i;

 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> initialize_gate_params : starting "
    //alert('initialize_gate_params: show_gate_params= '+ show_gate_params)
    if(rstate==state_stopped && show_gate_params)
       {
  
	  for(i=0; i<4; i++)
	  {
             if(i==0 || ( i==3 && show_fref) ) 
             {
                idx = gate_control[i];
                idx=parseInt(idx);
   
                idx = gate_control[i];
                idx=parseInt(idx);
        
                if(i==0)
	        {
                   if(typeof(document.form1.gateradiogroup0) !='undefined')
                     document.form1.gateradiogroup0[idx].checked = 1;
                }
		else
		{  // i==3 && fref
                   if(typeof(document.form1.gateradiogroup3) !='undefined')
                     document.form1.gateradiogroup3[idx].checked = 1;
		}
	     }
	  }
       }
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending "
}


function show_psm_debug_params()
{

   var text="";
   var i;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>show_psm_debug_params</b>: starting "
   // enable_psm_debug_params=get_bool(ODBGet(tpath));
   enable_psm_debug_params = document.form1.tbox0.checked;
   //alert('show_psm_debug_params:  enable_psm_debug_params= '+enable_psm_debug_params);
   if(!enable_psm_debug_params)
   {
       document.getElementById("dbg").innerHTML="";
       if(!gbl_debug) document.getElementById('tdebug').innerHTML=""; // clear debug title
       return;
   }
   document.getElementById('tdebug').innerHTML=remember_tdebug; // show debug title
   text ='<h3>PSM Debug</h3>'
   text+='Idle freq (Hz) = '+idle_freq_hz
   if(show_fref)
       text+='&nbsp&nbsp Ref freq (Hz) = '+ fref_tuning_freq
   if(table_driven)
   {
      text+='<br>Frequency Table-driven;&nbsp&nbsp Freq end sweep jump to idle = '+ jump_to_idle
      text+='&nbsp&nbsp Load first sweep value in idle = '+ load_first
   }
   
   // Debug table
   text+='<table style="width: 100%;" border="1" cellpadding="5" bgcolor=white>'
   text += '<tr align="left">'

   text += '<td class="helplinks">profile</td>'
   text += '<td class="helplinks">index</td>'
   text +='<td class="helplinks">enabled</td><td class="helplinks">enable quad</td>'
   if(psm_module=="PSMII") // PSM Module
      text += '<td class="helplinks">simulate single tone</td>'
   text += '<td class="helplinks">amplitude</td><td class="helplinks">moduln mode</td><td class="helplinks">bandwidth</td>'
   text += '<td class="helplinks">jump idle iq</td><td class="helplinks">load 1st</td><td class="helplinks">idle i</td>'
   text += '<td class="helplinks">idle q</td><td class="helplinks">load iq file</td><td class="helplinks">set const i</td>'
   text += '<td class="helplinks">const i</td><td class="helplinks">set const q</td><td class="helplinks">const q</td>'
   text += '<td class="helplinks">gate (1=def)</td></tr>'
	  								    
   var pattern=/freq */;  
   var pf
       
   for (i=0; i<4; i++)
   {
    pf = profiles[i].replace  (pattern,"f"); 
    text += '<tr><td>'+pf+'</td>'
    text += '<td>'+i+'</td>'
    text += '<td>'+ profile_enabled[i]+'</td>'
    if(profile_enabled[i])
      {
         text += '<td>'+ enable_quad[i]+'</td>'
         if(psm_module=="PSMII") // PSM Module
            text += '<td>'+ simulate_single_tone[i]+'</td>'
        text += '<td>'+ amplitude[i]+'</td>'
        text += '<td>'+ moduln_mode[i]+'</td>'

        text += '<td>'+ bandwidth[i]+'</td>'
        text += '<td>'+ jump_idle_iq[i]+'</td>'
        text += '<td>'+ load_first_iq[i]+'</td>'
        text += '<td>'+ idle_i[i]+'</td>'
        text += '<td>'+ idle_q[i]+'</td>'
        text += '<td>'+ load_iq_file[i]+'</td>'
        text += '<td>'+ set_const_i[i]+'</td>'
        text += '<td>'+ const_i[i]+'</td>'
        text += '<td>'+ set_const_q[i]+'</td>'
        text += '<td>'+ const_q[i]+'</td>'

      
        text += '<td>'+ gate_control[i]+'</td></tr>'
     }

   }

   text += '</table>'
   document.getElementById("dbg").innerHTML=text;
  if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending "
}


//  get_profile_enabled() should be called before get_psm_quad_path_arrays

//  get_psm_path_arrays() calls get_psm_quad_path_arrays
//  init calls get_psm_path_arrays() after  assign_profile_paths();

//  get_profile_enabled() is called from build_psm_params()
//   build_psm_params() is called from load()
//                

function get_profile_enabled()
{
    var i;
     if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><b>get_profile_enabled</b>: starting'
     //   alert('get_profile_enabled: profile_enabled_path[0]='+profile_enabled_path[0])
    profile_enabled[0] =EqData.frontend.hardware.psm.one_f["profile enabled"]; //  ODBGet(profile_enabled_path[0]);  

    profile_enabled[1]= profile_enabled[2]= 0;
    profile_enabled[3] = EqData.frontend.hardware.psm.fref["profile enabled"]; // ODBGet(profile_enabled_path[3]);
    for(i=0; i<4; i++)
    {
        if(profile_enabled[i]==undefined)
           alert('get_profile_enabled:  profile_enabled['+i+'] is undefined' );
        else
	    profile_enabled[i] = get_bool(profile_enabled[i])
     }
  if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><b>get_profile_enabled</b> ends with profile_enabled[0]= '+profile_enabled[0]
}


function get_psm_quad_path_arrays(i)
{
    // called from  get_psm_path_arrays()

  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_psm_quad_path_arrays</b>: starting with profile i= "+i
    // alert('get_psm_quad_path_arrays i= '+i+' (bool) enable_quad['+i+']='+enable_quad[i]   ) // profile_path_array
   
   profile_path_array[i]= profile_enabled_path[i];
  //  alert('get_psm_quad_path_arrays: i= '+i+' profile_path_array[i]='+ profile_path_array[i])
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="...now  profile_path_array["+i+"] = "+ profile_path_array[i]

         if(mod_path[i].length < 3)
         {
            alert('get_psm_quad_path_arrays:  invalid mod_path for index '+i    +' i.e. '+mod_path[i])
            return;
         }

        jump_to_idle_iq_path[i]= mod_path[i] + '/jump to idle iq';
        load_iq_file_path[i]= mod_path[i] + '/load i,q pairs file';
        set_const_i_path[i] =  mod_path[i] + '/set constant i value in file';
        set_const_q_path[i] =  mod_path[i] + '/set constant q value in file';


        moduln_mode_path[i] = mod_path[i] +  '/Moduln mode (ln-sech,Hermite)'
        bandwidth_path[i] = mod_path[i] + '/requested bandwidth (Hz)'
        idle_i_path[i]= mod_path[i] + '/idle i (max plus or minus 511)'
        idle_q_path[i]= mod_path[i] + '/idle q (max plus or minus 511)'
        const_i_path[i] = mod_path[i] + '/const i (max plus or minus 511)'
        const_q_path[i] = mod_path[i] + '/const q (max plus or minus 511)'
     if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b> get_psm_quad_path_arrays</b> ending "
}



function assign_profile_paths()
{
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>assign_profile_paths</b>: starting "
    profile_path[0] = psm_path + "/one_f";
    profile_path[1] = " "; // three_f not used
    profile_path[2] = " "; // five_f not used
    profile_path[3]= psm_path + "/fref";

    for (i=0; i<4; i++)
	mod_path[i] = profile_path[i] + "/IQ Modulation";  // mod_path global

    mod_path[1]=mod_path[2]=" "; // 3f, 5f not used

   // alert('assign_profile_paths: mod_path array = '+mod_path)
    profile_enabled_path[0] = profile_path[0] + "/profile enabled";//  /Equipment/FIFO_acq/frontend/hardware/PSM/one_f/profile enabled
    profile_enabled_path[3] = profile_path[3] + "/profile enabled"; //  /Equipment/FIFO_acq/frontend/hardware/PSM/fref/profile enabled

    // alert(' profile_enabled_path[0]='+profile_enabled_path[0])
 

 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (assign_profile_paths)"
}


function get_psm_path_arrays()
{
    // called from initialize
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_psm_path_arrays</b>: starting with i= "+i
  
 
  for(i=0; i<4; i++)
   {
      
      if(i==0  || (i==3 && show_fref)  )
      {
         if(profile_path[i].length < 3)
         {
            alert('get_psm_path_arrays:  invalid profile_path for index '+i    +' i.e. '+profile_path[i])
            return;
         }
        // alert('get_psm_path_arrays: profile_path['+i+'] = '+profile_path[i])

         amplitude_path[i] = profile_path[i] + '/scale factor (def 181 max 255)'
         gate_control_path[i] = profile_path[i] + "/gate control";
         quadrature_path[i]= profile_path[i] + '/quadrature modulation mode'; //e.g. /Equipment/FIFO_acq/frontend/hardware/PSM/one_f/quadrature modulation mode

	 // if(psm_module == "PSMII") // PSMII Module
             simulate_single_tone_path[i]= profile_path[i] + '/simulate single tone mode';

         get_psm_quad_path_arrays(i);


     } // if (i=0 or 3&fref) 

 } // for
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_psm_path_arrays</b>: .. and ending "
}


function get_psm_data_arrays()
{
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_psm_data_arrays</b>: starting with index i= "+i

   if (profile_enabled[0]) // one_f enabled
   { 
       // onef
       amplitude[0] = EqData.frontend.hardware.psm.one_f["scale factor (def 181 max 255)"] //INT  ODBGet ( amplitude_path[i] );
       gate_control[0]  = EqData.frontend.hardware.psm.one_f["gate control"] // INT  ODBGet( gate_control_path[i]);     
       enable_quad[0] = get_bool( EqData.frontend.hardware.psm.one_f["quadrature modulation mode"])  //BOOL ODBGet( quadrature_path[i] );
    
      
	   //  if(psm_module == "PSMII") // PSMII Module
	
	  simulate_single_tone[0] = get_bool( EqData.frontend.hardware.psm.one_f["simulate single tone mode"])  //BOOL ODBGet(  simulate_single_tone_path[i]);
          
   if( enable_quad[0]) 
    {
     moduln_mode[0]  =  EqData.frontend.hardware.psm.one_f["iq modulation"]["moduln mode (ln-sech,hermite)"] // ODBGet( moduln_mode_path[i]);
     bandwidth[0]    =  EqData.frontend.hardware.psm.one_f["iq modulation"]["requested bandwidth (hz)"] // INT ODBGet( bandwidth_path[i]);
     jump_idle_iq[0] = get_bool( EqData.frontend.hardware.psm.one_f["iq modulation"]["jump to idle iq"])  // BOOL ODBGet( jump_to_idle_iq_path[i]);
     load_first_iq[0] = get_bool( EqData.frontend.hardware.psm.one_f["iq modulation"]["load first val in idle"])  // BOOL ODBGet(mod_path[i] + "/load first val in idle")

     idle_i[0] =  EqData.frontend.hardware.psm.one_f["iq modulation"]["idle i (max plus or minus 511)"]      //INT  ODBGet( idle_i_path[i] )
     idle_q[0] =  EqData.frontend.hardware.psm.one_f["iq modulation"]["idle q (max plus or minus 511)"]      //INT  ODBGet( idle_q_path[i])
 
     load_iq_file[0] =get_bool( EqData.frontend.hardware.psm.one_f["iq modulation"]["load i,q pairs file"])//BOOL ODBGet( load_iq_file_path[i])
     set_const_i[0] = get_bool( EqData.frontend.hardware.psm.one_f["iq modulation"]["set constant i value in file"]) // BOOL ODBGet( set_const_i_path[i])
     const_i[0] =      EqData.frontend.hardware.psm.one_f["iq modulation"]["const i (max plus or minus 511)"]// ODBGet( const_i_path[i])
     set_const_q[0] =get_bool(EqData.frontend.hardware.psm.one_f["iq modulation"]["set constant q value in file"])// ODBGet( set_const_q_path[i])
     const_q[0] =      EqData.frontend.hardware.psm.one_f["iq modulation"]["const q (max plus or minus 511)"]// ODBGet( const_q_path[i])
    }   // if enable_quad
  } // if one_f enabled
    
   if(profile_enabled[3] && show_fref) // three_f
   { 
       // onef
       amplitude[3] = EqData.frontend.hardware.psm.fref["scale factor (def 181 max 255)"] //INT  ODBGet ( amplitude_path[i] );
       gate_control[3] =EqData.frontend.hardware.psm.fref["gate control"]  // INT  ODBGet( gate_control_path[i]);     
	   enable_quad[3] = get_bool( EqData.frontend.hardware.psm.fref["quadrature modulation mode"])  // ODBGet( quadrature_path[i] );
    
	   // if(psm_module == "PSMII") // PSMII Module
	  simulate_single_tone[3] = get_bool(  EqData.frontend.hardware.psm.fref["simulate single tone mode"])  // ODBGet(  simulate_single_tone_path[i]);
	   //  alert('simulate_single_tone[3]= '+  simulate_single_tone[3]);
   if( get_bool (enable_quad[3]) )
    {
     moduln_mode[3]  =  EqData.frontend.hardware.psm.fref["iq modulation"]["moduln mode (ln-sech,hermite)"] // ODBGet( moduln_mode_path[i]);
     bandwidth[3]    =  EqData.frontend.hardware.psm.fref["iq modulation"]["requested bandwidth (hz)"] // INT ODBGet( bandwidth_path[i]);
     jump_idle_iq[3] =  get_bool( EqData.frontend.hardware.psm.fref["iq modulation"]["jump to idle iq"])  // BOOL ODBGet( jump_to_idle_iq_path[i]);
     load_first_iq[3] = get_bool( EqData.frontend.hardware.psm.fref["iq modulation"]["load first val in idle"])  // BOOL ODBGet(mod_path[i] + "/load first val in idle")

     idle_i[3] =  EqData.frontend.hardware.psm.fref["iq modulation"]["idle i (max plus or minus 511)"]      //INT  ODBGet( idle_i_path[i] )
     idle_q[3] =  EqData.frontend.hardware.psm.fref["iq modulation"]["idle q (max plus or minus 511)"]      //INT  ODBGet( idle_q_path[i])
 
     load_iq_file[3] = get_bool( EqData.frontend.hardware.psm.fref["iq modulation"]["load i,q pairs file"])//BOOL ODBGet( load_iq_file_path[i])
     set_const_i[3] =  get_bool( EqData.frontend.hardware.psm.fref["iq modulation"]["set constant i value in file"]) // BOOL ODBGet( set_const_i_path[i])
     const_i[3] =      EqData.frontend.hardware.psm.fref["iq modulation"]["const i (max plus or minus 511)"]// ODBGet( const_i_path[i])
     set_const_q[3] = get_bool( EqData.frontend.hardware.psm.fref["iq modulation"]["set constant q value in file"])// ODBGet( set_const_q_path[i])
     const_q[3] =      EqData.frontend.hardware.psm.fref["iq modulation"]["const q (max plus or minus 511)"]// ODBGet( const_q_path[i])
    }   // if enable_quad
  }  // if (i=0 or 3&fref) 

    check_psm_data_arrays()  // check values
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_psm_data_arrays</b>: and ending ( get_psm_data_arrays)"
	}


function check_psm_data_arrays()
{
  var i;
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>check_psm_data_arrays</b>: starting "
  for(i=0; i<4; i++)
  {
    if(profile_enabled[i])
    {
      if( amplitude[i] == undefined)
	 alert(' amplitude['+i+'] is undefined ( EqData.frontend.hardware.psm.'+profile[i]+'["scale factor (def 181 max 255)"]  ) ')
      if( gate_control[i] == undefined)
	 alert(' gate_control['+i+'] is undefined ( EqData.frontend.hardware.psm.'+profile[i]+'["gate control"]   ) ')
      if( enable_quad[i] == undefined)
         alert(' enable_quad['+i+'] is undefined ( EqData.frontend.hardware.psm.'+profile[i]+'["quadrature modulation mode"]  ) ')

      if( simulate_single_tone[i] == undefined)
	 alert(' simulate_single_tone['+i+'] is undefined ( EqData.frontend.hardware.psm.'+profile[i]+'["simulate single tone mode"]) ')

 
      if( enable_quad[i] )
      {
         if(moduln_mode[i] == undefined)  // STRING
	     	   alert('moduln_mode['+i+'] is undefined ( EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["moduln mode (ln-sech,hermite)"]  ) ')
 
	 if( bandwidth[i] == undefined) // INT
	   alert(' bandwidth['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"][""requested bandwidth (hz)"]) ')


	 if( jump_idle_iq[i]== undefined) // BOOL
	   alert('jump_idle_iq['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["jump to idle iq"]) ')

	 if(load_first_iq[i] == undefined)  // BOOL
	  alert('load_first_iq['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["load first val in idle"]) ')
       	
         if( idle_i[i] == undefined)
	   alert(' idle_i['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["idle i (max plus or minus 511)"]) ')
          if( idle_q[i] == undefined)
	    alert(' idle_q['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["idle q (max plus or minus 511)"]) ')
      	if(  load_iq_file[i]== undefined)  // BOOL
	    alert(' load_iq_file['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["load i,q pairs file"]) ')

  

        if( set_const_i[i] == undefined)  // BOOL
	   alert(' set_const_i['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["set constant i value in file"]) ')
        else
	  set_const_i[i] =get_bool(  set_const_i[i])

        if( set_const_q[i] == undefined)  // BOOL
	   alert(' set_const_q['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["set constant q value in file"]) ')
      
       if( const_i[i] == undefined) // INT
          alert(' const_i['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["const i (max plus or minus 511)"]) ')
       if( const_q[i] == undefined) // INT
          alert(' const_q['+i+'] is undefined (  EqData.frontend.hardware.psm.'+profile[i]+'["iq modulation"]["const q (max plus or minus 511)"]) ')
      } // enable quad
    }// enable profile
  }// end loop
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="... and ending (check_psm_data_arrays)"
}

// build_one_psm_profile missing

//============================================================================

function enable_profile(i,val)
{
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>enable_profile</b>: starting "
    i=parseInt(i);
    val=parseInt(val);
    //  alert(' enable_profile: profile_path_array='+profile_path_array[i])
 if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br>i='+i+' and profile_path_array='+profile_path_array[i]
   

 
    if(i==0) // one_f
     document.form1.psmbox0.checked=val; // initialize to correct value
    else if(i==3 && show_fref )
     document.form1.psmbox1.checked=val; // initialize to correct value

     // ODBSet(profile_path_array[i], val);
    gbl_code=2; // for update
    // alert('Calling cs_odbset with i='+i+' and profile_path_array[i]='+ profile_path_array[i]+' and val= '+val)
    cs_odbset(profile_path_array[i], val) 
    if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending  (enable_profile) with a call to update  (2)"
    update();
 } 

function check_consistency()
{
    // checks certain PPG params against RF state and sets class=error if needed

    var pattern_A = /2c/;
    var pattern_B = /1g|2[0h]/;  //  RFon_duration_dt 
    var pattern_C = /1[ab]|2[abde]/ // rf_on_time_ms 
    var pattern1_scan = /1[abfg]/;
    var pattern2_scan = /2[abce]/;

    if(rstate != state_stopped)
	return; // do nothing 

    // alert('check_consistency: starting with have_psm= '+have_psm)
    if(!have_psm)
	return; // no rf for this mode

    if(typeof(document.getElementById("psm_on0")) == undefined)
    {
	alert(' check_consistency:  Error - document.getElementById("psm_on0") is not defined ' );
        return;
    }
  
    // clear error
      document.getElementById("psmerr").className="normal"
	  document.getElementById("psmerr").innerHTML="clear errors";
    if(!profile_enabled[0])
    { 
        if( pattern1_scan.test(ppg_mode) || pattern2_scan.test(ppg_mode) || hole_burning_flag )  // One-f profile ought to be enabled
	    {
              document.getElementById("psm_on0").className="error";
	      document.getElementById("psmerr").className="bolderror";
	      document.getElementById("psmerr").innerHTML= "RF must be ENABLED when RFon duration > 0";
	    }
        
        if(pattern_B.test(ppg_mode))  // 1g/20/2h
	{
           if (RFon_duration_dt <= 0) 
	    {  // remove error in case it is set
		      if(document.getElementById("RFonDT").className == "error")
	             document.getElementById("RFonDT").className="param";

                      if(document.getElementById("psm_on0").className == "error")
	                 document.getElementById("psm_on0").className= profile_class[0]
		 
	     }
	}
    }
    else
    { //  RF (onef) is enabled 
         if (pattern_B.test(ppg_mode))
	 {  // 1g/20/2h
	     // alert('check_consistency:  RFon_duration_dt '+RFon_duration_dt)
	    if (RFon_duration_dt <= 0)
	       {
                  if(document.getElementById("RFonDT") != undefined)
	             document.getElementById("RFonDT").className="error";
                  else 
		      alert('check_consistency: document.getElementById("RFonDT") = '+document.getElementById("RFonDT"))

                  document.getElementById("psmerr").className="bolderror";
	          document.getElementById("psmerr").innerHTML = "RF should be disabled when RFon duration = 0"

	// put this in to check for error; but why is it null? Null when running, maybe
        if(typeof(document.getElementById("psm_on0")) != null)
                  document.getElementById("psm_on0").className="error";
               }
	    else
		{  // no error
		    if(document.getElementById("RFonDT").className == "error")
                      document.getElementById("RFonDT").className="param";
		  
		    // check amplitude is > 0
                   if(amplitude[0]==0 )   // enabled with zero amplitude... no RF
		   {
                        document.getElementById("psmerr").className="bolderror";
	                document.getElementById("psmerr").innerHTML = "RF Amplitude should be set non-zero"
		   }

		    if(typeof(document.getElementById("psm_on0")) != null)
		    {	// put this in to check for error; but why is it null? Null when running, maybe
		       if(document.getElementById("psm_on0").className=="error")
                          document.getElementById("psm_on0").className= profile_class[0]
		    }

               }


         }
         if (pattern_A.test(ppg_mode)  )
	     {   // e2c
		 if( e2c_rf_on_ms <= 0)
                 {
                     if(document.getElementById("e2cRFon") != undefined)
	                document.getElementById("e2cRFon").className="error";
                     document.getElementById("psm_on0").className="error";
                 }
             }
         if (pattern_C.test(ppg_mode)  )
	     { 
	       if(  rf_on_time_ms <= 0)
	       {
                  if(document.getElementById("RFonms") != undefined)
	             document.getElementById("RFonms").className="error";
                  document.getElementById("psm_on0").className="error";
               }
	     }

    }
}



function set_psmii_quad_mode()
{
  var paths=new Array(); 
  var values=new Array();
  var index=0;
  //alert('set_psmii_quad_mode: setting quad mode for all valid profiles')
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_psmii_quad_mode</b>: starting "
  for(i=0; i<4; i++)
  {
      if(profile_enabled[i] && profile_path[i].length > 5) // quadrature_path depends on profile_path
      {
	  //alert('set_psmii_quad_mode: profile_path['+i+']='+profile_path[i]+' length='+profile_path[i].length)
    
	  paths[index]=quadrature_path[i];
          values[index]=1;
          index++;
      }
  }
  gbl_code=2; // for psm table update
  async_odbset(paths,values); // called with ARRAYS; 
 
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>set_psmii_quad_mode</b>: ending "
}

