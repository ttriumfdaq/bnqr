// Functions on Parameters page to write tunes lines
function my_test()
{
    alert('my_test: progress_got_callback=8 '+  progress_got_callback)
 var name = CustomScriptData["load tune"].tunename
 var name0 =  CustomScriptData.createdescription.tunename
     alert(  'my_test:name= '+name+' and name0= '+name0);
}

function build_tunes(my_ppg_mode)
{  // called from write_tunes
   var text="";
   var sel="";
   var i;
   var temp;
   var last_selected_tune, last;
   var lst_pattern=/^(\w\w)_(.+)/; // eg 2a_test5

   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>build_tunes</b>: starting "
   if(ppg_mode != my_ppg_mode)
   {
      text='<td  colspan=10 class="tunes">List of tunes has not been loaded for this mode. Click '
      text+='<input name="customscript" value="List Tunes" type="submit" style="color:firebrick"> to load list.</td>'
      document.getElementById("tuneline1").innerHTML=text;
      if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (build_tunes) "
      return;
    }

    if(num_tunes < 1)
       text+='<center>None saved for this Mode</center>'
     else
     { 
         text+='<span class="tune">Selected tune:&nbsp</span>'
         if(document.getElementById("last_selected_tune_index").value =="")
	     // if( form1.last_selected_tune_index.value =="") // on a full reload, this will be blank...
	 {
             //  so read the last stored tune from the ODB (of the form 2a_tunename )
	     last_selected_tune= CustomData.hidden["last selected tune"]; // last_selected_path="/custom/hidden/last selected tune";
             if(last_selected_tune == undefined)
		 alert("last_selected_tune is undefined")
             if(last_selected_tune !="")
	     {
		if(lst_pattern.test(last_selected_tune))  // value is of the form xx_yyy
                {  
	           //alert('build_tunes: RegExp.$1= '+RegExp.$1+' RegExp.$2= '+RegExp.$2);
                   if(RegExp.$1 == ppg_mode)
	           {
		       //alert('gotcha! '+ppg_mode)

                       // see if we can find this tune in tune_names array
                       //alert('build_tunes: last selected tune was '+ RegExp.$2);
                       last =  RegExp.$2;
		       //alert(' looking for '+last+' in tune_names array length ='+num_tunes);
                       for(i=0; i<num_tunes; i++)
	       	       {
	       	          if(tune_names[i] == last )
		          {
			      //alert ('found '+last+' in tune_names array at index ' +i)
			      //replace form1.last_selected_tune_index.value = i; by
			      document.getElementById("last_selected_tune_index").value = i;
                             break;
		          }
		       } // end of for loop
		   } // end of current ppg mode
                } // end of found pattern
           
	     } // end of last_selected_tune not blank
	 } // end of blank
         // tune index last selected is stored in hidden document.getElementById("last_selected_tune_index").value
         //       or else it is blank, in which case select first value
     
         text+='<select name="select_tunes" onChange="select_tune()" class="tune">'
	     //  alert('tune_names= '+tune_names+' num_tunes='+num_tunes)
	     for(i=0; i<num_tunes; i++)
	     {
		 if (i == document.getElementById("last_selected_tune_index").value) // was form1.last_selected_tune_index.value
	     	      sel="selected";
                  else
                      sel="";
                  text+='<option value="0" '+sel+' >'+tune_names[i]+' </option>'
              }
         text+='</select>'
     }
    // alert('text='+text)
  
    document.getElementById("selecttune").innerHTML=text; //  Cell 2a

    

     
     if(num_tunes > 0)
     {
	 write_tune_buttons()

     }
     else
     {
	document.getElementById("htd").innerHTML="";   //  cells 1b,1c,1d  Tune Description heading  (colspan=3)
        document.getElementById("cdbtn").innerHTML="";    // cell 2b
	document.getElementById("chngd").innerHTML=""; //  cell 2c  change description button

	document.getElementById("llt").innerHTML="";     // cell 1e  
        document.getElementById("lltn").innerHTML="";     // cell 2e
	document.getElementById("ldbtn").innerHTML="";    // cells 1f,1g (colspan=2)  Load/Rename/Delete buttons
        document.getElementById("showall").innerHTML="";  // cell 2d
    
        document.getElementById("rtl").innerHTML="";     // cell 2g
     }

      
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>build_tunes</b>: ending "
   return;
}

function write_tune_buttons()
{
    // called from build_tunes
        var llt;
        var llt_pattern=/^(\w\w)_(.+)/; // eg 2a_test5

        
	//    alert('write_tune_buttons: index = '+document.form1.select_tunes.selectedIndex);
        selected_tune = tune_names[document.form1.select_tunes.selectedIndex];
      
	
        odbset_one(cs_load_tune_name_path,selected_tune); // calls mjsonrpc_db_paste
	// ODBSet(cs_load_tune_name_path,selected_tune); // also writes to /customscript/delete tune/tunename
                                         // and                         /rename_tune/tunename
                                         // as these are softlinks to cs_load_tune_name_path in ODB   
	//  alert('write_tune_buttons: index = '+document.form1.select_tunes.selectedIndex + ' selected tune= '+selected_tune);

 
        llt = CustomData.hidden["last loaded tune"]; // last loaded tune with ppgmode attached  lpath = "/custom/hidden/last loaded tune";
        document.getElementById("llt").innerHTML=""; // default
        document.getElementById("lltn").innerHTML=""; // default
        if(llt !="")
	{
           if(llt_pattern.test(llt))
           {
	       //alert('write_tune_buttons: RegExp.$1= '+RegExp.$1+' RegExp.$2= '+RegExp.$2);
              if(RegExp.$1 == ppg_mode)
	      {
		  //alert('gotcha! '+ppg_mode)
                  document.getElementById("llt").innerHTML=remember_llt;  // Cell 1e 
                  document.getElementById("lltn").innerHTML='<small><span class="tune">'+RegExp.$2+'</span></small>';
                  last_loaded_tune = RegExp.$2; // ppgmode stripped off
              }
	   }
        }
        else
	    last_loaded_tune="";
        temp = 	document.getElementById("ldbtn").innerHTML  //  Cells 1f,1g
	if(temp=="")
	document.getElementById("ldbtn").innerHTML= remember_ldbtn;
 
           

        temp = document.getElementById("cdbtn").innerHTML  // Cell 2b
        if(temp=="")
	    document.getElementById("cdbtn").innerHTML= remember_cdbtn;

       
        // hide RefreshTuneList button unless debug_ppg is set
        temp = document.getElementById("rtl").innerHTML;
        if(temp=="")
	{
	   if(document.form1.pbox0.checked) 
               document.getElementById("rtl").innerHTML= remember_rtl;
        }
        else
	{
           if(!document.form1.pbox0.checked) 
               document.getElementById("rtl").innerHTML= "";
        }

        // Tune description or Create Description button
	var description = get_tune_description(); 
      
        // alert('write_tune_buttons: Description from run_try is " '+description +'"');
        if(description == "")
	{
	    document.getElementById("cdbtn").innerHTML=remember_cdbtn;
            document.getElementById("chngd").innerHTML=""; //  cell 2c  hide change description button
        }    
        else
	{   
            document.getElementById("cdbtn").innerHTML='<small>\"'+description+'\"</small>';
	    document.getElementById("chngd").innerHTML=remember_chngd; 
        }
}

function create_new_tune()
{
  var ans,name,status;
  var loop=0;

  // paths for mjson db_paste
  var paths= new Array();
  var values = new Array();
  var i,len;
  var cmd=document.form1.crtune.value; //Delete Tune ; // cmd=CustomScript name for ODBcs
  suspend_updates();
 

  // not necessary as  ODBcs will not be called if error
  // odbset_one(cs_create_action_path,9);   // ODBSet( cs_create_action_path, 9); // default abort 

  while(loop==0)
  {
     name = prompt('Enter name of new Tune ?');
     if(name)
     {
       status = check_name_reserved(name);  // check name not reserved
       if(status)
          loop =  check_name_not_in_use(name);
     }
     else
      { //alert ("empty name ")
	  restore_updates();
	 return;
      }
  }
 		       
  //  ans = confirm('Create new tune '+  name );
  
  if(check_perlscript_done() )
  {
      paths[0]=cs_create_tune_name_path; values[0]=name;  // ODBSet(cs_create_tune_name_path,  name );
      paths[1]=cs_create_action_path   ; values[1]=2;     // ODBSet( cs_create_action_path, 2); // 2 = save
      paths[2]=poll_on_perl_path       ; values[2]=1      // ODBSet(poll_on_perl_path,1); // set flag (polling_on_perl)
      mjsonrpc_db_paste(paths,values).then(function(rpc) {
						
	      //  alert('status= '+rpc.result.status+' and inner='+   document.getElementById('readStatus').innerHTML )
          document.getElementById('readStatus').innerHTML = 'create_new_tune:  status= '+rpc.result.status
           len=rpc.result.status.length // get status array length
        	//	alert('create_new_tune: length of rpc.result.status='+len)
           for (i=0; i<len; i++)
           {
              if(rpc.result.status[i] != 1) 
		    alert('create_new_tune: status error '+rpc.result.status[i]+' at index '+i)
           } 
           document.getElementById('writeStatus').innerHTML='create_new_tune: : writing paths '+paths+' and values '+values
	  // alert('submit cmd'+cmd)
           ODBcs(cmd) // now submit the cmd
	 
	  wait_then_reload(1); // reload page completely in 1s
           
      }).catch(function(error)
      { 
          mjsonrpc_error_alert(error);
          reload(); // full reload
      });
  } // check_perlscript_done
  else
      restore_updates();
  return; 
}

function check_name_reserved(name, msg)
{
    var pattern_ws =/\s/;
    var pattern_ac =/undefined/;
    var pattern_reserved=/^last$|^tune_names$|^compare|^description$|^ppgmode$|^tunes$|^modes$/i;
  //var reserved="last,compare,description,ppgmode,tunes,modes"; // last 4 are are json 
    var status=0;

    if(name.length > 63)
	{   // 64 characters  tune_names.odb
	alert('Name is too long; limit of 63 characters');
        return 0;
    }

    if(pattern_ws.test (name))
    {    
	alert('Illegal name for tune. No whitespace allowed');
	return 0;		
    }     
    else if(pattern_reserved.test (name))
    {
	alert('This name "'+name+'" is reserved');
	return 0;
    }
    else
      return 1;
}

function check_name_not_in_use(name)
{
  for  (var i=0; i<num_tunes; i++)
  {
     if(tune_names[i] ==  name )
     {
        alert('Tune '+ name + ' already exists');
        return 0;
     }
  }
  return 1;
}

function check_name_in_use(name)
{
  for  (var i=0; i<num_tunes; i++)
  {
     if(tune_names[i] ==  name )
        return 1;
  }
  alert('Tune '+ name + ' is not known' );
  return 0;
}


function load_tune()
{
   var ans;
   var temp;
   // paths for mjson db_paste
  var paths= new Array();
  var values = new Array();
  var i,len;
  var cmd=document.form1.ldtune.value; //Delete Tune ; // cmd=CustomScript name for ODBcs

 
  // ODBSet(cs_load_action_path, 9); // abort not needed as using ODBcs

   var name = CustomScriptData["load tune"].tunename
   if(name==undefined)
       {
          alert('load_tune: tune name is undefined ( CustomScriptData["load tune"].tunename) ')
          return;
       }

  suspend_updates();

       // ans = confirm('Load tune "'+ODBGet(cs_load_tune_name_path)+'"?');
   ans = confirm('Load tune "'+name+'"?');
   if(ans)
   {
     if(check_perlscript_done() )
     {
         //alert('load_tune: check_perlscript_done is true ')
         temp = ppg_mode + '_' + selected_tune;
         paths[0]= cs_load_action_path ; values[0]=1 ; // ODBSet( cs_load_action_path, 1); // 1 = load
         paths[1]= lpath               ; values[1]=temp ; // ODBSet(lpath,temp); // last loaded tune with ppg_mode affixed
         paths[2]= poll_on_perl_path   ; values[2]=1 ; // ODBSet(poll_on_perl_path,1); // set flag

         mjsonrpc_db_paste(paths,values).then(function(rpc) {
						
	  //  alert('status= '+rpc.result.status+' and inner='+   document.getElementById('readStatus').innerHTML )
          document.getElementById('readStatus').innerHTML = 'load_tune:  status= '+rpc.result.status
           len=rpc.result.status.length // get status array length
        	//	alert('load_tune: length of rpc.result.status='+len)
           for (i=0; i<len; i++)
           {
              if(rpc.result.status[i] != 1) 
		    alert('load_tune: status error '+rpc.result.status[i]+' at index '+i)
           } 
           document.getElementById('writeStatus').innerHTML='load_tune: writing paths '+paths+' and values '+values
	  // alert('submit cmd'+cmd)
           ODBcs(cmd) // now submit the cmd
	 
	  wait_then_reload(1); // reload page completely in 1s
           
      }).catch(function(error)
      { 
          mjsonrpc_error_alert(error);
          reload();
      });
     } // check_perlscript_done
     else
       restore_updates(); 
   } // ans
   else
         restore_updates(); 
   return;    
}

function delete_tune()
{
   var ans;
   var name, status;

   // paths for mjson db_paste
  var paths= new Array();
  var values = new Array();
  var i,len;
  var cmd=document.form1.deltune.value; //Delete Tune ; // cmd=CustomScript name for ODBcs
 
   // using ODBcs, don't need to do this 
   // ODBSet( cs_delete_action_path, 9); // abort
   // ODBSet(cs_change_descr_action_path , 9); // default = abort

   // Delete selected tune
   name = CustomScriptData["delete tune"].tunename // ODBGet(cs_delete_tune_name_path);
   if(name==undefined)
   {
       alert('delete_tune: Tune name is undefined ( CustomScriptData["delete tune"].tunename) ')
       return;
   }
   //  selected tune should not included a reserved name
   status = check_name_reserved(name);
   if(status == 0)  // cannot delete a reserved tunename
      return;

   status =  check_name_in_use(name);  // make sure there is such a tune
   if(status == 0) // tune is not defined
      return;

 
   suspend_updates();
   ans = confirm('Delete tune "'+name+'" ?');
   if(ans)
   {
     if(check_perlscript_done() )
     {
	 paths[0]=cs_delete_action_path ; values[0]=3 ; // ODBSet( cs_delete_action_path, 3); // 3 = delete
	 paths[1]=perlscript_done_path ; values[1]=4 ; // ODBSet(perlscript_done_path, 4); // init perlscript status code 
	 paths[2]=poll_on_perl_path ; values[2]=1 ; // ODBSet(poll_on_perl_path,1); // set flag

         mjsonrpc_db_paste(paths,values).then(function(rpc) {
						
	      //  alert('status= '+rpc.result.status+' and inner='+   document.getElementById('readStatus').innerHTML )
          document.getElementById('readStatus').innerHTML = 'delete_tune:  status= '+rpc.result.status
           len=rpc.result.status.length // get status array length
        	//	alert('delete_tune: length of rpc.result.status='+len)
           for (i=0; i<len; i++)
           {
              if(rpc.result.status[i] != 1) 
		    alert('delete_tune: status error '+rpc.result.status[i]+' at index '+i+'; paths='+paths)
           } 
           document.getElementById('writeStatus').innerHTML='delete_tune: writing paths '+paths+' and values '+values
	  // alert('submit cmd'+cmd)
           ODBcs(cmd) // now submit the cmd
	 
	  wait_then_reload(1); // reload page completely in 1s
           
      }).catch(function(error)
      { 
          mjsonrpc_error_alert(error);
          reload();
      });
    } // check_perlscript_done
     else
	   restore_updates(); // reload page
   }// if ans
   else
         restore_updates(); // reload page
   return;
}


function select_tune()
{
    // called from selection box in build_tunes, (not a customscript button)

    var temp;
    //  alert('select_tune: index = '+document.form1.select_tunes.selectedIndex);
    // alert('select_tune: ivalue= '+document.form1.select_tunes.options[document.form1.select_tunes.selectedIndex].value);

    document.form1.last_selected_tune_index.value  = document.form1.select_tunes.selectedIndex;

    //alert('selected '+tune_names[ document.form1.last_selected_tune_index.value]);
    temp = ppg_mode + '_' + tune_names[ document.form1.last_selected_tune_index.value]; // e.g. 2a_tunename

    odbset_one(last_selected_path, temp); // calls mjson_db_paste
    // ODBSet(last_selected_path, temp); // save in ODB in case full reload
               // last tune name is  tune_names[document.form1.select_tunes.selectedIndex];
    write_tune_buttons(); // also writes selected tune to ODB
}

function rename_tune()
{
    var ans,name, name0;
    var status;
    var loop=0;
      // paths for mjson db_paste
  var paths= new Array();
  var values = new Array();
  var i,len;
  var cmd=document.form1.rentune.value; //Rename Tune   cmd=CustomScript name for ODBcs

 
  // using ODBcs, don't need to do this 
  // ODBSet( cs_rename_action_path, 9); // abort

  
   name0 = CustomScriptData["rename tune"].tunename //  ODBGet(cs_rename_tune_name_path)
   if(name0==undefined)
       {
       alert('rename_tune:tune name is undefined ( CustomScriptData["rename tune"].tunename) ')
	   return;
       }
    // Reserved tunes are not shown
    // status = check_name_reserved(name0);
    // if(status == 0)  // cannot rename a reserved name
    //    return;
    

  suspend_updates();
  while (loop==0)
  {
     name = prompt('Enter new name for selected Tune "'+name0+'"?');
     if(name)
     {
	 status = check_name_reserved(name);  // check new name
	 if(status)
           loop =  check_name_not_in_use(name);
     }
     else
      { //alert ("empty name ")
	  //loop=1;
	  restore_updates();
	 return;
      }
  } // while
 
  //ans = confirm('Rename tune "'+ODBGet(cs_rename_tune_name_path)+'" to "'+name+'" ?');
  if (check_perlscript_done() )
  {

      paths[0]=cs_rename_newtunename_path ; values[0]=name ; // ODBSet(cs_rename_newtunename_path, name);
      paths[1]=perlscript_done_path       ; values[1]=4 ; //ODBSet(perlscript_done_path, 4); // init perlscript status code 
      paths[2]=cs_rename_action_path      ; values[2]=5 ; //ODBSet( cs_rename_action_path, 5); // 5 = rename (also builds a new list of tunefiles)   
      paths[3]=poll_on_perl_path          ; values[3]=1 ; //ODBSet(poll_on_perl_path,1); // set flag
      
      //alert('last_loaded_tune='+last_loaded_tune+' selected tune= '+selected_tune);
      if(last_loaded_tune == selected_tune)
      {     // rename last_loaded_tune as well
        var temp = ppg_mode + '_' + name;
        paths[4]=lpath  ; values[4]=temp; //ODBSet(lpath,temp); // last loaded tune with ppg_mode affixed
	// alert('settin lpath='+lpath+' to '+temp)
      }

      mjsonrpc_db_paste(paths,values).then(function(rpc) {
						
	  //  alert('status= '+rpc.result.status+' and inner='+   document.getElementById('readStatus').innerHTML )
          document.getElementById('readStatus').innerHTML = 'rename_tune:  status= '+rpc.result.status
           len=rpc.result.status.length // get status array length
        	//	alert('rename_tune: length of rpc.result.status='+len)
           for (i=0; i<len; i++)
           {
              if(rpc.result.status[i] != 1) 
		  alert('rename_tune: status error '+rpc.result.status[i]+' at index '+i+'; paths='+paths)
           } 
           document.getElementById('writeStatus').innerHTML='rename_tune: writing paths '+paths+' and values '+values
	  // alert('submit cmd'+cmd)
           ODBcs(cmd) // now submit the cmd
	 
	  wait_then_reload(1); // reload page completely in 1s
           
      }).catch(function(error)
      { 
          mjsonrpc_error_alert(error);
          reload();
      });
  } // check_perlscript_done
  else
       restore_updates(); // reload page
  return;
}

function list_tunes()
{
// paths for mjson db_paste
  var paths= new Array();
  var values = new Array();
  var i,len;
  var cmd=document.form1.relist.value; //Refresh TuneList ; // cmd=CustomScript name for ODBcs
  suspend_updates();

 // not necessary as  ODBcs will not be called if error
 // ODBSet(cs_create_descr_action_path , 9); // default = abort
    if(check_perlscript_done() )
    {
	alert('Updating tune selection list by rechecking saved tune files')
       paths[0]=perlscript_done_path; values[0]=4;  // ODBSet(perlscript_done_path, 4); // init perlscript status code 
       paths[1]=poll_on_perl_path   ; values[1]=1;  // ODBSet(poll_on_perl_path,1); // set flag
      
       mjsonrpc_db_paste(paths,values).then(function(rpc) {
						
	       //     alert('status= '+rpc.result.status+' and inner='+   document.getElementById('readStatus').innerHTML )
          document.getElementById('readStatus').innerHTML = 'list_tunes:  status= '+rpc.result.status
           len=rpc.result.status.length // get status array length
        	//	alert('list_tunes: length of rpc.result.status='+len)
           for (i=0; i<len; i++)
           {
              if(rpc.result.status[i] != 1) 
		    alert('list_tunes: status error '+rpc.result.status[i]+' at index '+i+'; paths='+paths)

           } 
           document.getElementById('writeStatus').innerHTML='list_tunes: writing paths '+paths+' and values '+values
	  // alert('submit cmd'+cmd)
           ODBcs(cmd) // now submit the cmd
	 
	  wait_then_reload(1); // reload page completely in 1s
           
      }).catch(function(error)
      { 
          mjsonrpc_error_alert(error);
          reload();
      });
    } // check_perlscript_done
    else
	restore_updates();
    return;
}

function create_description(cmd)
{ // also replaces the existing description. Tune name NOT changed. 

 // paths for mjson db_paste
  var paths= new Array();
  var values = new Array();
  var i,len;
  var cmd=document.form1.crdes.value; //Create Description ; // cmd=CustomScript name for ODBcs

 

  // alert('create_description: starting with cs_create_descr_action_path='+cs_create_descr_action_path)
  var pattern_ac =/undefined/;
  
  // not necessary as  ODBcs will not be called if error
  //ODBSet(cs_create_descr_action_path , 9); // default = abort

  var name0 = CustomScriptData.createdescription.tunename  //  cs_create_descr_name_path
  if(name0 == undefined)
      {
     alert('tune name is undefined ( CustomScriptData.createdescription.tunename)')
	 return;
      }
  suspend_updates();
  var ans = prompt('Enter description for Tune "'+ name0 + '" ?');
 
  if(ans)
  {
      if(check_perlscript_done() )
      {
	  var string='"'+ ans  +'"';  // add quotes in case of spaces
          //alert ('string='+string)
	  paths[0]=cs_create_description_path;   values[0]=string;  // ODBSet(cs_create_description_path , string);
          paths[1]=perlscript_done_path;         values[1]=4;  // OODBSet(perlscript_done_path, 4); // perlscript status code 
	  paths[2]=cs_create_descr_action_path;  values[2]=6;  // OODBSet(cs_create_descr_action_path , 6); // 6= create/replace description for this tune 
	  paths[3]=poll_on_perl_path;            values[3]=1;  // OODBSet(poll_on_perl_path,1); // set flag

          mjsonrpc_db_paste(paths,values).then(function(rpc) {
						
	      //  alert('status= '+rpc.result.status+' and inner='+   document.getElementById('readStatus').innerHTML )
          document.getElementById('readStatus').innerHTML = 'create_description:  status= '+rpc.result.status
           len=rpc.result.status.length // get status array length
        	//	alert('create_description: length of rpc.result.status='+len)
           for (i=0; i<len; i++)
           {
              if(rpc.result.status[i] != 1) 
		    alert('create_description: status error '+rpc.result.status[i]+' at index '+i+'; paths='+paths)
           } 
           document.getElementById('writeStatus').innerHTML='create_description: writing paths '+paths+' and values '+values
	  // alert('submit cmd'+cmd)
           ODBcs(cmd) // now submit the cmd
	 
	  wait_then_reload(1); // reload page completely in 1s
           
      }).catch(function(error)
      { 
          mjsonrpc_error_alert(error);
          reload();
      });

    } // check_perlscript_done
    else 
      restore_updates()
  }   // ans
  else
      restore_updates()
}


function change_description()
{ // replaces the existing description. Tune name NOT changed. Page reloads with submit
 
  var pattern_ac =/\w+/;
  var name0, status;
  var ans, param;
 // paths for mjson db_paste
  var paths= new Array();
  var values = new Array();
  var i,len;
  var cmd=document.form1.chnge.value; //Change Description ; // cmd=CustomScript name for ODBcs

  var name0 = CustomScriptData.change.tunename  //   ODBGet(cs_change_descr_name_path)
  if(name0 == undefined)
      {
     alert('Tune name is undefined ( CustomScriptData.change.tunename)')
     return;
      }
  //alert('change_desc: cmd= '+cmd);
  suspend_updates();
 
  var answer = prompt("Enter new description (blank to delete)","");
      //status = check_name_reserved(name0);
      //if(status == 0)  // cannot change description for a reserved name
      // return; 
 
  // alert('answer= "'+answer+'"');
  if(answer == null) // abort
      {
       restore_updates(); // restore now
       return;
      }

  //ODBSet(perlscript_done_path, 4); // perlscript status code  set below

 // not necessary as  ODBcs will not be called if error
 // ODBSet(cs_change_descr_action_path , 9); // default = abort


  if(!pattern_ac.test(answer)) // no word characters
  {
      ans = confirm ('Delete description for Tune "'+name0+ '" ?');
      if(!ans)
	  return;
      param=7; // clear description
  }
  else
  {
      param = 6; // description supplied
  }

  
  if(check_perlscript_done() )
     {
        var string='"'+ answer +'"';  // add quotes in case of spaces

        paths[0]=perlscript_done_path;        values[0]=4;  // ODBSet(perlscript_done_path, 4); // init perlscript status code 
        paths[1]=cs_change_description_path;  values[1]=string; // ODBSet(cs_change_description_path , string);
        paths[2]=cs_change_descr_action_path; values[2]= param ; // ODBSet(cs_change_descr_action_path , param); // 6= create/replace description for this tune, 7= clear descrip 
        paths[3]=poll_on_perl_path;           values[3]=1;  // ODBSet(poll_on_perl_path,1); // set flag

       mjsonrpc_db_paste(paths,values).then(function(rpc) {
						
	       //   alert('status= '+rpc.result.status+' and inner='+   document.getElementById('readStatus').innerHTML )
          document.getElementById('readStatus').innerHTML = ' change_description:  status= '+rpc.result.status
           len=rpc.result.status.length // get status array length
        	//	alert(' change_description: length of rpc.result.status='+len)
           for (i=0; i<len; i++)
           {
              if(rpc.result.status[i] != 1) 
		    alert(' change_description: status error '+rpc.result.status[i]+' at index '+i)
           } 
           document.getElementById('writeStatus').innerHTML=' change_description: writing paths '+paths+' and values '+values
	  // alert('submit cmd'+cmd)
           ODBcs(cmd) // now submit the cmd
	 
	  wait_then_reload(1); // reload page completely in 1s
           
      }).catch(function(error)
      { 
          mjsonrpc_error_alert(error);
          reload();
      });

     } // check_perlscript_done 
  else
      restore_updates();
  return;
}



