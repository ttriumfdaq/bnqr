function temp_read_custom()
{
 var paths=[
     "/Custom/"]


     //clearTimeout(customTimerID);

 if(gbl_debug)
      document.getElementById('gdebug').innerHTML+='<br><b>read_custom</b>: getting new custom/hidden data'

 document.getElementById('readStatus').innerHTML ='read_custom: getting new data'
 document.getElementById('writeStatus').innerHTML ='read_custom: getting new data'
	  //customCntr++;

  progressFlag =  progress_callback // waiting for callback
  mjsonrpc_db_get_values(paths).then(function(rpc) {
  if(rpc.result.status[0] != 1) 
              document.getElementById('readStatus').innerHTML +='<br>read_custom: status error at path='+paths
 CustomData= rpc.result.data[0];

  progressFlag= progress_got_callback;
  clicked_pmb = get_bool(CustomData.hidden["clicked ppgmode button"]);
  var remember_ppgmode = CustomData.hidden["remember ppgmode"];
  var experiment_name = CustomData.hidden["experiment name"]; // link to ppg mode
  if(gbl_debug)
      document.getElementById('gdebug').innerHTML+='<br><b>read_custom</b>: clicked_pppgmode_button = '+clicked_pmb

  if(clicked_pmb)
  {
      mainTimerID = setTimeout('main()', 2000);  // call main again
      return;
  }
  
     if(remember_ppgmode == experiment_name  && customCntr > 2)
     {
        if(gbl_debug)
           document.getElementById('gdebug').innerHTML+='<br><b>read_custom</b>:ppgmode has not changed cntr='+ customCntr
           clearTimeout(updateTimerId); // clear update timer
	   clearTimeout(customTimerID)
           customTimerID = setTimeout('read_custom()', 1000); 
     }
     else
     {
        if(gbl_debug)
	    {
		if(customCntr > 2) 
                    document.getElementById('gdebug').innerHTML+='<br><b>read_custom</b>ppgmode has not changed, TIMEOUT, restarting update timer '
                else
                   document.getElementById('gdebug').innerHTML+='<br><b>read_custom</b>:ppgmode HAS changed, restarting update timer'
	    }
	my_odbset(['/custom/hidden/clicked ppgmode button'], [0]) // callback clear button
	updateTimerID = setTimeout('update()', updatePeriod ); // restart update timer
        build_ppgmode_buttons();
 write_ppgmode_box()
     }
 
  } // end of clicked_pmb
  else
  {
      updateTimerID = setTimeout('update()', updatePeriod ); // restart update timer
      build_ppgmode_buttons();
 write_ppgmode_box()
  }

      }).catch(function(error) {
         document.getElementById('readStatus').innerHTML='callback error from custom_data()'
        mjsonrpc_error_alert(error);
       })
 }
