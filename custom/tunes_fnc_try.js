function write_tune_buttons()
{
    // called from build_tunes
        var llt;
        var llt_pattern=/^(\w\w)_(.+)/; // eg 2a_test5

         if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>write_tune_buttons</b>: starting "
	//    alert('write_tune_buttons: index = '+document.form1.select_tunes.selectedIndex);
        selected_tune = tune_names[document.form1.select_tunes.selectedIndex];
      
	 //	 alert('write_tune_buttons cs_load_tune_name_path= '+cs_load_tune_name_path+' selected_tune='+selected_tune )
	   ODBSet(cs_load_tune_name_path,selected_tune); // also writes to /customscript/delete tune/tunename
	   ///	 cs_odbset(cs_load_tune_name_path,selected_tune)
                                         // and                         /rename_tune/tunename
                                         // as these are softlinks to cs_load_tune_name_path in ODB   
	     // alert('write_tune_buttons: index = '+document.form1.select_tunes.selectedIndex + ' selected tune= '+selected_tune);

 
        llt = CustomData.hidden["last loaded tune"]; // last loaded tune with ppgmode attached  lpath = "/custom/hidden/last loaded tune";
        document.getElementById("llt").innerHTML=""; // default
        document.getElementById("lltn").innerHTML=""; // default
        if(llt !="")
	{
           if(llt_pattern.test(llt))
           {
	       //alert('write_tune_buttons: RegExp.$1= '+RegExp.$1+' RegExp.$2= '+RegExp.$2);
              if(RegExp.$1 == ppg_mode)
	      {
		  //alert('gotcha! '+ppg_mode)
                  document.getElementById("llt").innerHTML=remember_llt;  // Cell 1e 
                  document.getElementById("lltn").innerHTML='<small><span class="tune">'+RegExp.$2+'</span></small>';
                  last_loaded_tune = RegExp.$2; // ppgmode stripped off
              }
	   }
        }
        else
	    last_loaded_tune="";
        temp = 	document.getElementById("ldbtn").innerHTML  //  Cells 1f,1g
	if(temp=="")
	document.getElementById("ldbtn").innerHTML= remember_ldbtn;
 
           

        temp = document.getElementById("cdbtn").innerHTML  // Cell 2b
        if(temp=="")
	    document.getElementById("cdbtn").innerHTML= remember_cdbtn;

       
        // hide RefreshTuneList button unless debug_ppg is set
        temp = document.getElementById("rtl").innerHTML;
        if(temp=="")
	{
	   if(document.form1.pbox0.checked) 
               document.getElementById("rtl").innerHTML= remember_rtl;
        }
        else
	{
           if(!document.form1.pbox0.checked) 
               document.getElementById("rtl").innerHTML= "";
        }

        // Tune description or Create Description button
	var description = get_tune_description(); 
      
        // alert('write_tune_buttons: Description from run_try is " '+description +'"');
        if(description == "")
	{
	    document.getElementById("cdbtn").innerHTML=remember_cdbtn;
            document.getElementById("chngd").innerHTML=""; //  cell 2c  hide change description button
        }    
        else
	{   
            document.getElementById("cdbtn").innerHTML='<small>\"'+description+'\"</small>';
	    document.getElementById("chngd").innerHTML=remember_chngd; 
        }
     
   if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (write_tune_buttons) "
}

function create_new_tune()
{
  var ans,name,status;
  var loop=0;

   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>create_new_tune</b>: starting "

       ODBSet( cs_create_action_path, 9); // default abort 
   // cs_odbset( cs_create_action_path, 9); // default abort 

  while(loop==0)
  {
     name = prompt('Enter name of new Tune ?');
     if(name)
     {
       status = check_name_reserved(name);  // check name not reserved
       if(status)
          loop =  check_name_not_in_use(name);
     }
     else
      { //alert ("empty name ")
	 return;
      }
  }
 		       
  //  ans = confirm('Create new tune '+  name );
  
  if(check_perlscript_done() )
  {
     ODBSet(cs_create_tune_name_path,  name );
     ODBSet( cs_create_action_path, 2); // 2 = save
     ODBSet(poll_on_perl_path,1); // set flag (polling_on_perl)
     ///  var paths=new Array(); var values=new Array();
     ///  paths[0]=cs_create_tune_name_path; values[0]=name;
     ///  paths[1]=cs_create_action_path;    values[1]=2;
     ///  paths[2]=poll_on_perl_path;        values[2]=1;
     ///  async_odbset(paths,values); // arrays
  }
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>create_new_tune</b>: ending "

  return;
}
function check_name_reserved(name, msg)
{
    var pattern_ws =/\s/;
    var pattern_ac =/undefined/;
    var pattern_reserved=/^last$|^tune_names$|^compare|^description$|^ppgmode$|^tunes$|^modes$/i;
  //var reserved="last,compare,description,ppgmode,tunes,modes"; // last 4 are are json 
    var status=0;
    if(pattern_ws.test (name))
    {    
	alert('Illegal name for tune. No whitespace allowed');
	return 0;		
    }     
    else if(pattern_reserved.test (name))
    {
	alert('This name "'+name+'" is reserved');
	return 0;
    }
    else
      return 1;
}

function check_name_not_in_use(name)
{
  for  (var i=0; i<num_tunes; i++)
  {
     if(tune_names[i] ==  name )
     {
        alert('Tune '+ name + ' already exists');
        return 0;
     }
  }
  return 1;
}

function check_name_in_use(name)
{
  for  (var i=0; i<num_tunes; i++)
  {
     if(tune_names[i] ==  name )
        return 1;
  }
  alert('Tune '+ name + ' is not known' );
  return 0;
}


function load_tune()
{
   var ans;
   var temp;
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>load_tune</b>: starting "

   ODBSet(cs_load_action_path, 9); // abort
 // cs_odbset(cs_load_action_path, 9); // abort
   var name = CustomScriptData["load tune"].tunename
   if(name==undefined)
       alert('load_tune: tune name is undefined ( CustomScriptData["load tune"].tunename) ')

       // ans = confirm('Load tune "'+ODBGet(cs_load_tune_name_path)+'"?');
   ans = confirm('Load tune "'+name+'"?');
   if(ans)
   {
     if(check_perlscript_done() )
     {
         //alert('load_tune: check_perlscript_done is true ')
	  ODBSet( cs_load_action_path, 1); // 1 = load
         temp = ppg_mode + '_' + selected_tune;
	  ODBSet(lpath,temp); // last loaded tune with ppg_mode affixed
	  ODBSet(poll_on_perl_path,1); // set flag
	  //   var paths=new Array();
	  //   var values=new Array();
	  //   paths[0]= cs_load_action_path; values[0]=1;
	  //  paths[1]=lpath;                values[1]=temp;
	  //  paths[2]=poll_on_perl_path;    values[2]=1;
	  //  async_odbset(paths,values); // arrays
     }
   }
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>load_tune</b>: ending "

   return;    
}

function delete_tune()
{
   var ans;
   var name, status;
   var paths=new Array();
   var values=new Array();

 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>delete_tune</b>: starting "
 ODBSet( cs_delete_action_path, 9); // abort
 ODBSet(cs_change_descr_action_path , 9); // default = abort
		  // async_odbset(paths,values);
		  // paths[0]= cs_delete_action_path;       values[0]=9;  //ODBSet( cs_delete_action_path, 9); // abort
		  // paths[1]= cs_change_descr_action_path; values[1]=9;  //ODBSet(cs_change_descr_action_path , 9); // default = abort
		  // async_odbset(paths,values);

   // Delete selected tune
   name = CustomScriptData["delete tune"].tunename // ODBGet(cs_delete_tune_name_path);
   if(name==undefined)
       alert('delete_tune: Tune name is undefined ( CustomScriptData["delete tune"].tunename) ')

   //  selected tune should not included a reserved name
   status = check_name_reserved(name);
   if(status == 0)  // cannot delete a reserved tunename
      return;

   status =  check_name_in_use(name);  // make sure there is such a tune
   if(status == 0) // tune is not defined
      return;

 

   ans = confirm('Delete tune "'+name+'" ?');
   if(ans)
   {
     if(check_perlscript_done() )
     {
 ODBSet( cs_delete_action_path, 3); // 3 = delete
ODBSet(perlscript_done_path, 4); // init perlscript status code 
ODBSet(poll_on_perl_path,1); // set flag
	 //	 paths[0]= cs_delete_action_path; values[0]=3; //  ODBSet( cs_delete_action_path, 3); // 3 = delete
	 //	 paths[1]= perlscript_done_path;  values[1]=4; //ODBSet(perlscript_done_path, 4); // init perlscript status code 
	 //	 paths[2]=poll_on_perl_path;      values[2]=1; //ODBSet(poll_on_perl_path,1); // set flag
	 //      async_odbset(paths,values); // arrays
     }
   }
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>delete_tune</b>: ending "
   return;
}


function select_tune()
{
    var temp;
    //  alert('select_tune: index = '+document.form1.select_tunes.selectedIndex);
    // alert('select_tune: ivalue= '+document.form1.select_tunes.options[document.form1.select_tunes.selectedIndex].value);
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>select_tune</b>: starting "
    document.form1.last_selected_tune_index.value  = document.form1.select_tunes.selectedIndex;

    //alert('selected '+tune_names[ document.form1.last_selected_tune_index.value]);
    temp = ppg_mode + '_' + tune_names[ document.form1.last_selected_tune_index.value]; // e.g. 2a_tunename
  ODBSet(last_selected_path, temp); // save in ODB in case full reload
    //  cs_odbset(last_selected_path, temp); //ODBSet(last_selected_path, temp); // save in ODB in case full reload
               // last tune name is  tune_names[document.form1.select_tunes.selectedIndex];
    write_tune_buttons(); // also writes selected tune to ODB
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>select_tune</b>: ending "
}

function rename_tune()
{
    var ans,name, name0;
    var status;
    var loop=0;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>rename_tune</b>: starting "
    ODBSet( cs_rename_action_path, 9); // abort
    //  cs_odbset (cs_rename_action_path, 9); // ODBSet( cs_rename_action_path, 9); // abort

   name0 = CustomScriptData["rename tune"].tunename //  ODBGet(cs_rename_tune_name_path)
   if(name0==undefined)
       alert('rename_tune:tune name is undefined ( CustomScriptData["rename tune"].tunename) ')

    // Reserved tunes are not shown
    // status = check_name_reserved(name0);
    // if(status == 0)  // cannot rename a reserved name
    //    return;
    

 
  while (loop==0)
  {
     name = prompt('Enter new name for selected Tune "'+name0+'"?');
     if(name)
     {
	 status = check_name_reserved(name);  // check new name
	 if(status)
           loop =  check_name_not_in_use(name);
     }
     else
      { //alert ("empty name ")
	  //loop=1;
	 return;
      }
  } // while
 
  //ans = confirm('Rename tune "'+ODBGet(cs_rename_tune_name_path)+'" to "'+name+'" ?');
  if (check_perlscript_done() )
  {
    ODBSet(cs_rename_newtunename_path, name);
    ODBSet(perlscript_done_path, 4); // init perlscript status code 
    ODBSet( cs_rename_action_path, 5); // 5 = rename (also builds a new list of tunefiles)   
    ODBSet(poll_on_perl_path,1); // set flag
 
      //   var paths=new Array();
      //    var values=new Array();

      //    paths[0]=cs_rename_newtunename_path;  values[0]=name  // ODBSet(cs_rename_newtunename_path, name);
						      //    paths[1]=perlscript_done_path;        values[1]=4  //ODBSet(perlscript_done_path, 4); // init perlscript status code 
      //  paths[2]=cs_rename_action_path;       values[2]=5  //ODBSet( cs_rename_action_path, 5); // 5 = rename (also builds a new list of tunefiles)   
      //  paths[3]=poll_on_perl_path;           values[3]=1  // ODBSet(poll_on_perl_path,1); // set flag
    
      //alert('last_loaded_tune='+last_loaded_tune+' selected tune= '+selected_tune);
    
      if(last_loaded_tune == selected_tune)
      {     // rename last_loaded_tune as well
        var temp = ppg_mode + '_' + name;
        ODBSet(lpath,temp); // last loaded tune with ppg_mode affixed 
	// paths[4]=lpath;   values[4]=temp; //  ODBSet(lpath,temp); // last loaded tune with ppg_mode affixed 
	// alert('set lpath='+lpath+' to '+temp)
      }
      // async_odbset(paths,values) // write all odb values
  }
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>rename_tune</b>: ending "
  return;
}

function list_tunes()
{
    // var paths=new Array();
    // var values=new Array();
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>list_tunes</b>: starting "
 ODBSet(cs_create_descr_action_path , 9); // default = abort
   // paths[0]=cs_create_descr_action_path ; values[0]=9  ; //ODBSet(cs_create_descr_action_path , 9); // default = abort
    if(check_perlscript_done() )
    {
        ODBSet(perlscript_done_path, 4); // init perlscript status code 
	//  paths[1]=perlscript_done_path ; values[1]=4  ; //ODBSet(perlscript_done_path, 4); // init perlscript status code 
       alert('Updating tune selection list by rechecking saved tune files')
       ODBSet(poll_on_perl_path,1); // set flag
	   // paths[2]= poll_on_perl_path; values[2]=1  ; //ODBSet(poll_on_perl_path,1); // set flag
    }
    //  async_odbset(paths,values);
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>list_tunes</b>: ending "
    return;
}

function create_description()
{ // also replaces the existing description. Tune name NOT changed
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>create_description</b>: starting "
  var pattern_ac =/undefined/;
  
  ODBSet(cs_create_descr_action_path , 9); // default = abort
// cs_odbset(cs_create_descr_action_path , 9); //ODBSet(cs_create_descr_action_path , 9); // default = abort
 
  var name0 = CustomScriptData.CreateDescription.tunename  //  cs_create_descr_name_path
  if(name0 == undefined)
     alert('tune name is undefined ( CustomScriptData.CreateDescription.tunename)')

  ans = prompt('Enter description for Tune "'+ tunename+ '" ?');

  if(ans)
  {
      if(check_perlscript_done() )
      {
          var paths=new Array();
          var values=new Array();
	  var string='"'+ ans  +'"';  // add quotes in case of spaces
          //alert ('string='+string)
	  paths[0]=cs_create_description_path  ; values[0]=string ; //ODBSet(cs_create_description_path , string);
          paths[1]=perlscript_done_path        ; values[1]=4 ; //ODBSet(perlscript_done_path, 4); // perlscript status code 
	  paths[2]=cs_create_descr_action_path ; values[2]=6 ; //ODBSet(cs_create_descr_action_path , 6); // 6= create/replace description for this tune 
	  paths[3]=poll_on_perl_path           ; values[3]=1 ; //ODBSet(poll_on_perl_path,1); // set flag
          async_odbset(paths,values);
      }  
   } 
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>create_description</b>: ending "
}


function change_description()
{ // replaces the existing description. Tune name NOT changed
 
  var pattern_ac =/\w+/;
  var name0, status;
  var ans, param;

  var answer = prompt("Enter new description (blank to delete)","");
  var paths=new Array();
  var values=new Array();
  var name0 = CustomScriptData.Change.tunename  //   ODBGet(cs_change_descr_name_path)
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>change_description</b>: starting "
  if(name0 == undefined)
     alert('Tune name is undefined ( CustomScriptData.Change.tunename)')

 
      //status = check_name_reserved(name0);
      //if(status == 0)  // cannot change description for a reserved name
      // return; 
 
  // alert('answer= "'+answer+'"');
  if(answer == null) // abort
       return;

  paths[0]=perlscript_done_path        ; values[0]=4 ; // ODBSet(perlscript_done_path, 4); // perlscript status code 
  paths[1]=cs_change_descr_action_path ; values[1]=9 ; //   ODBSet(cs_change_descr_action_path , 9); // default = abort
  async_odbset(paths,values);

  if(!pattern_ac.test(answer)) // no word characters
  {
      ans = confirm ('Delete description for Tune "'+name0+ '" ?');
      if(!ans)
	  return;
      param=7; // clear description
  }
  else
  {
      param = 6; // description supplied
  }

  
  if(check_perlscript_done() )
  {
     var string='"'+ answer +'"';  // add quotes in case of spaces
     paths[0]=perlscript_done_path        ; values[0]=4 ;      //         ODBSet(perlscript_done_path, 4); // init perlscript status code 
     paths[1]=cs_change_description_path  ; values[1]=string ; //         ODBSet(cs_change_description_path , string);
     paths[2]=cs_change_descr_action_path ; values[2]=param ;  //         ODBSet(cs_change_descr_action_path , param); 
                                                            // 6= create/replace description for this tune, 7= clear descrip 
     paths[3]=poll_on_perl_path           ; values[3]=1 ;      //         ODBSet(poll_on_perl_path,1); // set flag
     async_odbset(paths,values);
  }    
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>change_description</b>: ending "
}



