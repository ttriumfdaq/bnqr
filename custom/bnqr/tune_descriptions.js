// tune_descriptions.js
// 
var json_txt = 
'{"modes":[' +

        '{"ppgmode":"10","tunes":[' +
              '{"tuneName":"test3","description":"another test" },' + 
              '{"tuneName":"test","description":"test again" },' + 
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 10" }' +
         ']},'+

        '{"ppgmode":"1a","tunes":[' +
              '{"tuneName":"test","description":"test only" },' + 
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1a" }' +
         ']},'+

        '{"ppgmode":"1b","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1b" }' +
         ']},'+

        '{"ppgmode":"1c","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1c" }' +
         ']},'+

        '{"ppgmode":"1f","tunes":[' +
              '{"tuneName":"ref_freq","description":"Reference freq also enabled " },' + 
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1f" }' +
         ']},'+

        '{"ppgmode":"1g","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1g" }' +
         ']},'+

        '{"ppgmode":"1j","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1j" }' +
         ']},'+


        '{"ppgmode":"20","tunes":[' +
              '{"tuneName":"tu123","description":"dsd" },' + 
              '{"tuneName":"M1536_SLR_mode1","description":"test again" },' + 
              '{"tuneName":"last","description":"Description for Tune:last  PPG Mode 20" }' +
         ']},'+

        '{"ppgmode":"2a", "tunes":[' +
              '{"tuneName":"test","description":"test parameters only" },' + 
              '{"tuneName":"last","description":"Description for Tune last  PPG Mode: 2a" }' +
         ']},'+

        '{"ppgmode":"2b","tunes":[' +
              '{"tuneName":"test","description":"test only for mode 2b" },' + 
              '{"tuneName":"last","description":"Description for Tune:last  PPG Mode: 2b" }' +
         ']},'+

        '{"ppgmode":"2c","tunes":[' +
              '{"tuneName":"test","description":"test only for mode 2c" },' + 
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 2c" }' +
         ']},'+

        '{"ppgmode":"2d","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 2d" }' +
         ']},'+

        '{"ppgmode":"2e","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 2e" }' +
         ']}'+

']}'

