// Specific parameters for bnqr used by cs_functions.js


var mode2Names=new Array("SLR","2a","2b","2c","2d","2e","2f","Alpha2"); // Alpha is 2h

 var mode2Info=[
		 "Mode 20: Select no RF or fixed freq","","","","",
                 "RF scanned (loads frequency table)",
                 "not tested; do not use",
		 "Mode 20 with ALPHA inputs histogrammed"
		 ];

var mode1Names=new Array("Scalers","1a","1b","Camp","Field","Freq","NaCell","LCR","Fast");
var num_type1_modes=7;  // 2 modes (LCR,FAST) are combined modes. Add new type1 modes in list before LCR,Fast

  var mode1Info=[
				 "Mode 10; nothing is scanned","","",
		         "Mode 1c: CAMP variable scanned",
		         "Mode 1e: Field current scanned",
                 "Mode 1f: RF scanned",
                 "Mode 1n: NaCell voltage scanned",
                 "Mode 1h: 1f mode with Alpha counters",
                 "Mode 1j: combination of Modes 20 and 1c (CAMP scan)",
                 "Mode 1g: combination of Modes 20 and 1f (RF scan)"
                ];

