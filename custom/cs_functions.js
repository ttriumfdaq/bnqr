// cs_functions - functions for Custom_Status.html

//  <!-- javascript globals  -->
 var gbl_debug=0;

// json data from ODB
var EqData;  //  //  /Equipment/FIFO_acq/
var CustomData;  //  /Custom/
var AutoData;    //  /Autorun
var RunInfoData; //  /Runinfo
var AlarmData;   //  /Alarms/
var ProData;     //  /Programs/
var ExperimData; //  /Experiment/
var CampData;    //  /Equipment/Camp/
var EpicsLogData;//  /Equipment/Epicslog/
var ScalerData;  //  /Equipment/Cycle_Scalers/
var LoggerData;  //  /Logger
var OdbInfoData; //  /Equipment/Info ODB/
var HistoData;   //  /Equipment/Histo/Statistics
var HeaderData;  //  /Equipment/HEADER/Statistics

 var state_stopped = 1  // run state
 var state_paused  = 2
 var state_running = 3 
 

var pattern_y = /y/;
var pattern_n = /n/;
var pattern_true = /true/;
var pattern_false= /false/;
var pattern1 = /^1/
var pattern2 = /^2/
var pattern_no_key = /DB_NO_KEY/    // for ODBGET
var pattern_x = /\d(\w)/   // ppgmode  e.g. 2a
var pattern_undef = /undefined/ 
var pattern_1f = /^1f/
var pattern_1n = /^1n/
var my_expt;

// constants
var helstate= new Array("DOWN","UP","?","??");
var bnmr_expt;
var bnqr_expt;
var init_flag=0;

var progress_init=0
var progress_callback=1 // waiting for callback  read_data, enable_autorun, async_odbset, try_write_last_message
var progress_got_callback=11 // read_data, enable_autorun, async_odbset
var progress_getdata=2  // assign_data
var progress_gotdata=12 // done
var progress_load_all=5
var progress_load_all_done=15
var progress_load_partial=4
var progress_load_partial_done=14
var progress_update=6;  // function update()
var progress_update_done=16;
var progress_write_callback=7; // waiting for callback from db_paste
var progress_write_got_callback=17; // got  callback from db_paste
var progress_msg=3; // write message callback 
var progress_msg_done=13; // write msg callback done
var progressFlag=progress_init;

var ferr=9;


var mh_hold 

// mh_hold conditions (defined in mheaderBnmr.c)
   var mh_all_clear=0
   var mh_on_hold=1
   var mh_fix_camp=2
   var mh_fix_epics=3
   var mh_rip = 4
   var mh_no_log=5

   var customTimerID;
   var customCntr=0;
 var chgCntr=0;

// Constants
  var eq_titles=new Array("CyclScal","Histo","Info","Camplog","Hdr","Epicslog"); // Equipment titles as they appear on Status page
                                                                                  // type 1 uses all; type 2 uses first 3
//                  "Cycle_Scalers","Histo","Info ODB","Camp","Header","Epicslog" // Actual Equipment names as in ODB                               


 var runinfo; // global get_record
 var autorun; // global get_record
 var autoruns_enable; // global
 var rstate;

 var status_key=999;

 var alarm_names = new Array(); 
 var alarm_clients = new Array(); 
 var HSCL= new Array();

 var elog_alarm_set; // was alarm_set
 var epics_alarm_set; // was epics_set
 var ppg_mode;
 var ppgtype;
 var run_number;
 var on_hold_flag;
 var psm_enabled;     
 var data_dir;
 var rf_onef_enabled
 var rf_fref_enabled;
 var  rf_onef_ampl,rf_fref_ampl;
 var data_logging_flag;
 var loge_flag,logc_flag;
 var skip_tol,helicity_set, helicity_read, helicity_mismatch
 var hel_flip,hel_check,epics_switch_check
 var got_event_flag
 var dual_channel_mode
 


 var sapath="/custom/hidden/show autorun parameters";
 var utpath="/custom/hidden/status page update time (s)";
 var sspath="/custom/hidden/show sample stats";
 var show_autorun_params;
 var autorun_enable_path = "/Autorun/Enable"; // ODBEdit
 var autorun_state_path = "/Autorun/State"; // ODBEdit
 var autorun_plan_path = "/Autorun/plan file"; // ODBEdit
 var autorun_pause_path = "/Autorun/enable pausing"; // ODBEdit
 var autorun_refresh_path = "/Autorun/refresh seconds"; // ODBEdit
 var autorun_time_path = "/Autorun/time limit (minutes)"; // ODBEdit
 var autorun_target_counts_path= "/Autorun/target counts"; // ODBEdit
 var autorun_countHis_path ="/Autorun/count histogram";  // ODBEdit
 var eor_rn_check_path = "/custom/hidden/get next runnum";
 var num_scans_path = "/Experiment/edit on start/Number of scans";  // ODBEdit
 var num_cycles_path = "/Experiment/edit on start/Number of cycles";  // ODBEdit
 var alarm_path="/alarms/Alarm system active"; // ODBEdit
 var hardware_path ="/Equipment/fifo_acq/frontend/hardware/"
 var epics_switch_check_path = hardware_path+"enable epics switch checks" // ODBSet
 var hel_check_path = hardware_path+"enable all hel checks";
 var epics_switch_check_path= hardware_path+"enable epics switch checks";
 var dual_channel_path = hardware_path + "Enable dual channel mode";

 var camp_ok, epics_ok;
 var alarm_system_active; 
 var ctrig; // client alarm triggered flag
 var transition_in_progress;
 var ppg_mode_changed,ppgtype_changed,rstate_changed, onhold_changed, runnum_changed, alarm_changed, tipsart_changed;
 var autorun_changed=0;
 var rncheck_changed;
 var timerID,timer2_ID, timer3_ID, timer4_ID, timer5_ID;
var gbl_sr_mode; // sample/ref mode
 var tip_counter=0;
var update_time_s
var busy_timeout_s = 20;   // timeout for busy to clear itself
var update_short_s = 1;    //
var client_status=0;
var nhis;
var ncycle_per_scan
// remember innerhtml  globals
    var start_index=0;

var remember_buttons_array= new Array();  // filled in load_remember_buttons()
var start_index=0;  // indices to above array
var stop_index=1;
var hold_index=2;
var cont_index=3;
var kill_index=4;
var elog_index=5;
var reref_index=6;
var rCampl_index=7;
var rEpicsl_index=8;
var rfcnfg_index=9;
var cancel_index=10;
var toggle_index=11;

var remember_eqnames_type1, remember_eqnames_type2;
var remember_autorun_params;
var remember_type2_buttons;
var remember_type1_buttons;
var remember_combo_buttons;
var remember_ppg_buttons;
var remember_runmode_buttons;
var remember_sample_ref_table;
var remember_ref_line;
//var remember_cb_titles;
var remember_cb_table;
var remember_mismatch;
var remember_tip;
var remember_dcm; 
var remember_scm; 
var remember_prmode;
var remember_tdebug;
var ppgtype_last_run;
var eor_rn_check;

var my_toggle_flag=0;

// mode2Names and mode2Info are now different for bnmr/bnqr.
// mode1Names and mode1Info are now different for bnmr/bnqr.
// They are moved to bn[mq]r_params.js under /custom/expt_specific!

function update()
 {  // page update every 5s (default)
    // alert('update: starting')
    // Countdown until full reload

   // if(update.arguments.length > 0)
    

    countdown--;
    progressFlag = progress_update;  
    document.getElementById('countdown').innerHTML='Countdown: '+countdown
    clearTimeout(updateTimerId);
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>Function update()</b> with countdown= "+countdown

      var my_date= new Date;
      var s=my_date.toString();
      s = s.replace(/ GMT.+/,""); // strip off time zone info

      document.getElementById('LastUpdated').innerHTML = "Last partial page update " + s;
   clear_click_msg() // clear message for users if interface is slow
      if(!gbl_debug)
	  {
 document.getElementById('writeStatus').innerHTML = ''
 document.getElementById('readStatus').innerHTML = ''
     }
      // alert('update: calling read_data')
    read_data(); // read the data - init_flag=1 for partial load
    if (updatePeriod > 0)  // this will be the short one
       updateTimerId = setTimeout('update()', updatePeriod);
  progressFlag = progress_update_done;  
 }

function calculate_update_timer()
{
   var minimum_update_time_s
   var my_class
   var text="";
   //alert('autoruns_enable='+autoruns_enable)
   if(autoruns_enable)
   {
      minimum_update_time_s = 10;
      update_time_s=10; // fixed at 10s
      my_class="on"
      text='Page Update every '+update_time_s+'sec' 
   }
   else
   {  // not autorun  
      my_class="buttons"
      minimum_update_time_s = 5;
   
      update_time_s = CustomData.hidden["status page update time (s)"]
      if (update_time_s == undefined)
      {
          update_time_s=minimum_update_time_s;
          text='<br><span style="color:red;font-size:80%">update time undefined; using minimum : '+minimum_update_time_s+'s</span>'
      }
      else
      {
         text= 'Page update <a href="#" onclick="ODBEdit(utpath)" >'+ update_time_s +'</a> sec (min 5s)'
         var tmp=parseInt(update_time_s)
         if(tmp < minimum_update_time_s )
         {
           update_time_s=minimum_update_time_s;
           text+='<span style="color:red;font-size:80%"> Using '+minimum_update_time_s+'s</span>'
         }
         else if(tmp > 30) // maximum update time
         {
           update_time_s=30;
           text+='<span style="color:red;font-size:80%"> Using 30s</span>'
         }
       
      } 
   } // end of not autorun
   updatePeriod = update_time_s * 1000; // ms
   
   document.getElementById('put').innerHTML=text;
   document.getElementById('put').className=my_class;   

    // full page update every 60s (fixed)
    countdown = parseInt(60 / update_time_s);  // both in seconds
    updateTimerId = setTimeout('update()', updatePeriod);
    document.getElementById('countdown').innerHTML='Countdown: '+countdown
  
}

function initialize()
{ // called from load_all()
  
   var pattern_bnmr = new RegExp ("bnmr","i"); // alternative declaration for pattern
   var pattern_bnqr = new RegExp ("bnqr","i"); //     i means ... case insensitive
   var my_class;
   setup_customstatus();  // Full load only
   
   my_expt = ExperimData.name; // global
   bnmr_expt = pattern_bnmr.test(my_expt);  // globals
   bnqr_expt = pattern_bnqr.test(my_expt);

 
   if(bnmr_expt)
   {
      fe_name = "febnmr"
      document.getElementById('title').innerHTML="Run Control for &beta;-nmr (VMIC version)";
   }
   else if (bnqr_expt)
   {
      fe_name="febnqr";
      document.getElementById('title').innerHTML="Run Control for &beta;-nqr (VMIC version)";
   }

   load_remember_buttons();
   calculate_update_timer();
 
    init_event_stat_titles();

   remember_autorun_params = document.getElementById('autorun').innerHTML;
   remember_prmode = document.getElementById('prmode').innerHTML;
   remember_ppg_buttons =  document.getElementById('ppgbtns').innerHTML;

   remember_cb_table = document.getElementById('cbtable').innerHTML;
   remember_mismatch = document.getElementById('mismatch').innerHTML;
   remember_runmode_buttons =  document.getElementById('runmode').innerHTML;
   remember_dcm =  document.getElementById('dcm').innerHTML;
   remember_scm = remember_dcm.replace(/Dual/,"Single");
   remember_tdebug =  document.getElementById('tdebug').innerHTML;
   remember_sample_ref_table  =  document.getElementById('sr').innerHTML;
   remember_ref_line = document.getElementById('refvals').innerHTML;
   gbl_debug=get_bool(CustomData.hidden["cs debug"]);  // initialize checkbox from /custom/hidden
   document.form1.dbox.checked=gbl_debug;
   if(!gbl_debug)
      document.getElementById('tdebug').innerHTML=""; // clear title
    init_flag=1; // initialized
    // alert('init:remember_ppg_buttons='+  remember_ppg_buttons)

    document.form1.click_ppgmode.value=0; // reset
    document.form1.click_runmode.value=0; // reset
    document.form1.click_toggle.value=0; // reset
    // alert('init: document.form1.click_ppgmode.value='+ document.form1.click_ppgmode.value)

    remember_show_sample_his_stats =   document.getElementById('sshs').innerHTML;
  

  document.getElementById('myProgress').innerHTML=' Progress: '
} 



function get_ppgmode(value)
{
    var my_ppg_mode;
    my_ppg_mode = value;  // global
    my_ppg_mode = my_ppg_mode.replace(/\n/,""); // strip off any extra carriage return
    my_ppg_mode = my_ppg_mode.toLowerCase();
    return my_ppg_mode; 
}

function get_ppgtype( my_ppgmode )
{
   var my_ppgtype;

   if(pattern1.test(my_ppgmode)) 
      my_ppgtype=1
   else if(pattern2.test(my_ppgmode)) 
      my_ppgtype=2
   return my_ppgtype;
}

function load_remember_buttons()
{
    // start,stop,hold,continue,kill,elog,reref,resetCampLog,resetEpicsLog,rfconfig,cancel
    //  0     1    2     3       4    5    6      7            8             9        10
    // indices to array defined as globals
    remember_buttons_array[0]= document.getElementById('start_btn').innerHTML;
    remember_buttons_array[1]= document.getElementById('stop_btn').innerHTML;
    remember_buttons_array[2]= document.getElementById('hold_btn').innerHTML;
    remember_buttons_array[3]= document.getElementById('cont_btn').innerHTML;
    remember_buttons_array[4]= document.getElementById('kill_btn').innerHTML;
    remember_buttons_array[5]= document.getElementById('elog_btn').innerHTML;
    remember_buttons_array[6]= document.getElementById('reref_btn').innerHTML;
    remember_buttons_array[7]= document.getElementById('rsc_btn').innerHTML;
    remember_buttons_array[8]= document.getElementById('rse_btn').innerHTML;
    remember_buttons_array[9]= document.getElementById('rfc_btn').innerHTML;
    remember_buttons_array[10]= document.getElementById('cancel_btn').innerHTML; // Cancel Elog&Warn Alrm
    remember_buttons_array[11]= document.getElementById('toggle_btn').innerHTML; // toggle between test/real
}
// rfconfig_button ='<input name="customscript" value="rf_config" type="submit" style="color:firebrick" title="Run rf_config to check parameter values" >'


function read_data()
{
   
  var paths=[
	     "/Equipment/FIFO_acq/",
	             "/Custom/",
	            "/Autorun",
	         "/Runinfo",
	         "/Alarms/",
	     	     "/Programs/",
	          "/Experiment/",
	           "/Equipment/Camp/",
	         "/Equipment/EpicsLog/",
	          "/Equipment/Cycle_Scalers/",
	            "/Logger/",
	            "/Equipment/Info ODB/",
	             "/Equipment/Histo/Statistics/",
	             "/Equipment/HEADER/Statistics/",
	           "/Custom/status"
          ];

    var my_date= new Date;
    var s=my_date.toString();
    
     // alert(pattern_date.test(s))
     s = s.replace(/ GMT.+/,""); // strip off time zone info
   
    document.getElementById('cntdn').innerHTML = "ODB data last read " + s 
    progressFlag=progress_callback  // load
    if(gbl_debug)
	{     
              document.getElementById('readStatus').innerHTML ='read_data: getting new data'
	      document.getElementById('writeStatus').innerHTML = 'read_data: getting new data'
	}
     mjsonrpc_db_get_values(paths).then(function(rpc) {
    if(gbl_debug)
	{
          document.getElementById('readStatus').innerHTML = 'read_data: status='+rpc.result.status
          document.getElementById('writeStatus').innerHTML = 'read_data:processing data'
	}
      progressFlag = progress_got_callback
        var i;
        var len=rpc.result.status.length
        for ( i=0; i<len; i++)
        {  // check individual status
           if(rpc.result.status[i] != 1) 
              document.getElementById('readStatus').innerHTML +='<br>status error at index ='+i+' path='+paths[i]+' id='+rpc.id
        }
      
      status_key=rpc.result.status[len-1];
       progressFlag=  progress_getdata
       if(1)
	 {
	 assign_data(rpc); // success
         get_global_data();
         progressFlag=  progress_gotdata
	 if(!init_flag) // full load
	  	  load_all();
	     else if (autoruns_enable)
	         load_all();
	     else
	  	  load_partial();
	  }
	  else
	  {  // testing only e.g. on failure
                 update_time_s=5;
                 updatePeriod = update_time_s * 1000; // ms
                 updateTimerId = setTimeout('update()', updatePeriod);
	  }

      }).catch(function(error) {
         document.getElementById('readStatus').innerHTML='callback error from read_data() '
        mjsonrpc_error_alert(error);
       })
}

function load_all()
{      // called from read_data when init_flag is false
   //  alert("load_all starting")
    if(gbl_debug)
	{
         document.getElementById('writeStatus').innerHTML = 'loading all data'
         document.getElementById('gdebug').innerHTML+='<br><span style="color:orange;"><b>load_all: updating page</b></span> '
      }
      progressFlag=progress_load_all
      if(!init_flag)   
            initialize();  // initialize once (for autoruns)
      try_write_last_message();
      
      write_control_buttons();
      check_logging();
      write_run_info();
      write_status();
      write_info_line1(); 
      write_info_line2();  //Info line 2
      build_cycles_info(); // calculate box in new line added (free-running; nn cycles/scan etc);
      update_new_line(); // new line added (Data Directory...)  

      write_frontend_event_stats();
      write_cycle_scalers(ppgtype);
      write_cb_stats();
       check_show_ref_histos()
      nhis=read_histo_titles();
      get_histo_data(nhis);

      // try this
      if (autoruns_enable) // BOOL
          document.getElementById('prmode').innerHTML=""; // hide the whole line
      else
      {
	  if(document.getElementById('prmode').innerHTML=="")
	      document.getElementById('prmode').innerHTML=remember_prmode;
	  write_ppgmode_buttons();
	  select_run_mode();
      }

      build_statistics_links();
      build_param_links();
      build_help_links();
      build_alarms();
      build_autorun_params();
      if(gbl_debug)
       document.getElementById('writeStatus').innerHTML = 'full page loaded successfully'
   progressFlag=progress_load_all_done
}

function load_partial()
{ // called from read_data when init_flag=1
    // alert("load_partial starting")

  var message;
  progressFlag =  progress_load_partial
      if(gbl_debug)
  document.getElementById('writeStatus').innerHTML = 'loading partial data'


  if(gbl_debug) 
  {
         var now=new Date();
         var s=now.toString();
         s = s.replace(/ GMT.+/,""); // strip off time zone info

         document.getElementById('gdebug').innerHTML+='<br><span style="color:green;"><b>load_partial: updating page</b> at '+s+'</span> '
  } 
  
   try_write_last_message(); // always
  
   if(rstate == state_stopped &&  rncheck_changed)
      write_control_buttons();
   if(rstate != state_stopped  || rstate_changed  )
   {
      write_control_buttons();
      check_logging();
   }
   
   if(rstate_changed || transition_in_progress > 0 || onhold_changed)
      write_run_info();
   else if( rstate==state_running)	
   {
       message=get_elapsed_time();
       document.getElementById('rtime').innerHTML=message 
   }
    
   write_status();
 
   if(rstate_changed )
   {
                           //  (changes in PSM may not be written until run_start).
      write_info_line1();  //Info line 1  Titles  Last Cycle (stopped) or Current Cycle/Scan
     // write_info_line2();  //     line 2  Counters  current cycle/scan cycle   thresholds etc  // written below
   }
   else
   {
       if(ppgtype ==1  &&  rstate==state_running)     // Cell 4 Type 1 current cycle
       {
       //alert('load: document.getElementById(cc).innerHTML='+ document.getElementById("cc").innerHTML );
       document.getElementById('cc').innerHTML= parseInt(OdbInfoData.variables["current cycle"]); 
 
       }
   }
 
   if(rstate_changed || rstate==state_running )
   {
       write_info_line2();  //Info line 2
       build_cycles_info(); // freerunning number cycles/scans or stop after ... cycles/scans
       update_new_line();  // Data Directory   helicity etc
       check_show_ref_histos();
     
   }
   if( rstate==state_running)
   {	
      //alert('load:  before write_frontend_event_stats and  write_cycle_scalers call, ppgtype= '+ppgtype)	      
      write_frontend_event_stats();
      write_cycle_scalers(ppgtype);
      write_cb_stats();
   }
   

    nhis=read_histo_titles();
    get_histo_data(nhis);
    //alert('load_partial: ppg_mode='+ppg_mode+'  document.form1.ppgmode.value='+ document.form1.ppgmode.value+' changed= '+ ppgmode_changed)
 
  // try this
      if (autoruns_enable) // BOOL
          document.getElementById('prmode').innerHTML=""; // hide the whole line
      else
      {
	  if(document.getElementById('prmode').innerHTML=="")
	      document.getElementById('prmode').innerHTML=remember_prmode;
	  write_ppgmode_buttons();
	  select_run_mode();
      }
  

 if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br><b>build various links:</b> starting..'
 build_statistics_links();
 build_param_links();
 build_help_links();
if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'
 build_alarms();
 build_autorun_params();


 var hel_check = get_bool(EqData.frontend.hardware["enable all hel checks"], 'EqData.frontend.hardware["enable all hel checks"]');
 set_helcheck(hel_check);
 
 var epics_switch_check = get_bool(EqData.frontend.hardware["enable epics switch checks"], 'EqData.frontend.hardware["enable epics switch checks"]');
  set_escheck(epics_switch_check); // set colour, initialize checkbox

  if(gbl_debug)
      {
     document.getElementById('gdebug').innerHTML+='<br><b>load_partial:</b> ending'
     document.getElementById('writeStatus').innerHTML = 'partial page loaded successfully'
	 }
  progressFlag = progress_load_partial_done // done
  return;

}

function check_show_ref_histos()
{
    if(gbl_sr_mode)
    {
	document.getElementById('sshs').innerHTML =  remember_show_sample_his_stats;
        document.form1.box5.checked=get_bool(CustomData.hidden["show sample stats"]);  // initialize checkbox from /custom/hidden
    }
    else
       document.getElementById('sshs').innerHTML="";
       
}


 function assign_data(rpc)
  {    

    EqData=rpc.result.data[0]
   // hel_flip = get_bool(EqData.frontend.hardware["enable helicity flipping"])
   // alert("hel_flip="+hel_flip + "status= "+ rpc.result.status[0])
     if(gbl_debug)
       document.getElementById('writeStatus').innerHTML = 'assign_data: assigning data'
   
    CustomData= rpc.result.data[1];
    AutoData= rpc.result.data[2];
    RunInfoData = rpc.result.data[3];
    AlarmData= rpc.result.data[4];
    ProData=rpc.result.data[5];
    ExperimData= rpc.result.data[6];
    CampData= rpc.result.data[7];
    EpicsLogData= rpc.result.data[8];
    ScalerData= rpc.result.data[9];
    LoggerData=rpc.result.data[10];
    OdbInfoData=rpc.result.data[11];
    HistoData=rpc.result.data[12];
    HeaderData=rpc.result.data[13];

   
}


function get_global_data()
{

    // Fill some globals used elsewhere
    rstate=RunInfoData.state;
    
    if(gbl_debug)
       document.getElementById('writeStatus').innerHTML = 'gettting global data'
    transition_in_progress=RunInfoData["transition in progress"]
    run_number=RunInfoData["run number"]
	
    ppg_mode = get_ppgmode(EqData.frontend.input["experiment name"]); // convert to lower case.  ppg_mode is global 
    // alert('ppg_mode is '+ppg_mode);
    on_hold_flag = get_bool (EqData.frontend.flags.hold, "EqData.frontend.flags.hold"); // global
    camp_ok =  CampData.settings["camp ok"] 
    epics_ok =  EpicsLogData.settings["epics ok"]

    
    rf_onef_enabled = get_bool(EqData.frontend.hardware.psm.one_f["profile enabled"],"EqData.frontend.hardware.psm.one_f.profile_enabled");
    rf_fref_enabled = get_bool(EqData.frontend.hardware.psm.fref["profile enabled"],"EqData.frontend.hardware.psm.fref.profile_enabled");  
    psm_enabled     = rf_onef_enabled ||   rf_fref_enabled;  // globals 

    if(psm_enabled)
    {
       rf_onef_ampl    = EqData.frontend.hardware.psm.one_f["scale factor (def 181 max 255)"];
       rf_fref_ampl    = EqData.frontend.hardware.psm.fref["scale factor (def 181 max 255)"];
    }

    data_logging_flag =  get_bool( ExperimData["edit on start"]["write data"],"/Experiment/edit on start/write data") // bool
    alarm_system_active = get_bool( AlarmData["alarm system active"])
    if(alarm_system_active)
    {       // if ctrig > 0 fatal error
        ctrig = get_bool( AlarmData.alarms["client alarm"].triggered, "/alarm/alarms/client alarm")
        
      }
    
  
    if(rstate != document.form1.rstate.value)
       rstate_changed=1;
    else
       rstate_changed=0;
    document.form1.rstate.value=rstate;

    ppgmode_changed=0;  // default
    if(document.form1.ppgmode.value != "")
    {
       if(ppg_mode != document.form1.ppgmode.value)
          ppgmode_changed=1;
    }
	 
    // if(ppgmode_changed)alert('get_global_data: ppg_mode='+ppg_mode+'  document.form1.ppgmode.value='+ document.form1.ppgmode.value+' changed= '+ ppgmode_changed)
    document.form1.ppgmode.value=ppg_mode;
    
    ppgtype = get_ppgtype(ppg_mode);
    if(ppgtype != document.form1.ppgtype.value)
       ppgtype_changed=1;
    else
       ppgtype_changed=0;
    document.form1.ppgtype.value=ppgtype;
     

 
    if( rstate== state_running)
    {
        ppgtype_last_run=ppgtype;
	//	if( rstate_changed)   // rf_config now saves this at tr_stop()
	//  ODBSet("/custom/hidden/ppgtype last run", ppgtype); // remember what type ran last for write_cycle_scalers
    }
    else // run is stopped
    {
       ppgtype_last_run=CustomData.hidden["ppgtype last run"];
      
       eor_rn_check = get_bool(CustomData.hidden["get next runnum"]);
       if(eor_rn_check != document.form1.eor_rn_check.value)
          rncheck_changed=1;
       else
          rncheck_changed=0;
       document.form1.eor_rn_check.value =eor_rn_check;
    }
  
   if(on_hold_flag != document.form1.onhold.value)
       onhold_changed=1;
    else
       onhold_changed=0;

    document.form1.onhold.value=on_hold_flag;

 if(run_number != document.form1.runnum.value)
       runnum_changed=1;
    else
       runnum_changed=0;
 // alert('get_global_data: runnum='+run_number+'  document.form1.runnum.value='+ document.form1.runnum.value+' changed= '+ runnum_changed)
    document.form1.runnum.value=run_number;
 

  if(alarm_system_active != document.form1.alarm.value)
       alarm_changed=1;
    else
       alarm_changed=0;

    document.form1.alarm.value=alarm_system_active;

  // fill globals     
   autoruns_enable = get_bool(AutoData.enable, autorun_enable_path);
   show_autorun_params =  get_bool(CustomData.hidden["show autorun parameters"], sapath);
   //alert('get_global_data..  show_autorun_params='+  show_autorun_params)
  current_cycle = parseInt(OdbInfoData.variables["current cycle"]);

  skip_tol=        parseInt(OdbInfoData.variables["ncycle sk tol"]);
  helicity_set =   parseInt(OdbInfoData.variables["helicity set"]);
  helicity_read =   parseInt(OdbInfoData.variables["helicity read"]);
  helicity_mismatch =   parseInt(OdbInfoData.variables["wrong hel cntr"]);
  hel_check = get_bool(EqData.frontend.hardware["enable all hel checks"], 'EqData.frontend.hardware["enable all hel checks"]');
  hel_flip = get_bool(EqData.frontend.hardware["enable helicity flipping"], 'EqData.frontend.hardware["enable helicity flipping"]');
  epics_switch_check = get_bool(EqData.frontend.hardware["enable epics switch checks"], 'EqData.frontend.hardware["enable epics switch checks"]');

  got_event_flag =  get_bool(EqData["client flags"].got_event,"EqData.client_flags.got_event")   // flag true if type 2 event logged
  dual_channel_mode=get_bool(EqData.frontend.hardware["enable dual channel mode"], 'EqData.frontend.hardware["enable dual channel mode"]') 
  thresh_last_failed = parseInt(OdbInfoData.variables["last failed thr test"]);
  data_dir = LoggerData["data dir"];

  if(rstate == state_running)
  {
     logc_flag = get_bool(EqData["client flags"]["logging camp"], "EqData.client_flags.logging_camp");   // flag true if camp is being logged    
     loge_flag = get_bool(EqData["client flags"]["logging epics"], "EqData.client_flags,logging_epics" )   // flag true if epics is being logged
     mh_hold =  parseInt(EqData["client flags"].mh_on_hold);  //INT value indicates condition
  }
  else
    logc_flag=loge_flag=mh_hold=0;


  gbl_sr_mode = get_bool(EqData.frontend.hardware["enable sampleref mode"]);

 
  // Scaler Data
   HSCL=ScalerData.variables.hscl;
 
    return;
}


function progress()
{
    var colour = new Array("black","red",   "blue", "slateblue", "brown", "purple",  "green",  "fuchsia","black","black");
  //                        0        1        2        3          4        5          6         7         8       9
  //                      init     read      assign    write    load      load       update   write        
  //                               waitfor   data      msg      partial   all                 waitfor
  //                               callback           callback                                callback        
  //             
  //                                11       12       13          14      15          16        17
  //                               read got  build    write      done     done       done      write 
  //                               callback  psm      msg got                                  got
  //                                         done     callback                                 callback  
    var rlp=new String();
    var color = new Array("grey","black");
  clearTimeout(progressTimerId);

  if(progressPeriod > 0)
        progressTimerId = setTimeout('progress()', progressPeriod);

  if(progressFlag < 10)
      {
     document.getElementById('myProgress').innerHTML +='<span style="color:'+colour[progressFlag]+'">'+progressFlag+'</span>'
     remember_progress=document.getElementById('myProgress').innerHTML;
      }
     else
	 {
	    
             if(progressFlag==progress_last)
	     {
                 progress_last_index++;
                 if(progress_last_index > 1) progress_last_index=0;
	
                 document.getElementById('myProgress').innerHTML =remember_progress+'<span style="color:'+color[progress_last_index]+'">'+(progressFlag-10) +'</span>'
               
	     }
	     else
	     { 
                remember_progress=document.getElementById('myProgress').innerHTML;
                progress_last_index=0; // black
                document.getElementById('myProgress').innerHTML +='<span style="color:black">'+(progressFlag-10)+'</span>'
	     }
	 }
  progress_last=progressFlag;
}


function progress_old()
{
  // Display progress
  var colour = new Array("black","magenta","blue","lime","purple", "orange","navy","green","red","black");
  // "peachpuff","tan","red","lime"
  var index;
  clearTimeout(progressTimerId);
 
  if(progressPeriod > 0)
        progressTimerId = setTimeout('progress()', progressPeriod);

  //alert('progressFlag= '+progressFlag+ ' array length='+ colour.length)
  if(progressFlag < colour.length)
     index=progressFlag;
  else
    index=colour.length

document.getElementById('myProgress').innerHTML +='<span style="color:'+colour[index]+'">'+progressFlag+'</span>'
 
}

function try_write_last_message()
{
 if(gbl_debug) 
     {
         document.getElementById('writeStatus').innerHTML = 'writing last message'
         document.getElementById('gdebug').innerHTML+='<br><b>try_write_last_message:</b> starting... '
     }
  progressFlag=progress_msg;
  myODBGetMsg("midas",0,1,msg_callback);
 if(gbl_debug) 
         document.getElementById('gdebug').innerHTML+='and ending '
}

function myODBGetMsg(facility, start, n, callback)
{
  var url = ODBUrlBase + '?cmd=jmsg&f='+facility+'&t=' + start+'&n=' + n;
  return ODBCall(url, callback);
}

function msg_callback(msg)
{
 // latest MIDAS (August 2015) - clean off unwanted information
    var pattern=/^\d+ (\d\d:\d\d:\d\d)\.\d+( .+)/
    
    msg=msg.replace(pattern,"$1 $2"); 
    document.getElementById('lastmsg').innerHTML = msg;
    progressFlag= progress_msg_done // all done
}

function write_control_buttons()
{ 
  document.getElementById('writeStatus').innerHTML = 'writing control buttons'
   if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br><b> write_control_buttons:</b> starting '

   elog_alarm_set  = EqData["client flags"]["elog alarm"];
   epics_alarm_set = EqData["client flags"]["epics access alarm"];

   //document.write('<br> elog_alarm_set ='+ elog_alarm_set +'  epics_alarm_set'+ epics_alarm_set);

 
   if(!autoruns_enable) // BOOL
         document.getElementById('auto_button').className="none";
   else
     document.getElementById('auto_button').className="input6";

   if (elog_alarm_set > 0 || epics_alarm_set > 0)
       document.getElementById('cancel_btn').innerHTML= remember_buttons_array[cancel_index]; // button to cancel elog/epics warning
   else
      document.getElementById('cancel_btn').innerHTML=""; // button to cancel elog/epics warning 


   if(rstate == state_running)
   {
 
      document.getElementById('start_btn').innerHTML="";
      document.getElementById('stop_btn').innerHTML= remember_buttons_array[stop_index];
      if(on_hold_flag)
      { 
         document.getElementById('cont_btn').innerHTML= remember_buttons_array[cont_index];
         document.getElementById('kill_btn').innerHTML= remember_buttons_array[kill_index];
         document.getElementById('hold_btn').innerHTML="";
      }
      else
      {
         document.getElementById('hold_btn').innerHTML= remember_buttons_array[hold_index];
         document.getElementById('cont_btn').innerHTML="";
         document.getElementById('kill_btn').innerHTML="";
      }

    
      document.getElementById('rfc_btn').innerHTML=""; // rf_config
      document.getElementById('reref_btn').innerHTML= remember_buttons_array[reref_index];
      
   }
   else
   {
      
      document.getElementById('stop_btn').innerHTML="";


    

   //   if(gbl_client_status > 0)
   //      document.getElementById('start_btn').innerHTML='<span style="color: red; font-size: 80%; font-style: normal">Start</span>'
  
      if( eor_rn_check)
{
        document.getElementById('start_btn').innerHTML='<span style="color: white; font-size: 80%; font-style: normal">Busy</span>';
	//	if(!timer5_ID)set_timer(5);  
        if(!timer5_ID)
	{
	    // if(debug)
	    // document.getElementById('mytest').innerHTML +='<br>set_timer: setting timerID 5 -> clear_busy() .. time(sec) = '+busy_timeout_s
     
            //alert('setting timer 5 ');
		    // if(timer5_ID)
		    //  clearTimeout(timer5_ID);
            
            timer5_ID =  setTimeout ('clear_busy()', (busy_timeout_s * 1000));
	}

}
      else
	  {
	    if(timer5_ID)  clearTimeout(timer5_ID);
        document.getElementById('start_btn').innerHTML= remember_buttons_array[start_index];
}


      document.getElementById('hold_btn').innerHTML="";
      document.getElementById('cont_btn').innerHTML="";
      document.getElementById('kill_btn').innerHTML="";
      document.getElementById('rfc_btn').innerHTML= remember_buttons_array[rfcnfg_index];
      document.getElementById('reref_btn').innerHTML="";
      document.getElementById('rsc_btn').innerHTML="";  // reset Camp
      document.getElementById('rse_btn').innerHTML="";  // reset Epics

   }

   if(gbl_debug) document.getElementById('gdebug').innerHTML+='... and ending '
   return;
}
function check_logging()
{
  document.getElementById('writeStatus').innerHTML = 'checking logging'
  if(gbl_debug)
      {
       document.getElementById('gdebug').innerHTML+='<br><b> check_logging:</b> starting..'
       document.getElementById('gdebug').innerHTML+='<br>epics_ok='+epics_ok+' logc_flag= '+logc_flag+ ' loge_flag='+loge_flag+'<br>' 
     }

  if(rstate == state_running)
  {
     // If Running, may show ResetCampLog or ResetEpicsLog buttons

    // alert('camp_ok= '+camp_ok+' mh_hold ='+mh_hold)
    // alert('check_logging:  epics_ok='+epics_ok+' logc_flag= '+logc_flag+ ' loge_flag='+loge_flag);

     if(ppgtype == 1 )  // Type 1 depends on list of camp values
     { 
         if(mh_hold < mh_rip) // ie. waiting for camp/epics hdr, not RIP or NO_LOG
         {
              
            if(camp_ok != 2 &&  !logc_flag ) // 2=script running; logc_flag indicates if camp is being logged
            {
               document.getElementById('rsc_btn').innerHTML= remember_buttons_array[rCampl_index];;   // show reset Camp button
               document.getElementById('status').innerHTML='mh_hold= '+mh_hold+' camp_ok= '+camp_ok+' logc_flag= '+logc_flag
            }
            if(epics_ok != 2 && !loge_flag) // 2=script running; loge_flag indicates if epics is being logged
            {
               document.getElementById('rse_btn').innerHTML= remember_buttons_array[rEpicsl_index];;   // show reset Epics button
               document.getElementById('status').innerHTML='mh_hold= '+mh_hold+' camp_ok= '+camp_ok+' logc_flag= '+logc_flag
            }
          }
	  else
          {
             document.getElementById('rsc_btn').innerHTML="";  // clear reset Camp button
             document.getElementById('rse_btn').innerHTML="";  // clear reset Epics button
          }
	    
     }
     else   // type 2 Epics can be redone any time
     {
            document.getElementById('rsc_btn').innerHTML="";  // clear reset Camp button
            document.getElementById('rse_btn').innerHTML="";  // clear reset Epics button
     }
  }
  if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending '
}

function write_run_info()
{
   var pattern,sw;

   var requested_transition = RunInfoData["requested transition"]; 
   var start_abort =  RunInfoData["start abort"];
  
   var rstart =  RunInfoData["start time"];
   var rstop =   RunInfoData["stop time"];

   var text;
 if(gbl_debug)
     {
     document.getElementById('writeStatus').innerHTML = 'writing run information'
      document.getElementById('gdebug').innerHTML+='<br><b> write_run_info:</b> starting..'
	 }
   write_ppgmode_box();
  write_run_number_box();

   if(rstate == state_running)
   {
      if(on_hold_flag)
      {
           text = "Run is On Hold"
           my_class = "yellow"
      }
      else
      {
         text="Running"
         my_class="green"
      }
   }
   else if (rstate == state_paused)
   {   // illegal for BNMR/BNQR
       alert("Run is PAUSED; stop run and restart") 
       text="Run is Paused"
       my_class="orange"
   }
   else // Not running
   {
       text="Stopped";
       my_class="red"
   }

   document.getElementById('rstate').innerHTML = text;
   document.getElementById('rstate').className=my_class;


   if(transition_in_progress > 0  || requested_transition > 0 || start_abort > 0)
   {
      if(transition_in_progress > 0)
      {
         remember_tip=1;
         text="Transition in progress";
         my_class="orange";
         tip_counter++; // counter in case it is stuck
      }
      if( requested_transition > 0)
      {
         text="Transition is requested";
         my_class="blue";
      }
      if(  start_abort > 0)
      {
         text="Start is Aborted"
         my_class="red";
      }

      document.getElementById('trans').innerHTML = text;
      document.getElementById('trans').className=my_class;   
   }
   else
   {
      document.getElementById('trans').innerHTML="";
      document.getElementById('trans').style.backgroundColor="white";
      remember_tip=0;
   }
 
    document.getElementById('rstart').innerHTML=rstart

    if (rstate == state_stopped) // not running - display stop time
    {
       document.getElementById('rstop').innerHTML="Stop Time";
       document.getElementById('rtime').innerHTML=rstop
    }
    else
    { // running
       message=get_elapsed_time();
       document.getElementById('rstop').innerHTML="Elapsed Time";
       document.getElementById('rtime').innerHTML=message 
    }
     if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending '  
}
 
function write_run_number_box()
{

   if (run_number < 40000)
      my_class="yellow";
   else
      my_class="dkgreen";

   document.getElementById('rn').innerHTML = run_number;
   document.getElementById('rn').className=my_class;

}

function write_ppgmode_box()
{
  // called from write_run_info() and  load_ppgmode()
  // ppgmode
   if(ppgtype == 1)
   {
       if(pattern_x.test(ppg_mode))  // look for combined modes 1j,1g
       sw = RegExp.$1;
       if(sw=='g' || sw=='j')
          my_class="ppgmodeC";
       else
          my_class="ppgmode1" 
   }
   else if (ppgtype == 2)
       my_class="ppgmode2";

   document.getElementById('ppgmode').innerHTML = ppg_mode;
   document.getElementById('ppgmode').className=my_class;
   // alert('write_ppgmode_box: ppg_mode='+ppg_mode);
}
function get_elapsed_time(units)
{
    var mydate=new Date()
    var time_now = mydate.getTime() /1000 ; // present time in seconds
    var binary_time =  RunInfoData["start time binary"]
    var elapsed_time = time_now - binary_time // seconds
    var temp = Math.round(elapsed_time)
    var pattern = /seconds/;
    if(pattern.test(units))
	    return temp; // return the number of seconds

    message = temp + ' sec'

    if (elapsed_time > 60)
    {
       temp = elapsed_time % 60  // remainder in seconds
       var elapsed_min = (elapsed_time - temp)/60  // subtract remainder -> integer minutes
       var elapsed_sec = Math.round(temp) // rounded remainder (sec) 

       message = elapsed_min + ' min ' + elapsed_sec + ' sec';

       if(elapsed_min > 60)
       {
          temp = elapsed_min % 60 // remainder in minutes
          var elapsed_hours = (elapsed_min - temp) / 60  // integer hours 
          elapsed_min = Math.round(temp) // round remainder (min)

          message =  elapsed_hours + ' hr ' +  elapsed_min + ' min ' + elapsed_sec + ' sec';

          if (elapsed_hours > 24)
          {
             temp  =  elapsed_hours % 24  // remainder in hours
             var elapsed_days =  (elapsed_hours -  temp) / 24  // integer days 
             elapsed_hours = Math.round(temp) // round remainder (hours)
             message = elapsed_days + ' days ' +  elapsed_hours + ' hr ' +  elapsed_min + ' min ' + elapsed_sec + ' sec';
          }
       }
    }
    return message;
}

 


function write_status()
{  // write status line
  
   var text;

   // first check for stuck transition in progress
   if( (tip_counter * update_time_s) > 20) // sec
      {
         text='Transition in progress may be stuck '
         text+=  ' <input type="button" value="clear TiP" onclick="clear_tip()">'
         document.getElementById('status').innerHTML=text;
         document.getElementById('status').style.backgroundColor="orange";
         return;
      }

   var ccode= parseInt( EqData["client flags"].error_code_bitpat);   // if >0  non-fatal unless ctrig is also set 
   var client_alarm = parseInt(EqData["client flags"].client_alarm); // if >0  fatal error; ccode may also be set 
                                                                        // client_alarm cleared at eor 
  
   // For these client flags, FALSE is failure
   var ca_mdarc=     get_bool(EqData["client flags"].mdarc, "EqData.client_flags.mdarc");
   var ca_mheader=   get_bool(EqData["client flags"].mheader,"EqData.client_flags.mheader") ;
   var ca_frontend=  get_bool(EqData["client flags"].frontend,"EqData.client_flags.frontend");
   var ca_rfconfig=  get_bool(EqData["client flags"].rf_config,"EqData.client_flags.rf_config") ;
   
 if(gbl_debug)
     {
       document.getElementById('writeStatus').innerHTML = 'writing status line'
        document.getElementById('gdebug').innerHTML+='<br><b> write_status:</b> starting..'
    }
 // ctrig is still used
 // alert('write_status: ca_mdarc='+ca_mdarc+'ca_mheader='+ca_mheader+'ca_frontend='+ca_frontend+'ca_rfconfig='+ca_rfconfig);
 //  alert('write_status:  ccode='+ccode+'calarm='+client_alarm+'ctrig='+ctrig)
 
   var ca_alarm = (!ca_mdarc ||  !ca_mheader ||  !ca_frontend || !ca_rfconfig )
   
   var max_ccode=7; 

      set_alarms(alarm_system_active)

<!-- Client Flag messages  (fatal errors) -->
    var clients_failed="";
    if(!ca_rfconfig)
       clients_failed = ' rf_config'
    if(!ca_frontend)
       clients_failed += ' frontend'
    if(!ca_mheader)
       clients_failed +=  ' mheader'
    if(!ca_mdarc)
       clients_failed=clients_failed + ' mdarc'
   
   // alert('clients failed:'+clients_failed)

   text="";
   my_colour="white"

   if(client_alarm > 0 || ctrig > 0 || ccode > 0 || ca_alarm > 0 ) // some kind of error
   {
       //alert('client_alarm= '+client_alarm+'ctrig= '+ctrig+'ccode= '+ccode+' ca_alarm= '+ca_alarm)
       if(rstate == state_stopped  &&  transition_in_progress == 0 ) // stopped
       {
         // alert(EqData["client flags"].prestart_failure)
          if(EqData["client flags"].prestart_failure > 0)
          {
             my_colour="red";
             if (clients_failed =="")
                text='Run start failed due to...'
             else
                text='Run start failed due to client(s):'+ clients_failed 
          }
          else
          {
             my_colour="pink"
             if (clients_failed =="")
                text='Run stopped with error from...'
             else
                text='Run stopped with error from client(s):'+ clients_failed 
          }
          
          text+='...  click <input name="cmd" value="Messages" type="submit"> for details';
        }
        else // running
        {
           if(!ca_alarm) // goes off at bor if this is set
           {
              text=' Non-fatal problem with... (client_alarm= '+client_alarm+'ctrig= '+ctrig+'ccode= '+ccode+' ca_alarm= '+ca_alarm+')'
    
           // now decode ccode

           ccode = ccode & 0xFF  ; // max_ccode=7 bits; strip off any higher bits that are not defined
           if (ccode == 0) // no specific information
             text+='...  click <input name="cmd" value="Messages" type="submit"> for details';
           else
           {
             var my_bit=0;
             var gotone=0;

             while (my_bit < (max_ccode+1) )
             {
 
                if (bitCheck (ccode, my_bit) == 1 )
                {
                    if(gotone)
                       message +="&"+ decode_message(my_bit)
                    else
                    {
                       message = decode_message(my_bit)
                       gotone=1;
                     }
                 }
                 my_bit++;
               } // while

             text+=message;
           } // else ccode > 0

 
      //      text='Fatal error detected (debug'+client_alarm+ctrig+ccode+') - click on Messages button (above) for more information';
       //    my_colour="red";
          } // end of not ca_alarm
       } // end of running
   text+=  '.....  <input type="button" value="clear" onclick="clear_client_flags()">'
   } // end of some kind of problem


   else
   {
      // mh_hold = mh_fix_camp,mh_fix_epics ie. waiting for camp/epics hdr
     
     if (on_hold_flag)       // on_hold_flag is bool
     {
         if ((mh_hold == mh_fix_camp) || (mh_hold == mh_fix_epics))   // special case; Type 1 run is on hold by mheader waiting for camp and/or epics
         {
            text='Run on hold waiting for initialization of CAMP and/or EPICS logging. Click "Messages" for details'
            my_colour="pink"
         }
      } 
   }
   //alert('write_status:  text='+text)
   document.getElementById('status').innerHTML=text;
   document.getElementById('status').style.backgroundColor=my_colour;

   if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending '
   return;
}

function write_info_line1()
{

//alert('hel_mismatch='+helicity_mismatch)
   var current_cycle; 
   var text, my_colspan;
   var my_class;
   text="";
 if(gbl_debug)
     {
    document.getElementById('writeStatus').innerHTML = 'writing info line 1'
   document.getElementById('gdebug').innerHTML+='<br><b> write_info_line1:</b> starting..'
	}
 
   if (rstate == state_running)                       // Cell 1
      document.getElementById('lc').innerHTML="Current Cycle" 
   else
      document.getElementById('lc').innerHTML="Last Cycle"
 
  if(ppgtype==2)
      document.getElementById('ls').innerHTML=""; // Cell 3 no scan for Type 2 
  else
  {
     if (rstate == state_running)      
        document.getElementById('ls').innerHTML="Current Scan";  // Cell 3
     else
        document.getElementById('ls').innerHTML="Last Scan";
  }

// Cell 17 contains PSM 


   if(psm_enabled)  
   {                                 // Cell 18
      if (rf_onef_enabled)
         my_class="dkgreen";
      else
         my_class="pink";
        
      document.getElementById('onef').className=my_class;

      if ( (rf_fref_enabled) || (pattern_1f.test(ppg_mode) )) // fREF enabled or ppg_mode is 1f )
      {
         if (rf_fref_enabled)                            // Cell 19
            my_class="dkgreen";
         else
            my_class="pink";

         document.getElementById('fref').className=my_class;
      }
      else
         document.getElementById('fref').innerHTML=""; // do not show fref 
   } // psm enabled
   else
   {
      document.getElementById('fref').innerHTML=""; // do not show fref
      document.getElementById('onef').innerHTML=""; // do not show onef   
   }
   if (pattern_1f.test(ppg_mode))
       document.getElementById('srtitle').innerHTML="RF (Hz)"
   if(pattern_1n.test(ppg_mode))
      document.getElementById('srtitle').innerHTML="NaCell (V)"
   document.getElementById('srtitle').className="item"

   if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'
   return;
} 

function write_info_line2()
{
                             
  var  ninc, scan_factor, current_scan, my_current_scan;
  var text, txt;
  var my_class, my_class_c, my_class_e;
  var set_value, read_value;
 if(gbl_debug)
  {
     document.getElementById('writeStatus').innerHTML = 'writing info line 2'
     document.getElementById('gdebug').innerHTML+='<br><b> write_info_line2:</b> starting..'
  }
  var thresh1= EqData.frontend.hardware["cycle thr1"];
  var thresh2= EqData.frontend.hardware["cycle thr2"];
  var thresh3= EqData.frontend.hardware["cycle thr3"];
  var tot_thresh=0;

  if(thresh1 > 0)tot_thresh++;
  if(thresh2 > 0)tot_thresh++;
  if(thresh3 > 0)tot_thresh++;

 
  text="";
  
  document.getElementById('cc').innerHTML =  current_cycle;
  if(ppgtype ==1) 
  {
     ncycle_per_scan =  parseInt(EqData.frontend.output["e1 number of cycles per scan"]); // Type 1
     ninc =             parseInt(EqData.frontend.output["e1 number of incr (ninc)"] );
     current_scan  = parseInt(OdbInfoData.variables["current scan"]);
     scan_factor = ncycle_per_scan / ninc ; // actual scan may be up/down
     my_current_scan =  Math.ceil (current_scan / scan_factor); // ceil -> round up to get current scan  
                                                                // floor()  integer part i.e. last completed scan
     document.getElementById('sc').innerHTML =  my_current_scan;  // Cell 3 scan count (Type 1 only)
  } // end of Type 1
  else
     document.getElementById('sc').innerHTML = ""; // no scan for type 2

  
// Cancelled cycles
   var ncancel = parseInt(OdbInfoData.variables["cancelled cycle"]);
   document.getElementById('cancel').innerHTML=ncancel;

// Logging

   if (rstate == state_running) 
   {                                                       
      if (data_logging_flag) // bool
      {
         if(on_hold_flag) // running but data is not being logged; on_hold_flag is bool
            my_class="yellow"                                               // Cell 5 Data Logging status 
         else // running and logging data
            my_class="dkgreen"
      }  
      else  // data not being logged
         my_class="red"
  
      document.getElementById('dlog').className=my_class;  // data logging
      document.getElementById('dlog').innerHTML="Data";
  
  // document.getElementById('sc').className=my_class;
     
     if (ppgtype==2 &&  (!got_event_flag))
     {
        // camp/epics logging status not available until first data is logged
        // check the client flag "got event"
        my_class_c="yellow"
        my_class_e="yellow";
     }
     else
     {
        if (logc_flag)
           my_class_c="dkgreen"
        else
           my_class_c="pink"
        document.getElementById('clog').className=my_class; //cell 6 camp logging

        if (loge_flag)
           my_class_e="dkgreen"
        else
           my_class_e="pink" 
      } // type 1 or type2 if data is saved

      document.getElementById('clog').className=my_class_c; //cell 6 camp logging
      document.getElementById('elog').className=my_class_e; //cell 7 epics logging  
      document.getElementById('clog').innerHTML="Camp";
      document.getElementById('elog').innerHTML="Epics";
   } // if running  
   else // not running
   {   // leave white
      document.getElementById('dlog').innerHTML="" //cell 5 data logging  
      document.getElementById('clog').innerHTML="Inactive"; //cell 6 camp logging
      document.getElementById('elog').innerHTML=""; //cell 7 epics logging  
   }

// Thresholds
   if (tot_thresh > 0) // one or more of the thresholds is active
   {
      if(thresh1 > 0)  //  Cell 8 Ref Threshold is active
      {
         if(thresh_last_failed==1)
            my_class="pink"
         else
            my_class="dkgreen"
       }
       else
          my_class="white"

       document.getElementById('rthr').className=my_class; //cell 8 ref thresh  
       document.getElementById('rthr').innerHTML="Ref"

       if(thresh2 > 0) // Cell 9 Prev threshold is active
       {
          if(thresh_last_failed==2)
             my_class="pink"
          else
             my_class="dkgreen"
       }
       else
          my_class="white"

       document.getElementById('pthr').className=my_class; //cell 9 Prev thresh  
       document.getElementById('pthr').innerHTML="Prev"

       // Skip cycles
       if (thresh_last_failed ==4)  //   Cell 11,12  skip cycle counter
       {
          document.getElementById('skip').classname ="item";
          document.getElementById('skipc').className="orange";  // cell 12
          document.getElementById('skipc').innerHTML = skip_tol;
       }
       else
       {
          document.getElementById('skip').innerHTML =""; // do not show skip cell 11
          document.getElementById('skipc').innerHTML =""; // do not show skip cntr cell 12
       }    
    }
    else  // no thresholds are active
    {
        document.getElementById('rthr').innerHTML =""; // ref
        document.getElementById('pthr').innerHTML ="Disabled"; // prev
        document.getElementById('skip').innerHTML =""; // skip
        document.getElementById('skipc').innerHTML =""; // skip counter cell 12
    }

//  Helicity Set Value     Cell 13,14 
    document.getElementById('helset').innerHTML =helicity_set +' '+helstate[helicity_set];
    if (helicity_set == 1) // Hel Up
       document.getElementById('helset').className="helup";
    else

       document.getElementById('helset').className="heldown";
    //  document.getElementById('helset').innerHTML = helicity_set;
    
 
//  Helicity Read Value    Cell 15,16 

    text+='<td  class="item" id="hread">'
    if(dual_channel_mode)
       document.getElementById('hread').innerHTML = "Latched:"
       

    if(helicity_read == 1) // Hel Up
       document.getElementById('helrd').className="helup";
    else
       document.getElementById('helrd').className="heldown";
    document.getElementById('helrd').innerHTML = helicity_read+' '+helstate[helicity_read];
    


  // Mismatch - show only if > 0
  if(helicity_mismatch > 0)
  { 
     document.getElementById('hmis').innerHTML="Mismatch";
     document.getElementById('mismatch').className="red";
     document.getElementById('mismatch').innerHTML =helicity_mismatch;
  }
  else
  {
     document.getElementById('hmis').innerHTML ="";  // mismatch
     document.getElementById('mismatch').innerHTML =remember_mismatch; // mismatch counter
  }

 if(psm_enabled)
 {
    if( rf_onef_ampl == 0)   //  RF Amplitude  one-f   cell 19
       my_class="pink" 
    else
       my_class="beige"  
    document.getElementById('a1f').className=my_class;
    document.getElementById('a1f').innerHTML = rf_onef_ampl; 

   if (rf_fref_enabled ||  (ppg_mode == "1f"))   
   {  // only show this for ppgmode 1f or if fref enabled  //  Cell 20 RF Amplitude  fREF
      
      if ( !rf_fref_enabled || rf_fref_ampl == 0 )
         my_class="pink" 
      else
         my_class="beige"

      document.getElementById('afref').className=my_class;
      document.getElementById('afref').innerHTML = rf_fref_ampl; 
     
    }
    else
        document.getElementById('afref').innerHTML =""; // do not show

  } // psm_disabled
  else
  {
    document.getElementById('psmamp').innerHTML ="Disabled";
    document.getElementById('psmamp').className="pink";
    document.getElementById('a1f').innerHTML =""; // do not show
    document.getElementById('afref').innerHTML =""; // do not show
  }
 

 if(gbl_sr_mode)
     { 
	 // elem= document.getElementById('sr').innerHTML;
	 // alert(' elem='+elem);
     
         set_value = parseInt(OdbInfoData.variables["microwave set"])
	 read_value = parseInt(OdbInfoData.variables["microwave read"])

	 if( document.getElementById('sr').innerHTML =="")
	     {
		 //alert(' remember_sample_ref_table='+remember_sample_ref_table);
              document.getElementById('sr').innerHTML= remember_sample_ref_table
	     }
         document.getElementById('setsr').innerHTML = set_value;	 
         document.getElementById('readsr').innerHTML = read_value;
	 if(set_value ==0)
	 {
            document.getElementById('srtitle').innerHTML="Reference"
            document.getElementById('srtitle').className="reference"
            document.getElementById('setsr').className="reference"
            document.getElementById('readsr').className="reference"
	 }
	 else
	{
	    document.getElementById('srtitle').innerHTML="Sample"
            document.getElementById('srtitle').className="sample"
            document.getElementById('setsr').className="sample"
            document.getElementById('readsr').className="sample"
       }
	 if(set_value != read_value)
	     document.getElementById('readsr').className="red";
     }
 else if (pattern_1f.test(ppg_mode))
     {
	 if( document.getElementById('sr').innerHTML =="")
	     {
		 //alert(' remember_sample_ref_table='+remember_sample_ref_table);
              document.getElementById('sr').innerHTML= remember_sample_ref_table
	     }
         set_value = parseInt(OdbInfoData.variables.rf_set_1f)
	 // no readback for frequency
         document.getElementById('rdtitle').innerHTML=""
	 document.getElementById('readsr').innerHTML ="";
	 document.getElementById('setsr').className="white"
         document.getElementById('setsr').innerHTML = set_value;
     }
 else if (pattern_1n.test(ppg_mode))
     {
	 if( document.getElementById('sr').innerHTML =="")
	     {
		 //alert(' remember_sample_ref_table='+remember_sample_ref_table);
              document.getElementById('sr').innerHTML= remember_sample_ref_table
	     }
	 //   if(document.getElementById('rdtitle').innerHTML="")
	 //     document.getElementById('rdtitle').innerHTML="Read:"
         set_value = parseInt(OdbInfoData.variables["epicsdev set(v)"])
	 read_value = parseInt(OdbInfoData.variables["epicsdev read(v)"])
         document.getElementById('setsr').innerHTML = set_value;	 
         document.getElementById('readsr').innerHTML = read_value;
	 document.getElementById('setsr').className="white"
	 document.getElementById('readsr').className="white"
     }
 else
     {
        document.getElementById('srtitle').innerHTML =""; // do not show
        document.getElementById('sr').innerHTML =""; // do not show
     }
 
  if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'
  return; 
                                 
}// end write_info_line2


function build_cycles_info()
{
   var num_xx,units;
   var my_class,txt,text;
   text="";
  
 if(gbl_debug)
    document.getElementById('writeStatus').innerHTML = 'building cycles info'
   document.getElementById('dd2').innerHTML="<small>"+data_dir+"</small>";

   
   if (rstate == state_running)
   {   
     //  Calculate number cycles/scans here in case needed for status message below 
     if(ppgtype ==1)
     {
        num_xx =  parseInt(EqData.frontend.hardware["num scans"])
        units = ' scan(s)';

     }
     else
     {
        num_xx =    parseInt(EqData.frontend.hardware["num cycles"])
        units = ' cycle(s)';
     }
   //  alert("num_xx="+num_xx)
     if(num_xx == 0)                         // Cell 1-2    free-running 
     {
 
         my_class="white"         
         txt='Free-running'
       
         if(ppgtype == 1)                   
            txt +='; '+ncycle_per_scan+ ' cycles/scan';   
        
     }   
     else
     {  // stop after..                    // Cell 1-2    stop after...
        my_class="pink"
        txt ='Stop after '+ num_xx + units
        if(ppgtype == 1)
           txt+=' ('+ncycle_per_scan*num_xx+'cycles)'
 
     }
    
     text='<td class='+my_class+' id="stmsg" colspan=2>'+txt+'</td>' ;   

  } // end of running
  else
     text='<td class="white" id="stmsg" colspan=2></td>';
   document.getElementById('cyinfo').innerHTML = text // cyinfo

 return ;
}

function update_new_line()
{ // update line with Data Directory, alarms active etc
 if(gbl_debug)
     document.getElementById('writeStatus').innerHTML = 'updating line Data dir .. '
  set_escheck(epics_switch_check); // set colour, initialize checkbox
  set_helcheck(hel_check); // set colour, initialize checkbox
  set_alarms(alarm_system_active) // set colour, initialize checkbox
    
 if(hel_flip) // bool  Helicity flipping
      document.getElementById('flip').className="green";
  else
      document.getElementById('flip').className="red";
  
  write_dual_channel_mode(dual_channel_mode); // fill dual/single  channel mode box
 
}
function write_dual_channel_mode(val)
{
  dual_channel_mode = parseInt(val);
  if(dual_channel_mode)
  {
     document.getElementById('dcm').innerHTML = remember_dcm;
     document.getElementById('dcm').className="orange"; //dcm enabled
  }
  else
  {
     document.getElementById('dcm').innerHTML = remember_scm;
     document.getElementById('dcm').className="yellow"; // scm enabled
 }  
  document.form1.box4.checked = dual_channel_mode;
  //alert('write_dual_channel_mode: dual_channel_mode='+ dual_channel_mode);
}

function set_escheck(val)
{
 if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br><b>set_escheck:</b> starting'
 document.form1.box3.checked=parseInt(val); // initialize to correct value
 
  if(val==1) // bool  Check on Epics switches
      document.getElementById('epchk').className="green";
  else
  { 
    if(run_number > 40000)
    {
     
      if(document.getElementById('epchk').className == "red") 
         document.getElementById('epchk').className = "redwhite";
      else
         document.getElementById('epchk').className="red";
     }
     else
        document.getElementById('epchk').className = "red"
  }
 if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'
  return;
}


function set_autorun_pause(val)
{
   
   document.form1.box3.checked=parseInt(val); // initialize to correct value
   //ODBSet(autorun_pause_path, val);
   var paths=new Array();
   var values=new Array();
   paths[0]=autorun_pause_path;
   values[0]=val;
   async_odbset(paths,values);
}

function clear_tip()
{
   async_odbset(["/runinfo/transition in progress"],["0"]);
}

function enable_autorun_button()
{
    if(autoruns_enable)
        enable_autorun(0);
    else
        enable_autorun(1);
}

function enable_autorun(val)
{
   var values=new Array();
   var paths=new Array();
  
 
   values[0]=parseInt(val);
   autoruns_enable =  values[0] // set global for calculate_update_timer
   paths[0]=autorun_enable_path;

  if (values[0])  // BOOL
       my_colour="green";
    else
       my_colour="pink";
   document.getElementById('auen').style.backgroundColor=my_colour;
   document.form1.abox2.checked=values[0];


   if(!autoruns_enable) // BOOL
         document.getElementById('auto_button').className="none";
   else
     document.getElementById('auto_button').className="input6";

   if(!autoruns_enable)
   {   // set number of cycles/scans to zero if autoruns are turned off
      if(ppgtype==1) // ppgtype cannot be changed while autoruns are on
          paths[1]="/Experiment/edit on start/Number of scans"
      else
          paths[1]="/Experiment/edit on start/Number of cycles"
      values[1]=0;
   }
   progressFlag= progress_write_callback
   if(gbl_debug) 
         document.getElementById('gdebug').innerHTML+='<br>enable_autorun: writing  paths '+paths+' and values '+values

   mjsonrpc_db_paste(paths,values).then(function(rpc) {
        var i;
   if(gbl_debug)
        document.getElementById('readStatus').innerHTML = 'enable_autorun:  status= '+rpc.result.status
        len=rpc.result.status.length // get status array length
       // alert('enable_autorun: length of rpc.result.status='+len)

        for (i=0; i<len; i++)
        {
           if(rpc.result.status[i] != 1) 
                   alert('enable_autorun: status error '+rpc.result.status[i]+' at index '+i)
        } 
 if(gbl_debug)
     {
        document.getElementById('writeStatus').innerHTML='enable_autorun: writing paths '+paths+' and values '+values
         document.getElementById('gdebug').innerHTML+='<br>enable_autorun: wrote paths '+paths+' and values '+values
     }
     progressFlag=  progress_write_got_callback
    // alert('calling reload now')
       location.reload(); // reload now
    }).catch(function(error)
          { 
          mjsonrpc_error_alert(error); });

   calculate_update_timer();
   write_ppgmode_buttons()
   return;
}


function set_helcheck(val)
{
  
 // alert('set_helcheck val='+val)
 if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br><b>set_helcheck:</b> starting'
 document.form1.box2.checked=parseInt(val); // initialize to correct value
 if(val==1) // integer  Check on Helicity readback
      document.getElementById('hechk').className="green";
 else
 {
    if(run_number > 40000)
    {
      if(document.getElementById('hechk').className == "red") 
         document.getElementById('hechk').className = "redwhite";
      else
         document.getElementById('hechk').className = "red"
    }
    else
       document.getElementById('hechk').className = "red"
 }  
 if(gbl_debug) document.getElementById('gdebug').innerHTML+=' and ending'
 return;
}

function show_auto_params(val)
{
   show_autorun_params=parseInt(val);
   build_autorun_params()
}


function set_alarms(val)
{
  
  //alert('set_alarms val='+val)
 document.form1.box0.checked=parseInt(val); // initialize to correct value
 if(val==1) // integer  Alarms system on
      document.getElementById('aact').className="green";
 else
 {
    if(run_number > 40000)
    {
      if(document.getElementById('aact').className == "red") 
         document.getElementById('aact').className = "redwhite";
      else
         document.getElementById('aact').className = "red"
    }
    else
       document.getElementById('aact').className = "red"
 }  
 return;
}


//function to encode the non-fatal error message
//      Uses globals ccode, cclient, camp_ok, epics_ok, n_epics, units  

function decode_message(bit)
{
 var msg;

 // test this first    
 if (bit == 6)  // param = u
    msg = ' Requested number of '+ units +' reached'

 else if (bit == 1)
    msg = ' EPICS Scan'
 else if(bit == 2)
    msg = ' CAMP  Scan'
 else if(bit == 3)
    msg = ' RF Config '

// test this before camp/epics logging
 else if (bit == 7)  // param = u
    msg = 'mheader restarted during a Type 1 run'

 else if(bit == 4)
 {
    msg = ' CAMP Logging '
    if (camp_ok == 3) 
         msg = msg + '(timeout) '
 }        
 else if(bit == 5)
 {
    msg = ' EPICS Logging '
    if (epics_ok == 3) 
        msg = msg + '(timeout) '
    else
    {      
       if(n_epics <= 0) 
          msg = ' EPICS Log: none selected'
    }
 }  

 else
    msg = '?'  // code not defined    

//document.write('decode_message: bit='+bit+' msg='+msg)
return msg
}


function clear_alarm(num)
{

   // global array alarm_names[] to passes the name to this function
   var alarm_path='/alarms/alarms/'+alarm_names[num]+'/triggered'
      
   // array to pass parameters to async_odbset
   var alarm_paths=new Array();
   var alarm_values=new Array();

   alarm_paths[0]=alarm_path;

   //ODBSet( alarm_path,0);
   async_odbset(alarm_paths, ["0"]); // clear alarm
   var id='alarm'+num;
   document.getElementById(id).innerHTML = ""; // clear the alarm banner
}


function clear_client_flags()
{  // after run start has failed, several client flags may have been set
   var client_flag_paths=new Array();
   var client_flag_values=new Array();

   var jj=0; // counter

  // alert(' clear_client_flags: Clearing various client flags')
  if( EqData["client flags"].mh_on_hold == undefined)
       alert('clear_client_flags: .../client flags/mh_on_hold is undefined. Cannot clear flag') 
   
  else if( EqData["client flags"].mh_on_hold == 1)
  {    
       //  ODBSet("/Equipment/FIFO_acq/client flags/mh_on_hold",0);
       client_flag_paths[jj] = "/Equipment/FIFO_acq/client flags/mh_on_hold";
       client_flag_values[jj] = 0;
       jj++;
  }

  if( EqData["client flags"].prestart_failure  == undefined)
     alert('clear_client_flags: ... client flags/prestart_failure is undefined. Cannot clear flag')
  else if( EqData["client flags"].prestart_failure == 1)
  {
      // ODBSet("/Equipment/FIFO_acq/client flags/prestart_failure",0);
      client_flag_paths[jj] ="/Equipment/FIFO_acq/client flags/prestart_failure";
      client_flag_values[jj] = 0;
       jj++;
  }

  if( EqData["client flags"]["error code bitpat"] == undefined)
    alert('clear_client_flags: ... client flags/error code bitpat is undefined. Cannot clear flag')

  else if ( EqData["client flags"]["error code bitpat"] == 1)
  {
      // ODBSet("/Equipment/FIFO_acq/client flags/error code bitpat",0);
      client_flag_paths[jj] ="/Equipment/FIFO_acq/client flags/error code bitpat";
      client_flag_values[jj] = 0;
      jj++;
  }
  if( EqData["client flags"]["client alarm"] == undefined)
       alert('clear_client_flags:... client flags/client alarm is undefined. Cannot clear flag')

  else if ( EqData["client flags"]["client alarm"] == 1)
  {
      //ODBSet("/Equipment/FIFO_acq/client flags/client alarm",0);
      client_flag_paths[jj] ="/Equipment/FIFO_acq/client flags/client alarm" ;
      client_flag_values[jj] = 0;
      jj++;
  }  

  if( EqData["client flags"].start_abort == undefined)
     alert('clear_client_flags:... client flags/client alarm is undefined. Cannot clear flag')

  else if( EqData["client flags"].start_abort == 1)
  {
      //ODBSet("/Equipment/FIFO_acq/client flags/start_abort",0);
      client_flag_paths[jj] ="/Equipment/FIFO_acq/client flags/start_abort";
      client_flag_values[jj] = 0;
      jj++;
  }
  if( EqData["client flags"].mdarc == undefined)
     alert('clear_client_flags:... client flags/mdarc is undefined. Cannot clear flag')

  else if( EqData["client flags"].mdarc != 1)
  { 
      //ODBSet("/Equipment/FIFO_acq/client flags/mdarc",1);  // true is success
      client_flag_paths[jj] ="/Equipment/FIFO_acq/client flags/mdarc";
      client_flag_values[jj] = 1;
      jj++;
  }

  if( EqData["client flags"].rf_config == undefined)
     alert('clear_client_flags:... client flags/rf_config is undefined. Cannot clear flag')
  else if( EqData["client flags"].rf_config != 1)
  {
      // ODBSet("/Equipment/FIFO_acq/client flags/rf_config",1);
      client_flag_paths[jj] ="/Equipment/FIFO_acq/client flags/rf_config";
      client_flag_values[jj] = 1;
      jj++;
  }

  if( EqData["client flags"].mheader == undefined)
     alert('clear_client_flags:... client flags/mheader is undefined. Cannot clear flag')
  else   if( EqData["client flags"].mheader != 1)
  {
      //ODBSet("/Equipment/FIFO_acq/client flags/mheader",1);
      client_flag_paths[jj] ="/Equipment/FIFO_acq/client flags/mheader"
      client_flag_values[jj] = 1;
      jj++;
  }

  if( EqData["client flags"].frontend == undefined)
     alert('clear_client_flags:... client flags/frontend is undefined. Cannot clear flag')

  else if( EqData["client flags"].frontend != 1)
  {
      // ODBSet("/Equipment/FIFO_acq/client flags/frontend",1);
      client_flag_paths[jj] ="/Equipment/FIFO_acq/client flags/frontend";
      client_flag_values[jj] = 1;
      jj++;
  }
  
  async_odbset( client_flag_paths, client_flag_values); // called with arrays  
}

function clear_alarms(num)
{
   var alarm_paths=new Array();
   var alarm_values=new Array();
   var i, alarm_path, id;
   //alert('clear_alarms: clients whose alarms are set are '+alarm_clients+' and length of array is '+alarm_clients.length);
   for(i=0; i<alarm_clients.length; i++)
   {
      alarm_paths[i]='/alarms/alarms/'+alarm_clients[i]+'/triggered'          
      alarm_values[i]='0';
   }
   async_odbset( alarm_paths,alarm_values); // called with arrays  
   id='alarm'+num;
   document.getElementById(id).innerHTML = "";
}






function write_frontend_event_stats()
{
   
  // Fifo_acq equipment actually sends the histos now
 if(gbl_debug)
      document.getElementById('writeStatus').innerHTML = 'writing frontend event stats'
   if(ppgtype==1)
      document.getElementById('eqtitles').innerHTML = remember_eqtitles_type1;  
   else
      document.getElementById('eqtitles').innerHTML = remember_eqtitles_type2;  

   var text="";
   
   //alert(' write_frontend_event_stats: ppgtype='+ppgtype+' and ppgtype_last_run= '+ppgtype_last_run)

   if(rstate == state_stopped && ppgtype != ppgtype_last_run)
      text+='<td class="festats" style="font-size: 75%; text-align: center" colspan=6>not yet available as last run was Type '+ppgtype_last_run+'</td>';
   else
   {
      text+='<td class="festats">'+ScalerData.statistics["events sent"]+'</td>';
      text+='<td class="festats">'+HistoData["events sent"]+'</td>';
      text+='<td class="festats">'+OdbInfoData.statistics["events sent"]+'</td>';
      if(ppgtype==1)
      {
         text+='<td class="festats">'+CampData.statistics["events sent"]+'</td>';
         text+='<td class="festats">'+HeaderData["events sent"]+'</td>';
         text+='<td class="festats">'+EpicsLogData.statistics["events sent"]+'</td>';
      }
   }
   // alert(' write_frontend_event_stats: text='+text)
    document.getElementById('eqstats').innerHTML = text;  
   
   return;

}

function write_cb_stats()
{
   var const1f_mode;
  // var eq_names=new Array("PPG (ms)","Max (ms)","Av (ms)","Repeats","%");
   var info_path="/Equipment/info odb/variables/";
   var ppg_cb_cntr,per_cent_restarts, max_busy, min_busy, av_busy, daq_service;
 //  alert('write_cb_stats: remember_cb_titles= '+ remember_cb_titles);
 if(gbl_debug)
     document.getElementById('writeStatus').innerHTML = 'writing cb stats'
   const1f_mode = 0; // default
 
   if(ppgtype==1)
   {
       if(pattern_x.test(ppg_mode))  // look for 1f
       sw = RegExp.$1;
       if(sw=='f')
       {
         
	  const1f_mode = get_bool(EqData.frontend.input["e1f const time between cycles"],"/Equipment/fifo_acq/frontend/input/e1f const time between cycles");  // global
       }
    }
  
    if(const1f_mode)			    
    {  // Math.round
      // document.getElementById('cbtitles').innerHTML = remember_cb_titles; 
       document.getElementById('cbtable').innerHTML = remember_cb_table; 
       document.getElementById('daqbsy').innerHTML = 'DAQ<br>Busy<br>Stats</td>';
       ppg_cb_cntr = OdbInfoData.variables["const1f ppg_cb_cntr"];
       per_cent_restarts =  OdbInfoData.variables["const1f perCent"];
       per_cent_restarts = roundup(per_cent_restarts, 2); // to 2 dp
       max_busy =  Math.round( OdbInfoData.variables["const1f max cb (ms)"]  ); // round to nearest integer
       min_busy =  Math.round( OdbInfoData.variables["const1f min cb (ms)"]  ); // round to nearest integer
       av_busy =   OdbInfoData.variables["const1f av cb (ms)"];
       if(parseFloat(av_busy) < 0)
          av_busy=0;
       daq_service =   EqData.frontend.input["daq service time (ms)"];

       txt = '<td  class="festats">'+daq_service+'</td>';
       txt+= '<td  class="festats">'+max_busy+'</td>';
       txt+= '<td  class="festats">'+min_busy+'</td>';
       txt+= '<td  class="festats">'+av_busy+'</td>';
       txt+= '<td  class="festats">'+ppg_cb_cntr+'</td>';
       txt+= '<td  class="festats">'+per_cent_restarts+'</td>';

       document.getElementById('1fstats').innerHTML=txt;
    }
    else
    {
      // document.getElementById('cbtitles').innerHTML = "";   // default
      // document.getElementById('1fstats').innerHTML = "";   // default
        document.getElementById('cbtable').innerHTML = "";
       document.getElementById('daqbsy').innerHTML = ""; // default
    }
    
// alert('write_cb_stats: const1f_mode= '+ const1f_mode);
   return;
}


function write_cycle_scalers(ppgtype)
{
   //  This is now done for Scaler A having 2 inputs only (Back Sum and Front Sum)
   //  If we return to 32 inputs for Scaler A, return to the commented indices,
   //  and add 30 to indices where noted.
   //  There is a difference of 30 between the two (i.e. 32-2)
   //  
   //    32 Channels :
   //    var scaler_indices_1 =new Array("52","53","54", "55",  "58", "59");  // 2x16 ch front/back
   //    var scaler_indices_2 =new Array("44","45","46", "47",  "50", "51");  // 2x16 ch front/back
   //  


   //   BNMR Cycle Scaler Indices
   //
   //   Type 1
   //                                 Back Front            Neutral Beam
   //                                 Sum  Sum   B/F  Asym   Sum   Asym	   
      var scaler_indices_1 =new Array("22","23","24", "25",  "28", "29");  // 2x1  ch front/back

   //   Type 2
   //                                 Back Front            Neutral Beam
   //                                 Sum  Sum   B/F  Asym   Sum   Asym	   
      var scaler_indices_2 =new Array("14","15","16", "17",  "20", "21");  // 2x1  ch front/back
   //     cumulative +                 22     to       25
   //     cumulative -                 26     to       29

   //   BNMR titles
   var bnmr_titles=new Array("Back","Front","Back/Front","Asym","NB Sum","NB Asym");
   var len_bnmr_titles =bnmr_titles.length;




   // BNQR Cycle Scaler Indices
   //   Both Types 
   //                                 Pol Cycle   Neutral Beam
   //                                 Sum  Asym   Sum   Asym	   
   // var scaler_indices   =new Array("12"  "13"  "14", "15");

   //   BNQR titles
   var bnqr_titles=new Array("Pol Sum","Pol Asym","NB Sum","NB Asym");
   var len_bnqr_titles =bnqr_titles.length;

   // common
   var j;
   var text="";
   var text_debug="";
 if(gbl_debug)
       document.getElementById('writeStatus').innerHTML = 'write cycle scalers'
   text='<td class="festatt" colspan=2></td>'; // for id=percytitles

   //           B N Q R

   if(bnqr_expt)
   {
      // Write the titles for Cycle Scalers

      for (j=0; j < len_bnqr_titles ; j++)
        text+='<td class="festatt">'+bnqr_titles[j]+'</td>'

      document.getElementById('percytitles').innerHTML=text;
      document.getElementById('qplus').innerHTML="";  // clear;  BNMR only
      document.getElementById('qminus').innerHTML=""; // clear;  BNMR only

      text='<td colspan=2 class="festatt">per Cycle</td>';
      if(rstate == state_stopped && ppgtype != ppgtype_last_run)
	  {
	      // alert('ppgtype is '+ppgtype +' and ppgtype_last_run is '+ ppgtype_last_run); 
	      // the scalers will show nonsense values if the PPG type has been changed while run stopped
	      text+='<td class="festats" style="font-size: 75%; text-align: center" colspan=6>not yet available as last run was Type '+ppgtype_last_run+'</td>';
	      // alert('write_cycle_scalers: text='+text)
		  document.getElementById('percycle').innerHTML=text;
	      return;
	  } 
      
   

      if(HSCL==undefined)
      {
        text+='<td class="festats" style="font-size: 75%; text-align: center" colspan=6>cannot read scaler array HSCL</td>';
        document.getElementById('percycle').innerHTML=text;
        return;
      }

      for (j=12; j<16; j++)  
      {
              if(HSCL[j]  == -99999 )
                 text+='<td class="festats">NaN</td>';
              else if (j == 12 | j==14 )  // integer counters (sum)    (future - add 30 to indices)
                 text+='<td class="festats">'+parseInt(HSCL[j])+'</td>'; // show as integer
              else   // float
                 text+='<td class="festats">'+roundup(HSCL[j], 4)+'</td>'; 
              
      }
      
      document.getElementById('percycle').innerHTML=text;
  



   } // end bnqr_expt

   //           B N M R
   
   else if(bnmr_expt)
   {

      var type1_offset = 8; // 22-14=8; 

      // if(debug)
      //   document.getElementById('mytest').innerHTML+='<br>mytest...ppgtype = '+ppgtype+' ppgtype_last_run='+ppgtype_last_run+'  document.form1.ppgtype.value = '+ document.form1.ppgtype.value+ 'countdown: '+countdown

  
      // this code will work for Type 1 or Type 2
      istart=14;
      offset = 0
  
      if(ppgtype == 1)
         offset=type1_offset;

      document.getElementById('qminus').innerHTML=''; // clear Type2 line
      document.getElementById('qplus').innerHTML=''; // clear Type2 line

      text='<td colspan=2 class="festatt">per Cycle</td>';

 
      if(rstate == state_stopped && ppgtype != ppgtype_last_run)
      {
         // alert('ppgtype is '+ppgtype +' and ppgtype_last_run is '+ ppgtype_last_run); 
         // the scalers will show nonsense values if the PPG type has been changed while run stopped
        text+='<td class="festats" style="font-size: 75%; text-align: center" colspan=6>not yet available as last run was Type '+ppgtype_last_run+'</td>';
	// alert('write_cycle_scalers: text='+text)
        document.getElementById('percycle').innerHTML=text;
        return;
      } 
   
      if (HSCL == undefined)
      {
        text+='<td class="festats" style="font-size: 75%; text-align: center" colspan=6>cannot read scaler array HSCL</td>';
        document.getElementById('percycle').innerHTML=text;
        return;
      }


      istart += offset;			   
      istop  = istart + 8; 

      for(j=istart; j< istop; j++)
      {
           if(j<(istart+4) || j>(istart+5)) // skip indices 26-7  or 18-9
           {
              if(HSCL[j]  == -99999 )
                 text+='<td class="festats">NaN</td>';
              else if (j < (istart+2) || j == (istart+6))  // 24,28 or 16,20 integer counters
                 text+='<td class="festats">'+parseInt(HSCL[j])+'</td>'; // show as integer
              else   // float
                 text+='<td class="festats">'+roundup(HSCL[j], 4)+'</td>'; 
           }
       }
       document.getElementById('percycle').innerHTML=text;
       //  document.getElementById('mytest').innerHTML=text_debug;

       if(ppgtype == 1)
          return;

      // Cumulative Counters
       text='<td class="festatt" rowspan=2>Cumulative</td>';
       text+='<td class="festatt">+</td>'
       for(j=22; j< 26; j++)
       {
          if(HSCL[j]  == -99999 )
            text+='<td class="festats">NaN</td>';
       
          else if(j < 24)
             text+='<td class="festats">'+parseInt(HSCL[j])+'</td>'; // show as integer
          else
             text+='<td class="festats">'+roundup(HSCL[j], 4)+'</td>';  // float

       }
       text+='<td class="festats" colspan=2 rowspan=2></td>'
       document.getElementById('qplus').innerHTML=text;

       text='<td class="festatt">-</td>'
       for(j=26; j< 30; j++)
       {
          if(HSCL[j]  == -99999 )
            text+='<td class="festats">NaN</td>';
          else if(j < 28)
             text+='<td class="festats">'+parseInt(HSCL[j])+'</td>'; // show as integer
          else
             text+='<td class="festats">'+roundup(HSCL[j], 4)+'</td>';  // float
       }
       document.getElementById('qminus').innerHTML=text;

    } // bnmr_expt

    return; 
}


function read_histo_titles(ntemp)
{
   // temporary param ntemp for type1
   var numhist=0;
   var hisnames=new Array(25);
   var text="";
 if(gbl_debug)
     document.getElementById('writeStatus').innerHTML = 'writing histo titles'
   // Histo names are in directory /Equipment/fifo_acq/mdarc/histograms/
   if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br><b> read_histo_titles:</b> starting..'
   var i,titles_path;

   if(ppgtype==2)
      hisnames = EqData.mdarc.histograms.titles
   else
      hisnames = EqData.mdarc.histograms.type1_titles
	  // alert('read_histo_titles: hisnames='+hisnames)
   if(hisnames == undefined )
   {
      text='histogram names not available'
      numhist=0; 
   }	
   else
   {						    

       // write histogram titles  (S/R mode  only "num type2 frontend histograms" titles are used
       if (ppgtype == 2)
       {  
          len=parseInt(EqData.frontend.input["num type2 frontend histograms"]) // num type2 frontend histograms
	  // All type 2 modes have only 10 titles shown (sample not shown even if s/r mode enabled
       }
       else
          len=parseInt(EqData.frontend.input["num type1 frontend histograms"]) // num type1 frontend histograms
   
       // alert('hisnames.length='+hisnames.length+' len= '+len);
   	 
       for (i=0; i< len; i++)
       {
         //document.getElementById('mytest').innerHTML +=' ; '+hisnames[i]+' length '+hisnames[i].length
         if(hisnames[i].length <= 1) break
         text+='<td class="statt">'+hisnames[i]+'</td>';	      
       }
       numhist=i;
       //  alert('len= '+len+' numhist='+numhist);
   }
   document.getElementById('histitles').innerHTML=text;
   if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending, returning numhist='+numhist
   return numhist;
}


function get_histo_data(numhist)
{  // get the histogram statistics data
   // uses globals 

   var i=0;
   var temp;
   var my_key;
   
   // statistics
   var hisdata=new Array(25);
   var i;

   // alert('totals_path '+totals_path);
 if(gbl_debug)
     {
        document.getElementById('writeStatus').innerHTML = 'getting histo data'
        document.getElementById('gdebug').innerHTML+='<br><b> get_histo_data:</b> starting..'
     }
   // histogram contents
   hisdata   =  EqData.mdarc.histograms.output["histogram totals"]
   if(hisdata == undefined)
   {
      text='<td class="festats" style="font-size: 75%; text-align: center" colspan=20> Could not read histogram contents'
      if(gbl_debug) document.getElementById('gdebug').innerHTML+='<span style="color:red"> Could not read histogram contents</span>'
      return; 
   }
   //alert('hisdata '+hisdata)

  
   var text_debug="";
   var text="";


   /* I don't think this is needed any more 
 // temp while we have no histo titles for type 1
      if (numhist==0)
   {
     // alert('hisdata.length= '+hisdata.length);
      for (i=hisdata.length; i>=0; i--)
        if(hisdata[i] > 0)
        {
           numhist=i+1;
           break;
        }
      
     // alert('numhist= '+numhist); 
      read_histo_titles(numhist);     // temp read the titles here
   }
   // end of temp
   */


 
   
   if(rstate == state_stopped && ppgtype != ppgtype_last_run)
   {
      text='<td class="festats" style="font-size: 75%; text-align: center" colspan=20>not yet available as last run was Type '+ppgtype_last_run+'</td>';
      document.getElementById('hisvalues').innerHTML=text;
      document.getElementById('refvals').innerHTML="";
   }				    
   else
       {  // running or same ppgtype as last run

       if(!gbl_sr_mode)
	   {  // Type 1 or Type 2
	   //alert(' gbl_sr_mode ='+gbl_sr_mode)
	  for (i=0; i<numhist; i++)
	  {
	      text+='<td class="stats">'+hisdata[i]+'</td>';
	  }
	   
          document.getElementById('hisvalues').innerHTML=text;
          document.getElementById('refvals').innerHTML="";
       }
       else
       {  // Sample/Ref mode  Type 2 only 
               
	  // sample/ref mode  2 Fluor histos (not duplicated)  index 0,1
          //                  8 Reference histos               index 2,10
          //                  8 Sample histos                  index 11,18
          //     Default is to show only the SAMPLE histos
	  for (i=0; i<2; i++)
	      text+='<td class="stats">'+hisdata[i]+'</td>'; // Fluor 1,2

          var len = parseInt(EqData.frontend.input["num type2 frontend histograms"]) // number of histos sent out 10 (NOT S/R)
	  var max = parseInt(EqData.frontend.input["num type2 fe histos sr mode"]);  // num histos sent in s/r mode 18 total (BNQR)
	  //  alert('len='+len+' and max='+max)
	      for (i=len; i<max; i++)
	      text+='<td class="stats">'+hisdata[i]+'</td>';   // Sample histos 
              document.getElementById('hisvalues').innerHTML=text


              if(document.form1.box5.checked)
	      {   // Show the reference values as well
                  document.getElementById('refvals').innerHTML=remember_ref_line;
                  text='<td colspan=2 class="stats"></td>' // first two histos are Fluor and not duplicated for Ref
		  for (i=2; i<len; i++)
	   	  {
		     text+='<td class="stats">'+hisdata[i]+'</td>'; // Reference histos
		  }
		  
                  document.getElementById('refvals').innerHTML=text;
              }
	      else
		  document.getElementById('refvals').innerHTML="";
       } // end sample/ref mode
           

    
   }


   if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'
   return;
}

function write_ppgmode_buttons()
{
   // may call build_ppgmode_buttons
   if(gbl_debug)
     {
       document.getElementById('writeStatus').innerHTML = 'write_ppgmode_buttons with ppgtype='+ppgtype+' ppg_mode='+ppg_mode
       document.getElementById('gdebug').innerHTML+='<br><b>write_ppgmode_buttons:</b> starting with  ppgtype='+ppgtype+' ppg_mode='+ppg_mode
       }
   document.getElementById('ppgmodemsg').innerHTML=''
  
   document.getElementById('ppgbtns').innerHTML =  remember_ppg_buttons;

   // alert('write_ppgmode_buttons: remember_ppg_buttons ='+remember_ppg_buttons)
 //   alert('build_ppgmode: autoruns_enable ='+autoruns_enable)
   if (autoruns_enable) // BOOL
   {
       document.getElementById('ppgbtns').innerHTML= '<br>PPG mode cannot be changed while Autorun is enabled <br>';
       if(gbl_debug)
          document.getElementById('gdebug').innerHTML+=' and returning (autoruns enabled)'
       return;
    }

    if  (rstate == state_running)
    {
        document.getElementById('ppgbtns').innerHTML= '<br>PPG mode cannot be changed while running<br>';
         if(gbl_debug)
          document.getElementById('gdebug').innerHTML+=' and returning (running)'
       return;
    }

    if(transition_in_progress)
    {
        document.getElementById('ppgbtns').innerHTML= '<br>PPG mode cannot be changed while transition is in progress<br>';
         if(gbl_debug)
          document.getElementById('gdebug').innerHTML+=' and returning (transition)'
       return;
    }

  // run stopped
  document.getElementById('ppgbtns').innerHTML = remember_ppg_buttons  // important - includes ids type1 and type1


  if(ppgmode_changed)  // Global
  {  
      // alert(' write_ppgmode_buttons: ppgmode_changed: '+ ppgmode_changed)
            write_ppgmode_box()
            build_ppgmode_buttons();
            document.getElementById('ppgmodemsg').innerHTML='ppgmode has changed'
            document.getElementById('ppgmodemsg').style.backgroundColor="silver";
            document.form1.click_ppgmode.value=0;

            update_psm_info(); // PSM may have changed when we changed modes

            // ppgmode has changed
            if( document.getElementById('sr').innerHTML =="")
		{
		    //alert(' remember_sample_ref_table='+remember_sample_ref_table);
		    document.getElementById('sr').innerHTML= remember_sample_ref_table
		}
	    write_info_line1(); 
	    write_info_line2();  //Info line 2

            if(ppgtype_changed)
            {
               if(ppgtype == 2)
               {
                   nhis=read_histo_titles();
                   get_histo_data(nhis);
               }
               else
               { // temp while we have no type1 titles
                  get_histo_data(0);
               }
               write_cycle_scalers(ppgtype);
               write_frontend_event_stats();
               write_cb_stats();
            }
            build_help_links();

  }
  else if(document.form1.click_ppgmode.value > 0)
  {
       document.form1.click_ppgmode.value++;
       if( document.form1.click_ppgmode.value > 3)
       {
          // clicked_pmb set then cleared by perlscript - can be used for debugging
            
             if(gbl_debug)
             {
                 var clicked_pmb = get_bool(CustomData.hidden["clicked ppgmode button"]);
                 document.getElementById('gdebug').innerHTML+='<br> ...  timeout, clicked ppgmode button= '+clicked_pmb      
             }
           document.getElementById('ppgmodemsg').innerHTML= 'Perlscript may have failed, please refresh page '
           document.getElementById('ppgmodemsg').style.backgroundColor="red"
       }
       else
          document.getElementById('ppgmodemsg').innerHTML= 'Waiting for perlscript to change ppgmode '
	   // document.getElementById('ppgmodemsg').innerHTML= 'ppgmode not yet changed ... waiting for perlscript '+document.form1.click_ppgmode.value 
  }
  else
      build_ppgmode_buttons();  // XXXX


   
    if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br> and ending write_ppgmode_buttons'
}
	
 
function build_ppgmode_buttons()
{
   // called when run is stopped

   // these are now global - moved to bnqr_params.js (different for bnmr/bnqr)
   // var mode1Names=new Array("Scalers","1a","1b","Camp","Freq","NaCell","LCR","Fast");
   // var mode2Names=new Array("SLR","2a","2b","2c","2d","2e","2f");
   var idx;
   var spacer;
   var text="";
   var temp;

   if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br><b>build_ppgmode_buttons:</b> starting with  ppgtype='+ppgtype+' ppg_mode='+ppg_mode
   idx=99; // not selected
   if(ppgtype == 1)
   {
       if(pattern_x.test(ppg_mode))
       sw = RegExp.$1;
          
       //  alert('ppgtype1 sw='+sw)
           
      
       switch(sw)
       {
          case '0':
             idx=0;
             break;
          case 'a':
              idx=1;
              break;
          case 'b':
              idx=2;
              break;
          case 'c':             
              idx=3;
              break;
          case 'e':             
              idx=4;
              break;
          case 'f':
              idx=5;
              break;
          case 'n':
              idx=6;
              break;
          case 'h':
              idx=7;
              break;
          
          case 'j':    // combined mode
              idx=num_type1_modes;  // currently 8
              break;
          case 'g':    // combined mode
              idx=num_type1_modes+1; // currently 9;
              break;
          default :
      } // switch
 
   } // ppg type 1 selected
    
   text="";
         
   for(i=0; i<num_type1_modes; i++)
   {
      if(i==idx)
      {
	  // Click button to see run parameters  ("4" is a dummy for click_ppg_button)
         text+='<input name="customscript" class="input1" onClick="click_ppg_button(4,0); document.location.href=\'/CS/Parameters&\'"  value="'+mode1Names[i]+'" title="Show Run Parameters custom page" type="button" >'
	     // text+= '<span '+selected_1+'>'+spacer+mode1Names[i]+spacer+'</span>' 
      }
      else
         text+='<input name="customscript"   onClick="click_ppg_button(1,'+i+')"  value="'+mode1Names[i]+'" type="button" title="'+mode1Info[i]+'" >'	
   }
	  
   //  text+= '<br><span style="color:red; font-size:80%">Currently only Mode2h (Alpha2) works with sis3820</span>'

   document.getElementById('type1').innerHTML=text;  // write Type1 buttons
   remember_type1_buttons =  text;
      
		 
          
   // Write Combination buttons  (<div>)
   text="";
   for(i=num_type1_modes; i<mode1Names.length;i++)
   {
      if(i==idx)
      {
         text+='<input name="customscript" class="input3" onClick="click_ppg_button(4,0);document.location.href=\'/CS/Parameters&\'"  value="'+mode1Names[i]+'" title="Show Run Parameters custom page" type="button" >'
      }
      else
         text+='<input name="customscript" value='+mode1Names[i]+' onClick="click_ppg_button(3,'+i+');" type="submit" title="'+mode1Info[i]+'" >'
   }
   document.getElementById('combo').innerHTML=text; 
   remember_combo_buttons =  text;
        
   idx=99; // not selected
   if(ppgtype == 2)
   {
      if(pattern_x.test(ppg_mode))
      sw = RegExp.$1;
     
      switch(sw)
      {                          
         case '0':
            idx=0; text+= '<br><span style="color:red; font-size:80%">Currently only Mode2h works with sis3820 scaler</span>'

            break;
         case 'a':
             idx=1;
             break;
         case 'b':
             idx=2;
             break;
         case 'c':             
             idx=3;
             break;
          case 'd':
             idx=4
             break;
          case 'e':
             idx=5
             break;
          case 'f':
             idx=6
             break;
          case 'h':
             idx=7
	     break;
          default :
      } // switch
  
   } // ppg type 2 selected
  
   text=" ";
   //  alert(' mode2Names.length='+ mode2Names.length+' idx='+idx);
   for(i=0; i < mode2Names.length;i++)
   {
      if(i==idx)
      {
    
      text+='<input name="customscript" class="input2" onClick="click_ppg_button(4,0); document.location.href=\'/CS/Parameters&\'"  value="'+mode2Names[i]+'" title="Show Run Parameters custom page" type="button" >'

      }
      else
	text+='<input name="customscript"  onClick="click_ppg_button(2,'+i+')"  value="'+mode2Names[i]+'" title="'+mode2Info[i]+'" type="button" >'
   }
   // alert('type2 text='+text)
   //    text+= '<br><span style="color:red;font-size:80%">For all other modes, use sis3801 scaler</small></span>'

   document.getElementById('type2').innerHTML=text;  // Write Type2 buttons
   remember_type2_buttons =  text;

   remember_ppg_buttons =  document.getElementById('ppgbtns').innerHTML;
   //  alert('  remember_ppg_buttons='+  remember_ppg_buttons)

 
   if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br> and ending build_ppgmode_buttons'
 }


function select_run_mode()
{
  if(gbl_debug)
     {
       document.getElementById('writeStatus').innerHTML = 'selecting run mode'
       document.getElementById('gdebug').innerHTML+='<br><b> select_run_mode:</b> starting..'
     }

   // Initialize
   document.getElementById('runmode').innerHTML=remember_runmode_buttons;
   document.getElementById('runmodemsg').innerHTML='';
   document.getElementById("runmodemsg").style.backgroundColor="silver";
   if (autoruns_enable) // BOOL
   {
     document.getElementById('test').innerHTML='Selected by'
     document.getElementById('real').innerHTML='Autorun'
     document.getElementById('toggle_btn').innerHTML=''
     if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'
     return;
   }

   if(transition_in_progress)
   {
     document.getElementById('test').innerHTML='Transition'
     document.getElementById('real').innerHTML='in Progress'
     document.getElementById('toggle_btn').innerHTML=''
     if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'
     return;
   }

  if(runnum_changed)
     write_run_number_box();

  if (rstate == state_running)
  {
     if(ppgtype==1)
     {  // type 1
       document.getElementById('test').innerHTML='Toggle not supported'
       document.getElementById('real').innerHTML="";
       document.getElementById('toggle_btn').innerHTML=''
       if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'
     }
     else
     {  // ppgtype 2, running
       write_toggle_buttons()
     }
  } // running
  else
  {  // not running
      document.getElementById('toggle_btn').innerHTML='';  // hide toggle button
     if(runnum_changed)  // test/real button pressed 
     {
       document.getElementById('runmodemsg').innerHTML='Run number has changed'
       document.getElementById('runmodemsg').style.backgroundColor="silver";
       document.form1.click_runmode.value=0;
       write_runmode_buttons(); // restore buttons
    }
    else if(document.form1.click_runmode.value > 0)  // clicked TEST/REAL button
    {
       document.form1.click_runmode.value++; // use as a counter
       if( document.form1.click_runmode.value > 3)
       {
          document.getElementById('runmodemsg').innerHTML= 'Perlscript failed, please refresh page'
          document.getElementById('runmodemsg').style.backgroundColor="red"
       }
       else
          document.getElementById('runmodemsg').innerHTML='Waiting for perlscript to change the run number' 
	   // document.getElementById('runmodemsg').innerHTML= 'Run number not yet changed ... <br>waiting for perlscript; counter='+document.form1.click_runmode.value
    } // end of clicked button
    write_runmode_buttons(); // write buttons
  } // end of not running
  if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'

}

function write_toggle_buttons()
{
 // called from select_run_mode() if RUNNING, for ppgtype 2 only
 if(gbl_debug)
 {
    document.getElementById('gdebug').innerHTML+='<br><b><span style="color:brown">write_toggle_buttons starting...</span>'
    document.getElementById('writeStatus').innerHTML = 'write toggle button (ppg type2, running)'
 }
  // hide test/real buttons
     document.getElementById('real').innerHTML="";
     document.getElementById('test').innerHTML="";

     if( document.getElementById('toggle_btn').innerHTML=="")
	 document.getElementById('toggle_btn').innerHTML= remember_buttons_array[toggle_index];
   
  var toggle_bit = get_bool(EqData.mdarc.toggle);
  if(runnum_changed)  // toggle button pressed  (Type 2 only)
  {  // Run number has changed; data not saved yet
     if(gbl_debug)
	 {
            document.getElementById('gdebug').innerHTML+='<br>run mode has changed; run number='+run_number+'  mdarc toggle flag='+toggle_bit+' s(should be set)'
         }
     document.form1.click_toggle.value=0; // clear
     document.form1.runnum.value = run_number; // set to current run number

     document.getElementById('toggle_button').disabled=true;   // disable toggle button
     if(run_number < 40000)  // match the run number colour
        document.getElementById('toggle_button').className="input4"; // yellow
     else
        document.getElementById('toggle_button').className="input5"; // lime
     document.getElementById('toggle_button').title="Toggle in progress..run number has changed"
     
     if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br><span style="color:brown">and ending</span>'
     return;
  }

 
  if(document.form1.click_toggle.value > 0)  // clicked TOGGLE button
  { // waiting for toggle to happen
       if(toggle_bit)  // perlscript has set toggle bit
       {
	   document.getElementById('toggle_button').style.backgroundColor="f5eff5" // pale thistle
           my_toggle_flag=1; // indicates toggle_bit is set by perlscript
          // toggle is finished when data is saved
          if(gbl_debug) 
	  {
              document.getElementById('gdebug').innerHTML+='<br>Toggle in progress, toggle bit set. Counter= '+document.form1.click_toggle.value 
              document.getElementById('tglmsg').innerHTML="Toggle in progress; waiting for mdarc to toggle run number"
              document.getElementById('toggle_button').title="Toggle bit is set; waiting for mdarc to toggle run number"
	  }
          document.form1.click_toggle.value++; // use as a counter  

          if( document.form1.click_toggle.value > 3 &&  gbl_debug)
	  {
                document.getElementById('toggle_button').style.backgroundColor="ffccff" // pale pink
                document.getElementById('toggle_button').title="Toggle bit set; mdarc has not toggled run number"
                document.getElementById('tglmsg').innerHTML="mdarc has not toggled run number. Check mdarc is running"
              document.getElementById('gdebug').innerHTML+='<br>Toggle bit set, still waiting for mdarc to toggle; counter='+document.form1.click_toggle.value
	  }
       }
       else
       { // toggle bit is not set
          if(my_toggle_flag) // toggle has happened, and now is off
	  {
           if(gbl_debug) 
              document.getElementById('gdebug').innerHTML+='<br>Run number should have changed. Try Refresh screen'
            document.getElementById('tglmsg').innerHTML="Run number should have changed. Try Refresh screen"
	    document.getElementById('toggle_button').title="Run number should have changed.  Try Refresh screen"
            document.getElementById('toggle_button').style.backgroundColor="pink"
	  }
          else
          {
	     my_toggle_counter++; // use as a counter
      
             document.getElementById('toggle_button').style.backgroundColor="fuchsia"
             document.getElementById('toggle_button').title="Waiting for Toggle perlscript to set toggle bit"
             if(gbl_debug) 
              document.getElementById('gdebug').innerHTML+='<br>Waiting for Toggle perlscript to set toggle bit'
             if(my_toggle_counter > 3)
             {
              if(gbl_debug) 
                 document.getElementById('gdebug').innerHTML+='<br>Perlscript may have failed. Please refresh screen; cntr='+document.form1.click_toggle.value

                document.getElementById('runmodemsg').innerHTML="Perlscript may have failed or not run. Check Messages"
                document.getElementById('toggle_button').style.backgroundColor="red"  ; // red
                document.getElementById('toggle_button').title="Toggle perlscript has not set toggle bit"
             }
          }
       }// end of else toggle bit not set

    } // end of  toggle clicked
    else  // TOGGLE button has not been clicked or after full page reload
    {      // show toggle button 

       if(toggle_bit)
       {
	   // alert('save interval is '+parseInt(EqData.mdarc["save_interval(sec)"]) )
           if(gbl_debug)
	   {
              document.getElementById('gdebug').innerHTML+='<br>Toggle pending - waiting for mdarc to save data and clear toggle flag. This may take up to '+parseInt(EqData.mdarc["save_interval(sec)"])+' s'
 	      document.getElementById('tglmsg').innerHTML='<span style="font-size:80%">Toggle pending - waiting for mdarc to save data<br>and clear toggle flag. This may take up to '+parseInt(EqData.mdarc["save_interval(sec)"])+' s</span>'
            }

	    document.getElementById('toggle_button').style.backgroundColor="#f0f0f0"    // silver
            
            document.getElementById('toggle_button').title='Waiting for mdarc to save data & clear toggle flag. Takes up to '+parseInt(EqData.mdarc["save_interval(sec)"])+' s' 
	   
  
               
       }
       else
       {
           // re-enable toggle button
	 
           document.getElementById('toggle_btn').innerHTML= remember_buttons_array[toggle_index]; // resets the title
           document.getElementById('toggle_button').disabled=false;
           document.getElementById('tglmsg').innerHTML="";
       }
    }
   
    if(gbl_debug) document.getElementById('gdebug').innerHTML+='<br><span style="color:brown">and ending</span>'
    return;
}



function write_runmode_buttons()
{
   // called from  select_run_mode() if NOT running

 
   if(gbl_debug)
      document.getElementById('writeStatus').innerHTML = '<br><b>write_runmode_buttons</b> starting'

	   
  if (run_number < 40000)
      {   // Test mode : test button is disabled, has no id. Real button is enabled, has id=RunmodeBtn  
  document.getElementById('test').innerHTML='<input class="input4"  name="customscript" value="TEST" disabled onClick="click_run_mode()" type="button" >'

    document.getElementById('real').innerHTML='<input id=RunmodeBtn name="customscript" value="REAL"  onClick="click_run_mode()" type="button" >'
  }
  else
  {  // Real mode : real button is disabled, has no id. Test button is enabled, has id=RunmodeBtn  
    document.getElementById('test').innerHTML='<input id=RunmodeBtn class="input4" name="customscript" value="TEST"  onClick="click_run_mode()" type="button" >'

 document.getElementById('real').innerHTML='<input class="input5"  name="customscript" value="REAL" disabled onClick="click_run_mode()" type="button" >'
  }
 if(gbl_debug)
  document.getElementById('writeStatus').innerHTML = ' .. and ending'
}  
 

function build_statistics_links()
{
   document.getElementById('alias1').innerHTML='<a href="/SC/Cycle_Scalers?exp='+my_expt+'">Cycle Scalers</a>';
   document.getElementById('alias2').innerHTML='<a href="/SC/Epics?exp='+my_expt+'">Epics</a>  ';
}

function build_param_links()
{

   var text;
   text ='<a href="/CS/Parameters&?exp='+my_expt+'"> PPG/PSM Parameters </a>&nbsp;';
   text+='<a href="/Alias/Thresholds&amp;?exp='+my_expt+'"> Thresholds </a>&nbsp;';
   text+='<a href="/Alias/Epics_logged&amp;/?exp='+my_expt+'">  Epics_logging </a>&nbsp;'
   text+='<a href="/Alias/Camp_logged&amp;/?exp='+my_expt+'"> Camp_logging </a>&nbsp;'
   document.getElementById('alias3').innerHTML=text;


   text=  '<a href="/Alias/Auto%20Headers&amp;?exp='+my_expt+'"> AutoHeaders </a>&nbsp;'; 
   text+= '<a href="/Alias/Run_info&amp;?exp='+my_expt+'"> Edit_on_start </a>&nbsp;';
   text+= '<a href="/Alias/User_info_odb&amp;?exp='+my_expt+'"> FE_Info </a>&nbsp;';
   text+= '<a href="/Alias/User_hardware_dir?exp='+my_expt+'"> Hardware </a>&nbsp;';
   text+= '<a href="/Alias/Expert&amp;?exp='+my_expt+'"> Expert </a>&nbsp;';
   text+= '<a href="/Alias/debug"> Debug </a>&nbsp;';
   document.getElementById('alias4').innerHTML=text;

}

function build_help_links()
{
   // HELP on PARAMS  e.g.

   var hr,text;

   // timing diagram - for both bnmr/bnqr
   //  type 10,1c same as 1f
   //  type 1d,1e same as 1n
   //  type 1g FAST and 1j LCR -> combination of 20,1f
   //  type 2d same as 1b
   var diag = new Array();
   diag['10'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1f_timing.ps'+'"'
   diag['1c'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1f_timing.ps'+'"'
   diag['1f'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1f_timing.ps'+'"'
   diag['1n'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1n_timing.ps'+'"'
   diag['1d'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1n_timing.ps'+'"'
   diag['1e'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1n_timing.ps'+'"'
   diag['1g'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1f_timing.ps'+'"'
   diag['1a'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1a_timing.ps'+'"'
   diag['1b'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1b_timing.ps'+'"'
   diag['20'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/20_timing.ps'+'"'
   diag['2a'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/2a_timing.ps'+'"'
   diag['2b'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/2b_timing.ps'+'"'
   diag['2c'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/2c_timing.ps'+'"'
   diag['2d'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1b_timing.ps'+'"'
   diag['2e'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/2e_timing.ps'+'"'
       //diag['2g'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/20_timing.ps'+'"'

   if(bnmr_expt)
      diag['2f'] = '"'+'http://isdaq01:8081/CS/2f_params'+'"'
   else
      { // bnqr
	  // diag['1h'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/1f_timing.ps'+'"'
        diag['2h'] = '"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/20_timing.ps'+'"'
        diag['2f'] = '"'+'http://isdaq01:8082/CS/2f_params'+'"'
      }
   // Parameter help
   hr='"'+'http://dasdevpc.triumf.ca/online/bnmr/hardware/ppg/'+ppg_mode+'_params.help.html#'+ppg_mode+'"'
   text ='<a href='+hr+' target=\"_blank\">PPG'+ppg_mode+'</a>&nbsp&nbsp&nbsp'

   // Timing Diagram
   hr = diag[ppg_mode]
   text += '<a href='+hr+' target=\"_blank\">Timing Diag</a>&nbsp'
  
   document.getElementById('help1').innerHTML=text;
   
}


function build_alarms()
{
 if(gbl_debug)
     {
      document.getElementById('writeStatus').innerHTML = 'building alarms'
      document.getElementById('gdebug').innerHTML+='<br><b>build alarms:</b> starting..'
     }

   if(!alarm_system_active )
   {  // alarm system is NOT active
      document.getElementById("alarum").innerHTML ="";
      if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending (alarm system disabled)'
      return;
   }
  
   var alarm_class; 
   var message;
   var ctrig,rn,mdarc,mheader,fe,rf_config,epics,logger,ar;
   var temp;
   var text="";
   // 
   // Add any new Alarm Classes here
   var b = new Array() ;
   b['Alarm'] =     AlarmData.classes.alarm["display bgcolor"];
   b['Warning'] =   AlarmData.classes.warning["display bgcolor"]
   b['Caution'] =   AlarmData.classes.caution["display bgcolor"]
   b['Threshold'] =  AlarmData.classes.threshold["display bgcolor"]
   b['auto elog'] = AlarmData.classes["auto elog"]["display bgcolor"]
   b['mheader'] =  AlarmData.classes.mheader["display bgcolor"]
   // autorun alarm is for MIDAS standard Status page only and is not shown on custom page

   var f = new Array() ;
   f['Alarm'] =     AlarmData.classes.alarm["display fgcolor"] ;
   f['Warning'] =   AlarmData.classes.warning["display fgcolor"] ;
   f['Caution'] =   AlarmData.classes.caution["display fgcolor"] ;
   f['Threshold'] = AlarmData.classes.threshold["display fgcolor"] ;
   f['auto elog'] = AlarmData.classes["auto elog"]["display fgcolor"] ;
   f['mheader'] =   AlarmData.classes.mheader["display fgcolor"] ;

   var id;
 
   //document.getElementById('getrecord2_target').innerHTML = parseInt( AlarmData.classes.threshold["display fgcolor"] )

   // ctrig  from get_alarm_data 
   rn =    parseInt(AlarmData.alarms["run number"].triggered) // if > 0 fatal error
  
   id=0;
   // temp = document.getElementById("alarum")
   //  document.getElementById("alarum").innerHTML=text;

   if(rn > 0) // Run Number Alarm is Triggered
   {   //  client alarms:  Alarm_Class  Alarm

      alarm_class =  AlarmData.alarms["run number"]["alarm class"]
      alarm_names[id]="run number";
      text+='<tr id=alarm'+id+'><td class="alarm" background-color:'+ b[alarm_class]+' color:'+ f[alarm_class]+' id="alrm1">'
      text+=  AlarmData.alarms["run number"]["alarm message"] 
      text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+')"></td></tr>'
      id++;
     
   }

   var pretrig = parseInt(AlarmData.alarms.prestart_alarm.triggered)
   if(pretrig > 0  &&  rstate == state_stopped  &&  transition_in_progress == 0)
   {
      alarm_class =  AlarmData.alarms.prestart_alarm["alarm class"]
      alarm_names[id]="prestart_alarm";
      text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
      text+=  AlarmData.alarms.prestart_alarm["alarm message"] 
      text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+'); clear_client_flags();"></td></tr>'
      id++;
    }
    
   var start_abort = parseInt(AlarmData.alarms.start_abort.triggered)
   if(start_abort > 0  &&  rstate == state_stopped)
   {
      alarm_class =  AlarmData.alarms.start_abort["alarm class"]
      alarm_names[id]="start_abort";
      text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
      text+=  AlarmData.alarms.start_abort["alarm message"] 
      text+=  'Click  <input name="cmd" value="Messages" type="submit"> for details...    ';
      text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+'); clear_client_flags();"></td></tr>'
      id++;
      my_colour="pink"
      document.getElementById('status').innerHTML='Click <input name="cmd" value="Messages" type="submit"> for details'
      document.getElementById('status').style.backgroundColor=my_colour;
    }


   // client alarms: alarm_class Caution
   mdarc =      parseInt( AlarmData.alarms.mdarc.triggered); 
   mheader =    parseInt( AlarmData.alarms.mheader.triggered);
   if(bnmr_expt)
      fe =         parseInt( AlarmData.alarms.febnmr_vmic.triggered);
   else
      fe =         parseInt( AlarmData.alarms.febnqr_vmic.triggered);
   rf_config =  parseInt(AlarmData.alarms.rf_config.triggered);
   epics =      parseInt(AlarmData.alarms.feepics.triggered);
   logger =     parseInt(AlarmData.alarms.logger.triggered);
   ar =         parseInt(AlarmData.alarms.autorun.triggered);
   // alert('mdarc='+mdarc+' mheader='+mheader+' fe='+fe+' rf_config='+rf_config+' epics='+epics+' logger='+logger+' ar='+ar);
 
   if (mdarc > 0 || mheader > 0 || fe > 0 || rf_config > 0 || epics > 0 || logger > 0 || ar > 0)
   {
   
     alarm_clients.length = 0; // clear the array
     if(mdarc     > 0) alarm_clients.push("mdarc");
     if(mheader   > 0) alarm_clients.push("mheader");
     if(fe        > 0) alarm_clients.push(fe_name+"_VMIC");
     if(rf_config > 0) alarm_clients.push("rf_config");
     if(epics     > 0) alarm_clients.push("feEpics");
     if(logger    > 0) alarm_clients.push("mlogger")
     if(ar    > 0) alarm_clients.push("autorun")
   
    // alert('alarm_clients= '+alarm_clients);
     alarm_class =  AlarmData.alarms.mdarc["alarm class"]
     text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
     text+=  'Required Program(s) '+ alarm_clients  +'  not running! '
     text+=  '<input type="button" value="clear alarm(s)" onclick="clear_alarms('+id+')"></td></tr>'
     id++;
     my_colour="pink"
     temp ='Run <big><b>start-all</b></big> in an xterm'
     temp += ', or click <input name="cmd" value="Programs" type="submit"> and restart missing client(s)'
     document.getElementById('status').innerHTML=temp;
     document.getElementById('status').style.backgroundColor=my_colour;
   }

   // alarm_class : threshold
   var thr = parseInt( AlarmData.alarms.thresholds.triggered);
  
   if(thr > 0)
   {
      alarm_class =   AlarmData.alarms.thresholds["alarm class"];
      alarm_names[id]="thresholds";
      text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
      text+=   AlarmData.alarms.thresholds["alarm message"]
      text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+')"></td></tr>'
      id++;
   }

  //  alarm_class :  auto elog
  var elog =   parseInt(AlarmData.alarms.elog.triggered)
  var epicslog =    parseInt(AlarmData.alarms.epicslog.triggered)

  if(elog > 0 ) // Alarm_Class: auto elog
  {
     alarm_class =   AlarmData.alarms.elog["alarm class"]    
     alarm_names[id]="elog";
     text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
     text+=   AlarmData.alarms.elog["alarm message"] 
     text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+')"></td></tr>'
     id++;
  }
  if(epicslog > 0 ) // Alarm_Class: epicslog
  {
     alarm_class =   AlarmData.alarms.epicslog["alarm class"]
     alarm_names[id]="epicslog";
     text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
     text+=   AlarmData.alarms.epicslog["alarm message"] 
     text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+')"></td></tr>'
     id++;
  }

  if(bnmr_expt)
  {
     // alarm_class : alarm RF TRIP...  BNMR only
     var rftrip =    parseInt(AlarmData.alarms["rf trip"].triggered)
     if( rftrip > 0)  // Alarm_Class: Alarm
     {
        alarm_class =   AlarmData.alarms["rf trip"]["alarm class"]
        alarm_names[id]="rf trip";
        text+='<tr  id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
        text+=   AlarmData.alarms["rf trip"]["alarm message"]
        text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+')"></td></tr>'
        id++;
      }
   } // bnmr_expt

   // alarm_class : alarm  EPICS ACCESS
   var access =   parseInt(AlarmData.alarms["epics access"].triggered);
   if(access > 0) // Alarm_Class: Warning
   {
      alarm_class =   AlarmData.alarms["epics access"]["alarm class"]
      alarm_names[id]="epics access";
      text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
      text+=   AlarmData.alarms["epics access"]["alarm message"]
      text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+')"></td></tr>'
      id++;
   
   }

   // alarm_class : alarm  MHEADER (ON HOLD)
   var mh_on_hold =    parseInt(AlarmData.alarms.mh_on_hold.triggered) // type 1 waiting for camp/epics

 


   if(mh_on_hold > 0)
   {
      if( (rstate == state_running) ||  (transition_in_progress > 0 )) // Alarm_Class: Mheader
      {
         alarm_class =   AlarmData.alarms.mh_on_hold["alarm class"]
         alarm_names[id]="mh_on_hold";
         text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
         text+=   AlarmData.alarms.mh_on_hold["alarm message"]
         text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+')"></td></tr>'
         id++;


         var tim = parseInt(get_elapsed_time("seconds"));
         
         // Don't show status msg if user has taken run off hold, or if on hold and elapsed time from run start is > 20s
         if(on_hold_flag && tim < 20) 
         {
          //  mh_on_hold=0; // not on hold
            my_colour="pink"
            document.getElementById('status').innerHTML='If run does not resume shortly, try <input name="cmd" value="Messages" type="submit">, click ResetCampLog,ResetCampLog, check camp/epics and/or restart the run '
            document.getElementById('status').style.backgroundColor=my_colour;
         }
      }
   }

  // helicity mismatch
//  var helcheck =    parseInt(AlarmData.alarms.hel_mismatch.triggered) 
  var helcheck =    get_bool(EqData["client flags"].hel_mismatch,"EqData.client_flags.hel_mismatch") 
  if(helcheck > 0) // Alarm_Class: Alarms
   {
       //my_path="/Equipment/fifo_acq/client flags/hel_mismatch"
      alarm_class =   AlarmData.alarms.hel_mismatch["alarm class"]
      alarm_names[id]="hel_mismatch";
      text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
      text+=   AlarmData.alarms.hel_mismatch["alarm message"]
      text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+');async_odbset([\"/Equipment/fifo_acq/client flags/hel_mismatch\"],[\"0\"]);"></td></tr>'
      id++;
   }

  // sample/ref warning

  if(gbl_sr_mode)
  {
    var sr_warn =    get_bool(EqData.frontend.hardware["daq drives sampleref"],"EqData.frontend.hardware.daq_drives_sampleref") 
    if(sr_warn == 0) // Alarm_Class: Warning
    {
       alarm_class =   AlarmData.alarms.sampleref["alarm class"]
       alarm_names[id]="sampleref";
       text+='<tr id=alarm'+id+'><td class="alarm"  style="background-color:'+ b[alarm_class]+'; color:'+ f[alarm_class]+'">'
       text+=   AlarmData.alarms.sampleref["alarm message"]
      // text+=  ' <input type="button" value="clear alarm" onclick="clear_alarm('+id+');async_odbset([\"/Equipment/fifo_acq/client flags/hel_mismatch\"],[\"0\"]);"></td></tr>'
       id++;
    }
  }



  document.getElementById("alarum").innerHTML =text;

  if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending '
  return;
}


function build_autorun_params()
{
// AUTORUN TABLE
  var my_class, my_colour;
  var text;
  var colour = new Array("pink","peachpuff","tan","yellow","magenta","red","aliceblue","cyan","lime","thistle", "silver");
  var states = new Array("disabled","idle","acquiring","paused","ending","stopped","setting","changing","starting","reload","undefined")
  
  //alert(' build_autorun_params: show_autorun_params='+show_autorun_params)
 if(gbl_debug)
     {
      document.getElementById('writeStatus').innerHTML = 'building autorun parameters'
      document.getElementById('gdebug').innerHTML+='<br><b>build_autorun_params:</b> starting..'
     }
   show_autorun_params = parseInt(show_autorun_params)
 document.form1.abox0.checked= show_autorun_params;  // initialize to the correct value
 
 

  if(show_autorun_params==1)
  {
      

      if( document.getElementById('autorun').innerHTML=="")
         document.getElementById('autorun').innerHTML=remember_autorun_params; 

 
   
      if( document.getElementById('aupa').innerHTML=="")
         document.getElementById('aupa').innerHTML="Autorun Parameters:";
   
    if (autoruns_enable)  // BOOL
       my_colour="green";
    else
       my_colour="pink"

    
  
   // // text= ' <input  name="abox2"  type="checkbox"  onClick="enable_autorun(this.checked?\'1\':\'0\')">'
 // // document.getElementById('auen').innerHTML=text;
    document.getElementById('auen').style.backgroundColor=my_colour;
    document.form1.abox2.checked= get_bool(AutoData.enable);  // initialize to the correct value

    var state_select = parseInt(AutoData.state);
    if(state_select > states.length)
       state_select =  states.length;

    document.getElementById('aust').style.backgroundColor= colour[state_select];
    document.getElementById('aust').innerHTML=states[state_select];

    var ar_plan = AutoData["plan file"];

    text= '<a href="#" onclick="ODBEdit(autorun_plan_path)" >'+ ar_plan +'</a>'
    document.getElementById('aupl').innerHTML=text;

    document.getElementById('aumsg').innerHTML= AutoData.message
    
 
 
// text= ' <input  name="abox3"  type="checkbox"  onClick="set_autorun_pause( this.checked?\'1\':\'0\');">'
   
  // text+= unbool( get_bool(AutoData["enable pausing"],autorun_pause_path))  // json returns this as "true" or "false"
 //   text+= '</a>';
  // document.getElementById('aupau').innerHTML= text; // now in <body> as a checkbox
      document.form1.abox3.checked= get_bool(AutoData["enable pausing"]);  // initialize to the correct value

  

    text= '<a href="#" onclick="ODBEdit(autorun_refresh_path)" >'
    text+=  AutoData["refresh seconds"]
    text+= '</a>';
    document.getElementById('aurf').innerHTML= text;
 

 
    if(ppgtype == 1)
    {
       // target scans
       document.getElementById('autg').innerHTML= 'Target Scans:';
  
       var num_scans = ExperimData["edit on start"]["number of scans"];

       if(num_scans==undefined)
          text+= '?'; // could not find key
       else
       {
          if(autoruns_enable)
             text= '<a href="#" onclick="ODBEdit(num_scans_path)" >'+ num_scans + '</a>';
          else
             text= num_scans  // users cannot change this unless autoruns enabled
       } 
       document.getElementById('aunum').innerHTML= text;
      
       
       document.getElementById('auchi').innerHTML="";
       document.getElementById('auchi').className="white";
       document.getElementById('auhi').innerHTML="";
       document.getElementById('autc').innerHTML="";
       document.getElementById('autc').className="white";
       document.getElementById('aucnt').innerHTML="";
    }

    else 
    { // Type 2

       // target cycles  
       document.getElementById('autg').innerHTML= 'Target Cycles:';
    
       var num_cycles =  parseInt(ExperimData["edit on start"]["number of cycles"])
       

       if(num_cycles ==undefined)
          text= '?'; // could not find key
       else
       {
          if(autoruns_enable)
             text= '<a href="#" onclick="ODBEdit(num_cycles_path)" >'+ num_cycles +'</a>';
          else
             text= num_cycles;
       }
       document.getElementById('aunum').innerHTML= text;   
      

       // target counts
   
       text = '<a href="#" onclick="ODBEdit( autorun_target_counts_path)" >'
       text+=  AutoData["target counts"]
       text+= '</a>';
       document.getElementById('aucnt').innerHTML= text;   
       document.getElementById('autc').innerHTML= "Target Counts:"  

       // count histogram

       text=  '<a href="#" onclick="ODBEdit(autorun_countHis_path)" >'
       text+=  AutoData["count histogram"]
       text+= '</a>';
       document.getElementById('auhi').innerHTML= text;   
       document.getElementById('auchi').innerHTML="Count Histogram:"   
     } // type 2



    

     text=  '<a href="#" onclick="ODBEdit(autorun_time_path)" >'
     text+=  AutoData["time limit (minutes)"]
     text+= '</a>';
     document.getElementById('autl').innerHTML=text;
     

     
     
     remember_autorun_params =  document.getElementById('autorun').innerHTML; 

     // alert('2  remember_autorun_params='+ remember_autorun_params);
     if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'

   }  // end of show autorun params
   else
   {
        // alert('build_autorun_params: show_autorun_params= '+show_autorun_params);
         //document.getElementById('mytest').innerHTML='mytest... build_autorun_params: clearing autorun'
    

      document.getElementById('autorun').innerHTML="";
    //  document.form1.abox0.checked=  show_autorun_params;  // initialize to the correct value
      document.getElementById('aupa').innerHTML="";
   }
   if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending'
}


function set_autorun_defaults()
{  // set number of cycles/scans to zero if autoruns are turned off

   if ( !autoruns_enable)
   {
    // alert('autoruns are turned OFF')
   //  ODBSet(num_scans_path, 0);
   //  ODBSet(num_cycles_path, 0);
   if(ppgtype==1) // ppgtype cannot be changed while autoruns are on
       async_odbset(["/Experiment/edit on start/Number of scans"],[0]); // clear
    else
       async_odbset(["/Experiment/edit on start/Number of cycles"],[0]); // clear

     document.getElementById('auen').style.backgroundColor="pink";
     document.getElementById('aunum').innerHTML= '0';
   }
}





// ======================================================
function htmlize(text)
{
   // Escape the angle brackets for display in HTML
   text = text.replace(/\</gm, '&lt;');
   text = text.replace(/\>/gm, '&gt;');
   return text;
}

function get_bool(jval,path)
{
  var ival=0;

 if(pattern_no_key.test(jval))
 {
     alert('get_bool: no key for path '+path+'  jval= '+jval);
     ival=-1
 }
  else
  {
    if( (pattern_y.test(jval)) ||  (pattern_true.test(jval)) || (jval == 1))
      ival=1;
    else
      ival=0;


   //  alert('get_bool:  jval= '+jval+ ' returning ival= '+ival)
  }
  return ival;
}

function unbool(jval)
{
    if(jval)
	return ("y");
    else
	return ("n");
}

function roundup(fval, dec_places)
{
   // fval will be converted to a string
   // rounds up to dec_places places of decimal

   var point_pattern = /./;
   var exp="";
   var a,c,dec,e,f,g,int
   var rounded;
   var zero;
   var factor;

  if(fval == undefined)
      return undefined;
  
   s=new String();
   s=fval.toString(); // make sure fval is a string
   
   if(typeof(dec_places) != 'undefined' && dec_places != null && dec_places === parseInt(dec_places))
     factor = Math.pow(10,dec_places);
   else
     factor = 1000;

   dec = s.indexOf(".") // find if there's a decimal point
   if (dec > 0)
   {
       //  alert('found a decimal point in string '+s);
       zero =  s.indexOf(".0") // find if there's a decimal point followed by a zero
   
      a = s.indexOf("e")  // find exponent
      if (a > 0)
         exp = s.substring(a)
  

     //  alert('string= '+s+'a = '+a+' exp= '+exp)
     c = s.slice(0,a);
     // alert('string= '+s+'c= '+c);
      d=dec;
      dec++;
      int = s.slice(0,dec); // integer portion

      f=dec+5;
      e = s.slice(d,f)
      // look for zeroes e.g. .023 .002
      
      // alert('d= '+d+' e= '+e+' zero = '+zero)
        e=e*factor;  
     // e=e*100;  // 256 -> 25.6 -> 26 ->  .26
               // 023 -> 2.3  -> 2 -> .2 so we must add a zero -> .02 
  
      g = Math.round(e);
     // alert ('string= '+s+ ' e= '+e+' g= '+g+'  int= '+int)

      if(zero > 0 )
          rounded = int + "0"+ g + exp  // add a zero as a spacer
      else
         rounded = int + g +exp

      // alert ('s= '+s+ '   rounded= '+rounded)   
   }
   else
   {
     // alert ('no change in string '+s)
      rounded=s;
   }
   return rounded;
}


//function to check if a bit is set 

function bitCheck(bitpat,bit)
{
 var shift;
 shift=1<<bit;
   if (bitpat & shift)
     return 1;
  else
     return 0;
}
// ======================================================



function init_event_stat_titles()
{
   
   var text1="";
   var text2;
   var max_stats_1 =eq_titles.length;  // equipment titles length (Type 1)
   var max_stats_2=3;  //  equipment titles length (Type 2)

   for (j=0; j <max_stats_2 ; j++)
   {
      text1+='<td class="festatt">'+eq_titles[j]+'</td>' 
   }
   text2 =text1; 


   for (j=max_stats_2; j <max_stats_1 ; j++)
   {
      text1+='<td class="festatt">'+eq_titles[j]+'</td>'
    //**  text2+='<td class="festattout">'+eq_titles[j]+'</td>'
   }
   remember_eqtitles_type2 =text2;
   remember_eqtitles_type1 =text1;
  // alert('text2= '+text2 +' and text1= '+text1)  

   if(ppgtype==1)
      document.getElementById('eqtitles').innerHTML=remember_eqtitles_type1;
   else
      document.getElementById('eqtitles').innerHTML=remember_eqtitles_type2;

// alert('text2= '+text2 +' and text1= '+text1)  
   return;
}


function setup_customstatus()
 {
    var my_action, my_alias_name, redir_flag;
 
     if(status_key != 1)
    {
   
 
       // did not find key   /custom/status... therefore this page is using alias link from MIDAS Status page
	// alert('using alias link for customstatus')
      my_alias_name="CustomStatus&";
      my_action = 'CustomStatus&';
      document.getElementById('CS').innerHTML ="Using alias link '"+my_alias_name+"' from Midas Main Status page"
      redir_flag = 1; // need to redirect so custom buttons return to same custom page
   }
    else


       //var my_custom_status = CustomData["status"];

 
       //if(my_custom_status)
    {    // this page is replacing the MIDAS status page
       redir_flag = 0; // no redirect
       my_action="Status";
       my_alias_name="../";
       document.getElementById("CS").innerHTML =''
       //alert('Replacing Status page; my_action='+my_action+' and my_alias_name='+my_alias_name);
    }
    
   
   
    document.getElementById("form1").action="Status"
    document.getElementById("redir").value=my_alias_name

    return;
 }

function run_rf_config(val)
{
    // alert(' value='+document.form1.rfc.value);
       ODBcs(document.form1.rfc.value) // custom_script
	   document.getElementById('click').innerHTML='Click Messages button to see result';
}

function click_ppg_button(type, i)
{
    // timer2_cntr=0;
    var cmd;
    // alert('ppg type='+type+' i='+i )
    document.getElementById('ppgmodemsg').innerHTML='Click! '
 
    if(type==1 || type==3)
	    cmd = mode1Names[i];
  
    else if (type ==2)
            cmd = mode2Names[i];
    else if (type==4)
	{ // dummy - just write message
          document.getElementById('ppgmodemsg').innerHTML='Loading parameters page...'
	  return;
	}

    else
    {
	  document.getElementById('ppgmodemsg').innerHTML+='Error: unknown ppgmode'
          return;
    }
   document.getElementById("ppgmodemsg").style.backgroundColor="orange";
   document.getElementById('ppgbtns').innerHTML="PPG mode is changing!"
   document.getElementById('ppgmodemsg').innerHTML='<b>'+cmd+'</b> button clicked!'
   document.form1.click_ppgmode.value=1;
 
    ODBcs(cmd) // custom_script
	// ODBcs('xxx')  // test failure of perlscript
}

function click_run_mode()
{
    var cmd=document.form1.RunmodeBtn.value  // REAL or TEST
	//   alert('click_run_mode cmd='+cmd)
   document.form1.click_runmode.value=1;
   document.getElementById('runmodemsg').innerHTML='<b>'+cmd+'</b> button clicked!'
   document.getElementById('runmode').innerHTML="Runmode changing!"
   document.getElementById("runmodemsg").style.backgroundColor="orange";
   ODBcs(cmd);
	//  ODBcs('xxx')  // test failure of perlscript
}

function click_toggle_button()
{  
   // only the toggle button (test->real or vice versa) calls this
 
   my_toggle_counter=0; // clear counter
   cmd="Toggle"
   document.form1.click_toggle.value=1;
 document.getElementById('toggle_button').disabled=true;
    document.getElementById('toggle_button').style.backgroundColor="orange";
    // document.getElementById('runmodemsg').innerHTML='<b>'+cmd+'</b> button clicked!'
     document.getElementById('toggle_button').title="Toggle Button clicked!"
   document.getElementById('tglmsg').innerHTML=" "
	 //  document.getElementById("runmodemsg").style.backgroundColor="orange";
   
   ODBcs(cmd);
}


function clear_busy()
{
  // clear busy if it hasn't cleared itself in 20s
  //alert('Clear busy called');
  cs_odbset(eor_rn_check_path,0);  //ODBSet(eor_rn_check_path,0);                                               				    
}


function ToggleStatusPage()
{
    var cmd=document.form1.tsp.value;
    // alert('cmd='+cmd);
    ODBcs(cmd)
    alert("Now refresh page")
}


function update_psm_info()
{  // called from load_ppgmode
    // alert('  update_psm_info')
    if(gbl_debug) document.getElementById('gdebug').innerHTML+='update_psm_info starting.. '  // YYY

   if(psm_enabled)
   {
       document.getElementById('onef').innerHTML = 'One-f';
       if (rf_onef_enabled)                               // Cell 19   Info 1
          document.getElementById('onef').className="dkgreen";  
       else
          document.getElementById('one_f').className="pink";


       document.getElementById('psmamp').innerHTML= 'Amplitude:'  // Cell 18 PSM Amplitude
       document.getElementById('psmamp').className="item";
       if( rf_onef_ampl == 0)
          document.getElementById('a1f').className="pink"; //  RF Amplitude  one-f
       else
          document.getElementById('a1f').className="beige";

       document.getElementById('a1f').innerHTML =rf_onef_ampl;



       if ( (rf_fref_enabled) || (pattern_1f.test(ppg_mode) )) // fREF enabled or ppg_mode is 1f )
       {
         document.getElementById('fref').innerHTML = 'fREF';
         if (rf_fref_enabled)                            // Cell 20   Info 1
           document.getElementById('fref').className="dkgreen";  
         else
           document.getElementById('fref').className="pink";


         //  Cell  RF Amplitude  fREF
      
         if ( !rf_fref_enabled || rf_fref_ampl == 0 )
             document.getElementById('afref').className="pink"
         else
             document.getElementById('afref').className="beige"
         document.getElementById('afref').innerHTML = rf_fref_ampl;

       }
       else
       {
           document.getElementById('fref').innerHTML="";
           document.getElementById('fref').className="item"
           document.getElementById('afref').innerHTML ="";
           document.getElementById('afref').className="pink"
       }
  } // psm_disabled
  else
  {
       document.getElementById('psmamp').innerHTML="Disabled";
       document.getElementById('psmamp').className="pink";
 
       document.getElementById('fref').innerHTML="";
       document.getElementById('fref').className="item"
       document.getElementById('onef').innerHTML="";
       document.getElementById('onef').className="item"
       document.getElementById('afref').innerHTML="";
       document.getElementById('a1f').innerHTML="";
       document.getElementById('afref').className="pink"
       document.getElementById('a1f').className="pink"
   }
   if(gbl_debug) document.getElementById('gdebug').innerHTML+='and ending ' 
   return;
}



function set_debug(i)
{
    
    var values=new Array();
    gbl_debug=get_bool(i);
    values[0]=gbl_debug;
    
    async_odbset(["/custom/hidden/cs debug"], values);  // called with arrays
    if(!gbl_debug)
    {
       document.getElementById('gdebug').innerHTML=""; // clear debug area
       document.getElementById('tdebug').innerHTML=""; // clear title
    }
    else
       document.getElementById('tdebug').innerHTML=remember_tdebug; // write title
}



function cs_odbset(one_path, one_value)
{   // cs_odbset   convert single values to array then call async_odbset

    var Paths=new Array(); var Values=new Array();
    
    Paths[0]=one_path;
    Values[0]=one_value;
    // alert('cs_odbset:  Paths='+Paths[0]+' and Values='+Values[0])
   async_odbset(Paths,Values)
   return;  
}



function async_odbset(paths,values)
{
   // expects parameters are ARRAYS
   var len;
  // alert('async_odbset:length of paths array='+paths.length+' and values.length='+values.length);
   
    progressFlag= progress_write_callback
    if(gbl_debug) 
         document.getElementById('gdebug').innerHTML+='<br>async_odbset: writing  paths '+paths+' and values '+values
	     // alert('async_odbset:  writing  arrays paths '+paths+' and values '+values)
    mjsonrpc_db_paste(paths,values).then(function(rpc) {
        var i;
        if(gbl_debug)
           document.getElementById('readStatus').innerHTML = 'async_odbset  status= '+rpc.result.status
        len=rpc.result.status.length // get status array length
       // alert('async_odbset: length of rpc.result.status='+len)
        for (i=0; i<len; i++)
        {
           if(rpc.result.status[i] != 1) 
                   alert(' async_odbset: status error '+rpc.result.status[i]+' at paths['+i+']="'+paths[i]+'"')
        } 
     if(gbl_debug)
     {
        document.getElementById('writeStatus').innerHTML='async_odbset: writing paths '+paths+' and values '+values
         document.getElementById('gdebug').innerHTML+='<br>async_odbset: wrote paths '+paths+' and values '+values
     }
     progressFlag= progress_write_got_callback
    }).catch(function(error)
          { 
          mjsonrpc_error_alert(error); });
 }


function ODBcs(cmd)
{  // custom script button without page reload
   var value, request, url;
   
   var request = XMLHttpRequestGeneric();
    url = ODBUrlBase
   if(redir_path!=undefined)
       //url += ODBUrlBase + '?redir=CustomStatus&&customscript='+ encodeURIComponent(cmd);
    url +=  '?redir='+redir_path
    url+='&customscript='+ encodeURIComponent(cmd);
    //  alert('url='+url)
   request.open('GET', url, true); // async

   request.send(null);
   //   alert('url='+url+'request.status='+request.status)
 //      if (request.status != 200 || request.responseText != 'OK') 
//        alert('ODBcs error:\nHTTP Status: '+request.status+'\nMessage: '+request.responseText+'\n'+document.location) ;
//   if (request.status != 200)
//       alert('ODBcs Error calling Custom Script "'+cmd+'" :\nHTTP Status: '+request.status)
}




// ================== MAIN ===============================
function main()
{
  //   alert('main...')
    
     
      var my_date= new Date;
      var s=my_date.toString();
      s = s.replace(/ GMT.+/,""); // strip off time zone info

     document.getElementById('prld').innerHTML = "Last full page reload " + s;

     // temp
     //open_pop()
  


      if(gbl_debug)document.getElementById('gdebug').innerHTML="<br>Main..full reload at " + s
      read_data(); // read the data
      progress();
}


function dump_data(xhr_responseText)
{
    // If there is an error e.g.
    //     json.parse bad control character in string literal at line 1881 column 145 of the json data
    //     need to dump the data and find the line
    // 1. in mhttpd.js  look for rpc_response = json.parse(xhr.responseText)
    //       add a call to dump_data
    //     
    //         try {
    //             dump_data(xhr.responseText);    // add this line     
    //           rpc_response = JSON.parse(xhr.responseText);
    //           if (!rpc_response) {
    //              throw "JSON parser returned null";
    //           }
    // 2. Save mhttpd.js and in  Custom_Status.html   comment out  
    //           <!--     <script type="text/javascript" src="mhttpd.js">
    //                   </script>      -->
    //   Reload page and then remove comment signs. This loads a new mhttpd.js
    // 3.  Now every time read_data() is called, it dumps the data into a pop-up window
    // 4. Grab the data and put it into emacs so line number with error can be found
    // 5. Last time it was /autorun/message  so now autorun params are got separately

    // alert ('hi from dump_data')
     open_pop()
     Pop2Win.document.writeln('<br>================================================================<br> ');
     Pop2Win.document.writeln('pop xhr.responseText '+xhr_responseText);
}

function close_pop()
{
  if (typeof Pop2Win !='undefined')
  {
      Pop2Win.close();
  }  
}
function open_pop()
{
    // called by dump_data
    close_pop(); // close window if open

  // open popup window
   Pop2Win = window.open ('','','height=1000,width=1000,scrollbars=yes,menubar=no,toolbar=no',true);
   if(Pop2Win == null)
   {
      alert('Cannot open popup window')
   }
   else
   {
   Pop2Win.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">')
   Pop2Win.document.write('<html><head><title>xhr.responseText</title>');
   Pop2Win.document.write('</head><body>')
       Pop2Win.document.writeln('<pre>')
       }
  return
}

function show_windows()
{
     open_pop1()
}
function close_pop1()
{
  if (typeof Pop1Win !='undefined')
  {
      Pop1Win.close();
  }  
}

function write_click()
{ // display message for users if interface is slow

    document.getElementById('click').innerHTML='Button clicked! Please wait...'
    return;
}

function clear_click_msg()
{   // clear message for users 
    document.getElementById('click').innerHTML='';
}

function call2()
{
    alert('call2: Callback from cs_button') 
}



function cs_button(cmd, callback)
{ // send a request to execute a custom script

    var url;
    var request = XMLHttpRequestGeneric();
    url = ODBUrlBase
    if(document.getElementById("redir") != null)

        url += '?redir='+document.getElementById('redir').value

    url+='&customscript='+ encodeURIComponent(cmd);
    request.open('GET', url, true); // asynchronous request
    request.send(null);
    if(callback!=undefined) {

        request.onreadystatechange = function() {

            if (request.readyState == 4) {

                if (request.status == 200)

                    callback();

                else

                    alert('cs_button: Error after sending request to Custom Script "'+cmd+'" :\nHTTP Status: '+request.status)

            }

        }

    }
    else {

        if (request.status != 200)

            alert('cs_button: Error after sending request to Custom Script "'+cmd+'" :\nHTTP Status: '+request.status)

    }

}
