// Common js functions to  parameters.html (i.e. parameter_functions.js)  and compare_tunes.html

//var perlscript_done_path ="/custom/hidden/perlscript_done";
// var poll_on_perl_path="/custom/hidden/poll on perl";
// var last_selected_path="/custom/hidden/last selected tune";

function poll()
{
    //alert('poll: polling_on_perl='+ polling_on_perl)
    var path=[
       "/Custom/"
     ];
    if(polling_on_perl)
        ODBMCopy(path, poll_callback, "json");

     progressFlag=progress_poll_callback  // poll (waiting for callback)
}

function poll_callback(data)
{
    // alert('poll_callback: polling_on_perl='+ polling_on_perl)
   // perl codes:
     var done_code=2;
     var in_progress=1;
     var error_code=3;
   
     progressFlag=progress_poll_got_callback;  // poll_callback

    var obj= JSON.parse(data);
    CustomData= obj[0];


    //   alert('poll_callback: starting with polling_on_perl  = '+polling_on_perl);
   if(polling_on_perl)  // flag is set
   {
       code =CustomData.hidden.perlscript_done; //  ODBGet(perlscript_done_path);
       if(code==undefined)
	   alert('poll_callback: code is undefined (CustomData.hidden.perlscript_done)')

    if(code ==done_code) 
    {  // done
	document.getElementById("perl").innerHTML="perlscript done!";
        document.getElementById("perl").style.color="orange";
        polling_on_perl=0;
        ODBSet(poll_on_perl_path,polling_on_perl);  // clear flag
	reload();  // either reload page (parameter_functions.js) or reloads frame (compare_tunes.html)
        return;
    }
    else if (code ==error_code) 
    {  // error
	document.getElementById("perl").innerHTML="error from perlscript";
         polling_on_perl=0;
         ODBSet(poll_on_perl_path,polling_on_perl);  // clear flag
         reload(); // show error messages
        return;
    }
    else if (code == 4)
    {  // perlscript has not started 
	document.getElementById("perl").innerHTML="perlscript failed to start (error in perlscript?)";
        document.getElementById("perl").style.color = "red";
        polling_on_perl=0;
        ODBSet(poll_on_perl_path,polling_on_perl);  // clear flag
        clear_frame(); // either calls reload() (parameter_functions.js) or clears frame (compare_tunes.html)
     
	 
        return;
    }
    // perlscript is running
  	document.getElementById("perl").innerHTML="pcounter= "+pcounter
    if(document.form1.pcounter.value > 3)
    {
 
       	document.getElementById("perl").innerHTML="perlscript timed out!";
        polling_on_perl=0;
        ODBSet(poll_on_perl_path,polling_on_perl); // clear flag
        return;
    }
    set_poll_timer(poll_time_s);
   }
    return;
}

function set_load_timer(update_time_s)
{
    //  alert('set_load_timer: setting timerID .. update_time(sec)= '+update_time_s)
 
  if(timerID)
     clearTimeout(timerID);
  timerID =  setTimeout ('load()', (update_time_s * 1000));

  return;
}

function set_poll_timer(time_s)
{
    //alert('set_poll_timer: setting ptimerID .. poll_time(sec)= '+time_s)
  if(ptimerID)
     clearTimeout(ptimerID);
  ptimerID =  setTimeout ('poll()', (time_s * 1000));
  return;
}

function  check_perlscript_done()
{
    // alert('check_perlscript_done')
     var perl_done=CustomData.hidden.perlscript_done; //  ODBGet(perlscript_done_path);
     if( perl_done==undefined)
	   alert(' check_perlscript_done:  perl_done is undefined (CustomData.hidden.perlscript_done)')
  
     if(perl_done == 1)
     {
        alert("Perlscript is busy. Try again later, or set ODB key /Custom/hidden/perlscript_done to 2");
        return 0;
     }
     else
        ODBSet(perlscript_done_path, 4); // perlscript status code 

     return 1;
}

function roundup(fval, dec_places)
{
    // rounds a number up to dec_places places of decimal
 
   // fval will be converted to a float,
   //     multiplied by a factor of 10 to convert to an integer
   //     sent to Math.round, then divided by the factor of 10

  

  if(fval == undefined)
  {
     alert('roundup: value to roundup is undefined')
      return fval;
  }
  
  if(dec_places == undefined)
  {
     alert('roundup: number of decimal places is undefined')
     return fval;
  }
     
   if(dec_places == null) return fval;

   if (dec_places === parseInt(dec_places))
     factor = Math.pow(10,dec_places);
   else
       return fval;
   f=parseFloat(fval)*factor;
 
   g = Math.round(f);
   g = g/factor;
   return g;
}

function odbset_one(my_path,my_value)
{  
  
   var paths=new Array;
   var values=new Array;
   paths[0]=my_path;
   values[0]=my_value;

   //  alert('odbset_one: starting with path= '+my_path+' and value= '+my_value) 
    mjsonrpc_db_paste(paths,values).then(function(rpc) {
   var i,len;
   document.getElementById('readStatus').innerHTML = 'odbset_one:  status= '+rpc.result.status
   len=rpc.result.status.length // get status array length
  
      if(rpc.result.status[0] != 1) 
                   alert('odbset_one: status error '+rpc.result.status[0])
   
   document.getElementById('writeStatus').innerHTML='odbset_one: writing paths '+paths+' and values '+values

   }).catch(function(error)
   { 
          mjsonrpc_error_alert(error); });

 }      

function ODBcs(cmd)
{
   var value, request, url;
   
   var request = XMLHttpRequestGeneric();
    url = ODBUrlBase
   if(redir_path!=undefined)
    url +=  '?redir='+redir_path
 //url += ODBUrlBase + '?redir=CustomStatus&&customscript='+ encodeURIComponent(cmd);
    url+='&customscript='+ encodeURIComponent(cmd);
    //  alert('url='+url)
   request.open('GET', url, true); // async

   request.send(null);
   //  alert('url='+url+'request.status='+request.status)
 //      if (request.status != 200 || request.responseText != 'OK') 
//        alert('ODBcs error:\nHTTP Status: '+request.status+'\nMessage: '+request.responseText+'\n'+document.location) ;
//   if (request.status != 200)
//       alert('ODBcs Error calling Custom Script "'+cmd+'" :\nHTTP Status: '+request.status)
}


 
