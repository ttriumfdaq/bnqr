/*   trPPG.h
     Author:  

     Include file for  PPG (Pulse Programmer) for BNMR

  $Log: trPPG.h,v $
  Revision 1.6  2005/02/04 18:30:56  suz
  add new prototypes & mask for dual channel mode

  Revision 1.5  2005/01/26 00:11:11  suz
  added VME_TRIG_CTL (dual channel mode modification)

  Revision 1.4  2001/08/06 18:52:30  midas
  New functions - beam control register(RP)

  Revision 1.3  2000/11/28 18:18:48  renee
  Change the DEFAULT_PPG_POL_MSK value

  Revision 1.2  2000/11/06 21:16:28  midas
  add comment for cvs


*/
#ifndef _PPG_INCLUDE_
#define _PPG_INCLUDE_

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED

#if defined( _MSC_VER )
#define INLINE __inline
#elif defined(__GNUC__)
#define INLINE __inline__
#else
#define INLINE
#endif

typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
#ifdef __alpha
typedef unsigned int       DWORD;
#else
typedef unsigned long int  DWORD;
#endif

#endif /* MIDAS_TYPE_DEFINED */
#ifdef PPCxxx
#define A16            0xfbff0000
#else
printf(" not supported under this OS\n");
#endif
/* old way #define DEFAULT_PPG_POL_MSK 0xffdf7f */
#define DEFAULT_PPG_POL_MSK 0x000080  /*  (Type 1/Type 2 HelDown*/ 
#define HEL_FLIP_PPG_POL_MSK 0x004080 /* Type 2 Hel Up */
#define PPG_RESET_REG       0x00   /* W     BYTE   W  */
#define PPG_START_TRIGGER   0x01   /* W     BYTE   W  */
#define BYTES_PER_WORD      0x02   /* 8     BYTE  RW  */
#define TOGL_MEM_DEVICE     0x03   /* W     BYTE   W  */
#define CLEAR_ADDR_COUNTER  0x04   /* W     BYTE   W  */
#define LOAD_MEM            0x06   /* 1     BYTE  RW  */
#define PROGRAMMING_FIN     0x07   /* 1     BYTE  RW  */
#define OUTP_POL_MASK_HI    0x08   /* 8     BYTE  RW  */
#define OUTP_POL_MASK_MID   0x09   /* 8     BYTE  RW  */
#define OUTP_POL_MASK_LO    0x0A   /* 8     BYTE  RW  */
#define POLZ_SOURCE_CONTROL 0x0B   /* 2     BYTE  RW  */
#define VME_POLZ_SET        0x0C   /* 1     BYTE  RW  */
#define VME_READ_STAT_REG   0x0D   /* 8     BYTE  RO  */
#define VME_TRIGGER_REG     0x0E   /* 8     BYTE  RW  */
#define VME_RESET           0x0F   /* 8     BYTE  RW  */
#define VME_RESET_COUNTERS  0x10   /* 8     BYTE  R0  */
#define VME_MCS_COUNT_HI    0X11   /* 8     BYTE  R0  */
#define VME_MCS_COUNT_MH    0X12   /* 8     BYTE  R0  */
#define VME_MCS_COUNT_ML    0X13   /* 8     BYTE  R0  */
#define VME_MCS_COUNT_LO    0X14   /* 8     BYTE  R0  */
#define VME_CYC_COUNT_HI    0X15   /* 8     BYTE  R0  */
#define VME_CYC_COUNT_MH    0X16   /* 8     BYTE  R0  */
#define VME_CYC_COUNT_ML    0X17   /* 8     BYTE  R0  */
#define VME_CYC_COUNT_LO    0X18   /* 8     BYTE  R0  */
#define VME_BEAM_CTL        0X19   /* 2     BYTE  RW  */
#define VME_TRIG_CTL        0X1A   /* 2     BYTE  RW  */

struct parameters
{
    unsigned char   opcode;
    unsigned long   branch_addr;
    unsigned long   delay;
    unsigned long   flags;
    char            opcode_width;
    char            branch_width;
    char            delay_width;
    char            flag_width;
};
typedef struct parameters PARAM;

INLINE void ppgInit(const DWORD base_adr);
INLINE INT   ppgLoad(const DWORD base_adr, char *file);
INLINE DWORD ppgPolmskWrite(const DWORD base_adr, const DWORD pol);
INLINE DWORD ppgPolmskRead(const DWORD base_adr);
INLINE void  ppgDisable(const DWORD base_adr);
INLINE void  ppgEnable(const DWORD base_adr);
INLINE BYTE  ppgStatusRead(const DWORD base_adr);
INLINE PARAM lineRead(FILE *input);
INLINE void  byteOutputOrder(PARAM data, char *array);
INLINE BYTE  ppgRegWrite(const DWORD base_adr, DWORD reg_offset, BYTE value);
INLINE void  ppgPolzSet(const DWORD base_adr, BYTE value );
INLINE BYTE  ppgPolzFlip(const DWORD base_adr);
INLINE BYTE  ppgPolzRead(const DWORD base_adr);
INLINE void  ppgBeamOn(const DWORD base_adr);
INLINE void  ppgBeamOff(const DWORD base_adr);
INLINE void  ppgBeamCtlPPG(const DWORD base_adr);
INLINE BYTE  ppgBeamCtlRegRead(const DWORD base_adr);
INLINE void  ppgPolzCtlVME(const DWORD base_adr);
INLINE void  ppgPolzCtlPPG(const DWORD base_adr);
INLINE void ppgDisableExtTrig(const DWORD base_adr);
INLINE void ppgEnableExtTrig(const DWORD base_adr);

#endif
