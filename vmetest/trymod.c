#include <stdio.h>
#include <string.h>
#include <stdlib.h>
double time_slice = 0.0002;
FILE  *ppginput,*ppgoutput,*ppgmods;
typedef struct
{
  char            name[80]; // name of variable, e.g. "dwell time"
  int             index; // instruction number in the file
  unsigned long   value;     // value to set instruction to
  char            data_type[15] ; // data type (one of "delay" "loopcount" ...)
} FILE_VAR;  // filled by reading mod file
int  get_next_line(char *line);
int get_mod(char *line, FILE_VAR *file_var);
int getinsline(char *line, int max, FILE *file);

int main(void)
{
  char modfilename[80];
  char outfile[]="/home/pol/vmetest1/ppgmods.dat";
  char mode[]="1h";
  int len;
  FILE_VAR file_var;

  char  mline[100];
  double lne_pw=0.005; //ms
  double dw=0.6; //ms

  long int dw_count = (int)((dw/time_slice)+0.5);
  long int lne_count =(int)((lne_pw/time_slice)+0.5);
  printf("dw_count=%d lne_count=%d\n",dw_count,lne_count);
  long int cycle_lc = 100;
  long int scan_lc = 1;
  int flag;
  sprintf(modfilename,"/home/pol/vmetest1/mode%s_mods.dat",mode);
  
  printf("Opening ppg mod file: %s   ...  \n",modfilename);
  ppgmods = fopen(modfilename,"r");
  sleep(1) ; // sleep 1s
  if(ppgmods == NULL){
    printf("ppgLoad: ppg mod file %s could not be opened.\n", modfilename);
    return 0;
  }
  printf("Opening output mod file: %s   ...  \n",outfile);
  ppgoutput = fopen(outfile,"w");
  sleep(1) ; // sleep 1s
  if(ppgoutput == NULL){
    printf("ppgLoad: output mod file %s could not be opened.\n", outfile);
    return 0;
  }

  //  modnum=0;
  int i=0;
  fprintf(ppgoutput,"# PPGMOD output file \n"); // write line to output file
  while(get_next_line(mline) > 0)
    {
      i++;
      if (get_mod(mline, &file_var) == 0)
	return 0; // error
      printf("\nppg_modify_file: input parameters\n");
      printf("name:\"%s\"\n",file_var.name);
      printf("pc:%3.3d\n",file_var.index);
      printf("data type:\"%s\"\n",file_var.data_type);
      flag=1;
      if(strcmp(file_var.name,"scan loop count")==0)
	file_var.value = scan_lc;
      else if (strcmp(file_var.name,"cycle loop count")==0)
	file_var.value = cycle_lc;
      else if (strcmp(file_var.name,"dwell time")==0)
	file_var.value = dw_count;
      else if(strcmp(file_var.name,"lne pulse width")==0)
	file_var.value = lne_count;
      else
	{
	  printf ("Input value for %s not found\n",file_var.name);
	  flag=0;
	}
      
      if(flag)fprintf(ppgoutput,"%d %d # %s\n",file_var.index,file_var.value,file_var.name); // write line to output file
      if (i>10)break;
   }
  printf("i=%d\n",i);
  return 1; // success
}

  
int  get_next_line(char *line)
{

  int len,linenum=0;
  len=0;
    // printf("ppgoutput:  [%p]\n",ppgoutput);
  while (getinsline(line,99,ppgmods) > 0)
    {     
      linenum++;
      printf("File line %d: %s",linenum,line);
      if ( strncmp(line,"#",1) !=0 ) // skip comments at begin of file
	{
	  len=strlen(line);
	  break;
	}
    }
  // printf("returning len=%d (line=\"%s\")",len,line);
  return len;
}


int get_mod(char * line, FILE_VAR *mods)
{
  //  FILE_VAR mods;
  char str[80];
  char *p;
  char *q;
  int len=1;
  char c;
	 
  printf ("get_mod: line=\"%s\n",line);
  // check format of first item
  sscanf(line, "%c", &c );
  if( ! isdigit(c) )
    {
      len=strlen(line);
      line[len-1]='\0'; // strip off <CR> for message
      printf("Invalid format for line \"%s\"; First item must be a digit\n",line);
      return 0;
    }

  sscanf(line,"%u %s %s", &(mods->index), &(mods->data_type), str);
  p=strstr(line,str);
  //printf("p=%s\n",p);
  q=p;
  while(*p!='#')
    p++;
  p--; // strip off any blanks between last character and #
  while(*p==' ')
    p--;
  *(p+1)='\0'; // terminate string
  strcpy(mods->name,q);
  //if(debug)
    {
      printf("\nget_mod: output parameters\n");
      printf("name:\"%s\"\n",mods->name);
      printf("pc:%3.3d\n",mods->index);
      printf("data type:\"%s\"\n",mods->data_type);
    }
    return 1; // success
}
/** getinsline
    Read line of input instruction file
    @memo read line.
    @param *input char* line buffer to contain line of input file
    @param *input int max  integer maximum characters in line
    @param *input FILE *file file handle
    @return COMMAND data structure
*/
/* getinsline */
int getinsline(char *line, int max, FILE *file)
{
  if (fgets(line,max,file) == NULL)
    return 0;
  else
    return strlen(line);
} 
