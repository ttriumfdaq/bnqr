/*********************************************************************

  Name:         newppg.c
  Created by:   
        based on v292.c and trPPG.c Pierre-Andre Amaudruz

  Contents:     PPG Pulse Programmer
                
  $Id: tppg.c 3638 2007-03-02 01:35:35Z amaudruz $
*********************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>     /* gmtime(),strftime() */
#include <ctype.h>
#include "vmicvme.h"
#include "unistd.h" // for sleep
#include "newppg.h"
int ddd=0; //debug
FILE  *ppginput;
int  num_instructions=0; // number of instructions from last loaded ppg file
int  start_pc=0; // pc for start of last loaded ppg file
#define FALSE 0
#define TRUE 1

#define HALT 0
#define DELAY 1
#define LOOP 2
#define ENDLOOP 3


/*****************************************************************/

/*------------------------------------------------------------------*/

void ppg(void)
{
  printf(" hallo\n");
   printf("       TRIUMF PPG function support\n");
   printf("A Init               B Load\n");
   printf("C StopSequencer      E StartSequencer\n");
   printf("F EnableExtTrig      G DisableExtTrig\n");
   printf("H Use Alpha          I StatusRead\n");
   printf("J PolMaskRead        K PolMaskWrite\n");
   printf("L RegWrite           M RegRead\n");
   printf("                     O ReadIns\n");
   printf("Q EnableExtClock     R DisableExtClock\n");
   printf("S EnableSlowClock    T DisableSlowClock\n");
   printf("U EnableTestMode     V DisableTestMode\n");
   printf("W SetPgmStartAddr    Y ReadPgmStartAddr\n");
   printf("D debug (toggles)    P Print this list\n");
   printf("X exit \n");
   printf("\n");
}

/*------------------------------------------------------------------*/
/** TPPGRegRead
    Read TRIUMF PPG register.
    @memo Read PPG.
    @param base\_adr PPG VME base addroless
    @param reg\_offset PPG register
    @return register (32 bit)
*/
DWORD TPPGRegRead(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD reg_offset)
{
  DWORD value,myreg;
  int  cmode, localam;
  
    mvme_get_dmode(mvme, &cmode); // store present data mode
    mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 

    mvme_get_am(mvme, &localam); // store present addressing mode
    mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 

  myreg =  base_adr + reg_offset;
  if (ddd) printf("Reading from address 0x%x\n", myreg );
  value = mvme_read_value(mvme, myreg );
  if(ddd) printf("Read back 0x%x (%u) (%08x)\n",value,value,(int)value);

   mvme_set_dmode(mvme, cmode);// restore data mode
   mvme_set_am(mvme, localam);// restore addressing mode
  return  value;
}


/*------------------------------------------------------------------*/
/** TPPGRegWrite
    Write into TRIUMF PPG register.
    @memo Write TPPG.
    @param base\_adr TPPG VME base address
    @param reg\_offset TPPG register
    @param value (8bit)
    @return status register
*/
DWORD TPPGRegWrite(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD reg_offset, DWORD value)
{

  DWORD myval,myreg;
  int  cmode, localam;
  
  mvme_get_dmode(mvme, &cmode);  // remember present VME data mode
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32

  mvme_get_am(mvme, &localam); // remember present VME addressing mode
  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32

  myreg =  base_adr + reg_offset;
  myval = (DWORD)value;
  mvme_write_value(mvme, myreg, value);
  if (ddd) printf("Writing reg 0x%x value 0x%x\n", myreg,value);
  myval = 0x1BAD1BAD; // set to a different value in case of failure
  myval = mvme_read_value(mvme, myreg);
  // testing ppgload - read always returns zero
  //   if(ddd)printf("mvme_read_value returns 0x%x (%u)\n",myval,myval);

  mvme_set_dmode(mvme, cmode); // restore VME data mode
  mvme_set_am(mvme, localam); // restore VME addressing mode

  return myval;
}

/*------------------------------------------------------------------*/
/** TPPGSetIns
    Set a complete instruction
    @memo Set an instruction
    @param base\_adr PPG VME base address
    @param address  Address to write instruction
    @param PPG bit pattern 
    @param delay  clock cycles
    @param instruction  instruction 0:HALT 1:CONT 2:LOOP 3:ENDLOOP 4:SUBR 5:RETURN 6:BRANCH
    @param instr_data  20 bit data used for instructions LOOP,SUBR,BRANCH
    @return register (32 bit)
*/
DWORD TPPGSetIns(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD addr, DWORD instr, DWORD bitpat, DWORD delay_count, DWORD instr_data)
{
  DWORD top=0;
  DWORD data;
  DWORD setbits,clrbits;
  int  cmode, localam;

  if (addr < 0 || addr > 128)
    {
      printf ("TPPGSetIns: illegal address for instruction (%d)\n",(int)addr);
      return -1;
    }
  if (instr < 0 || instr > 6)
    {
      printf ("TPPGSetIns: unknown instruction (%d)\n",(int)instr);
      return -1;
    }
  if (delay_count < MINIMAL_DELAY)
    {
      printf ("TPPGSetIns: Error -  delay count (%u) is less than minimal delay (%d clock cycles)\n",
	      (unsigned int)delay_count,MINIMAL_DELAY);
      return -1;
    }
  else if ( delay_count > MAX_DELAY_COUNT)
    {
      printf ("TPPGSetIns: Error - delay (%u) is greater than maximum delay ( %u clock cycles) \n",
	      (unsigned int)delay_count, (unsigned int) MAX_DELAY_COUNT);
      return -1;
    }

  top = instr << 20;   
  top = top | instr_data;
  
  setbits = bitpat;
  clrbits = ~bitpat;
  printf("compound instruction/data: 0x%x  setbits: 0x%x  clrbits: 0x%x\n",top,setbits,clrbits);

  mvme_get_dmode(mvme, &cmode); // store present data mode
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 

  mvme_get_am(mvme, &localam); // store present addressing mode
  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 



  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , addr); // write instruction address 
  printf("TPPGSetIns: after writing instruction addr %d,  data = %d\n",(int)addr,(int)data);
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_LO  , setbits); // "set" bits (32 bits)
  printf("TPPGSetIns: after writing data lo = 0x%x,  data = 0x%x\n",setbits,data);
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_MED  , clrbits);// "clear" bits (32 bits)
  printf("TPPGSetIns: after writing data med = 0x%x,  data = 0x%x\n",clrbits,data);
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_HI  , delay_count);//  delay count 
    printf("TPPGSetIns: after writing data hi = 0x%x,  data = 0x%x\n",delay_count,data);
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_TOP  , top);// instruction and its data 
    printf("TPPGSetIns: after writing data top = 0x%x,  data = 0x%x\n",top,data);


  mvme_set_dmode(mvme, cmode); // restore VME data mode
  mvme_set_am(mvme, localam); // restore VME addressing mode


  return 0; // success
}
/*------------------------------------------------------------------*/
/** TPPGReadIns
    Read back a complete instruction
    @memo Read an instruction
    @param base\_adr PPG VME base address
    @param address  Address to read instruction
    @return register (32 bit)
*/
PARAM TPPGReadIns(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD addr)
{
  DWORD data, tmp;
  DWORD setbits,clrbits;
  DWORD  ins,delay_count;
  int  cmode, localam;

  PARAM read_info;

  if (addr < 0 || addr >128)
      printf ("TPPGReadIns: Warning - illegal address for instruction (%d)\n",(int)addr);


  mvme_get_dmode(mvme, &cmode); // store present data mode
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 

  mvme_get_am(mvme, &localam); // store present addressing mode
  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 

  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , addr); // write instruction address 
  if(ddd)printf("TPPGReadIns: after writing instruction addr %d,  data = %d\n",(int)addr,(int)data);

  setbits=0;
  setbits = TPPGRegRead(mvme, base_adr,  TPPG_DATA_LO ); // read "set" bits (32 bits)
  if(ddd)printf("TPPGReadIns: after reading data lo (set bits) = 0x%x\n",setbits);
  
  clrbits=0;
  clrbits = TPPGRegRead(mvme, base_adr,  TPPG_DATA_MED );// read "clear" bits (32 bits)
  if(ddd)printf("TPPGReadIns: after reading data med (clear bits) = 0x%x\n", clrbits);

  delay_count=0;
  delay_count = TPPGRegRead(mvme, base_adr,  TPPG_DATA_HI );// read delay count (32 bits)
  if(ddd)printf("TPPGReadIns: after reading data hi (delay count) = 0x%x\n", delay_count);
  
  data = 0;
  data = TPPGRegRead(mvme, base_adr,  TPPG_DATA_TOP  );// instruction and data 
  if(ddd)printf("TPPGReadIns: after reading data top = 0x%x\n",data);
  ins = data & 0xFFFFF;
  ins = ((ins >> 20) & 7);
  if (ins > 6)
      printf ("TPPGReadIns: Warning - illegal instruction (%d)\n",(int)ins);
  tmp = data & 0xFFFF; // mask away instruction
   
  printf ("TPPGReadIns: InstrAddr:%d SetBitpat:0x%x ClrBitpat:0x%x DelayCount:%u Instruction %s (%d) Instruction data %u\n",
	  (int)addr,setbits,clrbits,delay_count,instructions[ins],(int)ins,tmp );
	  


  mvme_set_dmode(mvme, cmode); // restore VME data mode
  mvme_set_am(mvme, localam); // restore VME addressing mode


  read_info.pc = addr;
  read_info.setpat = setbits ;
  read_info.clrpat = clrbits;
  read_info.delay = delay_count;
  read_info.ins_data = data;
  return read_info; // success
}




/*------------------------------------------------------------------*/
/** TPPGInit
    Initialize the TRIUMF PPG
    @memo Initialize PPG
    @param base\_adr PPG VME base address
    @return void
*/
void TPPGInit(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD bitpat)
{
  DWORD zero;
  // No Reset ??
  zero = 0;
  TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , zero); // stop seq, internal clock and trigger
  // Set instruction address back 0
  TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , zero ); // write start instruction address 
  TPPGPolmskWrite(mvme, base_adr, zero); // clear polarity
  TPPGStatusRead(mvme, base_adr);
  return;
}

/*------------------------------------------------------------------*/
/** TPPGStatusRead
    Read Status register.
    @memo Read status.
    @param base\_adr PPG VME base address
    @return status register
*/
DWORD TPPGStatusRead(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,temp;
  char str[256];

  data = TPPGRegRead(mvme, base_adr,  TPPG_CSR_REG );
  //if (ddd)
  printf("TPPGStatusRead: status = 0x%x\n\n",data);
  if (data & 1)    
    sprintf(str,"PPG running: Y ");
  else
    sprintf(str,"PPG running: N ");


  if (data & 2)
    strcat(str,"External clock: Y ");
  else
    strcat(str,"External clock: N ");

  if (data & 4)
    strcat(str,"External trigger: Y ");
  else
    strcat(str,"External trigger: N ");

  if (data & 8)
    strcat(str,"Slow Clock: Y ");
  else
    strcat(str,"Slow Clock: N ");

  if (data & 0x10)
    strcat(str,"Test Mode: Y ");
  else
    strcat(str,"Test Mode: N \n");

  printf("%s\n",str);
  temp = (data & TPPG_STATUS_READBACK_MASK) >> 5 ;
  printf("PC,SP, Current Delay counter: 0x%x\n\n",(unsigned int)temp); 
  return data;
}


/*------------------------------------------------------------------*/
/** TPPGStartSequencer
    Start the PPG sequencer (internal trigger) at PC of last loaded program
    @memo start the PPG sequencer.
    @param base\_adr PPG VME base address
    @return void
*/
  void TPPGStartSequencer(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data;
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data & TPPG_RUN_MASK) | 1;  // mask all but clock,start bits
  if (ddd)printf("TPPGStartSequencer: writing 0x%4.4x to CSR 0x%2.2x \n",(unsigned int) data, TPPG_CSR_REG);
  TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  return;
}

/*------------------------------------------------------------------*/
/** TPPGStopSequencer
    Stop the TRIUMF PPG sequencer.
    @memo Stop the PPG sequencer.
    @param base\_adr PPG VME base address
    @return void
*/
void TPPGStopSequencer(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data1 = TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data1 & TPPG_RUN_MASK) ;  // 0x1E  mask all control bits except run bit
  //if (ddd)
    printf("TPPGStopSequencerNow: writing 0x%4.4x to CSR 0 (was 0x%4.4x) \n",
		 (unsigned int) data,(unsigned int) data1 );
  TPPGRegWrite(mvme, base_adr,   TPPG_CSR_REG  , data);
  return;
}

/*------------------------------------------------------------------*/
/** TPPGSetStartPC
    Set the starting PC of the program in the PPG sequencer.
    @memo Set the pgm start PC.
    @param base\_adr PPG VME base address
    @param pc  Address to start program 
    @return void
*/
DWORD TPPGSetStartPC (MVME_INTERFACE *mvme,  const DWORD base_adr, DWORD pc)
{
  DWORD data;
  // Set instruction address back to start of program
  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , pc ); // write start instruction address 
  return data;
}



#ifdef GONE  // this doesn't work
void TPPGStopSequencerEOC(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data;

  // try to overwrite scan loop counter with 1
  
  TPPGOverwritePgm(mvme, base_adr, 1,1);
  
  data = TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data & TPPG_RUN_MASK) ;  // mask all but clock,start bits
  if (ddd)printf("TPPGStopSequencerEOC: writing 0x%4.4x to CSR 0 \n",(unsigned int) data);
  TPPGRegWrite(mvme, base_adr,   TPPG_CSR_REG  , data);
  return;
}

// no longer needed now TPPGStopSequencer works properly
void TPPGHaltPgm(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  int num;
  num = num_instructions;
  if (num <=0)
    num=10;
  TPPGOverwritePgm(mvme,base_adr, 0, num_instructions);  
}

void TPPGOverwritePgm(MVME_INTERFACE *mvme, const DWORD base_adr, unsigned int pc, int num)
{
  int  cmode, localam;
  char zero[]="000 0x00000000 0xffffffff  0x00000003 0x100000"; // clear all output bits
  char halt[]="000 0x00000000 0xffffffff  0x00000003 0x000000";
  PARAM  command_info;
  int i;
  DWORD data;
  unsigned int my_pc;
  //PARAM read_info;
  my_pc = start_pc = pc;

  if (num==0)
    num=10;

  mvme_get_dmode(mvme, &cmode); // store present data mode
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 
  
  mvme_get_am(mvme, &localam); // store present addressing mode
  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 
  
  if (num==1)
    printf("Overwriting program with %d halt instruction starting at pc=%d\n",num,my_pc);
  else
    {
      printf("Overwriting program with zero-outputs and %d halt instruction(s) starting at pc=%d\n",num-1,my_pc);
      printf("Loading zero-outputs instruction at pc %d\n",my_pc);
      command_info = lineRead(zero);
      // load this instruction
      command_info.pc = my_pc;
      data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , command_info.pc ); // write instruction address 
      data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_LO  , command_info.setpat); // "set" bits (32 bits)
      data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_MED  ,command_info.clrpat );// "clear" bits (32 bits)
      data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_HI  , command_info.delay);// delay count
      data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_TOP  , command_info.ins_data);// instruction and any data

      if(TPPGCheckIns(mvme,base_adr, &command_info)!=0)
	printf("Error from TPPGCheckIns for instruction at PC %d\n",my_pc);
      my_pc++;
      num--;
    }


  command_info = lineRead(halt);
  
  if(ddd)
    {
      printf("Halt instruction:\n");
      printf("pc:%3.3d ",my_pc);
      printf("set bitpat:%8.8lx ",command_info.setpat);
      printf("clr bitpat:%8.8lx ",command_info.clrpat);
      printf("delay:%8.8lx ",command_info.delay);
      printf("ins/data:%8.8lx\n",command_info.ins_data);
    }
 


  for(i=0; i<num; i++)
    {
      printf("Writing halt instruction to pc %d\n",my_pc);
      // load this instruction
      command_info.pc = my_pc;
      data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , command_info.pc ); // write instruction address 
      data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_LO  , command_info.setpat); // "set" bits (32 bits)
      data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_MED  ,command_info.clrpat );// "clear" bits (32 bits)
      data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_HI  , command_info.delay);// delay count
      data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_TOP  , command_info.ins_data);// instruction and any data

      my_pc++;
    } // end of while

  // Set instruction address back to start of program
  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , start_pc ); // write start instruction address 
  
  mvme_set_dmode(mvme, cmode); // restore VME data mode
  mvme_set_am(mvme, localam); // restore VME addressing mode
   

  printf("\nNow trying to run the program with the HALT instruction...\n");
  TPPGStopSequencer(mvme, base_adr); // Start needs an edge
  sleep(0.5) ; // sleep 0.5s
  TPPGStartSequencer(mvme, base_adr);
  sleep(1) ; // sleep 1s
  TPPGStopSequencer(mvme, base_adr); // needs an edge

  printf("Has it stopped?\n");
  TPPGStatusRead(mvme, base_adr);
  return;
}
#endif // GONE

int TPPGCheckIns (MVME_INTERFACE *mvme, const DWORD base_adr, PARAM *command_info)
{ // Can only check the last instruction written
  PARAM read_info;
  int ret_code;

  ret_code =0;

  if(ddd)
    {
      printf("TPPGCheckIns:  Wrote:\n");
      printf("pc:%3.3d ",command_info->pc);
      printf("set bitpat:%8.8lx ",command_info->setpat);
      printf("clr bitpat:%8.8lx ",command_info->clrpat);
      printf("delay:%8.8lx ",command_info->delay);
      printf("ins/data:%8.8lx\n",command_info->ins_data);
    }

  read_info = TPPGReadIns(mvme,base_adr,command_info->pc);

  if(ddd)
    {
      printf("TPPGCheckIns:  Read back:\n");
      printf("pc:%3.3d ",read_info.pc);
      printf("set bitpat:%8.8lx ",read_info.setpat);
      printf("clr bitpat:%8.8lx ",read_info.clrpat);
      printf("delay:%8.8lx",read_info.delay);
      printf("ins/data:%8.8lx\n",read_info.ins_data);
    }

  if( read_info.pc != command_info->pc)
    {
      printf("TPPGCheckIns : wrote addr as %3.3d, read back %3.3d\n",command_info->pc,read_info.pc);
      ret_code=1;
    }
  
  if( read_info.setpat != command_info->setpat)
    {  
      printf("TPPGCheckIns : wrote setpat as %8.8lx, read back %8.8lx\n",command_info->setpat,read_info.setpat);
      ret_code=1;
    }
  if( read_info.clrpat != command_info->clrpat)
    {
      printf("TPPGCheckIns : wrote clrpat as %8.8lx, read back %8.8lx\n",command_info->clrpat,read_info.clrpat);
      ret_code=1;
    }
  if( read_info.delay != command_info->delay)
    {
      printf("TPPGCheckIns : wrote delay as %8.8lx, read back %8.8lx\n",command_info->delay,read_info.delay);
      ret_code=1;
    }
  if( read_info.ins_data != command_info->ins_data)
    {
      printf("TPPGCheckIns : wrote ins/data as %8.8lx, read back %8.8lx\n",command_info->ins_data,read_info.ins_data);
      ret_code=1;
    }
  
  return ret_code;
}
/*------------------------------------------------------------------*/
/**  TPPGEnableExtTrig(ppg_base)
    Enable front panel trigger input so external inputs can start the sequence
    @memo Enable external trigger  input so external inputs can start the sequence
    @param base\_adr PPG VME base address
    @return data
*/
void TPPGEnableExtTrig(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data=data1=0xBAD1BAD1; // set to something
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  printf("TPPGEnableExtTrig:  Read CSR as 0x%x\n",data);
  data = (data & TPPG_TRIG_MASK) | 4 ;
  data1 = TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);

   printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);
  if( (data & TPPG_STATUS_BITS_MASK) != (data1 & TPPG_STATUS_BITS_MASK ))
    printf("TPPGEnableExtTrig: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);
  printf("ExtTrig selected\n");    
  return;
}



/*------------------------------------------------------------------*/
/**  TPPGDisableExtTrig(ppg_base)
    Disable front panel trigger input so external inputs cannot start the sequence
    @memo Disable external trigger  input so external inputs cannot start the sequence
    @param base\_adr PPG VME base address
    @return data
*/
void TPPGDisableExtTrig(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data=data1=0xBAD1BAD1; // set to something
  data = TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  printf("TPPGDisableExtTrig:  Read CSR as 0x%x\n",data);
  data = (data & TPPG_TRIG_MASK)  ;  // mask all status bits but clock,run bits (run should be stopped)
  data1 = TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);

 if( (data & TPPG_STATUS_BITS_MASK) != (data1 & TPPG_STATUS_BITS_MASK ))
    printf("TPPGDisableExtTrig: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);
  printf("ExtTrig disabled\n");
  return;
}




/*------------------------------------------------------------------*/
/**  TPPGEnableExtClock(ppg_base)
    Enable front panel clock input 
    @memo Enable external clock input 
    @param base\_adr PPG VME base address
    @return data
*/
void TPPGEnableExtClock(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data=data1=0xBAD1BAD1; // set to something  
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  printf("TPPGEnableExtClock:  Read CSR as 0x%x\n",data);
  data = (data & TPPG_CLOCK_MASK) | 2 ;  // 0x1D mask all but trig,run bits
  data1=TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);

  if( (data & TPPG_STATUS_BITS_MASK) != (data1 & TPPG_STATUS_BITS_MASK ))
    printf("TPPGEnableExtClock: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);

  printf("ExtClock selected\n");    
  return;
}



/*------------------------------------------------------------------*/
/**  TPPGDisableExtClock(ppg_base)
    Disable front panel clock input so internal clock is used
    @memo Disable external clock  input so internal clock is used
    @param base\_adr PPG VME base address
    @return data
*/
void TPPGDisableExtClock(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data=data1=0xBAD1BAD1; // set to something
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  printf("TPPGDisableExtClock:  Read CSR as 0x%x\n",data);
  data = (data & TPPG_CLOCK_MASK)  ;  // 0x1D  mask all but trig,run bits

  data1 = TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);

  if( (data & TPPG_STATUS_BITS_MASK) != (data1 & TPPG_STATUS_BITS_MASK ))
    printf("TPPGDisableExtClock: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);

  printf("ExtClock disabled\n");
  return;
}



/*------------------------------------------------------------------*/
/**  TPPGEnableSlowClock(ppg_base)
    Enable slow clock
    @memo Enable slow clock 
    @param base\_adr PPG VME base address
    @return data
*/
void TPPGEnableSlowClock(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data=data1=0xBAD1BAD1; // set to something  
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  printf("TPPGEnableSlowClock:  Read CSR as 0x%x\n",data);
  data = (data & TPPG_SLOW_CLOCK_MASK) | 8 ;  // 0x17 mask all but trig,run bits
  data1=TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);

  if( (data & TPPG_STATUS_BITS_MASK) != (data1 & TPPG_STATUS_BITS_MASK ))
    printf("TPPGEnableSlowClock: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);
  printf("SlowClock selected\n");    
  return;
}



/*------------------------------------------------------------------*/
/**  TPPGDisableSlowClock(ppg_base)
    Disable slow clock input so default clock is used
    @memo Disable slow clock  so default clock is used
    @param base\_adr PPG VME base address
    @return data
*/
void TPPGDisableSlowClock(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data=data1=0xBAD1BAD1; // set to something
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  printf("TPPGDisableSlowClock:  Read CSR as 0x%x\n",data);
  data = (data & TPPG_SLOW_CLOCK_MASK)  ;  // mask all but trig,run bits

  data1 = TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);

  if( (data & TPPG_STATUS_BITS_MASK) != (data1 & TPPG_STATUS_BITS_MASK ))
    printf("TPPGDisableSlowClock: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);
  printf("Writing 0x%x to CSR;  read back 0x%x\n",data,data1);
  printf("SlowClock disabled\n");
  return;
}

/*------------------------------------------------------------------*/
/**  TPPGEnableTestMode(ppg_base)
    Enable test mode (LED/NIM follow state of address reg)
    @memo Enable test mode  (LED/NIM follow state of address reg)
    @param base\_adr PPG VME base address
    @return data
*/
void TPPGEnableTestMode(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data=data1=0xBAD1BAD1; // set to something  
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );

  data = (data & TPPG_TEST_MODE_MASK) | 0x10 ;  // 0x0F mask - set test mode bit
  data1=TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);

  if( (data & TPPG_STATUS_BITS_MASK) != (data1 & TPPG_STATUS_BITS_MASK ))
    printf("TPPGEnableTestMode: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);
  printf("TestMode selected\n");    
  return;
}

/*------------------------------------------------------------------*/
/**  TPPGDisableTestMode(ppg_base)
    Disable test mode
    @memo Disable test mode
    @param base\_adr PPG VME base address
    @return data
*/
void TPPGDisableTestMode(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data=data1=0xBAD1BAD1; // set to something
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  printf("TPPGDisableTestMode:  Read CSR as 0x%x\n",data);

  data = (data & TPPG_TEST_MODE_MASK)  ;  // 0x0F mask - clear test mode bit

  data1 = TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
   printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);

 if( (data & TPPG_STATUS_BITS_MASK) != (data1 & TPPG_STATUS_BITS_MASK ))
    printf("TPPGDisableTestMode: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);
  printf("Writing 0x%x to CSR;  read back 0x%x\n",data,data1);
  printf("TestMode disabled\n");
  return;
}

/*------------------------------------------------------------------*/
/** TPPGPolmskRead 
    Read the Polarity mask.
    @memo Read polarity mask.
    @param base\_adr PPG VME base address
    @return polarity (24bit)
*/
  DWORD TPPGPolmskRead(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD  data;
  data = TPPGRegRead(mvme, base_adr,  TPPG_POL_MASK );
  return data;
}

/*------------------------------------------------------------------*/
/** TPPGPolmskWrite
    Write the Polarity mask.
    @memo Write polarity mask.
    @param base\_adr PPG VME base address
    @return polarity (24bit)
*/
DWORD TPPGPolmskWrite(MVME_INTERFACE *mvme, const DWORD base_adr, const DWORD pol)
{
  DWORD  data;
  data = TPPGRegWrite(mvme, base_adr,  TPPG_POL_MASK, pol );
  return data;
}


/*------------------------------------------------------------------*/
/** ppgLoad
    Load PPG file into sequencer.
    @memo Load file PPG.
    @param base\_adr PPG VME base address
    @param pc\_offset Offset at which to load program
    @return 1=SUCCESS, -1=file not found
*/
int TPPGLoad(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD pc_offset, char *file)
{
  /*  Local Variables  */

  PARAM  command_info;
  char  line[128];
  int  cmode, localam;
  int linenum;
  char *p;
  int header_flag;
  int counter;
  DWORD data;

  //  FILE  *ppginput;

  //  New PPG
  //  SET bits:0-31    |  CLR bits 32-63  | Delay Count:64-95  | Delay Count: 96-115 | Instruction 116-119 |  120-127
  //        32 bits    |        32 bits   |    32 bits         |    32 bits          |     4 bits          |  ignored

  
  TPPGStatusRead(mvme,base_adr);
  printf("TPPGLoad: stopping sequencer\n");
  TPPGStopSequencer (mvme, base_adr);
    TPPGStatusRead(mvme,base_adr);  
  start_pc = num_instructions = 0; // globals
  printf("Opening ppg load file: %s   ...  \n",file);
  ppginput = fopen(file,"r");
  sleep(1) ; // sleep 1s
  if(ppginput == NULL){
    printf("ppgLoad: ppg load file %s could not be opened. [%p]\n", file, ppginput);
    return -1;
  }

  linenum=counter=0;
  header_flag=1;

  mvme_get_dmode(mvme, &cmode); // store present data mode
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 

  mvme_get_am(mvme, &localam); // store present addressing mode
  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 

  // Find number of instructions
  while (getinsline(line,127,ppginput) > 0) // line[128]
    {        
      linenum++;
      printf("File line %d: %s\n",linenum,line);
      if(header_flag)
	{
	  if ( strncmp(line,"#",1) !=0 ) // skip comments at begin of file
	    {
	      if ( strncmp(line,"Num Instruction Lines =",23) ==0 ) // first line after comments
		{
		  p=line + 23;  // skip "Instruction Lines "
		  char *q;
		  q =strchr(p,'#');
		  if(q != 0)
		    *q='\0';
		  printf("Now line is %s\n",p);
		  sscanf(p,"%d",&num_instructions);
		  printf("instruction lines = %d\n",num_instructions);  
		  printf("Base Address = %x\n",base_adr);
		  header_flag=0; // we have processed header
		}
	    }
	}
      else
	{ // instructions
	  command_info = lineRead(line);
	  counter++;
	  if (counter==1)
	    {
	      start_pc =  command_info.pc  + pc_offset; // remember where program starts
	      printf("TPPGLoad: offset =  %d; program will start at PC = %d\n",pc_offset,start_pc);
	    }
	  // if (ddd)
	    {
	      printf("Line %d (instr %d) : ",linenum,counter);
	      printf("pc:%3.3d ",command_info.pc);
	      printf("setpat:%8.8lx ",command_info.setpat);
	      printf("clrpat:%8.8lx ",command_info.clrpat);
	      printf("delay:%8.8lx",command_info.delay);
	      printf("ins/data:%8.8lx\n",command_info.ins_data);
	    }
	    command_info.pc += pc_offset;
	    if ( command_info.pc > 0x1000) 
	      {
		printf ("Error : instruction address (%d) exceedes program memory capacity\n",command_info.pc);
		return -1;
	      }
	  // load this instruction
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , command_info.pc ); // write instruction address 
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_LO  , command_info.setpat); // "set" bits (32 bits)
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_MED  ,command_info.clrpat );// "clear" bits (32 bits)
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_HI  , command_info.delay);// delay count
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_TOP  , command_info.ins_data);// instruction and any data
	}
    } // end of while


  if(header_flag > 0)
    {
      printf("TPPGLoad: incorrect format for ppg load file. Did not find number of instruction lines\n");
      return -1;
    }

  if(counter != num_instructions)
    printf("TPPGLoad: Warning - stated number of instructions lines (%d) does not agree with actual number(%d)\n",
	   num_instructions,counter);
  
  

  // Set instruction address back to start of program
  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , start_pc ); // write start instruction address 
  printf("TPPGLoad: set instruction address (reg 0x%x) back to start of program (pc = %d)\n",
	 TPPG_ADDR,data);

  mvme_set_dmode(mvme, cmode); // restore VME data mode
  mvme_set_am(mvme, localam); // restore VME addressing mode
    
  fclose(ppginput);
  TPPGStatusRead(mvme,base_adr);
  if(ddd) printf("Programming ended, PPG ready");
  return 1;
}

/*------------------------------------------------------------------*/
/** lineRead
    Read line of input file
    @memo read line.
    @param *input char* line  line of input file
    @return PARAM data structure
*/
PARAM lineRead(char *line)
{
  PARAM data_struct;
  if(ddd)printf ("lineRead: line=\"%s\n",line);
  char *p=strchr(line,'#'); // look for comment (#) following instruction
  if (p)
    *p='\0';
  if(ddd)printf ("lineRead: line=\"%s\n",line);
  sscanf(line,"%u %lx %lx %lx %lx", &(data_struct.pc), &(data_struct.setpat),
	 &(data_struct.clrpat), &(data_struct.delay), &(data_struct.ins_data));
  return(data_struct);
}
/*------------------------------------------------------------------*/
/** getinsline
    Read line of input instruction file
    @memo read line.
    @param *input char* line buffer to contain line of input file
    @param *input int max  integer maximum characters in line
    @param *input FILE *file file handle
    @return PARAM data structure
*/
/* getinsline */
int getinsline(char *line, int max, FILE *file)
{
  if (fgets(line,max,file) == NULL)
    return 0;
  else
    return strlen(line);
} 
		  

 
char *firmware_date(time_t val)
{
   static char fwtimebuf[FWTBUFSIZ];
   struct tm *tptr = gmtime(&val);
   strftime(fwtimebuf, FWTBUFSIZ, "%d%b%g_%H:%M", tptr);
   return(fwtimebuf);
}
/*****************************************************************/
/*-PAA- For test purpose only */
#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {


  DWORD PPG_BASE = 0x00100000;

  MVME_INTERFACE *mvme;

  int status;
 
  DWORD data,reg_offset;
  //DWORD setbits,clrbits,delay;
  char cmd[]="hallo";

  int s,v;
  DWORD ival;
  char filename[128];

  DWORD rpol,pol,adr;

  char default_file[80];
  char expt[20];


  strncpy(expt, getenv("MIDAS_EXPT_NAME"), sizeof(expt));
  
  printf("Current experiment is %s\n",expt);
  // POL is for pol expt. ALPHA is for testing
  sprintf(default_file,"/home/%s/online/%s/ppgload/ppgload.dat",expt,expt);
  printf("default file is %s\n",default_file);
 
  if (argc>1) {
    sscanf(argv[1],"%lx",&PPG_BASE);
  }
  if (argc>2) {
    sscanf(argv[2],"%d",&ddd);
  }

  printf("PPG base: %lx   debug=%d\n",PPG_BASE,ddd); 
    
  // Test under vmic   
  status = mvme_open(&mvme, 0);
  if(status != SUCCESS)
    {
      printf("failure after mvme_open, status = %d\n",status);
      return status;
    }

  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 

 
  v=TPPGRegRead(mvme, PPG_BASE, TPPG_FIRMWARE_ID);
  printf("\nPPG FirmwareID: %s [0x%08x]\n\n", firmware_date(v), v );


  TPPGStatusRead(mvme,PPG_BASE);
    
  ppg();
  while ( isalpha(cmd[0]) )
    {
      printf("\nEnter command (A-Y) X to exit?  ");
      scanf("%s",cmd);
      //  printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];
      
      switch(s)
	{
	case ('A'):
	  TPPGInit(mvme, PPG_BASE,0);
	  break;
	case ('B'):
          printf("Default filename is %s\n",default_file);
	  printf("Enter PPG filename or enter \"def\" for default:");
	  scanf("%s",filename);
          if (strncmp( filename,"def",3)==0)
	    {
	      printf ("Using default filename\n");
	      strcpy(filename,default_file);
	    }
	  printf("Enter program start address (usually 0) (dec) : ");
	  scanf("%ld",&ival);
	  printf("Loading program at PC %d\n",ival);
	  TPPGLoad(mvme, PPG_BASE, ival, filename);
	  break;

	case ('C'):
	  TPPGStopSequencer(mvme, PPG_BASE);
	  break;
	case ('E'):
	  TPPGStopSequencer(mvme, PPG_BASE); // needs an edge
          sleep(0.5) ; // sleep 0.5s
	  TPPGStartSequencer(mvme, PPG_BASE);
	  break;
	case ('F'):
	  TPPGEnableExtTrig(mvme, PPG_BASE);
	  break;
	case ('G'):
	  TPPGDisableExtTrig(mvme, PPG_BASE);
	  break;
	  //	case ('H'):
	  
	  //break;
	case ('I'):
	    TPPGStatusRead(mvme, PPG_BASE);
	    break;
	case ('J'):
	  rpol = TPPGPolmskRead(mvme, PPG_BASE);
	  printf("Read back Pol Mask=0x%lx\n",rpol);
	  break;
	case ('K'):
	  printf("Enter Mask value to write :0x");
	  scanf("%lx",&pol);
	  printf("Writing Pol Mask=0x%lx\n",pol);
	  rpol = TPPGPolmskWrite(mvme, PPG_BASE, pol);
	  printf("Read back Pol Mask=0x%lx\n",rpol);
	  break;
	case ('L'):
	  printf("Enter PPG register offset : 0x");
	  scanf("%lx",&reg_offset);
	  printf("Enter data to write: 0x");
	  scanf("%lx",&ival);
	  printf("Writing 0x%lx (%ld) to register offset 0x%lx (%lu)\n",
		 ival,ival,reg_offset,reg_offset);
	  data=TPPGRegWrite(mvme, PPG_BASE, reg_offset, ival);
	  printf("Read back from offset 0x%lx (%lu)  data = 0x%lx (%ld)\n",
		 reg_offset,reg_offset,data,data);
	  break;
	case ('M'):
	  printf("Enter PPG register offset : 0x");
	  scanf("%lx",&reg_offset);
	  data=TPPGRegRead(mvme, PPG_BASE, reg_offset);
	  printf("Read back from offset 0x%lx (%lu)    data = 0x%lx (%ld)\n ",
		 reg_offset,reg_offset,data,data);
	  break;
	case ('N'):
	  break;
	case ('O'):
	  printf("Enter address of instruction to read :0x");
	  scanf("%lx",&adr);
	  TPPGReadIns(mvme, PPG_BASE,adr);
	  break;
	case ('Q'):
	  TPPGEnableExtClock(mvme, PPG_BASE);
	  break;
	case ('R'):
	  TPPGDisableExtClock(mvme, PPG_BASE);
	  break;
	case ('S'):
	  TPPGEnableSlowClock(mvme, PPG_BASE);
	  break;
	case ('T'):
	  TPPGDisableSlowClock(mvme, PPG_BASE);
	  break;
	case ('U'):
	  TPPGEnableTestMode(mvme, PPG_BASE);
	  break;
	case ('V'):
	  TPPGDisableTestMode(mvme, PPG_BASE);
	  break;
	case ('W'):
	  printf("Enter program start address (decimal) : ");
	  scanf("%ld",&ival);
	  TPPGSetStartPC(mvme, PPG_BASE, ival);
	  printf("Program will start at address %d\n",ival);
	  break;
	case ('Y'):
	  data=TPPGRegRead(mvme, PPG_BASE, TPPG_ADDR);
	  printf("Program will start at address %d\n",data);
	  break;
	case ('D'):
	  if (ddd)
	    ddd=0;
	  else
	    ddd=1;	  break;
	case ('P'):
	  ppg();
	  break;
	case ('X'):
	  return 1;
	default:
	  break;
	}
    }



  status = mvme_close(mvme);
  return 1;
}	
#endif
