#include <stdio.h>
#include <string.h>
#include <stdlib.h>

FILE  *ppgmods;
int get_next_line(char *line);
int getinsline(char *line, int max, FILE *file);

int main(void)
{ 
  char line[100];
  char modfile[]="mode1h_mods.dat";
  int len;

  printf("Opening ppg mod file: %s   ...  \n",modfile);
  ppgmods = fopen(modfile,"r");
  sleep(1) ; // sleep 1s
  if(ppgmods == NULL){
    printf("ppgLoad: ppg mod file %s could not be opened. [%p]\n", modfile, ppgmods);
    return -1;
  }
  len=5;
  while(len > 0)
    {
  len = get_next_line(line);
  if(len>0)
    printf("*** len=%d line=%s\n",strlen(line),line);
    }

}


int  get_next_line(char *line)
{

  int len,linenum=0;
  len=0;
  while (getinsline(line,99,ppgmods) > 0)
    {     
      linenum++;
      //printf("File line %d: %s\n",linenum,line);
      if ( strncmp(line,"#",1) !=0 ) // skip comments at begin of file
	{
	  len=strlen(line);
	  break;
	}
    }
  //printf("len=%d, line=%s\n",len,line);
  return len;
}


 int getinsline(char *line, int max, FILE *file)
{
  if (fgets(line,max,file) == NULL)
    return 0;
  else
    return strlen(line);
} 
