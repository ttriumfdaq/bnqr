#include <stdio.h>
#include <string.h>
main()
{
  char *ins (int n);
  int len;
  char string[85];
  int i,c;
  int max = sizeof(string);
  unsigned int j,k,l;
 
  printf("Enter PPG Instruction e.g. 0x200004 : ");
  for (i=0; i<max-1 &&( c=getchar())!=EOF && c!='\n'; ++i)
    string[i]=c;
 
  string[i]='\0';
  len = strlen(string);
  printf("Length of line : %d\n",len);
  printf("Line: \"%s\"\n",string);

  sscanf(string,"%i",&j);
  printf ("  i=%d  0x%x\n",j,j); 

  // PPG instruction word  bits 0-19 data  bits 20-23 instruction
  unsigned int data_mask = 0xFFFF;
  unsigned int instr_mask= 0xF;

  k = j & data_mask;
  //  printf("data: 0x%x or %d\n",k,k);
  l = (j >> 20) & instr_mask;
  if (l > 6) l=7;
  printf("instruction: %s  data: 0x%x or %d\n",ins((int)l),k,k);

}
  
char *ins (int n)
{
  char *instructions[]={"Halt","Continue","begin Loop","end Loop","Call Subr","Return",
		       "Branch","Illegal"};
  return (n<1 || n > 7) ? instructions[0] : instructions[n];
} 

