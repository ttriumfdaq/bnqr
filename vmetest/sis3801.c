/*********************************************************************
  @file
  Name:         sis3801.c
  Created by:   Pierre-Andre Amaudruz

  Contents:      Routines for accessing the SIS Multi-channel scalers board

gcc -g -O2 -Wall -g -DMAIN_ENABLE -I/home1/midas/midas/include
    -o sis3801 sis3801.c vmicvme.o -lm -lz -lutil -lnsl -lpthread -L/lib
    -lvme

    $Id:$
*********************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h> // for sleep

#include "sis3801.h"

#ifndef MAIN_ENABLE
extern int dsis; // debug
#else
int dsis = 0;
#define fsiz 10000
DWORD fifo[fsiz];
DWORD *pfifo;
#endif

/*****************************************************************/
/**
   Module ID
   Purpose: return the Module ID number (I) version (V)
   IRQ level (L) IRQ vector# (BB) 0xIIIIVLBB
*/
DWORD sis3801_module_ID(MVME_INTERFACE *mvme, DWORD base)
{
  DWORD id;

  mvme_set_am(mvme, MVME_AM_A24_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  id = mvme_read_value(mvme, base + SIS3801_MODULE_ID_RO);
  return (id);
}

/*****************************************************************/
/**
 */
void sis3801_module_reset(MVME_INTERFACE *mvme, DWORD base)
{
  
  mvme_set_am(mvme, MVME_AM_A24_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  mvme_write_value(mvme, base + SIS3801_MODULE_RESET_WO, 0x0);
  return;
}

/*****************************************************************/
/**
   DWORD                : 0xE(1)L(3)V(8)
*/
DWORD sis3801_IRQ_REG_read(MVME_INTERFACE *mvme, DWORD base)
{
  int   id;
  DWORD reg;

  mvme_set_am(mvme, MVME_AM_A24_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  id = mvme_read_value(mvme, base + SIS3801_IRQ_REG_RW);
  reg = (DWORD)id;
  return (reg & 0xFFF);
}


/*****************************************************************/
/**
   Purpose: write irq (ELV) to the register and read back
   DWORD                : 0xE(1)L(3)V(8)
*/
DWORD sis3801_IRQ_REG_write(MVME_INTERFACE *mvme, DWORD base, DWORD vector)
{
  DWORD reg;
  
  mvme_set_am(mvme, MVME_AM_A24_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  mvme_write_value(mvme, base + SIS3801_IRQ_REG_RW, (vector & 0xFF));
  reg = mvme_read_value(mvme, base + SIS3801_IRQ_REG_RW);
  return (reg & 0xFFF);
}

/*****************************************************************/
/**
   Purpose: Set input configuration mode
   DWORD mode          : Mode 0-4
*/
/************ INLINE function for General command ***********/
DWORD sis3801_input_mode(MVME_INTERFACE *mvme, DWORD base, DWORD mode)
{
  DWORD rmode;
  
  mvme_set_am(mvme, MVME_AM_A24_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  if (mode < 4)
    mode <<= 2;
  mvme_write_value(mvme, base + SIS3801_CSR_RW, mode);
  
  rmode = mvme_read_value(mvme, base + SIS3801_CSR_RW);
  return ((rmode & GET_MODE) >> 2);
}

/*****************************************************************/
/**
   Purpose: Set dwell time in us
   DWORD dwell         : dwell time in microseconds
*/
DWORD sis3801_dwell_time(MVME_INTERFACE *mvme, DWORD base, DWORD dwell)
{
  DWORD prescale;
  
  mvme_set_am(mvme, MVME_AM_A24_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  prescale = (10 * dwell ) - 1;
  if ((prescale > 0) && (prescale < 2<<24))
    mvme_write_value(mvme, base + SIS3801_PRESCALE_REG_RW, prescale);
  
  prescale = mvme_read_value(mvme, base + SIS3801_PRESCALE_REG_RW);
  
  return (prescale);
}

/*****************************************************************/
/**
   Purpose: Enable/Disable Reference on Channel 1
   DWORD endis         : action either SIS3801_ENABLE_REF_CH1_WO
   SIS3801_DISABLE_REF_CH1_WO
*/
int sis3801_ref1(MVME_INTERFACE *mvme, DWORD base, DWORD endis)
{
  DWORD csr;
  
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  if ((endis == SIS3801_ENABLE_REF_CH1_WO) || (endis == SIS3801_DISABLE_REF_CH1_WO))
    {
      mvme_write_value(mvme, base + endis, 0x0); 
    }
  else
    printf("sis3801_ref1: unknown command 0x%x\n",(unsigned int) endis);
  
  /* read back the status */
  csr = mvme_read_value(mvme, base + SIS3801_CSR_RW);
  
  return ((csr & IS_REF1) ? 1 : 0);
  
}

/*****************************************************************/
/**
 */
int sis3801_next_logic(MVME_INTERFACE *mvme, DWORD base, DWORD endis)
{
  DWORD csr;
  
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  if ((endis == SIS3801_ENABLE_NEXT_CLK_WO) || (endis == SIS3801_DISABLE_NEXT_CLK_WO))
    {
      mvme_write_value(mvme, base + endis, 0x0);
    }
  else
    printf("sis3801_next_logic: unknown command 0x%x\n",(unsigned int)endis);
  
  /* read back the status */
  csr = mvme_read_value(mvme, base + SIS3801_CSR_RW);

  
  return ((csr & IS_NEXT_LOGIC_ENABLE) ? 1 : 0);
}

/*****************************************************************/
/**
   Purpose: Enable nch channel for acquistion.
   blind command! 1..24 or 32
*/
void sis3801_channel_enable(MVME_INTERFACE *mvme, DWORD base, DWORD nch)
{
  
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
#ifdef HAVE_SIS3801E
  if (nch > 17) nch = 32; 
#else
  if (nch > 24) nch = 32;
#endif
  mvme_write_value(mvme, base + SIS3801_COPY_REG_WO, (1<<nch));

  
  return;
}

#ifdef HAVE_SIS3801E
/*****************************************************************/
/**
   Purpose: Write acquistion preset register
*/
void sis3801_write_preset(MVME_INTERFACE *mvme, DWORD base, DWORD data)
{
  // preset value 18 bits maximum. Bit 31 is enable acquisition preset mode.
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  data = data & 0x3FFFF;
  data = data | 0x80000000;
  mvme_write_value(mvme, base + SIS3801_ACQ_PRESET_W, data);
  printf("Wrote preset value of 0x%x\n",(unsigned int)data);
  return;
}

/**
   Purpose: Write acquistion preset register
*/
DWORD sis3801_read_acquisition_count(MVME_INTERFACE *mvme, DWORD base)
{
  DWORD data;

  // acquisition count value is 19 bits maximum.
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  data = mvme_read_value(mvme, base + SIS3801_ACQ_COUNT_R);
  return (data);
}

#endif //  HAVE_SIS3801E

/*****************************************************************/
/**
   Purpose: Read the CSR and return 1/0 based on what.
   except for what == CSR_FULL where it returns current CSR
*/
DWORD sis3801_CSR_read(MVME_INTERFACE *mvme, DWORD base, const DWORD what)
{
  DWORD csr;
  
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  if (what == CSR_FULL)
    {
      csr = mvme_read_value(mvme, base + SIS3801_CSR_RW);
    } else if (what == GET_MODE) {
      csr = mvme_read_value(mvme, base + SIS3801_CSR_RW);
      csr = ((csr & what) >> 2);
    } else {
      csr = mvme_read_value(mvme, base + SIS3801_CSR_RW);
      csr = ((csr & what) ? 1 : 0);
    }

  return (csr);
}

/*****************************************************************/
/**
   Purpose: Write to the CSR and return CSR_FULL.
*/
DWORD sis3801_CSR_write(MVME_INTERFACE *mvme, DWORD base, const DWORD what)
{
  int csr;
  
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  mvme_write_value(mvme, base + SIS3801_CSR_RW, what);
  
  csr =  sis3801_CSR_read(mvme, base, CSR_FULL);


  return (csr);
}

#ifdef MAIN_ENABLE
/*****************************************************************/
/**
   Purpose: test the  FIFO by writing to it
*/
void sis3801_FIFO_test(MVME_INTERFACE *mvme, DWORD base)
{
  DWORD value, data[125];
  int i;

  mvme_set_am(mvme, MVME_AM_A24_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D32);

  
  printf("Writing enable test bit to CSR\n\n");
  sis3801_CSR_write(mvme, base, ENABLE_FIFO_TEST);  /* enable test bit in CSR */
  printf("Wrote %d to CSR\n",ENABLE_TEST);

  SIS3801_Status(mvme,base);

 printf("\nClearing FIFO\n");
  sis3801_FIFO_clear(mvme, base);

 printf("\nEnabling next logic\n");
  sis3801_next_logic(mvme, base, SIS3801_ENABLE_NEXT_CLK_WO);

  printf("\nWriting 100 words to FIFO\n");
  for (i=0; i<100; i++)
    {  
      data[i]=0xABCDABCD +i ;
       mvme_write_value(mvme, base +  SIS3801_FIFO_WRITE_WO , data[i]);
    }
  printf("\nReading 100 words from FIFO\n");


 
  for (i=0; i<100; i++)
    {
      value =mvme_read_value(mvme, base +  SIS3801_FIFO_RO );
      printf("i=%d  Wrote 0x%x,  read back  0x%x",i,(unsigned int)data[i],(unsigned int)value);
      if(data[i] != value)
	printf(" values are different  !! \n");
      else
	printf("\n");
    }


  printf("\nWriting 0xFFFFFFFF to FIFO  (100 words)\n");
  for (i=0; i<100; i++)
    mvme_write_value(mvme, base +  SIS3801_FIFO_WRITE_WO , 0xFFFFFFFF);
    
  printf("\nReading 100 words from FIFO\n");

 
  for (i=0; i<100; i++)
    {
      value =mvme_read_value(mvme, base +  SIS3801_FIFO_RO );
      printf("i=%d  Wrote 0xFFFFFFFF,  read back  0x%x",i,(unsigned int)value);
      if(value != 0xFFFFFFFF)
	printf(" values are different  !! \n");
      else
	printf("\n");
    }
  return;
}

#endif // MAIN_ENABLE




/*****************************************************************/
/**
   Purpose: Clear FIFO and logic
*/
void sis3801_FIFO_clear(MVME_INTERFACE *mvme, DWORD base)
{
  
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  mvme_write_value(mvme, base + SIS3801_FIFO_CLEAR_WO, 0x0);
  

  return;
}

/*****************************************************************/
/**
   Purpose: Reads 32KB (8K DWORD) of the FIFO
   Function value:
   int                 : -1 FULL
   0 NOT 1/2 Full
   number of words read
*/
int sis3801_HFIFO_read(MVME_INTERFACE *mvme, DWORD base, DWORD * pfifo)
{
  int   i;
  
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  if (sis3801_CSR_read(mvme, base, IS_FIFO_FULL)) {
 
    return -1;
  }
  if (sis3801_CSR_read(mvme, base, IS_FIFO_HALF_FULL) == 0) {

    return 0;
  }

  for (i=0;i<HALF_FIFO;i++) {
      
    *pfifo++ = mvme_read_value(mvme, base + SIS3801_FIFO_RO);
  }

  return HALF_FIFO;
}

/*****************************************************************/
/**
   Purpose: Test and read FIFO until empty
   This is done using while, if the dwelling time is short
   relative to the readout time, the pfifo can be overrun.
   No test on the max read yet!
   Function value:
   int                 : -1 FULL
   number of words read
*/
int sis3801_FIFO_flush(MVME_INTERFACE *mvme, DWORD base, DWORD * pfifo)
{
  int counter=0;
  
  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  if (sis3801_CSR_read(mvme, base, IS_FIFO_FULL)) {
 
    return -1;
  }
  while ((sis3801_CSR_read(mvme, base, IS_FIFO_EMPTY) == 0) && (counter < MAX_FIFO_SIZE))
    {
      counter++;
      *pfifo++ = mvme_read_value(mvme, base + SIS3801_FIFO_RO);
      if(counter > 20)  // TEMP
	{
	printf("breaking from while loop with counter %d\n",counter);
	break;
	}
    }
  

  return counter;
}

/*****************************************************************/
/**
   Purpose: Enable the interrupt for the bitwise input intnum (0xff).
   The interrupt vector is then the VECTOR_BASE
*/
void sis3801_int_source_enable (MVME_INTERFACE *mvme, DWORD base, const int intnum)
{
  
  DWORD int_source;
  

  
  switch (intnum)
    {
    case  0:
      int_source = ENABLE_IRQ_CIP;
      break;
    case  1:
      int_source = ENABLE_IRQ_FULL;
      break;
    case  2:
      int_source = ENABLE_IRQ_HFULL;
      break;
    case  3:
      int_source = ENABLE_IRQ_ALFULL;
      break;
    default:
      printf("Unknown interrupt number (%d)\n",(int) intnum);
      return;
    }

  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  
  mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
  

  return;
}

/*****************************************************************/
/**
   Purpose: Enable the interrupt for the bitwise input intnum (0xff).
   The interrupt vector is then the VECTOR_BASE
*/
void sis3801_int_source_disable (MVME_INTERFACE *mvme, DWORD base, const int intnum)
{
  
  DWORD int_source;
  
  
  switch (intnum)
    {
    case  0:
      int_source = DISABLE_IRQ_CIP;
      break;
    case  1:
      int_source = DISABLE_IRQ_FULL;
      break;
    case  2:
      int_source = DISABLE_IRQ_HFULL;
      break;
    case  3:
      int_source = DISABLE_IRQ_ALFULL;
      break;
    default:
      printf("Unknown interrupt number (%d)\n",(int)intnum);
      return;
    }

  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);

  mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
  

  return;
}

/*****************************************************************/
/**
 Purpose: Enable the one of the 4 interrupt using the
          predefined parameters (see sis3801.h)
  DWORD * intnum          : interrupt number (input 0..3)
*/
void sis3801_int_source (MVME_INTERFACE *mvme, DWORD base, DWORD int_source)
{
  

  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);

  int_source &= (ENABLE_IRQ_CIP | ENABLE_IRQ_FULL
		| ENABLE_IRQ_HFULL | ENABLE_IRQ_ALFULL
		| DISABLE_IRQ_CIP  | DISABLE_IRQ_FULL
		| DISABLE_IRQ_HFULL| DISABLE_IRQ_ALFULL);
  mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);


}

/*****************************************************************/
/**
 Purpose: Book an ISR for a bitwise set of interrupt input (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:
  DWORD * base_addr      : base address of the sis3801
  DWORD base_vect        : base vector of the module
  int   level            : IRQ level (1..7)
  DWORD isr_routine      : interrupt routine pointer
*/
void sis3801_int_attach (MVME_INTERFACE *mvme, DWORD base, DWORD base_vect, int level, void (*isr)(void))
{

  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);

  /* disable all IRQ sources */
  sis3801_int_source(mvme, base
		     , DISABLE_IRQ_CIP | DISABLE_IRQ_FULL
		     | DISABLE_IRQ_HFULL | DISABLE_IRQ_ALFULL);

  printf("Not implemented for this OS\n");
}

/*****************************************************************/
/**
 Purpose: Unbook an ISR for a bitwise set of interrupt input (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:
  DWORD * base_addr       : base address of the sis3801
  DWORD base_vect        : base vector of the module
  int   level            : IRQ level (1..7)
*/
void sis3801_int_detach (MVME_INTERFACE *mvme, DWORD base, DWORD base_vect, int level)
{

  /* disable all IRQ sources */
  sis3801_int_source(mvme, base , DISABLE_IRQ_CIP
        | DISABLE_IRQ_FULL | DISABLE_IRQ_HFULL
	| DISABLE_IRQ_ALFULL);

  return;
}

/*****************************************************************/
/**
 Purpose: Disable the interrupt for the bitwise input intnum (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:
  DWORD * base_addr       : base address of the sis3801
  int   * intnum          : interrupt number (input 0..3)
*/
void sis3801_int_clear (MVME_INTERFACE *mvme, DWORD base, const int intnum)
{
  DWORD int_source;

  switch (intnum)
    {
    case  0:
      int_source = DISABLE_IRQ_CIP;
      mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
      int_source = ENABLE_IRQ_CIP;
      mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
      break;
    case  1:
      int_source = DISABLE_IRQ_FULL;
      mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
      int_source = ENABLE_IRQ_FULL;
      mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
      break;
    case  2:
      int_source = DISABLE_IRQ_HFULL;
      mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
      int_source = ENABLE_IRQ_HFULL;
      mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
      break;
    case  3:
      int_source = DISABLE_IRQ_ALFULL;
      mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
      int_source = ENABLE_IRQ_ALFULL;
      mvme_write_value(mvme, base + SIS3801_CSR_RW, int_source);
      break;
    default:
      printf("Unknown interrupt number (%d)\n",(int) intnum);
    }

  return;
}


/*****************************************************************/
/**
Sets all the necessary parameters for a given configuration.
The configuration is provided by the mode argument.
Add your own configuration in the case statement. Let me know
your setting if you want to include it in the distribution.
@param *mvme VME structure
@param  base Module base address
@param mode  Configuration mode number
@param *nentry number of entries requested and returned.
@return MVME_SUCCESS
*/
int  sis3801_Setup(MVME_INTERFACE *mvme, DWORD base, int mode)
{

  mvme_set_am(mvme, MVME_AM_A24_ND);;
  mvme_set_dmode(mvme, MVME_DMODE_D32);

  switch (mode) {
  case 0x1:
    printf("Default setting after power up (mode:%d)\n", mode);
    printf("...\n");
    printf("...\n");
    break;
  case 0x2:
    break;
  default:
    printf("Unknown setup mode\n");
    return -1;
  }

  return 0;
}

/*****************************************************************/
/**
 */
void SIS3801_Status(MVME_INTERFACE *mvme, DWORD base)
{
  DWORD csr;
  
  csr = sis3801_CSR_read(mvme, base, CSR_FULL);
  
  printf("Module Version    : %d\t", (int) ((sis3801_module_ID (mvme, base) & 0xf000) >> 12));
  printf("Module ID         : %4.4x\t", (unsigned int) (sis3801_module_ID (mvme, base) >> 16));
  printf("CSR contents      : 0x%8.8x\n", (unsigned int) csr);
  printf("LED               : %s \t",(csr &     IS_LED) ? "Y" : "N");
  printf("FIFO test mode    : %s \t",(csr &        0x2) ? "Y" : "N");
  printf("Input mode        : %d \n",  (int) sis3801_input_mode(mvme, base, 2));
  printf("25MHz test pulse  : %s \t",(csr &   IS_25MHZ) ? "Y" : "N");
  printf("Input test mode   : %s \t",(csr &    IS_TEST) ? "Y" : "N");
  printf("10MHz to LNE      : %s \t",(csr &  IS_102LNE) ? "Y" : "N");
  printf("LNE prescale      : %s \n",(csr &     IS_LNE) ? "Y" : "N");
  printf("Reference pulse 1 : %s \t",(csr &    IS_REF1) ? "Y" : "N");
  printf("Next Logic        : %s \n",(csr & IS_NEXT_LOGIC_ENABLE) ? "Y" : "N");
  printf("FIFO empty        : %s \t",(csr & IS_FIFO_EMPTY ) ? "Y" : "N");
  printf("FIFO almost empty : %s \t",(csr & IS_FIFO_ALMOST_EMPTY) ? "Y" : "N");
  printf("FIFO half full    : %s \t",(csr & IS_FIFO_HALF_FULL) ? "Y" : "N");
  printf("FIFO full         : %s \n",(csr & IS_FIFO_FULL) ? "Y" : "N");
  printf("External next     : %s \t",(csr & IS_EXTERN_NEXT) ? "Y" : "N");
  printf("External clear    : %s \t",(csr & IS_EXTERN_CLEAR) ? "Y" : "N");
  printf("External disable  : %s \t",(csr & IS_EXTERN_DISABLE) ? "Y" : "N");
  printf("Software counting : %s \n",(csr & IS_SOFT_COUNTING) ? "N" : "Y");
  printf("IRQ enable CIP    : %s \t",(csr & IS_IRQ_EN_CIP) ? "Y" : "N");
  printf("IRQ enable FULL   : %s \t",(csr & IS_IRQ_EN_FULL) ? "Y" : "N");
  printf("IRQ enable HFULL  : %s \t",(csr & IS_IRQ_EN_HFULL) ? "Y" : "N");
  printf("IRQ enable ALFULL : %s \n",(csr & IS_IRQ_EN_ALFULL) ? "Y" : "N");
  printf("IRQ CIP           : %s \t",(csr & IS_IRQ_CIP) ? "Y" : "N");
  printf("IRQ FIFO full     : %s \t",(csr & IS_IRQ_FULL) ? "Y" : "N");
  printf("IRQ FIFO 1/2 full : %s \t",(csr & IS_IRQ_HFULL) ? "Y" : "N");
  printf("IRQ FIFO almost F : %s \n",(csr & IS_IRQ_ALFULL) ? "Y" : "N");
  printf("internal VME IRQ  : %s \t",(csr &  0x4000000) ? "Y" : "N");
  printf("VME IRQ           : %s \n",(csr &  0x8000000) ? "Y" : "N");
}

/*****************************************************************/
/*-PAA- For test purpose only */

#ifdef MAIN_ENABLE

int main (int argc, char* argv[]) {
  
  int status;
  //  DWORD SIS3801_BASE = 0x780000;
  DWORD SIS3801_BASE = 0x000000;
  MVME_INTERFACE *mvme;
  int s;  
  char cmd[]="hallo";
  DWORD id;
  pfifo=fifo;
  dsis=0; // debug
  
  if (argc>1) {
    sscanf(argv[1],"%lx",&SIS3801_BASE);
  }
  
  // Test under vmic
  status = mvme_open(&mvme, 0);
  
  // Set am to A24 non-privileged Data
  mvme_set_am(mvme, MVME_AM_A24_ND);
  
  // Set dmode to D32
  mvme_set_dmode(mvme, MVME_DMODE_D32);

  
  while ( isalpha(cmd[0]) ||  isdigit(cmd[0]) )
    {
      printf("\nEnter command (A-Z) X to exit?  ");
      scanf("%s",cmd);
      //  printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];

      switch(s)
	{
	case ('A'):  // 
	  {
	    id=sis3801_module_ID(mvme,SIS3801_BASE );
            printf("\nModule ID=0x%x\n",(unsigned int)id);
	    break;
	  }


	case ('B'):   //  test 1 (LED)
	  {
	    test1(mvme, SIS3801_BASE);
            break;
          }

        case ('C'):    //  test 2 (write to FIFO and read back)
	  {
	    test2(mvme, SIS3801_BASE);
            break;
          }

#ifdef  HAVE_SIS3801E

	case ('E'):  //  test 3 (set into test mode and read data)
	  {
	    test3(mvme, SIS3801_BASE);
	    break;
	  }


        case ('F'):   //  test 3 (write to FIFO and read back)
	  {
	    id= ( sis3801_module_ID(mvme,SIS3801_BASE ) & 0x0000F000) >>12 ;
	    if(id==13 || id==14)
	      test4(mvme, SIS3801_BASE);
	    else
	      printf("Firmware version %d does not support preset\n",(int)id);
            break;
          }

        case ('G'):   // Test 5  as test 3 except 3 scans
	  {
	    id= ( sis3801_module_ID(mvme,SIS3801_BASE ) & 0x0000F000) >>12 ;
	    if(id==13 || id==14)
	       test5(mvme, SIS3801_BASE);
	    else
	      printf("Firmware version %d does not support preset\n",(int)id);
            break;
          }
#endif //   HAVE_SIS3801E


	case ('P'):  // Print list
	  {
	    sis3801();
	    break;
	  }

	case ('R'):  // Reset
	  {
	    sis3801_module_reset(mvme, SIS3801_BASE );
	    break;
	  }


        case ('S'):   //  status
	  {
	    SIS3801_Status(mvme, SIS3801_BASE);
            break;
          }




	case ('X'): 
	  {
	    return(SUCCESS);
	    break;
	  }

	default:
	  {
	    printf("Unknown command. Enter \"P\" for list of commands\n");
	    break;
	  }
	} 
    }
  status = mvme_close(mvme);
  return SUCCESS;
}

void sis3801(void)
{
  printf("\nsis3801 commands: \n");
  printf("A module ID      \n");  
  printf("B test1 cycle LEDs \n");
  printf("C test2 write to FIFO and read back\n");

#ifdef HAVE_SIS3801E
  printf("E test3 one test cycle (using preset)\n");
  printf("F test4 one test cycle (using preset) \n");
  printf("G test5 multiple test cycles (using preset)\n");       
#endif //  HAVE_SIS3801E
  printf("P print this list            R reset module\n");
  printf("S status          \n");      
  printf("X quit\n");
 
}


void test1 (MVME_INTERFACE *mvme, DWORD base)
{
  int i;

  sis3801_module_reset(mvme,  base);
  sleep(1);

  sis3801_module_ID(mvme, base);

  printf("ID:%x\n", (unsigned int) sis3801_module_ID(mvme, base));
  SIS3801_Status(mvme, base);
  
  printf("\nCycling front panel LED...\n");
  for(i=0;i<4;i++) {
    sis3801_CSR_write(mvme, base, LED_ON);
    usleep(500000);
    sis3801_CSR_write(mvme, base, LED_OFF);
    usleep(500000);
  }
  return;
}
  

// write to FIFO and read back
void test2 (MVME_INTERFACE *mvme, DWORD base)
{  
  int i, nwords;

  sis3801_module_reset(mvme,  base);
  sleep(1);
  printf("\n Disabling next logic\n");
  sis3801_next_logic(mvme, base, SIS3801_DISABLE_NEXT_CLK_WO);
  printf("\nClearing FIFO\n");
  sis3801_FIFO_clear(mvme, base);
  
  // Try writing to and reading back from FIFO
  
  sis3801_FIFO_test(mvme,base);
  

  if (sis3801_CSR_read(mvme, base, IS_FIFO_EMPTY) == 0)
    {
      printf("sis3801_read says module is not empty\n");
      nwords = sis3801_FIFO_flush(mvme, base, pfifo); /* get the data */
      if(nwords < 1) 
	printf("sis3801_flush says module is empty\n");
      else
	{ 
	  printf("Module contains %d words\n", nwords);
	  if (nwords > 25)
	    nwords=25;
	  
	  for (i=0; i<nwords; i++)
	    printf("i=%d   pfifo[i]=0x%x\n",i,  (unsigned int)pfifo[i]);
	}	
    }
  else
    printf("sis3801_read says module is empty\n");
  // }
  

  sis3801_module_reset(mvme,  base);
  sleep(1);
  printf("\n Disabling next logic\n");
  sis3801_next_logic(mvme, base, SIS3801_DISABLE_NEXT_CLK_WO);
  printf("\nClearing FIFO\n");
  sis3801_FIFO_clear(mvme, base);
  
  return ;
}


   
#ifdef HAVE_SIS3801E

// These tests need code to be compiled with HAVE_SIS3801E (Firmware versions D,E hex) 


void test3 (MVME_INTERFACE *mvme, DWORD base)
{
  DWORD data,nbins;

  int nwords;
  int i,j;
    
  SIS3801_Status(mvme,  base);
  
  
  printf("\nSetting up SIS3801 version 0x%x at base address 0x%x\n",
	 (unsigned int) ((sis3801_module_ID (mvme,base ) & 0xf000) >> 12),(unsigned int)base);
   printf("Lowest 4 channels enabled; 10 time bins\n\n");
  
  data = 4 ;// disable all except lowest 4 channels 
  sis3801_channel_enable(mvme,  base, data);
  
  nbins=10;
 

         // write 1 less bin to preset reg
  sis3801_write_preset(mvme,  base, (nbins-1)); // set number of acquisitions (LNE or bins)
  sis3801_ref1(mvme, base, SIS3801_ENABLE_REF_CH1_WO);
  
  sis3801_input_mode(mvme,base, 0);            /* SIS input mode 0 */
  sis3801_dwell_time(mvme,base, 1000000);      /* 1s */
  sis3801_CSR_write(mvme,base, DISABLE_EXTERN_NEXT);
  sis3801_CSR_write(mvme,base, DISABLE_EXTERN_DISABLE);
  sis3801_CSR_write(mvme,base, ENABLE_102LNE);
  sis3801_CSR_write(mvme,base, ENABLE_LNE);
  sis3801_CSR_write(mvme,base, ENABLE_TEST);  /* enable test bit */
  sis3801_CSR_write(mvme,base, ENABLE_25MHZ); /* enable 25MHZ test pulser */
  
  SIS3801_Status(mvme,  base);



  printf("\nClearing FIFO\n");
  sis3801_FIFO_clear(mvme, base);
  
  printf("\nEnabling next logic\n");
  sis3801_next_logic(mvme, base, SIS3801_ENABLE_NEXT_CLK_WO);
  
  sleep(1);
    
  j=0;
  while (j< 10)  
    {    
      if (sis3801_CSR_read(mvme, base, IS_FIFO_EMPTY) == 0)
	{
          printf("sis3801_read says module is not empty\n");
          nwords = sis3801_FIFO_flush(mvme, base, pfifo); /* get the data */
	  if(nwords < 1) 
	    printf("sis3801_flush says module is empty\n");
	  else
	    { 
	      printf("Module contains %d words\n", nwords);
	      if (nwords > 25)
		nwords=25;
              
	      for (i=0; i<nwords; i++)
		printf("i=%d   pfifo[i]=0x%x\n",i, (unsigned int)pfifo[i]);
	    }	
	}
      else
        printf("sis3801_read says module is empty\n");
    
      
      sleep(2);
      j++;  
    } // while
  printf("\n Resetting module\n");     
  sis3801_module_reset(mvme,  base);
  sleep(1);
  printf("\n Disabling next logic\n");
  sis3801_next_logic(mvme, base, SIS3801_DISABLE_NEXT_CLK_WO);
  printf("\nClearing FIFO\n");
  sis3801_FIFO_clear(mvme, base);

  return;
}
  

void test4 (MVME_INTERFACE *mvme, DWORD base)
{ 
  DWORD data,nbins;
  int bincount,oldbincount;
  int total,nwords;
  int i;
  
  SIS3801_Status(mvme,  base);
  
  printf("\nSetting up SIS3801 version 0x%x at base address 0x%x\n", 
	 (unsigned int)((sis3801_module_ID (mvme,base ) & 0xf000) >> 12), (unsigned int) base);
    printf("Lowest 4 channels enabled; 10 time bins\n\n");
  
  data = 4 ;// disable all except lowest 4 channels 
  sis3801_channel_enable(mvme,  base, data);
  
  nbins=10;

  printf("Writing %d to preset register\n",(int)(nbins-1)); // write one less bin to preset reg
  sis3801_write_preset(mvme,  base, (nbins-1)); // set number of acquisitions (LNE or bins)
  
  
  sis3801_ref1(mvme, base, SIS3801_ENABLE_REF_CH1_WO);
  sis3801_input_mode(mvme,base, 0);            /* SIS input mode 0 */
  sis3801_dwell_time(mvme,base, 1000000);      /* 1s */
  sis3801_CSR_write(mvme,base, DISABLE_EXTERN_NEXT);
  sis3801_CSR_write(mvme,base, DISABLE_EXTERN_DISABLE);
  sis3801_CSR_write(mvme,base, ENABLE_102LNE);
  sis3801_CSR_write(mvme,base, ENABLE_LNE);
  sis3801_CSR_write(mvme,base, ENABLE_TEST);  /* enable test bit */
  sis3801_CSR_write(mvme,base, ENABLE_25MHZ); /* enable 25MHZ test pulser */
  
  SIS3801_Status(mvme,  base);
  
  printf("\nClearing FIFO\n");
  sis3801_FIFO_clear(mvme, base);
  
  printf("\nEnabling next logic\n");
  sis3801_next_logic(mvme, base, SIS3801_ENABLE_NEXT_CLK_WO);
  
  sleep(1);

  bincount=0;
  oldbincount=0;
  total=0;

  while (bincount!=nbins) 
    {
      sleep(1);
      bincount =     sis3801_read_acquisition_count(mvme, base);
      printf("acquisition count = %d\n",bincount);
      

      if(bincount!=oldbincount)
	{
	  oldbincount=bincount;
	  printf("\nMCS mode, bin %d completed\n",bincount);
	  	 
	  if (sis3801_CSR_read(mvme, base, IS_FIFO_EMPTY) == 0)
	    {
	      printf("sis3801_read says module is not empty\n");
	      nwords = sis3801_FIFO_flush(mvme, base, pfifo); /* get the data */
	      if(nwords < 1) 
		printf("sis3801_flush says module is empty\n");
	      else
		{ 
		  printf("Module contains %d words\n", nwords);
		  if (nwords > 25)
		    nwords=25;
		  
		  for (i=0; i<nwords; i++)
		    printf("i=%d   pfifo[i]=0x%x\n",i,  (unsigned int)pfifo[i]);
		}	
	    }
	  else
	    printf("sis3801_read says module is empty\n");
	}
      
    }   // end of bincount != nbins

  sleep(2);
  sis3801_module_reset(mvme,  base);
  sleep(1);
  printf("\n Disabling next logic\n");
  sis3801_next_logic(mvme, base, SIS3801_DISABLE_NEXT_CLK_WO);
  printf("\nClearing FIFO\n");
  sis3801_FIFO_clear(mvme, base);

  return;    
}
     




void test5 (MVME_INTERFACE *mvme, DWORD base)
{ 


  DWORD data,nbins;
  int bincount,oldbincount;
  int total,nwords;
  int i,num_scans;
  
  printf("test 5 \n");
  //  SIS3801_Status(mvme,  base);
  
  printf("\nSetting up SIS3801 version 0x%x at base address 0x%x\n", 
	 (unsigned int)((sis3801_module_ID (mvme,base ) & 0xf000) >> 12), (unsigned int) base);
    printf("Lowest 4 channels enabled; 10 time bins and 3 scans (i.e. 3 cycles of 10 bins) \n\n");

  data = 4 ;// disable all except lowest 4 channels 
  sis3801_channel_enable(mvme,  base, data);
  
  nbins=10;

  printf("Writing %d to preset register\n",(int)(nbins-1)); // write 1 less bin to preset reg
  sis3801_write_preset(mvme,  base, (nbins-1)); // set number of acquisitions (LNE or bins)
  
  
  sis3801_ref1(mvme, base, SIS3801_ENABLE_REF_CH1_WO);
  sis3801_input_mode(mvme,base, 0);            /* SIS input mode 0 */
  sis3801_dwell_time(mvme,base, 1000000);      /* 1s */
  sis3801_CSR_write(mvme,base, DISABLE_EXTERN_NEXT);
  sis3801_CSR_write(mvme,base, DISABLE_EXTERN_DISABLE);
  sis3801_CSR_write(mvme,base, ENABLE_102LNE);
  sis3801_CSR_write(mvme,base, ENABLE_LNE);
  sis3801_CSR_write(mvme,base, ENABLE_TEST);  /* enable test bit */
  sis3801_CSR_write(mvme,base, ENABLE_25MHZ); /* enable 25MHZ test pulser */
  
  SIS3801_Status(mvme,  base);
  num_scans=0;
  while (num_scans < 3)
    {
      printf("\n\nScan %d starting...\n",(num_scans+1));
      bincount =     sis3801_read_acquisition_count(mvme, base);
      printf("acquisition count= %d\n",bincount);
      
      printf("\nClearing FIFO\n");
      sis3801_FIFO_clear(mvme, base);
      bincount =     sis3801_read_acquisition_count(mvme, base);
      printf("acquisition count= %d\n",bincount);
     
      printf("\nEnabling next logic\n");
      sis3801_next_logic(mvme, base, SIS3801_ENABLE_NEXT_CLK_WO);
      bincount =     sis3801_read_acquisition_count(mvme, base);
      printf("acquisition count= %d\n",bincount);
  

      sleep(1);
      bincount =     sis3801_read_acquisition_count(mvme, base);
      printf("acquisition count= %d\n",bincount);

      bincount=0;
      oldbincount=0;
      total=0;

      while (bincount!=nbins) 
	{
	  sleep(1);
	  bincount =     sis3801_read_acquisition_count(mvme, base);
	
	  if(bincount!=oldbincount)
	    printf("acquisition bin count = %d\n",bincount);
	  else
	    printf("...waiting for next LNE\n");
	  oldbincount=bincount;
	}

      printf("\nMCS mode, %d bins completed\n",bincount);
	    

      while (sis3801_CSR_read(mvme, base, IS_FIFO_EMPTY) == 0)
	{
	  printf("sis3801_read says module is not empty\n");
	  nwords = sis3801_FIFO_flush(mvme, base, pfifo); /* get the data */
	  if(nwords < 1) 
	    printf("sis3801_flush says module is empty\n");
	  else
	    { 
	      printf("Module contains %d words\n", nwords);
	      
	      
	      for (i=0; i<nwords; i++)
		printf("i=%d   pfifo[i]=0x%x\n",i,  (unsigned int)pfifo[i]);
	    }
	}
	       
	
	printf("sis3801_read says module is empty\n");
    
      

      printf("\nEnd of scan %d\n",num_scans);
      sleep(2);
      
      bincount =     sis3801_read_acquisition_count(mvme, base);
      printf("acquisition count= %d\n",bincount);
 
      num_scans++;
    } // while num_scans < 3
  sleep(1);
  sis3801_module_reset(mvme,  base);
  sleep(1);
  printf("\n Disabling next logic\n");
  sis3801_next_logic(mvme, base, SIS3801_DISABLE_NEXT_CLK_WO);
  printf("\nClearing FIFO\n");
  sis3801_FIFO_clear(mvme, base);
  return;    
}
#endif //  HAVE_SIS3801E

#endif // MAIN_ENABLE 
/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */
