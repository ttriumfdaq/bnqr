 echo "VME Reset and wait 2s"
  vme_poke -a VME_A24UD -A 0x0080902E -d VME_D8 0x1
  sleep 2
 echo "load idle frequency 250000Hz in BCD"
 vme_poke -a VME_A24UD -A 0x00808FFC -d VME_D16 0x51
 vme_poke -a VME_A24UD -A 0x00808FFE -d VME_D16 0xeb85
 echo "strobe"
 vme_poke -a VME_A24UD -A 0x0080902A -d VME_D8 0x1

 echo "load tuning frequency 500000Hz in BCD into FREF"
 vme_poke -a VME_A24UD -A 0x00809020 -d VME_D16 0xa3
 vme_poke -a VME_A24UD -A 0x00809022 -d VME_D16 0xd70a
 echo "load i and q  into idle for 1f  511->0x201 -511=0x1FF"

 echo "For 1f and fREF, i=511,q=0 for 3f q=511,i=0 for 5f i=-511 q=0"
 vme_poke -a VME_A24UD -A 0x00801FFC -d VME_D16 0x201
 vme_poke -a VME_A24UD -A 0x00801FFE -d VME_D16 0x0

 echo "load i and q  into idle for 3f"
 vme_poke -a VME_A24UD -A 0x00803FFC -d VME_D16 0x0
 vme_poke -a VME_A24UD -A 0x00803FFE -d VME_D16 0x201

 echo "load i and q  into idle for 5f"
 vme_poke -a VME_A24UD -A 0x00805FFC -d VME_D16 0x1FF
 vme_poke -a VME_A24UD -A 0x00805FFE -d VME_D16 0x0

 echo "load i and q  into idle for fREF"
 vme_poke -a VME_A24UD -A 0x00807FFC -d VME_D16 0x201
 vme_poke -a VME_A24UD -A 0x00807FFE -d VME_D16 0x0



 #echo "set single tone mode"
 #vme_poke -a VME_A24UD -A 0x00809001 -d VME_D8 0x1
 #vme_poke -a VME_A24UD -A 0x00809009 -d VME_D8 0x1
 #vme_poke -a VME_A24UD -A 0x00809011 -d VME_D8 0x1
 #vme_poke -a VME_A24UD -A 0x00809019 -d VME_D8 0x1

 echo "set quad mode"
 vme_poke -a VME_A24UD -A 0x00809001 -d VME_D8 0x0
 vme_poke -a VME_A24UD -A 0x00809009 -d VME_D8 0x0
 vme_poke -a VME_A24UD -A 0x00809011 -d VME_D8 0x0
 vme_poke -a VME_A24UD -A 0x00809019 -d VME_D8 0x0

 echo "set default gate"
 vme_poke -a VME_A24UD -A 0x0080902C -d VME_D8 0x55

