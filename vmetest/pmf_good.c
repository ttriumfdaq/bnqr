#include <stdio.h>
#include <string.h>
#include <stdlib.h>

FILE  *ppginput;
FILE  *ppgoutput;
int start_pc,num_instructions;
typedef struct
{
    unsigned int    pc;
    unsigned long   setpat;
    unsigned long   clrpat;
    unsigned long   delay;
    unsigned long   ins_data; // instruction and data
}COMMAND;

typedef struct
{
  char            name[80]; // name of variable, e.g. "dwell time"
  int             index; // instruction number in the file
  unsigned long   value;     // value to set instruction to
  char            data_type[15] ; // data type (one of "delay" "loopcount" ...)
} FILE_VAR;

// prototypes
int ppg_modify_file(char * infile, char * outfile, FILE_VAR * file_var);
void  lineWrite (COMMAND *data_struct);
int getinsline(char *line, int max, FILE *file);
COMMAND lineRead(char *line);

int main(void)
{
  char filename[]="/home/pol/vmetest1/ppgtemplate.dat";
  char outfile[]="/home/pol/vmetest1/ppgload.dat";
  FILE_VAR file_var;
  int status;
  sprintf(file_var.name,"cycle loop count");
  file_var.index = 2;
  file_var.value = 1000;
  sprintf(file_var.data_type,"loopcount");


  status = ppg_modify_file(filename, outfile, &file_var);
  if (status ==0)
    printf("success form ppg_modify_file\n");
}


int ppg_modify_file(char * infile, char * outfile, FILE_VAR * file_var)
{
  /*  Local Variables  */

  COMMAND  command_info;
  char  line[100];
  //int  cmode, localam;
  int linenum;
  char *p;
  int header_flag;
  int counter;
  unsigned long data;
  unsigned long temp;


  //  New PPG
  //  SET bits:0-31    |  CLR bits 32-63  | Delay Count:64-95  | Delay Count: 96-115 | Instruction 116-119 |  120-127
  //        32 bits    |        32 bits   |    32 bits         |    32 bits          |     4 bits          |  ignored


  printf("\nppg_modify_file: input parameters\n");
  printf("name:\"%s\"\n",file_var->name);
  printf("pc:%3.3d\n",file_var->index);
  printf("value:0x%x or %d \n",file_var->value,file_var->value);
  printf("data type:\"%s\"\n",file_var->data_type);


  start_pc = num_instructions = 0; // globals


  printf("Opening ppg template file: %s   ...  \n",infile);
  ppginput = fopen(infile,"r");
  sleep(1) ; // sleep 1s
  if(ppginput == NULL){
    printf("ppgLoad: ppg load file %s could not be opened. [%p]\n", infile, ppginput);
    return -1;
  }

  start_pc = num_instructions = 0; // globals
  printf("Opening ppg output file: %s   ...  \n",outfile);
  ppgoutput = fopen(outfile,"w");
  sleep(1) ; // sleep 1s
  if(ppgoutput == NULL){
    printf("ppgLoad: ppg load file %s could not be opened. [%p]\n", outfile, ppgoutput);
    return -1;
  }
  fprintf(ppgoutput,"# PPGLOAD file modified from template %s\n",infile); // write line to output file
  fprintf(ppgoutput,"# PC %d %s set to 0x%x (%d) for %s \n",file_var->index,file_var->data_type,
	  file_var->value,file_var->value,  file_var->name);
  linenum=counter=0;
  header_flag=1;
 
  // Find number of instructions
  while (getinsline(line,99,ppginput) > 0)
    {        
      linenum++;
      printf("File line %d: %s\n",linenum,line);
      if(header_flag)
	{
	  fprintf(ppgoutput,"%s",line); // write line to output file
	  if ( strncmp(line,"#",1) !=0 ) // skip comments at begin of file
	    {
	      if ( strncmp(line,"Num Instruction Lines =",23) ==0 ) // first line after comments
		{
		  p=line + 23;  // skip "Instruction Lines "
		  char *q;
		  q =strchr(p,'#');
		  if(q != 0)
		    *q='\0';
		  printf("Now line is %s\n",p);
		  sscanf(p,"%d",&num_instructions);
		  printf("instruction lines = %d\n",num_instructions);  
		  //printf("Base Address = %x\n",base_adr);
		  header_flag=0; // we have processed header
		}
	    }
	}
      else
	{ // instructions
	  command_info = lineRead(line);
	  counter++;
	  if (counter==1)
	    start_pc =  command_info.pc ; // remember where program starts (usually 0)
	  //	  if (ddd)
	  // {
	      printf("File line %d; instruction line %d :\n",linenum,counter);
	      printf("pc:%3.3d ",command_info.pc);
	      printf("set bitpat:%8.8lx ",command_info.setpat);
	      printf("clr bitpat:%8.8lx ",command_info.clrpat);
	      printf("delay:%8.8lx\n",command_info.delay);
	      printf("ins/data:%8.8lx\n",command_info.ins_data);
	      //  }

	  if(counter == file_var->index)
	    { // this line needs to be modified
	      if(strcmp(file_var->data_type,"loopcount")==0)
		{
		  temp = command_info.ins_data &  0xF00000 ;
		  temp = temp |  file_var->value;
		  printf("ins/data for instruction number %d (i.e. %s) changed from 0x%x to 0x%x\n",
			 counter, file_var->name, command_info.ins_data,temp);
		  command_info.ins_data = temp;
		}
	      else if (strcmp(file_var->data_type,"delay")==0)
		{
		  printf("ins/data for instruction number %d (i.e. %s) changed from 0x%x to 0x%x\n",
			 counter, file_var->name, command_info.delay, file_var->value);
		  command_info.delay = file_var->value;
		}
	      lineWrite( &command_info); // write modified line to output file
	    }
	  else
	    fprintf(ppgoutput,"%s",line); // write line to output file
	  // load this instruction
	  // data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , command_info.pc ); // write instruction address 
	  // data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_LO  , command_info.setpat); // "set" bits (32 bits)
	  // data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_MED  ,command_info.clrpat );// "clear" bits (32 bits)
	  // data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_HI  , command_info.delay);// delay count
	  // data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_TOP  , command_info.ins_data);// instruction and any data
	}
    } // end of while


  if(header_flag > 0)
    {
      printf("TPPGLoad: incorrect format for ppg load file. Did not find number of instruction lines\n");
      return -1;
    }

  if(counter != num_instructions)
      printf("TPPGLoad: Warning - stated number of instructions lines does not agree with actual number of instruction lines\n");
  
  

  // Set instruction address back to start of program
  // data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , start_pc ); // write start instruction address 
  
  //mvme_set_dmode(mvme, cmode); // restore VME data mode
  //mvme_set_am(mvme, localam); // restore VME addressing mode
    
  fclose(ppginput);
  ///if(ddd) printf("Programming ended, PPG ready");
  return 1;
}

/*------------------------------------------------------------------*/
/** lineRead
    Read line of input file
    @memo read line.
    @param *input char* line  line of input file
    @return COMMAND data structure
*/
COMMAND lineRead(char *line)
{
  COMMAND data_struct;
  printf ("lineRead: line=\"%s\n",line);
  sscanf(line,"%u %lx %lx %lx %lx", &(data_struct.pc), &(data_struct.setpat),
	 &(data_struct.clrpat), &(data_struct.delay), &(data_struct.ins_data));
  return(data_struct);
}


void  lineWrite( COMMAND  *command_info)
{
  char line[128];
  printf("lineWrite input parameters:\n");
  printf("pc:%3.3d\n",command_info->pc);
  printf("set bitpat:%8.8lx\n",command_info->setpat);
  printf("clr bitpat:%8.8lx\n",command_info->clrpat);
  printf("delay:%8.8lx\n",command_info->delay);
  printf("ins/data:%8.8lx\n",command_info->ins_data);	    
  // sprintf(line,"%3.3u %lx %lx %lx %lx", command_info->pc, command_info->setpat,
  //  command_info->clrpat, command_info->delay, command_info->ins_data);
  sprintf (line,"%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x",
	   command_info->pc, command_info->setpat,
	   command_info->clrpat, command_info->delay, command_info->ins_data);

  printf ("lineWrite: line=\"%s\n",line);
  fprintf(ppgoutput,"%s\n",line);
  return;
}


/** getinsline
    Read line of input instruction file
    @memo read line.
    @param *input char* line buffer to contain line of input file
    @param *input int max  integer maximum characters in line
    @param *input FILE *file file handle
    @return COMMAND data structure
*/
/* getinsline */
int getinsline(char *line, int max, FILE *file)
{
  if (fgets(line,max,file) == NULL)
    return 0;
  else
    return strlen(line);
} 
