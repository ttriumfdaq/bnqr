/*
===============================================================================
                            COPYRIGHT NOTICE

    Copyright (C) 2001-2002 VMIC
    International Copyright Secured.  All Rights Reserved.

-------------------------------------------------------------------------------
    Description: VMEbus master window read using memory-mapped registers
                 example and utility
-------------------------------------------------------------------------------

===============================================================================
*/


#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <vme/vme.h>
#include <vme/vme_api.h>
#include "vme_test.h"

#define DEBUG 1
#ifdef DEBUG
#define DPRINTF(x...)  fprintf(stderr, x)
#else
#define DPRINTF(x...)
#endif


int                             /* 0 on success, -1 on error */
main (int argc, char **argv)
{
  vme_bus_handle_t bus_handle;
  vme_master_handle_t handle = NULL;
  uint64_t vme_addr;
  int am, dw, flags, use_memcpy, c, rval;
  void *phys_addr = NULL, *buffer = NULL, *addr = NULL;
  size_t nelem, size;

  /* Set up default values
   */
  vme_addr = 0;
  am = VME_A32SD;
  dw = VME_D8;
  flags = VME_CTL_PWEN;
  nelem = 1;
  phys_addr = NULL;
  use_memcpy = 1;
  rval = 0;

  /* Parse the argument list
   */
  while (-1 != (c = getopt (argc, argv, "a:A:d:e:f:p")))
    switch (c)
      {
      case 'a':                /* Address modifier */
        if (0 > strtoam (optarg, &am))
          {
            fprintf (stderr, "Invalid address modifier\n");
            return (-1);
          }
        break;
      case 'A':                /* VMEbus address */
        vme_addr = strtoul (optarg, NULL, 0);
        break;
      case 'd':                /* VMEbus access data width */
        if (0 > strtodw (optarg, &dw))
          {
            fprintf (stderr, "Invalid access data width\n");
            return (-1);
          }
        /* If a specific data width was requested, then don't use memcpy. */
        use_memcpy = 0;
        break;
      case 'e':                /* Number of elements */
        nelem = strtoul (optarg, NULL, 0);
        break;
      case 'f':                /* Flags */
        if (0 > strtoflags (optarg, &flags))
          {
            fprintf (stderr, "Invalid flags\n");
            return (-1);
          }
        break;
      case 'p':                /* Physical address */
        phys_addr = (void *) strtoul (optarg, NULL, 0);
        break;
      default:
        fprintf (stderr,
                 "USAGE: vme_peek [-a addr_mod] [-A vme_addr][-d dwidth]"
                 "[-e num_elem][-f flags][-p phys_addr]");
        return (-1);
      }

  /* Use number of elements and data width to calculate the size.
   */
  size = nelem * dw;

  /* Initialize
   */
  if (0 > vme_init (&bus_handle))
    {
      perror ("vme_init");
      return (-1);
    }

  /* Create a handle to the necessary window
   */
  if (0 >
      vme_master_window_create (bus_handle, &handle, vme_addr, am, size,
                                flags, phys_addr))
    {
      perror ("vme_master_window_create");
      rval = -1;
      goto ERROR;
    }

  /* This is not necessary, I just put this function here to test it and
     demonstrate it's use.
   */
  if (NULL == (phys_addr = vme_master_window_phys_addr (bus_handle, handle)))
    {
      perror ("vme_master_window_phys_addr");
    }

  DPRINTF ("VME_PEEK: vme_addr=0x%lx - ", (unsigned long) vme_addr);
  DPRINTF ("am=0x%x - ", am);
  DPRINTF ("dw=0x%x - ", dw);
  DPRINTF ("size=0x%x - ", size);
  DPRINTF ("flags=0x%x - ", flags);
  DPRINTF ("WPA=0x%lx - ", (unsigned long) phys_addr);
  DPRINTF ("Data ->> ");

  /* Map in the window
   */
  if (NULL == (addr = vme_master_window_map (bus_handle, handle, 0)))
    {
      perror ("vme_master_window_map");
      rval = -1;
      goto ERROR;
    }

  /* Create a temporary buffer to copy data into before printing it so 
     performance measurements won't account for local I/O.
   */
  if (NULL == (buffer = malloc (size)))
    {
      perror ("malloc");
      rval = -1;
      goto ERROR;
    }

  /* Do the transfer. If data width was not given at the command line, then
     use memcpy for fast transfers. Note that this may cause the output to be
     byte-swapped.
   */
  if (use_memcpy)
    {
      if (0 > memcpy (buffer, addr, size))
        {
          perror ("memcpy");
          rval = -1;
          goto ERROR;
        }
    }
  else
    {
      if (0 > vmemcpy (buffer, addr, nelem, dw))
        {
          perror ("vmemcpy");
          rval = -1;
          goto ERROR;
        }
    }

  /* Print data to stdout.
   */
  if (0 > vdump (buffer, nelem, dw))
    {
      perror ("vdump");
      rval = -1;
      goto ERROR;
    }

  /* Clean everything up and exit
   */
ERROR:                         /* Jump to here if an error occurs */

  free (buffer);

  if (addr)
    if (0 > vme_master_window_unmap (bus_handle, handle))
      {
        perror ("vme_master_window_unmap");
        rval = -1;
      }

  if (handle)
    if (0 > vme_master_window_release (bus_handle, handle))
      {
        perror ("vme_master_window_release");
        rval = -1;
      }

  if (0 > vme_term (bus_handle))
    {
      perror ("vme_term");
      rval = -1;
    }

  return (rval);
}
