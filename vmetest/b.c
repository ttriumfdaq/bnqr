#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#define FAILURE -1  // SUCCESS 1 defined in trPSM.h
#define SUCCESS 1
#define TRUE 1
#define FALSE 0
typedef int                INT;

INT get_index(char *p_profile_code, INT *pindex);

int main(void)
{
  INT nprofile;
  char profile_code[]="1f";
  char *p_profile_code;
  p_profile_code=profile_code;
  
  if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;
  else
    printf("profile code %s is %d\n",p_profile_code, nprofile);
} 

INT get_index(char *p_profile_code, INT *pindex)
  /* get the array index depending on profile (1f,3f,5f or fREF) 
   */
{
  p_profile_code[0]=tolower(p_profile_code[0]);
   printf("get_index: starting with my_profile_code=%s\n",p_profile_code); 
  switch (p_profile_code[0])
    {
    case '1':
      *pindex=0;
      break;
    case '3':
      *pindex=1;
      break;
    case '5':
      *pindex=2;
      break;            
    case 'f':
      *pindex=3;
      break;
    default:
      printf("Register profile (%s) must be one of 1f,3f,5f or fREF\n",p_profile_code);
      return (FAILURE);
    }
  return(SUCCESS);
}
