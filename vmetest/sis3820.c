
/*********************************************************************

  Name:         sis3820.c
  Created by:   K.Olchanski

  Contents:     SIS3820 32-channel 32-bit multiscaler
                
  $Log$
  Revision 1.1  2006/05/25 05:53:42  alpha
  First commit

*********************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h> // pow
#include "sis3820drv.h"
//#include "sis3820.h"  included by sis3820drv.h
//#include "mvmestd.h"
#include "unistd.h" // for sleep
/*****************************************************************/

uint32_t write_reg (MVME_INTERFACE *myvme,  DWORD SIS3820_BASE,  uint32_t register, uint32_t data );
void sis3820_CSR0(MVME_INTERFACE *mvme, DWORD base);

/*
Read sis3820 register value
*/
static uint32_t regRead(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A32);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  return mvme_read_value(mvme, base + offset);
}

/*****************************************************************/
/*
Write sis3820 register value
*/
static void regWrite(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A32);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  mvme_write_value(mvme, base + offset, value);
}

/*****************************************************************/
uint32_t sis3820_RegisterRead(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  return regRead(mvme, base, offset);
}

/*****************************************************************/
void     sis3820_RegisterWrite(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  regWrite(mvme,base,offset,value);
}

/*****************************************************************/
uint32_t sis3820_ScalerRead(MVME_INTERFACE *mvme, DWORD base, int ch)
{
  if (ch == 0)
    return regRead(mvme, base, SIS3820_COUNTER_CH1+ch*4);
  else
    return regRead(mvme, base, SIS3820_COUNTER_SHADOW_CH1+ch*4);
}


/*****************************************************************/
/*
Read nentry of data from the data buffer. Will use the DMA engine
if size is larger then 127 bytes. 
*/
int sis3820_FifoRead(MVME_INTERFACE *mvme, DWORD base, void *pdest, int wcount)
{
  int rd;
  int save_am;

    mvme_get_am(mvme,&save_am);
    mvme_set_blt(  mvme, MVME_BLT_MBLT64);
    mvme_set_am(   mvme, MVME_AM_A32_D64);

  rd = mvme_read(mvme, pdest, base + SIS3820_FIFO_BASE, wcount*4);
  //  printf("fifo read wcount: %d, rd: %d\n", wcount, rd);
  mvme_set_am(mvme, save_am);


  return wcount;
}

int sis3820_sdramRead(MVME_INTERFACE *mvme, DWORD base, void *pdest, int wcount)
{
  int rd=0;
  int save_am;

    mvme_get_am(mvme,&save_am);
    mvme_set_blt(  mvme, MVME_BLT_MBLT64);
    mvme_set_am(   mvme, MVME_AM_A32_D64);
  printf("sdram wcount: %d\n", wcount);
  rd = mvme_read(mvme, pdest, base + SIS3820_SDRAM_BASE, wcount*4);
  printf("sdram read wcount: %d, rd: %d\n", wcount, rd);
  mvme_set_am(mvme, save_am);

  if(rd < 0) 
    return -1;
  else
    return wcount;
}


/*****************************************************************/
void sis3820_Reset(MVME_INTERFACE *mvme, DWORD base)
{
  regWrite(mvme,base,SIS3820_KEY_RESET,0);
}

/*****************************************************************/
int  sis3820_DataReady(MVME_INTERFACE *mvme, DWORD base)
{
  return regRead(mvme,base,SIS3820_FIFO_WORDCOUNTER);
}

/*****************************************************************/
void  sis3820_Status(MVME_INTERFACE *mvme, DWORD base)
{
  printf("SIS3820 at A32 0x%x\n", (int)base);
  printf("ModuleID and Firmware: 0x%x\n", regRead(mvme,base,SIS3820_MODID));
  // printf("CSR: 0x%x\n", regRead(mvme,base,SIS3820_CONTROL_STATUS));

  sis3820_CSR0(mvme,base);


  printf("Operation mode: 0x%x\n", regRead(mvme,base,SIS3820_OPERATION_MODE));
  printf("Inhibit/Count disable: 0x%x\n", regRead(mvme, base, SIS3820_COUNTER_INHIBIT));
  printf("Counter Overflow: 0x%x\n", regRead(mvme, base, SIS3820_COUNTER_OVERFLOW));
}

void sis3820_CSR0(MVME_INTERFACE *mvme, DWORD base)
{
  uint32_t bitpat;
  char onof[2][4]={"off","on"};
  char enab[2][10]={"disabled","enabled"};
  char acti[2][11]={"NOT active","active"};
  char arm[2][10]={"NOT armed","Armed"}; 
  char set[2][6]={"set","clear"}; 
  char tmp[30];

  bitpat = regRead(mvme,base,SIS3820_CONTROL_STATUS);
  printf("CSR: 0x%x\n", bitpat);
  
  sprintf (tmp, "User LED %s",onof[((bitpat & 0x1 ) ? 1:0)]); // bit 0
  printf ("%-25s",tmp);
  sprintf (tmp,"25MHz Pulses %s", enab[((bitpat & 0x10) ? 1:0)]); // bit 4
  printf ("%-25s",tmp);
  sprintf (tmp,"Cntr TestMode %s",enab[((bitpat & 0x20) ? 1:0)]); // bit 5
  printf ("%-25s",tmp);
  sprintf (tmp,"50MHz RefCh1 %s", enab[((bitpat & 0x40) ? 1:0)]); // bit 6
  printf ("%-25s\n",tmp);
  sprintf (tmp,"Sclr enable %s", acti[((bitpat & 0x10000) ? 1:0)]); // bit 16
  printf ("%-25s",tmp);
  sprintf (tmp,"MCS enable %s", acti[((bitpat & 0x40000) ? 1:0)]); // bit 18
  printf ("%-25s",tmp);
  sprintf (tmp,"SDRAM/FIFO test %s", enab[((bitpat & 0x800000) ? 1:0)]); // bit 23
  printf ("%-25s",tmp);
  printf ("%-10s\n", arm[((bitpat & 0x1000000) ? 1:0)]); // bit 24
  sprintf (tmp,"HISCAL %s", enab[((bitpat & 0x2000000) ? 1:0)]); // bit 25
  printf ("%-25s",tmp);  sprintf (tmp,"Overflow %s", set[((bitpat & 0x8000000) ? 1:0)]); // bit 27
  sprintf (tmp,"Input bits %d", (bitpat & 0x30000000 >> 28) ); // bits 28,29
  printf ("%-25s",tmp);
  sprintf (tmp,"Ext Latch bits %d\n\n", (bitpat & 0xC0000000 >> 30) ); // bits 30,31
  printf ("%-25s\n",tmp);  return;
}

#ifdef TEST1
/*****************************************************************/
/*-PAA- For test purpose only */
#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {
  int i,j,k;
  uint32_t SIS3820_BASE  = 0x38000000;
  uint32_t dest[32], scaler;
  uint32_t bits;
  uint32_t data,reg;
  MVME_INTERFACE *myvme;
  int status,wcount,wc;
  int scancount,oldscancount;
  uint32_t data_buffer [100];
  

  int nbins =  3; // number of channel advances

  if (argc>1) {
    sscanf(argv[1],"%x", &SIS3820_BASE);
  }

  // Test under vmic
  status = mvme_open(&myvme, 0);
  sis3820_Reset(myvme,  SIS3820_BASE);

  data = 0xFFF0FFFF;// disable all except channels 17-21
  reg  =  SIS3820_COPY_DISABLE;
  write_reg(myvme, SIS3820_BASE, reg, data);

  reg = SIS3820_ACQUISITION_PRESET;  // set number of acquistions (bins)
  write_reg(myvme, SIS3820_BASE, reg, nbins);

  reg =  SIS3820_LNE_PRESCALE; // enable LNE prescaler
  data = 9999999;
  write_reg(myvme, SIS3820_BASE, reg, data);
  int t=10*pow(10,6); // 10MHz 
  printf ("Frequency of LNE pulses will be %d sec\n", t/(data+1) );



  // Select internal 10 MHz clock as LNE source, select MCS + SDRAM mode
  //  bits =  SIS3820_LNE_SOURCE_INTERNAL_10MHZ |  SIS3820_OP_MODE_MULTI_CHANNEL_SCALER |  SIS3820_SDRAM_MODE ;
  bits = SIS3820_CLEARING_MODE | SIS3820_MCS_DATA_FORMAT_32BIT | 
    SIS3820_LNE_SOURCE_INTERNAL_10MHZ |  SIS3820_OP_MODE_MULTI_CHANNEL_SCALER |
    SIS3820_SDRAM_MODE ;

  reg =   SIS3820_OPERATION_MODE;
  write_reg(myvme, SIS3820_BASE, reg, bits);

// enable counting, S LED should come on 
  printf ("\nWriting 0x%x to reg 0x%x (S LED should come on)\n", 
	  SIS3820_KEY_OPERATION_ENABLE, SIS3820_KEY_OPERATION_ENABLE);
  sis3820_RegisterWrite(myvme, SIS3820_BASE,SIS3820_KEY_OPERATION_ENABLE, SIS3820_KEY_OPERATION_ENABLE); 

  // read status
  sis3820_Status(myvme, SIS3820_BASE);

  printf ("Waiting 1s...\n");
  sleep(1);

  scancount=0;
  oldscancount=0;
  while (scancount!=nbins) {
    scancount =   sis3820_RegisterRead(myvme, SIS3820_BASE, SIS3820_ACQUISITION_COUNT);
       if(scancount!=oldscancount){
           oldscancount=scancount;
           printf("\nMCS mode, scan %d completed\n",scancount);
       }
   }
  if(0)
    {
     /* readout of scaler data */
  wc = sis3820_sdramRead(myvme,  SIS3820_BASE, (void *)data_buffer, nbins);
     printf("read wc %d\n",wc);
     if(wc>0) {

    

    printf("scaler[%i] = %d\n", i, scaler);
         printf("scaler data\n");
         k=0;
         for (j=1;j<=nbins;j++) {
            printf("scan: %3.3d ",j);
            for (i=1;i<=4;i++) {
               printf("ch%2.2d %8.8x  ",i,data_buffer[k]);
               k++;
            }
            printf("\n");
         }
	  }
    } 

  if(1)
    {
  // try single word reads
      int nchan = 32;
  int nread = nbins * nchan ; 
    for (i=0;i<nread;i++) {
      data_buffer[i]= sis3820_ScalerRead(myvme, SIS3820_BASE, i); // FIFO read from same place; SRAM advance ptr
    printf("data_buffer[%i] = %d\n", i, data_buffer[i]);
    }

  return 1;
    }
}
uint32_t write_reg (  MVME_INTERFACE *mvme, DWORD base, uint32_t reg, uint32_t data )
{

  printf ("\nWriting 0x%x to reg 0x%x\n", data,  reg); 
  sis3820_RegisterWrite(mvme, base, reg, data);
  data = sis3820_RegisterRead(mvme, base, reg);
  printf ("Read back  0x%x from reg 0x%x\n", data,  reg);
  return data;
}
#endif  // MAIN
#endif  // TEST1
#define TEST2
#ifdef TEST2
/*****************************************************************/
/*-PAA- For test purpose only */
#ifdef MAIN_ENABLE


int main (int argc, char* argv[]) {
  int i;
  uint32_t SIS3820_BASE  = 0x38000000;
  //  uint32_t dest[32]; 
  uint32_t scaler;
  MVME_INTERFACE *myvme;
  int status;

  if (argc>1) {
    sscanf(argv[1],"%x", &SIS3820_BASE);
  }

  // Test under vmic
  status = mvme_open(&myvme, 0);
  sis3820_Reset(myvme, SIS3820_BASE);

  regWrite(myvme, SIS3820_BASE, SIS3820_OPERATION_MODE, 0x100001); // non-clearing mode, outputs mode 1
  //  regWrite(myvme, SIS3820_BASE, SIS3820_COUNTER_INHIBIT, 0x00000000);  // Enabled
  regWrite(myvme, SIS3820_BASE, SIS3820_CONTROL_STATUS, 0x10000); // switch off user LED,

  regWrite(myvme, SIS3820_BASE,  SIS3820_COPY_DISABLE,  0xFFF0FFFF); // disable all except channels 17-21
  printf("Read back copy/disable reg 0x%x  as 0x%x\n", SIS3820_COPY_DISABLE, regRead (myvme, SIS3820_BASE,  SIS3820_COPY_DISABLE) );
  sis3820_Status(myvme, SIS3820_BASE);

  for (i=0;i<32;i++) {
    scaler = sis3820_ScalerRead(myvme, SIS3820_BASE, i);
    printf("scaler[%i] = %d\n", i, scaler);
  }
  regWrite(myvme, SIS3820_BASE, SIS3820_KEY_OPERATION_ENABLE, 0x00);
  regWrite(myvme, SIS3820_BASE, SIS3820_CONTROL_STATUS, 0x1); // switch on user LED
  sleep(10);

  regWrite(myvme, SIS3820_BASE, SIS3820_KEY_OPERATION_DISABLE, 0x00);

  regWrite(myvme, SIS3820_BASE, SIS3820_CONTROL_STATUS, 0x10000); // switch off user LED

  sis3820_Status(myvme, SIS3820_BASE);

  //sis3820_RegisterWrite(myvme, SIS3820_BASE, SIS3820_COUNTER_CLEAR, 0xFFFFFFFF);
  // sis3820_FifoRead(myvme, SIS3820_BASE, &dest[0], 32);
    for (i=0;i<32;i++) {
    scaler = sis3820_ScalerRead(myvme, SIS3820_BASE, i);
    printf("scaler[%i] = %d\n", i, scaler);
  }

  return 1;
}  


#endif 
#endif


/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */

//end


