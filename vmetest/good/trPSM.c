/*------------------------------------------------------------------*
 * trPSM.c 
 * Procedures for handling the POL Synthesizer Module
 * This is a VME board specified by Syd Kreitzman - TRIUMF
 * designed and produced by Triumf electronics group - Hubert Hui
 * for use on BNQR experiment
 *
 * initial VMIC version based on CVS 1.13
 *

 *------------------------------------------------------------------*/


#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "unistd.h"  // for sleep
#include "trPSM.h" 
#include "math.h"
#include "vmicvme.h"

#define HAVE_PSMII  // BNMR experiment has a PSMII
                    // BNQR has a PSM
#define FAILURE -1  // SUCCESS 1 defined in trPSM.h
#define TRUE 1
#define FALSE 0

const char profile_string[][5]={"1f  ","3f  ","5f  ","fREF","end"};

 int pdd = 0; /* debug */

static FOUR_PROFILES four_profiles;
static BOOL init_flag=0;
static PROFILE_BASE_REGS ProBaseRegs;

/* conversion of frequency (Hz) to hex frequency values */
#ifndef HAVE_PSMII 
const double finc =  0.0093132257462;
#else
const double finc =  0.04656613;
#endif
double Fmax;
int status;
/* used by psmReadProfileReg */
int array[NUM_PROFILES];
int *pdata;

double my_freq=2500000;
DWORD get_hex(DWORD freq_Hz);
DWORD get_Hz(DWORD freq_hex);


/*****************************************************************
 *****************************************************************
 *   VMIC

 *****************************************************************/
//Read register value  (16 bits)

static uint32_t regRead(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  if(pdd)printf("regRead: base=0x%x offset=0x%x\n",base,offset);
  return ( mvme_read_value(mvme, base +offset) & 0xFFFF);
}

/*****************************************************************/
/*
Write register value  (16 bits)
*/
static void regWrite(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  if(pdd)printf("regWrite: base=0x%x offset=0x%x value=0x%x or %d\n",base,offset,value,value);
  mvme_write_value(mvme, base + offset, value);
}



/*****************************************************************/
/*   VMIC
Read register value  (8 bits)
*/
static uint32_t regRead8(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D8);
  if(pdd)printf("regRead8: base=0x%x offset=0x%x\n",base,offset);
  return (mvme_read_value(mvme, base + offset) & 0xFF);
}

/*****************************************************************/
/*
Write register value    (8 bits)
*/
static void regWrite8(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D8);
  if(pdd)printf("regWrite8: base=0x%x offset=0x%x value=0x%x or %d\n",base,offset,value,value);
  mvme_write_value(mvme, base + offset, value);
}
/*****************************************************************/




/*------------------------------------------------------------------*/
/** psmRegRead
    Read 16bit to register.
    @memo 16bit read.
    @param mvme MVME interface handle
    @param base\_adr PSM VME base address
    @param reg\_offset register offset
    @return read back value
*/

INT psmRegRead(MVME_INTERFACE *mvme, const DWORD base_addr, DWORD reg_offset)
{
  // Registers are offset from BASE_CONTROL_REGS
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset;
  if(pdd)printf("psmRegRead: base=0x%x offset=0x%x\n",base_addr,offset);

  return regRead(mvme, base_addr, offset);
}

/*------------------------------------------------------------------*/
/** psmRegRead8
    Read 8bit to register.
    @memo 8bit read.
    @param mvme MVME interface handle
    @param base\_adr PSM VME base address
    @param reg\_offset register offset
    @return read back value
*/
INT psmRegRead8(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset)
{
  // Registers are offset from BASE_CONTROL_REGS
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset;
  if(pdd)printf("psmRegRead8: base=0x%x offset=0x%x\n",base_addr,reg_offset);
  return ( regRead8 (mvme, base_addr, offset)  & 0xff);  
}

/*------------------------------------------------------------------*/
/** psmRegWrite
    @memo 16bit write.
    @param mvme MVME interface handle
    @param base\_adr PSM VME base address
    @param reg\_offset register offset (starting at 00)
    @param value to be written 

*/
INT psmRegWrite(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset,
                           const WORD value)
{
  // Registers are offset from BASE_CONTROL_REGS  
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset; //  0x09000
  if(pdd)printf("psmRegWrite8: base=0x%x offset=0x%x value=0x%x or %d\n",base_addr,offset,value,value);
  regWrite(mvme,base_addr,offset,(uint32_t) value);
  return  regRead( mvme,base_addr,offset);
}

/*------------------------------------------------------------------*/
/** psmRegWrite8
    Write 8bit to register.
    @memo 8bit write.
    @param mvme MVME interface handle
    @param base\_adr PSM VME base address
    @param reg\_offset register offset
    @param value to be written 
*/
INT psmRegWrite8(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset,
                           const BYTE value)
{
  // Registers are offset from BASE_CONTROL_REGS
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset;
  if(pdd)printf("psmregWrite8: base=0x%x offset=0x%x value=0x%x or %d\n",base_addr,offset,value,value);
  regWrite8(mvme,base_addr,offset,(uint32_t) value);
  return regRead8(mvme,base_addr,offset);
}

/** psmRegReadDM
    Read 16bit from DM
    @memo 16bit read.
    @param base\_adr PSM VME base address
    @param reg\_offset register offset
    @return read back value
*/
 INT psmRegReadDM( MVME_INTERFACE *mvme, DWORD base_addr, DWORD reg_offset)
{
  // DM  reg_offset is offset from  MEMORY_BASE
  int offset;
  offset = MEMORY_BASE + (int)reg_offset;
  if(pdd)printf("psmRegReadDM: base=0x%x offset=0x%x\n",base_addr,offset);
  return regRead(mvme, base_addr, offset);  
}

/*------------------------------------------------------------------*/
/** psmRegWriteDM
    @memo 16bit write.
    @param base\_adr PSM VME base address
    @param reg\_offset register offset (starting at 00)
    @param value to be written 
    @return read back value
*/
 INT psmRegWriteDM( MVME_INTERFACE *mvme, DWORD base_addr, const DWORD reg_offset,
                           const WORD value)
{
  // DM  reg_offset is offset from  MEMORY_BASE
  int offset;
  offset = MEMORY_BASE + (int)reg_offset;  // MEMORY_BASE = 0000
  if(pdd)printf("psmRegWriteDM: base=0x%x offset=0x%x  value=0x%x or %d\n",base_addr,offset,value,value);
  regWrite(mvme,base_addr,offset,(uint32_t) value);

  return regRead (mvme,base_addr,offset);
}

   
void dump(MVME_INTERFACE *mvme,  DWORD base_addr)
{
  /* dump the device registers */


  DWORD start,stop;
  DWORD i;
  int j;
  char msg[80];
  DWORD data0,data1;

  char *dev_regs[]={"Control Reg 1","Control Reg 2","Control Reg 3","Output Scale Factor",
		   "IQ Memory Length MSB", "IQ Memory Length LSB", "IQ Memory Address MSB", "IQ Memory Address LSB",""};

  char *fref_reg[]={"fREF tuning Freq MSB","fREF tuning Freq LSB","Freq sweep length","Freq sweep address",""};

  char *mod_cntrl_regs[]={"End Sweep Control","RF Trip Threshold","Freq Sweep Internal Strobe",
			 "Freq Sweep Addr Reset","Gate Control","RF Trip Status/Reset","VME Module Reset",""};
  start = BASE_CONTROL_REGS - 4; /* idle */
  stop  = 0x9020; 
  i=start;

  printf("\nDump of Control Registers:\n\n");

  data0 = regRead(mvme, base_addr, i); // 16 bits read
  sprintf(msg,"   Freq Sweep Idle MSB \n");
  printf("address: 0x%4.4x  contents Word: 0x%4.4x (%6.6d) %s",
	 i,data0,data0, msg);
  i+=2;
  data0 = regRead(mvme, base_addr, i); // 16 bits read
  sprintf(msg,"   Freq Sweep Idle LSB \n");
  printf("address: 0x%4.4x  contents Word: 0x%4.4x (%6.6d) %s",
	 i,data0,data0, msg);
  printf("\n");
  i+=2;

  j=0;
  while (i < stop)
    { 
      if(i==0x9000)
	{
	  printf ("                        1f   Device Registers\n");
	  j=0;
	}
      else if (i==0x9008)
	{
	  printf ("                        2f   Device Registers\n");
	  j=0;
	}
      else if (i==0x9010)
	{
	  j=0;
	  printf ("                        3f   Device Registers\n");
	}
      else if (i==0x9018)
	{
	  printf ("                        5f   Device Registers\n");
	  j=0;
	}
      
      else if (i==0x9024)
	{
	  j=0;
	  printf ("                        fREF Device Registers\n");
	}

      data1 = regRead8(mvme, base_addr, i); // 8 bits read 
      printf("address: 0x%4.4x  contents Byte: 0x%2.2x (%3.3d) %s \n",
	     i,data1,data1,dev_regs[j] );
      i++;
      j++;
    }
  printf("\n");

  i=stop;
  stop=0x9028;
  j=0;
  while (i < stop)
    {
      data0 = regRead(mvme, base_addr, i); // 16 bits read
      printf("address: 0x%4.4x  contents: 0x%4.4x (%6.6d) %s\n",
	     i,data0,data0, fref_reg[j]);
      i+=2;
      j++;
    }

  i=0x9028;
  stop=0x902F;
  printf("\n");
  j=0;
  while (i < stop)
    {

      data1 = regRead8(mvme, base_addr, i); // 8 bits read 
      printf("address: 0x%4.4x  contents: Byte: 0x%2.2x (%3.3d) %s\n",
	     i,data1,data1,mod_cntrl_regs[j] );
      i++;
      j++;
    }

  return;
}



INT psmTryIQ (MVME_INTERFACE *mvme, const DWORD base_addr, char * freq_filename, char * IQ_filename)
{
  DWORD I,Q;
  INT status,offset=0;
  DWORD temp,freq,ninc,first;
  char default_freq_filename[]="2a.psm";
  char default_IQ_filename[]="2a_iq.psm";
  DWORD i,q;
  BOOL diff,print_data;
  BOOL twos_comp;
  INT j;
  if(!init_flag)init();

  if (strncmp(freq_filename,"def",3)==0)
    {
      printf("using default frequency file %s\n",default_freq_filename);
      strcpy(freq_filename, default_freq_filename);
    }
  printf("frequency filename is \"%s\"\n",freq_filename);

  if (strncmp(IQ_filename,"def",3)==0)
    {
      printf("using default IQ file %s\n",default_IQ_filename);
      strcpy(IQ_filename, default_IQ_filename);
    }
  printf("IQ filename is \"%s\"\n",IQ_filename);

  

  offset = 52;
  I=320;
  Q=-40;


  twos_comp=FALSE; /* data not in 2s compliment */
  printf("Calling psmLoadOneIQ for \"3f\" with I=%d,Q=%d, mem offset=%d\n",I,Q,offset);
  psmLoadOneIQ(mvme, base_addr, "3f", I,Q, offset, twos_comp); 

  I=-1; Q=-1;
  printf("Calling psmReadOneIQ for \"3f\" with mem offset=%d\n",offset);
  status = psmReadOneIQ(mvme, base_addr,  "3f", &I, &Q, offset, &diff, twos_comp);
  if(status != FAILURE)
    printf("I=%d Q=%d\n\n\n",I,Q);

  /* frequency */

  /* frequency is in hex (not Hz) */
  freq=3000000;
  offset=100;
  printf("Calling psmLoadOneFreq with freq=%d, (0x%x) mem offset=%d\n",freq,freq,offset);
  psmLoadOneFreq(mvme, base_addr,freq , offset); 


  printf("Calling psmReadOneFreq with mem offset=%d\n",offset);
  temp=psmReadOneFreq(mvme, base_addr, offset);
  if(temp != FAILURE)
    printf("Read frequency as %d (0x%x)",temp,temp);


  printf("\n============================\n");
  printf("Calling psmLoadIQ_DM with base_addr=0x%x, filename =%s, profile=\"1f\" \n",base_addr,IQ_filename);

  /* load a file... file must already in 2s compliment format */
  status = psmLoadIQ_DM(mvme, base_addr, IQ_filename , "1f", &ninc,&i,&q);
  printf("no. IQ increments = %d; initial value i = %d q = %d\n",ninc,i,q);
  printf("calling psmReadIQ_DM for 1f\n");

  /* read back .. don't check against the file; print the data if less than 25 words  */
  if (ninc <25 )
    print_data=1;
  else
    print_data = 0;
  printf("print_data = %d\n",print_data);

  psmReadIQ_DM(mvme, base_addr,"1f",&i,&q,0,ninc,&diff, print_data, "");

  printf("\n============================\n");

  printf("Calling psmLoadIQ_DM with base_addr=0x%x, filename =%s, profile=\"fREF\" \n",base_addr,IQ_filename);

  /* load a file... file must already in 2s compliment format */
  status = psmLoadIQ_DM(mvme, base_addr, IQ_filename , "fREF", &ninc,&i,&q);
  printf("no. IQ increments = %d; initial value i = %d q = %d\n",ninc,i,q);
  printf("calling psmReadIQ_DM for FREF\n");

  /* read back .. don't check against the file; print the data if < 25 words */
  psmReadIQ_DM(mvme, base_addr,"fref",&i,&q,0,ninc,&diff, print_data, "");
  printf("\n======================================================\n");
  /* now loading file into profile 3f */
  /* load a file... file must already in 2s compliment format */
  status = psmLoadIQ_DM(mvme, base_addr, IQ_filename , "3f",  &ninc,&i,&q);
  printf("no. IQ increments = %d; initial value i = %d q = %d\n",ninc,i,q);
  printf("calling psmReadIQ_DM for 3f\n");

  /* read back .. don't check against the file; print the data  */
  psmReadIQ_DM(mvme, base_addr,"3f",&i,&q,0,ninc,&diff, print_data, "");
  printf("\n======================================================\n");
  /* now loading file into profile 5f */
  /* load a file... file must already in 2s compliment format */
  status = psmLoadIQ_DM(mvme, base_addr, IQ_filename , "5f",  &ninc,&i,&q);
  printf("no. IQ increments = %d; initial value i = %d q = %d\n",ninc,i,q);
  printf("calling psmReadIQ_DM for 5f\n");

  /* read back .. don't check against the file; print the data  */
  psmReadIQ_DM(mvme, base_addr,"5f",&i,&q,0,ninc,&diff, print_data, "");
  printf("\n======================================================\n");

  /* Now repeat, this time check against the file */

  printf("\n Now checking against the file.... \n");
  printf("\n======================================================\n");

  printf("Calling psmLoadIQ_DM with base_addr=0x%x, filename =%s, profile=\"1f\" \n",base_addr,IQ_filename);

  print_data = 0;

  /* load a file... file must already in 2s compliment format */
  status = psmLoadIQ_DM(mvme, base_addr, IQ_filename , "1f",  &ninc,&i,&q);
  printf("no. IQ increments = %d; initial value i = %d q = %d\n",ninc,i,q);
  printf("calling psmReadIQ_DM for 1f\n");

  /* read back .. check against the file; don't print the data  */
  psmReadIQ_DM(mvme, base_addr,"1f",&i,&q,0,ninc,&diff, print_data, IQ_filename);

  printf("\n============================\n");


  /* load a file... file must already in 2s compliment format */
  status = psmLoadIQ_DM(mvme, base_addr, IQ_filename , "fREF",  &ninc,&i,&q);
  printf("no. IQ increments = %d; initial value i = %d q = %d\n",ninc,i,q);
  printf("calling psmReadIQ_DM for FREF\n");

  /* read back .. check against the file; don't print the data  */
  psmReadIQ_DM(mvme, base_addr,"fref",&i,&q,0,ninc,&diff, FALSE, IQ_filename);
  printf("\n======================================================\n");
  /* now loading file into profile 3f */
  /* load a file... file must already in 2s compliment format */
  status = psmLoadIQ_DM(mvme, base_addr, IQ_filename , "3f",  &ninc,&i,&q);
  printf("no. IQ increments = %d; initial value i = %d q = %d\n",ninc,i,q);
  printf("calling psmReadIQ_DM for 3f\n");

  /* read back .. check against the file; don't print the data  */
  psmReadIQ_DM(mvme, base_addr,"3f",&i,&q,0,ninc,&diff, FALSE, IQ_filename);
  printf("\n======================================================\n");
  /* now loading file into profile 5f */
  /* load a file... file must already in 2s compliment format */
  status = psmLoadIQ_DM(mvme, base_addr, IQ_filename , "5f", &ninc,&i,&q);
  printf("no. IQ increments = %d; initial value i = %d q = %d\n",ninc,i,q);
  printf("calling psmReadIQ_DM for 5f\n");

  /* read back .. check against the file; don't print the data  */
  psmReadIQ_DM(mvme, base_addr,"5f",&i,&q,0,ninc,&diff, FALSE, IQ_filename);
  printf("\n======================================================\n");


  printf("\n\nCalling psmLoadfreqDM with base_addr=0x%x, filename =%s \n",base_addr,freq_filename);
  status = psmLoadFreqDM(mvme, base_addr, freq_filename, &ninc, &first );
  printf("no. frequency increments = %d; initial value = 0x%x\n",ninc,first);
 

   printf("\n=====================================================\n");

  printf("Calling psmLoadIQ_DM_all with base_addr=0x%x, filename =%s \n",base_addr,IQ_filename);
  status = psmLoadIQ_DM_all(mvme, base_addr, IQ_filename, &ninc,&i,&q);
  printf("no. IQ increments = %d; initial value i = %d q = %d\n",ninc,i,q);

  printf("\n =============================================\n");
  printf("calling psmReadIQ_DM for all profiles \n");
  printf(" .. first don't check against the file; print the data is < 25 words \n");

  if(ninc < 25)
    print_data=TRUE;
  else
    print_data=FALSE;
  /* don't check against the file;  print the data if < 25 words */
  psmReadIQ_DM(mvme, base_addr,"all",&i,&q,0,ninc,&diff,print_data, "");

  printf("\n =============================================\n");
  printf("calling psmReadIQ_DM for all profiles \n");
  printf(" .. now check against the file; don't print the data \n");

  /* check against the file; don't print the data */
  psmReadIQ_DM(mvme, base_addr,"all",&i,&q,0,ninc,&diff,print_data, IQ_filename);

   printf("\n =============================================\n");


   /* load idle value */
   i=-50; q=430;
   printf(" Now loading idle IQ pair i=%d q=%d to all profiles\n",i,q);

  psmLoadIdleIQ (mvme, base_addr, "all", 
		 i,q, FALSE); /* i,q, pair values NOT in 2s compliment */

 

  sleep(2); // sleep for 2s
  
  for(j=0; j<6; j++)
    {	  
      printf ("\n About to send internal strobe no. %d and sleeping for 3 sec\n",j);
      status = psmFreqSweepStrobe (mvme, base_addr);
      sleep(3); // sleep for 3s
    }

  return SUCCESS ;
}



INT psmInit( MVME_INTERFACE *mvme, DWORD base_addr)
{
  /* initialize PSM
     reset, put into single tone mode and into full sleep
  */

  INT status,temp;

 if(!init_flag)init();

 /*  Reset seems to be taking too long. Just do it once
 psmVMEReset (base_addr);
 sleep(5);
 if(pdd)printf("psmInit: module has been reset\n");
 */
 /* set all profiles into single tone mode ; default value of this register is 0   */
 if(pdd)printf("psmInit:Setting all profiles into single tone & full sleep mode\n");
 temp = FULL_SLEEP_MODE | SINGLE_TONE_MODE; 
 status = psmWriteProfileReg (mvme, base_addr, "all", PROFILE_REG2, temp);
 if(status == FAILURE)
   {
     printf("psmInit: error return from psmWriteProfileReg\n");
     return(FAILURE);
   }
 if(pdd)printf("psmInit: after writing 0x%x to all , read back  = 0x%x\n",temp,status);
 return(SUCCESS);
}


INT psmTestGate( MVME_INTERFACE *mvme, DWORD base_addr )
{
  INT status;
  DWORD idle_Hz = 0x250000;
  INT I=0;
  INT Q=255;
  INT i,q;
  INT temp,offset,val;
  char my_profile[20];
  status = psmInit(mvme, base_addr); /* calls init, sets full sleep, single tone */
  if (status == FAILURE) return status;


  printf("First a VME Reset\n");
  psmVMEReset(mvme,  base_addr);
  sleep(2);

  printf("Enter profile \"1f\" or \"all\"? ");
  scanf("%s",my_profile);
  my_profile[4]='\0';
  if (strncmp(my_profile,"all",3)==0)
    printf("Profile \"all\" will be used\n");
  else
      printf("Profile \"1f\" will be used\n");
  
  printf("Putting all profiles into full sleep and single tone mode\n");
 temp = FULL_SLEEP_MODE | SINGLE_TONE_MODE; 
 status = psmWriteProfileReg (mvme, base_addr, "all", PROFILE_REG2, temp);
 if(status == FAILURE)
   {
     printf("psmInit: error return from psmWriteProfileReg\n");
     return(FAILURE);
   }

  //  printf("Enter Idle frequency in Hz :");
  //scanf("%d",&idle_Hz);
  printf("Idle Freq is %d Hz\n",idle_Hz);
  if(idle_Hz > Fmax)
    {
      printf("psmTestGate: requested idle freq %d Hz is larger than maximum (%f Hz)\n",idle_Hz,Fmax);
      return(FAILURE);
    }


  printf("Setting profile %s into single tone mode\n",my_profile);
  temp = SINGLE_TONE_MODE; 
  status = psmWriteProfileReg (mvme, base_addr, my_profile, PROFILE_REG2, temp);
	  if(status == FAILURE)
    {
      printf("psmTestGate: error return from psmWriteProfileReg\n");
      return status;
    }


    printf("\nNow enabling front panel gates for all profiles \n");
    status = psmWriteGateControl (mvme, base_addr, "all",  1);
    if(status == FAILURE)
      {
	printf("psmTestGate: error return from psmWriteGateControl for 1f\n");
	return(status);
      }

    if (strncmp(my_profile,"all",3)==0)
      {
	i=-511;q=0;
	psmLoadIdleIQ (mvme, base_addr, "all", 
		       i,q, FALSE); /* i,q, pair values NOT in 2s compliment */
      }
    else
      {
	i=TwosComp_convert(I,FALSE); // convert data into 2's comp
	q=TwosComp_convert(Q,FALSE);
	printf("After converting %d,%d to 2's complement,  Idle iq pair=%d,%d \n",
	       I,Q,i,q);
	offset =  0x1FFC;  // idle IQ address for 1f
	
	i=1023;q=0;
	status=psmRegWriteDM(mvme, base_addr,offset, i);
	status=psmRegWriteDM(mvme, base_addr,offset+2, q);
	
      }

 printf("\nNow loading frequency  %dHz into IDLE", idle_Hz);
    
  val = psmLoadIdleFreq_Hz (mvme, base_addr, idle_Hz);
  if(val != FAILURE)
    { 
      printf("read back idle frequency as %dHz\n",val);
    }
  else
    {
      printf("psmTestGate: error writing idle frequency\n");
      return(FAILURE);
    }


  /* load the IDLE freq into DDS device */
  printf("\n Now loading IDLE with memory address reset\n");
  status = psmFreqSweepMemAddrReset(mvme, base_addr);

  printf("\nNow writing internal strobe register\n");
  status = psmFreqSweepStrobe (mvme, base_addr);

  return status;
}

INT psmSetOneFreq_Hz( MVME_INTERFACE *mvme, DWORD base_addr, DWORD Idle_freq_Hz, DWORD fref_Hz) 
{
  /* Test procedure 

     load IDLE and fref; set module into Single Tone Mode, enable 1f and fref. 
     Set gate control to 3 (always on) for 1f, fref . 3f,5f have normal gate, set into sleep mode */
  
  INT status;
  INT val;
  //double Freq;
  //long Fhex;
  int temp;
  
  status = psmInit(mvme, base_addr); /* calls init, sets full sleep, single tone */
  if (status == FAILURE) return status;


  if(Idle_freq_Hz > Fmax)
    {
      printf("psmSetOneFreq_Hz: requested idle freq %d Hz is larger than maximum (%f Hz)\n",Idle_freq_Hz,Fmax);
      return(FAILURE);
    }
  if(fref_Hz > Fmax)
    {
      printf("psmSetOneFreq_Hz: requested tuning freq %d Hz is larger than maximum (%f Hz)\n",fref_Hz,Fmax);
      return(FAILURE);
    }
  
  /*  re-enable 1f and fREF only  and set their gates to always on */

  printf("\nSetting  profiles 1f and fREF out of full sleep mode\n");
  temp = SINGLE_TONE_MODE; 
  status = psmWriteProfileReg (mvme, base_addr, "1f", PROFILE_REG2, temp);
  if(status == FAILURE)
    {
      printf("psmSetOneFreq_Hz: error return from psmWriteProfileReg\n");
      return status;
    }
  status = psmWriteProfileReg (mvme, base_addr, "fREF", PROFILE_REG2, temp);
  if(status == FAILURE)
    {
      printf("psmSetOneFreq_Hz: error return from psmWriteProfileReg\n");
      return status;
    }

    printf("\nNow enabling gates always for profile 1f and fref only \n");
    status = psmWriteGateControl (mvme, base_addr, "1f",  3);
    if(status == FAILURE)
      {
	printf("psmSetOneFreq_Hz: error return from psmWriteGateControl for 1f\n");
	return(status);
      }
    if(pdd)
	printf("after writing gate_code=3 to profile 1f , read back  = 0x%x\n",status);

    printf("\nNow re-enabling profile fREF  \n");
      
  status = psmWriteGateControl (mvme, base_addr, "fREF", 3);
  if(status ==FAILURE )
    {
      printf("psmSetOneFreq_Hz: error return from psmWriteGateControl for fREF\n");
      return(status);
    }
  if(pdd)
      printf("after writing gate_code=3 to fREF , read back  = 0x%x\n",status);
  
  printf("\nNow loading frequency  %dHz into IDLE", Idle_freq_Hz);
    
  val = psmLoadIdleFreq_Hz (mvme, base_addr, Idle_freq_Hz);
  if(val != FAILURE)
    { 
      printf("read back idle frequency as %dHz\n",val);
    }
  else
    {
      printf("psmSetOneFreq_Hz: error writing idle frequency\n");
      return(FAILURE);
    }
  /* also load tuning freq */

  printf("\nNow loading fref frequency  %dHz into fref freq tuning regs\n",fref_Hz);
  val = psmWrite_fREF_freq_Hz (mvme, base_addr,fref_Hz );
  if(val != FAILURE)
      printf("read back fref tuning frequency as %dHz\n",val);
  else
    {
      printf("psmSetOneFreq_Hz: error writing fref tuning frequency\n");
      return(FAILURE);
    }



  printf("\nNow resetting mem addr pointer & loading IDLE  (psmFreqSweepMemAddrReset)\n");
  /* load the IDLE freq into DDS device */
  status = psmFreqSweepMemAddrReset(mvme, base_addr);

  printf("\nNow writing internal strobe register\n");
  status = psmFreqSweepStrobe (mvme, base_addr);

  /* psmGetstatus(mvme, base_addr); */
  return SUCCESS;
}
/*=================================================================*/

INT psmSetOneFreq_Hz_2( MVME_INTERFACE *mvme, DWORD base_addr, DWORD Idle_freq_Hz, DWORD fref_Hz) 
{
  /* Test procedure 

     load IDLE and fref; set module into Single Tone Mode, enable 1f and fref. 
     Set gate control to 1 (FP) for 1f, fref . 3f,5f have normal gate, set into sleep mode */
  
  INT status;
  INT val;
  //double Freq;
  //long Fhex;
  int temp;
  
  status = psmInit(mvme, base_addr); /* calls init, sets full sleep, single tone */
  if (status == FAILURE) return status;


  if(Idle_freq_Hz > Fmax)
    {
      printf("psmSetOneFreq_Hz: requested idle freq %d Hz is larger than maximum (%f Hz)\n",Idle_freq_Hz,Fmax);
      return(FAILURE);
    }
  if(fref_Hz > Fmax)
    {
      printf("psmSetOneFreq_Hz_2: requested tuning freq %d Hz is larger than maximum (%f Hz)\n",fref_Hz,Fmax);
      return(FAILURE);
    }
  
  /*  re-enable 1f and fREF only  and set their gates to always on */

  printf("\nSetting  profiles 1f and fREF out of full sleep mode\n");
  temp = SINGLE_TONE_MODE; 
  status = psmWriteProfileReg (mvme, base_addr, "1f", PROFILE_REG2, temp);
  if(status == FAILURE)
    {
      printf("psmSetOneFreq_Hz_2: error return from psmWriteProfileReg\n");
      return status;
    }
  status = psmWriteProfileReg (mvme, base_addr, "fREF", PROFILE_REG2, temp);
  if(status == FAILURE)
    {
      printf("psmSetOneFreq_Hz_2: error return from psmWriteProfileReg\n");
      return status;
    }

    printf("\nNow enabling FP gates  for profile 1f \n");
    status = psmWriteGateControl (mvme, base_addr, "1f",  1);
    if(status == FAILURE)
      {
	printf("psmSetOneFreq_Hz_2: error return from psmWriteGateControl for 1f\n");
	return(status);
      }
    //if(pdd)
	printf("after writing gate_code=1 to profile 1f , read back  = 0x%x\n",status);

    printf("\nNow setting GAte Always for profile fREF  \n");
      
  status = psmWriteGateControl (mvme, base_addr, "fREF", 3);
  if(status ==FAILURE )
    {
      printf("psmSetOneFreq_Hz: error return from psmWriteGateControl for fREF\n");
      return(status);
    }
  //if(pdd)
      printf("after writing gate_code=3 to fREF , read back  = 0x%x\n",status);
  
  printf("\nNow loading frequency  %dHz into IDLE", Idle_freq_Hz);
    
  val = psmLoadIdleFreq_Hz (mvme, base_addr, Idle_freq_Hz);
  if(val != FAILURE)
    { 
      printf("read back idle frequency as %dHz\n",val);
    }
  else
    {
      printf("psmSetOneFreq_Hz_2: error writing idle frequency\n");
      return(FAILURE);
    }
  /* also load tuning freq */

  printf("\nNow loading fref frequency  %dHz into fref freq tuning regs\n",fref_Hz);
  val = psmWrite_fREF_freq_Hz (mvme, base_addr,fref_Hz );
  if(val != FAILURE)
      printf("read back fref tuning frequency as %dHz\n",val);
  else
    {
      printf("psmSetOneFreq_Hz_2: error writing fref tuning frequency\n");
      return(FAILURE);
    }



  printf("\nNow resetting mem addr pointer & loading IDLE  (psmFreqSweepMemAddrReset)\n");
  /* load the IDLE freq into DDS device */
  status = psmFreqSweepMemAddrReset(mvme, base_addr);

  printf("\nNow writing internal strobe register\n");
  status = psmFreqSweepStrobe (mvme, base_addr);

  /* psmGetstatus(mvme, base_addr); */
  return SUCCESS;
}
/*=================================================================*/






INT psmTryTwoFreq( MVME_INTERFACE *mvme, DWORD base_addr, DWORD Idle1_freq_Hz, DWORD Idle2_freq_Hz, DWORD fref_Hz) 
{
  /* load IDLE and fref; set module into Single Tone Mode, enable 1f and fref. 
     Set gate control to 3 (always on) for 1f, fref . 3f,5f have normal gate, set into sleep mode 
     then try a second value
*/
  
  INT status;
  int temp;

  status = psmInit(mvme, base_addr); /* calls init, sets full sleep, single tone */
  if (status == FAILURE) return status;
  
  if(Idle1_freq_Hz > Fmax || Idle2_freq_Hz > Fmax  )
    {
      printf("Requested idle freq(s) %dHz or %dHz is larger than maximum (%lfHz)\n",
	     Idle1_freq_Hz,Idle2_freq_Hz,Fmax);
      return(FAILURE);
    }
  if(fref_Hz > Fmax)
    {
      printf("Requested tuning freq %dHz is larger than maximum (%lfHz)\n",fref_Hz,Fmax);
      return(FAILURE);
    }
  
 
  
  /*  enable 1f and fREF only  and set their gates to always on */

  printf("\nSetting  profiles 1f and fREF out of full sleep mode\n");
  temp = SINGLE_TONE_MODE; 
  status = psmWriteProfileReg (mvme, base_addr, "1f", PROFILE_REG2, temp);
  if(status ==FAILURE )
    {
      printf("error return from psmWriteProfileReg\n");
      return status;
    }
  status = psmWriteProfileReg (mvme, base_addr, "fREF", PROFILE_REG2, temp);
  if(status == FAILURE)
    {
      printf("error return from psmWriteProfileReg\n");
      return status;
    }

    printf("\nNow enabling gates always for profile 1f and fref only \n");
    status = psmWriteGateControl (mvme, base_addr, "1f",  3);
    if(status == FAILURE)
      {
	printf("error return from psmWriteGateControl for 1f\n");
	return(status);
      }
    printf("after writing gate_code=3 to profile 1f , read back  = 0x%x\n",status);
  
    printf("\nNow re-enabling profile fREF  \n");
  status = psmWriteGateControl (mvme, base_addr, "fREF", 3);
  if(status ==FAILURE )
    {
      printf("error return from psmWriteGateControl for fREF\n");
      return(status);
    }
  printf("after writing gate_code=3 to fREF , read back  = 0x%x\n",status);
  
  printf("\nNow loading frequency  %dHz into IDLE", Idle1_freq_Hz);
  status = psmLoadIdleFreq_Hz (mvme, base_addr, Idle1_freq_Hz);
  if(status !=FAILURE ) 
    printf("read back idle1 frequency as %dHz\n",status);
  else
    {
    printf("error writing idle1 frequency\n");
    return(status);
    }
  /* also load tuning freq */

  printf("\nNow loading fref frequency  %dHz into fref freq tuning regs\n",fref_Hz);
  status = psmWrite_fREF_freq_Hz (mvme, base_addr,fref_Hz );
  if(status != FAILURE) 
    printf("read back fref tuning frequency as %dHz\n",status);
  else
    {
      printf("error writing fref tuning frequency\n");
      return(status);
    }



  printf("\nNow resetting mem addr pointer & loading IDLE  (psmFreqSweepMemAddrReset)\n");
  /* load the IDLE freq into DDS device */
  status = psmFreqSweepMemAddrReset(mvme, base_addr);

  printf("\nNow writing internal strobe register\n");
  status = psmFreqSweepStrobe (mvme, base_addr);

  psmGetStatus(mvme, base_addr, NULL);
  sleep(2); /* sleeping for 2s */

  printf("\n\n****** Now try loading another idle freq ****** \n");

 printf("\nNow loading frequency  %dHz into IDLE", Idle2_freq_Hz);
  status = psmLoadIdleFreq_Hz (mvme, base_addr, Idle2_freq_Hz);
  if(status != FAILURE) 
    printf("read back idle2 frequency as %dHz\n",status);
  else
    {
    printf("error writing idle2 frequency\n");
    return(status);
    }
  sleep(1); /* sleeping for 1s */
  printf("\nNow resetting mem addr pointer & loading IDLE  (psmFreqSweepMemAddrReset)\n");
  /* load the IDLE freq into DDS device */
  status = psmFreqSweepMemAddrReset(mvme,base_addr);
  sleep(1); /* sleeping for 1s */
  printf("\nNow writing internal strobe register\n");
  status = psmFreqSweepStrobe (mvme, base_addr);

  return status;
}

void psmTryScan( MVME_INTERFACE *mvme, DWORD base_addr, DWORD fref_Hz, BOOL jump_to_idle, char *filename)
{
  /* load a frequency file, idle and fref and try to scan. 1f,fREF profiles enabled. 
     First freq is loaded into IDLE.
     jump_to_idle = 0 -> stop at Nth
                  = 1 -> jump to idle
 */
  INT status;
  double Freq;
  DWORD Fhex;
  char default_file_name[]="2a.psm";
  DWORD ninc,first;
  int i;
 
  status = psmInit(mvme, base_addr); /* calls init, sets full sleep, single tone */
  if (status == FAILURE) return;

  
  status=psmRegReadDM(mvme, base_addr,0x9026);
  printf("after psmRegReadDM(mvme, base_addr,0x9026) data=0x%x\n",status);

  /* set end sweep mode to stop at Nth for frequency */
   printf("\nNow setting frequency end sweep mode to %d \n",jump_to_idle);
 status = psmFreqWriteEndSweepMode(mvme, base_addr, jump_to_idle);
  printf("  read back End Sweep Mode reg  = 0x%x\n",status);
  /*
  re-enable profile "all"  and set  gate to always on 
 */
  printf("\nNow re-enabling profile all \n");
  status=psmWriteProfileReg (mvme, base_addr, "all",  PROFILE_REG2, SINGLE_TONE_MODE);
  if(status == FAILURE)
    {
      printf("error return from psmWriteProfileReg for all\n");
      return;
    }
  printf("after writing 1 to all , read back  = 0x%x\n",status);
  
  printf("\nNow enabling gates always for profile all  only \n");
  status = psmWriteGateControl (mvme, base_addr, "all",  3);
  if(status ==FAILURE )
    {
      printf("error return from psmWriteGateControl for 1f\n");
      return;
    }


  /* load tuning freq */

  /* conversion from Hz */
  Freq = (double)fref_Hz;
  Freq=Freq/finc;
  Fhex=  (DWORD)(Freq + 0.5);
  printf("Input fref freq=%d Hz; -> Freq=%.1f, Freqx=%d (0x%x)\n",fref_Hz,Freq,Fhex,Fhex); 

  printf("\nNow loading fref hex frequency  0x%x into fref freq tuning regs\n",Fhex);
  status = psmWrite_fREF_freq (mvme, base_addr, Fhex);
  if(status != FAILURE) 
    printf("read back fref tuning frequency as 0x%x\n",status);
  else
    printf("error writing fref tuning frequency\n");
  

  if (strncmp(filename,"def",3)==0)
    {
      printf("using default frequency file %s\n",default_file_name);
      strcpy(filename, default_file_name);
    } 

  printf("\nNow loading frequency file %s\n",filename);
  status = psmLoadFreqDM(mvme, base_addr,filename,&ninc,&first);
  if(status == FAILURE)
    {
      printf("Error loading frequency file %s \n",filename);
      return;
    }
  printf("Loaded frequency file; no. frequency increments = %d, first value=0x%x\n",ninc,first);

  /* conversion from Hz -this was for when Idle_Hz was a parameter */
  /*  Freq = (double)idle_Hz;
      Freq=Freq/finc;
      Fhex=  (long)(Freq + 0.5);
      printf("Input idle freq=%d Hz; -> Freq=%.1f, Freqx=%d (0x%x)\n",idle_Hz,Freq,Fhex,Fhex); 
  */
  printf("\nNow loading first frequency value 0x%x into IDLE\n",first);
  status = psmLoadIdleFreq (mvme, base_addr, first);
  if(status != FAILURE) 
    printf("read back idle frequency as 0x%x\n",status);
  else
    printf("error writing idle frequency\n");
 

  //  printf("\nNow resetting mem addr pointer & loading IDLE  (psmFreqSweepMemAddrReset)\n");
      // load the IDLE freq into  device 
  //status = psmFreqSweepMemAddrReset(mvme, base_addr);
    
  
  /*printf("\nNow writing internal strobe register\n");
    status = psmFreqSweepStrobe (mvme, base_addr);
  */
  psmGetStatus(mvme, base_addr, NULL);
  /*  printf("\nWaiting for strobes; after 6 strobes, reset with  psmFreqSweepMemAddrReset(mvme, base_addr)\n");
   */  
  sleep(2);
  
  for(i=0; i<6; i++)
    {	  
      printf ("\n About to send internal strobe no. %d and sleeping for 3 sec\n",i);
      status = psmFreqSweepStrobe (mvme, base_addr);
      sleep(3);
    }
  printf("After 6 strobes, frequency should be at Idle\n");
  sleep(2);
  printf("Sending an extra strobe... it shouldn't do anything\n");
  status = psmFreqSweepStrobe (mvme, base_addr);
  sleep(2);
  printf("Now sending a memory address reset\n");
  status = psmFreqSweepMemAddrReset(mvme, base_addr);
  sleep(2);
  
  for(i=0; i<6; i++)
    {	  
      printf ("\n About to send internal strobe no. %d and sleeping for 3 sec\n",i);
      status = psmFreqSweepStrobe (mvme, base_addr);
      sleep(3);
    }
  printf("After 6 strobes, frequency should be at Idle\n"); 
}



/*------------------------------------------------------------------*/
void psmInitTest( MVME_INTERFACE *mvme, DWORD base_addr)
{
  int data,ival;
  int mask=0x3F;

  if(!init_flag)init();  /* software setup */
  
  /* try writing a few things */
  ival = psmWriteProfileReg(mvme, base_addr,"all",PROFILE_REG1,0x8);
  if(ival == FAILURE)
    printf("error return from psmWriteProfileReg\n");
  else
    printf("After writing 0x8 to all , for 3F, read back Reg1  = 0x%x, masked value is 0x%x\n",ival,(ival & mask));
  ival = psmReadProfileReg(mvme, base_addr,"all",PROFILE_REG1);
  if(ival == FAILURE)
    {
      printf("error return from psmReadProfileReg\n");
      return;
    }
  printf("Info - bits 6,7 of Reg1 are read-only\n");
  printf("  Expect all profile registers to be identical\n");
  
  
  if(check_identical()) /*	(pdata and array are globals) */ 
    printf("All profile REG1 registers are identical\n");
  else
    printf("All profile REG1 registers not identical\n");   
  
  
  psmReadProfile(mvme, base_addr,  &four_profiles.profile_reg_3f);
  if(ival == FAILURE)
    {
      printf("error return from psmReadProfile\n");
      return;
    }
  
  printf("readprofile gives Reg1 for 3f = 0x%x, masked value is 0x%x\n",
	 four_profiles.profile_reg_3f.registers[0],(four_profiles.profile_reg_3f.registers[0] & mask) );
  data = four_profiles.profile_reg_3f.registers[0];
  
  printf ("\n Now setting bit 5 of Reg1 for ONE profile (i.e. 3f)\n");
  data = ( data | 0x20);  /* set the bit */
  data = data & REG1_MASK;
  ival= psmWriteProfileReg(mvme, base_addr,"3f",PROFILE_REG1,data);
  printf("after writing 0x%x, read back Reg1 for 3f = 0x%x (masked = 0x%x)\n",data,ival,(ival & mask));
     

  /* (pdata and array are globals) */
  ival = psmReadProfileReg(mvme, base_addr,"all",PROFILE_REG1);
  if(ival == FAILURE)
    {
      printf("error return from psmReadProfileReg\n");
      return;
    }
  
  printf("\nInfo: expect all profiles NOT to be identical\n");
  if(check_identical())
    printf("All profile REG1 registers are identical\n");   
  else	 
    printf("All profile REG1 registers are not identical\n");
   


  data = ival;
  data = data & ~0x20 ; /* clear the bit */
  data = data & REG1_MASK;
  printf("\nNow writing 0x%x to Reg1  on profile \"3f\" \n",data);

   ival = psmWriteProfileReg(mvme, base_addr,"3f",PROFILE_REG1,data);
   printf("after writing 0x%x, read back Reg1 for \"3f\" = 0x%x (masked value is 0x%x)\n",data,ival,(ival&mask));
   psmReadProfile(mvme, base_addr,  &four_profiles.profile_reg_3f);
   printf("read back Reg1 for \"3f\" = 0x%x  (masked value is 0x%x)\n",
	  four_profiles.profile_reg_3f.registers[0], ( four_profiles.profile_reg_3f.registers[0] & mask));
       

  ival = psmReadProfileReg(mvme, base_addr,"all",PROFILE_REG1);
  if(ival == FAILURE)
    {
      printf("error return from psmReadProfileReg\n");
      return;
    }

  printf("\nInfo: expect all profiles now to be identical\n");
  if(check_identical())
    printf("All profile REG1 registers are identical\n");   
  else	 
    printf("All profile REG1 registers are not identical\n");

     
  printf("\n Now writing IQ DM length 0xD0 for all profiles\n"); 
  psmWriteProfileReg(mvme, base_addr,"all",PROFILE_REG5,0xD0); /* DM length */
   

  printf("and writing 0 to ALL EndSweepMode registers, then 1 to 3f  EndSweepMode register\n");
  /* test writing Sweep Control Register */
   psmIQWriteEndSweepMode(mvme, base_addr,"ALL", 0);  /* clear bit; jump to idle (IQ) */ 
   psmIQWriteEndSweepMode(mvme, base_addr,"3f",1); /* set bit; stop at Nth (IQ) */
   
  printf("and writing 1 for jump to idle for FREQ \n");
   data = psmFreqWriteEndSweepMode(mvme, base_addr,1); /* write bit for jump to idle for FREQ */
   
   /* write some other control registers  */
   printf("and writing 0x88 to RF_TRIP_THRESHOLD ");
   data = psmRegWrite8(mvme, base_addr,RF_TRIP_THRESHOLD, 0x88); 
   
   printf("and 0xBAD1BAD1 as  frequency tuning value \n");
   psmWrite_fREF_freq(mvme, base_addr, 0xBAD1BAD1); /* write frequency tuning value */
   
   printf("\nNow show status (S) to check register values\n");
}


void init(void)
{
  printf("init: starting\n");
  /* initialize !! */
  pdata = &array[0]; /* needed by psmReadProfileReg */

#ifdef HAVE_PSMII
 Fmax = 80000000- finc; /* globals for freq conversion */
#else
  Fmax = 40000000- finc; /* globals for freq conversion */
#endif

  /* offsets from BASE_CONTROL_REGS */
  ProBaseRegs.control_base[0] =  BASE_1F;
  ProBaseRegs.control_base[1] =  BASE_3F;
  ProBaseRegs.control_base[2] =  BASE_5F;
  ProBaseRegs.control_base[3] =  BASE_FREF;

  /* offsets from MEMORY_BASE */
  ProBaseRegs.dm_base[0] = BASE_DM_1F_IQ;
  ProBaseRegs.dm_base[1] = BASE_DM_3F_IQ; 
  ProBaseRegs.dm_base[2] = BASE_DM_5F_IQ;
  ProBaseRegs.dm_base[3] = BASE_DM_FREF_IQ;

  ProBaseRegs.control_reg_offset[0] = PROFILE_CONTROL_REG_1;
  ProBaseRegs.control_reg_offset[1] = PROFILE_CONTROL_REG_2;
  ProBaseRegs.control_reg_offset[2] = PROFILE_CONTROL_REG_3;
  ProBaseRegs.control_reg_offset[3] = PROFILE_OUT_SCALE_FAC;
  ProBaseRegs.control_reg_offset[4] = PROFILE_IQ_DM_LENGTH;
  ProBaseRegs.control_reg_offset[5] = PROFILE_IQ_DM_ADDRESS;


  
  four_profiles.profile_reg_1f.profile_offset = ProBaseRegs.control_base[0] ;
  four_profiles.profile_reg_3f.profile_offset =  ProBaseRegs.control_base[1];
  four_profiles.profile_reg_5f.profile_offset =  ProBaseRegs.control_base[2];
  four_profiles.profile_reg_fREF.profile_offset = ProBaseRegs.control_base[3] ;

  init_flag=1;
  return;
}





/*------------------------------------------------------------------*/
/** psmGetStatus
    Read back the status of all the registers 
    @param base\_adr PSM VME base address
*/
void psmGetStatus( MVME_INTERFACE *mvme, DWORD base_addr, FILE *fout)
{
  int val;
  //  int reg_offset;
  DWORD I,Q;
  BOOL identical;
  // char psm2txt[256];
  
  if(fout==NULL)fout=stdout;
  
  fprintf(fout,"Base Address: 0x%x\n",base_addr);
  if(!init_flag)
    init();
  
  psmReadProfile(mvme, base_addr, &four_profiles.profile_reg_1f);
  psmReadProfile(mvme, base_addr,  &four_profiles.profile_reg_3f);
  psmReadProfile(mvme, base_addr, &four_profiles.profile_reg_5f);
  psmReadProfile(mvme, base_addr,  &four_profiles.profile_reg_fREF);
  
  /* Read also End Sweep Control Register (28) which also contains profile info */
  val=psmRegRead8(mvme, base_addr, END_SWEEP_CONTROL );  /* word */
  fprintf(fout,"End sweep control reg = 0x%x (%d)\n",val,val);
  
  four_profiles.profile_reg_1f.end_sweep_mode=
    four_profiles.profile_reg_3f.end_sweep_mode=
    four_profiles.profile_reg_5f.end_sweep_mode=
    four_profiles.profile_reg_fREF.end_sweep_mode=0;
  
  if (val & (1 << 0) )
    four_profiles.profile_reg_1f.end_sweep_mode = 1;
  if (val & (1 << 1) )
    four_profiles.profile_reg_3f.end_sweep_mode = 1;
  if (val & (1 << 2) )
    four_profiles.profile_reg_5f.end_sweep_mode = 1;
  if (val & (1 << 3) )
    four_profiles.profile_reg_fREF.end_sweep_mode = 1;
  
  /* Read also Gate Control Register which also contains profile info */
  val=0;
  val=psmRegRead8(mvme, base_addr, GATE_CONTROL_REG);  /* word */
  fprintf(fout,"Gate control reg = 0x%x (%d)\n",val,val);
  
  four_profiles.profile_reg_1f.gate_control = (val & 3<<0) >>0;
  four_profiles.profile_reg_3f.gate_control = (val & 3<<2) >>2;
  four_profiles.profile_reg_5f.gate_control = (val & 3<<4) >>4;
  four_profiles.profile_reg_fREF.gate_control =  (val & 3<<6) >>6;

  /* Read also  Register which also contains profile info */
  val=0;
  val = psmReadProfileBufFactor(mvme, base_addr, "all", &identical);
  if(identical)
    {   /* they are expected to be identical */
      fprintf(fout,"Buf Factor reg = 0x%x (%d)\n",val,val);
      
      four_profiles.profile_reg_1f.buffer_factor =
	four_profiles.profile_reg_3f.buffer_factor =
	four_profiles.profile_reg_5f.buffer_factor =
	four_profiles.profile_reg_fREF.buffer_factor = val;
    }
  else
    {
      four_profiles.profile_reg_fREF.buffer_factor = val; /* returns last value */
      four_profiles.profile_reg_1f.buffer_factor =  
	psmReadProfileBufFactor(mvme, base_addr, "1f", &identical);
      four_profiles.profile_reg_3f.buffer_factor =  
	psmReadProfileBufFactor(mvme, base_addr, "3f", &identical);
      four_profiles.profile_reg_5f.buffer_factor =  
	psmReadProfileBufFactor(mvme, base_addr, "5f", &identical);
    }
  
#ifdef PSM
  // sprintf(psm2txt,"empty");
  if(psmReadIdleIQ(mvme, base_addr, "all", &I,&Q, TRUE)==0)
    {
      /* all profiles are the same */
      /* printf("profiles are the same, i=%d q=%d\n",I,Q);*/
      four_profiles.profile_reg_1f.I_idle=I;
      four_profiles.profile_reg_3f.I_idle=
	four_profiles.profile_reg_5f.I_idle=
	four_profiles.profile_reg_fREF.I_idle=I;
      
      four_profiles.profile_reg_1f.Q_idle=
	four_profiles.profile_reg_3f.Q_idle=
	four_profiles.profile_reg_5f.Q_idle=
	four_profiles.profile_reg_fREF.Q_idle=Q;
      
    }
  else
    {
      psmReadIdleIQ(mvme, base_addr, "1f", &four_profiles.profile_reg_1f.I_idle,
		    &four_profiles.profile_reg_1f.Q_idle, TRUE);
      psmReadIdleIQ(mvme, base_addr, "3f", &four_profiles.profile_reg_3f.I_idle,
		    &four_profiles.profile_reg_3f.Q_idle, TRUE);
      psmReadIdleIQ(mvme, base_addr, "5f", &four_profiles.profile_reg_5f.I_idle,
		    &four_profiles.profile_reg_5f.Q_idle, TRUE);
      psmReadIdleIQ(mvme, base_addr, "fREF", &four_profiles.profile_reg_fREF.I_idle,
		    &four_profiles.profile_reg_fREF.Q_idle, TRUE);
    }
  
#else // PSM II

  if(psmReadIdleIQ(mvme, base_addr, "all", &I,&Q, TRUE)==0)
    {
      /* all profiles agree with the I/Q Q/I -I/-Q I/Q  PSMII pattern */
      printf("profiles agree with PSMII pattern 1f i=%d q=%d\n",I,Q);
      // sprintf(psm2txt,"Profiles agree with PSMII pattern");
    }
  else
    {
      printf("\n\n ERROR Profiles DO NOT agree with PSMII pattern (for 1f i=%d q=%d)\n",I,Q);
      //  sprintf(psm2txt,"Profiles DO NOT agree with PSMII pattern");
    
      // read each profile 
      psmReadIdleIQ(mvme, base_addr, "1f", &four_profiles.profile_reg_1f.I_idle,
		    &four_profiles.profile_reg_1f.Q_idle, TRUE);
      psmReadIdleIQ(mvme, base_addr, "3f", &four_profiles.profile_reg_3f.I_idle,
		    &four_profiles.profile_reg_3f.Q_idle, TRUE);
      psmReadIdleIQ(mvme, base_addr, "5f", &four_profiles.profile_reg_5f.I_idle,
		    &four_profiles.profile_reg_5f.Q_idle, TRUE);
      psmReadIdleIQ(mvme, base_addr, "fREF", &four_profiles.profile_reg_fREF.I_idle,
		    &four_profiles.profile_reg_fREF.Q_idle, TRUE);
    }
#endif

  /* print out the contents of the profiles */
  
  char string[80];
  sprintf(string, "test string");
  psmPrintProfiles(&four_profiles,  fout, string);
  
  psmReadControlRegs(mvme, base_addr,fout); /* and read/print the other registers */
  return;
}

void  psmPrintProfiles(FOUR_PROFILES *p_four_profiles, FILE *fout ,  char *string)
{
  /* called from  psmGetStatus */

  //char buffer[128];
  if(fout==NULL)  fout=stdout;

  if(0)
    {

  if(!init_flag)init();  /* software setup */

  /* printf("PrintProfiles starting\n"); */

  fprintf(fout,"\nOffset                    1f      3f      5f    fREF      \n");

  fprintf(fout,"%2.2x       Cntrl Reg 1:    0x%2.2x    0x%2.2x    0x%2.2x   0x%2.2x  \n", 
	 PROFILE_CONTROL_REG_1,
	 p_four_profiles->profile_reg_1f.registers[0],
	 p_four_profiles->profile_reg_3f.registers[0],
	 p_four_profiles->profile_reg_5f.registers[0],
	 p_four_profiles->profile_reg_fREF.registers[0]); 



  fprintf(fout,"%2.2x       Cntrl Reg 2:    0x%2.2x    0x%2.2x    0x%2.2x   0x%2.2x  \n", 
	 PROFILE_CONTROL_REG_2,
	 p_four_profiles->profile_reg_1f.registers[1],
	 p_four_profiles->profile_reg_3f.registers[1],
	 p_four_profiles->profile_reg_5f.registers[1],
	 p_four_profiles->profile_reg_fREF.registers[1]); 

  fprintf(fout,"%2.2x       Cntrl Reg 3:    0x%2.2x    0x%2.2x    0x%2.2x   0x%2.2x  \n", 
	 PROFILE_CONTROL_REG_3,
  p_four_profiles->profile_reg_1f.registers[2],
	 p_four_profiles->profile_reg_3f.registers[2],
	 p_four_profiles->profile_reg_5f.registers[2],
	 p_four_profiles->profile_reg_fREF.registers[2]); 

  fprintf(fout,"%2.2x       Scale Fac:      0x%2.2x    0x%2.2x    0x%2.2x   0x%2.2x  \n", 
	 PROFILE_OUT_SCALE_FAC,
	 p_four_profiles->profile_reg_1f.registers[3],
	 p_four_profiles->profile_reg_3f.registers[3],
	 p_four_profiles->profile_reg_5f.registers[3],
	 p_four_profiles->profile_reg_fREF.registers[3]); 
 
  fprintf(fout,"%2.2x       IQ DM Length: 0x%4.4x  0x%4.4x  0x%4.4x 0x%4.4x  \n", 
	 PROFILE_IQ_DM_LENGTH,
	 p_four_profiles->profile_reg_1f.registers[4],
	 p_four_profiles->profile_reg_3f.registers[4],
	 p_four_profiles->profile_reg_5f.registers[4],
	 p_four_profiles->profile_reg_fREF.registers[4]); 

  fprintf(fout,"%2.2x       IQ DM Addr:   0x%4.4x  0x%4.4x  0x%4.4x 0x%4.4x  \n", 
	 PROFILE_IQ_DM_ADDRESS,
	 p_four_profiles->profile_reg_1f.registers[5],
	 p_four_profiles->profile_reg_3f.registers[5],
	 p_four_profiles->profile_reg_5f.registers[5],
	 p_four_profiles->profile_reg_fREF.registers[5]); 

  fprintf(fout,"%4.4x     IQ Idle I:    0x%4.4x  0x%4.4x  0x%4.4x 0x%4.4x   %s \n", 
	 IDLE_IQ_OFFSET,
	 p_four_profiles->profile_reg_1f.I_idle,
	 p_four_profiles->profile_reg_3f.I_idle,
	 p_four_profiles->profile_reg_5f.I_idle,
	 p_four_profiles->profile_reg_fREF.I_idle,
	 string ); 

 fprintf(fout,"%4.4x     IQ Idle Q:    0x%4.4x  0x%4.4x  0x%4.4x 0x%4.4x   %s\n\n",
	IDLE_IQ_OFFSET+2,
	 p_four_profiles->profile_reg_1f.Q_idle,
	 p_four_profiles->profile_reg_3f.Q_idle,
	 p_four_profiles->profile_reg_5f.Q_idle,
	 p_four_profiles->profile_reg_fREF.Q_idle,
	 string); 


  /* Control reg 1 */

  fprintf(fout,"    BITS                 1f  3f  5f  fREF\n");
  fprintf(fout,"PLL Lock Control:        %1.1d   %1.1d   %1.1d   %1.1d  (1=ignore PLL pin status)\n",
	  p_four_profiles->profile_reg_1f.lock_control,
	  p_four_profiles->profile_reg_3f.lock_control,
	  p_four_profiles->profile_reg_5f.lock_control,
	  p_four_profiles->profile_reg_fREF.lock_control);

  fprintf(fout, "Ref.ClockMultiplier:    %2.2d  %2.2d  %2.2d  %2.2d  (Multiplier; 1=bypass)\n",
	  p_four_profiles->profile_reg_1f.ref_clock_multiplier,
	  p_four_profiles->profile_reg_3f.ref_clock_multiplier,
	  p_four_profiles->profile_reg_5f.ref_clock_multiplier,
	  p_four_profiles->profile_reg_fREF.ref_clock_multiplier);

  /* control reg 2 */   

  fprintf(fout, "Operating Mode:          %1.1d   %1.1d   %1.1d   %1.1d  (Modulation 1=single;  0=Quad)\n",
	  p_four_profiles->profile_reg_1f.operating_mode,
	  p_four_profiles->profile_reg_3f.operating_mode,
	  p_four_profiles->profile_reg_5f.operating_mode,
	  p_four_profiles->profile_reg_fREF.operating_mode);
   
  fprintf(fout, "Auto Powerdown:          %1.1d   %1.1d   %1.1d   %1.1d  (1=low power mode)\n",
	  p_four_profiles->profile_reg_1f.auto_powerdown,
	  p_four_profiles->profile_reg_3f.auto_powerdown,
	  p_four_profiles->profile_reg_5f.auto_powerdown,
	  p_four_profiles->profile_reg_fREF.auto_powerdown);

  fprintf(fout, "Full Sleep:              %1.1d   %1.1d   %1.1d   %1.1d  (1=complete shutdown; 0=normal)\n",
	  p_four_profiles->profile_reg_1f.full_sleep_mode,
	  p_four_profiles->profile_reg_3f.full_sleep_mode,
	  p_four_profiles->profile_reg_5f.full_sleep_mode,
	  p_four_profiles->profile_reg_fREF.full_sleep_mode);

  fprintf(fout, "Inverse SINC bypass:     %1.1d   %1.1d   %1.1d   %1.1d  (SINC filter 1=bypassed;0=active)\n",
	  p_four_profiles->profile_reg_1f.inverse_sinc_bypass,
	  p_four_profiles->profile_reg_3f.inverse_sinc_bypass,
	  p_four_profiles->profile_reg_5f.inverse_sinc_bypass,
	  p_four_profiles->profile_reg_fREF.inverse_sinc_bypass);

  fprintf(fout, "cic clear:               %1.1d   %1.1d   %1.1d   %1.1d  (CIC filters 1= cleared; 0=normal)\n",
	  p_four_profiles->profile_reg_1f.cic_clear,
	  p_four_profiles->profile_reg_3f.cic_clear,
	  p_four_profiles->profile_reg_5f.cic_clear,
	  p_four_profiles->profile_reg_fREF.cic_clear);

  /* Control register 3 */
  fprintf(fout, "inverse cic bypass:      %1.1d   %1.1d   %1.1d   %1.1d  (1=Inverse CIC filter bypassed; 0=active)\n",
	  p_four_profiles->profile_reg_1f.inverse_cic_bypass,
	  p_four_profiles->profile_reg_3f.inverse_cic_bypass,
	  p_four_profiles->profile_reg_5f.inverse_cic_bypass,
	  p_four_profiles->profile_reg_fREF.inverse_cic_bypass);

  fprintf( fout,"spectral invert:         %1.1d   %1.1d   %1.1d   %1.1d  (1= + factor; 0= -factor)\n",
	  p_four_profiles->profile_reg_1f.spectral_invert,
	  p_four_profiles->profile_reg_3f.spectral_invert,
	  p_four_profiles->profile_reg_5f.spectral_invert,
	  p_four_profiles->profile_reg_fREF.spectral_invert);


  fprintf(fout, "cic interpolating rate: %2.2d  %2.2d  %2.2d  %2.2d  (Rate; 1=CIC filters bypassed)\n",
	  p_four_profiles->profile_reg_1f.cic_interpolation_rate,
	  p_four_profiles->profile_reg_3f.cic_interpolation_rate,
	  p_four_profiles->profile_reg_5f.cic_interpolation_rate,
	  p_four_profiles->profile_reg_fREF.cic_interpolation_rate);

  fprintf(fout, "end sweep mode:          %1.1d   %1.1d   %1.1d   %1.1d  (0=stop at Nth; 1=jump to IDLE)\n",
	  p_four_profiles->profile_reg_1f.end_sweep_mode,
	  p_four_profiles->profile_reg_3f.end_sweep_mode,
	  p_four_profiles->profile_reg_5f.end_sweep_mode,
	  p_four_profiles->profile_reg_fREF.end_sweep_mode);

  fprintf(fout, "gate control:            %1.1d   %1.1d   %1.1d   %1.1d  (0=dis fp; 1=en fp 2=en fp (inverted) 3=always on)\n",
	  p_four_profiles->profile_reg_1f.gate_control,
	  p_four_profiles->profile_reg_3f.gate_control,
	  p_four_profiles->profile_reg_5f.gate_control,
	  p_four_profiles->profile_reg_fREF.gate_control);

  fprintf(fout, "buffer factor:          %2.2d  %2.2d  %2.2d  %2.2d  (no. cycles to cycle IQ pairs)\n",
	  p_four_profiles->profile_reg_1f.buffer_factor,
	  p_four_profiles->profile_reg_3f.buffer_factor,
	  p_four_profiles->profile_reg_5f.buffer_factor,
	  p_four_profiles->profile_reg_fREF.buffer_factor);

    }
  fprintf(fout,"\n");
  return;

}
void psmReadControlRegs( MVME_INTERFACE *mvme, DWORD base_addr, FILE *fout)
{
  /* called from psmGetStatus; can be called directly 
 
     Read the rest of the registers */
  INT val,temp;
  DWORD val_Hz;
  DWORD reg_offset;

  if(fout==NULL)
    fout=stdout;
  
  /* Note: psmRegRead adds BASE_CONTROL_REGS=0x9000 to reg_offset */
  fprintf(fout,"psmReadControlRegs:\n");
  reg_offset = FREF_FREQ_LSW;
  val=psmRegRead(mvme, base_addr, reg_offset );  /* word */
  if(pdd)printf("LSB = 0x%x\n",val);
  temp = val;
  reg_offset = FREF_FREQ_MSW;
  val=psmRegRead(mvme, base_addr, reg_offset );  /* word */
  if(pdd)printf("MSB = 0x%x\n",val);
  temp = temp | (val << 16);
  val_Hz = get_Hz(temp);
  fprintf(fout,"f REF freq tuning value = %d Hz (hex equivalent = 0x%8.8x\n",val_Hz,temp);


  /* Frequency sweep length (16 bits) */
  reg_offset =  FREQ_SWEEP_LENGTH;
  val=psmRegRead(mvme, base_addr, reg_offset );  /* word */
  if(pdd)printf("Value read for freq sweep length=0x%x\n",val);
  val=val &  FREQ_SWEEP_MASK; /* 10 bits */
  fprintf(fout,"frequency sweep length  =     0x%4.4x (%d)\n",val,val);

  reg_offset =  FREQ_SWEEP_ADDRS;
  val=psmRegRead(mvme, base_addr, reg_offset );  /* word */
  val=val &  FREQ_SWEEP_MASK;  /* 10 bits */ 
  fprintf(fout,"frequency sweep address =     0x%4.4x (%d)\n",val,val);


  reg_offset =  RF_TRIP_THRESHOLD ;
  val=psmRegRead8(mvme, base_addr, reg_offset );  /* word */
  fprintf(fout,"RF power trip threshold =       0x%2.2x (%d)\n",val,val);

  reg_offset =  RF_TRIP_STATUS_RESET  ;
  val=psmRegRead8(mvme, base_addr, reg_offset );  /* word */

  val=val &  RF_TRIP_STATUS_RESET_MASK;
  fprintf(fout,"RF power status/trip reset reg = 0x%1.1x (%d)\n",val,val);


  /* display the GATE CONROL reg */
  val = psmRegRead8(mvme, base_addr, GATE_CONTROL_REG );
  fprintf(fout, "Gate control reg 0x%4.4x (%d)\n",val,val);

  /* Read also End Sweep Control Register (28) which also contains profile info */
  val=psmRegRead8(mvme, base_addr, END_SWEEP_CONTROL );  /* word */
  fprintf(fout,"End sweep control reg =         0x%2.2x (%d)\n",val,val);

  /* the other bits (for the profiles) will have been written out in  psmGetStatus*/  
  val = (val & 0x10)>>4; /* get this bit only */
  fprintf(fout,"Freq sweep: %d  (1=jump to IDLE; 0=stop at Nth)\n",val);


  /* display the IDLE frequency as well */
  val = psmReadIdleFreq (mvme, base_addr);
  val_Hz = get_Hz(val);
  fprintf(fout,"Idle freq =       %d Hz  (hex equivalent = 0x%x)\n",val_Hz,val);



 /* IQ: display the Max. Buffer Factor */
  val = psmReadMaxBufFactor(mvme, base_addr);
  fprintf(fout,"Max Buf factor =  %d  (0x%x)\n",val,val);


}


/*------------------------------------------------------------------*/
/** psmReadProfile
    Read back the all the registers in a profile (profile can be 1f,3f,5f or fREF)
    @structure of profile params\_adr profile parameters
*/
 void psmReadProfile( MVME_INTERFACE *mvme, DWORD base_addr, PROFILE_REG_PARAMS *p_profile_params)
{
  INT val,temp;
  DWORD reg_offset;

  /* called from  psmGetStatus */

  if(!init_flag)init();

  /* PROFILE CONTROL REG 1 */
 
  reg_offset = p_profile_params->profile_offset + PROFILE_CONTROL_REG_1;  
  /*if(pdd) 
    printf("psmReadProfile: calling psmRegRead with base_addr=0x%x, reg_offset=0x%x\n",
    base_addr,reg_offset); */


  val=psmRegRead8(mvme, base_addr, reg_offset );
  /* printf("Reg 1: 0x%x \n",val); */
  p_profile_params->registers[0]=val;  
  p_profile_params->lock_control = (val & PLL_LOCK_CONTROL) >>5 ;

 /* defaults */
 p_profile_params->ref_clock_multiplier= 1; 
 
 temp = val & REF_CLOCK_MULTIPLIER;
 if (temp >= 4 && temp <= 20)
   p_profile_params->ref_clock_multiplier= temp;




  /* PROFILE CONTROL REG 2 */
 
  reg_offset = p_profile_params->profile_offset + PROFILE_CONTROL_REG_2;  
  /* if(pdd) 
    printf("psmReadProfile: calling psmRegRead with base_addr=0x%x, reg_offset=0x%x\n",
    base_addr,reg_offset); */
  val=psmRegRead8(mvme, base_addr, reg_offset );
  p_profile_params->registers[1]=val; 
  p_profile_params->operating_mode = val & SINGLE_TONE_MODE;
   
  p_profile_params->auto_powerdown= (val & AUTO_POWERDOWN >>2) ;
  p_profile_params->full_sleep_mode = (val & FULL_SLEEP_MODE)>>3 ;
  p_profile_params->inverse_sinc_bypass = (val & INVERSE_SINC_BYPASS)>>6;
  p_profile_params->cic_clear = (val & CIC_CLEAR)>>7;

  /* PROFILE CONTROL REG 3 */
 
  reg_offset = p_profile_params->profile_offset + PROFILE_CONTROL_REG_3;  
  /* if(pdd)
    printf("psmReadProfile: calling psmRegRead with base_addr=0x%x, reg_offset=0x%x\n",
    base_addr,reg_offset); */
  val=psmRegRead8(mvme, base_addr, reg_offset );
  /* printf("Reg 3: 0x%x \n",val); */
  p_profile_params->registers[2]=val;   
  p_profile_params-> inverse_cic_bypass = val & INVERSE_CIC_BYPASS;
  p_profile_params->spectral_invert = (val & SPECTRAL_INVERT)>>1;
  temp = (val & CIC_INTERPOLATION_RATE);
  temp = temp >> 2;
  p_profile_params->cic_filters_bypassed = p_profile_params-> cic_interpolation_rate =0;

  if (temp == 1)
    {
      p_profile_params->cic_filters_bypassed = 1;
      p_profile_params-> cic_interpolation_rate =0;
    }
  else
    p_profile_params-> cic_interpolation_rate = temp;

 
  /* PROFILE CONTROL REG 4  Output Scale Factor */
 
  reg_offset = p_profile_params->profile_offset + PROFILE_OUT_SCALE_FAC;  
  /*  if(pdd)
    printf("psmReadProfile: calling psmRegRead with base_addr=0x%x, reg_offset=0x%x\n",
    base_addr,reg_offset); */
  val=psmRegRead8(mvme, base_addr, reg_offset );
  /* printf("Reg 4: 0x%x \n",val); */
  p_profile_params->registers[3]=val; 

  p_profile_params->output_multiplier = val;

  /* PROFILE CONTROL REG 5 */
 
  reg_offset = p_profile_params->profile_offset + PROFILE_IQ_DM_LENGTH;  
  /*if(pdd)
    printf("psmReadProfile: calling psmRegRead with base_addr=0x%x, reg_offset=0x%x\n",
    base_addr,reg_offset); */
  val=psmRegRead(mvme, base_addr, reg_offset );
  /* printf("Reg 5: 0x%x \n",val); */
  p_profile_params->registers[4]=val;   
  p_profile_params-> IQ_data_memory_length = val &  IQ_DATA_MEMORY_LENGTH;

  /* PROFILE CONTROL REG 6 */
 
  reg_offset = p_profile_params->profile_offset + PROFILE_IQ_DM_ADDRESS;  
  /* if(pdd)
    printf("psmReadProfile: calling psmRegRead with base_addr=0x%x, reg_offset=0x%x\n",
    base_addr,reg_offset); */
  val=psmRegRead(mvme, base_addr, reg_offset );
  /* printf("Reg 6: 0x%x \n",val); */
  p_profile_params->registers[5]=val;   
  p_profile_params-> IQ_data_memory_address = val &  IQ_DATA_MEMORY_ADDRESS;


  return;



}




/*------------------------------------------------------------------*/
 INT psmClearRFpowerTrip( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data;

  reg_offset =  RF_TRIP_STATUS_RESET  ;
  data = psmRegWrite8(mvme, base_addr, reg_offset,0);
  return(data);
}
/*------------------------------------------------------------------*/
INT   psmWriteRFpowerTripThresh( MVME_INTERFACE *mvme, DWORD base_addr, float Vtrip)
{
  DWORD reg_offset;
  BYTE ival;
  INT jval;
  INT data;
  if(pdd)
    printf("psmWriteRFpowerTripThresh: starting with base_addr=0x%x, Vtrip=%f\n",
	   base_addr, Vtrip);
 
  jval = ( (INT) (Vtrip*51.0 +0.5));
  ival = jval & 0xFF; /* mask to 8 bits */
  if(pdd) 
    printf("psmWriteRFpowerTripThresh:Vtrip = %.2f, data=0x%2.2x (%d)\n",Vtrip,ival,ival);
  reg_offset =  RF_TRIP_THRESHOLD  ;
  data = psmRegWrite8(mvme, base_addr,reg_offset, ival); 
  return(data);
}

/*------------------------------------------------------------------*/
float psmReadRFpowerTripThresh( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data;
  float Vtrip;

  if(pdd)
    printf("psmReadRFpowerTripThresh: starting with base_addr=0x%x\n",
	   base_addr);

  reg_offset =  RF_TRIP_THRESHOLD  ;
  data = psmRegRead8(mvme, base_addr,reg_offset);
  data = data &0xFF;
  Vtrip = 5 * data/255; 

  if(pdd) 
    printf("psmReadRFpowerTripThresh: data= 0x%2.2x (%d)  Vtrip = %.2f \n",data,data, Vtrip);

  return(Vtrip);
}

/*------------------------------------------------------------------*/

 INT psmReadRFpowerTrip( MVME_INTERFACE *mvme, DWORD base_addr)
{
 DWORD reg_offset;
 INT data;

  reg_offset =  RF_TRIP_STATUS_RESET ;
  data=psmRegRead8(mvme, base_addr, reg_offset );  /* word */
  data = data & 0x1;
  if(pdd)printf("RF power trip threshold =       0x%2.2x (%d)\n",data,data);
  return data;
}


/*------------------------------------------------------------------*/
 INT psmVMEReset( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data;

  reg_offset =  PSM_VME_RESET  ;
  data = psmRegWrite8(mvme, base_addr, reg_offset,1);
  return(data);
}

/*------------------------------------------------------------------*/
 INT psmFreqSweepStrobe( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data;

  reg_offset =   FREQ_SWEEP_INT_STROBE  ;
  data = psmRegWrite8(mvme, base_addr, reg_offset,1);
  return(data);
}
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
 INT psmReadMaxBufFactor( MVME_INTERFACE *mvme, DWORD base_addr )
{
  /* this is a read-only register */
  INT data;
  data = psmRegRead8(mvme, base_addr, MAX_BUF_FACTOR_NCMX );
  return(data);
}

/*------------------------------------------------------------------*/
 INT psmWriteProfileBufFactor( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code, WORD Ncmx)
{
  DWORD reg_offset;
  INT first, data;
  INT multi_flag,nprofile;
  INT status;

  status = first= data= 0;
  if(pdd)
    printf("psmWriteProfileBufFactor: starting with base_addr 0x%x, profile_code =%s, Ncmx = %d\n",base_addr, p_profile_code,Ncmx);

  if(!init_flag)init();

  /* check to see if we are writing all profiles or just one */
  multi_flag=check_profile(p_profile_code);

  if (multi_flag)
    {  /* all profiles */
      if(pdd)printf("detected ALL profiles \n");
      nprofile=0;
    }
  else
    { /* one profile */
      printf("psmWriteProfileBufFactor:Multi flag is false\n");
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;  
      printf("psmWriteProfileBufFactor: Writing to profile %s \n",p_profile_code);
   }
 
  reg_offset =  BUF_FACTOR_1F;
   while (nprofile < 4) /* 4 profiles; 1f,3f,5f,fREF */
    {
      data = psmRegWrite8(mvme, base_addr, (reg_offset + nprofile), Ncmx);
      if (!multi_flag)
	{
	  printf("psmWriteProfileBufFactor: value written for profile \"%s\" is %d; read back %d \n", 
		 profile_string[nprofile],Ncmx,data);
	  return data;
	}
      else
	{
	  if(pdd)
	    printf("data written at offset=0x%x  for profile \"%s\"  is %d; read back %d \n",
		   (reg_offset+nprofile),profile_string[nprofile],Ncmx,data);
	}

      if(nprofile==0) 
	first = data;
      else
	{
	  if(data != first)
	    {
	      printf("psmWriteBufFactor: data read back for profile %s (%d) disagrees with data for profile 1f (%d)\n",  profile_string[nprofile],data,first);
	      status = FAILURE; /* indicate an error */
	    }
	}
     nprofile++;
    }
   if (status == FAILURE)
     return status;

   return(data);
}


/*------------------------------------------------------------------*/
 INT psmReadProfileBufFactor( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code, 
				   BOOL *identical)
{
  DWORD reg_offset;
  INT first, data, nprofile;
  BOOL multi_flag;

  first=data=0;

  /* One profile:  returns the value, identical is false
     All profiles:  checks if all are set the same; if so identical is set true
                     otherwise identical is set false, returns last value read 
	       Note: expect all profiles to be the same; users are only allowed to write "all"
*/

  if(pdd)
    printf("psmReadProfileBufFactor: starting with base_addr 0x%x, profile_code =%s\n",base_addr, p_profile_code);

  if(!init_flag)init();


  /* check to see if we are reading all profiles or just one */
  multi_flag=check_profile(p_profile_code);

  if (multi_flag)
    {  /* all profiles */
      if(pdd)printf("psmReadProfileBufFactor: detected ALL profiles \n");
      nprofile=0;
      multi_flag=1; /* flag is true */
      *identical=TRUE; /* default */
    }
  else
    { /* one profile */
      printf("psmReadProfileBufFactor:Multi flag is false\n");
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;  
      printf("psmReadProfileBufFactor: Reading from profile %s \n",p_profile_code);
      *identical=FALSE; /* not relevent for one profile */
    }


  reg_offset =  BUF_FACTOR_1F;

  while (nprofile < 4) /* 4 profiles; 1f,3f,5f,fREF */
    {	  

      data = psmRegRead8(mvme, base_addr, reg_offset + nprofile);
      if(pdd)
	printf("data read at offset=0x%x  is %d \n",(reg_offset+nprofile),data);

      if (!multi_flag)
	{
	  printf("psmReadProfileBufFactor: value read for profile \"%s\" is %d \n", profile_string[nprofile],data);
	  return data;
	}

      if(nprofile == 0)
	first=data; /* remember the data */
      else
	{
	  if(data != first) /* check the data */
	    {
	      *identical = FALSE;
	      printf("psmReadBufFactor: data read back for profile %s (%d) disagrees with data for profile 1f (%d)\n",  profile_string[nprofile],data,first);
	    }
	}
      nprofile++;
    }
 
  return(data);
}



/*------------------------------------------------------------------*/
 INT psmFreqSweepMemAddrReset(MVME_INTERFACE *mvme, const DWORD base_addr)
{
  DWORD reg_offset;
  INT data;

  reg_offset =  FREQ_SWEEP_ADDR_RESET;
  data = psmRegWrite8(mvme, base_addr, reg_offset,1);
  return(data);
}




/*------------------------------------------------------------------*/
 INT psmFreqSweepReadLength( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT val;
 
  /* Frequency sweep length (16 bits) */
  reg_offset =  FREQ_SWEEP_LENGTH;
  val=psmRegRead(mvme, base_addr, reg_offset );  /* word */
  if(pdd)printf("Value read for freq sweep length=0x%x\n",val);
  val=val &  FREQ_SWEEP_MASK; /* 10 bits */
  printf("psmFreqSweepReadLength: frequency sweep length  =     0x%4.4x (%d)\n",val,val);
  return (val);
}

INT psmFreqSweepReadAddress( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT val;
 
  reg_offset =  FREQ_SWEEP_ADDRS;
  val=psmRegRead(mvme, base_addr, reg_offset );  /* word */
  val=val &  FREQ_SWEEP_MASK;  /* 10 bits */ 
  printf("psmFreqSweepReadAddress: frequency sweep address =     0x%4.4x (%d)\n",val,val);
  return(val);
}

/*------------------------------------------------------------------*/
 INT psmWrite_fREF_freq( MVME_INTERFACE *mvme, DWORD base_addr, const DWORD freq)
{
  /* freq is in HEX */
  DWORD temp;
  //volatile WORD * spec_Adr;

  WORD hi,lo;

  // Writing 32-bits total as two 16-bit words

  //spec_Adr = (WORD *)(A24 + base_addr + BASE_CONTROL_REGS + FREF_FREQ_MSW ); 
  // *spec_Adr++ = *((WORD *) &freq);
  // *spec_Adr++ = *((WORD *) &freq+1);

  lo = (WORD)(freq & 0x0000FFFF);
  hi = (WORD)(freq>>16);
 
  printf("psmWrite_fREF_freq: Input freq in hex: 0x%x;   MSW = 0x%x LSB = 0x%x \n",(unsigned int)freq,hi,lo);

  psmRegWrite(mvme, base_addr,  FREF_FREQ_MSW, hi);  // psmRegWrite adds BASE_CONTROL_REGS (0x9000)
  psmRegWrite(mvme, base_addr,  FREF_FREQ_LSW, lo);

  printf("Wrote 0x%x to offset 0x%x  (MSW)\n",hi, FREF_FREQ_MSW);
  printf("Wrote 0x%x to offset 0x%x  (LSW)\n",lo, FREF_FREQ_LSW);

  /* read it back */
  lo=psmRegRead(mvme, base_addr,  FREF_FREQ_LSW );  /* word */
  // if(pdd)
  printf("Read back LSW = 0x%x ",lo);
  temp = lo & 0xFFFF;

  hi=psmRegRead(mvme, base_addr,  FREF_FREQ_MSW );  /* word */

  //  if(pdd)
  printf(" and MSW = 0x%x\n",hi);
  hi=hi & 0xFFFF;
  temp = temp | (hi << 16);
  if(pdd)
    printf("Read back f REF freq tuning value = 0x%x \n",temp);
  return(temp);

}

/*------------------------------------------------------------------*/
 INT psmWrite_fREF_freq_Hz( MVME_INTERFACE *mvme, DWORD base_addr, const DWORD freq_Hz)
{
  /* Freq is in Hz;  returns freq in Hz */
  DWORD temp;
  //  volatile WORD * spec_Adr;
  DWORD freq;

  
  freq=get_hex(freq_Hz); /* convert to hex */
  if (freq == 0)
    {
      if(freq_Hz > 0)
	{
	  printf("psmWrite_fREF_freq_Hz: input freq %dHz out of range\n",freq_Hz);
	  return FAILURE; 
	}
    }

  printf("Frequency in Hz (%d) converted to hex frequency 0x%x\n",freq_Hz, freq);

  temp = psmWrite_fREF_freq( mvme, base_addr, freq);

  freq=get_Hz(temp);
  printf("Read back f REF freq tuning value = 0x%8.8x hex equivalent or %d Hz\n",temp,freq);
  return(freq);

}



/*------------------------------------------------------------------*/
/** psmWriteProfileReg(mvme, base_addr, char *my_profile_code, INT my_reg, INT value)
    Write a control register for a particular profile (1f,3f,5f,fREF or ALL for all profiles 

    @param base\_adr PSM VME base address
    @param my\_profile\_code pointer to character string (1f,3f,5f,fREF or ALL for all profiles )
    @param my\_reg control register number 1-6
    @param value  value to write

    returns  -1 =fail  or last data word written

    e.g.  psmWriteProfileReg (mvme, base_addr, "3f", 5, 0xF);
*/
 INT psmWriteProfileReg( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code, INT my_reg, INT value )
{
  DWORD base,offset,reg_offset;
  INT data;
  INT index,nprofile;
  //char my_profile_code[3];
  BOOL multi_flag;

  data=0;
  if(pdd)
    printf("psmWriteProfileReg: starting with init_flag=%d, profile_code=%s,my_reg=0x%x,value=0x%x\n",
	 init_flag,p_profile_code,my_reg,value);

  if(!init_flag)init();
  /* check to see if we are writing all profiles or just one */
  multi_flag=check_profile(p_profile_code);

  if (multi_flag)
    {  /* all profiles */
      if(pdd)printf("detected ALL profiles \n");
      nprofile=0;
    }
  else
    { /* one profile */
      if(pdd)printf("calling get_index with profile_code=%s\n",p_profile_code);
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;
    }

 index=my_reg-1;
 if(index < 0 || index > 5)
   {
     if(pdd)printf("control register code must be 1-6 inclusive\n");
     return FAILURE;
   }
 offset =  ProBaseRegs.control_reg_offset[index];

  while (nprofile < 4) /* 4 profiles; 1f,3f,5f,fREF */
    {
      base = ProBaseRegs.control_base[nprofile];
      reg_offset = base + offset;

      if(pdd)printf("psmWriteProfileReg:in loop, nprofile=%d, profile base=0x%x, offset=0x%x, reg_offset=0x%x\n",
		    nprofile,base,offset,reg_offset);
      if (offset >= PROFILE_IQ_DM_LENGTH)
	data = psmRegWrite(mvme, base_addr, reg_offset,value);  /* 16 bit write */
      else
	data = psmRegWrite8(mvme, base_addr, reg_offset,value);  /* 8 bit write */

      if(pdd)printf("psmWriteProfileReg: wrote 0x%x at reg_offset 0x%x;  read back value = 0x%x (%d)\n",
	     value,reg_offset,data,data);
      if (!multi_flag) break;
      nprofile++;
    }

  return data;
}

/*------------------------------------------------------------------*/
/** psmReadProfileReg(mvme, base_addr, char *my_profile_code, INT my_reg)
    Read a control register for a particular profile (1f,3f,5f,fREF or ALL for all profiles 

    @param base\_adr PSM VME base address
    @param my\_profile\_code pointer to character string (1f,3f,5f,fREF or ALL for all profiles )
    @param my\_reg control register number 1-6
    @param *pdata pointer to a 4-word array in which data is returned
    returns  -1 =fail  or 0 = success

    e.g.  psmReadProfileReg (mvme, base_addr, "3f", 5);
*/
 INT psmReadProfileReg( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code, 
			     INT my_reg)
{
  DWORD base,offset,reg_offset;
  INT data;
  INT index,nprofile;
  //char my_profile_code[3];
  BOOL multi_flag;

  /* pdata is a global */

  if(pdd)
    printf("psmReadProfileReg: starting with init_flag=%d, profile_code=%s,my_reg=0x%x\n",
	 init_flag,p_profile_code,my_reg);

  if(!init_flag)init();

  /* check to see if we are writing all profiles or just one */
  multi_flag=check_profile(p_profile_code);
  if (multi_flag)
    {  /* all profiles */
      if(pdd)printf("detected ALL profiles \n");
      nprofile=0;
      multi_flag=1; /* flag is true */
    }
  else
    { /* one profile */
      if(pdd)printf("calling get_index with profile_code=%s\n",p_profile_code);
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;
    }

 index=my_reg-1;
 if(index < 0 || index > 5)
   {
     printf("control register code must be 1-6 inclusive\n");
     return FAILURE;
   }
 offset =  ProBaseRegs.control_reg_offset[index];

 while (nprofile < 4) /* 4 profiles; 1f,3f,5f,fREF */
   {
     base = ProBaseRegs.control_base[nprofile];
     if(pdd)
       printf("in while loop, nprofile=%d and profile base = 0x%x\n",nprofile,base);      

      reg_offset = base + offset;
      if(pdd)printf("psmReadProfileReg: base=0x%x, offset=0x%x, reg_offset=0x%x\n",
		    base,offset,reg_offset);
      if (offset >= PROFILE_IQ_DM_LENGTH)
	data = psmRegRead(mvme, base_addr, reg_offset);  /* 16 bit read */
      else
	data = psmRegRead8(mvme, base_addr, reg_offset);  /* 8 bit read */
      if(pdd)
	printf("psmReadProfileReg: at offset 0x%x;  read value = 0x%x (%d)\n",
	     reg_offset,data,data);
      if (!multi_flag)
	{ 
	  pdata[0]=data;
	  return(SUCCESS);
	}
      pdata[nprofile]=data; /* remember the data */
      nprofile++;
   } /* end of while loop */
 return SUCCESS;
}

INT check_identical(void)
{
 /* called after a call to psmReadProfileReg with all profiles
    ... for "all", check whether registers are identical

    pdata is a global pointer to a 4 element integer array
    returns TRUE  if registers are identical, otherwise FALSE
 */
  int i;
  BOOL flag=TRUE;
  for (i=1; i<NUM_PROFILES; i++)
    {
      if ( pdata[i] != pdata [0] )
	{
	  flag=FALSE; /* clear the flag */
	  break;
	}
    }
 /* If not identical, print them out */
 if (!flag)
   {
     printf("check_identical: profile regs are NOT IDENTICAL\n");
     for (i=0; i<NUM_PROFILES; i++)
       printf(" Profile %s : value = 0x%x (%d)\n",
	      profile_string[i],pdata[i],pdata[i]);
   }
 else
   {
     if(pdd)printf("check_identical: all profile regs are identical, Value = %d\n",pdata[0]);
   }
 return flag;
}

/*------------------------------------------------------------------*/
/** psmIQWriteEndSweepMode(mvme, base_addr, char *my_profile_code,  INT value)
    Write to End Sweep Reg which contains bit pattern for a particular profile (1f,3f,5f,fREF or ALL for all profiles 

    @param base\_adr PSM VME base address
    @param my\_profile\_code pointer to character string (1f,3f,5f,fREF or ALL for all profiles )
    @param  end\_sweep\_ bit end sweep mode bit (0 or 1)


*/
INT psmIQWriteEndSweepMode( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code,  
			       INT end_sweep_bit)
{
  DWORD reg_offset;
  INT data,value;
  INT nprofile;
  //char my_profile_code[3];
  INT bit_pattern;

  if(!init_flag)init();
  if(pdd)printf("psmIQWriteEndSweepMode: starting with profile_code=%s,end sweep bit=0x%x\n",
	 p_profile_code,end_sweep_bit);

  reg_offset = END_SWEEP_CONTROL;
  /* read the register first */
  data = psmRegRead8(mvme, base_addr, reg_offset);
  data=data & 0x1F;
  if(pdd)printf("Read back end sweep control register as 0x%x\n",data);

  /* check to see if we are writing all profiles or just one */

  bit_pattern=0;
  if( check_profile(p_profile_code) == TRUE)
    {  /* all profiles */
      if(pdd)printf("detected ALL profiles \n");
      if(end_sweep_bit==1)
	data = data | 0xF; /* set bits 0-3  */
      else
	data = data & 0x10; /* clear bits 0-3  */
    }
  else
    { /* one profile */
      if(pdd)printf("calling get_index with profile_code=%s\n",p_profile_code);
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;
      data = bitset(data,nprofile,end_sweep_bit);
    }

  value = psmRegWrite8(mvme, base_addr, reg_offset,data);
  if(pdd)printf("value=0x%x\n",value);
  return(value);
}

/*------------------------------------------------------------------*/
/** psmWriteGateControl(mvme, base_addr, char *my_profile_code,  INT value)
    Write to Gate Control Reg which contains bit pattern for a particular profile (1f,3f,5f,fREF or ALL for all profiles 

    @param base\_adr PSM VME base address
    @param my\_profile\_code pointer to character string (1f,3f,5f,fREF or ALL for all profiles )
    @param  gate\_code\_bits  gate code (0,1,2,3) i.e. 2 bits


*/
INT psmWriteGateControl( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code,  
			       INT gate_code)
{
  DWORD reg_offset;
  INT data,value;
  INT nprofile,bit_to_set;
  //char my_profile_code[3];
  INT j,bit_val;

  if(!init_flag)init();
  if(pdd)printf("psmWriteGateControl: starting with profile_code=%s, gate code = %d(0x%2.2x)\n",
	 p_profile_code,gate_code,gate_code);

  if(gate_code <0 || gate_code > 3)
    {
      printf("psmWriteGateControl: gate code (%d) must be between 0 and 3\n",gate_code);
      return (FAILURE);
    }
  reg_offset = GATE_CONTROL_REG;
  /* read the register first */
  data = psmRegRead8(mvme, base_addr, reg_offset);
  if(pdd)printf("Read back gate control register as 0x%4.4x\n",data);

  /* check to see if we are writing all profiles or just one */
  
  
  if( check_profile(p_profile_code) == TRUE)
    {  /* all profiles */
      data = 0 ; /* all the bits will be overwritten */
      if(pdd)printf("detected ALL profiles \n");
      /* calculate bit pattern */
      
      for (j=0; j<4; j++)
	data=(data | gate_code<<(j*2)); /* value to write to the register */
      
    }
  else
    { /* one profile */
      if(pdd)printf("calling get_index with profile_code=%s\n",p_profile_code);
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;
      bit_val=(gate_code & 0x1);  /* bit 0 of gate code */
      bit_to_set = nprofile*2; /* nprofile goes from 0 for 1f to 3 for fref */
      if(pdd)printf("calling bitset with data=0x%2.2x. bit to set=0x%2.2x, value to set (0 or 1)=%d\n",
		    data,bit_to_set,bit_val);
      data = bitset(data,bit_to_set,bit_val);
      if(pdd)printf("after first bitset, data=0x%2.2x\n",data);
      bit_to_set++;
      bit_val =(gate_code & 0x2) >> 1; /* bit 1 of gate code */
      if(pdd)printf
	       ("Now calling bitset with  data=0x%2.2x. bit to set=0x%2.2x & value to set (0 or 1)=%d\n",
		data,bit_to_set,bit_val);            
      data = bitset(data,bit_to_set,bit_val);
      if(pdd)printf("after second bitset, data=0x%2.2x\n",data);

    }
  if(pdd)printf("PsmWriteGateControl:data to write is 0x%.2x\n",data);
  value = psmRegWrite8(mvme, base_addr, reg_offset,data);
  if(pdd)printf("value read back=0x%x\n",value);
  return(value);
}

/*------------------------------------------------------------------*/
/** psmFreqWriteEndSweepMode(mvme, base_addr,   INT value)
    Write to freq end sweep mode bit to End Sweep Reg without changing the rest of the bits

    @param base\_adr PSM VME base address
    @param  end\_sweep\_ bit end sweep mode bit (0 or 1)

*/
 INT psmFreqWriteEndSweepMode( MVME_INTERFACE *mvme, DWORD base_addr, INT end_sweep_bit)
{

  DWORD reg_offset;
  INT data,value;

  if(pdd)printf("psmFreqWriteEndSweepMode: starting with end sweep bit=0x%x\n",
	 end_sweep_bit);

  reg_offset = END_SWEEP_CONTROL;
  /* read the register first */
  data = psmRegRead8(mvme, base_addr, reg_offset);
  data = data & 0x1F;
  if(pdd)printf("Read back end sweep control register as 0x%x\n",data);

  data = bitset(data,4,end_sweep_bit); /* bit 4 */
  if(pdd)printf("Writing  0x%x to End Sweep Control Register at offset 0x%x \n",data,reg_offset);
  value = psmRegWrite8(mvme, base_addr, reg_offset,data);
  if(pdd)printf("value=0x%x\n",value);
  return(value);
}


INT bitset(INT data, INT bit, INT val)
{
  /* Set or clear a bit in a word while preserving original value of other bits
       - for  8 bits only 
                                    
       bit = bit (0-15) to be set/cleared ( = the shift value)   
       val = 0 to clear, 1 to set            
       data = word whose bit(s) is/are to be set/cleared

       e.g.  data = 0x10  bit =1  val=1    data-> 0x12
           data = 0x12    bit =4  val=0    data-> 0x02
 
  */
  INT mask;

  mask=1<<bit;
  if(pdd)printf("bitset: data=0x%x, mask = 0x%x, bit=%d val=0x%x\n",data,mask,bit,val);
  if(val)
    data= data | mask;
  else
    {
      mask = ~mask & 0xFF; 
      data=data & mask;
    }
  data=data & 0xFF; /* mask to 8 bits only */

  return data;
}



INT get_index(char *p_profile_code, INT *pindex)
{
  /* get the array index depending on profile (1f,3f,5f or fREF) 
   */

  //printf("get_index: starting with my_profile_code=%s\n",p_profile_code); 


  switch (p_profile_code[0])
    {
    case '1':
      *pindex=0;
      break;
    case '3':
      *pindex=1;
      break;
    case '5':
      *pindex=2;
      break;            
    case 'f':
    case 'F':
      *pindex=3;
      break;
    default:
      printf("Register profile (%s) must be one of 1f,3f,5f or fREF\n",p_profile_code);
      return (FAILURE);
    }
  return(SUCCESS);
}



/*------------------------------------------------------------------*/
/** psmLoadFreqDM_ptr
    Load frequency sweep data memory from a pointer
    @memo Load frequency sweep data memory.
    @param input base\_adr PSM VME base address
    @param input pointer to list of frequency values
    @param input number of frequency increments
    @param returns first frequency value (hex value)
    @return SUCCESS, FAILURE=File not found
*/
 INT psmLoadFreqDM_ptr( MVME_INTERFACE *mvme, DWORD base_addr, INT *pfreq , INT nfreq, DWORD *first_freq)
{
  //volatile WORD * spec_Adr;
  DWORD  freq,reg_offset;
  INT    counter,data;
  //char string[128];
  BOOL first;
  WORD hi,lo;
  DWORD my_offset;
  

  if(pdd)printf("psmLoadFreqDM_ptr: starting with pfreq=%p\n",pfreq);
  if(pfreq == NULL){
    printf("PSM freq sweep data could not be loaded as pointer is NULL\n"); 
    return FAILURE;    
  }
  if(nfreq <= 0){
    printf("PSM freq sweep data could not be loaded as number of values is invalid (%d)\n",nfreq); 
    return FAILURE;    
  }

  /*  for (i=0; i<nfreq; i++) 
    printf(" pfreq[%d] = 0x%x\n",i,pfreq[i]);
  */
  /*  first mem address in DM */
  //spec_Adr =  (WORD *)(A24 + base_addr + BASE_DM_FREQ_SWEEP);
  my_offset =  BASE_DM_FREQ_SWEEP;
  first=TRUE;
  counter=0;


  while (counter < nfreq)
    {
      freq = (DWORD)pfreq[counter];
      if(pdd)  printf("loading  freq[%d]= 0x%x \n",counter, freq);
      if(first)
	{
	  *first_freq = freq;  /* remember this value */
	  first=FALSE;
	}

      //  *spec_Adr++ = *((WORD *) &freq);
      //  *spec_Adr++ = *((WORD *) &freq + 1);

      lo = (WORD)(freq & 0x0000FFFF);
      hi = (WORD)(freq>>16);

      psmRegWrite(mvme, base_addr,  my_offset, hi);
      psmRegWrite(mvme, base_addr,  (my_offset+2), lo);
      my_offset+=4;
      counter++;
    }

  if(pdd)printf("Frequency range is 0x%x to 0x%x \n",*first_freq,freq);

 /* PSM needs length+1 */
  if(pdd)
    printf("Num freq.incr.=%d; PSM needs one extra for data length reg.\n",counter); 
  counter++;
  
  /* write this value to the freq sweep length register */
  reg_offset =  FREQ_SWEEP_LENGTH;
  if(pdd)printf("Now writing data length %d to freq sweep length reg (at offset=0x%x)\n",
	 counter,reg_offset);
  data = psmRegWrite(mvme, base_addr, reg_offset,counter);  /* 16 bit write */
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/** psmLoadFreqDM
    Load frequency sweep data memory from a file.
    @memo Load frequency sweep data memory.
    @param base\_adr PSM VME base address
    @param file frequency file
    @param returns no. of frequency increments
    @param returns first frequency value (hex value)
    @return SUCCESS, FAILURE=File not found
*/
 INT psmLoadFreqDM( MVME_INTERFACE *mvme, DWORD base_addr, char * file , DWORD *nfreq, DWORD *first_freq)
{
  //volatile WORD * spec_Adr;
  FILE  *psminput;
  DWORD  freq,reg_offset;
  INT    counter,data;
  //char string[128];
  BOOL first;
  WORD lo,hi;
  DWORD my_offset;


  psminput = fopen(file,"r");
  sleep(0.5); // sleep for 0.5s
  if(psminput == NULL){
    printf("PSM freq sweep data file \"%s\" could not be opened.\n", file); 
    return FAILURE;    
  }
  
  /*  first mem address in DM */
  //spec_Adr =  (WORD *)(A24 + base_addr + BASE_DM_FREQ_SWEEP);
  my_offset =  BASE_DM_FREQ_SWEEP;

  first=TRUE;
  counter=0;
  while (fscanf(psminput,"%x", &freq) != EOF)
    {
      if(pdd)  printf("loading  freq= 0x%x \n",freq);
      if(first)
	{
	  *first_freq = freq;  /* remember this value */
	  first=FALSE;
	}

      //   *spec_Adr++ = *((WORD *) &freq);
      //*spec_Adr++ = *((WORD *) &freq + 1);
      
      lo = (WORD)(freq & 0x0000FFFF);
      hi = (WORD)(freq>>16);

      psmRegWrite(mvme, base_addr,  my_offset, hi);
      psmRegWrite(mvme, base_addr,  (my_offset+2), lo);
      my_offset+=4; 

      counter++;
    }
  fclose(psminput);

  printf("Frequency range is 0x%x to 0x%x \n",*first_freq,freq);

  /* PSM needs length+1 */
  if(pdd) printf("Num freq.incr.=%d; PSM needs one extra for data length reg.\n",counter); 
  counter++;

  /* write this value to the freq sweep length register */
  reg_offset =  FREQ_SWEEP_LENGTH;
  printf("Now writing data length %d to freq sweep length reg (at offset=0x%x)\n",
	 counter,reg_offset);
  data = psmRegWrite(mvme, base_addr, reg_offset,counter);  /* 16 bit write */
  *nfreq=counter;
  return SUCCESS;
}


/*------------------------------------------------------------------*/
/** psmReadFreqDM
    Read back sweep data memory
    @memo Read back frequency sweep data memory.
    @param base\_adr PSM VME base address
    @param file frequency file
    @param returns last loaded frequency
    @return 1=SUCCESS, FAILURE=failure
*/
 INT psmReadFreqDM( MVME_INTERFACE *mvme, DWORD base_addr, DWORD num_values)
{
  /* read back num_values of the freqency DM starting at lowest value */

  DWORD  offset,data;
  INT    i;


  if(pdd)printf("reading %d freq values at Freq DM base address 0x%x\n",
		num_values,BASE_DM_FREQ_SWEEP);

  for(i=0; i<num_values; i++)
    {
      offset = i*4;
      if(pdd)printf("Calling psmReadOneFreq with offset =%d\n",offset);
      data=psmReadOneFreq(mvme, base_addr, offset);
      if(data == FAILURE) break;
	printf("Word %d: 0x%x (%d)\n",i,data,data);
    }
  return 0;

}


/*------------------------------------------------------------------*/
/** psmLoadIQ_DM
    Load a profile (i.e. 1f,3f,5f or fREF) I & Q  data memory from a file.
    add mods for PSM II 
    1f,fREF load i,q; 3f = q,i; 5f = -i,-q 

    Profile "all" is not supported - use  psmLoadIQ_DM_all
    Note that rf_config will have written a constant value in the file if odb
    parameters  "set constant i value" or "set constant q value" are true.

    @memo Load I & Q data memory with 2's compliment data
    @param base\_adr PSM VME base address
    @param file filename of I & Q data
    @param profile code
    @param returns number of values and last loaded I & Q pair
    @return 1=SUCCESS, FAILURE=File not found
*/
 INT psmLoadIQ_DM ( MVME_INTERFACE *mvme, DWORD base_addr, char *file , char *p_profile_code, 
			 DWORD *nvalues, DWORD *first_i, DWORD *first_q )
{
  //volatile WORD * spec_Adr;
  FILE  *psminput;
  // DWORD  I,Q;
  DWORD   i,q;
#ifdef HAVE_PSMII 
  DWORD   ii,qq;
#endif
  DWORD  dm_base,offset,control_base;
  INT ix,counter,data;
  BOOL first;
  //  char my_profile_code[3];
  BOOL multi_flag;
  DWORD my_offset;

  i=q=0;
#ifdef HAVE_PSMII 
  ii=qq=0;
#endif

  /* Note: data must be in 2s compliment */

  if(!init_flag)init();
  /* check to see if we are writing all profiles or just one */
  multi_flag=check_profile(p_profile_code);
  
  if (multi_flag)
    {  /* all profiles */
      printf("Loading \'ALL\' profiles not supported. Please use psmLoadIQ_DM_all\n");
      return FAILURE;
    }


  if( get_index( p_profile_code, &ix) ==FAILURE)
    return FAILURE; /* invalid profile code */
    
    
  dm_base = ProBaseRegs.dm_base[ix];
  control_base = ProBaseRegs.control_base[ix]; /* needed for writing length */


  psminput = fopen(file,"r");
  sleep(0.5);// sleep for 0.5 s
  if(psminput == NULL)
    {
      printf("psmLoadIQ_DM: PSM I & Q data file %s could not be opened.\n", file); 
      return FAILURE;    
    }
  else
    printf("psmLoadIQ_DM: PSM I & Q data file %s successfully opened\n", file); 


  /* The first I & Q pair might be loaded in the last memory 
     address (called IDLE) as well as in the 1st memory address

     - these values will be returned
 */

  //spec_Adr = (WORD *)(A24 + base_addr + MEMORY_BASE + dm_base );
  my_offset =  MEMORY_BASE + dm_base;

  counter=0; /* count how many pairs of values are loaded */
  first=TRUE;
#ifdef HAVE_PSMII 
  printf("psmLoadIQ_DM: loading PSM II : profile = %s\n",p_profile_code);
#endif  
  while (fscanf(psminput,"%d %d", &i,&q) != EOF)
    {
      /* printf("value from file: i=%d q=%d\n",i,q); */      
      
      /* data in file is in 2's compliment */
#ifdef HAVE_PSMII       
      /* fref and 1f load i,q, data from file */
      if(ix==0 || ix == 3)
	{
	  ii = i;
	  qq = q;
	}
      else if(ix==1)
	{ /* 3f loads q,i */
	  ii = q;
	  qq = i;
	}
      else 
	{ /* 5f loads -i,-q (0=-0) */
	  if (i == 0)
	    ii = 0;
	  else
	    ii = 1024-i;

	  if (q==0)
	    qq=0;
	  else
	    qq = 1024-q;
	}

      if(first)
	{ /* return the first values */
	  *first_i = ii;
	  *first_q = qq;
	}

      if(pdd)
	{
	  if(counter<10)
	    printf("Index=%d: 2s comp data  i,q = %d,%d  \n",
		   counter,ii,qq );
	}
      //  *spec_Adr++ = (WORD) ii;
      // *spec_Adr++ = (WORD) qq;
      psmRegWrite(mvme, base_addr,  my_offset, ii);
      psmRegWrite(mvme, base_addr,  my_offset+2, qq);
      my_offset+=4;

#else  /* regular PSM  */
      if(first)
	{ /* return the first values */
	  *first_i = i;
	  *first_q = q;
	  first = FALSE;
	}

      if(pdd)
	{
	  if(counter<10)
	    printf("Index=%d:  i =%d q=%d \n",counter,i,q);
	}
      // *spec_Adr++ = (WORD) i;
      // *spec_Adr++ = (WORD) q;

      psmRegWrite(mvme, base_addr,  my_offset, i);
      psmRegWrite(mvme, base_addr,  my_offset+2, q);
      my_offset+=4;

#endif
      counter++;
      if(counter > 2048)
	{
	  printf("Too many I,Q pairs in file. Maximum is 2048\n");
	  return FAILURE;
	}
    }
  fclose(psminput);
#ifdef HAVE_PSMII
  if(pdd) printf("Last I,Q pair  %d %d   \n",ii,qq);
#else
  if(pdd) printf("Last I,Q pair  %d %d   \n",i,q);
#endif
  printf("Number of IQ pairs loaded : %d\n",counter);
  *nvalues = counter;



  /* write counter to the length register */
  offset =  control_base + PROFILE_IQ_DM_LENGTH;
  if(pdd)printf("Writing length=%d to IQ pair length register (offset=0x%x)\n",counter,offset);
  data = psmRegWrite(mvme, base_addr, offset,counter);  /* 16 bit write */
  return 0;
}


/*------------------------------------------------------------------*/
/** psmLoadIQ_DM_all
    Load all profiles  I & Q  data memory from a file.
    Supports PSMII

    Note that rf_config will have written a constant value in the file if odb
    parameters  "set constant i value" or "set constant q value" are true.

    @memo Load I & Q data memory with 2's compliment data
    @param base\_adr PSM VME base address
    @param file filename of I & Q data
    @param profile code
    @param zero I values if true
    @param zero Q values if true
    @param returns no. of values loaded
    @param last loaded I value  (2's compliment)
    @param last loaded Q value (2's compliment)
    @return 1=SUCCESS, FAILURE=File not found
*/
 INT psmLoadIQ_DM_all ( MVME_INTERFACE *mvme, DWORD base_addr, char *file ,
			     DWORD *nvalues, DWORD *first_i, DWORD *first_q )
{
  /* volatile WORD * spec_Adr_1f;
  volatile WORD * spec_Adr_3f;
  volatile WORD * spec_Adr_5f;
  volatile WORD * spec_Adr_fREF; */
  int offset_1f;
  int offset_3f;
  int offset_5f;
  int offset_fREF;  


  FILE  *psminput;

  //  DWORD  I,Q;
  DWORD   i,q;
#ifdef HAVE_PSMII
  DWORD iNeg, qNeg;
#endif
  //  INT    status;
  DWORD  dm_base[4],offset,control_base[4];
  INT ix,counter,data;
  DWORD first;
  // BOOL multi_flag;
  if(!init_flag)init();

#ifdef HAVE_PSMII
  printf("psmLoadIQ_DM_all: loading I,Q pairs file %s into PSM II\n",file);
#else
  printf("psmLoadIQ_DM_all: loading I,Q pairs file %s into PSM\n",file);
#endif

  for (ix=0; ix < 4; ix++)
    {
      dm_base[ix] = ProBaseRegs.dm_base[ix];
      control_base[ix] = ProBaseRegs.control_base[ix]; /* needed for writing length */
    }

//  offset_1f = (WORD *)(A24 + base_addr + MEMORY_BASE + dm_base[0] );
//  offset_3f = (WORD *)(A24 + base_addr + MEMORY_BASE + dm_base[1] );
//  offset_5f = (WORD *)(A24 + base_addr + MEMORY_BASE + dm_base[2] );
//  offset_fREF = (WORD *)(A24 + base_addr + MEMORY_BASE + dm_base[3] );

  offset_1f = base_addr + MEMORY_BASE + dm_base[0] ;
  offset_3f = base_addr + MEMORY_BASE + dm_base[1] ;
  offset_5f = base_addr + MEMORY_BASE + dm_base[2] ;
  offset_fREF = base_addr + MEMORY_BASE + dm_base[3] ;

  if(pdd)
    {
      printf ("Addresses:           1f         3f         5f       fREF\n");
      printf ("dm_base            %4.4x       %4.4x       %4.4x       %4.4x\n",
	      dm_base[0],dm_base[1],dm_base[2],dm_base[3]);
      printf ("Cntr base offset     %2.2x         %2.2x         %2.2x         %2.2x\n",
	      control_base[0],control_base[1],control_base[2],control_base[3]);
      printf ("offset       %8.8x   %8.8x   %8.8x   %8.8x\n\n",
	      offset_1f,offset_3f,offset_5f,offset_fREF);
    }      
  

  psminput = fopen(file,"r");
  
  
  sleep(0.5); // sleep for 0.5 s
  if(psminput == NULL){
    printf("PSM I & Q data file %s could not be opened.\n", file); 
    return FAILURE;    
  }
  
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  
  /* The first I & Q pair might be loaded in the last memory 
     address (called IDLE) as well as in the 1st memory address
     
     - these values will be returned
  */
  
  
  if(pdd)
    printf("Writing first pair to first address at base_addr=0x%x  + offset = 0x%x\n",
	   (unsigned int)base_addr,(unsigned int)dm_base);
  
  counter=0; /* count how many pairs of values are loaded */
  first=TRUE;
  
  
  if(pdd)
    printf("Counter  i    q   \n");
  while (fscanf(psminput,"%d %d", &i,&q) != EOF)
    {  /* data is in 2's compliment */
      if(first)
	{ /* return the first values (2's complement) */
	  *first_i = i;
	  *first_q = q;
	  first = FALSE;
	}
      
      if(pdd)
	{
	  if(counter<10)
	    printf("%d     %d, %d \n",counter,i,q);
	}
      // *spec_Adr_1f++ = (WORD) i;
      // *spec_Adr_1f++ = (WORD) q;
      mvme_write_value(mvme, base_addr+offset_1f,(WORD) i );
      mvme_write_value(mvme, base_addr+offset_1f + 2,(WORD) q);

#ifdef HAVE_PSMII
		       // *spec_Adr_3f++ = (WORD) q;  /* 3f : reverse i,q */   
		       // *spec_Adr_3f++ = (WORD) i;
		       mvme_write_value(mvme, base_addr+offset_3f,(WORD) q ); /* 3f : reverse i,q */   
		       mvme_write_value(mvme, base_addr+offset_3f + 2,(WORD) q );
      /* 5f load -i,-q */
      if(i==0)
	iNeg=0;
      else
	iNeg = 1024 - i;
      if(q==0)
	qNeg=0;
      else
	qNeg = 1024 - q;
		       // *spec_Adr_5f++ = (WORD) iNeg ;  
		       // *spec_Adr_5f++ = (WORD) qNeg;

		       mvme_write_value(mvme, base_addr+offset_5f,(WORD) iNeg );
		       mvme_write_value(mvme, base_addr+offset_5f + 2,(WORD) qNeg ); 
#else
      /*  *spec_Adr_3f++ = (WORD) i;
      *spec_Adr_3f++ = (WORD) q;
      *spec_Adr_5f++ = (WORD) i;
      *spec_Adr_5f++ = (WORD) q; */

		       mvme_write_value(mvme, base_addr+ offset_3f,(WORD) i );
		       mvme_write_value(mvme, base_addr+offset_3f + 2,(WORD) q );
		       mvme_write_value(mvme, base_addr+offset_5f,(WORD) i );
		       mvme_write_value(mvme, base_addr+offset_5f + 2,(WORD) q );


#endif
		       //   *spec_Adr_fREF++ = (WORD) i;
		       //  *spec_Adr_fREF++ = (WORD) q;  
		       mvme_write_value(mvme, base_addr+offset_fREF,(WORD) i );
		       mvme_write_value(mvme, base_addr+offset_fREF + 2,(WORD) q );
   
      
      counter++;
     if(counter > 2048)
	{
	  printf("Too many I,Q pairs in file. Maximum is 2048\n");
	  return FAILURE;
	}
    }
  fclose(psminput);
  
  if(pdd) printf("Last I,Q pair %d  %d \n",i,q);
  printf("Number of IQ pairs loaded : %d\n",counter);
  *nvalues = counter;

  /* write counter to the length register */
  for (ix=0; ix < 4; ix++)
    {
      offset =  control_base[ix] + PROFILE_IQ_DM_LENGTH;
      if(pdd)printf("Writing length=%d to IQ pair length register (offset=0x%x)\n",counter,offset);
      data = psmRegWrite(mvme, base_addr, offset,counter);  /* 16 bit write */
    }
  return 0;
}

/*------------------------------------------------------------------*/
/** psmLoadOneIQ
    Load one IQ pair in a given memory location according to the profile_code. 
    Code "ALL" is supported

  Modified to include PSM II

  For PSM II, 
     if using "all" profiles, the data is assumed to be that for 1f (and fREF)
     The data will be reversed for 3f and made negative for 5f

     If using a single profile, the data will be loaded directly 
     (i.e. no sign change for 5f, no reversal for 3f ).


    @memo Load one IQ pair in a given memory location .
    @param base\_adr PSM VME base address 
    @param profile\_code (1f,3f,5f or fREF)
    @param I value
    @param Q value
    @param relative address in profile IQ memory
    @param TRUE if data is in 2's compliment format

    @return 1=SUCCESS 
*/
 INT psmLoadOneIQ( MVME_INTERFACE *mvme, DWORD base_addr, char * p_profile_code, DWORD I, DWORD Q, DWORD memadd, BOOL twos_comp)
{
  DWORD  offset,base,i,q;
#ifdef HAVE_PSMII
  DWORD my_i,my_q;
#endif
  INT    status, nprofile;
  BOOL multi_flag;
  //  char my_profile_code[5];


  if(!init_flag)init();
  
  if(pdd)
    printf("psmLoadOneIQ:Starting with base_addr=0x%x, profile=%s, I=%d, Q=%d, address=0x%x\n",
	 base_addr, p_profile_code, I,Q,memadd);

  if (memadd > 0x1FFC)
    { 
      printf("psmLoadOneIQ: bad address (too large)\n");
      return FAILURE;
    }
   else if (memadd == 0x1FFC)
     printf("psmLoadOneIQ: Information - about to load IDLE I&Q pair (%d,%d)\n",I,Q);


  /* memory address must be a multiple of 4 i.e. 0,4,8 etc )*/
  if ( (memadd & 0x3) != 0)
    {
      printf("psmLoadOneIQ: bad address (not a multiple of 4) %d (0x%x)\n",
	     memadd,memadd);
      return FAILURE;
    }

  /* check to see if we are writing all profiles or just one */
  multi_flag=check_profile(p_profile_code);
  if (multi_flag)
    {
      /* all profiles */
      if(pdd)printf(" psmLoadOneIQ: detected ALL profiles \n");
      nprofile=0;
      multi_flag=1; /* flag is true */
    }
  else
    { /* one profile */
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;  
    }

  while (nprofile < 4) /* 4 profiles; 1f,3f,5f,fREF */
    {   
      base = ProBaseRegs.dm_base[nprofile];
      if(pdd)printf("in while loop, nprofile=%d and  base = 0x%x\n",nprofile,base);
  
      offset = base + memadd;
      if(pdd)
	{
	  printf("psmLoadOneIQ: writing at base_addr=0x%x, profile offset=0x%x, memadd=0x%x\n",base_addr,base,memadd);
	  printf("      i.e. writing at base_addr + 0x%x\n",offset);
	}
      if(!twos_comp)
	{  /* convert the data */
	  i=TwosComp_convert(I,twos_comp);
	  q=TwosComp_convert(Q,twos_comp);
	  if(pdd)
	    printf("Data (I,Q=%d,%d) converted to 2's complement and masked, i,q = %d,%d (0x%x,0x%x)\n",
		   I,Q,i,q,i,q);
	}
      else
	{ /* data is already in 2s compliment */
	  i=I;
	  q=Q;
	}

#ifdef HAVE_PSMII
      /* PSM II */
      my_i=i; my_q=q;  /* default; write the values of i,q */
      printf("psmLoadOneIQ: loading I,Q pair (%d,%d) (2s comp) into PSMII\n",my_i,my_q);
    
      if(multi_flag)
	{
	  if(nprofile == 1) /* 3f reverse data for PSM II ( "all" profiles only) */
	    {
	      my_i = q;
	      my_q =i;
	    }
	  else if (nprofile == 2) /* 5f write -i, -q for PSM II ( "all" profiles only) */
	    {
	      if(my_i != 0)
		my_i = 1024 -i;
	      if(my_q != 0)
		my_q = 1024 -q;
	    }
	}
       if(pdd) 
	printf("PSM II... calling psmRegWriteDM with base_addr=0x%x, offset=%d, i value=%d\n",
		    base_addr,offset,my_i);	    
      status=psmRegWriteDM(mvme, base_addr,offset, my_i);
       if(pdd) 
	{
	  printf("After psmRegWriteDM, read back=%d\n",status);
	  printf("PSM II ...calling psmRegWriteDM with base_addr=0x%x, offset=%d, q value=%d\n",
		 base_addr,offset+2,my_q);
	}
     
      status=psmRegWriteDM(mvme, base_addr,offset+2, my_q);

      if(pdd)printf("PSM II After psmRegWriteDM, read back=%d\n",status);
      if (!multi_flag) break;
      nprofile++;
    }
#else
  /* Regular PSM  */
      if(pdd)printf("Calling psmRegWriteDM with base_addr=0x%x, offset=0x%x, i value=0x%x\n",
	     base_addr,offset,i);	    
      status=psmRegWriteDM(mvme, base_addr,offset, i);
      if(pdd)
	{
	  printf("After psmRegWriteDM, read back=0x%x\n",status);
	  printf("Calling psmRegWriteDM with base_addr=0x%x, offset=0x%x, value=0x%x\n",base_addr,offset+2,q);
	}
     
      status=psmRegWriteDM(mvme, base_addr,offset+2, q);

      if(pdd)printf("After psmRegWriteDM, read back=0x%x\n",status);
      if (!multi_flag) break;
      nprofile++;
    }
#endif

  if(pdd)printf("psmLoadOneIQ returning\n");
  return 1;
}
/*------------------------------------------------------------------*/
/** psmLoadOneFreq
    Load one frequency in a given memory location.
    @memo Load one frequency in a given memory location .
    @param base\_adr PSM VME base address 
    @param frequency value in hex
    @param relative address in profile IQ memory
    @return 1=SUCCESS 
*/
 INT psmLoadOneFreq( MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq, DWORD memadd)
{
  //  volatile WORD * spec_Adr;
  //  DWORD  outfreq;
  INT temp;
  DWORD my_offset;
  WORD hi,lo;

  // Frequency sweep data is 32 bits wide

  temp=BASE_DM_FREQ_SWEEP; //   0x08000
   if(pdd)
     printf("psmLoadOneFreq: writing hex freq=0x%x at psm base_addr=0x%x, base_DM_freq_sweep=0x%x, memadd=0x%x\n",
	   freq, base_addr, temp,memadd);
   if (memadd > 0xFFC)
    { 
      printf("psmLoadOneFreq: bad address (too large)\n");
      return FAILURE;
    }
   else if (memadd == 0xFFC)
     {
       if(pdd)
	 printf("psmLoadOneFreq: Information - about to load IDLE frequency\n");
     }
  /* memory address must be a multiple of 4 i.e. 0,4,8 etc )*/
  if ( (memadd & 0x3) != 0)
    {
      printf("psmLoadOneFreq: bad address (not a multiple of 4) %d (0x%x)\n",
	     memadd,memadd);
      return FAILURE;
    }

  if(pdd)printf("psmLoad0neFreq: writing at address: 0x%x + 0x%x + 0x%x =0x%x\n",
		base_addr,BASE_DM_FREQ_SWEEP,memadd, (base_addr + BASE_DM_FREQ_SWEEP + memadd))	;

  my_offset =  BASE_DM_FREQ_SWEEP + memadd;  // 0x8000
  // spec_Adr = (WORD *)(A24 + base_addr + BASE_DM_FREQ_SWEEP + memadd );

  if(pdd)  
    printf("Writing hex freq 0x%x at base 0x%x and offset 0x%x \n",
	   freq,temp ,memadd);

  //*spec_Adr++ = *((WORD *) &freq);
  //*spec_Adr++ = *((WORD *) &freq+1);
  
  lo = (WORD)(freq & 0x0000FFFF);
  hi = (WORD)(freq>>16);

  psmRegWriteDM(mvme, base_addr,  my_offset, hi);  // freq memory - lower address is MSB
  psmRegWriteDM(mvme, base_addr,  (my_offset+2), lo);// freq memory - higher address is LSB
 
  return 1;
}
/*------------------------------------------------------------------*/
/** psmReadOneFreq
    Read one frequency in a given memory location.
    @memo Read one frequency in a given memory location .
    @param base\_adr PSM VME base address 
    @param relative address in IQ memory
    @return 1=SUCCESS FAILURE=Invalid frequency
*/
 INT psmReadOneFreq( MVME_INTERFACE *mvme, DWORD base_addr, DWORD memadd)
{
  //volatile WORD * spec_Adr;
  DWORD msw,lsw;
  DWORD  freq;
  DWORD my_offset;

  if(pdd)
    printf("psmReadOneFreq: reading at base_addr=0x%x, base=0x%x, memadd=0x%x\n",
	   base_addr,BASE_DM_FREQ_SWEEP,memadd);
  
  if (memadd > 0xFFC)
    { 
      printf("psmReadOneFreq: bad address (too large)\n");
      return FAILURE;
    }
  else if (memadd == 0xFFC)
    if(pdd)
      printf("psmReadOneFreq: Information - about to read IDLE frequency\n");

  /* memory address must be a multiple of 4 i.e. 0,4,8 etc )*/
  if ( (memadd & 0x3) != 0)
    {
      printf("psmReadOneFreq: bad address (not a multiple of 4) %d (0x%x)\n",
	     memadd,memadd);
      return FAILURE;
    }
  if(pdd)printf("psmRead0neFreq: reading at address: 0x%x + 0x%x + 0x%x =0x%x\n",
		base_addr,BASE_DM_FREQ_SWEEP,memadd, (base_addr + BASE_DM_FREQ_SWEEP + memadd))	;

  my_offset =  BASE_DM_FREQ_SWEEP + memadd;  // 0x8000 +
  // spec_Adr = (WORD *)(A24 + base_addr + BASE_DM_FREQ_SWEEP + memadd );
  msw = psmRegReadDM(mvme, base_addr, my_offset);  // MSW at lower address
  lsw = psmRegReadDM(mvme, base_addr, (my_offset+ 2));
  // (WORD) msw = *spec_Adr++;
  // (WORD) lsw = *spec_Adr;
  msw = msw & 0xFFFF;
  lsw = lsw & 0xFFFF;
  freq = (msw << 16) | lsw; 
  //  if(pdd)
printf("psmReadOneFreq: MSW=0x%x; LSW=0x%x; Hex freq= 0x%x\n",msw,lsw,freq); 
  return freq;
}




/*------------------------------------------------------------------*/
/** psmReadOneIQ
    Read one IQ pair in a given memory location.
    @memo Read one IQ pair in a given memory location .
    @param base\_adr PSM VME base address 
    @param profile code (1f,3f,5f,fREF)
    @param I value
    @param Q value
    @param relative address in IQ  memory
    @param TRUE if I,Q values are to be returned in 2s compliment
    @return 1=SUCCESS FAILURE=error
*/
 INT psmReadOneIQ( MVME_INTERFACE *mvme, DWORD base_addr,  char * p_profile_code, DWORD *pI, DWORD *pQ,  DWORD memadd, BOOL *diff_flag, BOOL twos_comp)
{
  //  volatile WORD * spec_Adr;
  DWORD  i,q,base,I,Q;
  INT    offset,nprofile;
  //  char my_profile_code[3];
  BOOL multi_flag;
  INT II[4],QQ[4];

  i=q=I=Q=0;

  if(!init_flag)init();

  if (memadd > 0x1FFC)
    { 
      printf("psmReadOneIQ: bad address (too large)\n");
      return FAILURE;
    }
   else if (memadd == 0x1FFC)
     if(pdd)printf("psmReadOneIQ: Information - about to read IDLE I&Q pair\n");


  /* memory address must be a multiple of 4 i.e. 0,4,8 etc )*/
  if ( (memadd & 0x3) != 0)
    {
      printf("psmReadOneIQ: bad address (not a multiple of 4) %d (0x%x)\n",
	     memadd,memadd);
      return FAILURE;
    }


  /* check to see if we are reading all profiles or just one */
  *diff_flag = FALSE;  /* initialize */
  multi_flag=check_profile(p_profile_code);
  if (multi_flag)
    {  /* all profiles */
      if(pdd)printf(" psmReadOneIQ: detected ALL profiles \n");
      nprofile=0;
      multi_flag=1; /* flag is true */
    }
  else
    { /* one profile */
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;  
    }
  while (nprofile < 4) /* 4 profiles; 1f,3f,5f,fREF */
    {
      base = ProBaseRegs.dm_base[nprofile];
      offset = base + memadd;
      if(pdd)
	{
	  printf("psmReadOneIQ: reading at base_addr=0x%x, base=0x%x, memadd=0x%x\n",base_addr,base,memadd);
	  printf("   i.e. at offset = 0x%x\n",offset);
	}
      i = psmRegReadDM (mvme, base_addr, offset);
      q = psmRegReadDM (mvme, base_addr, offset+2);
      if(pdd)printf("Read back i = %d 0x%x and q = %d 0x%x  (2s comp)\n",i,i,q,q);

      if(!twos_comp) /* data must be converted back */
	{
	  I= TwosComp_convert(i, TRUE ); /* data read IS in 2's comp; convert it back */
	  Q= TwosComp_convert(q, TRUE); /* data  read IS in 2's comp; convert it back */
	   if(pdd)
	    printf("psmReadOneIQ: Converted 2's comp data read back i,q=%d,%d   -> I=%d and Q=%d\n",i,q,I,Q); 
	}
      else
	{/* leave data as 2's compliment */
	  I=i;
	  Q=q;
	   if(pdd)
	     printf("2's complement data:  I=%d 0x%x and Q=%d 0x%x\n",I,I,Q,Q);
	}

      if (!multi_flag) goto exit;
      II[nprofile]=I; /* remember these values if "all" */
      QQ[nprofile]=Q;
      nprofile++;    
    }
  

#ifdef HAVE_PSMII
  /* check the four profiles */
  printf("psmReadOneIQ: Checking values for PSM II   \n");
  /* PSM II  1f,fREF=i,q;  3f i,q -> q,i;  5f i,q=-i,-q
     
  index 0 1f; 1 3f; 2 5f; 3 fREF; 
  
  Check 3f: i,q values should be  reversed */
  if ( II[1] != QQ[0] && QQ[1] != II[0]   )   
    {
      *diff_flag= 1 ;  
      
      printf("psmReadOneIQ: I or Q values not reversed between 1f (I,Q=%d,%d) and 3f (I,Q=%d,%d)\n",
	     II[0],QQ[0],II[1],QQ[1]);
    }
  

  /* index 0 1f; 1 3f; 2 5f; 3 fREF;
     check 5f: i,q = -i,-q ( 0 is the same ) */
  if (  II[0] == 0 && II[2] == 0)
    {
      /* continue */
    }
  else
    {
      if(twos_comp)
	{
	  if ( (II[2] != 1024 -II[0])) 
	    {
	      printf("psmReadOneIQ: I values not sign reversed between 1f (I=%d) and 5f (I=%d) (values in 2s comp)\n",
		   II[0],II[2]);
	      *diff_flag= 1 ;
	    } 
	}
      else   /* not 2s compliment data */
	{ 
	  if(II[2] != II[0] *-1 ) 
	    {
	      printf("psmReadOneIQ: I values not sign reversed between 1f (I=%d) and 5f (I=%d)\n",
		     II[0],II[2]);
	      *diff_flag= 1 ;  
	    }
	}
    }
  
  if (  QQ[0] == 0 && QQ[2] == 0)
    {
      /* continue */
    }
  else
    {      
      if(twos_comp)
	{
	  if (QQ[2] != 1024- QQ[0])
	    { 
	      printf("psmReadOneIQ: Q values not sign reversed between 1f (Q=%d) and 5f (Q=%d) (2s comp)\n",
		     QQ[0],QQ[2]);  
	      *diff_flag= 1 ;
	    }  
	}
      else   /* not 2s compliment data */
	{
	  if(QQ[2] != QQ[0] * -1) 
	    {
	      printf("psmReadOneIQ: Q values not sign reversed between 1f (Q=%d) and 5f (Q=%d)\n",
		     QQ[0],QQ[2]);
	      *diff_flag= 1 ;
	    }  
	}
    }
#else
  /* for "all", check whether values are identical */
  for (i=1; i<4; i++)
    {
      if ( II[i] != II[0]   )
	{
	  *diff_flag=TRUE; /* set the flag */
	  if(pdd)
	    printf("psmReadOneIQ: Warning - I values not identical between profiles index 0 and %d: I=%d & I=%d \n",
		   i,II[0],II[i]);
	  break;
	}
      if ( QQ[i] != QQ[0])
	{
	  *diff_flag=TRUE; /* set the flag */
	  if(pdd)
		printf("psmReadOneIQ: Warning - Q values not identical between profiles 0 and %d: Q=%d & %d \n",
		       i,QQ[0],QQ[i]);
	  break;
	}
    }
#endif
    
  /* If not identical, print them out */
  if (*diff_flag)
    {
#ifdef HAVE_PSMII
      printf("psmReadOneIQ: I&Q pairs do not check out for PSM II for all profiles\n");
#else
      printf("psmReadOneIQ: I&Q pairs are NOT IDENTICAL for all profiles\n");
#endif
      printf("    Profile:  %5s    %5s    %5s    %5s\n",profile_string[0],profile_string[1],
	     profile_string[2],profile_string[3]);
	 
      printf("   I,Q =   %4d,%4d  %4d,%4d  %4d,%4d  %4d,%4d\n",
	     II[0],QQ[0],II[1],QQ[1],II[2],QQ[2],II[3],QQ[3]);
    }
 exit:
  if(pdd)printf("psmReadOneIQ: returning I=%d (0x%x);  Q=%d (0x%x)\n",I,I,Q,Q);

  /* for ALL, return the last values read */
  *pI=I; 
  *pQ=Q;
    
  return 1;
}

#ifndef HAVE_PSMII
/*------------------------------------------------------------------*/
/** psmReadIQ_DM

Regular PSM Version

    Read  IQ pairs starting at a given memory location
    @memo Read IQ pairs starting at a given memory location .
    @param base\_adr PSM VME base address 
    @param profile code (1f,3f,5f,fREF or all)
    @param Last I value (2's compliment)
    @param Last Q value (2's compliment)
    @param relative starting address in IQ  memory
    @param no. of IQ pairs to read
    @param flag set if values are not identical for all profiles
    @param print_data if true, print out the contents of the memory
    @param filename if values are to be checked against a loadfile
    @return 1=SUCCESS -1=error -2=filecheck failed -3=length not same as IQlen register (ALL only)
*/
 INT psmReadIQ_DM( MVME_INTERFACE *mvme, DWORD base_addr,  char * p_profile_code, DWORD *pI, DWORD *pQ, 
             DWORD memadd, INT length, BOOL *diff_flag, BOOL print_data, char *loadfile)
{
  //  volatile WORD * spec_Adr;
  DWORD  i,q,base,pointer;
  /* DWORD I,Q  not used; 2's compliment only now */
  INT    status,offset,nprofile;
  INT k,j;
  //  char my_profile_code[3];
  BOOL multi_flag;
  INT II[4],QQ[4];
  FILE  *psminput; 
  //  char str[128];
  DWORD fi,fq,fcounter;
  INT my_len,nq_check,ni_check;

  /*  Regular PSM Version  */
  if(!init_flag)init();

  if (memadd > 0x1FFC)
    { 
      printf("psmReadIQ_DM: bad address (too large)\n");
      return FAILURE;
    }
   else if (memadd == 0x1FFC)
     {
       printf("psmReadIQ_DM: Information - about to read IDLE I&Q pair\n");
       length =1;
     }

  /* memory address must be a multiple of 4 i.e. 0,4,8 etc )*/
  if ( (memadd & 0x3) != 0)
    {
      printf("psmReadIQ_DM: bad address (not a multiple of 4) %d (0x%x)\n",
	     memadd,memadd);
      return FAILURE;
    }

  ni_check=nq_check=0;
  fcounter=0;
  if(strlen(loadfile)==0)
    {
      printf("psmReadIQ_DM: no check against file will be done\n");
      psminput = NULL;
    }
  else
    {
      psminput = fopen(loadfile,"r");
      sleep(0.5); // sleep for 0.5 s
      if(psminput == NULL)
	{
	  printf("psmReadIQ_DM: PSM freq sweep data file \"%s\" could not be opened.\n", loadfile); 
	  printf("psmReadIQ_DM: no check against file can be done\n");
	}
    }

  if(pdd)printf("psmReadIQ_DM: reading %d words for profile \"%s\" starting at 0x%x\n",
	 length,p_profile_code,memadd); 

  /* check to see if we are reading all profiles or just one */
  *diff_flag = FALSE;  /* initialize */
  multi_flag=check_profile(p_profile_code);

  if (multi_flag)
    {  /* all profiles */
      if(pdd)printf(" psmReadIQ_DM: detected ALL profiles \n");
      nprofile=0;
      multi_flag=1; /* flag is true */
    }
  else
    { /* one profile */
      if(pdd)printf("psmReadIQ_DM:Multi flag is false\n");
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;  
      if(pdd)printf("psmReadIQ_DM: Profile %s is enabled\n",p_profile_code);
    }

 /* for every address... */
  for (k=0; k<length; k++)
    {
      pointer = 4 * k; 
      if ( (pointer + memadd) >  0x1FFC)
	{
	  printf("pointer= %d (0x%x), index=%d exceeded memory (max=0x1FFC) .... exiting \n",
		 pointer,pointer,k);
	  goto exit;  /* reached the top of the memory (idle) */
	}
      if( multi_flag)
	  nprofile=0;

      while (nprofile < 4) /* 4 profiles; 1f,3f,5f,fREF */
	{	  
	  base = ProBaseRegs.dm_base[nprofile];
	  offset = base +  pointer + memadd;
	  /*  if(pdd) 
	    {
	      printf("psmReadIQ_DM: nprofile=%d starting at base_addr=0x%x, base=0x%x, memadd=0x%x, pointer=0x%x\n",
		     nprofile,base_addr,base,memadd,pointer);
	      printf("   i.e. at offset = 0x%x\n",offset);
	      }	 too much output */    
	  i = psmRegReadDM (mvme, base_addr, offset);
	  q = psmRegReadDM (mvme, base_addr, offset+2);
	  /* if(pdd)
	     printf("Read back i = %d and q = %d \n",i,q); */
	
	  if (!multi_flag)
	    {
	      if(print_data)
		printf("index=%d pointer=%d    i= %d  q=%d \n",k,pointer,i,q);
	      goto cont;
	    }
	  II[nprofile]=i; /* remember these values if "all" */
	  QQ[nprofile]=q;
	  nprofile++;    
	}

      /* for "all", check whether values are identical */
      for (j=1; j<4; j++)
	{
	  if ( II[j] != II[0]   )
	    {
	      *diff_flag=TRUE; /* set the flag */
	      if(pdd)
		printf("psmReadIQ_DM: Warning - I values not identical between profiles 0x%x & 0x%x \n",
		       II[0],II[j]);
	      break;
	    }
	  if ( QQ[j] != QQ[0])
	    {
	      *diff_flag=TRUE; /* set the flag */
	      if(pdd)
		printf("psmReadIQ_DM: Warning - Q values not identical between profiles 0x%x & 0x%x \n",
		       QQ[0],QQ[j]);
	      break;
	    }
	}
      /* If not identical, print them out.. too much data */
      /*     if (*diff_flag)
	{
	  printf("psmReadIQ_DM: I&Q pairs are NOT IDENTICAL for all profiles\n");
	  for (j=0; j<4; j++)
	    printf(" Profile %s : I = %d  ;  Q = %d \n",
		   profile_string[j],II[j],QQ[j]);
	}
      else //  they are identical so print them now */

      if(print_data)
	printf("index=%d  pointer=%4.4d  I,Q pair = %d,%d (all profiles identical) \n",
	       k,pointer,i,q);
	
cont:
      if(  psminput != NULL)
	{
	  if(fscanf(psminput,"%d %d", &fi,&fq) != EOF)
	    {
	      if(i != fi )
		{
		  printf("i value from file for index %d is %d; read i as %d\n",fcounter,fi,i);
		  ni_check++;
		}
	      if(q != fq)
		{
		  printf("q value from file for index %d is %d; read q as %d\n",fcounter,fq,q);
		  nq_check++;
		}
	      if(i==fi && q==fq)
		{
		  /*  if(pdd)
		      printf("i,q values from file for index %d is %d,%d; read as %d,%d\n",
		      fcounter,fi,fq,i,q); takes too long */
		}
	      fcounter++;
	    }
	}
      continue;
    }


exit:
  status = 1; /* success (default) */
  if(psminput)
    {
      /* check some extra things for the file case... including the I,Q length register */
      fclose(psminput);
      if(length != fcounter)
	printf("psmReadIQ_DM: file length %d does not match read length requested (%d)\n",fcounter,length);
      
      if(multi_flag)
	{
	  my_len = psmReadIQlen (mvme, base_addr); /* all profiles enabled*/
	  if(my_len != length)
	    {
	      printf("psmReadIQ_DM: IQ length register is set to %d; expect %d\n",my_len, fcounter);
	      status = -3;
	    }
	}
      
      if( ni_check == 0 && nq_check ==0 )
	{
	  if(pdd)printf("psmReadIQ_DM: success... all i,q pairs read back the same as in load file\n");
	  status=1; /* success */
	}
      else
	{
	  printf("psmReadIQ_DM: %d i values and %d q values did not agree with load file\n",
		 ni_check,nq_check);
	  status =-2; /* file check failed */
	}
      
    }
  
  if(pdd)printf("psmReadIQ_DM: returning i=%d (0x%x);  q=%d (0x%x)\n",i,i,q,q);
  *pI=i;  /* return the last values read */
  *pQ=q;
  return status;
}

#else
/* PSM II */  
/*------------------------------------------------------------------*/
/** psmReadIQ_DM

PSM II version

    Read  IQ pairs starting at a given memory location
    @memo Read IQ pairs starting at a given memory location .
    @param base\_adr PSM VME base address 
    @param profile code (1f,3f,5f,fREF or all)
    @param Last I value (2's compliment)
    @param Last Q value (2's compliment)
    @param relative starting address in IQ  memory
    @param no. of IQ pairs to read
    @param flag set if values are not identical for all profiles
    @param1 print_data if true, print out the contents of the memory;
    @param filename if values are to be checked against a loadfile
    @return 1=SUCCESS -1=error -2=filecheck failed -3=length not same as IQlen register (ALL only)
*/
 INT psmReadIQ_DM( MVME_INTERFACE *mvme, DWORD base_addr,  char * p_profile_code, DWORD *pI, DWORD *pQ, 
			DWORD memadd, INT length, BOOL *diff_flag, BOOL print_data, char *loadfile)
{
  //  volatile WORD * spec_Adr;
  DWORD  i,q,base,pointer;
  /* DWORD I,Q  not used; 2's compliment only now */
  INT    status,offset,nprofile;
  INT k;
  //char my_profile_code[3];
  BOOL multi_flag;
  INT II[4],QQ[4];
  FILE  *psminput; 
  DWORD fi,fq,fcounter;
  INT my_len,nq_check,ni_check;
  
  i=q=0;

  /*   PSM II  Version */
  if(!init_flag)init();
  
  if (memadd > 0x1FFC)
    { 
      printf("psmReadIQ_DM: bad address (too large)\n");
      return FAILURE;
    }
  else if (memadd == 0x1FFC)
    {
      printf("psmReadIQ_DM: Information - about to read IDLE I&Q pair\n");
      length =1;
    }
  
  /* memory address must be a multiple of 4 i.e. 0,4,8 etc )*/
  if ( (memadd & 0x3) != 0)
    {
      printf("psmReadIQ_DM: bad address (not a multiple of 4) %d (0x%x)\n",
	     memadd,memadd);
      return FAILURE;
    }
  
  ni_check=nq_check=0;
  fcounter=0;
  if(strlen(loadfile)==0)
    {
      printf("psmReadIQ_DM: no check against file will be done\n");
      psminput = NULL;
    }
  else
    {
      psminput = fopen(loadfile,"r");
      sleep(0.5); // sleep for 0.5 s
      if(psminput == NULL)
	{
	  printf("psmReadIQ_DM: PSM freq sweep data file \"%s\" could not be opened.\n", loadfile); 
	  printf("psmReadIQ_DM: no check against file can be done\n");
	}
    }
  
  printf("psmReadIQ_DM: reading %d words for profile \"%s\" starting at 0x%x; print_data=%d\n",
	 length,p_profile_code,memadd,print_data); 
  
  /* check to see if we are reading all profiles or just one */
  *diff_flag = FALSE;  /* initialize */
  multi_flag=check_profile(p_profile_code);
  
  if (multi_flag)
    {  /* all profiles */
      printf(" psmReadIQ_DM: detected ALL profiles \n");
      nprofile=0;
      multi_flag=1; /* flag is true */
    }
  else
    { /* one profile */
      printf("psmReadIQ_DM:Multi flag is false\n");
      if( get_index( p_profile_code, &nprofile ) ==FAILURE) 
	return FAILURE ;  
      printf("psmReadIQ_DM: Profile %s is enabled\n",p_profile_code);
    }
  
  /* for every address... */
  for (k=0; k<length; k++)
    {
      pointer = 4 * k; 
      if ( (pointer + memadd) >  0x1FFC)
	{
	  printf("pointer= %d (0x%x), index=%d exceeded memory (max=0x1FFC) .... exiting \n",
		 pointer,pointer,k);
	  goto exit;  /* reached the top of the memory (idle) */
	}
      if( multi_flag)
	nprofile=0;
      
      while (nprofile < 4) /* 4 profiles; 1f,3f,5f,fREF */
	{	  
	  base = ProBaseRegs.dm_base[nprofile];
	  offset = base +  pointer + memadd;
	  i = psmRegReadDM (mvme, base_addr, offset);
	  q = psmRegReadDM (mvme, base_addr, offset+2);
	  /* if(pdd)
	     printf("Read back i = %d and q = %d \n",i,q); */
	  
	  if (!multi_flag)
	    {
	      if(print_data)
		printf("Index=%d, pointer=%d  2s comp data  i,q = %d,%d -> %d,%d \n",
		       k,pointer,i,q,  
		       TwosComp_convert(i,1),
		       TwosComp_convert(q,1) );
	      goto cont;
	    }
	  II[nprofile]=i; /* remember these values if "all" */
	  QQ[nprofile]=q;
	  nprofile++;    
	}

/* PSM II differences start here */      
      
      /* index 0 1f; 1 3f; 2 5f; 3 fREF; */

      /* 1f profile and fREF loaded with i,q; 3f with q,i; 5f with -i,-q */
      if ( II[3] != II[0] &&   QQ[3] != QQ[0] )
	{
	  *diff_flag= 1 ;  
	  
	  printf("psmReadIQ_DM: I or Q values not identical between 1f (I,Q=%d,%d) and fREF (I,Q=%d,%d) at index %d\n",
		 II[0],QQ[0],II[3],QQ[3],k);
	}
      /* index 0 1f; 1 3f; 2 5f; 3 fREF;
      check 3f: i,q values should be  reversed */
      if ( II[1] != QQ[0] && QQ[1] != II[0]   )   
	{
	  *diff_flag= 1 ;  
	  
	  printf("psmReadIQ_DM: I or Q values not reversed between 1f (I,Q=%d,%d) and 3f (I,Q=%d,%d) at index %d\n",
		 II[0],QQ[0],II[1],QQ[1],k);
	}

      /* index 0 1f; 1 3f; 2 5f; 3 fREF;
      check 5f: i,q = -i,-q ( 0 is the same ) */
      if ( QQ[0] == 0 && QQ[2] == 0)
	{
	  /* continue */
	}
      else
	{
	  if  (QQ[2] != 1024- QQ[0])      
	    {
	      printf("psmReadIQ_DM: Q values not sign reversed between 1f (Q=%d) and 5f (Q=%d) at index %d\n",
		     QQ[0],QQ[2],k);
	      *diff_flag= 1 ;  
	    }
	}
     if ( II[0] == 0 && II[2] == 0)
	{
	  /* continue */
	}
      else
	{
	  if ( (II[2] != 1024 -II[0])   )   
	    {
	      printf("psmReadIQ_DM: I values not sign reversed between 1f (I=%d) and 5f (I=%d) at index %d\n",
		     II[0],II[2],k);
	      *diff_flag= 1 ;  
	    }
	}
      if ( print_data )
	{ /*  print them if print_data is true */
	  
	  printf("Index:%d;  %.4s: I,Q = %d,%d;  %.4s : I,Q = %d,%d;  %.4s : I,Q = %d,%d;  %.4s : I,Q = %d,%d; \n",
		 k,profile_string[0],II[0],QQ[0],
		 profile_string[1],II[1],QQ[1],
		 profile_string[2],II[2],QQ[2],
		 profile_string[3],II[3],QQ[3]);
	  
	}
cont:
      if(  psminput != 0 )
	{
	  if(fscanf(psminput,"%d %d", &fi,&fq) != (FAILURE) )
	    {
	      /* check directly against file for 1f, fREF 
		 for multi_flag, profiles have been checked against 1f */  
	      if ( (multi_flag) || (nprofile == 0) || (nprofile == 3) )
		{
		  if(i != fi )
		    {
		      printf("i value from file for index %d is %d; read i as %d\n",fcounter,fi,i);
		      ni_check++;
		    }
		  if(q != fq)
		    {
		      printf("q value from file for index %d is %d; read q as %d\n",fcounter,fq,q);
		      nq_check++;
		    }
		}
	      else if (nprofile ==1 )
		{  /* 3f profile only i,q reversed */
		  if(i != fq )
		    {
		      printf("q value from file for index %d (3f) is %d; read i as %d\n",fcounter,fq,i);
		      ni_check++;
		    }
		  if(q != fi)
		    {
		      printf("i value from file for index %d (3f)is %d; read q as %d\n",fcounter,fi,q);
		      nq_check++;
		    }
		}
	      else		 
		{  /* this is profile 5f  */
		  if(i == 0)
		    {
		      if(fi != 0)
			{
			  printf("file i value at index %d (5f) is %d; read i as %d, expect 0\n",fcounter,fi,i);
			  ni_check++;
			}
		    }
		  else if(i != 1024-fi )
		    {
		      printf("file i value at index %d (3f) is %d; read i as %d,expect -i=%d\n",
			     fcounter,fi,i,(1024-i));
		      ni_check++;
		    }
		  if(q == 0)
		    {
		      if(fq != 0)
			{
			  printf("file q value at index %d (5f) is %d; read q as %d, expect 0\n",
				 fcounter,fq,q);
			  nq_check++;
			}
		    }
		  else if(q != 1024-fq )
		    {
		      printf("file q value at index %d (3f) is %d; read q as %d,expect -q=%d\n",
			     fcounter,fq,q,(1024-q));
		      nq_check++;
		    }
		  
		  
		} /* end of 5f only */
	      fcounter++;
	    } /* fscanf */  
	} /* psminput */
continue;
    }  


exit:
  status = 1;  
  if(psminput)
    {
      
      fclose(psminput);
      if(length != fcounter)
	printf("psmReadIQ_DM: file length %d does not match read length requested (%d)\n",fcounter,length);
      
      if(multi_flag)
	{
	  my_len = psmReadIQlen (mvme, base_addr);  
	  if(my_len != length)
	    {
	      printf("psmReadIQ_DM: IQ length register is set to %d; expect %d\n",my_len, fcounter);
	      status = -3;
	    }
	}
      
      if( ni_check == 0 && nq_check ==0 )
	{
	  printf("psmReadIQ_DM: all i,q pairs check back with load file (this is PSM II)\n");
	  status=1;  
	}
      else
	{
	  printf("psmReadIQ_DM: %d i values and %d q values did not check correctly with load file (this is PSM II)\n",
		 ni_check,nq_check);
	  status =-2;  
	}
      
    }
  
  printf("psmReadIQ_DM: returning i=%d (0x%x);  q=%d (0x%x)\n",i,i,q,q);
  *pI=i;   
  *pQ=q;
  return status;
}

#endif /* PSM II */  
  

/*------------------------------------------------------------------*/


 INT psmSetQM_On ( MVME_INTERFACE *mvme, DWORD base_addr)
{
  /* Set Quadrature Modulation Mode bit ON in Reg1 for all profiles
     All profiles are expected to be  set up the same way
  */
 
  INT data,value;

  /* read the registers first (pdata & array are globals)*/
  status = psmReadProfileReg(mvme, base_addr,"all",PROFILE_REG2);
  if(status == FAILURE)
    {
      printf("error return from psmReadProfileReg\n");
      return FAILURE;
    }
  else
    {
      if(check_identical() )	
	{
	  data = array[0];
	  printf("psmSetQM_On: all profile REG2 registers read 0x%x\n",data);
	}
      else
	{
	  printf("psmSetQM_On: all profile REG2 registers  must read back the same value\n");
	  return FAILURE;
	}  
      
    }
  
  data = ( data | SINGLE_TONE_MODE );  /* set the bit */
  data = data & REG2_MASK;
  value = psmWriteProfileReg(mvme, base_addr,"all",PROFILE_REG2,data);
  if (value != FAILURE)
    printf("psmSetQM_On: all profile REG2 registers now read 0x%x\n",value);
  
  return value;
}

 INT psmSetQM_Off ( MVME_INTERFACE *mvme, DWORD base_addr)
{
  /* Set Quadrature Modulation Mode bit OFF in Reg1 for all profiles
     All profiles are expected to be  set up the same way
  */
 
  INT data,value;


  /* read the registers first (pdata and array are globals) */
   status = psmReadProfileReg(mvme, base_addr,"all",PROFILE_REG2);
   if(status == FAILURE)
     {
       printf("error return from psmReadProfileReg\n");
       return FAILURE;
     }
   else
     {
       if(check_identical())
	 {	 
	   data = array[0];
	   printf("psmSetQM_Off: all profile REG2 registers read 0x%x\n",data);
	 }
       else
	 {
	   printf("psmSetQM_Off: all profile REG2 registers  must read back the same value\n");
	   return FAILURE;
	 } 
       
     }
   data = data & ~SINGLE_TONE_MODE ; /* clear the bit */
   data = data & REG2_MASK;
   value = psmWriteProfileReg(mvme, base_addr,"all",PROFILE_REG2,data);
   if(value != FAILURE)
     printf("psmSetQM_Off: all profile REG2 registers now read 0x%x\n",value);
   
   return value;
}


 INT psmSetIQlen ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD value)
{
  /* Load the I&Q Data Memory length register (Reg5) for all profiles
  */
 
  INT data;
  value = value &  IQ_DATA_MEMORY_LENGTH ; /* mask away unused bits */
  data = psmWriteProfileReg(mvme, base_addr,"all",PROFILE_REG5,value);
  if(data != FAILURE)
    {
      data = data &  IQ_DATA_MEMORY_LENGTH ; /* mask away unused bits */
      if(pdd)printf("psmSetIQlen: all profile REG5 registers now read %d (0x%x)\n",data,data);
    }
  return data;
}


 INT psmSetScaleFactor ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD value)
{
  /* Load the Output Scale Factor register (Reg4) for all profiles
  */
 
  INT data;

  data = psmWriteProfileReg(mvme, base_addr,"all",PROFILE_REG4,value);
  if(data != FAILURE)
  if(pdd)printf("psmSetScaleFactor: all profile REG4 registers now read %d (0x%x)\n",data,data);

  return data;
}

 INT psmReadIQlen ( MVME_INTERFACE *mvme, DWORD base_addr)
{
  /* Read the I&Q Data Memory length register (Reg5) for all profiles
  */
 
  INT data;
  

  status = psmReadProfileReg(mvme, base_addr,"all",PROFILE_REG5);
  if(status == FAILURE) 
    {
      printf("psmReadIQlen: failure from psmReadProfileReg\n");
      return FAILURE;
    }
  else
 {
      if(check_identical() )	
	{
	  data = array[0];
	  data = data &  IQ_DATA_MEMORY_LENGTH ; /* mask away unused bits */
	  if(pdd)printf("psmReadIQlen: all profile REG5 registers read back %d (0x%x)\n",data,data);
	}
      else
	{
	  printf("psmReadIQlen: profile REG5 registers are NOT IDENTICAL\n");
	  return FAILURE;
	}
 }

  return data;
}


 INT psmReadIQptr ( MVME_INTERFACE *mvme, DWORD base_addr)
{
  /* Read the I&Q Data Memory pointer register (Reg6) for all profiles
  */
 
  INT data;
  

  status = psmReadProfileReg(mvme, base_addr,"all",PROFILE_REG6);
  if(status == FAILURE) 
    {
      printf("psmReadIQptr: failure from psmReadProfileReg\n");
      return FAILURE;
    }
  else
 {
      if(check_identical() )	
	{
	  data = array[0];
	  data = data &  IQ_DATA_MEMORY_ADDRESS ; /* mask away unused bits */
	  if(pdd)printf("psmReadIQptr: all profile REG6 registers read 0x%x\n",data);
	}
      else
	{
	  printf("psmReadIQptr: profile REG6 registers are NOT IDENTICAL\n");
	  return FAILURE;
	}
 }

  return data;
}



 INT psmSetCIC_Rate ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD ival)
{
  /* Set CIC Interpolating rate bits in Reg3 for all profiles
     All profiles are expected to be  set up the same way
  */
 
  INT data,value;


  if(ival < 1 || ival >63)
    {
      printf("psmSetCIC_Rate: invalid CIC rate (%d); 2-63 is valid,1=bypass\n",ival);
      return FAILURE;
    }
  
  /* read the registers first */
  data = psmReadProfileReg(mvme, base_addr,"all",PROFILE_REG3);
  if(data == FAILURE)
    return FAILURE;
  if(check_identical() )	
    {
      data = array[0];
      if(pdd)printf("psmSetCIC_Rate: all profile REG3 registers read 0x%x\n",data);
    }
  else
    {
      printf("psmSetCIC_Rate: all profile REG3 registers  must read back identical values for this routine to operate\n");
      return FAILURE;
    }
  data = data &  ~CIC_INTERPOLATION_RATE; /* mask away old interpolating rate */
  value = ival;
  value = value << 2;
  if(pdd)printf ("ival=%d (0x%x); shifted %d (0x%x)\n",ival,ival,value,value);
  value =  value | data;
  if(pdd)
    {
      printf ("value ORed with data: %d (0x%x)\n",value,value);
      printf("psmSetCIC_Rate: writing 0x%x \n",value);
    }
  data = psmWriteProfileReg(mvme, base_addr,"all",PROFILE_REG3,value);
  if(data != FAILURE)
    {
      value = data  &  CIC_INTERPOLATION_RATE; /* mask */
      value = value >>2;
      if(pdd)printf("psmSetCIC_Rate: all REG3 registers now read 0x%x; interpolating rate=%d\n",data,value); 
    }
  return value;
}


 INT psmLoadIdleFreq ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq)
{
  /* Load the IDLE frequency at address  FREQ_SWEEP_IDLE  (0x08FFC)
     Returns IDLE frequency value read back or FAILURE for error
  */
  DWORD offset, data;
  // BASE_DM_FREQ_SWEEP        0x08000  FREQ_SWEEP_IDLE           0x08FFC
  offset = FREQ_SWEEP_IDLE - BASE_DM_FREQ_SWEEP; /* make offset correct for LoadOneFreq */
  if(pdd)printf("psmLoadIdleFreq: calling psmLoadOneFreq with freq=0x%x offset=0x%x\n",
	 freq,offset);
  psmLoadOneFreq(mvme, base_addr,freq ,offset); 
  data = psmReadOneFreq(mvme, base_addr,offset);
  if(pdd)printf("psmLoadIdleFreq: read back hex value 0x%x\n",data);
  if(data != freq)
    {
      printf("psmLoadIdleFreq: hex value written (0x%x) does not agree with value read back (0x%x)\n",freq,data);
      return FAILURE;
    }
  return data;
}




 INT psmLoadIdleFreq_Hz ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq_Hz)
{
  /* Load the IDLE frequency  in Hz at address  FREQ_SWEEP_IDLE  (0x08FFC)
     Returns IDLE frequency value in Hz read back or -1 for error
  */
  DWORD offset, data, freq_x, temp;
  offset = FREQ_SWEEP_IDLE - BASE_DM_FREQ_SWEEP; /* make offset correct for LoadOneFreq */
  freq_x=get_hex(freq_Hz);
  if (freq_x == 0)
    {
      if(freq_Hz > 0)
	{
	  printf("psmLoadIdleFreq_Hz: input freq %dHz out of range\n",freq_Hz);
	  return FAILURE; 
	}
    }
  if(pdd)printf("psmLoadIdleFreq_Hz: calling psmLoadOneFreq with freq_x=0x%x offset=0x%x\n",
	 freq_x,offset);
  psmLoadOneFreq(mvme, base_addr,freq_x ,offset); 
  data = psmReadOneFreq(mvme, base_addr,offset);
  if(pdd)printf("psmLoadIdleFreq_Hz: read back hex value 0x%x\n",data);
  if(data != freq_x)
    {
      printf("psmLoadIdleFreq_Hz: hex value written (0x%x) (equivalent to %dHz) does not agree with value read back (0x%x)\n",
	     freq_x,freq_Hz, data);
      return FAILURE;
    }
  temp=get_Hz(data);
  if(pdd)printf("psmLoadIdleFreq_Hz: returning %d Hz (equivalent to 0x%x)\n",temp,data);
  return temp; /* return value in Hz */
}

INT psmReadIdleFreq ( MVME_INTERFACE *mvme, DWORD base_addr)
{
  /* Read the IDLE frequency (hex) at address  FREQ_SWEEP_IDLE  (0x08FFC)
     Returns IDLE frequency value (hex) read back or -1 for error
  */
  DWORD offset, data;
  offset = FREQ_SWEEP_IDLE - BASE_DM_FREQ_SWEEP; /* make offset correct for ReadOneFreq */ 
  data = psmReadOneFreq(mvme, base_addr,offset);
  if(data == FAILURE) 
    return FAILURE; /* error */
  if(pdd)printf("psmReadIdleFreq: read back Hex value as 0x%x\n",data);
  return data;
}

INT psmReadIdleFreq_Hz ( MVME_INTERFACE *mvme, DWORD base_addr)
{
  /* Read the IDLE frequency (hex) at address  FREQ_SWEEP_IDLE  (0x08FFC)
     Returns IDLE frequency value (hex) read back or -1 for error
  */
  DWORD offset, data;
  offset = FREQ_SWEEP_IDLE - BASE_DM_FREQ_SWEEP; /* make offset correct for ReadOneFreq */ 
  data = psmReadOneFreq(mvme, base_addr,offset);
  if(data == FAILURE) 
    return FAILURE; /* error */
  //  if(pdd)
  printf("psmReadIdleFreq: read back Hex value as 0x%x",data);
  data = get_Hz(data); // convert to Hz
  printf(" or %d Hz\n",data);
  return data;
  
}





 INT psmLoadIdleIQ ( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code,  DWORD I, DWORD Q, BOOL twos_comp)
{
  /* Load the IDLE IQ pair data at offset IDLE_IQ_OFFSET 
 
  calls psmLoadOneIQ and psmReadOneIQ which have been modified for PSM II

    NOTE: for PSM II, 
     for profile "all" i,q, pair is assumed to be that for 1f (and fREF); 3f and 5f values will
         be doctored before being written.
     for single profiles, i,q, pair is loaded directly
  
  */
  
  DWORD data,i,q;
  BOOL diff;
  INT status;
  status = 0;

  if(pdd)printf("psmLoadIdleIQ: loading I,Q=%d,%d\n",I,Q);

  psmLoadOneIQ(mvme, base_addr, p_profile_code,I,Q,IDLE_IQ_OFFSET, twos_comp); 
  data = psmReadOneIQ(mvme, base_addr,p_profile_code,&i,&q,IDLE_IQ_OFFSET,&diff, twos_comp);
  if (diff)
    printf("psmLoadIdleIQ: Error; expect all to be identical on readback (fREF: I=0x%x (%d); Q=0x%x (%d) )\n",i,i,q,q);
  else
    if(pdd) printf("psmLoadIdleIQ: read back I=%d; Q=%d\n",i,q);


  if (i != I )
    {
    printf("For I, value written (%d) does not agree with value read (%d)\n",
		     I,i);
    status = FAILURE;
    }
  if (q != Q )
    {
      printf("For Q, value written (%d) does not agree with value read (%d)\n",
		     Q,q);
      status = FAILURE;
    }
  return status;
}

INT psmReadIdleIQ ( MVME_INTERFACE *mvme, DWORD base_addr, char *p_profile_code,  DWORD *I, DWORD *Q, BOOL twos_comp)
{
  /* Read the IDLE IQ pair data at offset IDLE_IQ_OFFSET 
   */
  
  DWORD data,i,q;
  BOOL diff;

  data = psmReadOneIQ(mvme, base_addr,p_profile_code,&i,&q,IDLE_IQ_OFFSET,&diff, twos_comp);
  if (diff) /* may be set for "all" */
    printf("psmReadIdleIQ: all are NOT identical on readback (fREF: I=0x%x (%d); Q=0x%x (%d) )\n",i,i,q,q); /* the last value is fREF */
  else
    if(pdd)printf("psmReadIdleIQ: read back I=0x%x (%d); Q=0x%x (%d)\n",i,i,q,q);
  *I=i;
  *Q=q;
  if(diff) 
    return FAILURE;
  else
    return 0;
}

INT  check_profile(char *p_profile_code)
{
  /* detect if profile is "all"
     return true if "all", false otherwise
  */
  char my_profile_code[5];

  strncpy(my_profile_code,p_profile_code,1);
  my_profile_code[1]='\0';
  my_profile_code[0]=tolower(my_profile_code[0]); 
 if(strncmp(my_profile_code,"a",1)==0)
   return TRUE;
 else
   return FALSE;
}

DWORD get_Hz(DWORD Fhex)
{
  double Freq;
  if(!init_flag)init();
 
  Freq = (double)Fhex * finc;
  if(pdd)printf("get_Hz: Input freq=0x%x hex; -> Freq=%.1f, Freq(Hz)=%d \n",Fhex,Freq, (DWORD)Freq); 

  return (DWORD)(Freq+0.5);
}

DWORD get_hex(DWORD freq_Hz)
{
  double Freq;
  DWORD Fhex;
  DWORD tmax;

  if(!init_flag)init();
  

  tmax=(DWORD) (Fmax+0.5); /* round up */
  if(pdd)printf("get_hex:starting with freq_Hz=%d Hz, max frequency is =%d\n",freq_Hz,tmax);
  /* conversion for Hz */
  Freq = (double)freq_Hz;
  if (Freq > Fmax) 
    {
      printf("get_hex: Requested frequency %dHz is larger than maximum  (%dHz)\n",freq_Hz,tmax);
      return 0;
    }
  Freq = Freq/finc;
  Fhex=  (DWORD)(Freq+ 0.5);
  //if(pdd)
    printf("get_hex: Input freq=%d Hz; -> hex equivalent is Freqx=0x%x \n",freq_Hz,Fhex); 
  return Fhex;
}

INT TwosComp_convert(INT value, BOOL twos_comp)
{
 /*  Convert data into twos complement and back (10 bit data)
     twos_comp = FALSE -> converts data into 2s Compliment
     twos_comp = TRUE  -> converts data out of 2s Compliment
 
   Register is 10 bits.  
     Valid input data can be from -511 to +511 to convert to 2s compliment.
     2's compliment data is from 0 to 1023 

            Example:
     Input        2s compliment
     511             513
     480             544
      30             994
       1            1023
       0               0
      -1               1
     -30              30
    -254             254
    -511             511

  */

  int IQ,iq;
  
  if(! twos_comp)
    {
      /* Convert data to twos_comp */
      IQ=value;
      if(IQ > 511 || IQ < -511) 
	{
	  printf("TwosComp_convert: Data is out of range for 10 bits (-512<IQ<512) \n");
	  return FAILURE; 
	}

      iq=(~IQ +1) & I_Q_DATA_MASK ;
      if(pdd)
	printf("*** TwosComp_convert: Data (%d 0x%x) converted to 2's complement and masked = %d 0x%x\n",IQ,IQ,iq,iq);
      return iq;
    }

  else
    {
      /* Convert data back from twos_comp */
      iq=value;
      if(iq>1023 || iq < 0)
	{
	  printf("TwosComp_convert: 2s complement data (%d) is out of range for 10 bits (-1<iq<1024) \n",iq);
	  return -1024; 
	}
	
      if(iq<=511)
	IQ=~(iq)+1;
      else
	{
	  IQ=~(iq-1);
	  IQ = IQ & I_Q_DATA_MASK ;
	}
      if(pdd)printf("TwosComp_convert: Data (%d 0x%x) converted back from 2's complement = %d 0x%x\n",iq,iq,IQ,IQ);
      return IQ;
    }
}


/*****************************************************************/
/*  For test purpose only */
#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {
  int s;
  DWORD PSM_BASE  = 0x800000;

  MVME_INTERFACE *myvme;
  int status;
  int nbits;
  DWORD data,value,freq_Hz,jump_to_idle;
  DWORD idle_Hz,freq;
  DWORD offset;
  char profile_code[10];
  const DWORD mask8=0xFF;
  const DWORD mask16=0xFFFF;
  INT control_reg;
  BOOL identical;
  DWORD num;
  float ftemp;
  char file1[80];
  char file2[80];
  char cmd[]="hallo";
  char expt[20];
  int i;

  strncpy(expt, getenv("MIDAS_EXPT_NAME"), sizeof(expt));  
  printf("Current experiment is %s\n",expt);


  if (argc>1) {
    sscanf(argv[1],"%x", &PSM_BASE);
  }
  // Test under vmic
  status = mvme_open(&myvme, 0);
  if(status != SUCCESS)
    {
      printf("failure after mvme_open, status = %d\n",status);
      return status;
    }

  psm( PSM_BASE);

 
  while ( isalpha(cmd[0]) ||  isdigit(cmd[0]) )
    {
      printf("\nEnter command (A-Z a-z) X to exit?  ");
      scanf("%s",cmd);
      //  printf("cmd=%s\n",cmd);
      //cmd[0]=toupper(cmd[0]);
      s=cmd[0];

      data=offset=nbits=value=num=freq=jump_to_idle=0;
      switch(s)
	{


	case ('A'):   //  psmRegRead or  psmRegRead8
         
	  printf("Number of bits to read (8 or 16) :");
	  scanf("%d",&nbits);
	  printf("Register Offset from base :0x");
	  scanf("%x",&offset);
	  
	  if(nbits == 8)
	    data = psmRegRead8(myvme, PSM_BASE, offset);
	  else
	    {
	      nbits=16;
	      data = psmRegRead(myvme, PSM_BASE, offset);
	    }
	  printf("Read back  0x%x or %d  (%d bits)\n",data,data, nbits);
	  break;

	case ('B'):  //  psmRegWrite or  psmRegWrite8
    
	  printf("Number of bits to write (8 or 16) :");
	  scanf("%d",&nbits);
	  printf("Register Offset from base :0x");
	  scanf("%x",&offset);
	  printf("Enter Value to write :0x");
	  scanf("%x",&value);
	  
	  if(nbits == 8)
	    {
	      data = psmRegWrite8(myvme, PSM_BASE, offset, (value & mask8));
	      printf("Wrote  0x%2.2x or %d (8 bits) \n", (value & mask8), (value & mask8) );
	      printf("Read back  0x%x or %d  \n", data,data );
	    }
	  else
	    {
	      data = psmRegWrite(myvme, PSM_BASE, offset, (value & mask16));
	      printf("Wrote  0x%4.4x or %d (16 bits) \n", (value & mask16), (value & mask16));
	      printf("Read back  0x%x or %d  \n", data,data );
	    }
	  break;

	case ('C'):  // psmFreqSweepMemAddrReset
	  psmFreqSweepMemAddrReset(myvme, PSM_BASE);
          break;

	case ('D'):  // dump registers
	  dump( myvme,  PSM_BASE);
          break;

	case ('E'): // strobe
	  psmFreqSweepStrobe (myvme,  PSM_BASE);

	case ('F'):  // Read  RF Power Trip
	  psmReadRFpowerTrip(myvme,  PSM_BASE);
          break;

	case ('G'):  // Clear  RF Power Trip
	  psmClearRFpowerTrip(myvme,  PSM_BASE);
          break;
	  
	case ('H'):  // Help
	  psm(PSM_BASE);
          break;
	  
	case ('I'):  // Init
	  psmInit(myvme,  PSM_BASE);
          break;
	  
	case ('J'):  // ReadProfileReg  
	  if (get_profile_code(profile_code)== SUCCESS)
	    {
	      if(get_control_register(&control_reg) == SUCCESS)
		psmReadProfileReg (myvme,  PSM_BASE, profile_code, control_reg);
	    }
          break;
	
      
	case ('K'):  // WriteProfileReg  
	  if (get_profile_code(profile_code)== SUCCESS)
	    {
	      if(get_control_register(&control_reg) == SUCCESS)
		{
		  printf("Enter value to write :0x");
		  scanf("%x",&value);
		  psmWriteProfileReg (myvme,  PSM_BASE, profile_code, control_reg, value);
		}
	    }
	  break;
 
	case ('L'): // psmSetIQlen
	  printf("Enter Length :0x");
	  scanf("%x",&value);
	  psmSetIQlen (myvme,  PSM_BASE, value);
          break;

	case ('M'): // psmReadIQlen
	  data = psmReadIQlen (myvme,  PSM_BASE);
          printf("Read IQ len as %d \n", data);
          break;
 
	case ('N'): // psmSetQM_On
	  psmSetQM_On (myvme,  PSM_BASE);
          break;

	case ('O'): // psmSetQM_On
	  psmSetQM_Off (myvme,  PSM_BASE);
          break;

	case ('P'):  // Print list
	  psm(PSM_BASE);
          break;

	case ('Q'): 
          return(SUCCESS);
          break;


	case ('R'): // Read control regs
	  psmReadControlRegs(myvme,  PSM_BASE, NULL);
          break;

	case ('S'): // psmGetstatus 
	  psmGetStatus  (myvme,  PSM_BASE,NULL);
          break;

	case ('T'): // psmSetScaleFactor
	  printf("Enter Scale Factor (0-255) :0x");
	  scanf("%x",&value);
          value=value & 0xFF;
	  psmSetScaleFactor(myvme,  PSM_BASE, value);
          break;

	case ('U'): // psmSetCIC_Rate
	  printf("Enter CIC Rate (1-63) :");
	  scanf("%d",&value);
	  psmSetCIC_Rate(myvme,  PSM_BASE, value);
          break;

	case ('V'): //  VME Reset
	  psmVMEReset(myvme,  PSM_BASE);
          break;


	case ('W'): // psmReadIQptr
	  psmReadIQptr (myvme,  PSM_BASE);
          break;

	case ('X'): 
          return(SUCCESS);
          break;


	case ('Y'): // psmIQWriteEndSweepMode
	  if (get_profile_code(profile_code)== SUCCESS)
	    {
              printf("End Sweep Mode behaviour if strobes or gate exceed length\n");
	      printf("Mode bit  0: Stop at Nth  1: jump to IDLE\n");
	      printf("Enter End Sweep Mode bit (0 or 1) :");
	      scanf("%d",&value);
	      psmIQWriteEndSweepMode(myvme,  PSM_BASE, profile_code, value);
	    }
	  break;

	case ('a'): //  psmWrite_fREF_freq_Hz
	  printf("Enter fREF frequency in Hz :");
	  scanf("%d",&freq_Hz);
	  psmWrite_fREF_freq_Hz(myvme,  PSM_BASE, freq_Hz);
	  break;

	case ('b'): //  psmWrite_fREF_freq
	  printf("Enter fREF frequency in hex:");
	  scanf("%x",&freq);
	  psmWrite_fREF_freq(myvme,  PSM_BASE, freq);
	  break;

	case ('c'): //  psmFreqSweepReadLength
	  psmFreqSweepReadLength(myvme,  PSM_BASE);	  
	  break;

	case ('d'): //toggle debugging
	  if(pdd)
	    {
	      pdd=0;
	      printf("Debugging turned OFF\n");
	    }
	  else
	    {
	      pdd=1;
	      printf("Debugging turned ON\n");
	    }

	  break;

	case ('e'): //  psmFreqSweepReadAddress
	  psmFreqSweepReadAddress(myvme,  PSM_BASE);	  
	  break;

	case ('f'): // psmWriteProfileBufFactor
	  // if (get_profile_code(profile_code)== SUCCESS)
	    {
              //  users restricted to  "all" profiles
	      printf("Enter Maximum Ncmx Buffer Factor for ALL profiles   6 bits (0-63) :");
	      scanf("%d",&value);
	      value = value & 0x2F;
	      psmWriteProfileBufFactor(myvme,  PSM_BASE, "all",  value);
	    }
	  break;

	case ('g'): // psmReadProfileBufFactor
          // users must write "all"
	  psmReadProfileBufFactor( myvme, PSM_BASE, "all",  &identical);
	  break;

	case ('h'): // psmReadMaxBufFactor
	  psmReadMaxBufFactor( myvme, PSM_BASE);
	  break;

	case ('i'): //psmWriteGateControl
	  printf("  gate_code = 0 disable fp gate inputs;      = 1 enable fp gate inputs (default); \n");
	  printf("          = 2 as 1 but gate inputs inverted; = 3 fp gate ignored- always gated\n");
	  printf("Enter Gate Code (0-3) :");
	  scanf("%d",&value);
	  if (get_profile_code(profile_code)== SUCCESS)
	    psmWriteGateControl( myvme, PSM_BASE, profile_code, value);
 	  break;
 
	case ('j'): // write psmWriteRFpowerTripThresh
          printf("Vtrip = 5 Volts * ( Power Trip Threshold / 255\n");
	  printf("Enter Power Trip Threshold  8 bits (0-255) :");
	  scanf("%d",&value);
	  psmWriteRFpowerTripThresh (myvme, PSM_BASE, value);
 	  break;

	case ('k'): // read psmReadRFpowerTripThresh
          printf("Vtrip = 5 Volts * ( Power Trip Threshold / 255\n");
	  ftemp = psmReadRFpowerTripThresh (myvme, PSM_BASE);
          printf("Vtrip = %f volts\n", ftemp);
 	  break;

	case ('l'): // psmRegReadDM read from DM
	  printf("DM Offset  :0x");
	  scanf("%x",&offset);
	  printf("Number of 16-bit words to read (dec) :");
	  scanf("%d",&num);
	  for (i=0; i<num; i++)
	    {
	      data = psmRegReadDM(myvme, PSM_BASE, offset);
	      printf("Base Address + offset 0x%x  read back  0x%x (%d) \n", offset, data,data);
	      offset+=2;
	    }
 	  break;

	case ('m'): // psmRegWriteDM write to DM
	  printf("DM Offset  :0x");
	  scanf("%x",&offset);
	  printf("Enter value to write :0x");
	  scanf("%x",&value);
	  data = psmRegWriteDM(myvme, PSM_BASE, offset, value);
          printf("Wrote 0x%x to DM offset 0x%x\n",value,offset);
	  printf("Read back 0x%x (%d)\n",data,data);
	  break;

	case ('n'): // psmReadFreqDM
	  printf("Enter number of values to read back  :");
	  scanf("%d",&num);
	  psmReadFreqDM(myvme, PSM_BASE, num);
	  break;

	case ('o'): // psmLoadIdleFreq
	  printf("Enter idle Freq hex value  0x:");
	  scanf("%x",&freq);
	  psmLoadIdleFreq(myvme, PSM_BASE, freq);
	  break;

	case ('p'): //  psmReadIdleFreq 
	  data = psmReadIdleFreq (myvme, PSM_BASE);
          printf("Read Idle Frequency hex value as 0x%x\n",data);
	  break;


	case ('q'): // psmLoadIdleFreq_Hz
	  printf("Enter idle Freq (Hz)  :");
	  scanf("%d",&idle_Hz);
	  psmLoadIdleFreq_Hz(myvme, PSM_BASE, idle_Hz);
	  break;

	case ('r'): //  psmReadIdleFreq 
	  data = psmReadIdleFreq_Hz (myvme, PSM_BASE);
          printf("Read Idle Frequency as %d Hz \n",data);
	  break;
 
	case ('s'): // load one freq (Hz) at memory offset
	  printf("Enter frequency in Hz :");
	  scanf("%d",&freq_Hz);
          value = get_hex(freq_Hz); /* convert to hex */
	  if (value == 0)
	    {
	      if(freq_Hz > 0)
		{
		  printf("Input freq %d Hz out of range\n",freq_Hz);
		  break;
		} 
	    }
	  printf("Enter memory offset (0-0xFF8)  0x:");
	  scanf("%x",&offset);
	  psmLoadOneFreq(myvme, PSM_BASE, value, offset);
	  break;

	case ('t'): // read one freq  at memory offset
	  printf("Enter memory offset (0-0xFF8) 0x:");
	  scanf("%x",&offset);
	  freq = psmReadOneFreq(myvme, PSM_BASE, offset);
          freq_Hz = get_Hz(freq);
	  printf("Read back %d Hz (%x hex equivalent)\n",freq_Hz,freq);
	  break;
	
	case ('u'): // Test... read/write some regs
	  psmInitTest(myvme, PSM_BASE);
	  break;
	  
	case ('v'): // Test... set one freq
	  printf("Enter fREF frequency in Hz :");
	  scanf("%d",&freq_Hz);
	  printf("Enter Idle frequency in Hz :");
	  scanf("%d",&idle_Hz);
	  psmSetOneFreq_Hz(myvme, PSM_BASE, idle_Hz,freq_Hz);
	  break;


	case ('w'): // Test... set two freq
	  printf("Enter fREF frequency in Hz :");
	  scanf("%d",&freq_Hz);
	  printf("Enter first Idle frequency in Hz :");
	  scanf("%d",&idle_Hz);
	  printf("Enter second Idle frequency in Hz :");
	  scanf("%d",&value);
	  
	  psmTryTwoFreq(myvme, PSM_BASE, idle_Hz, value, freq_Hz);
	  break;
	  
	  
	case ('x'): // Test... scan using a frequency file
	  printf("Enter fREF frequency in Hz :");
	  scanf("%d",&freq_Hz);
          printf("Jump to Idle:  0=stop at Nth freq in file \n");
          printf("               1=jump to idle freq (i.e. 1st freq in file)\n");
	  printf("Enter jump to idle (0 or 1) :");
	  scanf("%d",&jump_to_idle);
	  printf("Enter frequency filename (or \"default\") :");
	  scanf("%s",file1);

	  psmTryScan(myvme, PSM_BASE, freq_Hz, jump_to_idle, file1);
	  break;

	case ('y'): //Test...  Load Freq & IQ pairs files
	  printf("Enter frequency filename (or \"default\") :");
	  scanf("%s",file1);
	  printf("Enter IQ filename  (or \"default\") :");
	  scanf("%s",file2);

	  psmTryIQ (myvme, PSM_BASE, file1, file2);
          break;

	default:
	  printf("Unknown command. Enter \"P\" for list of commands\n");
          break;
	}
    }
  return SUCCESS;
}

/*------------------------------------------------------------------*/
void psm(const DWORD psm_base)
{
  
   printf("\nPol Synthesizer Module: base address  psm_base=0x%x\n",psm_base);
   printf("       TRIUMF PSM function support\n");
  
   printf("A RegRead                B   RegWrite\n");
   printf("C Reset FreqSweepMemAddr D   Dump Registers\n");
   printf("E FreqSweep Strobe       \n");
   printf("F Read RF PowerTrip       G Clear RF PowerTrip     \n");
   printf("H help                   I Init   \n"); 
   printf("S status                 V VME Reset\n");
   printf("d debug (toggles)\n");
                        
   printf("\n== Profile Registers (1f,3f,5f,fREF): ==\n");
   printf("  profile_code can be \"1f\", \"3f\", \"5f\", \"fREF\" or \"all\" and control_reg is 1-6\n");
   printf("J Read Profile Register   K Write Profile Register   P Profile Status\n");
   printf("\nAll profiles:\n");
   printf("L Set IQ Len              M Read IQ Len\n");
   printf("N Set QM On               O Set QM Off\n");
   printf("T Set Scale Factor        U Set CIC Rate\n");
   printf("W Read IQ Pointer \n");
   printf("\n======= Other Control Registers : =====\n");
   printf(" R ReadControlRegs\n");
   printf(" Y  IQ WriteEndSweepMode     Z  Frequency WriteEndSweepMode\n");
   printf(" a  Write FREF freq in Hz    b  Write FREF freq in hex\n");     
   printf(" c  Read Frequency Sweep Length\n");
   printf(" e  Read Freq Sweep Address   f Write Profile Buffer Factor\n");         
   printf(" g  Read Profile Buffer Factor h Read Max Buffer Factor\n"); 
   printf(" i  Write Gate Control         j Write RF Power Trip Threshold \n");
   printf(" k  Read RF Power Trip Threshold             \n");

   printf("\n==============  Memory: ===============\n");
   printf("l Read DM               m Write DM \n");
   printf("n Read Frequency DM    \n"); 
   printf("o Load Idle Freq (hex)  p Read Idle Freq (hex)\n");
   printf("q Load Idle Freq (Hz)   r Read Idle Freq (Hz)\n");
   printf("s Load One Freq (Hz)    t Read One Freq (Hz) \n");

   printf("\n===============  Test Procedures: ===========\n");

   printf("u Test (read/write)    v Set One Freq\n");
   printf("w Two Freq             x Scan   \n");
   printf("y Load Freq & IQ pairs file \n");

   printf("X exit  Q exit\n");


 printf("\n");
 
   return;
}

int get_profile_code(char *code)

{
  
  char my_code[10];

  printf("Enter Profile code (\"1f\", \"3f\", \"5f\", \"fREF\" or \"all\") : ");
  scanf("%s",my_code);
  my_code[4]='\0';
  //for (i=0; i<strlen(my_code);i++)
  // my_code[i]=toupper (my_code[i]);
      
      if ((strncmp(my_code,"1F",2)==0) || (strncmp(my_code,"3F",2)==0) || (strncmp(my_code,"5F",2)==0))
	my_code[3]='\0';
      else if  ((strncmp(my_code,"FREF",3)==0) || (strncmp(my_code,"ALL",2)==0))
	my_code[4]='\0';
      else
	{
	  printf("Unknown Profile code \"%s\"\n",my_code);
	  return FAILURE;
	}
  strncpy(code, my_code, 5);
  return SUCCESS;
}

INT get_control_register(INT *reg)
 {
   INT my_reg;
   printf("Enter control register (1-6) :");
   scanf("%d",&my_reg);
   *reg=my_reg;
   if( (my_reg > 0) && (my_reg < 7))
     return SUCCESS;
   else
     return FAILURE;
 }

#endif // MAIN_ENABLE
