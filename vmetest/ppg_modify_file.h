// prototypes
int ppg_modify_file(char * infile, char * outfile, char *modfile);
void  lineWrite (COMMAND *data_struct);
int getinsline(char *line, int max, FILE *file);
COMMAND lineRead(char *line);
int  get_next_line(char *line);
int write_modifications_1h (char *modfilename, char *outfile, int array[]);
int get_mod(char * line, FILE_VAR *mods);
void load_mod(char *mline, int *index, unsigned int *count, int *type);
