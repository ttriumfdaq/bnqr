#include <stdio.h>
#include <string.h>


main()
{
  long unsigned int data,data1,masked;
  long unsigned int run_mask = 0x6;
  long unsigned int clock_mask = 0x5;
  data1 =0x06000006;
  data = (data1 & run_mask );
  printf ("run masked data=0x%lx\n",data);
  data = data | 1 ;
  printf ("start seq - writing data=0x%lx\n",data);

  data1=0x06789AB7;
  data = (data1 & run_mask );
  printf ("run masked data=0x%lx\n",data);
  data = data & 0x6 ;
  printf ("stop seq - writing data=0x%lx\n",data);

  data1=0x06789AB7;

  masked = (data1 & clock_mask );
  printf ("clock masked data=0x%lx\n",masked);
  data = masked | 2;
  printf ("ext clock - writing data=0x%lx\n",data);
  data = masked ;
  printf ("int clock - writing data=0x%lx\n",data);

TPPG_RUN_MASK     0x6
#define TPPG_CLOCK_MASK   0x5
#define TPPG_START_MASK   0x3
}
