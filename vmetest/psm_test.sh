 echo "VME Reset and wait 2s"
  vme_poke -a VME_A24UD -A 0x0080902E -d VME_D8 0x1
  sleep 2
 echo "load idle frequency 250000Hz in BCD"
 vme_poke -a VME_A24UD -A 0x00808FFC -d VME_D16 0x51
 vme_poke -a VME_A24UD -A 0x00808FFE -d VME_D16 0xeb85
 echo "strobe"
 vme_poke -a VME_A24UD -A 0x0080902A -d VME_D8 0x1

 echo "load i and q"
 vme_poke -a VME_A24UD -A 0x00801FFC -d VME_D16 0x7FF
 vme_poke -a VME_A24UD -A 0x00801FFE -d VME_D16 0x0
 echo "set single tone mode"
 vme_poke -a VME_A24UD -A 0x00809001 -d VME_D8 0x1
 echo "set default gate"
 vme_poke -a VME_A24UD -A 0x0080902C -d VME_D8 0x1
