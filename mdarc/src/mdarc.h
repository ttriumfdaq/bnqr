/*  mdarc.h
    common (global) variables for mdarc & bnmr_darc.c

    $Log: mdarc.h,v $
    Revision 1.2  2013/04/08 22:25:53  suz
    Changes after debugging new VMIC code

    Revision 1.1  2013/01/21 21:42:38  suz
    initial VMIC version to cvs

    Revision 1.20  2008/04/16 18:51:53  suz
    separation of CAMP and EPICS logged variables

*/
/* pHistData should not be global to bnmr_darc.c as it is also the parameter
   i.e. call bnmr_darc(pHistData)
   It is now a global in mdarcMusr (or mdarcBnmr);
//caddr_t pHistData; 
*/
#ifndef mdarc_header
#define mdarc_header

INT nHistBins; // number of histogram bins
INT nHistBanks; // number of histograms (BNMR,TD-MUSR)
#ifdef MUSR
INT nHistograms; // number of histograms (IMUSR)
#else
BOOL saving_data_flag; // for alpha mode, event pieces; 
BOOL gbl_alpha; // alpha mode flag
#endif
BOOL     firstxTime;
HNDLE   hDB, hKey, hMDarc;
BOOL    bGotEvent;
caddr_t pSN; // pointer to bank serial number stored in data array  (TD only)
INT     last_saved_SN;
#ifdef MUSR
BOOL    bGotCamp;  // IMUSR; got CAMP event
char i_eqp_name[32];
#endif
char    feclient[32], eqp_name[32];
INT     size;
char expt_name[HOST_NAME_LENGTH];
char beamline[6]; // was 16 for IMUSR; why? 
INT    run_number;
INT    old_run_number;
char   run_type[5];  /* test or real */
BOOL   toggle;
BOOL   stop_run_flag; // set if stop_run routine has been called due to fatal error

/* debugs */
BOOL debug ;
BOOL debug_mud;
#ifndef MUSR
BOOL debug_ppg; // BNMR
#endif
#ifdef EPICS_ACCESS
BOOL debug_epics;
#endif
BOOL debug_dump;
BOOL debug_check ;
BOOL debug_proc; // global for debug of process_*_event
BOOL debug_cf; // debug client flags routines 
INT  dump_begin;
INT  dump_length;
//BOOL timer ;    /* timing */
char perl_path[80]; /* perlscript path */
char archiver[80];
BOOL I_MUSR,TD_MUSR; /* flags indicate which MUSR type is running
			for BNMR these are both FALSE */
INT db,dl;

#ifdef MUSR 
/*        MUSR       */
DWORD scaler_counts[16];
HNDLE hMusrSet;
INT gbl_data_point; // the rest is for IMUSR
INT gbl_camp_data_point;
INT numCamp;// number of camp variables enabled in /equipment/camp/settings
/* Array of camp variables from CVAR event */
double *pCampData;

/* Array of scaler totals, allocated by realloc in M2MI_expand, not freed: */
IMUSR_CAMP_VAR *campVars ;
static FILE *Imusr_log_fileHandle = NULL ;

#else
/*   BNMR/BNQR       */
INT ppg_type;
// prototypes needed by midbnmr_darc.c for BNMR Type 1
void process_hist_event (HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
void process_darc_event  (HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
void process_camp_event  (HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
void process_epics_event  (HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
#endif 

#endif
