/* darc_files.c
        common for bnmr and musr

 Subroutines for use with mdarc

   Contents :
   darc_rename_file         write_link_msg
   darc_find_saved_file     write_rename_msg
   darc_set_symlink         write_remove_msg
   darc_unlink              write_message
   darc_kill_files          write_message1
   darc_archive_file        msg_print

 $Log: darc_files.c,v $
 Revision 1.9  2016/07/05 01:37:47  suz
 use ONLINE instead of vmic_online

 Revision 1.8  2015/11/27 23:24:45  suz
 added ifdef YBOS

 Revision 1.7  2015/05/01 19:49:55  suz
 remove function file_exist - not used

 Revision 1.6  2015/04/30 20:26:42  suz
 add change for ss_file_find not finding sym link any more. Seems like this change had not been committedto cvs and got lost

 Revision 1.5  2015/04/29 22:56:12  suz
 remove reference to cpbnmr (uses archiver string read from ODB)

 Revision 1.4  2015/04/29 22:31:23  suz
 change a comment and message

 Revision 1.3  2014/04/03 00:16:50  asnd
 Use relative path links when possible

 Revision 1.2  2013/04/08 22:25:53  suz
 Changes after debugging new VMIC code

 Revision 1.1  2013/01/21 21:42:38  suz
 initial VMIC version to cvs

 Revision 1.18  2007/07/27 20:12:30  suz
 someone added write_to_me

 Revision 1.17  2006/12/08 03:41:22  asnd
 Patch remaining memory leak

 Revision 1.16  2006/12/08 03:25:32  asnd
 Remove limit on number of versions.

 Revision 1.15  2004/05/05 18:23:28  suz
 change numerical test runs to be MIN and MAX_TEST

 Revision 1.14  2004/04/21 19:18:13  suz
 directory for archiver.txt same as for BNMR; add slash to archiver directory; increase length of strings to fix bugs

 Revision 1.13  2004/03/09 21:51:01  suz
 changes for Midas 1.9.3

 Revision 1.12  2004/02/11 02:31:12  suz
 add imusr support

 Revision 1.11  2004/01/09 22:01:07  suz
 small debug change

 Revision 1.10  2004/01/08 20:32:15  suz
 redirect stderr to archiver.txt. Add last_file_archived odb param

 Revision 1.9  2004/01/08 20:21:22  suz
 try to improve error handling if archiving fails

 Revision 1.8  2003/12/01 20:32:08  suz
 add check on blank string for archiver

 Revision 1.7  2003/05/21 21:31:49  suz
 included run_numbers.h

 Revision 1.6  2002/07/18 19:12:21  suz
 stop archiving of test runs
 

*/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
/* needed for lstat */
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
/* midas includes */
#include "midas.h"
#include "msystem.h" //MAX_STRING_LENGTH
#ifdef HAVE_YBOS
#include "ybos.h" // MAX_FILE_PATH
#else
#define MAX_FILE_PATH 128
#endif
#include "darc_files.h"
//#include "experim.h" /* odb structure NOT included here */
#ifdef MUSR
/* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "trii_fmt.h"  /* needed by MUSR in mdarc.h */ 
#endif

#include "mdarc.h"
#include "run_numbers.h"
#define FAILURE 0

void write_to_me (INT s, char * xx);
extern char beamline[6];

//const char *sys_errlist[];
//int sys_nerr;

INT
darc_rename_file(INT run_number, char *save_dir, char *archive_dir, char *saved_filename, 
		 char *archived_filename, BOOL *archived)
{
/*
  Called at end-of-run to purge and rename the saved files.
  Also archives data by copying the final run file to the directory given by
  archive_dir if archive_dir is not a null string. 
    
  Does not pass p_odb_data since tr_stop does not have access to this.
  
  Input
         INT  run_number     run number
         char *save_dir      directory where files are saved
         char *archive_dir   directory where files are archived
       
                             
   Output

        returns             SUCCESS or FAILURE
        saved_filename      path and name of saved file
	archived_filename   path and name of archived file
	archived            TRUE if file has been successfully archived, otherwise FALSE
*/


  
  char dir[128];
  int nfile,i,j, len, status;
  char * list = NULL;
  char outFile[128];
  char *pt;
  char filename[MAX_FILE_PATH];  /* MAX_FILE_PATH is defined as 128  in /midas/include/ybos.h */
  char *s;
  int max, min, ver, index;
  int keep;
  struct stat stat_buf;
  int exitstatus;

// for archiving
  char infofile[80];
  char copy_string[256];
  char str[256];
  char beamline_lc[10];

  FILE *errfile;
  INT first;
  char hdrstring[132];
  if(debug_check)
  {
    printf("\n");
    printf("darc_rename_file starting with run_number=%d\n",run_number);
    printf("      and save_dir=%s, archive_dir=%s\n",save_dir,archive_dir);    
  }
  *archived=FALSE; // initialize return flag
  max=0;
  min = 1000000000;
#ifndef MUSR
  if(debug_proc)printf("darc_files: ppg_type=%d\n",ppg_type);
#endif
  sprintf(filename,"%06d.msr_v*",run_number);  
  sprintf(dir,"%s",save_dir);
  if (dir[0] != 0)
    if (dir[strlen(dir)-1] != DIR_SEPARATOR)
      strcat(dir, DIR_SEPARATOR_STR);
  
  if(debug_check)  printf("darc_rename_file:Looking for files: %s%s\n",dir,filename);
  
  nfile = ss_file_find(dir, filename, &list);
  if(debug_check)  printf ("darc_rename_file:Found  %d files for this run \n",nfile);
  if(nfile <= 0 )
  {
    cm_msg(MERROR,"darc_rename_file","Found no saved files %s%s",dir,filename);
    return(FAILURE);
  }
  
  for ( j=0; j<nfile; j++ )
    {
      pt = list + j*MAX_STRING_LENGTH;

      if ( sscanf( pt, "%*06d.msr_v%d", &ver ) == 1 ) 
        {
          max = (ver > max ? ver : max);
          min = (ver < min ? ver : min);
        }
      else
	{
	  if(debug_check)  printf ("Ignore badly named file: %s\n", pt);
	}
    }
  /* Free memory from list of file names (allocated in ss_file_find). */
  free(list);

  if (debug_check) printf("max = %d,   min = %d\n", max, min);
  if ( max < min )
    {
      cm_msg(MERROR,"darc_rename_file","Found no properly versioned files %s%s",dir,filename);
      return(FAILURE);
    }

  keep = 1;

  /* now purge the files */
  for (ver=min; ver<=max-keep; ver++)
    {
      sprintf(outFile,"%s%06d.msr_v%d",dir,run_number,ver);

      if (debug_check) printf("darc_rename_file: About to delete file %s (index=%d)\n",outFile,j);
      
      status = ss_file_remove(outFile);
      /* 
       * Ignore return status because we didn't verify that this particular file exists.
       */
    }

  /* Generate filename for rename   */
  if(debug_check) printf("darc_rename_file: Starting renaming procedure... \n");
  sprintf(filename,"%s%06d.msr_v%d",dir,run_number,max);
  sprintf(outFile,"%s%06d.msr",dir,run_number);
  outFile[127]='\0'; /* terminate string */

  /* check for REGULAR file and SYMBOLIC LINK */
  if(debug_check)
  {
    printf("darc_rename_file: outFile = %s \n",outFile);
    printf("    - must correspond to the following line:\n");
    printf("    save_dir = %s and run_number=%d\n",save_dir,run_number);
    printf("darc_rename_file:Calling darc_unlink which calles darc_find_saved_file\n");  
  }

// darc_unlink calls darc_find_saved_file
  status = darc_unlink(run_number, save_dir);
  if (status == 1)
  {
    cm_msg(MERROR,"darc_rename_file","Regular saved file already exists for this run %d",run_number);
    cm_msg(MERROR,"darc_rename_file","Rename file at end-of-run FAILS. ");
    return(FAILURE);   
  }
  
  if(debug_check) printf(" darc_rename_file:  new file name will be : %s\n",outFile);
  status = rename(filename,outFile);
  if (status == -1 )
  {
    cm_msg(MERROR,"darc_rename_file","ERROR trying to rename file %s",filename);
    printf("darc_rename_file: to %s because\n",outFile);
    write_rename_msg(status);
    return(FAILURE);
  }
  
  printf("darc_rename_file: Renamed final saved file %s\n",filename);  /* SUCCESS */
  printf("                  to %s\n",outFile);
  strcpy (saved_filename,outFile);
  cm_msg(MINFO,"darc_rename_file","End-of-run final saved file: %s",outFile);
  if(debug_check) printf("darc_rename_file: Returning saved filename %s\n",saved_filename);      
  
  if ( run_number  >= MIN_TEST && run_number <= MAX_TEST  ) 
    {
      printf("darc_rename_file: test run %d will not be archived\n",run_number);
      return (SUCCESS);  // do not archive test runs
    }
 
  /* copy file to archive */
  if (strlen(archive_dir) > 0 )
    { 
      /* make sure there is a trailing '/'  */
      s = strrchr(archive_dir,'/');
      i= (int) ( s - archive_dir );
      j= strlen(archive_dir );
      if(debug_check) printf("archive_dir=\"%s\"; len=%d\n",archive_dir,j);
      if ( i != (j-1) )
	{
	  archive_dir[j]='/';
	  archive_dir[j+1]='\0'; 
	}
      j=strlen(archive_dir );
      if(debug_check)printf (" archive_dir=\"%s\"; len=%d\n",archive_dir,j);
      
      /* archiver submits a data file to the permanent archive */
      if (strlen (archiver) <= 0)
	{
	  cm_msg(MINFO,"darc_rename_file","Archiver is a blank string; cannot archive");
	  return(SUCCESS); // *archived=FALSE indicates failure archiving
	}
      /* Assign path of output file: convert to beamline to lower case */
      for (j=0; j<strlen(beamline); j++) beamline_lc[j]=tolower (beamline[j]);     
      beamline_lc[strlen(beamline)]='\0';
#ifndef MUSR
      sprintf(infofile,"/home/%s/%s/mdarc/archiver.txt",beamline_lc,getenv("ONLINE"));
#else
      sprintf(infofile,"/home/%s/online/%s/log/archiver.txt",beamline_lc,beamline_lc);
#endif
      if(debug_check)
	{
	  printf("darc_rename_file:  Archiving using %s; error output will be in %s\n",archiver,infofile);
	  printf("                   deleting \"%s\"\n",infofile);
	}
      unlink (infofile); // delete any old version there may be
      sprintf(copy_string,"%s %s %s 2>%s",archiver,outFile, archive_dir, infofile);
      if(debug_check)
	printf("darc_rename_file : Sending system command cmd: %s\n",copy_string); 
      status =  system(copy_string);
      exitstatus = WEXITSTATUS(status);
      if(debug_check)
	printf("After system command, status=%d and WEXITSTATUS = %d\n",status,exitstatus);
      // Success: status = WEXITSTATUS = 0
      // Fail:    status =256 WEXITSTATUS=1  (no such directory) 

      if ( status != 0)
        {
	  sprintf(hdrstring,"ERROR archiving %s to %s (status=%d, exitstatus=%d)",
		  outFile, archive_dir,status,exitstatus );
	  cm_msg(MERROR,"darc_rename_file","%s",hdrstring);
	  if(debug_check)
	    printf("about to open %s\n",infofile);
 	  errfile = fopen(infofile,"r");	  
	  if (errfile != NULL)
	    {
	      while(fgets(str,256,errfile) != NULL)
 	        {
		  printf("%s\n",str); 
		  cm_msg(MINFO, "darc_rename_file","%s",str );
		}
	      fclose(errfile);
	    } // end of if (errfile != NULL) 
	  else
	    cm_msg(MINFO,"darc_rename_file","cannot open message file %s\n",infofile);

	  return (SUCCESS); // *archived=FALSE indicates failure archiving
	} // end of if ( status )
      else
	{
	  *archived=TRUE; // set flag
	  cm_msg(MINFO,"darc_rename_file","Successfully archived file %06d.msr to %s",run_number,archive_dir);
	  /* if there is a trailing '/', remove it */
	  s = strrchr(archive_dir,'/');
	  i= (int) ( s - archive_dir );
	  j= strlen(archive_dir );
	  if ( i == (j-1) )
	    archive_dir[i]='\0';
	  sprintf(archived_filename,"%s/%06d.msr",archive_dir,run_number);

	  if(debug_check) printf("darc_rename_file: Returning archived filename %s\n",archived_filename);      
	}
    }  // end of if archived data directory has been supplied
  else
    {
      cm_msg(MINFO,"darc_renamefile"," INFO - file %06d.msr not archived as archive directory is not supplied\n",run_number);
    }

  return(SUCCESS);
}

INT
darc_find_saved_file(INT run_number, char *save_dir)

{
/*
  Looks for a final saved file with no version string for this run.

  If it finds a file, it test to see if it is a soft link (using lstat) or
  a real file. 
  

  Does not pass p_odb_data (may be called from tr_stop which does not have access to
  p_odb_data).
  
  Input
         INT  run_number     run number
         char *save_dir      directory where files are saved

                             
   Output

         returns 0          if no files at all
         returns 1          if a REGULAR file is found.
                            (a symbolic link does not count!)
         returns 2          if a file is found that is a SYMBOLIC LINK

                 see  /usr/include/statbuf.h for struct stat 

*/  
  char filename[MAX_FILE_PATH];
  struct stat stat_buf; /* needed for lstat */
  int status;

  if(debug_check)
  {
    printf("\n");
    printf("darc_find_saved_file starting\n");
    printf("save_dir = %s and run_number=%d\n",save_dir,run_number);
  }
  sprintf(filename,"%s/%06d.msr",save_dir,run_number);  /* No version string */
  if (debug_check) printf("darc_find_saved_file: running lstat on file: %s\n",filename);

  /* lstat the file */
  status=lstat (filename,&stat_buf) ;
  if (debug_check)printf("darc_find_saved_file: status after lstat: %d\n",status);
  if (status == -1)
  {
    if(debug_check)
      {
	printf("darc_find_saved_file: lstat bad status indicates file not found (errno=%d (0x%x)) \n",errno);
	write_link_msg(status);
      }
    return(0); /* no such file */
  }

  /* the file exists */
  
  /* test for symbolic link */
  status = S_ISLNK(stat_buf.st_mode);
  if (debug_check) printf("Testing for symbolic link, status = %d\n",status);
  if(status)
  {
    if (debug_check)printf("File is a symbolic link, S_ISLNK is true\n");
    return(2);
  }  
  /* test for real file */
  status = S_ISREG(stat_buf.st_mode);
  if (debug_check)printf("Testing for real file, status = %d\n",status);
  if(status)
  {
    if (debug_check)printf("File is a real file, S_ISREG is true\n");
    return(1);
  }  
  return(0); /* No file found or strange error  */
}

INT
darc_set_symlink(INT run_number, char *save_dir, char *filename)
{
/* set up a symbolic link for analysis pgms to always look for filename
     of type *.msr


     inputs:     run_number      use to generate name of 
                 save_dir        symbolic link
                 filename        name of regular versioned file (e.g. 040001.msr_v32)

     returns:    SUCCESS or FAILURE

*/
  char symlink_name[MAX_FILE_PATH];
  int status;
  char* f = filename;
  int dirlen;
  
  if(debug_check)
  {
    printf("\n");
    printf("darc_set_symlink starting\n");
  }
  dirlen = strlen( save_dir );
  /* If linking within same directory, link with relative path (none) */
  if ( strncmp( filename, save_dir, dirlen ) == 0 )
  {
    f = filename + dirlen;
    while ( *f == '/' ) { f++; }
  }
  sprintf(symlink_name,"%s/%06d.msr",save_dir,run_number);  /* No version string */
  status=symlink(f, symlink_name);
  if(status==-1)
  {
    cm_msg(MERROR,"darc_set_symlink","failure from symlink, (errno=%d)",errno);
    write_link_msg(status);
    return(FAILURE);
  }
  else if(debug || debug_check)
  {
    printf("darc_set_symlink: successfully linked %s \n",symlink_name);
    printf("darc_set_symlink: to %s \n",filename);
  }
  return(SUCCESS);
}

INT
darc_unlink(INT run_number, char *save_dir)
{
/* looks for a file of type  run_number.msr, checks it is
   a symlink (not a real file) and deletes it.
   

     inputs:     run_number      use to generate name of 
                 save_dir        symbolic link

     returns:    0  No files were found 
                 1  file is a REGULAR final saved file not a SYMLINK
                 2  SUCCESS file was a symlink & was deleted successfully
                 3  file was a symlink but delete failed
        
                 

*/
  char filename[MAX_FILE_PATH];
  int status;
  
  if(debug_check)
  {
    printf("darc_unlink starting\n");
    printf("\n");
  }
  sprintf(filename,"%s/%06d.msr",save_dir,run_number);  /* No version string */
  
  status =  darc_find_saved_file(run_number, save_dir) ;
  if (status == 0)
  {
    if(debug)printf("darc_unlink: no file %s was found\n",filename);
    return(status);
  }
  
  if (status == 1)
  {
    printf("darc_unlink: A REGULAR final saved file already exists\n ");
    return(status);
  }
  
  /* this is the usual case */
  if( status  == 2)
  {
    if(debug_check)printf("INFO - a SYMBOLIC LINK already exists for this run (%d)\n",run_number);
    status = unlink(filename); /* deletes symbolic link */
    if (status == -1)
    {
      printf("darc_unlink: deleting symlink %s failed due to \n",filename);
      write_link_msg(status); //unlink
      return(3); /* status = 3 couldn't delete symlink */
    }
  }
  return(2); /* success */
}



INT
darc_kill_files(INT killed_run_number, char *save_dir)

{
/*
  Called if (global) toggle is set.
  After run number is toggled, and data is saved under the new run number, this routine
  is called to delete the saved files.

  Note - there will be no archived file for the old run number since they are copied on end-of-run.
  Toggle is only permitted if running.
  
  
  
  Input
         INT  killed_run_number run number of killed run
         char *save_dir      directory where files are saved
       
                             
   Output
         returns            SUCCESS or FAILURE
*/


  
  char dir[128];
  int nfile,i,j, len, status;
  char * list = NULL;
  char outFile[128];
  char *pt;
  char filename[MAX_FILE_PATH];  /* MAX_FILE_PATH is defined as 128  in /midas/include/ybos.h */
  char *s;
  int  max, min, ver, index;
  struct stat stat_buf;
  char copy_string[128];
  char my_cmd[256];
  int exitstatus;

  if(debug || debug_check )
    {
      printf("\n");
      printf("darc_kill_files starting with killed run number=%d & save_dir=%s\n",killed_run_number,save_dir);    
    }
  
  sprintf(filename,"%06d.msr",killed_run_number);
  sprintf(dir,"%s",save_dir);
  if (dir[0] != 0)
    if (dir[strlen(dir)-1] != DIR_SEPARATOR)
      strcat(dir, DIR_SEPARATOR_STR);
 
  // ss_file_find doesn't find sym link any more; delete it first

  //darc_find_saved_file looks for just .msr - no version string
  status = darc_find_saved_file(killed_run_number, save_dir);
  printf("darc_kill_files: darc_find_saved_files returns %d\n",status);
  if(status == 2)
    {
      printf("darc_kill_files: darc_find_saved_files found a file %s which is a symlink\n",filename);
      sprintf(my_cmd,"rm -f %s%s", dir, filename);
      printf("darc_kill_files: Sending system command %s\n",my_cmd);
      status =  system(my_cmd);
      exitstatus = WEXITSTATUS(status);
      if(debug_check)
	printf("darc_kill_files: After system command, status=%d and WEXITSTATUS = %d\n",status,exitstatus);
    }
  else
    printf("symlink %s does not exist\n",filename);

  
  // now delete versions
  sprintf(filename,"%06d.msr*",killed_run_number);
 
  if(debug_check)  printf("darc_kill_files:Looking for files: %s%s\n",dir,filename);
  
  nfile = ss_file_find(dir, filename, &list);
  
  if(debug_check)
    printf ("darc_kill_files: Found  %d files for killed run %d \n",nfile,killed_run_number);
  if(nfile <= 0 )
    {
      cm_msg(MINFO,"darc_kill_files","Found no saved files for killed run  %s%s",dir,filename);
      return(SUCCESS);
    }
  
  /* now delete the files */
  for (j=0; j<nfile; j++)
    {
      pt = list+j*MAX_STRING_LENGTH;    /* MAX_STRING_LENGTH (max string length for
					   odb) defined as 256 in  msystem.h */
      
      sprintf(outFile,"%s%s",dir,pt);
      if(debug_check)    
	printf("darc_kill_files: About to delete file %s (index=%d)\n",outFile,j);
      
    status = remove(outFile);
    if (status==-1)
      {
	cm_msg(MERROR,"darc_kill_files","Failure to delete  %s ",outFile,status);
	write_remove_msg(status);
	free(list);
	return(FAILURE); /* return */
      }
    else    
      {
	cm_msg(MINFO,"darc_kill_files","Successfully deleted old run file %s",outFile);
      }
    }
  free(list);
  //


 
  return(SUCCESS);
}


void
write_link_msg(INT status)
  /* translate error codes returned by unlink and symlink */
{
  if (errno ==EEXIST)   // no unlink 
    printf("     a symlink with this name already exists\n"); 
  else if (errno == ENOENT)
     printf("     Non-existent dir component or dangling symbolic link\n");
  else if (errno == ENOTDIR)
    printf("     component used as dir is not a directory\n");
   else if (errno == ENAMETOOLONG) //unlink,symlink  OK
    printf("     name too long\n");
  else if (errno == EISDIR)  // unlink OK;  
    printf("     pathname refers to a directoy\n");
  else if (errno == EPERM) //bad for unlink; symlink OK
    printf("     filesystem doesn't support symbolic links\n");
  else if (errno == EFAULT)  // unlink, symlink  OK
    printf("     filename(s) point outside accessible address space\n");
  else if (errno == EACCES) // unlink, symlink  OK
    printf("     write access or search permission not allowed in one of directories\n");
  else if (errno == ENAMETOOLONG) // unlink ,symlink OK
    printf("     pathname too long\n");
  else if (errno == EROFS)  // unlink,symlink  OK 
    printf("     pathname refers to a read-only filesystem\n");
  else if (errno == ELOOP)  // unlink symlink OK 
    printf("     too many symbolic links were encountered\n");
  else
    printf ("    an error not decoded by write_link_msg\n");
}




void
write_rename_msg(INT status)
  /* translate most common error codes returned by rename */
{
  if (errno ==EEXIST)    
    printf("     the new pathname contained a path prefix of the old\n");
  else if (errno == EISDIR)    
    printf("     newpath is an existing directory, but oldpath is not a directory\n");
  else if (errno == EXDEV)
    printf("     oldpath and newpath are not on the same filesystem\n");
  else if (errno == ENOTEMPTY)
    printf("     newpath is a non-empty directory\n");
  else if (errno == ENOTDIR)
    printf("     component used as dir is not a directory\n");
  else if (errno == EACCES) 
    printf("     write access or search permission not allowed in one of directories\n");
  else if (errno == EFAULT)  
    printf("     pathname(s) point outside accessible address space\n");
  else if (errno == ENAMETOOLONG) 
    printf("     pathname(s) were too long\n");
  else if (errno == ENOENT)
    printf("    non-existent dir component or dangling symbolic link\n");
  else if (errno == EROFS) 
    printf("     the file is on a read-only filesystem\n");
  else if (errno == ELOOP)   
    printf("     too many symbolic links were encountered\n");  
  else if (errno == ENOSPC)  
    printf("     device has no room for the new directory entry \n");


  else
    printf ("     an error not decoded by write_rename_msg\n");
}


void
write_remove_msg(INT status)
  /* translate error codes returned by remove */
{
  if (errno == EFAULT) 
    printf("     pathname points outside accessible address space\n");
  else if (errno == EACCES)
    {
      printf("     write access  to the directory containing pathname is not allowed\n"); 
      printf("     for the process's effective uid,  or  one of the directories in\n");
      printf("     pathname did not allow search (execute) permission.\n");
    }   
  else if (errno == EPERM)
    {
      printf("     directory containing pathname has the sticky-bit (S_ISVTX) set and the\n");
      printf("     process's effective uid is neither the uid of the file to be deleted\n");
      printf("     nor that of the directory containing it.\n"); 
    }
  else if (errno == ENAMETOOLONG) 
    printf("     pathname too long\n");
  else if (errno == ENOENT)
    printf("     non-existent dir component or dangling symbolic link\n");
   else if (errno == ENOTDIR)
    printf("     component used as dir in pathname is not a directory\n");
  else if (errno == ENOMEM)
    printf("  Insufficient kernel memory was available.\n");   
  else if (errno == EROFS)   
    printf("     pathname refers to a read-only filesystem\n");
  else
    printf ("    an error not decoded by write_remove_msg\n");
}



void
write_message(INT status)
{
  char str[60];
  str[0]= '\0';
 printf("write_message .... \n"); 
  if (status == DB_INVALID_HANDLE) sprintf(str,"    because of invalid database or key handle\n"); 
  else if (status == DB_NO_KEY) sprintf (str,"    because   key_name does not exist\n");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"    because type does not match type in ODB\n");
  else if (status == DB_TRUNCATED) sprintf(str,"    because  data does not fit in buffer and has been truncated\n"); 

}


void write_message1(INT status, char *name)
{
  /* messages for the most common return values from db_* routines */
  
  char str[60];
  str[0]= '\0';

  if (status == DB_INVALID_HANDLE) sprintf(str,"because of invalid database or key handle"); 
  else if (status == DB_NO_KEY) sprintf (str,"because   key_name does not exist");
  else if (status == DB_NO_ACCESS) sprintf (str,"because Key has no read access");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"because type does not match type in ODB");
  else if (status == DB_TRUNCATED) sprintf(str,"because data does not fit in buffer and has been truncated");
  else if (status == DB_STRUCT_SIZE_MISMATCH) sprintf (str,"because structure size does not match sub-tree size");
  else if (status == DB_OUT_OF_RANGE) sprintf (str,"because odb parameter is out of range");
  else if (status == DB_OPEN_RECORD) sprintf (str,"could not open record");   
  if (strlen(str) > 1 ) cm_msg(MERROR,name,"%s",str );
}


/*---- msg_print ---------------------------------------------------*/
INT msg_print(const char *msg)
{
  /* print message to system log */
  ss_syslog(msg);

  /* print message to stdout */
  return puts(msg);
}

INT
darc_archive_file(INT run_number, INT make_backup, char *save_dir, char *archive_dir)
{
/*
  Called by cleanup  (mdarc uses darc_rename_file for archiving)

  Archives data by copying the final run file to the directory given by
  archive_dir if archive_dir is not a null string. 

  If the file already exists in the archived directory, 
     if make_backup is true : copies new file making a backup of the existing file 
     else                     does not copy file to archive
  
  Input
         INT  run_number     run number
         INT  make_backup    true or false (see above)
	 char *save_dir      directory where files are saved
         char *archive_dir   directory where files are archived
       

                             
   Output

         returns            SUCCESS or FAILURE
        saved_filename      filename of saved file
*/


  
  char sdir[128];
  char adir[128];
  int nfile,i,j, len, status;
  char filename[MAX_FILE_PATH];  /* MAX_FILE_PATH is defined as 128  in /midas/include/ybos.h */
  char outFile[MAX_FILE_PATH];
  struct stat stat_buf;
  char copy_string[128];

  // for archiving
  FILE *errfile;
  INT first;
  char str[256];

  if(debug_check)
  {
    printf("\n");
    printf("darc_archive_file starting with run_number=%d, make_backup=%d\n",run_number,make_backup);
    printf("      and save_dir=%s, archive_dir=%s\n",save_dir,archive_dir);    
  }
  if (strlen(archive_dir) <= 0 )
    {
      printf("darc_archive_file: INFO - file not archived as save directory is NULL\n");
      return(FAILURE);
    }
  else if  (strlen(save_dir) <= 0 )
    {
    printf("darc_archive_file: INFO - file not archived as archive directory is NULL\n");
    return(FAILURE);
    }
  else if  ( run_number  >= MIN_TEST && run_number <=MAX_TEST  )
    {
    printf("darc_archive_file: INFO - test files are not archived \n");
    return(FAILURE);
    }

            
  sprintf(filename,"%06d.msr",run_number);
  sprintf(sdir,"%s",save_dir);
  if (sdir[0] != 0)
    if (sdir[strlen(sdir)-1] != DIR_SEPARATOR)
      strcat(sdir, DIR_SEPARATOR_STR);
  
  sprintf(outFile,"%s%s",sdir,filename);
  if(debug_check)  printf("darc_archive_file:Looking for saved files: %s\n",outFile);
  

  sprintf(adir,"%s",archive_dir);
  if (adir[0] != 0)
    if (adir[strlen(adir)-1] != DIR_SEPARATOR)
      strcat(adir, DIR_SEPARATOR_STR);
  
  if(debug_check)  printf("darc_archive_file:Checking archive for files: %s%s\n",adir,filename);
   

  /* check for REGULAR  msr file only */
 
  nfile = darc_find_saved_file(run_number, save_dir);
  if (nfile == 1)
    {
      if(debug) printf("darc_archive_file: Found saved file (%06d.msr)\n ",run_number) ;
    }
  else
    {
      printf("No regular saved file (%06d.msr) found. Archive not done\n",run_number);
      return (SUCCESS);
    }
  
  /* DO NOT...
     check if there is a file already in the archive  *****
  nfile = darc_find_saved_file(run_number, archive_dir);
  if(debug)printf("darc_archive_file: after darc_find_saved_file, nfile=%d; make_backup=%d\n",nfile,make_backup);
  if (nfile == 1)
    {
      if (make_backup)
	printf("darc_archive_file: Warning - there is an existing archived file. A backup (%06d.msr~) will be made.\n",run_number) ;
      else
	{
	  printf("darc_archive_file: Warning - there is an existing archived file (%s%06d.msr). Archiving aborted\n",adir,run_number) ;
	  return(SUCCESS);
	}
    }
  printf("darc_archive_file: found no existing archived file\n");

  */

  /* archiver string (read from ODB) submits a data file to the permanent archive */
 
  if (strlen (archiver) <= 0)
    {
      cm_msg(MINFO,"darc_rename_file","Archiver is a blank string; cannot archive");
      return(FAILURE);
    }
 
  sprintf( copy_string,"%s %s %s", archiver, outFile, archive_dir );
  if(debug_check)printf("darc_archive_file : Sending system cmd: %s\n",copy_string); 

  printf("Archive file %06d.msr to %s\n",run_number,archive_dir);
  
  status =  system(copy_string);

  if ( status )
    {
      printf("darc_archive_file : FAILURE\n");
      return (FAILURE);
    }
  else
    return(SUCCESS);
}


