/*
 $Log: darc_files.h,v $
 Revision 1.1  2013/01/21 21:42:38  suz
 initial VMIC version to cvs

 Revision 1.4  2007/07/27 20:15:26  suz
 minor change

*/
#ifndef darc_files_header
#define darc_files_header
void write_message(int status); /* old version - may use again */
void write_message1(INT status, char* name);
INT darc_find_saved_file(INT run_number, char *save_dir);
INT darc_kill_files(INT killed_run_number, char *save_dir);
INT darc_rename_file(INT run_number, char *save_dir, char *archive_dir, char *saved_filename, char *archived_filename,
		     BOOL *archived);
INT darc_set_symlink(INT run_number, char *save_dir, char *filename);
INT darc_unlink(INT run_number, char *save_dir);
void write_link_msg(INT status);
void write_rename_msg(INT status);
void write_remove_msg(INT status);
INT msg_print(const char *msg);
INT darc_archive_file(INT run_number, INT make_backup, char *save_dir, char *archive_dir);

#endif // darc_files_header
