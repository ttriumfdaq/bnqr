/*
 *  midbnmr_darc.c 
 *  Writes (periodic) mud files from time-integral bnmr/bnqr data (Midas events)
 *
 *
 * Version to run with VMIC frontend on 64-bit machine
 *
 *  $Log: midbnmr_darc.c,v $
 *  Revision 1.10  2017/04/29 05:06:20  asnd
 *  Eliminate some warnings and minor errors
 *
 *  Revision 1.9  2016/12/12 23:11:24  suz
 *  change Na Cell  to Rb Cell
 *
 *  Revision 1.8  2016/07/04 23:32:01  suz
 *  Donald's changes: log PPG parameters, replace M2MI_ by MBD_
 *
 *  Revision 1.7  2015/12/01 22:49:45  suz
 *  updates for post-Ted CAMP library
 *
 *  Revision 1.6  2015/11/27 23:24:01  suz
 *  added ifdef YBOS
 *
 *  Revision 1.5  2014/01/09 00:57:50  suz
 *  Changed odb total_saved from float to double
 *
 *  Revision 1.2  2013/04/08 22:25:53  suz
 *  Changes after debugging new VMIC code
 *
 *  Revision 1.1  2013/01/21 21:42:38  suz
 *  initial VMIC version to cvs
 *
 *  Revision 1.19  2012/09/28 04:42:41  asnd
 *  Copy first Camp/Epics vals to earlier cycles. Change time-per-bin to sum of all dwell-times. Alter timing of data copying and mem allocation. Fix warning from strndup.
 
 */

#define _GNU_SOURCE  /* to enable strndup in string.h */

#include <string.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>

/* 
 * camp_clnt.h must come before midas.h (or one must separately include rpc/rpc.h before midas.h)
 * because midas.h defines RPC_SUCCESS as preprocessor macro.
 */

#include "camp_clnt.h" 

#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif

#include "midas.h"
#include "msystem.h"
#ifdef HAVE_YBOS
#include "ybos.h"
#else
#include "mdsupport.h"  // defines MD_STRING_BANKLIST_MAX
#endif
#define BOOL_DEFINED
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif


#include "mud_util.h"

#include "experim.h"
#include "darc_odb.h"
#include "darc_files.h"
#include "trii_fmt.h"
#include "mdarc.h" /* prototypes and common variables */


/*  Global variables -------------------------------------------------------------- */

HNDLE hDB, hMDarc, hMidb;
FIFO_ACQ_MDARC fmdarc;
BOOL running_already;

static BOOL   FatalErr = FALSE;  /* remember if we continued from a fatal error */

extern BOOL campFail; /* flag a failure from camp to avoid camp hanging next time */
extern int campInit;
extern BOOL campUpdated;

static double htots[MAX_HIS];

/* Cycle Variables */

static DWORD  cycle;
static DWORD  scycle;
static DWORD  scan;
static DWORD  phase;
static DWORD  scanflg;
static DWORD  cellread;
static DWORD  cellmv;
static DWORD  frequency;

/* 
   scanflg:  0            1        2      256
   scantyp:  0            1        2      3 
   what:   ppg frequency  Rb Cell  Laser  Camp freq
*/

#define DIM_CE_VAR 64

static DWORD  indepval;
static int    numPoint;
static BOOL   headYet = FALSE;
static BOOL   useBits;
static BOOL   bNQRflag = FALSE;
static int    ndumpTot;
static double currcamp[DIM_CE_VAR];
static double currepics[DIM_CE_VAR];
static int    numCamp;
static int    numEpics;
static BOOL   iniCamp,  iniEpics;
static BOOL   gotCVals, gotEVals;
static int    numAllArr;
static int    numMonArr;
static int    numHistArr;
static int    numScalArr;
static int    evtdim = 0;    /* length of preceding arrays */
static D_ODB  mid_hdr;

/* Variabled for output file names */

static time_t lastSaved    = 0; 
static int    saveInterval = 30;  

#define OUTFILE_LEN 128

static char   outFile[OUTFILE_LEN] = { 0 };

/*  Variables for scan range -- were passed-parameters in stand-alone version */

#define MAX_BIN_RANGES 5

/* 
 * Parameters for time-bin ranges that are summed.  Note the btag is a prefix
 * on the histogram name
 */
static int  nBinRanges = 0;
static int  bin0[MAX_BIN_RANGES] = { 0 };
static int  bin1[MAX_BIN_RANGES] = { 0 };
static char btag[MAX_BIN_RANGES][8] = { { 0 } };

/* 
 *  Allocated memory:
 */

/* Camp variable(s) info; allocated by realloc, not released: */
static IMUSR_CAMP_VAR *campVars = NULL; 
/* Epics variable(s) info; allocated by realloc, not released: */
static IMUSR_CAMP_VAR *epicsVars = NULL;

/* User bits offsets ( = 4*ub) (or zeros); allocated by malloc, released by free: */
static int*   ubits = NULL; 

/* Array of scaler totals, allocated by realloc in MBD_expand, not freed: */
static unsigned int* tots = NULL;   

/* Plus mud file structures, allocated by MUD_new and released by MUD_free */

/* stuff for mlogger */
char MData_dir[256]; /* saved directory for midas files, filled by check_logger */

/* --------------------- Function declarations --------------------- */

int MBD_mud_write( int icmd, char* outFile );
static void camp_values_copy( DWORD ic );
static void epics_values_copy( DWORD ic );
static void MBD_check_alloc( ) ;
static void MBD_apply_htots( D_ODB *p_odb );
static int MBD_expand( unsigned int** pparray, int ncol, int* nrow );
void MBD_update_output();
void MBD_write_next( D_ODB *p_odb );

int MBD_get_dynamic_headers( D_ODB *p_odb_data ); /* variant DARC_get_dynamic_headers */

/* from bnmr_darc.c */
INT DARC_camp_connect ( char * camp_host );
void DARC_head_num( char* str, int len, double val, double err, char* units, BOOL eflag );

/*-- Begin of Run --------------------------------------------------*/

INT ana_begin_of_run()
{
  INT size;
  INT status;
  int i;

  char exp_type[32] = { 0 };


  if (debug_proc) 
    printf ("Executing begin-of-run code ana_begin_of_run()-------------\n");

  headYet  = FALSE ;   /* Have we processed header event yet? */
  useBits  = FALSE ;   /* Do we have user-bits? */
  numCamp  = 0 ;       /* Number of Camp variables logged */
  numEpics = 0 ;       /* Number of Epics variables logged */
  iniCamp  = FALSE;    /* Camp var list has not been determined (before DARC CAMP bank) */
  iniEpics = FALSE;    /* Epics var list has not been determined (before DARC EPICS bank) */
  gotCVals = FALSE;    /* Have we received Camp values (CVAR) yet? */
  gotEVals = FALSE;    /* Have we received Epics values (EVAR) yet? */
  numHistArr = 16 ;    /* Number of (F,B) histogram arrays per time-bin range */
  numScalArr = 16 ;    /* Number of scaler arrays (multiple of numHistArr) */
  numMonArr  = 6 ;     /* Number of extra monitored scalers */
  numAllArr  = 0 ;     /* Total number of arrays, incl indep var, scalers, and camp & epics */
  cycle      = 0 ;     /* Cycle and super-cycle have become the same. First cycle is 1 */
  scycle     = 0 ;     
  scan       = 0 ;
  phase      = 0 ;
  scanflg    = 0 ;
  cellmv     = 0 ;
  frequency  = 0 ;
  indepval   = 0 ;
  numPoint   = 0 ;

  lastSaved  = 0; 
  campUpdated = FALSE;

  FatalErr = FALSE;

  
  /*
   *  Zero out the camp/epics event records to make errors more recognizable
   */
  for (i=0; i<DIM_CE_VAR; i++)
  {
    currcamp[i] = 0.0;
    currepics[i] = 0.0;
  }
  for (i=0; i<MAX_HIS; i++)
  {
    htots[i] = 0.0;
  }
  MBD_apply_htots( &mid_hdr );

  /* 
   * Get experiment type, like "1f"
   */
  size = sizeof(exp_type);
  status =db_get_value(hDB, 0, "/Equipment/FIFO_acq/frontend/Input/Experiment name",
               exp_type, &size, TID_STRING, FALSE);
 
  if(status != DB_SUCCESS)
  {
      cm_msg(MERROR,"midbnmr_darc","Cannot get value of \"/Equipment/FIFO_acq/frontend/Input/Experiment name\"(%d)",status);
      return status;
  }

  /*
   * Set (global) flag for BNMR vs BNQR
   */
  if( strchr(beamline,'Q') || strchr(beamline,'q') )
    bNQRflag = TRUE;
  else
    bNQRflag = FALSE;

  /*
   * Load bin-range (regions) values from ODB here, and retain constant values 
   * for the whole run.
   * Initial checks on bin ranges (regions) are here, but
   * further checks (on the exact bin ranges) are deferred until
   * after the header (darc) event is processed. 
   */

  nBinRanges = fmdarc.histograms.midbnmr.number_of_regions;
  if( nBinRanges < 1 ) nBinRanges = 1 ;

  if(nBinRanges > MAX_BIN_RANGES)
    {
      cm_msg(MERROR,"ana_begin_of_run","too many regions (%d); maximum is %d",
	     fmdarc.histograms.midbnmr.number_of_regions,MAX_BIN_RANGES);
      return (DB_INVALID_PARAM);
    }

  for(i=0; i<nBinRanges; i++)
    {
      bin0[i] = fmdarc.histograms.midbnmr.first_time_bin[i];
      bin1[i] = fmdarc.histograms.midbnmr.last_time_bin[i];
      strncpy(btag[i],fmdarc.histograms.midbnmr.range_label[i],7);
      btag[i][7] = '\0';
    }

  /*
   * In case I previously returned without freeing ubits...
   */
  if( ubits ) 
  {
    free( ubits );
    ubits = NULL;
  }

  MBD_check_alloc() ;

  return (SUCCESS);
}



/*-- End of Run ----------------------------------------------------*/

INT ana_end_of_run(INT run_number)
{
  if (numPoint == 0)
    {
      printf("ana_end_of_run: Error -- there were no data points in this run.\n");
      cm_msg(MERROR,"ana_end_of_run","Failure to save final data file: there were no data points in this run.");
      return SUCCESS;
    }

  camp_values_copy( numPoint-1 );
  epics_values_copy( numPoint-1 );

  if ( headYet )
    if ( FatalErr ) 
      {
        printf("ana_end_of_run: Error -- try to end run that had a previous fatal error.\n");
        cm_msg(MERROR,"ana_end_of_run","Failure to save final data file: Previous fatal error.");
      }
    else
      {
	MBD_write_next( &mid_hdr );
      }
  else
    {
      if(debug_check)
        printf("ana_end_of_run: Error -- try to end run with no header yet.\n");
        cm_msg(MERROR,"ana_end_of_run","Failure to save final data file: no header yet.");
    }

  if (campInit) 
    {
      camp_clntEnd(); 
      campInit = FALSE;
    }

  return (SUCCESS);
}


/* Here are the meanings of the status codes as in midbnmr.  

 *   0) Success
 *   1) file <infile> could not be opened
 *   2) could not allocate memory
 *   3) events out of order (data before header etc)
 *   4)
 *   5) could not parse camp paths
 *   6) number of camp variables changed
 *   7) could not write output mud file
 *   8) could not parse epics paths
 *   9) number of epics variables changed

*/


/* 
 ID  # banks
  2  5 CYCL(2) HIBP HIFP HM00 HM01 HM02 HM03 HM04 HM05 UBIT   Histogram data
  3  1 HSCL(40)   Scaler readout
  4  1 CH01(100)  ---- debugging? Ignore
  9  2 DMND MSRD  feEpics event -- Ignore
 13  1 CVAR       Logged Camp variable values
 14  3 DARC CAMP EPICS     Header info and logged variables, sent once or twice in a run.
 19  1 EVAR       Logged Epics variable values

*/


/******************************************************************
 *    camp_values_copy and epics_values_copy copy the values
 *    received from CVAR and EVAR events into the general histogram 
 *    arrays.  If we have not copied any previously, fill in any
 *    preceding rows with these values. 
 ******************************************************************/

/*     DWORD ic;  cycle index:  point number minus 1 */

static void camp_values_copy( DWORD ic )
{
  int i, jc, j0;
  float f;

  if ( ic < 0 ) return;

  j0 = ( gotCVals ? ic : 0 );

  if (numCamp <= 0) return;

  if(debug_proc) {
    if ( j0 < ic ) {
      printf( "camp_values_copy: Copy %d camp values for cycles %d thru %d.\n", numCamp, j0+1, ic+1 );
    } else {
      if(debug_proc>1)
        printf( "camp_values_copy: Copy %d camp values for cycle %d.\n", numCamp, ic+1 );
    }
  }

  for( jc=j0; jc<=ic; jc++)
  {
    for( i=0; i<numCamp; i++ )
    {
      f = currcamp[i];
      tots[numAllArr*jc+1+numScalArr+numMonArr+i] = *((int*)&f); /* float not converted */
      if(debug_proc>1)printf("camp_values_copy: copied camp value %f at index %d to tot[%d]\n",
                           f,i, (numAllArr*jc+1+numScalArr+numMonArr+i) );
    }
  }
  return;
}

static void epics_values_copy( DWORD ic )
{
  int i, jc, j0;
  float f;

  if ( ic < 0 ) return;

  j0 = ( gotEVals ? ic : 0 );

  if (numEpics <= 0) return;

  if(debug_proc) {
    if ( j0 < ic ) {
      printf( "epics_values_copy: Copy %d epics values for cycles %d thru %d.\n", numEpics, j0+1, ic+1 );
    } else {
      if(debug_proc>1)
        printf( "epics_values_copy: Copy %d epics values for cycle %d.\n", numEpics, ic+1 );
    }
  }

  for( jc=j0; jc<=ic; jc++)
  {
    for( i=0; i<numEpics; i++ )
    {
      f = currepics[i];
      tots [numAllArr*jc+1+numScalArr+numMonArr+numCamp+i] = *((int*)&f); /* float not converted */
      if(debug_proc>1)printf("epics_values_copy: copied epics value %f at index %d to tot[%d]\n",
                           f,i, (numAllArr*jc+1+numScalArr+numMonArr+numCamp+i) );
    }
  }
  return;
}

 /*******************************************************************\
 *                                                                   *
 *                   THE EVENT ANALYZER FUNCTIONS                    *
 *                                                                   *
 \*******************************************************************/

#define MEM_ALLOC_ERROR(size) \
    { \
      FatalErr = TRUE; \
      if (debug_proc) \
        printf("midbnmr_darc: Failed to allocate memory (%d bytes)\n", ( size ) ); \
      cm_msg(MERROR,"midbnmr_darc", \
            "Failed to allocate memory (%d bytes)", ( size ) ); \
    }


#define NUM_CAMP_ERROR(from,to) \
    { \
      cm_msg(MERROR,"midbnmr_darc", \
          "Number of Camp (or Epics) variables changed during run (%d to %d)", \
              ( from ), ( to ) ); \
    }
  

/********************************************************************/
void process_histo_event (HNDLE hBuf, HNDLE req, EVENT_HEADER *pheader, void *pevent)
/********************************************************************\
  Routine: process_histo_event
  Purpose: Read histogram events; set (global) scycle and point count.
           sum events for each histogram (="bank")).
  Input:   Pointers to event.  Lots of "global" (this source file only) 
           variables declared above.
  Output:  Add data to arrays
\********************************************************************/
{
  INT     i, jj, n_items;
  DWORD  * pdata ;
  DWORD   thiscycle ;
  INT     ipt;
  unsigned char * p;
  unsigned char   u;
  int     bin0x, bin1x;
  float   f;
  BOOL    newCycle = FALSE;
  INT status;
  INT nbk;
  BANK  *pmbk;
  BANK_HEADER *pmbh;
#ifdef YBOS
  char  banklist[YB_STRING_BANKLIST_MAX];
#else
 char  banklist[MD_STRING_BANKLIST_MAX];
#endif
  static char *MonBankNames[8] = { "HM00", "HM01", "HM04", "HM05" };
  static char *HisBanksBNMR[8] = { "HIBP", "HIFP" };
  static char *HisBanksBNQR[8] = { "HM02", "HM03" };

  char ** HistBankName;

  int mb; 
  int ibr;

  if( bNQRflag ) 
    HistBankName = HisBanksBNQR;
  else
    HistBankName = HisBanksBNMR;

  /*
   * Note yb_any_event_swap is done by process_event before calling this routine
   */
  if (debug_proc) 
    {
      printf("process_histo_event: hBuf=%p req=%d pheader=%p  pevent=%p\n", hBuf, req, pheader, pevent);
      printf(" process_histo_event: Starting with Ser: %ld, ID: %d, size: %ld \n",
	     pheader->serial_number, pheader->event_id, pheader->data_size);
      pmbh = (BANK_HEADER *) pevent;
      nbk = bk_list(pmbh, banklist);
      printf("process_histo_event: #banks:%i Bank list:-%s-\n", nbk, banklist);
    }

  /* 
   *  Quit immediately if there was a previous fatal (memory allocation) error
   *  or if we missed the beginning of the run.
   */

  if (FatalErr) return;
  if (running_already) return;

  ipt = numPoint-1;

  if( !headYet ) 
    {
    cm_msg(MINFO,"process_histo_event","Hist event received but no header yet; cannot save data; restart the run");
    return;  
  }

  /*
   * Process CYCL bank before the various HIST banks. 
   * If we have preceding HIST event saved, then output saved contents.
   */

  n_items = bk_locate(pevent, "CYCL", &pdata);

  if (n_items > 6)
  {
    cycle  =  *pdata++;
    thiscycle = *pdata++;
    scan   =  *pdata++;
    phase  =  *pdata++;
    scanflg = *pdata++;
    cellread=  *pdata++;
    cellmv  =  *pdata++;
    frequency = *pdata++;
    indepval = ( scanflg == 1 || scanflg == 2 || scanflg == 3 ? cellmv : frequency );

    if (debug_proc) 
      printf( "Hist event size %5d: %5d %5d %5d %5d %5d %9d %9d %11d %11d\n",
	      n_items, cycle, thiscycle, scan, phase, scanflg, cellread, cellmv, frequency, indepval);

    if( thiscycle < scycle )
    {
      cm_msg(MERROR,"process_histo_event",
             "Bad events: scycle jumped from %d to %d!\n", scycle, thiscycle );
      thiscycle = scycle + 1;  /* will this handle error?  Probably better to DIE */
    }

    newCycle = ( (thiscycle > scycle) );
    if (debug_proc)
      printf("process_histo_event: newCycle=%d  thiscycle = %d  scycle = %d\n",
	     newCycle, thiscycle, scycle );
    scycle = thiscycle;
    
  }

  /*
   * Look for, and read, user bits only once (null pointer indicates not read)
   * If no user bits bank in this first event, then declare all user bits zero.
   */

  if( !ubits )
  {
    if(debug_proc)printf("locating user bit bank\n");
    n_items = bk_locate( (BANK_HEADER *)pevent, "UBIT", &pdata);

    if( n_items > 0 )
    {
        if (debug_proc) 
            printf( "process_histo_event: Found user-bits for %d time-slices\n", n_items );      
        ubits = malloc( n_items*sizeof(int) );
        if( ubits == NULL )
	  { 
	    printf("calling MEM_ALLOC_ERROR\n");
            MEM_ALLOC_ERROR( n_items*sizeof(int) );
	    return;
	  }
        ubits[0] = (4 * pdata[0]) ;
	for( i=1; i<n_items; i++ )
        {
            ubits[i] = (4 * pdata[i]) ;
            if( ubits[i] != ubits[i-1] ) useBits = TRUE;
        }
    }
    else     /* no user bits, make array anyway */
    {
        if (debug_proc) 
            printf( "process_histo_event: No user-bits so prepare %d zeros\n", mid_hdr.his_nbin );
      
        ubits = malloc( mid_hdr.his_nbin*sizeof(int) );
        if( ubits == NULL ) 
        { 
	    printf("calling MEM_ALLOC_ERROR\n");
            MEM_ALLOC_ERROR( n_items*sizeof(int) );
	    return;
	}
        useBits = FALSE;
    }
    if( ! useBits ) {    /* no user bits -- fill array with zeros */
       bzero( ubits, mid_hdr.his_nbin*sizeof(int) );
    }
    numHistArr = ( useBits ? 16 : 4 ) ;
    {
      int jj,kk;
      kk =  numScalArr;
      numScalArr = numHistArr * nBinRanges ;
      if(kk != numScalArr)
	{
	  if(debug_proc)printf("numScalArr changed from %d to %d; numHistArr=%d nBinRanges=%d\n",
			       kk,numScalArr,numHistArr,nBinRanges);
	}
    }
    numAllArr = 1 + numScalArr + numMonArr + numCamp + numEpics;
  }

  if( newCycle )
  {
    if (debug_proc)
      printf("process_histo_event: New cycle %d.  numPoint %d. Perhaps update output when evtdim = %d numAllArr=%d\n",
	     thiscycle, numPoint, evtdim, numAllArr);
    /* Before new cycle, perhaps write a new mud file with points so far. */
    if( numPoint > 0 ) MBD_update_output();

    /* Check and revise memory allocation */
    MBD_check_alloc();
    if (debug_proc) 
      printf("After (possible) memory alloc, evtdim = %d\n",evtdim);
    
    /* zero tots for new scycle (numPoint - 1 + 1) */
    for( i=0; i<numAllArr; i++ ) tots[numPoint*numAllArr+i] = 0;
    
    camp_values_copy( numPoint );
    epics_values_copy( numPoint );

    numPoint++;
    ipt++;

  }
  if (debug_proc) 
    printf("process_histo_event: tots index=%d, indepval =%d\n", ipt*numAllArr+0,indepval) ;
  tots[ipt*numAllArr+0] = indepval;

  /*
   *  Now read the histograms of the 4 types (split into 16 if user bits)
   *
   *  As written here, they are only for ordinary imusr!
   *  Need to add cases of icmd.
   *  TD: No tots, but compile the TD histograms (check user bits -- warn only)
   *  3D: Preserve raw arrays when 3d.
   */
  if (debug_proc) 
    {
      printf("IT %d:",ipt);
      for(i=0;i<8;i++) printf(" %d", tots[ipt*numAllArr+i]);
      printf("\n");
    }

  /*  
   *  Collect Histogram banks, split into user bits, summing for each bin range
   */

  n_items = bk_locate(pevent, HistBankName[0], &pdata);
  if (n_items > 0)
  {
    if (debug_proc) 
      printf("Hist bank %s with %d items, ", HistBankName[0], n_items);
    for( ibr=0 ; ibr < nBinRanges ; ibr++ )
    {
      bin1x = _min(bin1[ibr],n_items);
      for( i=bin0[ibr]-1 ; i<bin1x ; i++ ) {
        tots[(ipt*numAllArr)+1+(ibr*numHistArr)+(2*phase)+ubits[i]] += pdata[i];
      }
    }
  }

  n_items = bk_locate(pevent, HistBankName[1], &pdata);
  if (n_items > 0)
  {
    if (debug_proc) 
      printf("Hist bank %s with %d items, ", HistBankName[1], n_items);
    for( ibr=0 ; ibr < nBinRanges ; ibr++ )
    {
      bin1x = _min(bin1[ibr],n_items);
      for( i=bin0[ibr]-1 ; i<bin1x ; i++ ) {
        tots[(ipt*numAllArr)+1+(ibr*numHistArr)+(2*phase)+ubits[i]+1] += pdata[i];
      }
    }
  }

  /* Collect Monitor Banks "HM00" - "HM01" (direct copy) */

  for( mb=0 ; mb < 2 ; mb++ )
  {
    n_items = bk_locate(pevent, MonBankNames[mb], &pdata);
    if (n_items > 0)
    {
      if (debug_proc) 
        printf("%s with %d items, ",MonBankNames[mb],n_items);
      for( i=0; i<n_items; i++ ) {
        tots[(ipt*numAllArr)+1+numScalArr+mb] += pdata[i];
      }
    }
  }

  /* DO NOT ! Collect Other-spectrometer's hist Banks "HM02" - "HM03" (split by phase) */

  /* Collect Neutral beam monitor Banks "HM04" - "HM05" (split by phase) */

  for( mb = 2 ; mb < 4 ; mb++ )
  {
    n_items = bk_locate(pevent, MonBankNames[mb], &pdata);
    if (n_items > 0)
    {
      if (debug_proc) 
        printf("%s with %d items, ",MonBankNames[mb],n_items);
      for( i=0; i<n_items; i++ )
        tots[(ipt*numAllArr)+1+numScalArr+(2*phase)+mb] += pdata[i];
    }
  }

  for ( i=0; i<_max(MAX_HIS,numScalArr+numMonArr); i++ )
  {
    htots[i] += (double)tots[(ipt*numAllArr)+1+i];
  }

  if (debug_proc) 
    printf("\n");

  return;
}


/********************************************************************/
void process_darc_event (HNDLE hBuf, HNDLE req, EVENT_HEADER *pheader, void *pevent)
/********************************************************************\
  Routine: process_darc_event
  Purpose: Read header event (darc and camp banks)
  Input:   Pointers to event
  Output:  Save header in static structure
\********************************************************************/
{
  INT     n_items, n_char, n_var_odb, n_var_bank, status;
  DWORD * pdata ;
  static  char  * pbuff = NULL;
  static  int     buffSize = 0;
  int     nitem;
  // char  * p;
  // char  * pn;
  int  i;
  //  long l;  see if these are used -> int 
  // int nc, ne;
  // unsigned long lp;


  if(running_already)
    {
      if (debug_proc) 
	printf("process_darc_event: returning as running_already is true\n");
      
      return; // Run was going when mdarc started
    }

  /// yb_any_event_swap(FORMAT_MIDAS, pheader);

  /* 
     ======================== DARC BANK ============================
  */
  n_items = bk_locate(pevent, "DARC", &pdata);
  if (debug_proc) 
    printf( "process_darc_event: Located DARC (head) event with size %d.\n",n_items);
  
  if (n_items > 0)
  {
    if (n_items != sizeof(mid_hdr))
    {
      fprintf( stderr, "FATAL ERROR: unrecognized run header size: %d; expected %d.\n", 
	       n_items, sizeof(mid_hdr) );
      cm_msg(MERROR,"process_darc_event", \
          "unrecognized run header size: %d.", n_items);
      return;
    }
    memcpy( (char*) &mid_hdr, (char*) pdata, sizeof(mid_hdr) );

    headYet = TRUE;
    /*
     *  Now that we know the bin range of the data, perform sanity checks on 
     *  requested bin ranges. Bin numbering is natural 1...n, not
     *  C array style 0...n-1 (??)
     */
      for( i=0; i<nBinRanges; i++ )
      {
          if( bin0[i] < 1 )  bin0[i] = 1;
          if( bin0[i] >= bin1[i] )
          {
              bin0[i] = 1;
              bin1[i] = mid_hdr.his_nbin;
          }
          if( bin1[i] > mid_hdr.his_nbin )  bin1[i] = mid_hdr.his_nbin;
      }

  }

  /* 
     ======================== CAMP BANK ============================
  */

  n_char = bk_locate(pevent, "CAMP", &pdata);
  n_var_bank = n_char/sizeof(IMUSR_CAMP_VAR);
  if (n_char > 0)
    {
      if(debug_proc) 
        printf("\n process_DARC_event: Located CAMP bank of with size %d\n",n_char);
    }
  else
    {
      if(debug_proc) 
        printf("\n process_DARC_event: No CAMP bank found\n");
    }

  /* If there is no Camp bank this time, but we already have seen a Camp bank, then skip over handling */
  /* (Yes, I preferred goto over widely separated braces.) */

  if ( n_char <= 0 && iniCamp ) goto doneCamp;

  /* Get number of camp variables logged.  Do so even if we will ignore the number, so
   * as to report a message.  Compare three possible values: previous value, odb request,
   * and actual bank size. */
  n_var_odb = 0;
  size = sizeof(n_var_odb);
  status = db_get_value(hDB,0, "/Equipment/camp/settings/n_var_logged", &n_var_odb, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"process_darc_event","key not found \"/Equipment/camp/settings/n_var_logged\"(%d)",status);
      write_message1(status,"process_darc_event");
    }
  
  if(debug_proc) 
    printf ("numCamp from bank = %d, from odb n_var_logged = %d \n",
	    n_var_bank,n_var_odb);
  
  if (n_char <= 0)
    {
      n_var_bank = n_var_odb;
      if(debug_proc) 
	printf ("No Camp bank so use number from odb (%d)\n",n_var_odb);
      cm_msg(MINFO,"process_darc_event","No CAMP bank so use number from odb (%d)\n",n_var_odb);
    }
  
  if ( n_var_bank != n_var_odb)
    {
      cm_msg(MINFO,"process_darc_event",
	     "Num. camp logged variables found in CAMP bank (%d) does not agree with number requested (%d)",
	     n_var_bank,n_var_odb); 
      cm_msg(MINFO,"process_darc_event","using number found in bank i.e. %d",n_var_bank);
    }

  if ( iniCamp ) /* Camp logging is already defined */
    {
      if ( n_var_bank != numCamp ) 
	{
	  cm_msg(MINFO,"process_darc_event",
		 "New number of Camp logged variables (%d) does not agree with previous (%d).  Ignore new",
		 n_var_bank,numCamp);
	}
    }
  else /* not init yet; so define now */
    {
      numCamp = n_var_bank;
      if (numCamp > DIM_CE_VAR) numCamp = DIM_CE_VAR;
      if(numCamp > 0)
	{
	  campVars = realloc( (void*) campVars, (size_t) numCamp*sizeof(IMUSR_CAMP_VAR) );
	  if( !campVars ) 
	    {
	      if(debug_proc) {
		printf("campVars is false, n_char=%d, numCamp=%d\n",n_char,numCamp*sizeof(IMUSR_CAMP_VAR));
		printf("calling MEM_ALLOC_ERROR\n");
	      }
	      MEM_ALLOC_ERROR(n_char);
	      return;
	    }
	}
      else
	cm_msg(MINFO,"process_DARC_event","no CAMP variables are selected; not allocating memory");
    }

  /* 
   * Copy camp variable structure.  Note that structure contains fixed length 
   * strings, not just pointers, so memcpy does copy everything.
   */
  if(numCamp > 0)
    {
      if ( n_char > numCamp*sizeof(IMUSR_CAMP_VAR) )
	n_char = numCamp*sizeof(IMUSR_CAMP_VAR);
      if ( n_char > 0 )
	memcpy( campVars, (char*) pdata, n_char );
    }

 doneCamp:

  iniCamp = TRUE;

  /* 
   ======================== EPICS BANK ============================
   */

  n_char = bk_locate(pevent, "EPICS", &pdata);
  n_var_bank = n_char/sizeof(IMUSR_CAMP_VAR);
  if ( n_char > 0 )
    {
      //      if(debug_proc) 
      printf("\n process_DARC_event: Located EPICS bank of size %d. numEpics=%d\n",n_char,numEpics);
    }
  else
    {
      //    if(debug_proc) 
      printf("\n process_DARC_event: No EPICS bank found\n");
    }

  /* If there is no Epics bank this time and we have already seen one, then skip over handling */

  if ( n_char <= 0 && iniEpics ) goto doneEpics;

  /* get number of epics variables logged, even if we already have a set number so we
   * can output information messages.  Compare three possible values: previous value, 
   * odb request, and actual bank size. */

  n_var_odb = 0;
  size = sizeof(n_var_odb);
  status = db_get_value(hDB,0, "/Equipment/epicslog/settings/output/num_log_ioc",  
			&n_var_odb, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"process_darc_event","key not found \"/Equipment/epicslog/settings/output/num_log_ioc\" (%d)",status);
      write_message1(status,"process_darc_event");
    }

  if(debug_proc) 
    printf ("numEpics from bank = %d, from odb num_log_ioc = %d \n",
	    n_var_bank,n_var_odb);

  if (n_char <= 0) 
    {
      n_var_bank = n_var_odb;
      if(debug_proc) 
	printf ("No Camp bank so use number from odb (%d)\n",n_var_odb);
      cm_msg(MINFO,"process_darc_event","No EPICS bank so use number from odb (%d)\n",n_var_odb);
    }

  if ( n_var_bank != n_var_odb)
    {
      cm_msg(MINFO,"process_darc_event",
	     "Num. epics logged variables found in EPICS bank (%d) does not agree with number requested (%d)",
	     n_var_bank,n_var_odb); 
      cm_msg(MINFO,"process_darc_event","using number found in bank i.e. %d",n_var_bank);
    }

  if ( iniEpics )  /* Epics logging is already defined */
    {
      if ( n_var_bank != numEpics ) 
	{
	  cm_msg(MINFO,"process_darc_event",
		 "New number of Epics logged variables (%d) does not agree with previous (%d).  Ignore new",
		 n_var_bank,numEpics);
	}
    }
  else /* not init yet; so define */
    {
      numEpics = n_var_bank;
      if (numEpics > DIM_CE_VAR) numEpics = DIM_CE_VAR;
      if(numEpics > 0)
	{
	  epicsVars = realloc( (void*) epicsVars, (size_t) numEpics*sizeof(IMUSR_CAMP_VAR) );
	  if( !epicsVars ) 
	    { 
	      if(debug_proc) 
		{
		  printf("epicsVars is false, n_char=%d, numEpics=%d\n",n_char,numEpics);
		  printf("calling MEM_ALLOC_ERROR\n");
		}
	      MEM_ALLOC_ERROR(n_char);
	      return;
	    }
	}
      else
	cm_msg(MINFO,"process_DARC_event","no EPICS variables are selected; not allocating memory");
    }

  /* 
   * Copy epics variable structure.  Note that structure contains fixed length 
   * strings, not just pointers, so memcpy does copy everything.
   */
  if(numEpics > 0)
    {
      if ( n_char > numEpics*sizeof(IMUSR_CAMP_VAR) )
	n_char = numEpics*sizeof(IMUSR_CAMP_VAR);
      if ( n_char > 0 )
	memcpy( epicsVars, (char*) pdata, n_char );	  
    }

 doneEpics:

  iniEpics = TRUE;

  /*************************************************************************************************/

  numAllArr = 1 + numScalArr + numMonArr + numCamp + numEpics ;

  MBD_check_alloc() ;

  if(debug_proc) 
    printf("process_darc_event: returning with numAllArr=%d, numScalArr=%d,numMonArr=%d,numCamp=%d numEpics=%d\n",
	   numAllArr,numScalArr,numMonArr,numCamp,numEpics);
  return;
}


/********************************************************************/
void process_camp_event (HNDLE hBuf, HNDLE req, EVENT_HEADER *pheader, void *pevent)
/********************************************************************\
  Routine: process_camp_event
  Purpose: Read list of camp variable values -- processing a CVAR event actually
  Input:   pointers to event
  Output:  Data in array currcamp[]
\********************************************************************/
{
  INT i,n_items;
  double *pdata;

  if(running_already)
    {
      if (debug_proc) 
	printf("process_camp_event: returning as running_already is true\n");
      
      return; // run was going when mdarc started
    }

  //yb_any_event_swap(FORMAT_MIDAS, pheader);
 
  n_items = bk_locate(pevent, "CVAR", &pdata);
  if (debug_proc)
    printf("process_camp_event: CVAR n_items=%d numCamp=%d\n",n_items, numCamp);

  if (n_items == 0) return;

  if (n_items != numCamp) 
  {
      NUM_CAMP_ERROR (numCamp, n_items);
      return;
  }

  for (i=0; i<n_items; i++)
    {
      currcamp[i] = pdata[i];
      if(debug_proc)printf("CVAR : data[%d] = %f\n",i,currcamp[i]);
    }

  if (numPoint > 0) 
    camp_values_copy( numPoint - 1 );

  gotCVals = TRUE;

  return; 
}


/********************************************************************/
void process_epics_event (HNDLE hBuf, HNDLE req, EVENT_HEADER *pheader, void *pevent)
/********************************************************************\
  Routine: process_epics_event
  Purpose: Read list of epics variable values.
  Input:   pointers to event
  Output:  Data in array currepics[]
\********************************************************************/
{
  INT i,n_items;
  double *pdata;

 if(running_already)
    {
      if (debug_proc) 
	printf("process_epics_event: returning as running_already is true\n");
      
      return; // run was going when mdarc started
    }

 //yb_any_event_swap(FORMAT_MIDAS, pheader);
 
  n_items = bk_locate(pevent, "EVAR", &pdata);
  if (debug_proc)
    printf("process_epics_event: EVAR  n_items=%d numEpics=%d\n", n_items, numEpics);

  if (n_items == 0) return;

  if (n_items != numEpics) 
    {
      NUM_CAMP_ERROR (numEpics, n_items);
      return;
    }

  for (i=0; i<n_items; i++)
    {
      currepics[i] = pdata[i];
      if(debug_proc) printf("EVAR : data[%d] = %f\n",i,currepics[i]);
    }

  if (numPoint > 0) 
    epics_values_copy( numPoint - 1 );

  gotEVals = TRUE;

  return; 
}


/*   ------------------- End of event analyzers ------------------- */


/**********************************************************************\
*                                                                      *
*                          Memory Management                           *
*                                                                      *
*   campvars  is allocated and extended by realloc, and not released   *  
*   epicsvars  is allocated and extended by realloc, and not released  *  
*                                                                      *
*   ubits     is allocated by malloc, and freed at the end of the run  *
*                                                                      *
*   tots      is the main array, allocated by realloc (in MBD_expand)  *
*             and not freed.  (This means a long run causes the memory *
*             consumption to go up, and stay high even for later short *
*             runs.  But this happens anyway due to system's memory    *
*             management.                                              *
*                                                                      *
\**********************************************************************/
 
static void
MBD_check_alloc ( ) 
{
  /*
   *  Always allocate enough array space for the *next* scycle plus a bunch.
   */
  if(debug_proc)
    printf("MBD_check_alloc starting with numPoint=%d and evtdim = %d numAllArr=%d\n",
	   numPoint,evtdim,numAllArr);

  if( numPoint+100 > evtdim && numAllArr > 0 )
    {
      if( MBD_expand( &tots, numAllArr, &evtdim ) )
        {
	  printf("calling MEM_ALLOC_ERROR\n");
          MEM_ALLOC_ERROR ( evtdim );
	  return;
        }
    }
  return  ;
}

static int
MBD_expand( unsigned int** pparray, int ncol, int* nrow )
{

  size_t siz;

  if(debug_proc)
    printf("MBD_expand starting with *nrow=%d and ncol=%d\n",*nrow,ncol);
  *nrow = *nrow * 1.2 + 102400;
  siz = *nrow * ncol * 4;

  if( ( *pparray = (unsigned int*)realloc( (void*)*pparray, siz) ) ) return( 0 );

  return( 2 );
}

 /**********************************************************************\
 *                                                                      *
 *      Writing out the mud file, using all data points so far.         *
 *                                                                      *
 *   MBD_update_output()  --  called after every data point. Writes     *
 *                            data file if save_interval has passed.    *
 *                                                                      *
 *   MBD_write_next(header, run_number) --                              *
 *                             called by MBD_update_output, pause       *
 *                             run, and end run.  Writes next version   *
 *                             of the output file                       *
 *                                                                      *
 *   MBD_mud_write()      --  called by MBD_write_next.  Assembles      *
 *                            the mud data and writes the file.         *
 *                                                                      *
 \**********************************************************************/


/*
 *   MBD_update_output
 *
 *   Check if not saved within preceding save_interval, and save new
 *   version if necessary (MBD_write_next)
 */

void MBD_update_output()
{
  if(debug_check)printf("MBD_update_output: Starting\n");
  if ( headYet && (time((time_t)NULL) > lastSaved + (time_t)fmdarc.save_interval_sec_) )
    {
      if(debug_check)printf("MBD_update_output: calling MBD_write_next\n");
      MBD_write_next( &mid_hdr );
    }
}

/*
 *   MBD_write_next(p_Darc_odb) -- called by MBD_update_output,     
 *
 *   Write the next version of the data file, and produce a symbolic link to it.
 *
 *   This version has been made almost identical with the code in
 *   bnmr_darc.c with the intention of combining them into a single 
 *   function.
 */

void MBD_write_next( D_ODB* p_odb_data )
{
  int icmd = 2; /* midas type-1 to mud integral */
  int status,size,nfile,next_version;
  char strLastSaved[32];
  INT no_purge=0;

  /* Write saved-totals to ODB */
  MBD_apply_htots( p_odb_data );

  /* Update dynamic header information only just before writing file */
  MBD_get_dynamic_headers( p_odb_data );

  /* see if there are any files already with this run number to find the next version */  
  if(debug_check)printf("MBD_write_next: Calling darc_check_files with keep=%d\n",no_purge);
  
  nfile = darc_check_files (p_odb_data, no_purge, &next_version);
  
  if(debug_check)printf("Found %d files for this run. Next file version will be %d\n",
                        nfile,next_version);
  
  if(next_version == 0)
    {
      cm_msg(MERROR,"MBD_write_next","Cannot save data file ");
      return;
    }
  
  sprintf( outFile, "%s/%06d.msr_v%d",
           p_odb_data->save_dir,p_odb_data->run_number, next_version );
  
  /*
   *  Remember when saved
   */
  
  lastSaved = time((time_t)NULL);
  strcpy(strLastSaved, ctime(&lastSaved));
  strLastSaved[24] = '\0';
  
  status = MBD_mud_write( icmd, outFile );
  if ( status )
    {
      cm_msg(MERROR,"MBD_write_next",
             "Failed to write output mud file %s", outFile) ;
      return;
    }
  else
    {
      cm_msg(MINFO,"MBD_write_next",
             "*** Saved data file %s at %s ***", outFile, strLastSaved) ;
    }
  
  size = sizeof(strLastSaved);
  status = db_set_value(hDB, hMDarc, 
                        "time_of_last_save", 
                        strLastSaved, size, 1, TID_STRING);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"MBD_write_next","failed to write \"time of last save\" to mdarc area (%d)",status);
      return;
    }
  
  size = sizeof(outFile);
  status = db_set_value(hDB, hMDarc, 
                        "last_saved_filename", 
                        outFile, size, 1, TID_STRING);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"MBD_write_next","failed to write \"last_saved_filename\" to mdarc area (%d)",status);
      return;
    }
  
  /*
   *  Next do symbolic linking for nnnnnn.msr -> nnnnnn.msr_xx, 
   *  but never write the link over a real file. 
   *  darc_unlink returns 1 for regular file found
   */
  if( darc_unlink (p_odb_data->run_number, p_odb_data->save_dir) != 1)
    {
      if(darc_set_symlink(p_odb_data->run_number, p_odb_data->save_dir,outFile)==SUCCESS)
        {
          if (debug_check) 
            printf("MBD_write_next: symbolic link successfully set up\n");
        }
      else 
        {
          printf("MBD_write_next: Info - couldn't set up symbolic link\n");
        }
    }
  else
    {
      printf("MBD_write_next: Info darc_unlink found final data file (not link)\n");
      cm_msg(MERROR,"MBD_write_next","final run file already exists (%s/%06d.msr)",
             p_odb_data->save_dir,p_odb_data->run_number);
      return ;
    }
  
  /*
   *      Purge the old nnnnnn.msr_vxx files
   */
  if(debug_check) {
    printf("Calling darc_check_files with keep=%d\n",p_odb_data->purge_after); 
  }
  nfile = darc_check_files(p_odb_data, p_odb_data->purge_after, &next_version);
  if(debug_check) 
    {
      printf("MBD_write_next: darc_check_files returns nfile=%d, next_version=%d\n",
             nfile,next_version);
    }
  if(next_version == 0)
    {
      if(debug)printf("MBD_write_next: next_version from darc_check_files is zero (error)\n");
      cm_msg(MINFO,"MBD_write_next","WARNING - Number of saved files found on disk: %d",nfile);
      cm_msg(MINFO,"MBD_write_next","Can't determine next version for saved file. Cannot purge histograms");
      /* not a fatal error but there may be a problem next time */
    }

  return;
}


 /*****************************************************************************\
 *                                                                             *
 *  MBD_mud_write -- write out the mud-TI file for a bnmr run.                *
 *		We have the header information in the midas darc-event         *
 *              structure, and the data, including camp, in a big array.       * 
 *                                                                             *
 \*****************************************************************************/


static int MBD_runDesc( MUD_SEC_TRI_TI_RUN_DESC* pMUD_desc );
static int MBD_hists( MUD_SEC_GRP* pMUD_histGrp, MUD_SEC_GRP* pMUD_indVarGrp );

static char* getHistName( int i );

/*
 *  Declared in mud_util.h:
 *  
 */
/*char* trimBlanks( char* inStr, char* outStr );
  char* skipBlanks( char* ptr );

   comment out strndup for later Midas  */
/* char* strndup( char *str, int max_len );
 */

#define M2M_hdr_str(to,from,len) \
    strncpy( tempString, from, len ); \
    tempString[len] = '\0'; \
    trimBlanks( tempString, tempString ); \
    to = strdup( tempString )

int
MBD_mud_write( int icmd, char outfile[] )
{
    FILE* fout;
    MUD_SEC_GRP* pMUD_fileGrp;
    MUD_SEC_TRI_TI_RUN_DESC* pMUD_desc;
    MUD_SEC_GRP* pMUD_histGrp;
    MUD_SEC_GRP* pMUD_indVarGrp;

    pMUD_fileGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_FMT_TRI_TI_ID );

    /*
     *  Convert the run description
     */
    pMUD_desc = (MUD_SEC_TRI_TI_RUN_DESC*)MUD_new( MUD_SEC_TRI_TI_RUN_DESC_ID, 1 );

    MBD_runDesc( pMUD_desc );

    /*
     *  Convert the histograms and independent variables
     */
    pMUD_histGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_GRP_TRI_TI_HIST_ID );
    if(  (numCamp + numEpics) > 0 )
      pMUD_indVarGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_GRP_GEN_IND_VAR_ARR_ID );
    else
      pMUD_indVarGrp = NULL;

    MBD_hists( pMUD_histGrp, pMUD_indVarGrp );

    /*
     *  Assemble the first level sections
     */
    MUD_addToGroup( pMUD_fileGrp, pMUD_desc );
    MUD_addToGroup( pMUD_fileGrp, pMUD_histGrp );
    if( (numCamp + numEpics) > 0 )
      MUD_addToGroup( pMUD_fileGrp, pMUD_indVarGrp );

    /*
     *  Do the write
     */
 
    fout = MUD_openOutput( outfile );
    if( fout == NULL ) return( 1 );

    MUD_writeFile( fout, pMUD_fileGrp );

    fclose( fout );

    /*
     *  Free malloc'ed mem
     */
    MUD_free( pMUD_fileGrp );

    return( 0 );
}


static int
MBD_runDesc( MUD_SEC_TRI_TI_RUN_DESC* pMUD_desc )
{
    char tempString[256];
    int  i;

    pMUD_desc->runNumber = mid_hdr.run_number;

    pMUD_desc->exptNumber = mid_hdr.experiment_number;

    M2M_hdr_str(pMUD_desc->experimenter,mid_hdr.experimenter,32);

    pMUD_desc->timeBegin = mid_hdr.start_time_binary;
    pMUD_desc->timeEnd = mid_hdr.stop_time_binary;
    if ( pMUD_desc->timeEnd == 0 )
      { /* use "lastsaved" global set above for the time "now" */
	pMUD_desc->elapsedSec = lastSaved - pMUD_desc->timeBegin ; 
      }
    else
      {
	pMUD_desc->elapsedSec = pMUD_desc->timeEnd - pMUD_desc->timeBegin;
      }

    M2M_hdr_str(pMUD_desc->title,mid_hdr.run_title, 127);

    pMUD_desc->lab = strdup( "TRIUMF" );
    pMUD_desc->method = strdup( "TI-bNMR" );
    pMUD_desc->das = strdup( "MIDAS" );
    pMUD_desc->area = strdup( beamline );

    M2M_hdr_str(pMUD_desc->apparatus,mid_hdr.rig, 32);
    M2M_hdr_str(pMUD_desc->insert,mid_hdr.mode, 32);

    pMUD_desc->subtitle = strdup( "none" );

    sprintf( tempString, "Temp = %s, Field = %s", mid_hdr.temperature, mid_hdr.field );
    tempString[127] = '\0'; /* truncate for comment */
    pMUD_desc->comment1 = strdup( tempString );

    if ( nBinRanges<=1 && bin0[0]<=1 && bin1[0]>=mid_hdr.his_nbin ) {
      sprintf( tempString, "Using all %d time bins", mid_hdr.his_nbin );
    }
    else
    {
      strcpy( tempString, "Using time bins ");
      for( i=0 ; i<nBinRanges ; i++ )
      {
	if ( bin0[i]<=1 && bin1[i]>=mid_hdr.his_nbin )
	  strcat (tempString, "all ");
        sprintf( &tempString[strlen(tempString)], "%d to %d", bin0[i], bin1[i] );
	if ( btag[i][0] )
	  sprintf( &tempString[strlen(tempString)], " (%s)", btag[i] );
	if ( i+1<nBinRanges )
	  strcat (tempString, ", ");
      }
    }
    pMUD_desc->comment2 = strdup( tempString );

    pMUD_desc->comment3 = strdup( "no comment" );

    M2M_hdr_str(pMUD_desc->sample,mid_hdr.sample, 15);
    M2M_hdr_str(pMUD_desc->orient,mid_hdr.orientation, 15);

    return( 1 );
}


/********************************************************************\
 *
 *  Get array names.
 *
\********************************************************************/
static char* getHistName( int ihn )
/********************************************************************\
  Routine: getHistName
  Purpose: Generate names for scaler arrays
  Input:   int ihn : histogram mapping number.
  Globals: scanflg, numScalArr, useBits, bNQRflag
  Output:  none
  Return:  pointer to name string (or null)
\********************************************************************/
{
  static char* itypes[] = { "Frequency", "Rb Cell mV set", "Laser", "Magnet mA", "Dummy",
			    "Frequency", "Field G", "Field G"};
  static int  ivalues[] = { 0, 1, 2, 3, 4, 256, 512, 1024 };
  static char* stypesNMR[] = { "B+","F+","B-","F-" };
  static char* stypesNQR[] = { "L+","R+","L-","R-" };
  static char* utypes[] = { "_frq0","_frq180","_ref0","_ref180" };
  static char* mtypes[] = { "Const","FluM2","NBMB+","NBMF+","NBMB-","NBMF-" };
  static char* tranges[] = { "","_2","_3","_4" };
  static char name[32]; 
  int len, i, j;

  char ** stypes;

  if( bNQRflag ) 
    stypes = stypesNQR;
  else 
    stypes = stypesNMR;

  i = ihn;

  /* 0: independent variable */
  if( i == 0 ) 
  {
      for( j=0; j<7; j++)
      {
          if ( ivalues[j] == scanflg ) return (itypes[j]);
      }
      return (itypes[0]);
  }
 
  /* 1 to numScalArr: scaler, perhaps with user bits or time-range */
  if( i > 0 && i <= numScalArr ) 
  {
    if( nBinRanges > 1 )
      strcpy( name, btag[(i-1)/numHistArr] );
    else
      strcpy( name, "");

    j = (i-1)%numHistArr;
    if( useBits )
    {
      strcpy( name, stypes[j%4] );
      strcat( name, utypes[j/4] );
    }
    else
    {
      strcat( name, stypes[j%4] );
    }

    return( name );
  }
  /* > numScalArr: monitor count / auxilliary scaler */
  i -= numScalArr+1 ;
  if( i >= 0 && i < 6 ) return( mtypes[i] );

  cm_msg(MERROR,"getHistName","invalid histogram number %d.",ihn);
  printf("Error: getHistName:  invalid histogram number %d.\n",ihn);
  return( "????" );

}

static int
MBD_hists( MUD_SEC_GRP* pMUD_histGrp, MUD_SEC_GRP* pMUD_indVarGrp )
{
    int h, d, i, j;
    MUD_SEC_GEN_HIST_HDR* pMUD_histHdr;
    MUD_SEC_GEN_HIST_DAT* pMUD_histDat;
    MUD_SEC_GEN_IND_VAR* pMUD_indVarHdr;
    MUD_SEC_GEN_ARRAY* pMUD_indVarDat;
    int nScalers;
    int indVarOffset;
    int npoints;
    int hsum;
    double val, sum, sum2, sum3, mean, stddev;
    int num;

    int numPPG = 0; /* local, unlike numCamp and numEpics */

    HNDLE hKeyPPG, hKeyPar;
    KEY key;
    char exp_type[32];
    char ppgpath[48];

    int status, size;

    INT ivalue;
    WORD wvalue;
    DWORD uvalue;
    BOOL bvalue;
    float fvalue;
    BOOL got;
    char svalue[255], ppgparpath[128];

    nScalers = 1 + numScalArr + numMonArr ; /* including DAC/FRQ */
    npoints = numPoint;

    //printf("MBD_hists nScalers=%d, \n", nScalers);
    for( h = 0; h < nScalers; h++ )
    {

	pMUD_histHdr = (MUD_SEC_GEN_HIST_HDR*)MUD_new( MUD_SEC_GEN_HIST_HDR_ID, h+1 );
	pMUD_histDat = (MUD_SEC_GEN_HIST_DAT*)MUD_new( MUD_SEC_GEN_HIST_DAT_ID, h+1 );
	//printf("hist %d -> %p\n",  h+1, pMUD_histDat);

 	/*
 	 *  Allocate space for the array (4 bytes/bin, no packing)
	 */
	pMUD_histDat->pData = (caddr_t)zalloc( 4*( npoints ) );
	hsum = 0;

	for( d = 0; d < npoints; d++ )
	{
            /* 
             *  No need to decode/encode because MUD_writeFile encodes everything.
             */
  	    hsum += tots[d*numAllArr+h];
            bcopy( &tots[d*numAllArr+h], &((UINT32*)pMUD_histDat->pData)[d], 4 );
	}

	//printf("Put %d points, total %d, in hist %d\n",  npoints, hsum, h+1);
        pMUD_histHdr->histType = MUD_SEC_TRI_TI_HIST_ID;
        pMUD_histHdr->nBins = npoints;
        pMUD_histHdr->nEvents = hsum;
        pMUD_histHdr->bytesPerBin = 4;
        pMUD_histHdr->nBytes = pMUD_histHdr->bytesPerBin * pMUD_histHdr->nBins;
        pMUD_histHdr->title = strdup( getHistName( h ) );

        /* 
         * Record ns/bin for bNMR runs. 
         * Although name claims fs/bin; dwell time is really in ms  
         */
        pMUD_histHdr->fsPerBin = (int)( mid_hdr.his_nbin * mid_hdr.dwell_time * 1000000.0 + 0.499 );

        /* For F/B histograms, record time-bin range used */
        if( h > 0 && h <= numScalArr )
        {
            pMUD_histHdr->goodBin1 = bin0[(h-1)/numHistArr];
            pMUD_histHdr->goodBin2 = bin1[(h-1)/numHistArr];
        }

        pMUD_histDat->nBytes = pMUD_histHdr->nBytes;

	MUD_addToGroup( pMUD_histGrp, pMUD_histHdr );
	MUD_addToGroup( pMUD_histGrp, pMUD_histDat );
    }

    /*
     *  Now do the independent variables
     */

    //printf("now working on independent variables, numScalArr=%d numMonArr=%d\n",
    //	   numScalArr,numMonArr);

    indVarOffset = 1 + numScalArr + numMonArr;

    for( i = 0; i < numCamp; i++ )
    {
    	h = i + indVarOffset;
  
	//printf ("h=%d npoints=%d\n",h,npoints);
	pMUD_indVarHdr = (MUD_SEC_GEN_IND_VAR*)MUD_new( MUD_SEC_GEN_IND_VAR_ID, i+1 );
	pMUD_indVarDat = (MUD_SEC_GEN_ARRAY*)MUD_new( MUD_SEC_GEN_ARRAY_ID, i+1 );

 	/*
 	 *  Allocate space for the array (4 bytes/bin, no packing)
	 */
	pMUD_indVarDat->pData = (caddr_t)zalloc( 4*( npoints ) );

	for( d = 0; d < npoints; d++ )
	{
            /*
             *  No need to decode/encode because MUD_writeFile encodes everything.
             *  In fact, if we encoded here, we would get garbage due to double-encoding.
             */
            bcopy( &tots[d*numAllArr+h], &((UINT32*)pMUD_indVarDat->pData)[d], 4 );
	}

	pMUD_indVarDat->num = npoints;
	pMUD_indVarDat->elemSize = 4;   /* 4 bytes/bin */
	pMUD_indVarDat->type = 2;       /* 2=real */
	pMUD_indVarDat->hasTime = 0;    /* no time data */
	pMUD_indVarDat->pTime = NULL;
        pMUD_indVarDat->nBytes = pMUD_indVarDat->num*pMUD_indVarDat->elemSize;

        pMUD_indVarHdr->name = strndup( campVars[i].path, campVars[i].len_path );
        pMUD_indVarHdr->description = strndup( campVars[i].title, (int)campVars[i].len_title );
        pMUD_indVarHdr->units = strndup( campVars[i].units, campVars[i].len_units );

	/*
         *  Calculate statistics
 	 */
	num = pMUD_indVarDat->num;
	if( num > 0 )
        {
	  sum = sum2 = sum3 = 0.0;
	  pMUD_indVarHdr->low = pMUD_indVarHdr->high = 
                                    ((REAL32*)pMUD_indVarDat->pData)[0];
	  for( j = 0; j < num; j++ )
	  {
	    val = (double)((REAL32*)pMUD_indVarDat->pData)[j];
	    pMUD_indVarHdr->low = _min( pMUD_indVarHdr->low, val );
	    pMUD_indVarHdr->high = _max( pMUD_indVarHdr->high, val );
	    sum += val;
	    sum2 += val*val;
            sum3 += val*val*val;
	  }
	  mean = pMUD_indVarHdr->mean = sum/num;
	  stddev = pMUD_indVarHdr->stddev = ( num == 1 ) ? 0.0 :
                     sqrt( fabs( ( sum2 - sum*sum/num )/( num - 1 ) ) );
	  pMUD_indVarHdr->skewness = ( stddev == 0.0 ) ? 0.0 :
              ( sum3 - 3.0*mean*sum2 + 
                 2.0*( sum*sum*sum )/( ((double)num)*((double)num) ) ) /
              ( ((double)num)*(stddev*stddev*stddev) );

        }

	MUD_addToGroup( pMUD_indVarGrp, pMUD_indVarHdr );
	MUD_addToGroup( pMUD_indVarGrp, pMUD_indVarDat );
    } 

    //================== EPICS ==============
    indVarOffset = 1 + numScalArr + numMonArr + numCamp;

    for( i = 0; i < numEpics; i++ )
    {
      h = i + indVarOffset;

      pMUD_indVarHdr = (MUD_SEC_GEN_IND_VAR*)MUD_new( MUD_SEC_GEN_IND_VAR_ID, i+numCamp+1 );
      pMUD_indVarDat = (MUD_SEC_GEN_ARRAY*)MUD_new( MUD_SEC_GEN_ARRAY_ID, i+numCamp+1 );

      /*
       *  Allocate space for the array (4 bytes/bin, no packing)
       */
      pMUD_indVarDat->pData = (caddr_t)zalloc( 4*( npoints ) );

      for( d = 0; d < npoints; d++ )
	{
	  /*
	   *  No need to decode/encode because MUD_writeFile encodes everything.
	   *  In fact, if we encoded here, we would get garbage due to double-encoding.
	   */
	  bcopy( &tots[d*numAllArr+h], &((UINT32*)pMUD_indVarDat->pData)[d], 4 );
	}

	pMUD_indVarDat->num = npoints;
	pMUD_indVarDat->elemSize = 4;   /* 4 bytes/bin */
	pMUD_indVarDat->type = 2;       /* 2=real */
	pMUD_indVarDat->hasTime = 0;    /* no time data */
	pMUD_indVarDat->pTime = NULL;
        pMUD_indVarDat->nBytes = pMUD_indVarDat->num*pMUD_indVarDat->elemSize;
	
        pMUD_indVarHdr->name = strndup( epicsVars[i].path, epicsVars[i].len_path );
        pMUD_indVarHdr->description = strndup( epicsVars[i].title, epicsVars[i].len_title );
        pMUD_indVarHdr->units = strndup( epicsVars[i].units, epicsVars[i].len_units );

	/*
         *  Calculate statistics
 	 */
	num = pMUD_indVarDat->num;
	if( num > 0 )
	  {
	  sum = sum2 = sum3 = 0.0;
	  pMUD_indVarHdr->low = pMUD_indVarHdr->high = 
                                    ((REAL32*)pMUD_indVarDat->pData)[0];
	  for( j = 0; j < num; j++ )
	  {
	    val = (double)((REAL32*)pMUD_indVarDat->pData)[j];
	    pMUD_indVarHdr->low = _min( pMUD_indVarHdr->low, val );
	    pMUD_indVarHdr->high = _max( pMUD_indVarHdr->high, val );
	    sum += val;
	    sum2 += val*val;
            sum3 += val*val*val;
	  }
	  mean = pMUD_indVarHdr->mean = sum/num;
	  stddev = pMUD_indVarHdr->stddev = ( num == 1 ) ? 0.0 :
                     sqrt( fabs( ( sum2 - sum*sum/num )/( num - 1 ) ) );
	  pMUD_indVarHdr->skewness = ( stddev == 0.0 ) ? 0.0 :
              ( sum3 - 3.0*mean*sum2 + 
                 2.0*( sum*sum*sum )/( ((double)num)*((double)num) ) ) /
              ( ((double)num)*(stddev*stddev*stddev) );

        }

	MUD_addToGroup( pMUD_indVarGrp, pMUD_indVarHdr );
	MUD_addToGroup( pMUD_indVarGrp, pMUD_indVarDat );
    }

    //=========================== PPG ==============
    /*
     * Log the PPG parameters -- the ODB parameters listed (as links) in /PPG/PPGxx
     * for experiment type "xx".  Save them as independent variables.  For numeric
     * parameters, save the number as the "mean".  For string parameters, save the
     * value as the "units".  For Booleans, save the number (0 or 1) as the mean,
     * and the words "no" or "yes" as the units.  Record the PPG path as the "name"
     * (which usually contains the Camp path) and the linked-to variable name as the
     * "description".  An array of values is expected after the summary ("header")
     * so include a stub with zero points. TODO - this should be handled by a revised
     * DARC_log_ppg_par( )
     */

    size = sizeof(exp_type);
    status = db_get_value(hDB, 0, "Equipment/FIFO_acq/frontend/Input/Experiment name",
                          exp_type, &size, TID_STRING, FALSE);

    sprintf(ppgpath, "PPG/PPG%s", exp_type);
    /* printf("Look for parameters at PPG/PPG%s\n", exp_type);*/

    /* printf("Var types are KEY %d, LINK %d, STRING %d, INT %d\n",
           TID_KEY,TID_LINK,TID_STRING,TID_INT); */

    status = db_find_key(hDB, 0, ppgpath, &hKeyPPG);

    if( status != DB_SUCCESS )
      {
        printf( "ERROR: Could not read PPG parameters %s\n",ppgpath) ;
        cm_msg(MERROR,"MBD_hists","Could not read PPG parameters %s\n",ppgpath) ;
        return ( 0 );
      }


    for (i=0 ; ; i++)
      {
        status = db_enum_link(hDB, hKeyPPG, i, &hKeyPar);
        if (status == DB_NO_MORE_SUBKEYS) break;
        if (status != DB_SUCCESS ) continue;

        status = db_get_key(hDB, hKeyPar, &key);
        if (status != DB_SUCCESS ) continue;
        sprintf(ppgparpath, "%s/%s", ppgpath, key.name );
        /* printf("\n%d Got key \"%s\" type %d\n", i, key.name, key.type);*/

        status = db_enum_key(hDB, hKeyPPG, i, &hKeyPar);
        if (status == DB_NO_MORE_SUBKEYS) break;
        if (status != DB_SUCCESS ) continue;

        status = db_get_key(hDB, hKeyPar, &key);
        if (status != DB_SUCCESS ) continue;
        /* printf("%d Got key \"%s\" type %d, %d values\n",
           i, key.name, key.type, key.num_values);*/

        if ( key.num_values == 1 ) /* ignore any arrays (COULD HANDLE THEM HERE!) */
          {
            svalue[0] = '\0';
            val = 0.0;
            got = TRUE;

            switch ( key.type )
              {
              case TID_INT:
		size=sizeof(ivalue);
                status = db_get_data( hDB, hKeyPar, &ivalue, &size, key.type );
                /* printf("Integer value %d\n", ivalue); */
                val = (double) ivalue;
                break;
              case TID_BOOL:
		size=sizeof(bvalue);
                status = db_get_data( hDB, hKeyPar, &bvalue, &size, key.type );
                /* printf("Boolean value %d\n", bvalue); */
                val = (double) bvalue;
                strcpy( svalue, (bvalue? "yes" : "no") );
                break;
              case TID_WORD:
		size=sizeof(wvalue);
                status = db_get_data( hDB, hKeyPar, &wvalue, &size, key.type );
                /* printf("Word value %d\n", wvalue); */
                val = (double) wvalue;
                break;
              case TID_DWORD:
		size=sizeof(uvalue);
                status = db_get_data( hDB, hKeyPar, &uvalue, &size, key.type );
                /* printf("Dword value %d\n", uvalue); */
                val = (double) uvalue;
                break;
              case TID_FLOAT:
		size=sizeof(fvalue);
                status = db_get_data( hDB, hKeyPar, &fvalue, &size, key.type );
                /* printf("Float value %f\n", fvalue); */
                val = (double) fvalue;
                break;
              case TID_DOUBLE:
		size=sizeof(val);
                status = db_get_data( hDB, hKeyPar, &val, &size, key.type );
                /* printf("Double value %lf\n", val); */
                break;
              case TID_STRING:
		size=sizeof(svalue);
                status = db_get_data( hDB, hKeyPar, &svalue, &size, key.type );
		/*printf("String value \"%s\", size=%d\n", svalue,size); */
                break;
              default:
                got = FALSE;
                break;
              }
	    if(status != DB_SUCCESS)
	      printf("MBD_hists: error after db_get_data for key type=%d (%d)\n",key.type,status);

            if( got )
              {
                numPPG++;
                pMUD_indVarHdr = (MUD_SEC_GEN_IND_VAR*)MUD_new( MUD_SEC_GEN_IND_VAR_ID, numCamp+numEpics+numPPG );
                pMUD_indVarDat = (MUD_SEC_GEN_ARRAY*)MUD_new( MUD_SEC_GEN_ARRAY_ID, numCamp+numEpics+numPPG );

                pMUD_indVarHdr->mean = val;
                pMUD_indVarHdr->name = strdup( ppgparpath );
                pMUD_indVarHdr->description = strdup( key.name );
                pMUD_indVarHdr->units = strdup( svalue );
                pMUD_indVarHdr->low = 0.0;
                pMUD_indVarHdr->high = 0.0;
                pMUD_indVarHdr->stddev = 0.0;
                pMUD_indVarHdr->skewness = 0.0;

                /* Stub of data array with no points. Could include one value, or use it for odb arrays. */
                pMUD_indVarDat->pData = NULL;
                pMUD_indVarDat->num = 0;
                pMUD_indVarDat->elemSize = 4;   /* 4 bytes/bin */
                pMUD_indVarDat->type = 2;       /* 2=real */
                pMUD_indVarDat->hasTime = 0;    /* no time data */
                pMUD_indVarDat->pTime = NULL;
                pMUD_indVarDat->nBytes = 0;

                MUD_addToGroup( pMUD_indVarGrp, pMUD_indVarHdr );
                MUD_addToGroup( pMUD_indVarGrp, pMUD_indVarDat );
              }
          }
      }


    return( 1 );
}


/*--------------MBD_get_dynamic_headers -------------------*/
int
MBD_get_dynamic_headers( D_ODB *p_odb_data )
{
/*-----------------------------------------------------------*/
  /* simplified duplicate of
   * DARC_get_dynamic_headers: fill the temperature and/or field run header
   * (p_odb_data->) with the average value from a Camp variable.  All failures
   * are silently ignored -- always return SUCCESS status.
   *
   * Don't yet know what to do with an automatic RF Power variable when there
   * is no Power header in the MUD file format.
   *
   *
   */

  CAMP_VAR* pVar;
  CAMP_NUMERIC* pNum;

  MUD_SEC_GEN_IND_VAR* pMUD_indVar;

  int status;
  double val, stddev, skewness;
  char units[16], str[32];
  char *pUnits;
  int n;
  BOOL eflag = TRUE;

  if(debug) printf("Get dynamic headers, where field_variable %s and temperature_variable %s\n", 
	 p_odb_data->field_variable, p_odb_data->temperature_variable);
  if( p_odb_data->field_variable[0] == '/' || p_odb_data->temperature_variable[0] == '/' )
  {

    DARC_camp_connect( p_odb_data->camp_host );

    if( campInit )
    {
      if( ! campUpdated ) 
      {
        status = camp_clntUpdate();  
        if( status == CAMP_SUCCESS )
          campUpdated = TRUE ;
	else
	  cm_msg(MERROR,"MBD_get_dynamic_headers","failed call to update CAMP data (%d)",status );
      }

      /****** Automatic Field header ******/

      stddev = -2.0;
      pUnits = NULL;

      if( campUpdated && p_odb_data->field_variable[0] == '/' )
      {
        //if(debug) printf("Field variable '%s' - get it from Camp\n", p_odb_data->field_variable);
        status = campSrv_varGet( p_odb_data->field_variable, CAMP_XDR_NO_CHILD );
        if( status != CAMP_SUCCESS )
        {
  	    printf("MBD_get_dynamic_headers: error status %d getting Camp var '%s'. %s\n", 
		 status, p_odb_data->field_variable, camp_getMsg() );
            cm_msg(MERROR,"MBD_get_dynamic_headers","Camp variable '%s' does not exist", 
                   p_odb_data->field_variable );
        }
        else
        {
            pVar = camp_varGetp( p_odb_data->field_variable );
            if( (pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK) != CAMP_VAR_TYPE_NUMERIC )
            {
              cm_msg(MERROR,"MBD_get_dynamic_headers","Camp variable %s is not numeric", 
                     p_odb_data->field_variable );
            }
            else
            {
              pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;
              pUnits = pNum->units;
              if( pNum->num < 2 )
              { /* not enough readings for an error bar, or a range */
                val = pNum->val;
                stddev = 0.0;
              }
              else
              { /* We want to calculate statistics, or we have a scan */
		if ( scanflg == 3 || scanflg == 512 || scanflg == 1024 )
		{ /* This is a field scan, report a range */
		  if( pUnits )
		    strncpy( units, pUnits, 8 );
		  else
		    strcpy( units, "");
		  snprintf( str, 32, "%.1f-%.1f%.7s", pNum->low, pNum->hi, units );
		  str[31]='\0';
		  if(debug) printf("Field scan so report range %s\n", str);
		  n = sizeof(p_odb_data->field) - 1 ;
		  strncpy( p_odb_data->field, str, n );
		  p_odb_data->field[n] = '\0';
		}
		else
		{ /* Not a scan, so calc stats */
		  camp_varNumCalcStats( pVar->core.path, &val, &stddev, &skewness );
		}
              }
            }
        }
        //if(debug) printf("Field value %f (%f)\n", val, stddev);
        /* Got field value.  Now do units conversion.
         * (see also title_validate in td_extra.tcl)
         */
        if( stddev > -1.0 )
        {
          strcpy( units, "G");
          if( pUnits )
          {
            strncpy( units, pUnits, 4 );
            units[0] = toupper( units[0] );
            units[1] = toupper( units[1] );
            units[3] = '\0';
            if( units[0] == 'G' || units[0] == 'O' )
            {
              strcpy( units, "G");
            }
            else if( units[0] == 'T' )
            {
              val = val*10000. ;
              stddev = stddev*10000. ;
              strcpy( units, "G");
            }
            else if( units[0] == 'M' && units[1] == 'T' )
            {
              val = val*10.0 ;
              stddev = stddev*10. ;
              strcpy( units, "G");
            }
            else if( units[0] == 'K' && ( units[1] == 'G' || units[1] == 'O' ) )
            {
              val = val*1000.0 ;
              stddev = stddev*1000. ;
              strcpy( units, "G");
            }
            else if( units[0] == 'M' && ( units[1] == 'G' || units[1] == 'O' ) )
            {
              val = val/1000.0 ;
              stddev = stddev/1000. ;
              strcpy( units, "G");
            }
            else if( isalpha(*pUnits) )
	    {
              strncpy( units, pUnits, 15 );
              units[15] = '\0';
            }
            else 
            {
              strcpy( units, "G" );
            }
          }

          eflag = fmdarc.camp.record_field_error;
          n = sizeof(p_odb_data->field) - 1 ;

          DARC_head_num( str, n, val, stddev, units, eflag ) ;
	  if(debug)printf("Record field %s\n", str);
          strncpy( p_odb_data->field, str, n ); /* Note: this does not propagate back to the ODB */
          p_odb_data->field[n] = '\0';
        }
      }  /* finished field variable */

      /****** Automatic Temperature header ******/

      stddev = -2.0;
      pUnits = NULL;

      if( campUpdated && p_odb_data->temperature_variable[0] == '/' )
      {
        //printf("Automatic temperature\n");
        if(debug) printf("Temperature variable '%s' - get it from Camp\n", p_odb_data->temperature_variable);
        status = campSrv_varGet( p_odb_data->temperature_variable, CAMP_XDR_NO_CHILD );
        if( status != CAMP_SUCCESS )
          {
  	    printf("MBD_get_dynamic_headers: error status %d getting Camp var '%s'.\n", 
		   status, p_odb_data->temperature_variable );
            cm_msg(MERROR,"MBD_get_dynamic_headers","Camp variable '%s' does not exist", 
                   p_odb_data->temperature_variable );
          }
        else
          {
            pVar = camp_varGetp( p_odb_data->temperature_variable );
            if( (pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK) != CAMP_VAR_TYPE_NUMERIC )
            {
              cm_msg(MERROR,"MBD_get_dynamic_headers","Camp variable %s is not numeric", 
                     p_odb_data->temperature_variable );
            }
            else
            {
              pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;
              pUnits = pNum->units;
              if( pNum->num < 2 )
              { /* not enough readings for an error bar */
                val = pNum->val;
                stddev = 0.0;
              }
              else
              {
                camp_varNumCalcStats( pVar->core.path, &val, &stddev, &skewness );
              }
            }
          }
        //if(debug) printf("Temperature value %f (%f)\n", val, stddev);
        /* 
         * Got temperature value.  Now do units conversion.
         */
        if( stddev > -1.0 )
        {
          strcpy( units, "K");
          if( pUnits )
          {
            strncpy( units, pUnits, 4 );
            units[0] = toupper( units[0] );
            units[1] = toupper( units[1] );
            units[3] = '\0';
            if( units[0] == 'C' )
            {
              val = val+273.15 ;
              strcpy( units, "K");
            }
            else if( units[0] == 'M' && units[1] == 'K' )
            {
              val = val/1000.0 ;
              stddev = stddev/1000.0 ;
              strcpy( units, "K");
            }
            else if( isalpha(*pUnits) )
            {
              strncpy( units, pUnits, 15 );
              units[15] = '\0';
            }
            else
            {
              strcpy( units, "K" );
            }
          }

          eflag = fmdarc.camp.record_temperature_error;
          n = sizeof(p_odb_data->temperature) - 1;

          DARC_head_num( str, n, val, stddev, units, eflag ) ;
	  if(debug)printf("Record temperature %s\n", str);
          strncpy( p_odb_data->temperature, str, n ); /* Note: this does not propagate back to the ODB */
          p_odb_data->temperature[n] = '\0';
        }
      }  /* finished temperature variable */

    }
  }
  return( SUCCESS );
}


/* 
 *  Write the histogram totals into the ODB.
 *  We already accumulated in a separate array of doubles.
 *  Here we copy them to the ODB array of floats.
 *  That is insufficient to put them in the ODB, so do so explicitly.
 */
static void MBD_apply_htots( D_ODB *p_odb )
{
  int i;
  double grand;
  HNDLE hKeyHT, hHout;
  KEY key;
  INT status;


  grand = 0.0;
  for (i=0; i<MAX_HIS; i++)
    {
      p_odb->his_total[i] = htots[i];
      grand += htots[i];
    }
  // p_odb->total_save = (float)grand;
   p_odb->total_save = (double)grand;
  
  /* Can I search starting from hMDarc ? Yes */
  status = db_find_key(hDB, hMDarc, "histograms/output/histogram totals", &hKeyHT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"midbnmr_darc","cannot find key for histogram totals in ODB (%d)",status);
      return;
    }
  
  status = db_get_key(hDB, hKeyHT, &key);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"midbnmr_darc","cannot get output/histogram totals key (%d)",status);
      return;
    }
  
  
  printf("Number of values in output/histogram totals array is %d\n",key.num_values);
  if(MAX_HIS > key.num_values)
    { 
      cm_msg(MERROR,"midbnmr_darc"," output/histogram totals array size in odb (%d) is less than MAX_HIS (%d). Cannot write histogram totals",
	     key.num_values,MAX_HIS);
      return;
    }

 

  for (i=0;  i < MAX_HIS; i++)
    fmdarc.histograms.output.histogram_totals[i]=  htots[i];   
  fmdarc.histograms.output.total_saved=grand;

  status = db_find_key(hDB, hMDarc, "histograms/output", &hHout);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"midbnmr_darc","cannot find key for histograms/output in ODB (%d)",status);
      return;
    }

  /* Update Histogram settings in ODB */
  size = sizeof(fmdarc.histograms.output);
  status = db_set_record(hDB, hHout, &fmdarc.histograms.output, size, 0);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"midbnmr_darc","Failed to save mdarc/histograms/output record (size=%d) (%d)",size,status);  
  //else
  // cm_msg(MINFO,"midbnmr_darc","Success saving mdarc/histograms/output record (size=%d) (%d)",size,status);  


  return;
}

#ifdef GONE     // NOT NEEDED
/* 
 *  skipBlanks() - return a pointer to the first non-blank in the string
 */
char*
skipBlanks( ptr )
    char* ptr;
{
    while( ( *ptr != '\0' ) && !isgraph( *ptr ) ) 
    {
    	ptr++;
    }
    return( ptr );
}


/* 
 *  trimBlanks() - return a string with no leading or trailing blanks
 */


char*
trimBlanks( inStr, outStr )
    char* inStr;
    char* outStr;
{
    char* p1;
    char* p2;
    int len;

    p1 = skipBlanks( inStr );
    if( *p1 == '\0' )
    {
    	outStr[0] = '\0';
    	return( outStr );
    }

    for( p2 = &inStr[strlen(inStr)-1]; ( p2 > p1 ) && !isgraph( *p2 ); p2-- );
    len = p2 - p1 + 1;
    strncpy( outStr, p1, len );
    outStr[len] = '\0';

    return( outStr );
}
#endif

