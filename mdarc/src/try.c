#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>


int main(void)
{
  char str[32];
  

  sprintf(str,"empty");
  if(getenv("HOSTTYPE"))
    strncpy(str, getenv("HOSTTYPE"), sizeof(str));

  printf("str=%s\n",str);
  if(strstr (str,"_64"))
    printf("64 bit machine\n");
  else
     printf("NOT 64 bit machine\n");

  printf("sizeof(long)=%d and sizeof(int)=%d \n",sizeof(long),sizeof(int));
  printf("sizeof(short)=%d and sizeof(int)=%d \n",sizeof(short),sizeof(int));
  printf("sizeof(double)=%d and sizeof(float)=%d \n",sizeof(double),sizeof(float));
  return 1;
}
