/********************************************************************\
  Name:         mdarcBnmr.c
  Created by:   
  Contents:    data logger for MODAS (Musr DAQ) run under MIDAS on linux


 Version used with VMIC FE
	
  $Log: mdarcBnmr.c,v $
  Revision 1.11  2016/12/12 22:54:50  suz
  shut down Epics channels for Type 2 at end of run

  Revision 1.10  2016/09/19 05:58:34  asnd
  Fix null-termination on some strings.

  Revision 1.9  2016/07/05 01:35:39  suz
  add HSxx bank for sample/ref mode

  Revision 1.8  2015/11/27 23:23:13  suz
  changes for latest MIDAS; removed YBOS; added extra check for type 1f with constant time

  Revision 1.7  2015/06/22 23:55:25  suz
  correct perlscript output file path

  Revision 1.6  2014/01/31 21:15:56  suz
  add a message

  Revision 1.5  2014/01/29 21:13:59  suz
  add check on '/custom/hidden/get next runnum' to prevent users starting run while run number check from prev run still going on

  Revision 1.4  2014/01/09 01:51:12  suz
  add bincheck (check for empty/too large bins) in 25MHz test pulse HM00  for special 1f mode with constant time between cycles

  Revision 1.3  2013/04/12 20:40:03  suz
  removed client_flags.c and .h which were preventing compilation

  Revision 1.2  2013/04/08 22:25:53  suz
  Changes after debugging new VMIC code

  Revision 1.1  2013/01/21 21:42:38  suz
  initial VMIC version to cvs

  Revision 1.22  2008/12/10 18:49:13  suz
  change error handling slightly



\*****************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>


/* midas includes */
#include "midas.h"
#include "msystem.h"
#ifdef HAVE_YBOS
#include "ybos.h"
#else
#include "mdsupport.h"  // defines MD_STRING_BANKLIST_MAX
#endif
#include "mdarc.h" /* common header for mdarc & bnmr_darc */
#include "darc_files.h" /* for msg_print only */
#include "experim.h" 
#include "run_numbers.h"
//#include "client_flags.h"  // prototypes
#ifndef MUSR
#include "client_error_codes.h"
#endif
// note caddr_t is defined as char*

#ifdef EPICS_ACCESS
#include "epics_log.h"
#include "mdarc_epics.h" // prototypes of mdarc_epics.c routines
#endif

/* prototypes */
INT    tr_start(INT rn, char *error);
INT    tr_stop(INT rn, char *error);
INT    tr_prestart(INT rn, char *error);
INT    setup_hotlink();
INT    setup_hot_toggle();
void   update_Time(INT , INT , void *);
void  hot_toggle (INT, INT, void * );
INT print_file(char* filename);
INT check_midas_logger(INT rn); // type 1 only
INT auto_run_number_check(INT rn);
INT get_ppg_type(void);
INT check_run_number(INT rn);
//INT check_host_type(void); not used
#ifdef EPICS_ACCESS
void hot_num_epics (INT, INT, void * );
#endif

#ifndef MUSR
#define BINCHECK  // TEMP ? check for empty bins special 1f mode
#endif // BINCHECK BNM/QR only,  not for MUSR

#ifdef BINCHECK
INT check_for_empty_bins(  HNDLE hBuf, HNDLE req, EVENT_HEADER *pheader, void *pevent);
BOOL bincheck_flag=0;
#endif // BINCHECK Type 1f


// this for MDARC stopping the run
HNDLE hCF;
FIFO_ACQ_CLIENT_FLAGS fcf;

char client_str[132];
// end of MDARC stop run section

// globals
//          (globals shared with bnmr_darc.c in mdarc.h)
caddr_t pHistData;
DWORD  time_save, last_time, start_time, stop_time,nhist;
INT    hBufEvent;
INT    request_id, camp_request_id, darc_request_id, epics_request_id ;
INT   status;
BOOL  mdarc_need_update;
DWORD mdarc_last_call;
BOOL gbl_rn_check=FALSE;


HNDLE hMDarc; // handles for mdarc
/*  DA moved to mdarc.h char beamline[6]; */
INT run_state;
BOOL write_data; // odb  mdarc flag enable_mdarc_logging or mlogger /mlogger/write data
BOOL running_already;
FIFO_ACQ_MDARC fmdarc;
HNDLE hFS;
char perl_script[80];  /* get_run_number.pl  Directory now read from odb in setup_hotlink */

/* initialize values (in mdarc.h) */
BOOL   bGotEvent = FALSE;
INT ppg_type; // type 1 (IMUSR) or 2 (TDMUSR)
char ppg_mode[5];

char MData_dir[256]; /* saved directory for midas files; size must agree with odb /logger/data dir */

BOOL tr_flag;
char lc_beamline[6];
/* client code (non-fatal errors) */
BOOL SET=TRUE;
BOOL CLEAR=FALSE;

#ifdef EPICS_ACCESS
HNDLE hEpics,hEpicsNum;
CAMP_SETTINGS camp_settings; // I-MUSR camp equipment includes epics settings
EPICSLOG_SETTINGS epicslog_settings; // I-MUSR camp equipment includes epics settings

#define NUM_EPICS 8 // size of array in odb
EPICS_LOG epics_log[MAX_EPICS];
INT n_epics=0; // number of epics devices to be logged (hotlinked)
BOOL epics_available=FALSE;
#endif

#ifdef BINCHECK   // not MUSR
INT  bin1_25MHz_count;  // needed for 1f empty bin check
INT  bin1_bad_cntr,bin1_good_cntr;
#endif // BINCHECK

//#include "client_flags.c"  // code for client_flags handling

/*----- prestart  ----------------------------------------------*/
INT tr_prestart(INT rn, char *error)
{

  int j;
  char str[128];
#ifndef MUSR
  BOOL flag;
  INT size,status;
#endif
  /* Move the run number checking from tr_start to tr_prestart so we can be sure of stopping the
     run from starting if there is a failure 
      - still can't stop run reliably so implement client flags
*/


  /* Use of cm_exist:

 Purpose: Check if a MIDAS client exists in current experiment

  Input:
    char   name             Client name
    BOOL   bUnique          If true, look for the exact client name.
                            If false, look for namexxx where xxx is
                            a any number

  Output:
    none

  Function value:
    CM_SUCCESS              Client found
    CM_NO_CLIENT            No client with that name


     e.g.   status=cm_exist("mdarc",TRUE);
     Compare status with CM_NO_CLIENT or CM_SUCCESS

     or call with FALSE and "febnmr"  
     e.g.   status = cm_exist("febnmr", FALSE);
     in this case it will return CM_SUCCESS for febnmr, febnmr2, febnmr1

     To check febnmr2 specifically have to specify:
            status = cm_exist("febnmr2", TRUE);

 */
  status=cm_exist("mdarc",FALSE); 
  //printf("status after cm_exist for mdarc = %d (0x%x)\n",status,status);
  if(status == CM_SUCCESS)
  {
      cm_msg(MERROR, "tr_prestart","Another copy of mdarc may be already running");
      return(CM_NO_CLIENT); /* return an error */
  }

  if(rn != 0)
    {   // do on a genuine run start only
      printf("\n\n ===== tr_prestart: run rn=%d is starting  =======\n\n",rn);

#ifndef MUSR
      size=sizeof(flag);
      /* don't let the run number check start if still running from previous run */
      status = db_get_value(hDB, 0, "/custom/hidden/get next runnum", &flag, &size, TID_BOOL, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"tr_prestart","could not get value for \"/custom/hidden/get next runnum\" (%d)",status);
	  return status;
	}
      if(flag)
	{
	  cm_msg(MERROR,"tr_prestart","run number checker is still running from previous run; retry run start later. (%d)",status);
	  return(DB_INVALID_PARAM);
	}
#endif

      start_time =  ss_time(); // set a timer when run was started
    }
  else
    {
      start_time = 0; // dummy value
      if(run_state == STATE_STOPPED)
	{
	  printf("tr_prestart: dummy run start, run is STOPPED\n");
	 
	}
    }

  /* NOTE: Client flags are used for fatal errors (e.g. to stop the run)   REMOVED
           Client error codes are used for non-fatal errors by the custom status page
  */
  //   Clear client error code bitpattern (used by custom_status for error handling) 
  status = clear_client_code("mdarc");
  if(status != SUCCESS)
    return status;


  /* 
     check if we are writing data
  */
  size = sizeof(write_data);
  status = db_get_value(hDB, 0, "/experiment/edit on start/write data", &write_data, &size, TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"tr_prestart","key not found \"/experiment/edit on start/write data\"  (%d)",status);
      return(status);
    }
  
  // BNMR/BNQR since MUSR does not use the midas logger

  // either Type (in fact type 2 causes no problems) 
    if(!write_data)
    {
      if(rn!=0)
	{  /* We're really starting a run  */
	  	  
	  // BNMR/BNQR only ; no real run numbers are < MIN_TEST
	  if(  rn < MIN_TEST || rn > MAX_TEST )   // a real run
	    {
	      /* Do not allow the user to start a REAL run unless they are saving data (to avoid HOLES)
		  - type 2 does not need this restriction, but insist on a test run for both types for consistency 

		  reset the run number to a test value to avoid HOLES ! */
	      cm_msg(MINFO,"tr_prestart",
		     "You must select data logging ON for REAL runs");
	      cm_msg(MERROR,"tr_prestart",
		     "If you are not saving data, RESET run number to a TEST value (i.e. press TEST button) then RESTART run");
	      
	      printf("tr_prestart: returning status = %d\n",DB_INVALID_PARAM);
	      return(DB_INVALID_PARAM);
	    }
	}
	    
    } // end of not writing data


  /* update the whole mdarc record first to reflect any changes */ 
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"tr_prestart","Failed to retrieve mdarc record  (%d)",status);
    write_message1(status,"tr_prestart");
    return(status);
  }

  // get the ppg type (needed by check_midas_logger and others)
  status=get_ppg_type();
  if(status!=DB_SUCCESS)
    {
      cm_msg (MERROR,"tr_prestart","Cannot determine ppg mode");
      return (status);
    }

  status = check_midas_logger(rn);  // both types; make sure midas logger is set up properly
  if (status != DB_SUCCESS)
    return (status);



  if(ppg_type == 2)
    { // type 2
      nHistBins =  fmdarc.histograms.num_bins; //fmdarc set up by setup_hotlink
      nHistBanks =  fmdarc.histograms.number_defined;  
      if(debug)printf("tr_prestart: Number of bins = %d;  no. defined histograms = %d\n",nHistBins,nHistBanks);
    }

#ifdef BINCHECK // not MUSR
  else  if (strncmp(ppg_mode, "1f",2)==0)
    {
      bin1_25MHz_count=bin1_good_cntr=bin1_bad_cntr=bincheck_flag=0; // clear these for 1f empty bin check

      sprintf(str,"/Equipment/%s/Frontend/Input/e1f const time between cycles",eqp_name); 
      size=sizeof(bincheck_flag);
      status = db_get_value(hDB, 0, str, &bincheck_flag, &size, TID_BOOL, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"tr_prestart","could not get value for \"%s\" (%d)",str,status);
	  return;    
	}
      printf("tr_prestart: Mode 1f bincheck_flag=%d\n",bincheck_flag);
    }
#endif // BINCHECK
  
  
  /* look for automatic run number check */
  if (fmdarc.disable_run_number_check)
    {
      status = check_run_number(rn);
      if (status != DB_SUCCESS)
	return (status); // check_run_number wrote an error message
    }
  else    /* automatic run number checking is enabled */
    {
      status = auto_run_number_check(rn);
      if (status != DB_SUCCESS)
	return (status); // auto_run_number_check failed
         
    } // end of automatic run number checking

  /* make sure that suppress_save_temporarily flag has not been left set */
  if (fmdarc.suppress_save_temporarily)
    {
      printf("prestart: clearing suppress_save_temporarily flag\n");
      fmdarc.suppress_save_temporarily=FALSE;
      size = sizeof( fmdarc.suppress_save_temporarily);
      status = db_set_value(hDB, hMDarc, "suppress_save_temporarily", 
			    &fmdarc.suppress_save_temporarily, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "tr_prestart", "cannot set mdarc flag  \"suppress_save_temporarily\" (%d) ",status);
	  return status;
	}   
    } 

  running_already=FALSE;
  if (run_state == STATE_RUNNING && rn == 0 ) // setup_hotlink got run_state
    running_already=TRUE;

  if(ppg_type ==1)
    { /* stop midbnmr_darc being started if type 1 run is going and we are supposed to be writing data 
         because it will have missed the headers */
      
      if(write_data)
	{
	  if (running_already )
	    {
	      cm_msg(MINFO,"tr_prestart",
		     "** Cannot convert MID data to MUD for a Type 1 RUN ALREADY IN PROGRESS..."); 
	      cm_msg(MINFO,"tr_prestart", "** Restart run, or use Midbnmr to convert data after run ends");
	    }
	  else
	    {
	      if(debug_proc)printf("tr_prestart: initializing for Type 1; calling ana_begin_of_run\n");
	      status = ana_begin_of_run();  // initialize type 1
	    }
	}
      else
	printf("tr_prestart: Write_data is false, not writing data\n");
    }

  // now  tr_start is executed (genuine run start)
  if(debug)printf("tr_prestart: returning\n");
  return(status);
}
  
/*----- start  ----------------------------------------------*/
INT tr_start(INT rn, char *error)
{
  char str[128];
  char string[256];
  HNDLE hRM,  hKeyTmp;
  char  cmd[132];
  int j;
  int stat;

  

  /* called every new run and as a dummy (rn=0) when mdarc is started */
  //  if(debug)

  printf("tr_start: starting\n");

  firstxTime = TRUE;  /* set global flag so bnmr_darc opens camp connection on begin run  */
  stop_time = 0; // initialize
  sprintf(archiver,"%s",fmdarc.archiver_task) ;  // archiver is in mdarc.h and passed to darc_files
  printf("tr_start:archiver: %s\n",archiver);
#ifndef MUSR
  // clear client code bits  
  write_client_code(CAMP_LOG_ERR,CLEAR,"mdarc");
  write_client_code(EPICS_LOG_ERR,CLEAR,"mdarc");
#endif

  printf("\ntr_start: mdarc parameters:\n");
  printf("   Time of last save: %s\n",fmdarc.time_of_last_save);
  printf("   Last saved filename: %s\n",fmdarc.last_saved_filename);
  printf("   Saved data directory: %s\n",fmdarc.saved_data_directory);
  printf("   # versions before purge: %d\n",fmdarc.num_versions_before_purge);
  printf("   End of run purge/rename/archive flag: %d\n",fmdarc.endrun_purge_and_archive);
  printf("   Suppress save of data temporarily flag: %d\n",fmdarc.suppress_save_temporarily);
  printf("   Archived data directory: %s\n",fmdarc.archived_data_directory);
  printf("   Last archived file:  %s\n",fmdarc.last_archived_filename);
  printf("   Save interval (sec): %d\n",fmdarc.save_interval_sec_);
  printf("   Archiver is : %s\n",archiver);
  printf("\n");

  if(ppg_type==2)
    { // Type 2
      if(nHistBins <= 0)        /*  use the number of bins found from the actual histograms */
	cm_msg(MINFO, "tr_start", "Invalid number of bins (%d) from mdarc/histograms/num_bins", nHistBins);
      if(nHistBanks <= 0)        /*  use the number of banks found from the actual histograms */
	cm_msg(MINFO, "tr_start", "Invalid number of histograms (%d) from mdarc/histograms/number_defined", nHistBanks);
      
      
      if(rn == 0)
	cm_msg(MINFO,"tr_start","Making a DUMMY call to bnmr_darc for initialization");
      else
	{
	  // Genuine run start
	  cm_msg(MINFO,"tr_start","In the process of starting run %d (type 2)",
		 run_number);


	  // write this for BNMR/BNQR custom web page error handling
    	  char got_event_str[128];
	  BOOL got_event_flag=FALSE;

	  sprintf(got_event_str,"/Equipment/%s/client flags/got_event",eqp_name);
	  status = db_set_value(hDB,0,got_event_str,&got_event_flag,sizeof(got_event_flag),1,TID_BOOL);
	  if(status != DB_SUCCESS)
	    cm_msg(MERROR,"bnmr_darc","Error setting got_event flag=%d to \"%s\"(%d)",got_event_flag,got_event_str,status); 
	  else
	    printf("tr_start: cleared type 2 got_event_flag\n");
        }

      if(write_data)
	{
	  
	  /* set up hot link on toggle on a genuine run start (or if run is going) 
	     hot link on toggle is closed by tr_stop
	  */
	  if (rn != 0  ||   run_state == STATE_RUNNING )
	    {
	      if(debug)printf("tr_start: Calling setup_hot_toggle\n");
	      status = setup_hot_toggle();
	      if(status!=DB_SUCCESS)
		printf("tr_start: Failure from setup_hot_toggle. Can still save data, but toggle won't work\n");
	    }
	} /* if write_data */
    } // end of Type 2
  if(write_data)
    {    
      if(debug)printf ("tr_start: logging is enabled\n");
      //DARC_camp_zero( p_odb_data->camp_host );
    }
  else
    printf ("tr_start: ** logging is currently disabled **\n");
   

#ifdef EPICS_ACCESS
      // open EPICS connection ONLY if we are already running and write_data is true

  if(rn==0 && run_state == STATE_RUNNING) // only if run is already in progress
    {
      // init_epics calls open_epics_log which fills n_epics;  also zeroes statistics
      if(ppg_type==2 && write_data)
	{
	  printf("tr_start: number of epics values to be logged is %d (requested) %d (output)\n",
		 epicslog_settings.n_epics_logged, epicslog_settings.output.num_log_ioc);
	  
#ifndef MUSR
	  {
	    INT flag=FALSE;
	    if( epicslog_settings.n_epics_logged != epicslog_settings.output.num_log_ioc)
	      { // one or more of the requested epics iocs has a problem; flag as non-fatal error
		flag=TRUE;
		printf("set flag TRUE\n");
	      }
	    write_client_code(EPICS_LOG_ERR,SET,"mdarc");
#endif
	    printf("Opening epics as we are already running\n");
	    status = init_epics(TRUE); // Type 2 run; keeps statistics
#ifndef MUSR
	    if(!flag)
	      write_client_code(EPICS_LOG_ERR,CLEAR,"mdarc");
	  }
#endif

      /* I don't think we should be calling this; it opens the epics connections
         and type 1 gets its epics from the bank evar.
      else
      status = init_epics(FALSE); // Type 1;  */
 
	  if(status != SUCCESS)
	    return status;
	  printf("tr_start: epics_available = %d\n",epics_available);
	}
    }
  else if (rn != 0)
    printf("tr_start: Genuine run start; not initializing EPICS yet, will be done by hotlink \n");
  
  
#endif
  
  if (rn != 0  ||   run_state == STATE_RUNNING )
    {
      printf("Success!\n");
 
    }
  printf("tr_start: returning success\n");
  return DB_SUCCESS;
}


/*----- stop  ----------------------------------------------*/
INT tr_stop(INT rn, char *error)
{
  
  char str[128], filename[128], archived_filename[128];/* standard length 128 */
  INT next, nfile;
  BOOL flag;
  BOOL archived;
  HNDLE hktmp;

  if(debug)printf("tr_stop: In the process of stopping the run %d\n", rn);

#ifndef MUSR
  flag=TRUE;
  status = db_set_value(hDB, 0, "/custom/hidden/get next runnum", &flag, sizeof(flag), 1, TID_BOOL);
  if(status != SUCCESS)
    cm_msg(MERROR,"mdarc","Error setting custom flag \"/custom/hidden/get next runnum\" (%d)",status);
  else
    printf("tr_stop: set hidden flag\n");
#endif
 

  // flush the buffer to get the last event & ensure next run gets new data 
  if (hBufEvent)
    {
      if(debug)printf("tr_stop: flushing the cache\n");
      status = bm_flush_cache(hBufEvent, TRUE);
      if(status != SUCCESS)
	printf("tr_stop: bm_flush_cache returns status %d\n",status);
    }

  if(!write_data) return SUCCESS;

  if( fmdarc.suppress_save_temporarily)
    {  // do not save the last event
      if(debug) printf("tr_stop: suppress_save_temporarily flag (%d)  is TRUE\n"
		       ,fmdarc.suppress_save_temporarily);
      cm_msg(MINFO,"tr_stop","Final data will not be saved (kill button sets this flag)");
    }
  if (ppg_type == 1)
    {
      if(debug)printf("tr_stop: type 1 event\n");
      if(running_already || !write_data)
	return (SUCCESS);  // not writing data, or already running when mdarc started
      //            if already running, cannot write an msr file
      if(!fmdarc.suppress_save_temporarily)
	{
	  if(debug)printf("tr_stop: calling ana_end_of_run\n");
	  status=ana_end_of_run(rn);
	}
    }
  else 
    { /* ppg type 2 */
      if(! fmdarc.suppress_save_temporarily)
	{ /* save the last event */
	  if (debug)printf("tr_stop: type 2 event\n");
	  
	  if(bGotEvent)
	    {
	      // no need to set saving_now flag as hotlink is closed
	      cm_msg(MINFO,"tr_stop","Calling bnmr_darc to save the final data for this run");
	      status = bnmr_darc(pHistData);
	    }
	  else
	    { 
	      cm_msg(MINFO,"tr_stop","No final data available for this run"); 
	    }
	} /* end of saving the last event */
      else
	bGotEvent=FALSE; // discard any remaining event
	
      /* 
	 if the toggle flag is set, we are in the middle of the toggle procedure,
	 and haven't saved a file with the new run number 
	 - this will happen if no valid data is available  */
      
      /* check whether toggle flag is still set  */
      if(toggle)
	{
	  cm_msg(MINFO,"tr_stop","Warning - toggle is set. There may not be a final saved file\n");
	  
	  toggle = FALSE;  /* set global toggle false */
	  /* turn toggle bit off in odb  */
	  size = sizeof(toggle);
	  status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
	  if (status != DB_SUCCESS)
	    cm_msg(MERROR, "tr_stop", "cannot clear toggle flag ");
	  
	  if(debug) printf ("tr_stop:  Toggle is cleared \n");
    
    
	  /* DO NOT restore the hot link on toggle  */
	  
	  /*    if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
		{
		size = sizeof(fmdarc.toggle);
		status = db_open_record(hDB, hktmp, &fmdarc.toggle, size, MODE_READ, hot_toggle , NULL);
		if (status != DB_SUCCESS)
		{
		cm_msg(MERROR,"tr_stop","Failed to open record (hotlink) for toggle (%d)", status );
		write_message1(status,"bnmr_darc");
		return(status);
		}      
		}
	  */
	}     // if toggle
      else   
	{
	  /* remove hot link while the run is off */
	  if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
	    {
	      if(debug)printf("tr_stop: Closing toggle hotlink\n");
	      db_close_record(hDB, hktmp);
	    }
	}
    } // end of if type 2


  /* kill script also sets this flag */
  if( ! fmdarc.endrun_purge_and_archive)
    {
      if(debug) printf(" End of run purge/rename/archive procedure flag (%d)  is FALSE\n"
		       ,fmdarc.endrun_purge_and_archive);
      cm_msg(MINFO,"tr_stop","Saved file WILL NOT be purged/renamed/archived (Kill button sets this flag");
      /*
	cm_msg(MINFO,"tr_stop","You may run 'cleanup' to execute end-of-run procedure for this run");
	cm_msg(MINFO,"tr_stop","once you have deleted any bad saved files");
      */
    }
  else 
    { 
      if(debug)
	{
	  printf(" End of run purge/rename/archive procedure flag (%d)  is TRUE\n"
		 ,fmdarc.endrun_purge_and_archive);
	  printf("tr_stop: saved_data_directory: %s\n",fmdarc.saved_data_directory);
	  printf("tr_stop: archived_data_directory: %s\n",fmdarc.archived_data_directory);
	  printf("calling darc_rename with run number %d   \n",rn);
	}
      
      /* purge/rename final run file  & archive */    
      status = darc_rename_file(rn, fmdarc.saved_data_directory
				, fmdarc.archived_data_directory, filename, archived_filename, &archived);
      if (status == SUCCESS)
	{      
	  /* update saved filename in odb 
	     &  make sure we write correct length string to odb */
	  sprintf(fmdarc.last_saved_filename, "%s", filename);
	  sprintf(str,"last_saved_filename");
	  if(debug) printf(" tr_stop: writing filename= %s to odb key %s \n",
			   fmdarc.last_saved_filename, str);
	  size=sizeof(fmdarc.last_saved_filename);
	  status = db_set_value(hDB, hMDarc, str, fmdarc.last_saved_filename, size, 
				1, TID_STRING);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"tr_stop","cannot write to odb key /equipment/%s/mdarc/%s", eqp_name,str);
	      write_message1(status,"tr_stop");
	    }
	  if(archived)
	    {
	      /* update last archived filename in odb 
		 &  make sure we write correct length string to odb */
	      sprintf(fmdarc.last_archived_filename, "%s", archived_filename);
	      sprintf(str,"last_archived_filename");
	      if(debug) printf(" tr_stop: writing filename= %s to odb key %s \n",
			       fmdarc.last_archived_filename, str);
	      size=sizeof(fmdarc.last_archived_filename);
	      status = db_set_value(hDB, hMDarc, str, fmdarc.last_archived_filename, size, 
				    1, TID_STRING);
	      if (status != DB_SUCCESS)
		{
		  cm_msg(MERROR,"tr_stop","cannot write to odb key /equipment/%s/mdarc/%s", eqp_name,str);
		  write_message1(status,"tr_stop");
		}
	    } // if archived
	} // status==success
    } // end of purge/rename flag set
  
  if (debug) 
    printf("tr_stop: releasing camp connection and saving odb\n");
  DARC_release_camp(); /* close the camp connection on end of run */

#ifdef EPICS_ACCESS
  if(ppg_type == 2)
    { // type 2  shut down EPICS at end of run
      if(debug_epics) printf("tr_stop: epics_available = %d\n",epics_available);
      if(epics_available)
	{
	  printf("tr_stop: closing epics connections");
	  epics_close();
          epics_available=FALSE;
	}
    }
#endif
  odb_save(rn, fmdarc.saved_data_directory);  // save odb

  stop_time = ss_time(); // set a timer when run was stopped (if saving data only)
  //                          this should get automatic run number checker to run after the run is ended
  if(debug)printf("tr_stop: set stop_time to %d\n",stop_time); 
  return SUCCESS;
}

/*----- process_event ----------------------------------------------*/
void  process_event(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
  char  bk[5];
#ifdef HAVE_YBOS
  char banklist[YB_STRING_BANKLIST_MAX];
#else
  char banklist[MD_STRING_BANKLIST_MAX]; // YBOS support gone
#endif
  BANK  *pmbk;
  BANK_HEADER *pmbh;
  DWORD * pdata;
  WORD  * pWdata;
  INT    bn, ii, jj, i,  nbk, bkitem, hn;
  DWORD  bklen, bktyp;
  DWORD    offset;
  BOOL   first;
  INT    num_HistBanks; /* local counter */
  INT    num_HistBins; /* local counter */
  char   type[10];
  char   str[128];
  INT    num_bytes;

  if(!write_data)
    {
      if(debug_proc)printf("process_event:  write_data is false, do nothing\n");
      return;
    }

  if(ppg_type ==1)   /* running type 1 */
    {
      if (running_already)
	{ /* send another message to remind the user */
	  cm_msg(MINFO,"process_event",
		 "** No MUD file (.msr) can be produced for this run; use Midbnmr to convert after end run"); 
	  return; // run was started before mdarc
	}
    }
  pmbh = (BANK_HEADER *) pevent;
  if(debug_proc)
    printf("\n process_event: Starting with Ser: %ld, ID: %d, size: %ld and bGotEvent %d\n",
	   pheader->serial_number, pheader->event_id, pheader->data_size,bGotEvent);
  
  first = TRUE; 
  /// status = yb_any_event_swap(FORMAT_MIDAS, pheader);
  
  nbk = bk_list(pmbh, banklist);
  if(debug_proc) printf("process_event: #banks:%i Bank list:-%s-\n", nbk, banklist);
  
  
  if(ppg_type ==1)   /* running type 1 */
    {      
#ifdef BINCHECK // not MUSR
      // (possibly) temporary check for type 1f (empty bin problem) BNM/QR only
      if (bincheck_flag)
	{
	  INT    status;
          printf("process_event: calling check_for_empty_bins for special mode 1f \n");
	  status = check_for_empty_bins( hBuf,  request_id,  pheader,  pevent);
	  if(status != SUCCESS)
	    {
	      cm_msg(MINFO,"process_event","event (Serial Number: %ld) found with 1f constant time empty bin problem (nbad=%d, ngood=%d)",
		     pheader->serial_number, bin1_bad_cntr,bin1_good_cntr);
	      // return; // do not skip this event as leaves a gap in file. Can check this event if not skipped.
	    }
	} // bincheck flag is set
#endif // BINCHECK
 
      if(debug)printf ("process_event: Process type 1 Hist event, calling process_histo_event\n"); 
      if(debug_proc)
	{
	  printf("process_event: hBuf=%p request_id=%d pheader=%p  pevent=%p\n", 
		 hBuf, request_id, pheader, pevent); 
	  printf("process_event: Processing event with Serial Number: %ld \n",
		 pheader->serial_number);
	}
      process_histo_event(hBuf,  request_id,  pheader,  pevent);
      return; 
    }

    

  /*
    Type 2
    process histogram bank
  */
  
/* Assuming each bank name is 4 characters only &   all histogram banks begin with HI or HM or HS (sample/ref).   
     All histogram banks have the same length.
     There may be banks present that are not histograms  */
  
  num_HistBanks=0; /* zero number of histogram banks in this event (local) */
  
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4)
    {
      /* bank name */
      strncpy(bk, &banklist[jj], 4);
      bk[4] = '\0';
      if (  (strncmp(bk,"HM",2) == 0) ||  (strncmp(bk,"HI",2) == 0) ||  (strncmp(bk,"HS",2) == 0) )
	{
	  /*if(debug_proc)printf ("Found bankname HI, HM or HS , bk=%s\n",bk);*/
	  num_HistBanks++;
	  /* Use the length of the bank to allocate space */
	  
          if (first)  /* assumes all banks are the same length */
	    {
	      first = FALSE;
	      status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);

	      num_HistBins = bk_locate(pmbh, bk, &pdata); /* bank length */
              if (num_HistBins == 0)
		{
		  cm_msg(MERROR,"process_event","Error - Bank %s length = 0; Bank not found", bk);
		  return;
		}    
	    }
          
	}
       else
	 if(debug_proc)printf ("bankname did NOT contain HI, HM or HS , bk=%s\n",bk); 
      
    }


  if(debug_proc)printf("process_event: event contains %d front-end histogram banks\n",num_HistBanks);  
  
  if(num_HistBanks != nHistBanks)  /* compare with expected value from mdarc area */
    {
      /* At BOR  process_event gets called before tr_start has updated the record 
	 Also it's possible rf_config hasn't had time to write the new value when changing between types, 
	 so read the sizes from odb to check.
      */
      if(debug_proc)printf("process_event: Front end has sent %d histogram banks. Expected %d\n",
			   num_HistBanks,nHistBanks);
      size = sizeof(nHistBanks);
      status = db_get_value(hDB, hMDarc, "histograms/number defined", &nHistBanks, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"process_event","could not get value for \"../mdarc/histograms/number defined\" (%d)",status);
	  return;
	}
      if(debug_proc)printf("process_event: Reread nHistBanks from odb as %d\n",nHistBanks);
      /* now check again */
      if(num_HistBanks != nHistBanks)  /* compare with expected value from mdarc area */
	{
	  /* let's reject this event - most likely rf_config still hasn't had time to update mdarc area
	     and in any case we are only saving data occasionally */
	  cm_msg(MINFO, "process_event", "Front end has sent %d histogram banks. Expected %d; rf_config may not yet have updated mdarc area",
		 num_HistBanks,nHistBanks);
	  return;
	}
    
    }
  
  if(num_HistBins != nHistBins)  /* compare with expected value from mdarc area */
    {
      
      /* At BOR process_event gets called before tr_start has updated the record (MUSR only)
	 Read the sizes from odb to check
      */
      if(debug_proc)printf("process_event: Front end has sent %d histogram bins. Expected %d\n",
			   num_HistBins,nHistBins);
      size = sizeof(nHistBins);
      status = db_get_value(hDB, hMDarc, "histograms/num bins", &nHistBins, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"process_event","could not get value for \"../mdarc/histograms/num bins\" (%d)",status);
	  return;
	}
      if(debug_proc)printf("process_event: Reread nHistBins from odb as %d\n",nHistBins);
      /* now check again */
      
      if(num_HistBins != nHistBins)  /* compare with expected value from mdarc area */
	{
	  cm_msg(MERROR, "process_event", "Front end has sent %d histogram bins. Expected %d",
		 num_HistBins,nHistBins);
	  return;
	}
    }
  
  /* assume histogram banks are all of length nHistBins. This will be checked later */
  if (pHistData) free(pHistData); pHistData = NULL;
  
  /* allocate full space once only
     Note for MUSR: data is stored as DWORD in pHistData even if the bank is WORD; 
     DARC_write only deals with DWORD - this ought to be changed so it can deal with WORD 

     add an extra word for the serial number of the bank
  */            
  num_bytes =  ( nHistBins * sizeof(DWORD) * nHistBanks) + sizeof(DWORD);
  if(debug_proc) printf("Histogram allocation %d bytes i.e. (%d * 4 * %d) + %d \n",
			num_bytes,nHistBins, sizeof(DWORD),nHistBanks, sizeof(DWORD));
  pHistData = (caddr_t) malloc(num_bytes ); /* nHistBanks histograms */
  if (pHistData == NULL)
    {
      cm_msg(MERROR,"process_event","Error allocating total space for histograms %d"
	     ,num_bytes);
      return;
    }
  else
    {
      if(debug_proc)
	printf("process_event_TD: pHistData = %p and serial no = %d \n",pHistData, pheader->serial_number);
      else
	printf("process_event_TD: processing event serial no = %d \n",pheader->serial_number);
    }
  /* remember pointer to where the serial number is stored (after all the data) */
  pSN = (caddr_t)(pHistData + num_bytes - sizeof(DWORD)); /* address of stored serial number */
  if(debug_proc)
    printf("pSN = %p ; pHistData = %p; num_bytes=%d; sizeof(DWORD)=%d   sizeof(int)=%d \n",
	   pSN,pHistData,num_bytes,  sizeof(DWORD),  sizeof(int)  );
  
  ((int*)pSN)[0] =  pheader->serial_number  ;

 
  /* assuming that each bank has always 4 char */
  hn=0;  /* initialize histogram counter */
  
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4) 
    {
      /* get bank name */
      strncpy(bk, &banklist[jj], 4);
      bk[4] = '\0';
      if (  (strncmp(bk,"HM",2) == 0) ||  (strncmp(bk,"HI",2) == 0)|| (strncmp(bk,"HS",2) == 0) )
	{
	  if(debug_proc)printf ("Got bankname HI, HM or HS, bk=%s\n",bk);
	  
	  if(debug_proc)printf("working on histogram bank %s\n",bk);
	  status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
	  bkitem = bk_locate(pmbh, bk, &pdata);

	  if (bkitem == 0)
	    {
	      cm_msg(MERROR,"process_event","Error - Bank %s length = 0; Bank not found", bk);
	      return;
	    }
	  
          if (bkitem != nHistBins)
	    {
	      cm_msg(MERROR,"process_event","Unexpected bank length (%d) for bank %s; expected # bins = %d",
		     bkitem, bk, nHistBins);
	      return;
	    }
	  
	  offset = hn * nHistBins; /* offset in words into pHistData */
	  hn++;      /* increment histogram number for next time */
	  if(debug_proc)printf("Offset in words into pHistData for histogram %s is %d\n",bk,offset);

          // BNMR always has DWORD (32-bit) data (no 16 bit)
	  memcpy(pHistData + offset*sizeof(int),  pdata, nHistBins*sizeof(DWORD));    
	           
          
	  if(debug_dump)
	    {
	      /* check parameters dump_begin, dump_length are valid */
	      if(dump_begin < 0) dump_begin = 0;
	      if( (dump_length + dump_begin) > bkitem)
		{
		  dump_length = bkitem + 1  - dump_begin;
		  printf("debug_dump - Invalid dump_length. Setting dump_length to %d\n",dump_length);
		}
	      printf("%s bank: offset in copied array will be: %d\n",bk ,offset); 
	      for (i=dump_begin; i< (dump_length + dump_begin) ;i++)
		printf("%s bank: Copied pdata[%d] = 0x%x to pHistData[%d]=0x%x\n",
		       bk , i, pdata[i], i+offset, ((int*)pHistData)[i+offset]);
		  
	    }
	}
      /*  else
	  if(debug_proc)printf("Bankname %s did not contain HM, HI or HS\n",bk); */ 
    }
  if(hn != nHistBanks)
    {
      cm_msg(MERROR,"process_event","Unexpected error in # histograms detected. Expected %d, got %d",
	     nHistBanks,hn);
      return;
    }
  
  if(debug_proc)printf("process_event: setting bGotEvent TRUE\n");
  bGotEvent = TRUE;
  
  return;
}

/*---- HOT_LINKS ---------------------------------------------------*/
INT setup_hotlink()
{
  /* does all initialization including setting up hotlinks */
  char str[128];
  HNDLE hktmp;
  char  *s;
  INT i,j, struct_size;
  
  FIFO_ACQ_MDARC_STR(acq_mdarc_str); 

  if(debug) printf("setup_hotlink starting\n");

  status=cm_exist("mdarc",FALSE); 
  //printf("status after cm_exist for mdarc = %d (0x%x)\n",status,status);
  if(status == CM_SUCCESS)
  {
      cm_msg(MERROR, "setup_hotlink","Another mdarc client has been detected");
      return(CM_NO_CLIENT); /* return an error */
  }


  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","key not found /Runinfo/State (%d)",status);
    return(status);
  }

  /* find the key for mdarc */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name); 
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      hMDarc=0;
      if(debug) printf("setup_hotlink: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_hotlink","Failure creating mdarc record (%d)",status);
	  if (run_state == STATE_RUNNING )
	    cm_msg(MINFO,"setup_hotlink","May be due to open records while running. Stop the run and try again");
	  return(status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hMDarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMDarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "setup_hotlink", "error during get_record_size (%d) for mdarc record",status);
	  return status;
	}
      struct_size =  sizeof(FIFO_ACQ_MDARC);
      
      printf("setup_hotlink:Info - size of mdarc saved structure: %d, size of mdarc record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        cm_msg(MINFO,"setup_hotlink","creating record (mdarc); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
        /* create record */
        status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"setup_hotlink","Could not create mdarc record (%d)\n",status);
          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"setup_hotlink","May be due to open records while running. Stop the run and try again");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }
  
  /* try again to get the key hMDarc  */
  
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "setup_hotlink", "key %s not found (%d)", str,status);
    write_message1(status,"setup_hotlink");
    return (status);
  }

  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"setup_hotlink");
    return(status);
  }
  
  /*
    get the path for the perl script get_next_run_number.pl
  */
  sprintf(perl_script,"%s",fmdarc.perlscript_path);
  trimBlanks(perl_script,perl_script);
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_script,'/');
  i= (int) ( s - perl_script );
  j= strlen( perl_script );
 
  //printf("setup_hotlink: string length of perl_script %s  = %d, last occurrence of / = %d\n", perl_script, j,i);
  if ( i == (j-1) )
  {
    //if(debug_check) printf("darc_get_odb: Found a trailing /. Removing it ... \n");
    perl_script[i]='\0';
  }
  strcpy(perl_path,perl_script); /* save in global; needed later (also by bnmr_darc for MUSR) */
  strcat(perl_script,"/get_next_run_number.pl");
  if(debug)printf("setup_hotlink: using perl_script = %s\n",perl_script);


  /* set hot link on num_versions_before_purge  */
  if (db_find_key(hDB, hMDarc, "num_versions_before_purge", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.num_versions_before_purge);
    status = db_open_record(hDB, hktmp, &fmdarc.num_versions_before_purge, size, MODE_READ, NULL, NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for num_versions_before_purge (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for num_versions_before_purge   (%d)", status );

  
  /* set hot link on endrun_purge_and_archive  */
  if (db_find_key(hDB, hMDarc, "endrun_purge_and_archive", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.endrun_purge_and_archive);
    status = db_open_record(hDB, hktmp, &fmdarc.endrun_purge_and_archive, size, MODE_READ, NULL, NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for endrun_purge_and_archive (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for endrun_purge_and_archive (%d)", status );
  
  /* set hot link on suppress_save_temporarily  */
  if (db_find_key(hDB, hMDarc, "suppress_save_temporarily", &hktmp) == DB_SUCCESS)
    {
      size = sizeof(fmdarc.suppress_save_temporarily);
      status = db_open_record(hDB, hktmp, &fmdarc.suppress_save_temporarily, size, MODE_READ, NULL, NULL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for suppress_save_temporarily (%d)", status );
	  write_message1(status,"setup_hotlink");
	  return(status);
	}
    }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for suppress_save_temporarily (%d)", status );
 
    
  /* set hot link on save_interval  */
  if (db_find_key(hDB, hMDarc, "save_interval(sec)", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.save_interval_sec_);
    status = db_open_record(hDB, hktmp, &fmdarc.save_interval_sec_, size, MODE_READ, update_Time , NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for save_interval (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for save_interval (%d)", status );

  /* saving previous save_Time for limit check later in update_Time */
  time_save = fmdarc.save_interval_sec_;
  if((INT)time_save < 1 || (INT)time_save > 24*3600 )                 /* time_save > 1sec, < 1 day */
  {
    fmdarc.save_interval_sec_ = time_save = 60;
    size=sizeof(fmdarc.save_interval_sec_);
    status = db_set_value(hDB, hMDarc, "save_interval(sec)", &fmdarc.save_interval_sec_, size, 1, TID_DWORD);
  }


  /* hot link is actually opened at begin of run */

  if(debug)
  {
    printf("setup_hotlink: Updated parameters from odb\n");
    printf("    time_save = %d \n",time_save);
    printf("    arch_dir = %s\n", fmdarc.archived_data_directory);
    printf("    save_dir = %s\n", fmdarc.saved_data_directory);
    if(fmdarc.endrun_purge_and_archive)
      printf("  eor purge flag is set : %d\n", fmdarc.endrun_purge_and_archive);
    else

      printf("  eor purge flag is NOT set : %d\n", fmdarc.endrun_purge_and_archive);
    if(fmdarc.suppress_save_temporarily)
      printf("  suppress_save_temporarily flag is set : %d\n", fmdarc.suppress_save_temporarily);
    else
      printf("  suppress_save_temporarily flag is NOT set : %d\n", fmdarc.suppress_save_temporarily);
    
  }


#ifdef GONE
  status = create_ClFlgs_record(); // get the record for the client flags
  if (status != DB_SUCCESS)
    printf("setup_hotlink: error return from create_ClFlgs_record (%d)\n",status);


  /* set up hot link on rf_config flag */
  if (db_find_key(hDB, hCF, "rf_config", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fcf.rf_config);
    status = db_open_record(hDB, hktmp, &fcf.rf_config, size, MODE_READ, hot_rf_config , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for client flag rf_config (%d)", status );
	write_message1(status,"setup_hotlink");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hotlink","Failed to find key for client flag rf_config (%d)", status );
      return(status);
    }
#endif // GONE

#ifdef EPICS_ACCESS
  hEpics = setup_epics_record(); // gets the handle to the epics information
  if(hEpics == 0)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to get or create record for epics access");
      return(DB_INVALID_PARAM);
    }
 
  // open the hotlink which will tell us when at_start_run.csh has finished updating epics values
  // open a hotlink on num_log_ioc
  
  size = sizeof(n_epics);
  sprintf(str,"output/num_log_ioc");
  status = db_find_key(hDB, hEpics, str, &hEpicsNum);
  if (status == DB_SUCCESS)
    {
      status = db_open_record(hDB, hEpicsNum, &n_epics
			      , size
			      , MODE_READ, hot_num_epics, "num_log_ioc touched");
      if (status != DB_SUCCESS) 
	cm_msg(MINFO,"setup_hot_link","open record on \"%s\"  failed (%d)",str,status);
      else
	cm_msg(MINFO,"setup_hot_link","open record on \"%s\" succeeded (%d)",str,status);
    }
  else
    cm_msg(MINFO,"setup_hot_link","find key on \"%s\" failed (%d)",str,status);
#endif

  return(status);
}

/*------------------------------------------------------------------*/
INT setup_hot_toggle()
{
  /* called by tr_start only on genuine run start, so toggle cannot be set before 
     a valid run number is established */
  char str[128];
  HNDLE hktmp;
  
  
  if(debug) printf("setup_hot_toggle starting\n");

  
/* set up hot link on toggle flag */
  if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
  {
    /* make sure toggle bit is switched off to start with */
    toggle = FALSE ;  // global
    size = sizeof(toggle);
    status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
    if (status != DB_SUCCESS)
      cm_msg(MERROR, "setup_hot_toggle", "cannot initialize toggle flag ");
    
    status = db_open_record(hDB, hktmp, &fmdarc.toggle, size, MODE_READ, hot_toggle , NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hot_toggle","Failed to open record (hotlink) for toggle (%d)", status );
      write_message1(status,"setup_hot_toggle");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hot_toggle","Failed to find key for toggle (%d)", status );
  
  return(status);
}


/*------------------------------------------------------------------*/
void update_Time(HNDLE hDB, HNDLE htmp, void *info)
{

  /* remove hot link */
  if(debug)printf("update_Time starting...present time_save=%d\n",time_save);
  
  db_close_record(hDB, htmp);
  
  /* get the new value */

  size = sizeof(fmdarc.save_interval_sec_);
  status = db_get_value(hDB, hMDarc, "save_interval(sec)", &fmdarc.save_interval_sec_, &size, TID_DWORD, FALSE);

  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"update_Time","could not get new value of save interval ");
      write_message1(status,"update_Time");
      return ;
    }
  if(debug)printf("update_Time: Time_save is to be updated to %d\n",fmdarc.save_interval_sec_);

  if((INT)fmdarc.save_interval_sec_ < 1
     || (INT)fmdarc.save_interval_sec_ > 24*3600 ) /* time_save > 1sec, < 1 day */
    {
      cm_msg(MINFO,"update_Time","Invalid time save interval (%d). Using previous value (%d sec)",
	     fmdarc.save_interval_sec_,time_save);
      fmdarc.save_interval_sec_ = time_save;
      
      /* update the odb with this value */
      if(debug)printf("update_Time: Restoring /Equipment/%s/mdarc/save_interval(sec) to %d\n"
		      , eqp_name, fmdarc.save_interval_sec_);
      size=sizeof(fmdarc.save_interval_sec_);
      status = db_set_value(hDB, hMDarc, "save_interval(sec)"
			    , &fmdarc.save_interval_sec_, size, 1, TID_DWORD);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"update_Time","cannot write to odb key /Equipment/%s/mdarc/save_interval(sec) %d"
		 , eqp_name, status );
	  write_message1(status,"update_Time");
	}   
    }  // end of bad value

  time_save = fmdarc.save_interval_sec_;
  if (debug)printf("update_Time: Time_save is now updated to %d\n",fmdarc.save_interval_sec_);


  /* restore Hot link */
  size = sizeof(fmdarc.save_interval_sec_);
  status = db_open_record(hDB, htmp, &fmdarc.save_interval_sec_, size, MODE_READ, update_Time , NULL);
  if (status != DB_SUCCESS)
  {
      cm_msg(MERROR,"update_Time","Failed to open record (hotlink) for mdarc (%d)", status );
      write_message1(status,"updata_Time");
      return;
  }

   return;
}

/*------------------------------------------------------------------*/

void hot_toggle (HNDLE hDB, HNDLE hktmp ,void *info)
{
/* toggle is done by user button TOGGLE - calls a perlscript toggle.pl which
   sets odb toggle flag on certain conditions - including that automatic run
   numbering in enabled */ 
  
  // uses BOOL toggle - a global in mdarc.h 
  char str[128];
  char cmd[256];
  char old_type[10];
  
  
  /* toggle bit has been touched - user wants to toggle between real and test
     runs  */
  printf ("hot_toggle: starting ..... toggle has been touched.  \n");
  if(debug)
    printf ("hot_toggle: global toggle flag = %d, run number = %d,  closing record\n",toggle,run_number);
  /* remove hot link while we toggle the run */
  db_close_record(hDB, hktmp);
  
  if (toggle) /* shouldn't happen if hot link is closed */
    {
      printf("hot_toggle: unexpected error - toggle global flag is already set\n");
      return;
    }
  
  
  toggle = TRUE;   /* global flag */
  
  old_run_number=run_number; /* save run number */
  strcpy(old_type,run_type);
  
  if (debug)
    printf("hot_toggle: old run number = %d, and  run type = %s\n",
           old_run_number, old_type);
  
  /* determine the run type */
  if(debug) printf ("hot_toggle: run_type = %s \n",run_type);
  if  (strncmp(run_type,"real",4) == 0)
    {
      if(debug) printf ("hot_toggle: detected present run type as real \n");
      strcpy(run_type,"test"); /* run is real so we now want a test run */
      //if (debug) printf ("now should be test;  run type=%s \n",run_type);
    }
  else if  (strncmp(run_type,"test",4) == 0)
    {
      if (debug) printf ("hot_toggle: detected present run type as test \n");
      strcpy(run_type,"real"); /* run is test so we now want a real run */
      //if (debug) printf ("now should be real;  run type=%s \n",run_type);
    }
  else
    {
      cm_msg(MERROR,"hot_toggle","Unknown run type detected (%s). Cannot toggle run \n",run_type);
      return; 
    }
  if(debug) printf ("Writing new run_type = %s to odb\n",run_type);
  sprintf(str, "/Equipment/%s/mdarc/run type",eqp_name);
  size = sizeof(run_type);
  if(size != sizeof(fmdarc.run_type))
    {
      cm_msg(MERROR,"hot_toggle","cannot update key %s; size mismatch",
	     str);
      return;
    }

  status = db_set_value(hDB, 0, str, run_type, size,1, TID_STRING);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
	     str,status);
      write_message1(status,"hot_toggle");
      return ;
    }    
  
  /*
    Get a new run number with perl script
  */
  if(debug) printf ("hot_toggle: getting a run number for a %s run...\n",run_type);

  /* add parameter perl_path */
  sprintf(cmd,"%s %s %s %d %s 0, %s %s",perl_script,perl_path,expt_name,run_number,eqp_name,lc_beamline,ppg_mode);
  if(debug) printf("Hot_toggle: sending system command  cmd: %s\n",cmd);
  status =  system(cmd);
  if (status)
    /* cannot get a new run number */
    {
      cm_msg (MERROR," hot_toggle",
	      "A status of %d has been returned from perl script get_next_run_number.pl",status);
      cm_msg (MERROR," hot_toggle",
	      "Check perlscript output file \"/home/midas/log/%s/get_next_run_number.txt\" for details",lc_beamline);
      /* note: if no recent addition to this file, there may be compilation errors in the perl script */
     

      if(debug) printf(" hot_toggle: Resetting run type to previous value (%s)",old_type);
      
      size = sizeof(old_type);
      if(size != sizeof(fmdarc.run_type))
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s; size mismatch",
		 str);
	  return;
	}

      status = db_set_value(hDB, 0, str, old_type, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
		 str,status);
	  write_message1(status,"hot_toggle");
	}
      toggle = FALSE;   /* clear global flag */
      cm_msg (MERROR," hot_toggle","Cannot toggle run. Continuing with run %d",run_number);
      return;
    }
  
  //  get the new run number - this is now a global value in mdarc.h
  size = sizeof(run_number);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"hot_toggle","key not found  /Runinfo/Run number");
      write_message1(status,"hot_toggle");
      return ;
    }
  if (run_number == old_run_number)
    {
      status=cm_msg(MERROR,"hot_toggle","??? Strange error: Run number not toggled after perlscript ");
      /* rewrite old run type */
      //sprintf(str, "/Experiment/edit on start/run type");
      size = sizeof(old_type);
      if(size != sizeof(fmdarc.run_type))
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s; size mismatch",
		 str);
	  return;
	}

      status = db_set_value(hDB, 0, str, old_type, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
		 str,status);
	  write_message1(status,"hot_toggle");
	}    
      return;
      
    }
  else
    status=cm_msg(MINFO,"hot_toggle",
                  "* * * *  hot_toggle:  Run number has been toggled from %d to %d ( %s to %s) * * * * ",
                  old_run_number,run_number,old_type, run_type);
  
  return;
}

/* this routine is not used any more as perlscript file is now opened for append */
INT print_file(char* filename)
{
  FILE *FIN;
  char str[256];

  printf ("\n------------------------------------------------------------------------\n");
  printf ("   print_file: Contents of information file %s:\n",filename);
  printf ("--------------------------------------------------------------------------\n");
  FIN = fopen (filename,"r");
  if (FIN == NULL)
  {
      printf ("print_file : Error opening file %s\n",filename);
      return(1);     
  }
  else
  {
    while(fgets(str,256,FIN) != NULL)
      printf("%s",str);
  }
  fclose (FIN);
  printf ("--------------------------------------------------------------------------\n");
  printf ("   print_file: End of information file %s: \n",filename);
  printf ("--------------------------------------------------------------------------\n\n");
  return(0);
}

#ifdef GONE
// changed all long to int to work on both 32- and 64-bit machines
INT check_host_type(void)
{
  char str[32];  
  
  host64=0; // assume 32-bit machine
  
  sprintf(str,"empty");
  if(getenv("HOSTTYPE"))
    {
      strncpy(str, getenv("HOSTTYPE"), sizeof(str));
      
      printf("str=%s\n",str);
      if(strstr (str,"_64"))
	{
	  printf("check_host_type:  Detected 64 bit machine\n");
	  host64=1; // set flag
	}
      else
	printf("check_host_type:  NOT 64 bit machine\n");
    }
  printf("check_host_type:  Assuming a 32-bit machine\n");
  
  if(host64)
    {
      if(sizeof(int) != sizeof(DWORD))
	{
	  printf("check_host_type: Expect sizeof(int) (%d) to be the same as sizeof(DWORD) (%d) on a 64-bit machine\n",
		 sizeof(int),sizeof(DWORD));
	  return 0; // FAILURE
	}
    }
  else
    { // 32-bit machine
      if(sizeof(long) != sizeof(DWORD))
	{
	  printf("check_host_type: Expect sizeof(long) (%d) to be the same as sizeof(DWORD) (%d) on a 32-bit machine\n",
		 sizeof(long),sizeof(DWORD));
	  return 0; // failure
	}
    }
  return SUCCESS;
}
#endif   // GONE


/*------------------------------------------------------------------*/
int main(unsigned int argc,char **argv)
{
  INT     size;
  char   host_name[HOST_NAME_LENGTH];
  char   ch;
  INT    msg, i;
  BOOL   daemon=FALSE;
  int j=0;  
  char   dbg[25];
  int    bug;
  char str[10];
  char hosttype[32];
  
  gbl_rn_check=FALSE;

#ifdef MUSR    // keep this just in case
  printf("\n This is the wrong main program for MUSR - it supports BNMR !!! \n\n");
  return 0;
#endif
 
  /* set defaults for globals */
  debug = debug_proc = debug_mud = debug_check = debug_dump = debug_cf = debug_epics = FALSE;
  dump_begin = 0;
  dump_length = 5;
  run_number = -1; /* initialize to a silly value for go_toggle */ 
  toggle = FALSE;  /* initial value */
  
  /* end defaults */

  cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);
  
  /* initialize global variables to default values */

  /* No longer determine feclient according to TWO_SCALERS because
     test system (bnmr) on dasdevpc has only one scaler */
    sprintf(feclient,"fe%s",expt_name);
    sprintf(eqp_name,"FIFO_acq");


  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
  {
    /* Run as a daemon */    
    if (argv[i][0] == '-' && argv[i][1] == 'D')
      daemon = TRUE;
    
    else if (argv[i][0] == '-')
    {
      if (i+1 >= argc || argv[i+1][0] == '-')
        goto usage;
      if (strncmp(argv[i],"-e",2) == 0)
        strcpy(expt_name, argv[++i]);
      else if (strncmp(argv[i],"-h",2)==0)
        strcpy(host_name, argv[++i]);
      else if (strncmp(argv[i],"-q",2)==0)
        strcpy(eqp_name, argv[++i]);
      else if (strncmp(argv[i],"-f",2)==0)
        strcpy(feclient, argv[++i]);
/*      else if (strncmp(argv[i],"-s",2)==0)   put time as odb param
        time_save = atoi(argv[++i]);
*/

      else if (strncmp(argv[i],"-d",2)==0)
      {
        strcpy(dbg, argv[++i]);  // dbg = debug level 
        debug=TRUE;
      }
      else if (strncmp(argv[i],"-b",2)==0)  /* in mdarc.h (these are debug parameters) */
        dump_begin =  atoi(argv[++i]);
      else if (strncmp(argv[i],"-l",2)==0)
        dump_length =  atoi(argv[++i]);
    }
    else
    {
   usage:
      printf("usage: mdarc  [-h Hostname] [-e Experiment]\n");
//      printf("              [-s time interval (sec)] to save data
//                                (def:60s)\n");
      printf("              [-f Frontend client name] (default:%s)\n",feclient);
      printf("              [-q equipment name] (default:%s)\n",eqp_name);
      printf("              [-D ] (become a daemon)\n");
      printf("              [-d debug level] \n");
      printf("  (0=debug 1=mud/camp 2=save files 3=dump 4=process event 5=client flags 6=epics 7=all\n");
      printf("              [-b begin] [-l length] (histo dump params for debug level=3. Default b=0 l=5. \n");
      printf(" e.g.  mdarc -h dasdevpc -e bnmr  \n");
      return 0;
    }
  }
 
  if (daemon)
    {
      printf("Becoming a daemon...\n");
      ss_daemon_init(FALSE);
    }


/* convert expt_name to lower case */
  for (j=0; j<strlen(expt_name); j++) expt_name[j]=tolower (expt_name[j]); 
/* make sure we have the right version of mdarc for this experiment */

  if (  !strncmp(expt_name,"bnmr",4)  ||  !strncmp(expt_name,"bnqr",4) )
  {
    if(debug)printf("This version of mdarc DOES  support experiment %s\n",expt_name);
  }
  else
  {
    printf("This version of mdarc DOES NOT SUPPORT experiment %s\n",expt_name);
    goto error;
  }

    
  if (debug)   
  {
    bug = atoi(dbg);
    switch (bug)
    {
    case 0:      /* level 0  Just debug */
      break;   
    case 1:      /* level 1  Just debug_mud  */
      debug_mud = TRUE;
      debug = FALSE;
      break;   
    case 2:      /* level 2 Just debug_check */
      debug_check = TRUE;
      debug = FALSE;
      break;
    case 3:
      debug_dump = TRUE;
      break;
    case 4:
      debug_proc = TRUE;
      break;
    case 5:
      debug_cf = TRUE;
      debug=FALSE;
      break;
    case 6:
      debug_epics = TRUE;
      debug=FALSE;
      break;
    default:
      debug_dump=debug_mud=debug_check=debug_proc=debug_cf=debug_epics=TRUE;
      break;
    }
  }
  if(debug_dump) printf("main: data dump will begin at bin = %d, length = %d\n",dump_begin,dump_length);
  if(debug)printf("main: feclient = \"%s\" and  eqp_name =  \"%s\"\n",feclient,eqp_name);

  /* connect to experiment */
  printf("calling cm_connect_experiment with host_name \"%s\", expt_name \"%s\"\n",host_name,expt_name);
  status = cm_connect_experiment(host_name, expt_name, "Mdarc", 0);
  if (status != CM_SUCCESS)
    goto error;

  /* turn off watchdog if in debug mode */
  if (debug)
    cm_set_watchdog_params(TRUE, 0);
  
  /* turn on message display, turn on message logging */
  cm_set_msg_print(MT_ALL, MT_ALL, msg_print);
  
  /* open the "system" buffer, 1M size */
  /*  bm_open_buffer("SYSTEM", EVENT_BUFFER_SIZE, &hBufEvent); midas 1.9.5
      for midas 2.0.0 EVENT_BUFFER_SIZE has gone, replace by  */
  bm_open_buffer("SYSTEM", 2*DEFAULT_MAX_EVENT_SIZE,  &hBufEvent);

  /* set the buffer cache size */
  bm_set_cache_size(hBufEvent, 100000, 0);
  
  /* place a request for a specific event id */

  bm_request_event(hBufEvent, 2 , TRIGGER_ALL, GET_ALL, &request_id, process_event);
  bm_request_event(hBufEvent, 13, TRIGGER_ALL, GET_ALL, &camp_request_id, process_camp_event);// CVAR bank
  bm_request_event(hBufEvent, 14, TRIGGER_ALL, GET_ALL, &darc_request_id, process_darc_event);// DARC CAMP EPICS banks
  bm_request_event(hBufEvent, 19, TRIGGER_ALL, GET_ALL, &epics_request_id, process_epics_event);// EVAR bank
 

      
      /* Midas 1.9.5 */
      /* frontend start is 500,  frontend stop is 500 
	 so prestart before frontend, 
	 start (poststart) after frontend, 
    Try this:
    post stop after frontend stops 600
    and frontend uses a deferred stop transition */
      
  status =  cm_register_transition(TR_START, tr_start, 500);
  if(status != CM_SUCCESS)
     {       
      printf("Failed to register transition \"tr_start\" (%d)\n",status);
      goto error;
    }
  status = cm_register_transition(TR_START, tr_prestart,450);
  if(status != CM_SUCCESS)
     {       
      printf("Failed to register transition \"tr_prestart\" (%d)\n",status);
      goto error;
    }
  status =   cm_register_transition(TR_STOP, tr_stop, 600) ;
  if(status != CM_SUCCESS)
     {       
      printf("Failed to register transition \"tr_stop\" (%d)\n",status);
      goto error;
    }

  
  /* connect to ODB */
  status=cm_get_experiment_database(&hDB, &hKey);
  if(status != CM_SUCCESS)
    {
      printf("Failed to get database (%d)\n",status);
      goto error;
    }
  

  /* Create and setup <eqp_name>/settings */
  if(debug)printf ("calling setup_hotlink\n");  
  status = setup_hotlink(); /* initialization including set up keys hMDarc and open hot links */
  if(status!=DB_SUCCESS)
  {
    cm_disconnect_experiment(); /* abort the program. Cannot save the data */
    return 0;
  }
 

  /* Init sequence through tr_prestart once */
  {
    char str[256];
    if(tr_prestart(0, str) !=DB_SUCCESS)
    {
      printf("Failure from tr_prestart\n");
      cm_disconnect_experiment(); /* abort the program. Cannot save the data */
      return 0;
    }
    printf("success from tr_prestart...  calling tr_start\n");
    if(tr_start(0, str) !=DB_SUCCESS)
    {
      printf("Failure from tr_start\n");
      cm_disconnect_experiment(); /* abort the program. Cannot save the data */
      return 0;
    }
  }

  if(debug)
  {
    printf("\n");
    printf("mdarc parameters : \n");
    printf("Camp hostname: %s\n",fmdarc.camp.camp_hostname);
    printf("Time of last save: %s\n",fmdarc.time_of_last_save);
    printf("Last saved filename: %s\n",fmdarc.last_saved_filename);
    printf("Saved data directory: %s\n",fmdarc.saved_data_directory);
    printf("# versions before purge: %d\n",fmdarc.num_versions_before_purge);
    printf("End of run purge/rename/archive flag: %d\n",fmdarc.endrun_purge_and_archive);
    printf("Archived data directory: %s\n",fmdarc.archived_data_directory);
    printf("Save interval (sec): %d\n",fmdarc.save_interval_sec_);
  }

  
  /* initialize ss_getchar() */
  ss_getchar(0);
  
  last_time = 0;

 do
   {
     /* call yield once every half second */
     msg = cm_yield(500);

    

	 if(stop_time > 0) // run has recently stopped
	   if(ss_time() - stop_time > 3) // allow 3s for everything to have stopped
	     {
	       /* run the run number checker ready for next time */
	       if (!fmdarc.disable_run_number_check)
		 {
		   printf("\n\n mdarc: calling auto_run_number_check 3s after a stop\n");
		   gbl_rn_check = FALSE;
		   status = auto_run_number_check(0);
		   if (status != DB_SUCCESS)
		     cm_msg(MERROR,"mdarc","Warning: post-stop run number check failed"); // auto_run_number_check failed
		   else
		     printf("mdarc main: success from post-stop run number check\n"); // auto_run_number_check worked
		   stop_time = 0; // reset so we don't come here again
#ifndef MUSR
		   BOOL flag=FALSE;
		   status = db_set_value(hDB, 0, "/custom/hidden/get next runnum", &flag, sizeof(flag), 1, TID_BOOL);
		   if(status != SUCCESS)
		     cm_msg(MERROR,"mdarc","Error clearing custom flag \"/custom/hidden/get next runnum\" (%d)",status);
		   else
		     printf("Cleared hidden flag\n");
#endif
		 }
	     }
	 
     /* time to save data */
     if (ss_time() - last_time > time_save )
       {
	 last_time = ss_time();

	 if(write_data)
	   {
	     if (ppg_type == 2)
	       {  // bnmr_darc for Type 2 only
		 if ( fmdarc.suppress_save_temporarily)
		   {
		     cm_msg(MINFO,"save_data",
			    "odb flag \"suppress_save_temporarily\" is set. Data will not be saved");   
		   }
		 else
		   {
		     if(debug)printf("     Mdarc: Calling bnmr_darc to save the data\n");
		     status = bnmr_darc(pHistData);
		   }
	       }
	   } /* end of write_data */
	 else if (ppg_type ==2)
	   if(debug)printf(" Mdarc: data logging is disabled in odb ... not calling bnmr_darc \n");
	 
	 fflush(stdout); /* output may be sent to a file */	  
       } // end of if(ss_time() ...  )
     
     
     /* check keyboard */
     ch = 0;
     if (ss_kbhit())
       {
	 ch = ss_getchar(0);
	 if (ch == -1)
	   ch = getchar();
	 if ((char) ch == '!')
	   break;
       }
   }
 while (msg != RPC_SHUTDOWN && msg != SS_ABORT);
 
 error:
 /* reset terminal */
 ss_getchar(TRUE);
 
 DARC_release_camp();
 cm_disconnect_experiment();
}
/*---------------------------------------------------------------------------*/
INT get_ppg_type(void)
/*---------------------------------------------------------------------------*/
{
  char str[128];
  INT j;
  char experiment_name[32]; // size must be same as in odb: ../frontend/input/experiment name 

  /* fills globals ppg_mode : a string e.g. 1a,2c etc   MUSR dummy -> "20"
                   ppg_type : integer 1 or 2  for type1 or type2 MUSR -> 2
  */

  /* Get the beamline 

     For BNMR/BNQR, we'll use expt_name as beamline(i.e. input parameter mdarc -e bnqr or Midas default )
  */

  strcpy(beamline,expt_name);  

  

  for (j=0; j<strlen(beamline); j++)
    {
      beamline[j] = toupper (beamline[j]); /* convert to upper case */
      lc_beamline[j]= tolower  (beamline[j]); /* and lower case for opening perl txt files */
    }
  if(debug) printf(" beamline:  %s\n",beamline);
  
  if (strlen(beamline) <= 0 )
    {
      printf("No valid beamline is supplied\n");
      return (DB_INVALID_PARAM);
    }

  /* Check for BNQR/BNMR Type  */    
  sprintf(str,"/equipment/%s/frontend/input/experiment name",eqp_name); // get ppg mode (e.g. 1f etc. )
  size = sizeof(experiment_name);
  status = db_get_value(hDB, 0, str, &experiment_name, &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"get_ppg_type",
	     "could not get value for \"%s\" (%d)",str,status);
      return DB_INVALID_PARAM;
    }
  strncpy(ppg_mode,experiment_name,2);
  ppg_mode[2] = '\0';

  if (strncmp(ppg_mode,"1",1)==0)
    {
      printf("get_ppg_type: a %s type 1 (I-MUSR) experiment is set up\n",beamline);
      ppg_type=1;
    }
  else if (strncmp(ppg_mode,"2",1)==0)
    {
      printf("get_ppg_type: a %s type 2 (TD-MUSR) experiment is set up\n",beamline);
      ppg_type = 2; 
    }
  else
    {
      cm_msg(MERROR,"get_ppg_type",
	     "unknown ppg mode \"%s\" (%d)",ppg_mode,status);
      return DB_INVALID_PARAM;
    }
  return(status);
}

/*------------------------------------------------------------------*/
INT check_midas_logger(INT rn)
/*------------------------------------------------------------------*/
{
  char format[]="%06d.mid";//official MUSR-type format
  char odb_format[]="%06d.odb";// format to agree with type 2
  char str[128];
  char filename[256];
  BOOL odb_dump=TRUE;
  BOOL active;
  INT len;
  /* 
     Called for both types of run 

     Check midas logger area is set up properly  
        ... only when the program is started (rn=0)

   Perlscript will check the saved directories are the same for both types at each begin run
   - unless no auto run numbering in which case check_run_number will do it

  */

  if(ppg_type < 1 || ppg_type > 2)
    {
      cm_msg(MERROR,"check_midas_logger","invalid ppg type %d ",ppg_type);
      return (DB_INVALID_PARAM);
    }

  /* get the Midas saved directory into global MData_dir regardless of type  */ 
  sprintf(str,"/logger/Data dir");
  size = sizeof(MData_dir);
  db_get_value(hDB, 0, str, MData_dir, &size, TID_STRING,FALSE);
  if(debug) printf("check_midas_logger : got key %s from odb as : %s\n",str,MData_dir); 
  if (status == DB_TRUNCATED)
    {
      cm_msg(MINFO,"check_midas_logger","truncated  %s",str);
      write_message1(status,"check_midas_logger");
    }
  else if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"check_midas_logger","key not found %s ",str);
      write_message1(status,"check_midas_logger");
      return (status);
    }
   
  

  if (rn == 0)
    {
      
      // Check filename format of midas file
      sprintf(str, "/logger/Channels/0/settings/Filename");
      size = sizeof(filename);
      db_get_value(hDB,0, str,filename, &size, TID_STRING,FALSE);
      if(debug) printf("check_midas_logger : got midas filename from odb as \"%s\"\n",filename); 
      if (status == DB_TRUNCATED)
	{
	  cm_msg(MINFO,"check_midas_logger","truncated string %s",str);
	  write_message1(status,"setup");
	}
      else if (status != DB_SUCCESS)
	{
	  status=cm_msg(MERROR,"check_midas_logger","key not found %s ",str);
	  write_message1(status,"check_midas_logger");
	  return (status);
	}
      
      len = strlen(format); /* official MUSR-type run numbering scheme */
      if(strncmp(filename,format,len) != 0)
	{
	  printf("check_midas_logger: INFO -  format is not right for BNMR saved files\n");
	  printf("check_midas_logger : got format from odb key %s as \"%s\"\n",str,filename); 
	  printf("check_midas_logger : correcting format in odb\n"); 
	  size = sizeof(format);
	  status = db_set_value(hDB, 0, str, format, size,1, TID_STRING);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"check_midas_logger","cannot update key %s (%d)",
		     str,status);
	      write_message1(status,"check_midas_logger");
	      return (status);;
	    }    
	}
      
      // Check ODB dump format of midas file
      sprintf(str, "/logger/ODB Dump File");
      size = sizeof(filename);
      db_get_value(hDB,0, str,filename, &size, TID_STRING, FALSE);
      if(debug) printf("check_midas_logger : got midas DUMP filename from odb as \"%s\"\n",filename); 
      if (status == DB_TRUNCATED)
	{
	  cm_msg(MINFO,"check_midas_logger","truncated string %s",str);
	  write_message1(status,"setup");
	}
      else if (status != DB_SUCCESS)
	{
	  status=cm_msg(MERROR,"check_midas_logger","key not found %s ",str);
	  write_message1(status,"check_midas_logger");
	  return (status);
	}
      
      len = strlen(odb_format); 
      if(strncmp(filename,odb_format,len) != 0)
	{
	  printf("check_midas_logger : INFO:  format is not right for ODB  files\n");
	  printf("check_midas_logger : got format from odb key %s as \"%s\"\n",str); 
	  printf("check_midas_logger : updating key %s to %s\n",str,odb_format);
	  
	  size = sizeof(odb_format);
	  status = db_set_value(hDB, 0, str, odb_format, size,1, TID_STRING);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"check_midas_logger","cannot update key %s (%d)",
		     str,status);
	      write_message1(status,"check_midas_logger");
	      return (status);;
	    }    
	  
	}
      // make sure odb dump is enabled (to dump in file .odb)
      sprintf(str,"/logger/ODB Dump");
      size = sizeof(odb_dump);
      db_set_value(hDB, 0, str, &odb_dump, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  status=cm_msg(MERROR,"check_midas_logger","error setting key %s to %d",str,odb_dump);
	  write_message1(status,"check_midas_logger");
	  return (status);
	}
      // but not this one or odb gets dumped in .mid file
      odb_dump = FALSE;
      sprintf(str,"/logger/Channels/0/Settings/ODB Dump");
      db_set_value(hDB, 0, str, &odb_dump, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  status=cm_msg(MERROR,"check_midas_logger","error setting key %s to %d ",str,odb_dump);
	  write_message1(status,"check_midas_logger");
	  return (status);
	}
      

    } // end of general checks when rn=0



  // Check whether logger is enabled;
  // Note: "/logger/Write data" should be linked to /Experiment/edit on start/write data
  // and write_data is already set to this value,  so we'll just check on "active"
  
  sprintf(str,"/logger/Channels/0/Settings/Active");
  size = sizeof(active);
  db_get_value(hDB, 0, str, &active, &size, TID_BOOL, FALSE);
  //if(debug) printf("check_midas_logger : %s from odb as : %d\n",str,active); 
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"check_midas_logger","key not found %s ",str);
      write_message1(status,"check_midas_logger");
      return (status);
    }


  if(write_data)
    {
      if( !active)
	{
	  if(ppg_type == 1)
	    {
	      cm_msg(MINFO,"check_midas_logger","Strange.... %s should have been set TRUE for type %d by change_mode.pl  ",str,ppg_type);
	      active=1;
	      db_set_value(hDB, 0, str, &active, size, 1, TID_BOOL);
	      //if(debug) printf("check_midas_logger : %s from odb as : %d\n",str,active); 
	      if (status != DB_SUCCESS)
		{
		  status=cm_msg(MERROR,"check_midas_logger","key not found %s ",str);
		  write_message1(status,"check_midas_logger");
		  return (status);
		}
	    }
	}
      else
	{
	  if (ppg_type ==2)
	    {
	      cm_msg(MINFO,"check_midas_logger","Strange.... %s should have been set FALSE by change_mode.pl for type %d",str,ppg_type);
	      active=0;
	      db_set_value(hDB, 0, str, &active, size, 1, TID_BOOL);
	      //if(debug) printf("check_midas_logger : %s from odb as : %d\n",str,active); 
	      if (status != DB_SUCCESS)
		{
		  status=cm_msg(MERROR,"check_midas_logger","key not found %s ",str);
		  write_message1(status,"check_midas_logger");
		  return (status);
		}
	    }
	}
    }

      return (DB_SUCCESS);
}

/*---------------------------------------------------------------------------*/
INT check_run_number(INT rn)
/*  ---------------------------------------------------------------------------*/
{
 
  /* Checks run number and saved directories when automatic run number check is turned off. 
     Perlscripts NOT called in this mode
  */

  INT status;
  INT i,j,len,len1;
  char str[128];
  
  cm_msg(MINFO,"check_run_number","* * WARNING:  Automatic run numbering is disabled. This mode    * *");
  cm_msg(MINFO,"check_run_number","* *           is NOT RECOMMENDED except for emergencies         * *");


  if(debug) printf("check_run_number: Midas logger writes saved files into directory : %s\n",MData_dir);
  len = strlen(MData_dir); // filled in check_midas_logger
  len1=strlen(fmdarc.saved_data_directory);
  if (len1 <= 0  )
    {
      status=cm_msg(MERROR,"check_run_number","Empty string detected for mdarc odb key \"saved_data_directory\" ; No saved directory supplied");
      return (DB_OUT_OF_RANGE);
    }
  if (len <= 0  )
    {
      status=cm_msg(MERROR,"check_run_number","Empty string detected for odb key \"%s\" ; No saved directory supplied",str);
      return (DB_OUT_OF_RANGE);
    }
  if(debug) 
    {
      printf("mdarc saved directory : %s, length %d\n",fmdarc.saved_data_directory,len1);
      printf("mlogger saved directory : %s, length %d\n",MData_dir,len);
    }

  /* Check this directory is identical with saved directory in mdarc area
     perlscript does this (only called when auto run numbering is enabled) */

  if(strncmp(fmdarc.saved_data_directory,MData_dir,len1) != 0 )    
    {
      cm_msg(MINFO,"check_midas_logger","Midas saved data directory (\"%s\") is not same as in mdarc area (\"%s\")",
	     MData_dir,fmdarc.saved_data_directory);
    }
  else
    if(debug) printf("Midas data directory IS identical to saved_data_directory in mdarc area\n");
  
  

  //  get the actual run number;  (run_number is a global in mdarc.h)
  size = sizeof(run_number);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"check_run_number","key not found  /Runinfo/Run number");
      write_message1(status,"check_run_number");
      return (status);
    }
  printf (" Run number = %d\n",run_number);
      
  /* check run number against beamline when automatic run number checking is
     disabled  only for a genuine run start */
  //printf("rn=%d, run_state=%d\n",rn,run_state);
      
  if ( rn !=0  ||   run_state == STATE_RUNNING )    /* rn not 0 or  run is going already  */
    {      /* check run number against beamline */
      strcpy(run_type,fmdarc.run_type);
      for  (j=0; j< (strlen(run_type) ) ; j++)
	run_type[j] = tolower(run_type[j]); /* convert to lower case */
      trimBlanks (run_type,run_type); /* note - perl script has checked validity of run_type */  
      
      /* check for test run first */
      printf("Automatic run numbering is DISABLED: checking run number against beamline\n");
      if ( run_number  >= MIN_TEST && run_number <= MAX_TEST  ) 
	{
	  /* test run for any beamline */
	  if  (strncmp(run_type,"test",4) != 0)
	    {
	      cm_msg(MERROR,"check_run_number","Mismatch between run_type (%s) and run number (%d)",run_type, run_number);
	      cm_msg(MINFO,"check_run_number","Test runs must be in range 30000-30499");
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number. ");
	      return (DB_INVALID_PARAM);
	    }
	  cm_msg(MINFO,"check_run_number","Starting a TEST data run (%d)",run_number);
	}
      
      else   /* real run */
	{   
	  if(run_number >= MIN_BNMR && run_number <= MAX_BNMR )   //BNMR
	    {
	      if (strncmp(beamline,"BNMR",4) != 0)
		{
		      cm_msg(MERROR,"check_run_number",
			     "Mismatch between run number (%d) & beamline (%s); expect beamline = BNMR for this run number",run_number, beamline );
		      cm_msg(MINFO,"check_run_number", "Data logging cannot proceed with this run number.");
		      return (DB_INVALID_PARAM);
		}
	    }
	  else if (run_number >= MIN_BNQR && run_number <= MAX_BNQR)   //BNQR
	    {
	      if (strncmp(beamline,"BNQR",4) != 0)
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = BNQR for this run number",run_number, beamline );
		  cm_msg(MINFO,"check_run_number", "Data logging cannot proceed with this run number.");
		  return (DB_INVALID_PARAM);
		}
	    }

	  else
	    {
	      cm_msg(MERROR,"check_run_number","Run number (%d) does not match a valid beamline\n",run_number); 
	      cm_msg(MINFO,"check_run_number","Test runs must be in range %d to %d",MIN_TEST,MAX_TEST);
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
	      return (DB_INVALID_PARAM);
		}
	  printf("detected run %d for beamline %s \n",run_number,beamline);
	  /*  real run */
	  if  (strncmp(run_type,"real",4) != 0)
	    {
	      cm_msg(MERROR,"check_run_number","Mismatch between run_type (%s) and run number (%d)",run_type, run_number);
	      cm_msg(MINFO,"check_run_number","Test runs must be in range 30000-30499");
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
	      return (DB_INVALID_PARAM);
	    }
	  else
	    cm_msg(MINFO,"check_run_number","Starting a REAL data run (%d), (beamline %s) ",run_number,beamline);  
	}
      
      
    } /* end of run number check on genuine start or run already going when mdarc starts */
  
      
  return (status);
}

/* ----------------------------------------------------------*/ 
INT auto_run_number_check(INT rn)
/* ----------------------------------------------------------*/ 
{
  char  cmd[132];
  INT j;
  BOOL check;

  printf("auto_run_number_check starting\n");

  check=TRUE;  // default

  if (! fmdarc.enable_prestart_rn_check)
    {   /* automatic rn check at begin run is disabled */
    if (!gbl_rn_check)
      {  // gbl_rn_check failed last check. ( It is set up by auto_run_number_check)
	cm_msg(MINFO,"auto_run_number_check", "Rechecking run numbers...");
      }
    else
      check=FALSE;
  }

  if (check)
    {
      printf("calling get_next_run_number.pl and  setting gbl_rn_check FALSE\n");

      gbl_rn_check=FALSE; // set flag in case of failure
      /*
	perl script to find next valid run number and write it to /Runinfo/run number
	It (should) handle all eventualities  including mdarc restarted midway through a
	run.  It uses run_type to determine what type of run (test/real) the user
	wants, and assigns a run number accordingly.
      */
      
      sprintf(cmd,"%s %s %s %d %s 0, %s %s",perl_script, perl_path, expt_name,rn,eqp_name,lc_beamline,ppg_mode); 
      if(debug) 
	printf("auto_run_number_check: Sending system command  cmd: %s\n",cmd);
      status =  system(cmd);
      if(debug)
	printf("after system(cmd) status=%d\n",status);
      
      if (status)
	{
	  cm_msg (MERROR,"auto_run_number_check",
		  "A status of %d has been returned from perl script get_next_run_number.pl",status);
	  cm_msg (MERROR,"auto_run_number_check",
		  "Check perlscript output file \"/home/midas/musr/log/%s/get_run_number.txt\" for details",lc_beamline);
	  /* note: if no recent addition to this file, there may be compilation errors in the perl script */
	  return (DB_INVALID_PARAM); // bad status from perl script
	}
      else
	{
	  cm_msg(MINFO,"auto_run_number_check", "Run number check done.");
	  printf("auto_run_number_check: success, from get_next_run_number.pl,  setting flag TRUE\n");
	  gbl_rn_check=TRUE; // success
	}
    }
  else
    {  // don't check the run numbering with the perlscript
      if(debug)printf("auto_run_number_check: NOT checking run number since flag gbl_rn_check=%d\n",gbl_rn_check);
    }

  // Remember: 
  //     db_get_value formerly creates the key if it doesn't exist (i.e. returns
  //           no error)
  // Midas 9.0 does not create key if last param is FALSE

  //  get the run number which may have been changed by get_next_run_number or by run start
  //  this is now a global value in mdarc.h
  size = sizeof(run_number);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"auto_run_number_check","key not found  /Runinfo/Run number");
      write_message1(status,"auto_run_number_check");
      return (status);
    }
  
  /* and the run type (a global)  */
  strcpy(run_type,fmdarc.run_type);
  for  (j=0; j< (strlen(run_type) ) ; j++)
    run_type[j] = tolower(run_type[j]); /* convert to lower case */
  
  trimBlanks (run_type,run_type); /* note - perl script has checked validity of run_type */
  printf("auto_run_number_check: run number = %d, run_type=%s\n",run_number,run_type);
  return(status);
}

#ifdef EPICS_ACCESS
/*-- Call_back  ----------------------------------------------------*/
void hot_num_epics(HNDLE hDB, HNDLE hkey, void * info)
{
  /* hot link on /equipment/epicslog/settings/output/num_log_ioc
      num_log_ioc should be updated at beginning of run by at_start_run.csh which calls check_epics.csh

      Hot link only opened for type 2 runs
  */
  INT size,status,flag;

  printf("\n** HOT_NUM_EPICS: hot link touched; hkey= %d \"%s\" \n",hkey,info);
  printf("  n_epics is now %d\n",n_epics);
  printf(" value in structure is %d\n", epicslog_settings.output.num_log_ioc);
  
  if(ppg_type !=2) // nothing to do for Type 1
    {
      printf("hot_num_epics: type 1 nothing to do\n");
      return;
    }
  
  
  /* get the record to reflect any changes 
     NOTE: output record is now filled by at_start_run.csh (calls check_epics.csh)
  */
  size = sizeof (epicslog_settings);
  if(debug) printf("hEpics = %d, size of record = %d \n",hEpics,size);
  
  status = db_get_record(hDB,hEpics, &epicslog_settings, &size, 0);
  if(status!= DB_SUCCESS)
    cm_msg(MERROR, "hot_num_epics", "cannot retrieve \"/Equipment/EpicsLog/Settings\" record for Epics");
  
  printf("hot_num_epics: updated the record, number of epics values to be logged is %d (requested) %d (output)\n",
	 epicslog_settings.n_epics_logged, epicslog_settings.output.num_log_ioc);

#ifndef MUSR
  flag=FALSE;
  if( epicslog_settings.n_epics_logged != epicslog_settings.output.num_log_ioc)
    { // one or more of the requested epics iocs has a problem; flag as non-fatal error
      flag=TRUE;
      printf("set flag TRUE\n");
    }
  // init_epics calls open_epics_log which fills n_epics;  also zeroes statistics
  write_client_code(EPICS_LOG_ERR,SET,"mdarc");
#endif	
	   
  printf("hot_num_epics: calling init_epics for Type 2 run\n"); 
  status = init_epics(TRUE); // Type 2 run; keeps statistics
#ifndef MUSR
  if(!flag) // non-fatal error detected earlier
     write_client_code(EPICS_LOG_ERR,CLEAR,"mdarc");
#endif
  if(status != SUCCESS)  
    cm_msg(MERROR, "hot_num_epics", "bad status from init_epics (%d)",status);
  
  printf("hot_num_epics: epics_available = %d\n",epics_available);
}   
#endif

#ifdef BINCHECK // not MUSR
INT check_for_empty_bins( HNDLE hBuf, HNDLE req, EVENT_HEADER *pheader, void *pevent)
{
  // Returns SUCCESS and writes error message if HM00 (25MHz Reference Ch 1) has empty bin or abnormally high value (gate disconnected)
  // Returns SUCCESS if event OK or cannot determine
  INT status,size;
  BOOL btmp;
  float ftmp;

  INT n_items;
  DWORD  *pdata ;
  INT i,j,k,min,max;
  INT idiff;


  size=sizeof(btmp);
  status = db_get_value(hDB, 0, "/Equipment/fifo_acq/frontend/input/e1f const time between cycles", &btmp, &size, TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"check_for_empty_bins","key not found \"/Equipment/fifo_acq/frontend/input/e1f const time between cycles\"  (%d)",status);
      return 1; // process data
    }
  
  if(!btmp)
    return 1;

  if( bin1_25MHz_count == 0)  // zeroed at BOR
    {
      // Calculate expected count for 25 MHz test pulses in bin 1
      size=sizeof(ftmp);
      status = db_get_value(hDB, 0, "/Equipment/fifo_acq/frontend/output/dwell time (ms)", &ftmp, &size, TID_FLOAT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"check_for_empty_bins","could not get ODB value \"/Equipment/fifo_acq/frontend/output/dwell time (ms)\"  (%d)",status);
	  return SUCCESS; // process data
	}

      bin1_25MHz_count = 25000 * ftmp;
      printf("check_for_empty_bins: dwell time = %f ms; expected bin count for 25MHz clock is %d\n", ftmp,bin1_25MHz_count);
    }



  n_items = bk_locate(pevent, "HM00", &pdata);
  if(n_items <= 0)
    {
      cm_msg(MINFO,"check_for_empty_bins","did not find HM00 bank in this event. No check performed.\n");
      return SUCCESS;
    }
  min = 2*bin1_25MHz_count;  // set to large number
  max=i=j=k=0;

  for (i=0; i< n_items; i++)
    {
      if( pdata[i] > max)
	{
	  max= pdata[i] ;
	  j=i;
	}
      else if ( pdata[i] < min)
	{
	  min =  pdata[i];
	  k=i;
	}
    }	
  idiff =  abs(max-min);	      
  printf("check_for_empty_bins:HM00  calculated count= %d;  max=%d  at bin %d, min=%d  at bin %d; abs diff=%d", bin1_25MHz_count, max,j,min,k,idiff);
  if ( abs(max-min) > 5)
    {
      printf("***** error: abs(max-min) > 5\n");
      cm_msg(MERROR, "check_for_empty_bins","HM00 max=%d , min=%d ; difference > 5 counts,  calculated count=%d  ********* \n",max,j,min,k,bin1_25MHz_count);
      bin1_bad_cntr++;
    }
  else
    {
      bin1_good_cntr++;
      printf("\n");
    }
	      
  return SUCCESS;
}
#endif //  BINCHECK
