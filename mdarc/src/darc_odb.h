/* darc_odb.h

   Structure to store the odb data needed for bnmr_darc and musr_darc

   IMPORTANT - the sizes of these parameters MUST MATCH the sizes
   in the odb PARTICULARLY where hotlinks are used (db_create_record)
   otherwise get mis-match. 

  $Log: darc_odb.h,v $
  Revision 1.4  2016/07/05 01:36:52  suz
  increase sizes to prevent overruns

  Revision 1.3  2014/01/09 01:53:24  suz
  his_total and total_save changed to double from float for BNM/QR

  Revision 1.2  2013/04/08 22:25:53  suz
  Changes after debugging new VMIC code

  Revision 1.1  2013/01/21 21:42:38  suz
  initial VMIC version to cvs

  Revision 1.18  2005/06/13 17:09:42  suz
  Donald enlarges temperature and field strings


*/
/* for combined IMUSR/TDMUSR version, use ifdef MUSR */

#ifndef darc_odb_header
#define darc_odb_header
void copy_item(char *p_struct_str,  INT struct_str_size, char *p_odb_str, char *name);

/* Maximum number of TD-MUSR histograms */
#ifdef MUSR
#define MAX_HIS 22
// the following needed by musr_config and bnmr_darc.c (MUSR only):
#define MAX_CNTR MAX_HIS/2     // max number of counters supported
#else // BNxR
//#define MAX_HIS 25     /* increase num his to add more for sample/ref */ 
#define MAX_HIS 29     /* increase num his to add more for sample/ref and alpha modes */
#endif

#define HIS_SIZE 32    /* size of histo titles (both MUSR & BNMR) */

#ifdef MUSR
/* maximum number of IMUSR histograms */
#define MAX_IHIS 19  /* IMUSR has max. 19 fixed histograms
		        PD,clk,totalrate, 4 front , 4 back
			[ 4 aux1 4 aux2 are optional ]  */
#define MAX_NSCALS 16  /* maximum no. of scalers  for MUSR */

#else  // BNMR/BNQR

#define MAX_NSCALS 80  /* maximum no. of scalers for BNMR  
     this value must be >=  N_SCALER_TOT in frontend code */

#endif

typedef struct {
  char area[32]; 
  char rig[32]; 
  char mode[32];
  int  run_state;         /* state = STATE_RUNNING  if running */
  //
  int  run_number       ; /* /<area>/<rig>/parameters/mdarc/local_runnumber */
  int  experiment_number; /*                               /experiment_number */
  char experimenter[128]; /* increase this from 32 to prevent overruns */
  char run_title[128]; /* increase this from 80 */
  char sample[128]; /* increase this from 48 */
  char orientation[128]; /* increase this from 15 */
  char field[80]; /* increase this from 32 */
  char temperature[80]; /* increase this from 32 */
  char start_time_str[32];
  int start_time_binary;
  char stop_time_str[32];
  int stop_time_binary;
  //
  /* Bnmr hardware information */
  float  dwell_time;      /* (ms)   c.f. MUSR tdc_res !! */
  float  beam_time;       /* (ms)  not used */
  int   ncycles;         /* not used */


  /* histogram information */
  int his_n;             /* number of histograms defined */
  int his_nbin;          /* number of histogram bins */
  int nchan;             /* number of scaler channels enabled */
  
  int his_bzero[MAX_HIS];     /* time zero */     
  int his_good_begin[MAX_HIS];/* first good bin */
  int his_good_end[MAX_HIS];  /* last good bin */
  int his_first_bkg[MAX_HIS]; /* first background bin */
  int his_last_bkg[MAX_HIS];  /* last background bin */
  
  char his_titles[MAX_HIS][HIS_SIZE]; /* histogram titles */
  
  int purge_after;           /* purge data files after n versions */
  char save_dir[128];         /* e.g. /bnmr/dlog */
  char camp_host[LEN_NODENAME]; /* LEN_NODENAME =127 (camp.h) */ 

  char temperature_variable[128];
  char field_variable[128];


  // scalers
  int nscal; /* number of scalers in use */
  int scaler_save[MAX_NSCALS];/* contains scaler data */
#ifdef MUSR // combined IMUSR/MUSR
  int scaler_rate[MAX_NSCALS];/* contains scaler rates */
#endif
  char scaler_titles[MAX_NSCALS][32];

  // darc writes these into odb:    
  char time_save[32];         /* outputs time when saved */
  char file_save[128];        /* outputs saved file name */

#ifdef MUSR
  float his_total[MAX_HIS];    /* outputs his_total */
  float total_save;            /*  output  total_saved */
#else  // BNMR
  double his_total[MAX_HIS];    /* outputs his_total */
  double total_save;            /*  output  total_saved */
#endif

#ifdef MUSR // combined IMUSR/MUSR
    // IMUSR specific
    char comment1[128]; /* increase all comments from 32 */
    char comment2[128];
    char comment3[128];
    char subtitle[128];
    char IMUSR_his_titles[MAX_IHIS][HIS_SIZE]; /* histogram titles; increase from 11
						  now same as TD his titles */
    float IMUSR_his_total[MAX_IHIS];
#endif
}D_ODB;


#endif // darc_odb_header




