/********************************************************************\

  Name:         mheader.c	
  Created by:   Suzannah Daviel, TRIUMF

    - combined version of older programs fe_header and fe_camp

  This version for use with VMIC frontend

  Contents:     Frontend program for BNMR/BNQR/IMUSR ( for Type 1 )
                Sends DARC header information and CAMP data and CAMP path information
                needed by midbnmr_darc (a subroutine of mdarc) or midbnmr ( MID/MUD conversion program) 
                to make saved MUD  histogram files.
                EPICS added 2007

                Defines two equipments, CAMP and HEADER 

		CAMP equipment produces one bank periodically i.e.
		       CVAR bank contains values read from camp
		HEADER equipment produces two banks DARC and CAMP  on transitions (BOR/EOR)

	        The camp paths are read from /equipment/camp/settings in odb.
                They are checked out by a perlscript, camp.pl executed
                at begin-of-run.

$Log: mheaderBnmr.c,v $
Revision 1.5  2015/12/01 22:50:26  suz
updates for latest MIDAS and post-Ted CAMP library

Revision 1.4  2015/02/23 23:41:54  suz
add casts to avoid warnings

Revision 1.3  2014/01/09 01:15:19  suz
try to improve error handling

Revision 1.2  2013/04/25 20:14:59  suz
change to campSrv_VarGet on Donald's advice

Revision 1.1  2013/01/21 21:42:38  suz
initial VMIC version to cvs

Revision 1.6  2012/10/11 17:13:55  suz
change poll time of cvar and evar events at BOR

\********************************************************************/

#include <stdio.h>
#include <ctype.h>
//#include <signal.h>
//#include <unistd.h>
/* camp includes come before midas.h */

#include "camp_clnt.h"
/* to avoid conflict with midas defines, undefine these
   - they will be redefined by midas the same as camp defined them */
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#ifdef INLINE
#undef INLINE
#endif

#include "midas.h"
#include "experim.h"
//* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "mud_util.h"
#include "trii_fmt.h"
#include "mdarc.h"
#include "darc_odb.h"
#include "mheader.h" // prototypes
#include "mdarc_subs.h" // prototypes
INT make_camp_bank(char *pevent, INT len_darc);


#ifdef CAMP_ACCESS
#define BNMR_CAMP
#endif


/*
#ifndef MUSR
void catch_sigalrm(int signo);
#endif
*/
void hot_runstate(HNDLE hDB, HNDLE hkey, void * info);
#ifdef BNMR_CAMP
void hot_camp(HNDLE hDB, HNDLE hkey, void * info);
INT prestop(INT run_number, char *error);
#endif // BNMR_CAMP
#ifdef EPICS_ACCESS
void hot_epics(HNDLE hDB, HNDLE hkey, void * info);
#endif
void hot_hold(HNDLE hDB, HNDLE hkey, void * info); // hotlink for hold (Type1 run now set on hold at BOR)
INT set_client_flag(char *client_name, BOOL value);
INT poststop(INT run_number, char *error);
INT prestart(INT run_number, char *error);
INT close_hot_links(void);
INT set_poll_fast(BOOL flag);
INT check_run_state(void);
#ifdef EPICS_ACCESS
#include "epics_ca.h"
#include "connect.h" /* prototypes */
#include "epics_log.h"
//#include "epics_scan_devices.h" // EPICS device names, units, titles
#include "mdarc_epics.h" // prototypes of mdarc_epics.c routines
#endif

#define FAILURE 0
#define NEW
/*-- Globals -------------------------------------------------------*/

HNDLE hCamp=0,  hEpics=0, hFS=0; // hDB hMdarc defined in mdarc.h
CAMP_SETTINGS camp_settings;
EPICSLOG_SETTINGS epicslog_settings; 
INT Check_last_time;
INT At_start_camp_timer;
INT At_start_epics_timer;
 
#include "client_error_codes.h"
FIFO_ACQ_MDARC fmdarc;
FIFO_ACQ_FRONTEND fifo_set;
char eqp_name[]="FIFO_acq";
INT nH; /* not needed by MUSR */
#define  POLL_CVAR 10000 /* default polling time of 10s */
#define  POLL_EVAR 10000 /* default polling time of 10s */

/* the following globals needed to set run on HOLD at BOR (Type 1)
   while waiting for CAMP/EPICS banks  */
char sisflags_hold_path[128]; // needed to set frontend on hold at BOR (Type 1)
char hold_flag_path[128]; // warn users run is on hold waiting for camp/epics
BOOL gbl_hold; // flag set if run is on hold (by user or by mheader)
BOOL mheader_set_hold; // flag set if mheader (i.e. this program) set the run on hold
/* the following two globals are used in frontend_loop*/
INT gbl_run_start_timer; // timer starts at prestart. Used to change the Alarm message
INT gbl_error_flag; // set if mheader started while a type 1 is running; frontend_loop returns immediately
INT current_alarm_msg_flag; // which alarm message is being displayed
BOOL cvar_bank_sent,evar_bank_sent;
char campstr[]="CAMP";
char epicsstr[]="EPICS";
// Current states of Mheaders Alarm message;  current_alarm_msg_flag is set (by set_alarm_message) to one of these
#define ALL_CLEAR 0
#define ON_HOLD 1 // on hold due to mheader (not user!)
#define FIX_CAMP 2
#define FIX_EPICS 3
#define RIP 4
#define NO_LOG 5

HNDLE hHold=0; // needed for hold hotlink
static HNDLE hRS=0;

#ifdef EPICS_ACCESS
static HNDLE hEpicsOK;
INT my_epics_ok=5; // initial value
INT old_epics_ok=5; //
BOOL first_epics; 
#endif
#ifdef CAMP_ACCESS
static HNDLE hCampOK;
INT my_camp_ok=5; // temp variable for hotlink
INT old_camp_ok=5;
BOOL first_camp;
#endif

// camp_ok or epics_ok =    0          1          2            3        4  
static char* okstring[]={"FAILURE","SUCCESS","IN PROGRESS","TIMEOUT","REDO", "UNKNOWN"};
// run_state           =     1         2          3
static char* runstring[]={"STOPPED","PAUSED","RUNNING"};
INT send_camp_hdr,camp_hdr_sent;
INT send_epics_hdr,epics_hdr_sent;
BOOL send_darc_hdr,darc_hdr_sent;
BOOL gbl_epics_done,gbl_camp_done; /*  indicates that initialization procedure has been done 
                                       (may have succeeded or failed or camp/epics may not be present) */
BOOL SET=TRUE;
BOOL CLEAR=FALSE;
INT EPICSFLG=1,CAMPFLG=0,BOTHFLG=2; // logging flags used to communicate with custom web page 
int status;
int firstTime = TRUE;
int campInit  = FALSE;
int camp_available;
char serverName[LEN_NODENAME+1];
char perl_script[80] ; 
char perl_cmd[132];
FILE *FIN;
BOOL end_run;
char str[256];
char beamline[6];
char lc_beamline[6];
BOOL eorflag; // suppress status message at eor when resetting camp_ok
BOOL autorun; // 0 if not an autorun; needed to take run off hold if camp/epics fail
// ppg_type declared in mdarc.h
//INT ppg_type; // type 1 (IMUSR) or 2 (TDMUSR); CAMP events sent only for Type 1
char ppg_mode[5];


#ifdef EPICS_ACCESS

EPICS_LOG epics_log[MAX_EPICS];
extern EPICS_PARAMS epics_params[NUM_EPICS_DEV]; // epics_scan_devices.h
INT n_epics=0; // number of epics devices to be logged
BOOL epics_available=FALSE;
#endif
BOOL no_epics,no_camp; // none to be logged

INT run_state;

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "mheader";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms    */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size = 50000;

/* maximum event size (for fragmented events only) */
INT max_event_size_frag = 5*1024*1024;


/* buffer size to hold events */
INT event_buffer_size =0x200000;// DEFAULT_EVENT_BUFFER_SIZE;  


/* prototypes */

/*-- Equipment list ------------------------------------------------*/
EQUIPMENT equipment[] = {
  { "CAMP",                 /* equipment name */
    { 13, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING |         /* read when running  */
     RO_ODB,               /* and update ODB */ 
    POLL_CVAR,             /* read every so often */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub events */
    0,                    /* do not log history */
      "", "", "",},
    camp_var_read,        /* readout routine */
    NULL,                 /* class driver main routine */
    NULL,                 /* device driver list */
    NULL,                 /* init string */
  },

  { "HEADER",               /* equipment name */
    {14, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
     RO_RUNNING, // | RO_EOR, 
        /* Bank probably not ready at BOR; frontend_loop will send it ASAP ( BNMR/BNQR only) */
    15000,                /* read every 15 sec (need RO_RUNNING above) */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub events */
    0,                    /* log history every event  (if non-zero event is sent to
                             odb) */
     "", "", "",},
    header_info_read,       /* readout routine */
    NULL,                 /* class driver main routine */
    NULL,                 /* device driver list */
    NULL,                 /* init string */
  },

  { "EPICSLOG",                 /* equipment name */
    {19, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING |         /* read when running  */
     RO_ODB,               /* and update ODB */ 
    POLL_EVAR,                /* read every so often */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub events */
    0,                   /* do not log history  */
     "", "", "",},
    epics_var_read,        /* readout routine */
    NULL,                 /* class driver main routine */
    NULL,                 /* device driver list */
    NULL,                 /* init string */
  },
  
  { "" }
};

/*-- Dummy routines ------------------------------------------------*/

INT  poll_event(INT source[], INT count, BOOL test) {return 1;};
INT  interrupt_configure(INT cmd, INT source[], PTYPE adr) {return 1;};


 
/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  char str[256];
  int size,i,j, len, my_size;
  CAMP_SETTINGS_STR(camp_settings_str);
  EPICSLOG_SETTINGS_STR(epicslog_settings_str);
  FIFO_ACQ_MDARC_STR(mdarc_str);


  char *s;
  char cmd[128];
  
  debug = FALSE; 
  campInit = camp_available = FALSE;
  first_camp =first_epics = FALSE;
  hDB=hMDarc=0; // globals
  Check_last_time = ss_time();
  At_start_camp_timer = At_start_epics_timer = -1;
  gbl_error_flag=FALSE;
  send_camp_hdr=camp_hdr_sent=send_epics_hdr=epics_hdr_sent=send_darc_hdr=darc_hdr_sent=FALSE;
  no_epics=no_camp=FALSE;

#ifdef BNMR_CAMP
  gbl_camp_done=gbl_epics_done=FALSE; // initialize
#endif
  cm_register_transition(TR_STOP, prestop, 200);  
  cm_register_transition(TR_START, prestart, 200); 
  cm_register_transition(TR_STOP, poststop, 600); // close the camp and epics connections  
  cm_get_experiment_database(&hDB, NULL);


  /* find out our experiment name */
  size = sizeof (expt_name);
  status = db_get_value(hDB,0, "/Experiment/name",  expt_name, &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"frontend_init","error accessing key /Experiment/name");
    write_message1(status,"frontend_init");
    return (status);
  }
  if(debug)printf("Experiment name: %s\n",expt_name);

#ifdef EPICS_ACCESS
  printf("EPICS_ACCESS is defined\n");
  no_epics=TRUE;
#else
  printf("EPICS_ACCESS is NOT defined\n");
  no_camp=TRUE;
#endif



  /* make sure there's only one copy of this program running */
  status=cm_exist("mheader",FALSE); 
  if(debug) printf("status after cm_exist for client mheader = %d \n",status);
  if(status == CM_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init","Another copy of mheader is already running");
      return (CM_NO_CLIENT);  /* return an error */
    }


  hRS=0;
  status = db_find_key(hDB, hRS, "/runinfo/state", &hRS);
  if (status != DB_SUCCESS)
    {
      cm_msg(MINFO,"frontend","find key on \"/runinfo/state\" failed (%d)",status);
      return status;
    }

  if (hRS==0)
    {
      cm_msg(MERROR,"frontend","key hRS is zero");
      return DB_INVALID_PARAM;
    }
  size = sizeof(run_state);
  status = db_open_record(hDB, hRS, &run_state
			  , size
			  , MODE_READ, hot_runstate, "run state touched");
  if (status != DB_SUCCESS) 
    {
      cm_msg(MINFO,"frontend","open record on \"/runinfo/state\"  failed (%d)",status);
      return status;
    }


  /* get the run state to see if run is going */
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","error from get_value for /Runinfo/State (%d)",status);
      return(status);
    }
 
  printf("frontend_init: run_state=%d\n",run_state);



  /* get the key hMDarc  */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name);
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      hMDarc=0;
      if(debug) printf("frontend_init: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(mdarc_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"frontend_init","Failure creating mdarc record (%d)",status);
	  if (run_state == STATE_RUNNING )
	    cm_msg(MINFO,"frontend_init","May be due to open records while running. Stop the run and try again");
	  return(status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key mdarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMDarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "frontend_init", "error during get_record_size (%d) for mdarc record",status);
	  return status;
	}

      my_size = sizeof(FIFO_ACQ_MDARC);	// BNMR/BNQR

      printf("Size of mdarc saved structure: %d, size of mdarc record: %d\n", 
	     my_size,size);  
      
      if (my_size != size)
	{
	  cm_msg(MINFO,"frontend_init",
		 "creating record (mdarc); mismatch between size of structure (%d) & record size (%d)", 
		 my_size,size);
	  
	  /* create record */
	  status = db_create_record(hDB, 0, str , strcomb(mdarc_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"frontend_init","Could not create mdarc record (%d)\n",status);
	      if (run_state == STATE_RUNNING )
		cm_msg(MINFO,"frontend_init","May be due to open records while running. Stop the run and try again");
	      return status;
	    }
	  else
	    if (debug)printf("Success from create record for %s\n",str);
	}
    }

      
  /* try again to get the key hMDarc  */
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","Failed to get the key %s (%d)",str,status);
      return(status);
    }
  
  size = sizeof (fmdarc);
  if(debug) printf("hMDarc = %d, size of record = %d \n",hMDarc,size);
  
  status = db_get_record(hDB, hMDarc, &fmdarc, &size, 0);
  if(status!= DB_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot retrieve %s record (%d)",str,status);
      return DB_NO_ACCESS;
    }

  sprintf(sisflags_hold_path,"/equipment/%s/frontend/flags/hold",eqp_name); // global -> hold (for prestart)
  sprintf(hold_flag_path,"/equipment/%s/client flags/mh_on_hold",eqp_name);// global -> if > 1 triggers alarm banner

  status = set_alarm_message(ALL_CLEAR); // clears mheader alarm trigger and resets mh alarm

  status=get_ppg_type(); // fills ppg_type and beamline

  if(status!=DB_SUCCESS)
    {
      cm_msg (MERROR,"frontend_init","Cannot determine ppg mode");
      return (status);
    } 
      


#ifdef CAMP_ACCESS
  if(debug)printf("Camp hostname = %s\n",fmdarc.camp.camp_hostname);
  hCamp = setup_camp_record();//  creates camp record if necessary 
  if(hCamp == 0) // error
    return DB_INVALID_PARAM;
#endif

#ifdef EPICS_ACCESS
  hEpics = setup_epics_record();//  creates epics record if necessary 
  if(hEpics == 0) // error
    return DB_INVALID_PARAM;
  /*  printf("Calling open_epics_hot_link\n");
  status = open_epics_hot_link();
  if(status != SUCCESS)
    cm_msg(MINFO,"frontend_init","could not set up a hotlink on \"epics OK\" ");
  */
#endif


#ifdef CAMP_ACCESS
      // BNMR/BNQR set up a hotlink on "camp ok" This might be changed if user resets camp.
  /*printf("Calling open_camp_hot_link\n");
  status = open_camp_hot_link();
  if(status != SUCCESS)
    cm_msg(MINFO,"frontend_init","could not set up a hotlink on \"camp OK\" ");
  */
  /*
    Camp.pl  perlscript 
    camp_settings.perl_script contains only the filename 
  */
  len = strlen(camp_settings.perl_script );
      
  if(len <= 0)
    {
      cm_msg(MERROR, "frontend_init","no perlscript name supplied in /equipment/camp/settings/perl_script");
      return(DB_INVALID_PARAM);
    }
  
  if(debug)
    printf("perlscript file name: %s (len=%d)\n",camp_settings.perl_script,len);
  /*
    get the path of the perl scripts 
  */
  sprintf(perl_path,"%s",fmdarc.perlscript_path); /* perl_path (in mdarc.h) is needed as an input parameter (to perl script)  */
  trimBlanks(perl_path,perl_path);
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_path,'/');
  i= (int) ( s - perl_path );
  j= strlen( perl_path );
  
  if(debug)
    printf("string length of perl_path %s  = %d, last occurrence of / = %d\n", perl_path, j,i);
  if ( i == (j-1) )
    perl_path[i]='\0';
  
  strcpy(perl_script,perl_path); /* perl_path needed as a parameter  */
  strcat(perl_script,"/"); // add a "/" 
  strcat(perl_script,camp_settings.perl_script);
  trimBlanks(perl_script,perl_script);
  
  if(debug) {
    printf ("Perl script: %s\n", perl_script);
    printf ("Perl path:   %s\n", perl_path);
  }
#endif
      


  if(debug)printf("frontend_init: calling open_hot_links\n");
  status = open_hot_links();
  if (status != SUCCESS)
    return status;


  if(debug)printf("\n\nfrontend:  clearing client code for epics,camp\n\n");
  write_client_code(EPICS_LOG_ERR,CLEAR,"mheader"); //clear the bits that this program can write (epics,camp)
  write_client_code(CAMP_LOG_ERR,CLEAR,"mheader"); //
  
  write_logging_flag(BOTHFLG,0); // clear logging flags
  
  
  if(debug)
    {
      if(run_state > 0 && run_state < 4) 
	printf("frontend_init: run state is %d (%s)\n",run_state,runstring[run_state-1]);
      else
	printf("frontend_init: run state is %d\n",run_state);
    }
  /* Check if a run is in progress*/
  if(run_state != STATE_STOPPED)
    {
      printf("A run IS in progress (run_state=%d)\n",run_state);
      if(ppg_type !=1)
	{
	  printf("TD-type run in progress; no further action\n");
	  return(CM_SUCCESS);
	}

      printf("I-MUSR type of run in progress\n");

      printf("\n\nfrontend:  setting client code for epics,camp\n\n");

      write_client_code(EPICS_LOG_ERR,SET,"mheader"); //set the bits that this program can write (epics,camp)
      write_client_code(CAMP_LOG_ERR,SET,"mheader"); // will be cleared later

      cm_msg(MERROR,"frontend_init","Type 1 run is already in progress. Restart mheader after ending run.");
      gbl_error_flag=TRUE;

      status = set_alarm_message(RIP); // sets mheader alarm trigger
      stop_run();
    }
  else
    printf("frontend_init: Success.... waiting for run to start\n");

  if(debug)printf("\nfrontend_init: returning SUCCESS\n");
  return CM_SUCCESS;
}



/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  if(debug) printf("frontend_exit: starting\n");
#ifdef CAMP_ACCESS
  if (camp_available)
    {
      if(campInit)
	{
	  if(debug)printf("frontend_exit : calling camp_clntEnd()\n");
	  camp_clntEnd();  
	}
      else
	if(debug) printf("frontend_exit: INFO: camp connection is already closed \n");
    }
  else
    if(debug) printf("frontend_exit: INFO: camp connection was never opened \n");
#endif
  
#ifdef EPICS_ACCESS
  epics_close();
#endif
  
  return CM_SUCCESS;
}

#ifdef CAMP_ACCESS
/*----------------------------------------------------------------------*/
INT redo_camp(void)
{
  INT status;

  camp_hdr_sent = FALSE; // stops sending CVAR banks
  write_logging_flag(CAMPFLG,0);
  /* update the camp record */
  size = sizeof (camp_settings);
  if(debug)printf("redo_camp: Updating camp record\n");
  status = db_get_record(hDB,hCamp, &camp_settings, &size, 0);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR, "redo_camp", "failed to retrieve Camp record (%d)",status);
      return status;
    }

  if(debug)printf("redo_camp: camp_settings.camp_ok=%d old_camp_ok=%d my_camp_ok=%d\n",
	 camp_settings.camp_ok, old_camp_ok, my_camp_ok);
  
  status = init_camp();
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"redo_camp","WARNING: no CAMP logging can be done; fix CAMP and retry");
      // PROBLEM with CAMP; send_camp_hdr=FALSE camp_available=FALSE?
      write_client_code(CAMP_LOG_ERR,SET,"mheader");
    }
  else
    {
      if(debug)printf("redo_camp: setting send_camp_hdr TRUE\n");
      send_camp_hdr=TRUE; // set a flag
       if(debug)printf("redo_camp: success from init_camp, returning\n");
    }
  return status;
}


INT prestop(INT run_number, char *error)
{
  INT status;
  printf("\nprestop: starting\n");

  //status=close_hot_links();

  if(ppg_type ==1)
    {
      end_run = TRUE;
      printf("prestop: setting send_darc_hdr to TRUE for end-of-run\n"); // resend DARC header bank only
      send_darc_hdr=TRUE;
      darc_hdr_sent=FALSE; // clear this as we want to send another DARC header bank
    }
  else
    printf("prestop: type 2 run.. no action\n");

  return CM_SUCCESS;
}
#endif



INT poststop(INT run_number, char *error)
{
  INT ival=0;
  printf("poststop: starting\n");
#ifdef CAMP_ACCESS
  camp_close();
#endif
#ifdef EPICS_ACCESS
  epics_close();
#endif
  // ready for next run
  gbl_error_flag=FALSE;
  first_camp=first_epics=0;
  gbl_epics_done=gbl_camp_done=FALSE;
  send_camp_hdr=camp_hdr_sent=send_epics_hdr=epics_hdr_sent=send_darc_hdr=darc_hdr_sent=FALSE;
  

  // frontend clears hold at end of run; no need to clear it here

  /* clear any alarm message that might be left on */

 status = set_alarm_message(ALL_CLEAR); // clears mheader alarm trigger and resets alarm
 // set camp_ok for next time 
 eorflag=TRUE;
  status = db_set_value(hDB,hCamp,"camp ok",  &ival, sizeof(ival), 1, TID_INT);
  if (status != DB_SUCCESS)
    cm_msg(MERROR,"poststop","error clearing \"camp_ok\" (%d)",str,status);
	 
  printf("\n\n-----------------   poststop: Run %d ended -----------------------------\n\n",run_number);
 return status;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  INT my_run_state,trans,size,status,flag;
  BOOL itemp,jtemp;

  cm_yield(100);
  if(gbl_error_flag)return;

#ifndef CAMP_ACCESS
#ifndef EPICS_ACCESS
  return;
#endif
#endif


  
  if(ppg_type !=1)
    return; // Type 2; nothing to do


  if (   !(gbl_hold &&  mheader_set_hold)) // run is not held by mheader
    return; // we are running; nothing to do
 

 


  /* Run is on hold waiting for header bank(s) to be sent */
  if ((ss_time() - Check_last_time) > 5)  /* is it time to check ?  */
    {
      if(debug)printf("frontend_loop: checking with Check_last_time=%d ss_time=%d diff=%d\n",
	     Check_last_time,ss_time(),( ss_time()- Check_last_time ));
      
           if(my_run_state == STATE_STOPPED)
      	{
        printf("\n +++frontend_loop: ERROR - we are supposed to be running if get here!");
      	}

      printf("\n +++frontend_loop: Running!\n");


      Check_last_time=ss_time();
      
      
      
      // Run is being held by mheader - which means we are running (or trying to)
 
      printf("frontend_loop: darc,camp,epics hdr_sent=%d,%d,%d no_camp,no_epics=%d,%d\n",
	     darc_hdr_sent,camp_hdr_sent,epics_hdr_sent,no_camp,no_epics);

      if(darc_hdr_sent && (camp_hdr_sent || no_camp) && (epics_hdr_sent || no_epics))
	{ // resume run
	  gbl_hold=FALSE;
	  cm_msg(MINFO,"frontend_loop","Success - all headers sent, resuming run");
	  printf("frontend_loop: resuming run with darc/camp/epics hdr_sent=%d %d %d\n",
		 darc_hdr_sent,camp_hdr_sent,epics_hdr_sent);
	  set_hold(gbl_hold);
	  //status = set_client_flag("mheader",SUCCESS);
	  return SUCCESS;
	}



#ifdef GONE    
      if(debug)
	{
	  // DEBUG
	  //  get the run state
	  size = sizeof(my_run_state);
	  status = db_get_value(hDB,0, "/Runinfo/State",  &my_run_state, &size, TID_INT, FALSE);
	  if (status != DB_SUCCESS)
	    {
	      status=cm_msg(MERROR,"frontend_loop","error from db_get_value  /Runinfo/State (%d)",status);
	      return (status);
	    }
	  if(my_run_state == STATE_STOPPED)
	    {
	      printf("frontend_loop: ERROR - we are supposed to be running if get here!\n");
	      return CM_SUCCESS;
	    }
	  
	  if(run_state != my_run_state)
	    {
	      size = sizeof(trans);
	      status = db_get_value(hDB, 0, "/Runinfo/transition in progress", &trans, &size, TID_INT, FALSE);
	      if (status != DB_SUCCESS)
		{
		  cm_msg(MERROR,"frontend_loop","key not found /Runinfo/transition in progress (%d)",status);
		  return (status);
		}
	      //printf("frontend_loop: current run state is %d trans=%d\n",my_run_state,trans);
	      
	      printf("\nfrontend_loop: trans=%d, run states DISAGREE (now %d or %s; previously %d or %s); returning !!! \n",
		     trans,my_run_state,runstring[my_run_state-1],
		     run_state,runstring[run_state-1]);
	      my_run_state =  run_state;
	      
	      return CM_SUCCESS; // do nothing
	    }
	} // debug
#endif
      
      if(send_darc_hdr)
	{
	  printf("frontend_loop: waiting for DARC header bank to be sent out (darc_hdr_sent=%d)\n",
		 darc_hdr_sent);
	  // return CM_SUCCESS;  see if CAMP/EPICS are ready
	}
    
      /* At begin of run ONLY
	 ...send Camp and/or Epics header event(s)  */

#ifdef CAMP_ACCESS      
      if(!gbl_camp_done)
	{   //  camp initialization procedure not done yet; check for timeouts
	  cm_msg(MINFO,"frontend_loop","waiting for Camp initialization");

	  printf("frontend_loop: gbl_camp_done=%d camp_hdr_sent=%d send_camp_hdr=%d \n",
		 gbl_camp_done,camp_hdr_sent, send_camp_hdr);
	  
	  if(At_start_camp_timer > 0)
	    {
	      INT diff;
	      diff= ss_time() - At_start_camp_timer;
	      cm_msg(MINFO,"frontend_loop","waiting for CAMP initialization to finish (diff=%d)",diff);
	      if ( diff > 15 )
		{
		  printf("\n*****  frontend_loop: camp timeout likely after %d sec, gbl_camp_done=%d \n",
			 diff,gbl_camp_done);
		  if(debug)
		    printf("   my_camp_ok=%d (expect 2 or 4 if at_start_run.csh never finished....\n",my_camp_ok);
		  
		  printf("frontend_loop: setting my_camp_ok to 3 to indicate a timeout\n");
		  // write "timeout" to indicate to custom webpage it should display the reset button for CAMP
		  my_camp_ok=3; // timeout
		  write_camp_ok(my_camp_ok); // write this for custom webpage
		  
		  // The CAMP server is probably down... at_start_run.csh never finished
		  //gbl_camp_done = TRUE;
		  camp_settings.camp_ok = 0; // error ; should not fire hotlink
		  first_camp = FALSE;
		  printf("camp_available=%d campInit=%d (expect 0s)\n",
			 camp_available,campInit);
		  if(check_run_state() == -1)
		    {
		       cm_msg(MERROR,"frontend_loop", "run has probably stopped. May need to restart mheader if stuck"); 
                       stop_run();
                       gbl_error_flag=1;
                       return;
                    }
		}
	      
	    }
	  else
	    printf("frontend_loop: At_start_camp_timer is not running\n");

	} // end of gbl_camp_done = FALSE  (may have set now set gbl_camp_done = TRUE)
      
      if(gbl_camp_done && !camp_hdr_sent)
	{ // need to send CAMP header bank
	  if(debug)
	    printf("frontend_loop: gbl_camp_done is true; camp header not yet sent; camp_available=%d\n",
		   camp_available);
	  if(camp_available)
	    { // send camp header bank
	      At_start_camp_timer=-1;
              printf("frontend_loop: setting send_camp_hdr TRUE\n");

	      send_camp_hdr=1;  // set a flag to send camp header event
	      printf("frontend_loop: camp is ready; sending camp header bank (set send_camp_hdr to 1)\n");
	    }
	  else
	    {
	      if(camp_settings.n_var_logged == 0)
		{
		  printf("frontend_loop: ERROR  Shouldn't get this..... no_camp should be true \n");
		  printf("         0 camp variables to be logged; camp header bank will not be sent\n");
		}	     
	      else
		printf("frontend_loop: CAMP system is not available; cannot send camp header bank\n");
	      
	    }
	}
	      
#endif // CAMP
    
      cm_yield(100);

#ifdef EPICS_ACCESS
      if(!gbl_epics_done)
	{   // required header bank(s) have not been sent yet; check for timeouts
	  printf("frontend_loop: gbl_epics_done=%d send_epics_hdr=%d, epics_hdr_sent=%d\n",
		 gbl_epics_done,send_epics_hdr,epics_hdr_sent);
	  
	  if(At_start_epics_timer > 0)
	    {
	      INT diff;
	      diff = ss_time() - At_start_epics_timer ;
	      cm_msg(MINFO,"frontend_loop","waiting for EPICS initialization to finish (diff=%d)",diff);
	      if (diff > 15)
		{
		  printf("frontend_loop: epics timeout likely (after %d sec) gbl_epics_done=%d \n",
			 diff,gbl_epics_done);
		  
		  printf("  epics_ok=%d (expect 2 if init_epics never finished)\n",
			 epicslog_settings.epics_ok);
		  printf("frontend_loop: setting my_epics_ok to 3 to indicate a timeout)\n");
		  
		  my_epics_ok=3; // timeout
		  write_epics_ok(3);
		  epicslog_settings.epics_ok=0; // indicates error... should not fire hotlink
		  //gbl_epics_done = TRUE;
		  printf("epics_available=%d  (expect 0)\n",epics_available);
		  if(check_run_state() == -1)
		    cm_msg(MERROR,"frontend_loop", "run may have stopped. May need to restart mheader if stuck"); 
		
		}
	      
	    }
	  else 
	    printf("frontend_loop: At_start_epics_timer has not been started\n");
	} // end of gbl_epics_done = FALSE 
      
      if(gbl_epics_done && !epics_hdr_sent)
	{ // gbl_epics_done is true
	  printf("frontend_loop: gbl_epics_done is true; send_epics_hdr=%d epics_hdr_sent=%d\n",
		 send_epics_hdr,epics_hdr_sent);
	  
	  if(epics_available) 
	    {
	      send_epics_hdr=1;  // set a flag to send epics header event
	      printf("frontend_loop: epics is ready; setting send_epics_hdr to 1\n");
	    }
	  else
	    {
	      if(epicslog_settings.output.num_log_ioc > 0)
		{
		  printf("frontend_loop: EPICS system is not available; cannot send EPICS header bank\n");
		  // NO EPICS !!!!!
		}
	      else
		{
		  printf("frontend_loop: no EPICS values are selected to be logged - hdr bank not sent\n");
		  no_epics=1; // set no_epics flag as no epics values are selected to be logged
		}
		 
	    }
	}
      
#endif

      cm_yield(100);

      if(gbl_epics_done && gbl_camp_done)
	return CM_SUCCESS;


      if(!gbl_epics_done)
	printf("frontend_loop: still waiting for gbl_epics_done\n");
      if(!gbl_camp_done)
	printf("frontend_loop: still waiting for gbl_camp_done\n");
 

      // catch delayed run start
      if(gbl_run_start_timer - ss_time() > 15 )
	{
	  if(autorun )
	    {
	      gbl_hold=FALSE;
	      cm_msg(MINFO,"frontend_loop","Autorun - timeout waiting for CAMP and/or EPICS headers; Resuming run");
	      printf("frontend_loop: resuming run with darc/camp/epics hdr_sent=%d %d %d\n",
		     darc_hdr_sent,camp_hdr_sent,epics_hdr_sent);
	      set_hold(gbl_hold);
	      //status = set_client_flag("mheader",SUCCESS);
	      return SUCCESS;
	    }
	    

	  /* seems to be a problem starting run due to camp or epics; 
             change message... and keep checking in case say camp gets going but epics doesn't 
             message changes depending whether epics/camp is causing the holdup */
	  printf("frontend_loop: holdup.... gbl_camp,epics_done=%d,%d current_alarm_msg_flag=%d\n",
		 gbl_camp_done,gbl_epics_done,current_alarm_msg_flag);
	  if(!gbl_camp_done && current_alarm_msg_flag != FIX_CAMP)
	    status = set_alarm_message(FIX_CAMP);
	  else if (!gbl_epics_done && current_alarm_msg_flag != FIX_EPICS)
	    status = set_alarm_message(FIX_EPICS);
	  if(status !=SUCCESS)
	    return status;
	}		
    }    
  return CM_SUCCESS;
}


INT set_hold(BOOL hold)
{
  /* this routine
     is called by prestart with hold=1 to set the hold flag before CAMP/EPICS banks are sent
     is called by frontend_loop to release the hold on the run when CAMP/EPICS hdr banks have been sent

     mheader_set_hold is a flag used to indicate the mheader (this routine) is holding the run
     (User may also set and release Hold run).

     hold=1; sets mheader_set_hold, sets sis hold flag, sets alarm message and triggers it; speeds up the
             polling for the header event
     hold=0; clears mheader_set_hold, clears sis hold flag, stops triggering alarm message and resets alarm;
             slows down polling for the header event

  */

  INT flag;
  size=sizeof(hold);

  // Set these prior to writing to hold so hotlink has correct value of the flag
  if(hold)
    mheader_set_hold=TRUE; // flag indicates run set on hold by mheader (not user)
  else
     mheader_set_hold=FALSE; // flag indicates run no longer on hold by mheader (not user)



  status = db_set_value(hDB,0, sisflags_hold_path,  &hold, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      printf("set_hold: error from db_set_value for \"%s\" (%d)",status,sisflags_hold_path);
      if(hold)
	cm_msg(MERROR,"set_hold","cannot set run on Hold (%d)",status);
      else
	cm_msg(MERROR,"set_hold","cannot Continue run (%d)",status);
      return status; // error
    }
    
  if(hold)
    {
      printf("set_hold: run has been set ON HOLD while headers are sent\n");
      cm_msg(MINFO,"set_hold","run has been set ON HOLD while headers are sent");
      status = set_alarm_message(ON_HOLD); // sets mheader alarm trigger
      if(status != SUCCESS)
	return status;

      status = set_poll_fast(TRUE); // set the polling fast while waiting for headers
    }
  else
    {
      printf("set_hold: run has been RESUMED\n");
      status = set_alarm_message(ALL_CLEAR); // clears mheader alarm trigger and resets alarm
      if(status != SUCCESS)
	return status;

      status = set_poll_fast(FALSE); // headers sent or not wanted. Set the polling slow
    }
  return status;
}


INT set_mh_client_flag(INT flag)
{
  /* This routine should be called via set_alarm_message 

     It sets/clears hold_flag_path i.e. "/equipment/fifo_acq/client flags/mh_on_hold"
     If flag>0  set_alarm_message should have has previously set the required message
     Using  flag= current_alarm_msg_flag tells custom page what the problem is


   Setting to >0  will cause alarm system to show the alarm banner with the message
   Setting to 0 will also call reset_mheader_alarm to reset the alarm so the banner will not show
  */


  // don't set this if the run has stopped already!!

  INT temp,size,status;

 
  // if (run_state  != STATE_RUNNING  && flag==1)
  //  {
  //    flag=0;  // clear flags if stopped
  //    printf("set_mh_client_flag: NOT running; clearing flag and alarm banner\n");
  //   }
       
   size=sizeof(flag);
   status = db_set_value(hDB,0, hold_flag_path,  &flag, size, 1, TID_INT);
   if (status != DB_SUCCESS)
     {
       cm_msg(MERROR,"set_mh_client_flag", "error from db_set_value for \"%s\" (%d)",status,hold_flag_path);
       return status; // error
     } 
   
   // read it back
   temp=9;
   size=sizeof(temp);
   status = db_get_value(hDB,0, hold_flag_path,  &temp, &size, TID_INT, FALSE);
   if (status != DB_SUCCESS)
     {
       cm_msg(MERROR,"set_mh_client_flag", "error from db_set_value for \"%s\" (%d)",status,hold_flag_path);
       return status; // error
     } 
   if(temp != flag)
     printf("******* set_mh_client_flag:  Error - flag(%d) and temp(%d)  do not agree !! ******** \n",flag,temp); 
   
   else
     printf("set_mh_client_flag: %s is set to %d \n",hold_flag_path,temp);
   
   if(flag==0)
     status = reset_mheader_alarm(); // remove the alarm banner
   

   return status;

}


INT set_alarm_message(INT flag)
{
  /* This routine sets the alarm message to be displayed for user.
      current_alarm_msg_flag is a global keeping track of which alarm message is written
      it is filled by this routine.

      This routine calles set_mh_client_flag to trigger the alarm or if flag=0 is clears the alarm.

     Sets the alarm message for the user to :


             flag
     ALL_CLEAR 0  no alarm message is displayed; all is well

     ON_HOLD    1  Run on hold waiting for initialization of CAMP/EPICS logging
     FIX_CAMP   2  Fix CAMP then press "ResetCAMPLog" OR press "Continue" to run without CAMP
     FIX_EPICS  3  Fix EPICS and press "ResetEPICSLog" OR press "Continue" to run without EPICS
     RIP        4  mheader started while run in progress; data not saved... STOP run and restart
     NO_LOG     5  running without logging CAMP/EPICS data
     STUCK      6  mheader is stuck waiting for CAMP when run is stopped

===== INPORTANT NOTE: if these values changed, must also change Custom_Status.html  ===========
     Alarm string must be 80 char for alarm structure size (or mismatch)
  */

  INT size80,status,size32;
  static char hold_alarm_msg_path[]="/alarms/alarms/mh_on_hold/alarm message";
  //static char alarm_class_mhdr_fgcol_path[]="/alarms/classes/mheader/Display FGColor";
  static char alarm_class_mhdr_bgcol_path[]="/alarms/classes/mheader/Display BGColor";
  static char *bgcol[]={"white", "green","blue", "navy","red","teal" }; // index 0 no message shown
  //static char *fgcol[]={"black","white","white","white","white","white"}

  char message[128];
  char message80[80]; // message must match alarm structure size
                      // message80,message2 etc must all be 80 long

  /*        flag char*                 bgcol   
     message 1    message_hld(fixed)       green
             2,3  message80 (variable)  blue/navy
             4    message_rip (fixed)      red
             5    message80 (variable)  green
  */

            
  static char message_hld[80]="Run on hold waiting for initialization of CAMP/EPICS logging";
  static char message_rip[80]="mheader started while run in progress; data not saved... STOP run and restart";

  char *p;
  char str[15];
  char bgcolour[32]; // must match alarm structure size;
  //char fgcolour[32];
  
  size32=sizeof(bgcolour);
  size80=sizeof(message80);

  switch (flag)
    {
    case ALL_CLEAR:
      // no alarm; no need to write any message
      current_alarm_msg_flag=flag; // ALL_CLEAR
      status =set_mh_client_flag(flag); // clears alarm and resets so any mheader alarm banner disappears
      return status;

    case ON_HOLD:
      p=message_hld;
      sprintf(bgcolour,"%s",bgcol[flag]);
      break;

    case FIX_CAMP:

      // send a new message
      sprintf(message,"Fix %s & press \"Reset%sLog\" OR press \"Continue\" to run without %s logging",
	      campstr,campstr,campstr);
      
      strncpy(message80,message,size80);
      message80[size80-1]='\0';
      p=message80;
      sprintf(bgcolour,"%s",bgcol[flag]);
      break;


    case FIX_EPICS:      
      sprintf(message,"Fix %s & press \"Reset%sLog\" OR press \"Continue\" to run without %s log",
	      epicsstr,epicsstr,epicsstr);
      
      strncpy(message80,message,size80);
      message80[size80-1]='\0';
      p=message80;
      sprintf(bgcolour,"%s",bgcol[flag]);
      break;

    case RIP:
      p=message_rip;
      sprintf(bgcolour,"%s",bgcol[flag]);
      break;


    case NO_LOG:
      // user took run off HOLD (without fixing logging)
      sprintf(str," ");
      if (!gbl_camp_done && !gbl_epics_done)
	sprintf(message,"Running without logging CAMP & EPICS data");      
      else if (!gbl_camp_done)
      sprintf(message,"Running without logging %s data",campstr);      
      else if (!gbl_epics_done)
      sprintf(message,"Running without logging %s data",epicsstr);      
      else
	{
	  if(no_camp && no_epics)
	  cm_msg(MINFO,"set_alarm_message","user seems to have taken run off hold, no camp or epics values to log");
	  else if (no_camp)
	    cm_msg(MINFO,"set_alarm_message","user seems to have taken run off hold, no camp values to log");
	  else if (no_epics)
	    cm_msg(MINFO,"set_alarm_message","user seems to have taken run off hold, no epics values to log");
	  else
	  /* strange... both camp and epics are being logged after all */
	    cm_msg(MINFO,"set_alarm_message","user seems to have taken run off hold, but camp,epics hdrs both sent");
	  // clear the  alarm; no need to write any message
	  current_alarm_msg_flag=ALL_CLEAR;
	  status =set_mh_client_flag(current_alarm_msg_flag); // clears alarm and resets so any mheader alarm banner disappears
	  return status;
	}      
      strncpy(message80,message,size80);
      message80[size80-1]='\0';
      p=message80;
      sprintf(bgcolour,"%s",bgcol[flag]);
      break;


    default:
      printf("set_alarm_message: illegal value of flag(%d). Cannot set alarm message\n",flag);
      return DB_INVALID_PARAM;
    }

  if(debug)printf("set_alarm_message: flag=%d bgcolour=%s strlen=%d size=%d \n",
	 flag,bgcolour,strlen(bgcolour),sizeof(bgcolour));
  // Set bgcolor
  status = db_set_value(hDB,0,alarm_class_mhdr_bgcol_path,bgcolour,size32,1,TID_STRING);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"set_alarm_message","Error writing \"%s\" to \"%s\"(%d)",
	     bgcolour,alarm_class_mhdr_bgcol_path,status);
      return status;
    }

  // Now set alarm message for the user
 
  if(debug)printf("set_alarm_message: message:\"%s\" size=%d\n",p,size);
  status = db_set_value(hDB,0,hold_alarm_msg_path,p,size80,1,TID_STRING);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"set_alarm_message","Error writing \"%s\" to \"%s\"(%d)",
	     p,hold_alarm_msg_path,status);
      return status;
    }
  current_alarm_msg_flag=flag;
  status =set_mh_client_flag(flag); // triggers alarm
  return status;
}





INT prestart(INT run_number, char *error)
{
  INT size;
  INT period=2000;  /* at BOR 2s period for CVAR,EVAR */

  gbl_run_start_timer=  0;
  

#ifdef MUSR
  /* Get the run type of this new run */
  status = get_musr_type(&fe_flag);
  return status;
#else


  /* Put the frontend on hold if starting a Type 1 run 
     This will prevent banks being sent until header banks are ready
  */

  printf("\n\n ********  prestart: starting run %d **************\n\n\n",run_number);
  send_camp_hdr=camp_hdr_sent=send_epics_hdr=epics_hdr_sent=send_darc_hdr=darc_hdr_sent=FALSE;
  eorflag=no_camp=no_epics=cvar_bank_sent=evar_bank_sent=FALSE;
  At_start_camp_timer=At_start_epics_timer= -1;
    /* Get the run type of this new run */
  status=get_ppg_type();
  if(status!=DB_SUCCESS)
    {
      cm_msg (MERROR,"prestart","Cannot determine run type (Integral or TD)");
      return (status);
    }


      if(ppg_type ==1)
	{
	  /* Set frontend on hold if Type 1 */
	  gbl_hold=TRUE;
	  status = set_hold(gbl_hold); // also sets alarm message and triggers it 
	  if(status!= SUCCESS)
	    return status;

	  
	  size=sizeof(period);
	  status = db_set_value(hDB,0, "/Equipment/Camp/Common/Period",&period, size,1, TID_INT);
	  if (status != DB_SUCCESS)
	      cm_msg(MERROR,"prestart","cannot set /Equipment/Camp/Common/Period to %d (%d)",period,status);
	   
	  status = db_set_value(hDB,0, "/Equipment/Epicslog/Common/Period",&period, size,1, TID_INT);
	  if (status != DB_SUCCESS)
	      cm_msg(MERROR,"prestart","cannot set /Equipment/Epicslog/Common/Period to %d (%d)",period,status);

	  
	  gbl_run_start_timer=ss_time(); 
	}
      else
	{ // Type 2

	  /* clear hold flag in case it was left on (e.g. frontend crashed) */
	  gbl_hold=FALSE;
	  status = set_hold(gbl_hold); // also sets alarm message and triggers it 
	  if(status != SUCCESS)
	    return status;

	  printf("prestart: INFO: TD-type run has been detected; setting client status flag for success & returning\n");
	  //	  status = set_client_flag("mheader",SUCCESS); // set client flag to success
	  return(SUCCESS);
	}

  return SUCCESS;

#endif
}



/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number_local, char *error)
{
    char client_str[128];
    // BOOL client_flag=TRUE; // mdarc sets the client flag for mheader FALSE on a prestart
    char cmd[128];
    BOOL fe_flag;
    INT status; 

    printf(" begin_of_run: starting *****\n");

    write_logging_flag(BOTHFLG,0);
    //gbl_epics_done=gbl_camp_done=FALSE; do not clear these here; at_start_run.csh may have finished already
    // they are cleared post-stop and in frontend_init

    end_run=FALSE; /* make sure flag is cleared */

    Check_last_time = ss_time();

  /* note: run number could have been changed on a prestart by run number checker 
*/  
  run_number=run_number_local;

  if(ppg_type !=1)
    {
       printf("begin_of_run: INFO: TD-type run has been detected; nothing to do\n");
      // status = set_client_flag("mheader",SUCCESS); // set client flag to success
      return(SUCCESS);
    }

  /* Update the whole mdarc record first to reflect any changes   */ 
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"begin_of_run","Failed to retrieve mdarc record  (%d)",status);
      write_message1(status,"begin_of_run");
      return(status);
    }


  /* These are used by darc_get_odb in  mdarc_subs   (BNMR/BNQR only)
     not needed for MUSR
  */
  nHistBins =  fmdarc.histograms.num_bins; //fmdarc set up by setup_hotlink
  nHistBanks = nH =  fmdarc.histograms.number_defined; 
  printf("begin_of_run: Number of bins = %d;  no. defined histograms = %d\n",nHistBins,nH);

  /* CAMP and EPICS may still not start, but run is put on hold  */
  // {
      //  if(debug)printf("\nbegin_of_run: setting client flag for mheader true (success)\n");
      // status = set_client_flag("mheader",SUCCESS);
  // }
 

  if(debug)printf("begin_of_run: Updating epicslog record \n");
  /* update the epicslog record  */
  size = sizeof (epicslog_settings);
  if(debug) printf("hEpics = %d, size of record = %d \n",hEpics,size);
  
  status = db_get_record(hDB,hEpics, &epicslog_settings, &size, 0);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin-of-run", "failed to retrieve Epics record (%d)",status);
      return status;
    }

  // Find out if this is an autorun, in case of camp/epics failure
  size=sizeof(autorun);
  status = db_get_value(hDB, 0, "/Autorun/Enable", &autorun, &size, TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin_of_run", "cannot get value of \"/autorun/enable\" (%d) ",
	     autorun,status);
      autorun=0; // assume false
    }


#ifndef EPICS_ACCESS
  gbl_epics_done=TRUE; // no epics
#endif

#ifndef CAMP_ACCESS
  gbl_camp_done = TRUE;
#endif

  send_darc_hdr = TRUE;  // send out the header event  

  printf("begin_of_run: returning status = %d\n",status);
  return SUCCESS ; /* SUCCESSful return */
}


/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  first_camp  = first_epics = FALSE;
  return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}



/*---Read info for data archiver: BNMR/BNQR  ---------------------------------------------------------------*/
INT header_info_read(char *pevent, INT off)
/*-------------------------------------------------------------*/
{
#ifndef MUSR
  D_ODB *p_odb_data;
  D_ODB odb_data;
#endif
  INT status;

/* for camp bank */
  char str1[512], tempString[80];
  INT len_event,len_darc,len_camp,len_epics; // length of event so far
  INT i, size;
  char *pdata;
  INT len;
  BOOL init_bank;

/*  this structure is in   ../mud/util/trii_fmt.h
    typedef struct {
    INT16 type;
    INT16 len_path;
    char path[50];
    INT16 len_title;
    char title[19];
    INT16 len_units;
    char units[8];
    } IMUSR_CAMP_VAR;
*/
  IMUSR_CAMP_VAR camp_info;

#ifdef EPICS_ACCESS
  IMUSR_CAMP_VAR epics_info;
#endif
 
  char my_string[132];
  CAMP_VAR_TYPE type;
  //INT type;
  char *pstr;

  debug=FALSE;
  if(debug) printf("\nheader_info_read: starting \n");
  if(ppg_type !=1)
    {
      if(debug)printf("header_info_read: type %d run; no action\n",ppg_type);
      return 0;
    }
  if (debug)
    printf("header_info_read: starting with darc,camp,epics hdr send;sent: = %d %d %d; %d %d %d\n",
	 send_darc_hdr, send_camp_hdr, send_epics_hdr,
	 darc_hdr_sent, camp_hdr_sent,  epics_hdr_sent );

  if(!send_darc_hdr && !send_camp_hdr && !send_epics_hdr)
    return 0; // nothing to do 



  /* debug strange error ; this should not happen */
  if( send_darc_hdr &&  darc_hdr_sent)
    cm_msg (MERROR,"header_info_read","error.... send and sent DARC_hdr both TRUE"); 
  if( send_camp_hdr &&  camp_hdr_sent)
    cm_msg (MERROR,"header_info_read","error.... send and sent CAMP_hdr both TRUE"); 
  if( send_epics_hdr &&  epics_hdr_sent)
    cm_msg (MERROR,"header_info_read","error.... send and sent EPICS_hdr both TRUE"); 
  

  init_bank=FALSE;
  len_event=len_darc=len_epics=len_camp=0;

  printf("header_info_read: starting with send_darc_hdr=%d send_camp_hdr=%d send_epics_hdr=%d\n",
	 send_darc_hdr,send_camp_hdr, send_epics_hdr);	 
  //  if(debug) 
  //printf("On entry, pevent = %p\n",pevent);

  /*   DARC HEADER BANK   
   */

  if(send_darc_hdr)
    {  /* send darc header bank  */
      send_darc_hdr = FALSE;

      /* read info into a fixed event */
      p_odb_data = &odb_data;
      /* clear memory area for odb_data */
      size = sizeof(odb_data);
      if(debug)printf("size of odb_data : %d\n",size);
      memset(p_odb_data,0,sizeof(odb_data) );
      
      /*
       *    Read the parameters from odb into the structure
       */
      
      
      
      status = darc_get_odb(p_odb_data);
      if(debug)printf("\n\n ***  header_info_read:   darc_get_odb returns status=%d\n\n",status);

      if (status == DB_SUCCESS)  
	{
	  
	  if(debug)
	    {
	      printf("run number = %d\n",odb_data.run_number);
	      printf("area = %s; rig = %s; mode = %s\n",odb_data.area,odb_data.rig,odb_data.mode);
	    }
	  bk_init(pevent);
	  init_bank=TRUE;
	  
	  /* create DARC bank */
	  bk_create(pevent, "DARC", TID_CHAR, (void**)&pdata);  // cast to  (void **) to avoid warning
	  if(debug) printf("After bk_create, pevent=%p,pdata = %p\n",pevent,pdata);
 
	  size = sizeof(odb_data);
	  if(debug)printf("size of odb_data : %d\n",size);
	  /* now copy the data into the bank */
	  if(debug) printf("Copying data at pointer pdata=%p\n",pdata);
	  memcpy(pdata, (char *) &odb_data, size);
	  pdata += size;
	  bk_close(pevent, pdata);
	  if(debug) printf("closed darc bank, pevent = %p, pdata= %p, bk_size(pevent) = %d\n",pevent, pdata,bk_size(pevent));
	  printf("header_info_read: Darc header bank  (ID=14) has length %d\n",bk_size(pevent)); 
	  len_darc = len_event = bk_size(pevent);  // length of DARC event
	}
      else
	{
	  cm_msg(MERROR,"header_info_read","Failure to get the odb data needed; cannot send DARC bank (%s)",status);
	  
	  goto camp_cont;
	}
    } // end of send_darc_hdr = TRUE 

 camp_cont:

#ifdef CAMP_ACCESS
  if (send_camp_hdr)
    {
      send_camp_hdr = FALSE;      

      /*
        Read camp info for data archiver 
      */
      if(debug)printf("header_info_read: reading CAMP to get header info, len_event=%d\n",len_event);

      if ( !camp_available  || (camp_settings.n_var_logged <= 0) )//  no camp variables defined or camp not OK
	{
	  // this is a error now; we should always have camp values
	  if(camp_settings.n_var_logged <= 0)
	    printf("header_info_read: No camp log variables are defined\n");
	  else
	    printf("header_info_read: Camp is not available\n");
	  goto epics_cont; // may be some EPICS data      
	}
      
      
      if (! campInit)  // just in case...
	{
	  status=reconnect();
	  if (status != CAMP_SUCCESS)
	    {
	      goto epics_cont; // may be some EPICS data
	    }
	}
      
      /* CAMP data should be available */
      size = sizeof(camp_info);
      memset(&camp_info,0,size); // clear structure to start
      
      
      if(debug)
	{
	  if (end_run)printf("INFO - Run is ending\n");
	  printf("Getting an update of camp database...\n");
	}
      
      
      /*
       *    Get an update of the CAMP database
       */
      
      status = camp_clntUpdate();
      if( status != CAMP_SUCCESS )
	{
	  cm_msg(MERROR,"header_info_read:", "failed call to retrieve CAMP data" );
	  /* try to reconnect */    
	  status=reconnect();
	  if (status != CAMP_SUCCESS)
	    {
	      goto epics_cont; // may be some EPICS data
	    }

	  status = camp_clntUpdate();
	  if( status != CAMP_SUCCESS )
	    {
	      cm_msg(MERROR,"header_info_read", "failed call to retrieve CAMP data after reconnect" );
	      campInit=FALSE;
	      goto epics_cont; // may be some EPICS data
	    }
	  if(debug) printf("header_info_read: success from camp_clntUpdate()\n");
	}
      if(debug)printf("Returned from camp_update after reconnect with CAMP_SUCCESS\n");
      
      
      /*
       * create the bank for CAMP 
      */
      if(debug) printf("On entry, pevent = %p\n",pevent);
      if(!init_bank)
	{ 
	  if(debug)printf("header_info_read: calling bk_init for CAMP bank (no header bank)\n");
	  bk_init(pevent); 
	  init_bank=TRUE;
	}
      bk_create(pevent, "CAMP", TID_CHAR, (void **)&pdata);// cast to  (void **) to avoid warning
      if(debug) printf("opened CAMP bank ( pevent=%p, pdata=%p) \n",pevent,pdata);

      /* 
	 now get the camp information for camp_info 
      */
      
      //    MAIN LOOP on each camp variable
      for (i=0;  i < camp_settings.n_var_logged; i++)
	{
	  if(debug) printf("\nLOOP %d, calling campSrv_varGet with path=%s\n",i,camp_settings.var_path[i]);
	  
	  status=campSrv_varGet(camp_settings.var_path[i], CAMP_XDR_NO_CHILD  );
	  if( status != CAMP_SUCCESS )
	    {
	      cm_msg(MINFO,"header_info_read","failed call campSrv_varGet with path=%s (%d) %s",
		     camp_settings.var_path[i], status, camp_getMsg());  	      
	      status = bk_delete(pevent, "CAMP"); /* delete the event */
	      goto epics_cont; // may be some EPICS data
	    }
	  
	  /*  path  */
	  len = sizeof(camp_info.path);
	  strncpy(tempString, camp_settings.var_path[i], len);
	  tempString[len-1] = '\0';
	  trimBlanks( tempString, tempString );
	  strcpy(camp_info.path,tempString);
	  camp_info.len_path=strlen( camp_info.path );
	  
	  /* type */
	  status=camp_varGetType(camp_settings.var_path[i],&type);
	  if( status != CAMP_SUCCESS )
	    {
	      if(status==CAMP_INVAL_VAR) printf("Got CAMP invalid variable type\n");
	      cm_msg(MINFO,"header_info_read","failed call camp_varGetType with path= %s (0x%x)",camp_settings.var_path[i]),status;
	      if (end_run) camp_close();
	      status = bk_delete(pevent, "CAMP"); /* delete the event */
	      goto epics_cont; // may be some EPICS data
	    }
	  camp_info.type=type;
	  
	  /* title */
	  status=camp_varGetTitle (camp_settings.var_path[i],str1, sizeof( str1 ));
	  if( status != CAMP_SUCCESS )
	    {
	      cm_msg(MINFO,"header_info_read","failed call camp_varGetTitle with path= %s (%d)",camp_settings.var_path[i]),status;
	      if (end_run) camp_close();
	      status = bk_delete(pevent, "CAMP"); /* delete the event */
	      goto epics_cont; // may be some EPICS data
	      
	    }
	  len = sizeof(camp_info.title);
	  strncpy( tempString, str1, len );
	  tempString[len-1] = '\0';
	  trimBlanks( tempString, tempString );
	  strcpy( camp_info.title,tempString );
	  camp_info.len_title=strlen( camp_info.title );
	  
	  /* units */
	  status=camp_varNumGetUnits(camp_settings.var_path[i],str1, sizeof( str1 ) );
	  if( status != CAMP_SUCCESS )
	    {
	      cm_msg(MINFO,"header_info_read","failed call camp_varGetUnits with path= %s (%d)",camp_settings.var_path[i]),status;
	      if (end_run) camp_close();
	      status = bk_delete(pevent, "CAMP"); /* delete the event */
	      goto epics_cont; // may be some EPICS data
	    }
	  len = sizeof(camp_info.units);
	  strncpy( tempString, str1, len );
	  tempString[len-1] = '\0';
	  trimBlanks( tempString, tempString );
	  strcpy(camp_info.units,tempString );      
	  camp_info.len_units=strlen( camp_info.units );
	  
	  if(debug)
	    {
	      printf(" -- ********** CAMP *************** -- \n");
	      printf("header_info_read: Values in structure camp_info for loop %d :\n",i);
	      printf("path %s, type %d units %s title %s\n",camp_info.path, camp_info.type,
		     camp_info.units, camp_info.title);
	      printf("len_path %d len_title %d len_units %d\n",camp_info.len_path,
		     camp_info.len_title, camp_info.len_units);
	      printf(" -- ************************* -- \n");
	      
	    }
	  
	  /* copy the structure into the bank */
	  if(debug)printf("Before memcopy for CAMP, pdata = %p\n",pdata);
	  memcpy((char *)pdata, (char *) &camp_info, sizeof(camp_info));
	  pdata += sizeof(camp_info);
	  if(debug) 
	    printf("Size of camp_info=%d copied to bank; pdata=%p \n",sizeof(camp_info),
		 pdata);
	} // end of MAIN LOOP on each CAMP variable
      

      if(debug)
	printf("closing bank CAMP with pevent=%p, pdata=%p\n",pevent,pdata);
      bk_close(pevent,pdata); // close CAMP event
      if(debug) 
	printf("closed camp bank,  pevent = %p, pdata = %p, bank size = %d, event_size = %d\n\n",
	     pevent, pdata, (bk_size(pevent)-len_darc),bk_size(pevent));
      len_event =  bk_size(pevent);
      len_camp = bk_size(pevent)-len_darc;
      printf("size of camp event: %d\n",len_camp);
    } // send_camp_hdr is true  
#endif // CAMP_ACCESS


epics_cont: // continue with EPICS

#ifdef EPICS_ACCESS
  if(send_epics_hdr)
    {
      send_epics_hdr = FALSE;
      if(debug)printf("header_info_read: reading EPICS to get header info, len_darc=%d len_camp=%d len_event=%d\n",
	     len_darc,len_camp,len_event);

      if( (n_epics <= 0) || (epics_available==FALSE))
	{      
	  if(n_epics <= 0)
	    printf("No EPICS log variables are defined\n");
	  else
	    {
	      printf("EPICS is not available\n");
	      goto end_hdr;
	    }
	}
      
      if(!init_bank)
	{
	  if(debug)printf("header_info_read: calling bk_init for EPICS bank (no header or camp banks)\n");
	  bk_init(pevent);
	  init_bank=TRUE;
	}

      // create a bank for EPICS info ; send it even if number of epics is 0
      bk_create(pevent, "EPICS", TID_CHAR,  (void **)&pdata);// cast to (void **) to avoid warning
      if(debug) 
	printf("opened EPICS bank ( pevent=%p, pdata=%p) \n",pevent,pdata);
  

      size = sizeof(epics_info);
      memset(&epics_info,0,size); // clear structure to start

      for (i=0; i<n_epics; i++)
	{
	  
	  /*  path */
	  len = sizeof(epics_info.path);
	  strncpy(tempString, epics_log[i].Name, len);
	  tempString[len-1] = '\0';
	  trimBlanks( tempString, tempString );
	  strcpy( epics_info.path, tempString );
	  epics_info.len_path=strlen( camp_info.path ); //// MUST BE CAMP_INFO.PATH !!
	  
	  /* type */
	  epics_info.type = CAMP_VAR_TYPE_FLOAT;  // they are all float
	  	  
	  /* title */
	  len = sizeof(epics_info.title);
	  strncpy(tempString, epics_params[epics_log[i].index].Title, len);
	  tempString[len-1] = '\0';
	  trimBlanks( tempString, tempString );
	  strcpy( epics_info.title, tempString );
	  epics_info.len_title=strlen( epics_info.title );
	  
	  /* units */    
	  len = sizeof(epics_info.units);
	  strncpy(tempString, epics_params[epics_log[i].index].Units, len);
	  tempString[len-1] = '\0';
	  trimBlanks( tempString, tempString );
	  strcpy( epics_info.units, tempString );
	  epics_info.len_units=strlen( epics_info.units );
	  
	  if(debug)
	    {
	      printf(" -- ************ EPICS ************* -- \n");
	      printf("header_info_read: Values in structure epics_info for loop %d :\n",i);
	      printf("path %s, type %d units %s title %s\n",epics_info.path, epics_info.type,
		     epics_info.units, epics_info.title);
	      printf("len_path %d len_title %d len_units %d\n",epics_info.len_path,
		     epics_info.len_title, epics_info.len_units);
	      printf(" -- ************************* -- \n");
	      
	    }
	  
	  /* copy the structure into the CAMP bank */
	  if(debug)printf("Before memcopy for EPICS names, pdata = %p\n",pdata);
	  memcpy((char *)pdata, (char *) &epics_info, sizeof(epics_info));
	  pdata += sizeof(epics_info);
	  if(debug) printf("Size of epics_info=%d copied to bank; pdata=%p \n",sizeof(epics_info),
			   pdata);
      
	} // end of MAIN LOOP on each EPICS variable


      if(debug)
	printf("closing bank EPICS with pevent=%p, pdata=%p\n",pevent,pdata);
      bk_close(pevent, pdata );
  
      len_epics =  (bk_size(pevent)-(len_camp+len_darc));
      if(debug) 
	printf("closed epics bank,  pevent = %p, pdata = %p, len_epics = %d, event_size = %d\n\n",
	     pevent, pdata, (bk_size(pevent)-(len_camp+len_darc)),bk_size(pevent));
      len_event = bk_size(pevent);
      if(debug)
	printf("header_info_read: len_camp=%d len_darc=%d len_epics=%d  len_event=%d\n",
	       len_camp,len_darc,len_epics, len_event);
    } // send_epics_hdr is true  
#endif  // EPICS

end_hdr:
  if(debug)
    printf("header_info_read: at end_hdr; returning (len_darc=%d, len_camp=%d  len_epics=%d len_event=%d)\n",
	 len_darc,len_camp,len_epics,len_event);
  
  sprintf(my_string,"header_info_read: sent ");

  char my_str[32];

  if(len_camp > 0)
    {
      camp_hdr_sent=TRUE;

      if(debug)printf("\n\n header_info_read:  clearing client code for camp\n\n");

      write_client_code(CAMP_LOG_ERR,CLEAR,"mheader"); // clear bit; CAMP is being sent
      write_logging_flag(CAMPFLG,1);
      sprintf(my_str,"bank CAMP (length %d) ",len_camp);
      strcat(my_string,my_str);
    }

  if(len_darc > 0)
    {
      darc_hdr_sent=TRUE;
      sprintf(my_str,"bank DARC (length %d) ",len_darc);
      strcat(my_string,my_str);
    }

  if(len_epics > 0)
    {
      epics_hdr_sent=TRUE;
      if(debug)printf("\n\n header_info_read:  clearing client code for Epics\n\n");

      write_client_code(EPICS_LOG_ERR,CLEAR,"mheader"); // clear bit; EPICS is being sent
      write_logging_flag(EPICSFLG,1);
      sprintf(my_str,"bank EPICS (length %d) ",len_epics);
      strcat(my_string,my_str);
    }

  if (len_event == 0)
    strcat(my_string,"no header bank ");
 
  cm_msg(MINFO,"mheader","%s; event length=%d",my_string,len_event);
  return len_event; 
}

/*------------------- camp_close ------------------*/
void 
camp_close(void)
/* ----------------------------------------------*/
{
#ifdef CAMP_ACCESS
     /* close the camp port when run stops */
  if(debug) printf("camp_close: starting\n");
  if (camp_available)
    {
      
      if(campInit)
	{
	  if(debug)printf("camp_close : calling camp_clntEnd()\n");
	  camp_clntEnd();  
	}
      else
	if(debug) printf("camp_close: INFO: camp connection is already closed. \n");
    }
  else
    if(debug) printf("camp_close: INFO: camp connection was never opened \n");
  
  campInit = FALSE;
  end_run = FALSE;
#endif
} 


/*---Read Camp variables ---------------------------------------------------------------*/
/* read camp variables into a variable length float event*/
INT camp_var_read(char *pevent, INT off)
{
  double *pdata;
  INT i,size, status, nerror;
  int j=0;
  INT  error_flag,len_camp;
  double camp_data;
  BOOL bki, cvar_bank;
  
  len_camp = 0;
  bki =FALSE;

  
#ifndef CAMP_ACCESS
  return 0;
#else

  if(debug) 
    printf("camp_var_read: starting \n");
  /* read camp into a fixed event */
  
  if(ppg_type !=1)
    {
      if(debug)printf("camp_var_read; TD-type, no action\n");
      return 0;
    }
  
  if (!camp_hdr_sent)
    {
      printf("camp_var_read: header bank (CAMP) not yet sent; CVAR event cannot be sent yet\n");
      return 0;
    }

  if(gbl_hold)
    { // may be waiting for EPICS bank or user may have put run on HOLD
      printf("camp_var_read: run is on HOLD; not sending CVAR bank\n");
      return 0;
    }

  
  if(debug)printf("camp_var_read: camp_ok = %d, camp_available=%d campInit=%d\n",
	 camp_settings.camp_ok, camp_available, campInit);
  
  if(camp_settings.camp_ok != 1)
    {
      if(debug)printf("camp_var_read: no Camp available; camp_ok != 1\n");
      return 0; // no camp available
    }
      
  if(!camp_available) // handles case of no camp variables defined
    {
      if(debug)printf("camp_var_read: No camp is available or no camp variables have been defined\n");
      return 0; // no bank to send
    }
  
  if(!campInit)
    {
      status=reconnect();
      if (status != CAMP_SUCCESS)
	return 0;
    }
  
  
  if(debug) printf("On entry, pevent = %p\n",pevent);
  bk_init(pevent);
  if(debug)printf("camp_var_read: calling bk_init for CVAR\n");
  bki=TRUE;
  bk_create(pevent, "CVAR", TID_DOUBLE,  (void **)&pdata);// cast to (void **) to avoid warning
  cvar_bank=TRUE;
  
  /*
   *    Get an update of the CAMP database
   */
  
  if(debug) printf("camp_var_read: Calling camp_clntUpdate()\n");
  status = camp_clntUpdate();
  if( status != CAMP_SUCCESS )
    {
      cm_msg(MERROR,"camp_var_read", "failed call to retrieve CAMP data" );
      
      /* try to reconnect */
      status=reconnect();
      if (status != CAMP_SUCCESS)
	{
	  status = bk_delete(pevent, "CVAR"); /* delete the event */
	  return 0; // cancel the event
	}
      status = camp_clntUpdate();
      if( status != CAMP_SUCCESS )
	{
	  cm_msg(MERROR,"camp_var_read", "failed call to retrieve CAMP data after reconnect" );
	  campInit=FALSE;     
	  status = bk_delete(pevent, "CVAR"); /* delete the event */
	  return 0; /* cancels the event */
	}
      if(debug)printf("Returned from camp_update after reconnect with CAMP_SUCCESS\n");
      
    }
  nerror=0; /* initialize error count */
  error_flag=FALSE; /* and error flag */
  
  for (i=0;  i < camp_settings.n_var_logged; i++)
    {
      //      printf("calling campSrv_varGet with path=%s\n",camp_settings.var_path[i]);

      status=campSrv_varGet(camp_settings.var_path[i], CAMP_XDR_NO_CHILD);
      if( status != CAMP_SUCCESS )
	{

	  cm_msg(MINFO,"camp_var_read","failed call campSrv_VarGet with path= %s (%d) %s",
		 camp_settings.var_path[i],status,camp_getMsg());

	  /* handle this by setting value to zero  */
	  error_flag = TRUE;
	  nerror++;
	}
      else
	{
	  //     printf("calling camp_varNumGetVal with path=%s\n",camp_settings.var_path[i]);
	  
	  status=camp_varNumGetVal(camp_settings.var_path[i], &camp_data);
	  if( status != CAMP_SUCCESS )
	    {
	      cm_msg(MINFO,"camp_var_read","failed call camp_VarNumGetValue with path= %s (%d)",camp_settings.var_path[i], status);
	      error_flag = TRUE;
	      nerror++;
	    }
	  if(debug)printf("Value read from camp path %s  : %f (index=%d)\n", camp_settings.var_path[i], camp_data, i);
	  if (error_flag)
	    *pdata++ = 0.0 ; /* could not read value */
	  else
	    *pdata++ = ( (double) camp_data);
	  error_flag=FALSE;
	}
      if(nerror > 3)
	{
	  status = bk_delete(pevent, "CVAR"); /* delete the event */
	  cm_msg(MINFO,"camp_var_read","too many errors reading camp (%d)",nerror);
	  return 0; /* cancels the event */
	}
    }

  bk_close(pevent, pdata);
  if(debug) printf("closed cvar bank, pevent = %p, pdata= %p, bk_size(pevent) = %d\n",
		   pevent, pdata,bk_size(pevent));

  printf("camp_var_read: CVAR bank sent of length %d\n",bk_size(pevent));

    
  if (!cvar_bank_sent)
    { /* this is the first CVAR bank to be sent */
      cvar_bank_sent = TRUE;
      status = db_get_value(hDB,0, "/Equipment/camp/settings/cvar event period (s)", &i, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MINFO,"camp_var_read","cannot read \"/Equipment/camp/settings/cvar event period (s)\" (%d)",status);
	  i = POLL_CVAR; /* use default value */
	}
      else
	{
	  if(i==0)
	    i=POLL_CVAR;
	  else
	    i*=1000; /* convert sec to ms */
	}
      size=sizeof(i);
      status = db_set_value(hDB,0, "/Equipment/Camp/Common/Period",&i, size,1, TID_INT);

	if (status != DB_SUCCESS)
	  cm_msg(MERROR,"camp_var_read","cannot set /Equipment/Camp/Common/Period to %d (%d)",i,status);
    }
     
  return (bk_size(pevent));
    
#endif // CAMP_ACCESS
}


/*---Read Epics variables ---------------------------------------------------------------*/
/* read epics variables into a variable length float event*/
INT epics_var_read(char *pevent, INT off)
{
  double *pdata;
  INT i,nerror,size,status;
  int j=0;
  INT  error_flag;
  double epics_data;
  BOOL  evar_bank;
  

  
#ifndef EPICS_ACCESS
  return 0;
#endif

  if(debug) 
    printf("epics_var_read: starting \n");

  
  if(ppg_type !=1)
    {
      if(debug)printf("epics_var_read; TD-type, no action\n");
      return 0;
    }
  
  if (!epics_hdr_sent)
    {
      printf("epics_var_read: Epics header bank not yet sent; EVAR event cannot be sent yet\n");
      return 0;
    }

  if(gbl_hold)
    { // may be waiting for CAMP bank or user may have run on hold
      printf("epics_var_read: run is on HOLD; not sending EVAR bank\n");
      return 0;
    }



  if(debug) printf("epics_var_read: EPICS ACCESS is defined\n");

  if( epicslog_settings.epics_ok == 0)
    {
      printf("epics_var_read: EPICS is not available as epics_ok is 0 \n");
      return 0; // no EVAR bank   
    }

  if(!epics_available || n_epics <= 0) 
    {
      if(n_epics<=0)
	printf("epics_var_read: no EPICS variables are selected to be logged\n");
      else
	printf("epics_var_read: EPICS is not available\n");

      return 0; // no EVAR bank   
    }

  if(debug)
    printf("epics_var_read: calling bk_init\n");
  bk_init(pevent);
  bk_create(pevent, "EVAR", TID_DOUBLE,  (void **)&pdata); // cast to (void **) to avoid warning
  evar_bank = TRUE;

  if(debug)
    printf("epics_var_read: created EVAR bank and filling with %d EPICS values\n", 
	 n_epics);
    
  nerror=0; /* initialize error count */
   
  
  for (i=0; i<n_epics; i++)
    {
      status = read_epics_value(i,FALSE); // no statistics
      if(status != SUCCESS)
	{
	  *pdata++ = 0.0; /* could not read value */
	  nerror++;
	}
      else
	{
	  *pdata++ = (double)  epics_log[i].value;
	  if(debug)printf("epics_var_read:  i=%d  added %f to EVAR bank\n",i, epics_log[i].value);
	}
    }
  
  bk_close(pevent, pdata);
  if(debug) 
    printf("epics_var_read: closed EVAR bank,  pevent = %p, pdata = %p, bank size = %d\n",
	   pevent, pdata, bk_size(pevent));
  
  printf("epics_var_read: EVAR bank sent of length %d  \n", 
	 bk_size(pevent) );

  if (!evar_bank_sent)
    { /* this is the first EVAR bank to be sent */
      evar_bank_sent = TRUE;
      status = db_get_value(hDB,0, "/Equipment/epicslog/settings/evar event period (s)", &i, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"epics_var_read","cannot read \"/Equipment/epicslog/settings/evar event period (s)\" (%d)",status);
	  i = POLL_EVAR; /* use default value */
	}
      else
	{
	  if(i==0)
	    i=POLL_EVAR;
	  else
	    i*=1000; /* convert sec to ms */
	}

      size=sizeof(i);
      status = db_set_value(hDB,0, "/Equipment/Epicslog/Common/Period",&i, size,1, TID_INT);

	if (status != DB_SUCCESS)
	  cm_msg(MERROR,"epics_var_read","cannot set /Equipment/Epicslog/Common/Period to %d (%d)",i,status);

    }
  return  bk_size(pevent);
}


#ifdef CAMP_ACCESS
INT reconnect(void)
{
  INT i;
  /* try to reconnect to CAMP */
  camp_close(); // closes connection if campInit and camp_available are both true.
  
  if (debug) printf("reconnect: trying to reconnect to camp... calling camp_clntInit \n");
  status = camp_clntInit( serverName, 10 );

  if(status == CAMP_SUCCESS)
    {
      if(debug)printf("Returned from camp_clntInit with CAMP_SUCCESS\n");
      campInit=TRUE;
    }
  else
    {
      if(debug)printf("reconnect: failure from camp_clntInit\n");
      cm_msg(MERROR,"reconnect","Cannot reconnect to CAMP\n");
    }
  return status;
}
#endif // CAMP_ACCESS

#ifdef MUSR

// #ifdef GONE  // Will be able to stop the run directly

/* NOTE: this is also in mdarc_subs. Include a copy here because MUSR (unlike BNMR) does
   not include mdarc_subs in this file (darc_write_odb is not wanted here */
/*------------------------------------------------------------------*/
INT set_client_flag(char *client_name, BOOL value)
/*------------------------------------------------------------------*/
{
  /* set the flag in /equipment/fifo_acq/client flags/mheader to indicate failure from mheader
     set the flag in /equipment/fifo_acq/client flags/client alarm to get browser mhttpd to put up an alarm banner

     Note that an almost identical routine is in febnmr.c for use of VxWorks frontend - later can try to just have 
     one routine with ifdefs 
  */
  char client_str[128];
  INT client_flag;
  BOOL my_value;
  INT status;
  if(debug)printf("set_client_flag: starting\n");

  my_value = value;
  sprintf(client_str,"/equipment/%s/client flags/%s",eqp_name,client_name );
  if(debug)
    printf("set_client_flag: setting client flag for client %s to %d\n",client_name,my_value); 

  /* Set the client flag to TRUE (success) */
  size = sizeof(my_value);
  status = db_set_value(hDB, 0, client_str, &my_value, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client status flag at path \"%s\" to %d (%d) ",
	     client_str,my_value,status);
      return status;
    }


  /* Set the alarm flag; TRUE - alarm should go off, if FALSE,  alarm stays off
     Note that in odb, 
        /alarm/alarms/client alarm/condition is set to "/equipment/fifo_acq/client flags/client alarm >0"

  */
  size = sizeof(client_flag);
  if(value) 
    client_flag = 0;
  else
    client_flag = 1;

  sprintf(client_str,"/equipment/%s/client flags/client alarm",eqp_name );
  if(debug)
    printf("set_client_flag: setting alarm flag to %d\n",client_flag); 

  size = sizeof(client_flag);
  status = db_set_value(hDB, 0, client_str, &client_flag, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }
  return CM_SUCCESS ;
}
// #endif // GONE
#else
/*---------------------------------------------------------------------------*/
INT get_ppg_type(void)
/*---------------------------------------------------------------------------*/
{
  /*  get experiment type for BNMR/BNQR
        Type 1 = Integral   Type 2 = TD
  */

  char str[128];
  INT j;
  char experiment_name[32]; // size must be same as in odb: ../frontend/input/experiment name 

  /* fills globals ppg_mode : a string e.g. 1a,2c etc   MUSR dummy -> "20"
                   ppg_type : integer 1 or 2  for type1 or type2 MUSR -> 2
  */

  /* Get the beamline */


  strcpy(beamline,expt_name);  
  

  for (j=0; j<strlen(beamline); j++)
    {
      beamline[j] = toupper (beamline[j]); /* convert to upper case */
      lc_beamline[j] = tolower (beamline[j]); /* and lower case */
    }
  if(debug) printf(" beamline:  %s\n",beamline);
  
  if (strlen(beamline) <= 0 )
    {
      printf("No valid beamline is supplied\n");
      return (DB_INVALID_PARAM);
    }

  if (strncmp(beamline,"BN",2)==0)  // BNMR or BNQR
    {    
      sprintf(str,"/equipment/%s/frontend/input/experiment name",eqp_name); // get ppg mode (e.g. 1f etc. )
      size = sizeof(experiment_name);
      status = db_get_value(hDB, 0, str, &experiment_name, &size, TID_STRING, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_ppg_type",
		 "could not get value for \"%s\" (%d)",str,status);
	  return DB_INVALID_PARAM;
	}
      strncpy(ppg_mode,experiment_name,2);

      if (strncmp(ppg_mode,"1",1)==0)
	{
	  printf("get_ppg_type: %s detected experiment type 1\n",beamline);
	  ppg_type=1;
	}
      else if (strncmp(ppg_mode,"2",1)==0)
	{
	  printf("get_ppg_type: %s detected experiment type 2\n",beamline);
	  ppg_type = 2; 
	}
      else
	{
	  cm_msg(MERROR,"get_ppg_type",
		 "unknown ppg mode \"%s\" (%d)",ppg_mode,status);
	  return DB_INVALID_PARAM;
	}
	       
    } // end of  BNMR/BNQR
  else
    {
      cm_msg(MERROR,"get_ppg_type",
	     "invalid beamline \"%s\" (%d)",beamline,status);
      return DB_INVALID_PARAM;


    }
  return(status);
}

#endif

#ifdef CAMP_ACCESS
INT init_camp(void)
{
  INT i,j,status;


  // see if at_start_run.csh has set up/checked the CAMP paths
#ifndef MUSR  // BNMR/BNQR only

  if(camp_settings.camp_ok != 1) // 1=SUCCESS
    {
      cm_msg(MINFO, "init_camp", "at_start_run script has not successfully set camp up");
      camp_available = campInit = FALSE; /* make sure these flags are false */ 
      if(debug)printf("init_camp: camp_ok=%d, returning with camp_available and campInit false\n",
	   camp_settings.camp_ok  );
      return DB_INVALID_PARAM;
    } 
#endif

  /* Check there is a valid number of camp variables to be logged */
  if (camp_settings.n_var_logged <= 0)
    {
      cm_msg(MINFO, "init_camp", "No camp variables are selected to be logged in /equipment/camp/settings");
      printf("init_camp: No camp variables are selected to be logged in /equipment/camp/settings\n");
      camp_available = campInit = FALSE; /* make sure these flags are false */
      if(debug)printf("init_camp: camp_ok=%d, returning with camp_available and campInit false\n",
	     camp_settings.camp_ok  );
      no_camp=TRUE; // no camp is needed for this run. Send no banks; midbnmr_darc will use a default.
      return SUCCESS;
    }
        
  if(debug)
    {
      printf("init_camp: number of camp values = %d\n",camp_settings.n_var_logged);
      for (j=0; j< camp_settings.n_var_logged; j++)
	printf("init_camp: camp path  = %s\n",camp_settings.var_path[j]);
    }
      
  /* we don't need to repeat this checking for BNMR since at_start_run.csh checked all this */

      
  /* Check there is a valid camp hostname */
  check_camp(); // sets camp_available
  if (!camp_available)
    {  cm_msg(MERROR,"init_camp","Camp hostname %s is invalid; no camp data can be saved "
	      ,serverName);
     printf("init_camp: invalid camp hostname, camp_ok=%d, returning with camp_available=%d and campInit =%d\n",
	   camp_available, campInit, camp_settings.camp_ok  );
    return DB_INVALID_PARAM; // no longer fatal; can run without camp
    }

#ifdef MUSR
  /* We have a valid CAMP hostname - but is it active?  
     ( ping the camp host because if someone turns off the crate, there is
     no timeout and it can waste a lot of time)  */ 
  status = ping_camp();
  if (status == DB_NO_ACCESS)
    {
      cm_msg(MERROR,"init_camp","Camp host %s is unreachable. Camp data cannot be saved",
	     serverName);
      camp_available=FALSE;
     printf("init_camp: no ping; camp_ok=%d, returning with camp_available false, campInit %d\n",
	   camp_settings.camp_ok,campInit  );
      return status; // no longer fatal; can run without camp
    }
  
  if (debug) printf ("Camp host is responding to ping\n");
  
  /* now execute perlscript to check the camp paths, write the polling intervals */
  if(debug)
    {
      printf ("init_camp: Perl script is %s\n", perl_script);
      printf ("init_camp: Perl path is   %s\n", perl_path);
    }

  sprintf(perl_cmd,"%s %s %s %s %s",perl_script,perl_path,expt_name,eqp_name,lc_beamline); 
  if(debug)
    {
      printf("init_camp: Executing perl script to check camp paths for logged variables...\n");
	  
      printf("init_camp: Sending system command  cmd: %s\n",perl_cmd);
    }
  status =  system(perl_cmd);  /* now open camp connection */
  if (status)
    {
      printf (" ========================================================================================\n");
      cm_msg (MERROR,"init_camp","Perl script %s returns failure status (%d)",perl_script,status);
      cm_msg (MINFO,"init_camp","Check information file \"/home1/midas/musr/log/%s/camp.txt\" for details",expt_name);
      
      /* Note:
	 if no recent additions to log file, there may be compilation errors in the perl script;check mheader window");
      */

      cm_msg (MERROR,"init_camp","Stop run, fix error in camp logged variables, then restart run");
      camp_available = FALSE;
     if(debug)printf("init_camp: perlscript camp_ok=%d, returning with camp_available false, campInit %d \n",
	   camp_settings.camp_ok,campInit  );
      return (DB_INVALID_PARAM); // bad status from perl script; now not a fatal error!
    }

  if(debug)printf("init_camp : Success from perl script to check camp paths\n");
	  
#endif // end of checking done by at_start_run.csh for bnmr
	  
  /* Initialize CAMP connection */
	  
	
  if (!campInit) 
    {   // camp_clntInit not yet called; camp port is not open
      if (debug) printf("init_camp: calling camp_clntInit \n");
      status = camp_clntInit( serverName, 10 );
      if(status == CAMP_SUCCESS)
	{
	  campInit = TRUE;
	  if(debug)printf("Returned from camp_clntInit with CAMP_SUCCESS\n");
	}
      else
	{
	  cm_msg(MERROR,"init_camp","Failed initialize call to CAMP (called because run is in progress)");
	  if(debug) printf("init_camp: Returned from camp_clntInit with CAMP_FAILURE\n");
	  camp_available = FALSE;
     if(debug)printf("init_camp: camp_clntInit camp_ok=%d, returning with camp_available false, campInit %d \n",
	   camp_settings.camp_ok,campInit  );
	  return DB_INVALID_PARAM; // no longer fatal; can run without camp
	}
    }
  else
    cm_msg(MINFO,"init_camp","Camp port is already open. It will NOT  be closed and re-opened");

  printf("init_camp: CAMP has been successfully initialized\n");
  if(debug)printf("init_camp: SUCCESS camp_ok=%d, returning with camp_available %d, campInit %d \n",
	 camp_settings.camp_ok,camp_available,campInit  );
  return SUCCESS;
}
#endif

#include "write_client_code.c"

  
/*
void catch_sigalrm(int signo)
{
  printf("Timeout signal alarm caught !\n");
  timeout=TRUE;
  return;
}
*/

INT open_hold_hot_link(void)
{
  char str[15];
  INT status;

  // BNMR/BNQR set up a hotlink on "hold" 
  

  if (hHold==0)
    {
      size = sizeof(gbl_hold);
      status = db_find_key(hDB, hHold, sisflags_hold_path, &hHold);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MINFO,"open_hold_hot_link","find key on \"%s\" failed (%d)",sisflags_hold_path,status);
	  return status;
	}
    }
  if (hHold==0)
    {
      cm_msg(MERROR,"open_hold_hot_link","key hHold is zero");
      return DB_INVALID_PARAM;
    }

  status = db_open_record(hDB, hHold, &gbl_hold
			  , size
			  , MODE_READ, hot_hold, "hold touched");
  if (status != DB_SUCCESS) 
    cm_msg(MINFO,"open_hold_hot_link","open record on \"%s\"  failed (%d)",sisflags_hold_path,status);

  return status;
}
/*-- Call_back  ----------------------------------------------------*/
void hot_runstate(HNDLE hDB, HNDLE hkey, void * info)
{
  if(run_state== STATE_STOPPED)
    printf("\n** HOT_RUNSTATE: run is now STOPPED\n");
  else if (run_state== STATE_RUNNING)
    printf("\n** HOT_RUNSTATE: run is now RUNNING\n");
  return;
}

/*-- Call_back  ----------------------------------------------------*/
void hot_hold(HNDLE hDB, HNDLE hkey, void * info)
{
  INT flag,temp;
  printf("\n****  hot_hold: touched hotlink hold; gbl_hold=%d ****\n",gbl_hold);

  if(!gbl_hold)
    { 
      if(mheader_set_hold) // User has resumed run  (CAMP/EPICS  hors de combat)
	{
	  cm_msg(MINFO,"hot_hold","user resumes run; clearing mheader_set_hold flag");
	  mheader_set_hold=FALSE; // mheader is no longer holding up the run
	  // Send alarm warning about running without CAMP/EPICS logging
	  set_alarm_message(NO_LOG);
	  /* set the header event polling way slower */
	  status = set_poll_fast(FALSE);
	}
    }    
  return;
}

/*--Set Poll Interval -------------------------------------------------------------------- */

INT set_poll_fast(BOOL flag)
{
  /* flag = TRUE  poll fast
            FALSE poll slowly
  */
  char header_poll_path[]="/equipment/header/common/period"; // change the polling interval of this event
  INT fast_poll=5000; // 5s
  INT slow_poll=60000; // 1 min
  INT temp,status,size,setval;

  size=sizeof(temp);

  status = db_get_value(hDB, 0, header_poll_path, &temp, &size, TID_INT,FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_poll", "cannot get polling interval at path \"%s\" (%d) ",
	     header_poll_path,status);
      return status;
    }
  printf("set_poll: polling interval is %d\n",temp);

  if(flag)
    setval=fast_poll;
  else
    setval=slow_poll;

  if(temp != setval)
    status = db_set_value(hDB, 0, header_poll_path, &setval, sizeof(setval), 1,TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_poll", "cannot set polling interval at path \"%s\" (%d) ",
	     header_poll_path,status);
      return status;
    }
  else
    printf("set_poll: polling interval is now set to %d\n",setval);
  return status;
}

/*----Reset mheader alarm------------------------------------------------------------------*/

INT reset_mheader_alarm(void)
{
  // called from set_mh_client_flag when alarm is cleared
  INT i=0;
  char str[]="/alarms/alarms/mh_on_hold/triggered";

  status = db_set_value(hDB,0,str,&i,sizeof(i),1,TID_INT);
  if(status != SUCCESS)
    cm_msg(MERROR,"reset_mheader_alarm","error setting \"%s\" to %d (%d)",str,i,status);
  return status;
}

/*---Open epics hotlink ------------------------------------------------------------------*/

#ifdef EPICS_ACCESS
INT open_epics_hot_link(void)
{
  char str[15];
  INT status;

  // BNMR/BNQR set up a hotlink on "epics ok" AFTER BOR
  if (hEpics==0)
    return DB_INVALID_PARAM;

  if (hEpicsOK==0)
    {
      size = sizeof(my_epics_ok);
      sprintf(str,"epics OK");
      status = db_find_key(hDB, hEpics, str, &hEpicsOK);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MINFO,"open_epics_hot_link","find key on \"%s\" failed (%d)",str,status);
	  return status;
	}
    }

  if (hEpicsOK==0)
    {
      cm_msg(MERROR,"open_epics_hot_link","key hEpicsOK is zero");
      return DB_INVALID_PARAM;
    }

  status = db_open_record(hDB, hEpicsOK, &my_epics_ok
			  , size
			  , MODE_READ, hot_epics, "epics_OK touched");
  if (status != DB_SUCCESS) 
    cm_msg(MINFO,"open_epics_hot_link","open record on \"%s\"  failed (%d)",str,status);
  
  return status;
}

/*-- Call_back  ----------------------------------------------------*/
void hot_epics(HNDLE hDB, HNDLE hkey, void * info)
{
  /* hot link on epics_ok
     epics_ok = 0  FAILURE  
                1  SUCCESS  
		2  IN PROGRESS 
		3  TIMEOUT set by frontend_loop
	// not supported	4  REDO EPICS (set by Custom webpage)
     these values are the same as camp_ok
  */
  INT trans,status,size;

  printf("\n** HOT_EPICS: hot link touched; hkey= %d \"%s\" \n",hkey,info);

  if(my_epics_ok >= 0 && my_epics_ok <= 4)
    {
      printf("Previously epics_ok was %d (%s); now %d (%s)\n",  
	     old_epics_ok, okstring[old_epics_ok],
	     my_epics_ok, okstring[my_epics_ok]);
      old_epics_ok = my_epics_ok;
    }
  else
    {
      printf("Invalid value of my_epics_ok (%d)\n",my_epics_ok);
      my_epics_ok = 5; // illegal
    }

  if(my_epics_ok==3)
    {
      if(debug)printf("hot_epics: timeout!\n");
      goto cont4;
    }

  if(old_epics_ok == 3 && my_epics_ok == 1)
    {
      printf("hot_epics: epics timed out, but has finally returned success\n");
      goto cont4;
    }
  if(ppg_type != 1)
    {
      printf("hot_epics: type 2 run is in progress; no further action\n");
      goto cont4;
    }

 if(first_epics)
    {
      if(my_epics_ok == 2 || my_epics_ok == 4 )
	{
	  printf("hot_epics: may be multiple versions of at_start_run.csh; no action\n");
	  goto cont4;
	}

      printf("hot_epics: at_start_run has ended; my_epics_ok=%d (%s)\n",my_epics_ok,okstring[my_epics_ok]);
      first_epics=FALSE;
      if(my_epics_ok == 0)
	  printf("hot_epics: failure from at_start_run.csh\n");
      else
	{
	  printf("hot_epics: Calling redo_epics \n");
	  redo_epics();
	}
      printf("hot_epics: epics_settings.camp_ok=%d epics_available=%d \n",
	     epicslog_settings.epics_ok,	 epics_available);
    } // first_epics

  else if(my_epics_ok == 2  || my_epics_ok == 4)
    {
      gbl_epics_done = FALSE;
      // look for a run start transition

      /* get the run state to see if run is going */
      size = sizeof(run_state);
      status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_epics","key not found /Runinfo/State (%d)",status);
	  goto cont4;
	}
      size = sizeof(trans);
      status = db_get_value(hDB, 0, "/Runinfo/transition in progress", &trans, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_epics","key not found /Runinfo/transition in progress (%d)",status);
	  goto cont4;
	}

      printf("hot_epics: trans=%d run_state=%d\n",trans,run_state); 

      if(trans==1 && run_state==STATE_STOPPED && my_epics_ok==2)
	printf("hot_epics: at run start\n");
	// these cases are no longer allowed. Cannot send banks after run start
	//      else if(trans==0 && run_state==STATE_RUNNING && my_epics_ok == 4)
	//printf("hot_epics: redoing epics (requested by webpage)\n");
	//else if (trans==0 && run_state==STATE_RUNNING && my_epics_ok ==2)
	//printf("hot_epics: mheader has been started during a run\n");
      else
	printf("hot_epics: unexpected combination of trans(%d) run_state (%d) and my_epics_ok(%d)\n",
	       trans,run_state,my_epics_ok);

      At_start_epics_timer =  ss_time(); // present time
      first_epics=TRUE;
      printf("hot_epics: at_start_run has started, my_epics_ok=%d (%s)\n",
	     my_epics_ok,okstring[my_epics_ok]);
      /* make sure no evar banks are sent */
      epics_hdr_sent=FALSE;
      write_logging_flag(EPICSFLG,0);
 
    } 
  else
    {
      if(debug)
	printf("hot_epics: unexpected value for my_epics_ok (%d -> %s)\n",
	       my_epics_ok,okstring[my_epics_ok]);
    }
 cont4:
  switch (my_epics_ok)
    {
    case 1:
    case 2:
      write_client_code(EPICS_LOG_ERR,CLEAR,"mheader"); // success or in progress
      break;
    default:
      write_client_code(EPICS_LOG_ERR,SET,"mheader"); // error
    }

 old_epics_ok = my_epics_ok;
 return;

}
/*----Write logging flag ------------------------------------------------------------------*/

void write_logging_flag(INT flag, BOOL setval )
{
  /* This flag is used by custom webpage for display
    flag=1 EPICS  flag=0 CAMP   flag=2  both */

  const char string[]="/Equipment/FIFO_acq/client flags/logging";
  char str[128];
  int i;
  
  i=0;
  while (i < BOTHFLG)
    {
      if(flag==EPICSFLG)
	sprintf(str,"%s epics",string);
      else
	sprintf(str,"%s camp",string);

      status = db_set_value(hDB,0,str,&setval,sizeof(setval),1,TID_BOOL);
      if(status != DB_SUCCESS)
	cm_msg(MERROR,"write_logging_flag","Error writing %d to \"%s\"(%d)",setval,str,status);
      if(flag != BOTHFLG)
	{
	  printf("write_logging_flag: break with logging flag=%d\n",flag);
	  break;
	}
      else
	flag = EPICSFLG; // now do epics
      i++;

    }
  return;    
}


/*----Redo epics------------------------------------------------------------------*/

INT redo_epics(void)
{
  INT i,size,status;

  
  epics_hdr_sent = FALSE; // stops sending EVAR banks
  write_logging_flag(EPICSFLG,0);

  gbl_epics_done = FALSE;
    
  /* update the epics record */
  size = sizeof (epicslog_settings);
  printf("redo_epics: Updating epics record\n");
  status = db_get_record(hDB,hEpics, &epicslog_settings, &size, 0);
  if(status != DB_SUCCESS)
    { 
      cm_msg(MERROR, "redo_epics", "failed to retrieve Epicslog record (%d)",status);
      epics_available=FALSE; // no epics access available
      write_epics_ok(0); // failure
    }
  else
    {
      if(debug)
	{
	  printf("redo_epics: epicslog_settings.epics_ok=%d old_epics_ok=%d my_epics_ok=%d (%s)\n",
		 epicslog_settings.epics_ok, old_epics_ok, my_epics_ok, okstring[my_epics_ok]);
	  printf("redo_epics: num_log_ioc=%d \n",epicslog_settings.output.num_log_ioc);
	}
      // Initialize epics if number to be logged > 0
      if(epicslog_settings.output.num_log_ioc > 0)
	{
	  for(i=0; i < epicslog_settings.output.num_log_ioc; i++)
	    printf("epics_log_ioc[%d]=%s\n",i,epicslog_settings.output.epics_log_ioc[i]);
            
	  At_start_epics_timer = ss_time();
	  write_epics_ok(2);  // indicate starting
	  status = init_epics(FALSE);  // fills n_epics
	  if(status == SUCCESS) // not fatal if epics fails
	    {	
	      write_epics_ok(1); // success
	      send_epics_hdr = TRUE; // set flag to send epics header bank
	      printf("redo_epics: success from init_epics, returning\n");
	    }
	  else
	    {
	      epics_available=FALSE; // no epics access available
	      write_epics_ok(0); // failure
	    }
	}
      else
	{ // nothing selected to be logged; this is not an error; midbnmr_darc will use a default
	  printf("redo_epics: No epics variables are selected to be logged\n");
	  cm_msg(MINFO,"redo_epics","No epics variables are selected to be logged");
	  epics_available = FALSE;
	  no_epics=TRUE;
	  
	}

    }
  gbl_epics_done=TRUE;
  if(debug)printf("redo_epics: set gbl_epics_done=TRUE and returning\n");
  return status;
}

/*----epics ok ------------------------------------------------------------------*/

INT write_epics_ok(INT ival)
{
  INT status,size;
  size=sizeof(ival);

  if(debug)printf("write_epics_ok: writing %d to \"epics ok\"\n",ival);
  status = db_set_value(hDB, hEpics, "epics OK", &ival, size, 1, TID_INT);
  if(status != SUCCESS)
    cm_msg(MERROR,"write_epics_ok","error writing %d to \"epics OK\" (%d)",ival,status);
  else
    epicslog_settings.epics_ok=ival;
  return status;
}
#endif

/*----Callback------------------------------------------------------------------*/

#ifdef BNMR_CAMP
INT open_camp_hot_link(void)
{
  char str[15];
  INT status;

  // BNMR/BNQR set up a hotlink on "camp ok" AFTER BOR
  if (hCamp==0)
    return DB_INVALID_PARAM;


  if (hCampOK==0)
    {
      size = sizeof(my_camp_ok);
      sprintf(str,"camp OK");
      status = db_find_key(hDB, hCamp, str, &hCampOK);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"open_camp_hot_link","find key on \"%s\" failed (%d)",str,status);
	  return status;
	}
    }
  if (hCampOK==0)
    {
      cm_msg(MERROR,"open_camp_hot_link","key hCampOK is zero");
      return DB_INVALID_PARAM;
    }

  status = db_open_record(hDB, hCampOK, &my_camp_ok
			  , size
			  , MODE_READ, hot_camp, "camp_OK touched");
  if (status != DB_SUCCESS) 
    cm_msg(MINFO,"open_camp_hot_link","open record on \"%s\"  failed (%d)",str,status);
  return status;
}

/*----Open hotlinks------------------------------------------------------------------*/

INT open_hot_links(void)
{
  INT status;

  if(debug)printf("open_hot_links: starting\n");
  //#ifdef CAMP_ACCESS
  status = open_camp_hot_link();
  if(status != SUCCESS)
      return status;
  //#endif
  //#ifdef EPICS_ACCESS    
  status = open_epics_hot_link();
  if(status != SUCCESS)
      return status;
  //#endif    
  status = open_hold_hot_link();
    
  return status;
}

/*----Close hotlinks------------------------------------------------------------------*/

INT close_hot_links(void)
{
  INT status;

  if(debug)printf("close_hot_links: starting\n");
  status = db_close_record(hDB, hCampOK);
  if(status != SUCCESS)
    {
      cm_msg(MERROR, "close_hot_links","Error closing hot link on CampOK (%d)",status);
      return status;
    }
  hCampOK=0;
  status = db_close_record(hDB, hEpicsOK);
  if(status != SUCCESS)
    {
      cm_msg(MERROR, "close_hot_links","Error closing hot link on EpicsOK (%d)",status);
      return status;
    }
  hEpicsOK=0;
  status = db_close_record(hDB, hHold);
  if(status != SUCCESS)
    {
      cm_msg(MERROR, "close_hot_links","Error closing hot link on Hold (%d)",status);
      return status;
    }
  hHold=0;
  return status;
}


/*-- Call_back  ----------------------------------------------------*/
void hot_camp(HNDLE hDB, HNDLE hkey, void * info)
{
  /* hot link on camp_ok
     camp_ok = 0  FAILURE set by at_start_run.csh 
     camp_ok = 1  SUCCESS  set by at_start_run.csh
     camp_ok = 2  IN PROGRESS at_start_run.csh sets to 2 when it starts
     camp_ok = 3  TIMEOUT at_start_run.csh did not finish (set by frontend_loop)
     camp_ok = 4  REDO CAMP  at_start_run.csh sets to 4 when it starts if a param is given (by webpage)
  */
  INT trans,status,size;

  
  printf("\n** HOT_CAMP: hot link touched; hkey= %d \"%s\" \n",hkey,info);
  if(my_camp_ok < 0 || my_camp_ok > 4)
    {
      printf("hot_camp: illegal value of my_camp_ok (%d)\n",my_camp_ok);
      goto cont4;
    }

  printf("Previously camp_ok was %d (%s); now %d (%s)\n",  
	 old_camp_ok, okstring[old_camp_ok],
	 my_camp_ok, okstring[my_camp_ok]);

 
  if(my_camp_ok==3)
    {
      	if(debug)printf("hot_camp: timeout!\n");
	goto cont4;
    }
 
  if(old_camp_ok == 3 && my_camp_ok == 1)
    {
      printf("hot_camp: camp timed out, but has finally returned success\n");
      goto cont4;
    }


  if(ppg_type != 1)
    {
      printf("hot_camp: type 2 run is in progress; no further action\n");
      my_camp_ok=1; // SUCCESS
      goto cont4;
    }

  if(first_camp) // run must be starting
    {
      if(my_camp_ok == 2 || my_camp_ok == 4 )
	goto cont4;


      printf("hot_camp: at_start_run has ended; my_camp_ok=%d (%s)\n",my_camp_ok,okstring[my_camp_ok]);
      first_camp=FALSE;
      if(my_camp_ok == 0)
	printf("hot_camp: failure from at_start_run.csh\n");
      else
	{
	  printf("hot_camp: Calling redo_camp \n");
	  status = redo_camp();
	  if (status != SUCCESS)
	    cm_msg(MERROR,"hot_camp","redo_camp returns error status (%d)",status);
	}
      printf("hot_camp: camp_settings.camp_ok=%d camp_available=%d campInit=%d\n",
	     camp_settings.camp_ok,	 camp_available, campInit);
      gbl_camp_done = TRUE;
    } // first_camp


  else if(my_camp_ok == 2  || my_camp_ok == 4) // in progress or redo camp
    {
      gbl_camp_done = FALSE;
      /* run must be going since hot links are open; user must be redoing camp */

      printf("hot_camp: trans=%d run_state=%d\n",trans,run_state); 

      At_start_camp_timer =  ss_time(); // present time
      first_camp=TRUE;
      printf("hot_camp: at_start_run has started, my_camp_ok=%d (%s)\n",
	     my_camp_ok,okstring[my_camp_ok]);
      camp_hdr_sent=FALSE;
      write_logging_flag(CAMPFLG,0);
     if(mheader_set_hold)
	  printf("hot_camp: mheader has the run on hold (at BOR)");
      else
	{
	   printf("hot_camp: mheader SHOULD have the run on hold (at BOR)...assume it will be shortly");

	   // At_start_camp_timer=-1;
	   //printf("hot_camp: run not being held by mheader; unexpected value for my_camp_ok (%d -> %s)\n",
	   //		 my_camp_ok,okstring[my_camp_ok]);
	}
    }

 cont4:
  
  if(!eorflag) // post-stop writes 0 to camp_ok for next time
    cm_msg(MINFO,"hot_camp","runstate=%d; status from at_start_run.csh is %s",run_state,okstring[my_camp_ok]);

  switch (my_camp_ok)
    {
    case 1:
    case 2:
      write_client_code(CAMP_LOG_ERR,CLEAR,"mheader"); // success or in progress
      break;
    default:
      if(debug)printf("eorflag=%d\n",eorflag);
      if(!eorflag)
	write_client_code(CAMP_LOG_ERR,SET,"mheader"); // error
    }


  old_camp_ok = my_camp_ok;
  return;
}

/*----camp ok------------------------------------------------------------------*/

INT write_camp_ok(INT ival)
{
  INT status,size;
  size=sizeof(ival);

  if(debug)printf("write_camp_ok: writing %d to \"camp ok\"\n",ival);
  status = db_set_value(hDB, hCamp, "camp OK", &ival, size, 1, TID_INT);
  if(status != SUCCESS)
    cm_msg(MERROR,"write_camp_ok","error writing %d to \"camp OK\" (%d)",ival,status);
  return status;
}

#endif // BNMR_CAMP

/*----Get record ------------------------------------------------------------------*/

#ifdef CAMP_ACCESS
/*------------------------------------------------------------*/
INT setup_camp_record()
//-------------------------------------------------------------
{
  /*  creates or gets camp equipment record (used for Epics for Type 2 runs)
      returns Handle hCamp
  */

  char str[80];
  CAMP_SETTINGS_STR(camp_settings_str); //  I-MUSR camp equipment includes epics settings
  INT status,size;
  HNDLE Hcamp;
  INT debug_camp=0;

  if(debug)printf("setup_camp_record: starting\n");
  /* 
     create camp record if necessary (needed for EPICS)
  */
  sprintf(str,"/Equipment/Camp/Settings");
  if(debug_camp) printf("str: %s\n",str);

  status = db_find_key(hDB, 0, str, &Hcamp);
  if (status != DB_SUCCESS)
    {
     	if(debug_camp) printf("setup_camp_record: Failed to find the key %s (%d) ",str,status);
      
      /* Create record for camp/settings area */     
      //printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB,0,str,  strcomb(camp_settings_str));
      if(status!= DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_camp_record","Create Record fails for %s (%d) ",status,str);
	  return(0); // returns hKey=0 meaning error
	}
      else
	if(debug_camp) printf("Success from create record for %s\n",str);
    }    
  else  /* key Hcamp has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, Hcamp, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "setup_camp_record", "error during get_record_size (%d) for camp record",status);
	  return 0;  // returns hKey=0 meaning error
	}
      printf("Size of camp saved structure: %d, size of camp record: %d\n", sizeof(CAMP_SETTINGS) ,size);
      if (sizeof(CAMP_SETTINGS) != size) 
	{
	  cm_msg(MINFO,"setup_camp_record","creating record (camp); mismatch between size of structure (%d) & record size (%d)", sizeof(CAMP_SETTINGS) ,size);
	  /* create record */
	  status = db_create_record(hDB,0,str,  strcomb(camp_settings_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"setup_camp_record","Could not create %s record (%d)\n",str,status);
	      return 0;  // returns hKey=0 meaning error
	    }
	  else
	    if (debug_camp)printf("Success from create record for %s\n",str);
	}
    }
  
  /* try again to get the key Hcamp  */

  status = db_find_key(hDB,0, str, &Hcamp);
  if(status!= DB_SUCCESS)
    {
      cm_msg(MERROR, "setup_camp_record", "cannot find Camp key for %s (%d)",str,status);
      return 0;  // returns hKey=0 meaning error
    }

  if(debug)printf("Got the key Hcamp\n");

  /* get the record */
  size = sizeof (camp_settings);
  if(debug_camp) printf("Hcamp = %d, size of record = %d \n",Hcamp,size);

  status = db_get_record(hDB,Hcamp, &camp_settings, &size, 0);
  if(status!= DB_SUCCESS)
  {
    cm_msg(MERROR, "setup_camp_record", "cannot retrieve %s record",str);
    return 0;  // returns hKey=0 meaning error
  }
  
  if(debug)printf("setup_camp_record: got the record, number of camp values to be logged is %d\n",
	 camp_settings.n_var_logged);
  old_camp_ok = camp_settings.camp_ok;
  return Hcamp; // returns the key
}


#endif

INT check_run_state(void)
{
  INT my_run_state,status,size,trans;
  //  get the run state
  size = sizeof(my_run_state);
  status = db_get_value(hDB,0, "/Runinfo/State",  &my_run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"check_run_state","error from db_get_value  /Runinfo/State (%d)",status);
      return (status);
    }
  if(my_run_state == STATE_STOPPED)
    {
      printf("check_run_state: Run has stopped!\n");
      return -1;
    }
  
  
  if(run_state != my_run_state)
    {
      size = sizeof(trans);
      status = db_get_value(hDB, 0, "/Runinfo/transition in progress", &trans, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"check_run_state","key not found /Runinfo/transition in progress (%d)",status);
	  return (status);
	}
      //printf("frontend_loop: current run state is %d trans=%d\n",my_run_state,trans);
      
      printf("\ncheck_run_state: trans=%d, run states DISAGREE (now %d or %s; previously %d or %s); returning !!! \n",
	     trans,my_run_state,runstring[my_run_state-1],
	     run_state,runstring[run_state-1]);
      my_run_state =  run_state;
      
      return -1;
    }
  
  
}
 
