/********************************************************************\
  Name:        bnmr_darc.c
  Created by:  Suzannah Daviel, TRIUMF

         originally based on a VMS program by Ted Whidden, with contributions by 
               Donald Arseneau & Pierre Amaudruz

  Contents:  this subroutines saves any of the following data in MUD format:
                 BNMR or BNQR type 2 (TD) data
                 TD-MUSR data
                 Integral-MUSR data
             
           It does NOT save BNMR/BNQR type 1 (Integral) data; midbnmr_darc.c is used instead.

 based on bnmr_darc.c 1.25 for TDmusr then on v1.3 imusr_darc.c
  
 $Log: bnmr_darc.c,v $
 Revision 1.7  2016/07/04 21:54:30  suz
 Donald's changes; total_saved is double

 Revision 1.6  2015/12/01 22:52:12  suz
 updates for post-Ted CAMP library

 Revision 1.5  2015/11/27 23:24:54  suz
 added ifdef YBOS

 Revision 1.4  2014/01/09 01:09:26  suz
 histogram totals array are doubles now not float

 Revision 1.2  2013/04/08 22:25:53  suz
 Changes after debugging new VMIC code

 Revision 1.1  2013/01/21 21:42:38  suz
 initial VMIC version to cvs

 Revision 1.61  2008/12/10 19:00:54  suz
 change a few debugs

 
\********************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
/* needed for lstat */
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

/* camp includes */
#include "camp_clnt.h"
// undefine these to prevent conflicts with midas.h
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#ifdef INLINE
#undef INLINE
#endif

/* midas includes */
#include "midas.h"
#include "msystem.h"
#ifdef HAVE_YBOS
#include "ybos.h"
#else
#define MAX_FILE_PATH 128
#endif
/* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "c_utils.h"
#include "triumf_fmt.h"
#ifdef MUSR
#include "trii_fmt.h"
#endif
/* darc includes */
#include "experim.h" /* odb structure */
#include "darc_odb.h"
#include "bnmr_darc.h"  /* prototypes */
#include "darc_files.h"  /* prototypes */
#include "mdarc.h" /* common header for mdarc & bnmr_darc */
// note caddr_t is defined as char*
#include "run_numbers.h"
#ifndef MUSR
#include "client_error_codes.h"
/* client code flags */
extern BOOL SET;
extern BOOL CLEAR;
#endif

#ifdef EPICS_ACCESS
#include "epics_ca.h"
#include "connect.h" /* prototypes */
#include "epics_log.h"
#include "mdarc_epics.h" // prototypes of mdarc_epics.c routines

extern HNDLE hEpics;
extern EPICS_LOG epics_log[MAX_EPICS];
extern INT n_epics; // number of epics devices to be logged
extern BOOL epics_available;
extern EPICS_PARAMS epics_params[NUM_EPICS_DEV]; // epics_scan_devices.h
#endif

BOOL got_event_flag;
char got_event_str[128];

void  hot_toggle (INT, INT, void * ); /* shared with mdarc.c */

/* globals */
#define VERSION 1.0
#define MAX_PURGE_FILES  150

/* 
 * Definition of ODB area for passing averages.  This could be moved to
 * experim.h, where such things normally live, but then people have to
 * generate a clean initial version before generating experim.h.  This
 * way the ODB record can simply be removed.  (Opinions may vary, so this
 * may be changed.)
 */

const char odb_average_area[] = { "averages" };

/* 
 * End each line with "\n\" because: warning: multi-line string literals are deprecated... bah humbug!
 * Many lines combined to help emacs' syntax highlighting.
 */
const char odb_average_def_str[] = { 
"[/averages]\n\
number = INT : 0\n\
variable = STRING[6] :\n[32] \n[32] \n[32] \n[32] \n[32] \n[32] \n\
mean = DOUBLE[6] :\n[0] 0.0\n[1] 0.0\n[2] 0.0\n[3] 0.0\n[4] 0.0\n[5] 0.0\n\
stddev = DOUBLE[6] :\n[0] 0.0\n[1] 0.0\n[2] 0.0\n[3] 0.0\n[4] 0.0\n[5] 0.0\n"
};
 
/* HNDLE hDB, hMDarc  are now defined in mdarc.h */
INT nH;  /* number of histograms - either nHistograms or nHistBanks */
time_t timbuf;
char timbuf_ascii[30];
MUD_SEC_GRP* pMUD_indVarGrp_gbl = NULL;
int numIndVar = 0;  /* global for recursion SD/TW */

#ifdef MUSR   /* IMUSR and TDMUSR both supported */
MUSR_I_ACQ_SETTINGS imusr;
MUSR_TD_ACQ_MDARC fmdarc;
SCALER_SETTINGS fscaler;
IMUSR_CAMP_VAR *campVars;
#else    /* (BNMR/BNQR) */
/* these to access frontend & mdarc trees */
FIFO_ACQ_FRONTEND fifo_set;
HNDLE  hFS;
FIFO_ACQ_MDARC fmdarc;
#endif

BOOL campFail = FALSE; /* flag a failure from camp to avoid camp hanging next time */
int campInit  = FALSE;
BOOL campUpdated  = FALSE;
double tot_events_gbl = 0.0;
int numWtLogged_gbl = 0; /* Number of weighted averages saved in odb */

CAMP_VAR_STATS * pCampSums = NULL;

AVER_ODB_HANDLES aveKeys;



#ifndef MUSR
#include "write_client_code.c"
#endif

DWORD bnmr_darc(caddr_t pHistData)
{
  /* bnmr_darc also uses globals  
         nHistograms   no. of histograms defined  (I-MUSR)
	   or
         nHistBanks   no. of histograms defined  (TD-MUSR)  
         nHistBins    no. of bins per histogram 
     (according to Bank information)
  */

  DWORD status;
  D_ODB *p_odb_data;
  D_ODB odb_data;
  INT i;
  INT run_state;
  HNDLE hktmp;
  INT serial_number;

  //printf("debug=%d debug_mud=%d debug_dump=%d  debug_proc=%d\n",	 
  //	 debug, debug_mud, debug_dump,  debug_proc);

  if(firstxTime)   /* first time through after tr_start */
    {
#ifdef MUSR
      if(I_MUSR)
	{
	  printf("bnmr_darc: starting with Integral MUSR data \n");
	}
      else if (TD_MUSR)
	{
	  printf("bnmr_darc: starting with TD MUSR data  \n");
	}
      else
	{
	  cm_msg(MERROR,"bnmr_darc", 
		 "ERROR - MUSR is selected, but flag is set for neither Integral nor TD MUSR; cannot proceed\n");
	  return DB_INVALID_PARAM;
	}
#else //BNMR
      got_event_flag=FALSE; // written to odb by mdarcBnmr at genuine run start (for custom wp)

      TD_MUSR=I_MUSR=FALSE;  /* initialize these FALSE */
      if(debug)
	printf("bnmr_darc: starting with BNMR/BNQR type 2 data \n");// type 1 uses midbnmr_darc instead
      //status = db_set_value(hDB,0,string,&setval,sizeof(setval),1,TID_BOOL);
#endif
    }  // end of first time 

#ifndef MUSR
  if (gbl_alpha)
    {
      if(bGotEvent && pHistData != NULL)
	saving_data_flag=1; // alpha mode set a flag to prevent clearing pHistData by next event pieces
    }
#endif 
  p_odb_data = &odb_data;  


  if (bGotEvent)
    {
      bGotEvent = FALSE;
#ifdef MUSR      
      if(I_MUSR)
	{
	  if(debug)
	    printf("bnmr_darc: IMUSR...bGotCamp = %d, campVars pointer = %p\n",bGotCamp, campVars);
	}
#else
      if(!got_event_flag)
	{
	  // write this for BNMR/BNQR custom web page error handling
	  got_event_flag=TRUE; // flag for custom wp
	  // write this for BNMR/BNQR custom web page error handling
	  sprintf(got_event_str,"/Equipment/%s/client flags/got_event",eqp_name);
	  status = db_set_value(hDB,0,got_event_str,&got_event_flag,sizeof(got_event_flag),1,TID_BOOL);
	  if(status != DB_SUCCESS)
	    cm_msg(MERROR,"bnmr_darc","Error clearing got_event flag=%d to \"%s\"(%d)",got_event_flag,got_event_str,status);
	}
	
#endif

      
      if(pHistData == NULL)
	{
	  printf("bnmr_darc: No Histogram data available; pHistData is NULL but bGotEvent was TRUE \n");
	  status=SUCCESS;
	  goto ret;
	}
      /* copy global nHistograms (I-MUSR) or nHistBanks (TD-MUSR and BNMR) to nH */
      
#ifdef MUSR
      if(I_MUSR)
	nH = nHistograms;  /* global  I-MUSR */
      else 
#endif
	nH = nHistBanks;   /* global    TD_MUSR or BNMR/BNQR type 2 */
      
      
      /* nH is the number of histograms for Integral or TD MUSR */
      if ( nH <= 0)
	{
	  cm_msg(MERROR,"bnmr_darc","Invalid number of histograms (%d)",nH);
	  status = DB_INVALID_PARAM;
	  goto ret;
	}
      
      if ( nHistBins <= 0)   /* nHistBins is global */
	{
	  cm_msg(MERROR,"bnmr_darc","Invalid number of histogram bins (%d)",nHistBins);
	  status = DB_INVALID_PARAM;
	  goto ret;
	}
      if(debug)
	printf("bnmr_darc: starting with %d histograms of %d bins\n",nH, nHistBins); 

#ifdef MUSR
      if(I_MUSR)
	printf("\n\n ******* bnmr_darc: starting with bGotEvent = %d ***********\n",bGotEvent);
      else
#endif
	{ // TD-MUSR /BNMR/BNQR 
	  if(pSN != NULL)
	    {
	      last_saved_SN =((int*)pSN)[0]; 
	      if(TD_MUSR)
		printf("\n\n ******* bnmr_darc: working on event SerNum = %d pHistData=%p *****************\n",
		       last_saved_SN,pHistData );
	      else /* BNMR/BNQR no danger of unsaved events; don't bother with pointer */
		printf("\n\n ******* bnmr_darc: working on event SerNum = %d  *****************\n",
		       last_saved_SN );
	    }
	  else
	    {
	      printf("\n\n ******* bnmr_darc: working on event with unknown SerNum  *****************\n");
	      last_saved_SN = -1;
	    }
	} // else TD-MUSR

      
      if(debug_dump)
	{
	  printf("bnmr_darc: pHistData = %p\n",pHistData);
	  if(!I_MUSR)
	    { // TD-MUSR, BNMR/BNQR
	      db=dump_begin;  // defaults
	      dl=dump_length;
	    }
	  /* for I-MUSR, db,dl were set up in process_event_I */
	  printf("bnmr_darc: dump begins at db=%d and ends at dl=%d\n",db,dl);
      
	  for (i = db; i < db + dl ; i++)
	    printf(" data[%d] = 0x%x  or %d (dec)\n"
		   ,i,((int*)pHistData)[i],((int*)pHistData)[i]);
#ifdef MUSR
	  if(I_MUSR)
	    {  /* dump I-MUSR's  Camp data if available */
	      if(pCampData == NULL)
		printf("No Camp Data available\n");
	      else
		{
		  for (i = db; i < db + dl ; i++)
		    printf(" camp data[%d] = %f  or %d(dec)\n",i, (float)pCampData[i], (INT)pCampData[i]     );
		}
	    }
#endif
	} // end of debug_dump
    
      status = darc_get_odb(p_odb_data);
      if (status != DB_SUCCESS)  
	{
	  cm_msg(MERROR,"bnmr_darc","Failure to get the odb data needed");
	  goto ret;
	}
      if(debug)
	printf("bnmr_darc: Success from darc_get_odb\n");
      
      campUpdated = FALSE;
      
#ifdef MUSR    
      if(I_MUSR)
	status = DARC_write_I(&odb_data,pHistData); //I-MUSR
      else
	status = DARC_write_TD(&odb_data,pHistData); // TD-MUSR
#else
      status = DARC_write_TD(&odb_data,pHistData); // and BNMR/BNQR
#endif
      if(status != SUCCESS)
	{
	cm_msg(MERROR,"bnmr_darc","failure to save the histogram data");
	if (toggle)    /* don't allow another toggle after error */
	  cm_msg(MERROR,"bnmr_darc","toggle did not delete old run files after error ");
	status = FAILURE;
	goto ret;
	}

      printf("==== bnmr_darc: toggle is true ===\n");
      if (toggle)
	{    /* delete old run files and reset toggle */
	  printf ("bnmr_darc:Toggle is set; deleting any run files for old run number %d \n",old_run_number);
	  status = darc_kill_files(old_run_number,  p_odb_data->save_dir);
	  
	  toggle = FALSE;  /* set global toggle false */
	  /* turn toggle bit off in odb  */
	  size = sizeof(toggle);
	  status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
	  if (status != DB_SUCCESS)
	    cm_msg(MERROR, "bnmr_darc", "cannot clear toggle flag ");
	  
	  if(debug) printf ("bnmr_darc:  Toggle is cleared \n");
	  /* restore the hot link on toggle  */
	  
	  if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
	    {
	      size = sizeof(fmdarc.toggle);
	      status = db_open_record(hDB, hktmp, &fmdarc.toggle, size, MODE_READ, hot_toggle , NULL);
	      if (status != DB_SUCCESS)
		{
		  cm_msg(MERROR,"bnmr_darc","Failed to open record (hotlink) for toggle (%d)", status );
		  write_message1(status,"bnmr_darc");
		  goto ret;
		}      
	    }
	}  /* if toggle */
    }    /* if bGotEvent */
  else
    {  /* bGotEvent is false */
      if (debug)
	printf ("bnmr_darc: Called with bGotEvent false; no new data available\n");
      /* get the run state to see if run is going */
      size = sizeof(run_state);
      status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"bnmr_darc","key not found /Runinfo/State");
	  write_message1(status,"bnmr_darc");
	  goto ret;
	}
      //if (debug) printf("run_state = %d (3=running)\n",run_state);
      
#ifdef MUSR
      if(run_state == STATE_PAUSED)
	{
	  if(debug) printf("bnmr_darc: Run %d is paused; data not saved\n",run_number);
	}
      else if (run_state == STATE_RUNNING)
	cm_msg(MINFO,"bnmr_darc","No new Histogram data is available at this time; data not saved");
      else  /* not running */
	if(debug) printf("bnmr_darc: Not running; no data found; data not saved\n");    
      
#else
      if(run_state == STATE_RUNNING)  /* running; we should have data unless hold flag is on (BNMR only) */
	{
	  BOOL flag;
	  char str[128];
	  
	  /* is  "frontend" area present?  (for BNMR hold flag)  */
	  sprintf(str, "/Equipment/%s/frontend",eqp_name);  
	  status = db_find_key(hDB, 0, str, &hFS);
	  sprintf(str,
		  "No valid Histogram data is available at this time; data not saved");
	  
	  if (status == DB_SUCCESS) 
	    {
	      /* run may be on hold */
	      size = sizeof(flag);
	      status = db_get_value(hDB,hFS, "flags/hold",  &flag, &size, TID_BOOL, FALSE);
	      if (status != DB_SUCCESS)
		{
		  status=cm_msg(MERROR,"bnmr_darc","Could not access frontend key: flags/hold ");
		  write_message1(status,"bnmr_darc");
		}
	      else
		{
		  if (flag)
		    sprintf(str,"Run %d is on hold; data not saved",run_number);
		}
	    }
	  cm_msg(MINFO,"bnmr_darc","%s",str); /* send appropriate message */
	}
      
      else  /* not running */
	if(debug) printf("bnmr_darc: Not running; no data found; data not saved\n");    
      
#endif
    } /* end of bGotEvent is false */
  status = SUCCESS;

 ret:  
#ifndef MUSR
  if (gbl_alpha)saving_data_flag=0; // clear flag
#endif;
  return(status );

}




/*------------- darc_write_odb ------------------------------*/
DWORD 
darc_write_odb(D_ODB *p_odb_data)
/*--------------------------------------------------------------*/
{
  INT  status, size, i,j;
  char str[128];
  HNDLE hKeyH;
  KEY key;
  INT nhis;
  
  /* write time file was saved */ 
  if (debug || debug_check)
    printf("darc_write_odb: Writing time file was saved = %s to the odb \n", p_odb_data->time_save);
  sprintf(fmdarc.time_of_last_save,"%s",p_odb_data->time_save);
  size=sizeof(fmdarc.time_of_last_save);
  status = db_set_value(hDB, hMDarc, "time_of_last_save", &fmdarc.time_of_last_save, size,1, TID_STRING);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_write_odb","cannot update key /Equipment/%s/mdarc/time_of_last_save (%d)",
	     eqp_name,status);
      write_message1(status,"darc_write_odb");
      return status;
    }
  
  /* write filename  */ 
  if (debug || debug_check)
    printf("darc_write_odb: Writing to odb saved filename = %s  \n", p_odb_data->file_save);
  sprintf(fmdarc.last_saved_filename,"%s",p_odb_data->file_save);
  size=sizeof(fmdarc.last_saved_filename);
  status = db_set_value(hDB, hMDarc, "last_saved_filename", &fmdarc.last_saved_filename, size,1, TID_STRING);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_write_odb","cannot update key /Equipment/%s/last_saved_filename (%d)",eqp_name,status);
      write_message1(status,"darc_write_odb");
      return status;    
    }
  if(!I_MUSR)
    {   // TD MUSR and BNMR (imusr has no temp or field) 

      /* write temperature and field header values in case they were dynamic */

      status = db_find_key(hDB, 0, "/Experiment/Edit on start", &hKeyH);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_write_odb","cannot find key %s", "/Experiment/Edit on start");
	  write_message1(status,"darc_write_odb");
	  return status;
	}
      
      if (debug || debug_check)
	printf("darc_write_odb: Writing to odb temperature = %s  \n", p_odb_data->temperature);
      size = sizeof(p_odb_data->temperature);
      status = db_set_value(hDB, hKeyH, "temperature", &p_odb_data->temperature, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_write_odb","cannot update key /Experiment/Edit on start/temperature (%s)",status);
	  write_message1(status,"darc_write_odb");
	  return status;
	}
      
      if (debug || debug_check)
	printf("darc_write_odb: Writing to odb field = %s  \n", p_odb_data->field);
      size = sizeof(p_odb_data->field);
      status = db_set_value(hDB, hKeyH, "field", &p_odb_data->field, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_write_odb","cannot update key /Experiment/Edit on start/field (%d)",status);
	  write_message1(status,"darc_write_odb");
	  return status;
	}
      /* write histogram totals */
      sprintf(str,"/Equipment/%s/mdarc/histograms/output/histogram totals",eqp_name);
    }  // not IMUSR
  
  nhis=MAX_HIS;
#ifdef MUSR
  if(I_MUSR)
    {
      sprintf(str,"/Equipment/%s/settings/imdarc/histograms/histogram totals",i_eqp_name);
      nhis = MAX_IHIS;
    }
#endif
  if (debug | debug_dump)printf("darc_write_odb: %d histograms are defined\n",nhis);

  db_find_key(hDB, 0, str, &hKeyH);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_write_odb","cannot find key %s (%d)",str,status);
      write_message1(status,"darc_write_odb");
      return status;
    }
  if(debug_dump)
    {
      for (j=0; j< nhis; j++)
	{
#ifdef MUSR
	  if(I_MUSR)
	    printf(" IMUSR_his_total[%d] = %f \n", j, p_odb_data->IMUSR_his_total[j]);
	  else    
#endif
	    printf(" his_total[%d] = %f \n", j, p_odb_data->his_total[j]);
	}
    }
 
  if(debug)
    printf("darc_write_odb: Writing histogram totals to odb key %s\n",str);  
#ifdef MUSR  
   /* float for musr */
  if(I_MUSR)
    status = db_set_data(hDB,hKeyH, p_odb_data->IMUSR_his_total,  nhis * sizeof(float), 
			 nhis, TID_FLOAT);
  else
    status = db_set_data(hDB,hKeyH, p_odb_data->his_total,  nhis * sizeof(float), 
			 nhis, TID_FLOAT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_write_odb","cannot set data for key %s (%d)",str,status);
      write_message1(status,"darc_write_odb");
      return status;
    }

  if(I_MUSR)
    {
      sprintf(str,"/Equipment/%s/settings/imdarc/histograms/total saved",i_eqp_name);
      status = db_set_value(hDB, 0, str, &p_odb_data->total_save, sizeof(float), 
		   1, TID_FLOAT);
    }
  
  if(!I_MUSR)
    {  // TD-MUSR
      sprintf(str,"/Equipment/%s/mdarc/histograms/output/total saved",eqp_name);
      status = db_set_value(hDB, 0, str, &p_odb_data->total_save, sizeof(float), 
		   1, TID_FLOAT);
    }
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_write_odb","cannot set data for key %s (%d)",str,status);
      write_message1(status,"darc_write_odb");
      return status;
    }

#else // BNMR/BNQR increased size of histogram totals array; totals are now double not float
  {
    HNDLE  hHout;
    KEY key1;
    
    status = db_get_key(hDB, hKeyH, &key1);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"midbnmr_darc","cannot get output/histogram totals key (%d)",status);
	return;
      }
    printf("Number of values in output/histogram totals array is %d\n",key1.num_values);
    if(MAX_HIS > key1.num_values)
      { 
	cm_msg(MERROR,"darc_write_odb"," output/histogram totals array size in odb (%d) is less than MAX_HIS (%d). Cannot write histogram totals",
	       key1.num_values,MAX_HIS);
	return;
      }
    

    // double for BNMR
    for (j=0;  j < nhis; j++)
      fmdarc.histograms.output.histogram_totals[j]=  p_odb_data->his_total[j];
    fmdarc.histograms.output.total_saved=  p_odb_data->total_save;
    
    status = db_find_key(hDB, hMDarc, "histograms/output", &hHout);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"darc_write_odb","cannot find key for histograms/output in ODB (%d)",status);
	return;
      }
    
    /* Update Histogram settings in ODB */
    size = sizeof(fmdarc.histograms.output);
    status = db_set_record(hDB, hHout, &fmdarc.histograms.output, size, 0);
    if (status != DB_SUCCESS)
      cm_msg(MINFO,"darc_write_odb","Failed to save mdarc/histograms/output record (size=%d) (%d)",size,status);  
    
  }
  
#endif // BNMR/BNQR
  return(SUCCESS); 
}



/*------------------------- DARC_write_TD ---------------------------*/
int
DARC_write_TD( D_ODB* p_odb_data, caddr_t pHistData)
/*-------------------------------------------------------------------*/     
{
  int status;
  MUD_SEC_GRP* pMUD_fileGrp;
  MUD_SEC_GEN_RUN_DESC* pMUD_desc;
  MUD_SEC_GRP* pMUD_scalGrp;
  MUD_SEC_GRP* pMUD_histGrp;
  MUD_SEC_GRP* pMUD_cmtGrp;
  int i, j, k;
  int offset;
  double cnt, dtotal_save; /* hist counts and run total counts, without loss of precision */
  
  /*
   *  Compute sums of bin quantities
   */
  
  p_odb_data->total_save = 0;
  dtotal_save = 0.0;
  
  offset = 0 ; /* initial offset into data array */
  
  if (debug || debug_mud) printf("DARC_write_TD: Starting with pHistData = %p\n",pHistData);
  for( i = 0; i <  p_odb_data->his_n; i++ )
    {
      if (debug_mud)printf("DARC_write_TD: His %d  pHistData = %p\n",i,pHistData);
      cnt = 0.0;
      if(debug_mud)printf("DARC_write_TD: his %d  offset= %d\n",i,offset);
      for( j = 0; j < p_odb_data->his_nbin; j++ )
	{
	  if(debug_dump)
	    {
	      if( j >= dump_begin && j < (dump_begin+dump_length) )
		{
		  if (debug) printf("Adding on data index %d = 0x%x (%u) at offset(%d); his_total[%d](%f)\n",
			 j, ((unsigned int*)pHistData)[offset+j],  ((unsigned int*)pHistData)[offset+j],
			 (offset+j), i, p_odb_data->his_total[i]);
		}
	    }
          cnt += (double) ((unsigned int*)pHistData)[offset+j];
	}
      p_odb_data->his_total[i] = (float) cnt;
      dtotal_save += cnt;
      
#ifndef MUSR
      /* BNMR/BNQR only */
      if(debug_dump)
	{
	  BOOL test_mode=0;
#ifdef HAVE_SIS3801
	  if ( fifo_set.sis_test_mode.sis3801.test_mode )
	    test_mode=1; // test mode
#endif
#ifdef HAVE_SIS3801
	  if ( fifo_set.hardware.sis3820.sis_mode < 2)
	    test_mode=1; // test mode
#endif
	  if (test_mode)
	    {
	      /* for test data only, approx total should be nbins * 1st data value */
	      float approx;
	      approx= ( (float) p_odb_data->his_nbin) * ((float) ((unsigned int*)pHistData)[offset+0]);
	      printf("Rough total for histogram %d should be %d * %u =  %f\n",i,
		     p_odb_data->his_nbin, ((unsigned int*)pHistData)[offset+0], approx);
	    }
	}
#endif // not MUSR
      if(debug_dump || debug_mud)
	printf("DARC_write_TD: his %d his_total[%d]= %f, total_save=%lf\n",
	       i,i,p_odb_data->his_total[i], dtotal_save ) ;
      
      offset = offset + p_odb_data->his_nbin; /* point to next histogram */
    }
  /* end of loop on all histograms, zeroing totals for unused histograms */
  for( i = p_odb_data->his_n; i < MAX_HIS; i++ )
    p_odb_data->his_total[i] = 0.0;

  p_odb_data->total_save = dtotal_save;


#ifdef EPICS_ACCESS
  if(epics_available)
    {
      if(debug_mud)
	printf("DARC_write_TD: logging Epics sections\n" );
      DARC_epics_log_TD( );
    }
  else
    printf("DARC_write_TD: Epics is not available\n");
#endif  // EPICS



  if(debug_mud)printf("DARC_write_TD: logging PPG parameters\n" );


#ifndef MUSR
  /*
   *  For bNMR, log the PPG parameters from the ODB as if they were
   *  Camp variables
   */

  DARC_log_ppg_par( );

  
#endif


  if(debug_mud)printf("DARC_write_TD: logging CAMP sections\n" );


  /*
   * Gather the Camp logged variables: (re)connect to Camp; collect all logged
   * Camp variables, and calculate (perhaps weighted) statistics; maybe use
   * logged values for temperature and field headers.
   */

  numWtLogged_gbl = 0;

  DARC_camp_connect( p_odb_data->camp_host );

  DARC_camp_log_TD( p_odb_data, dtotal_save );

  DARC_get_dynamic_headers( p_odb_data );

  if(debug) printf("End of any Camp\n");

  
  pMUD_fileGrp = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_FMT_TRI_TD_ID );
  if (debug_mud) printf(" DARC_write_TD: After MUD_new, pMUD_fileGrp = %p\n",pMUD_fileGrp );
  
  /*
   *  Convert the run description
   */
  pMUD_desc =  (MUD_SEC_GEN_RUN_DESC*) MUD_new( MUD_SEC_GEN_RUN_DESC_ID, 1 );
  DARC_runDesc_TD( p_odb_data, pMUD_desc );
  
  /*
   *  Convert the scalers
   */
  pMUD_scalGrp = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_GRP_TRI_TD_SCALER_ID );
  DARC_scalers( p_odb_data, pMUD_scalGrp );
  
  /*
   *  Convert the histograms
   */
  pMUD_histGrp = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_GRP_TRI_TD_HIST_ID );
  
  if( DARC_hists( p_odb_data, pMUD_histGrp, pHistData) == FAILURE )
    {
      cm_msg(MERROR,"DARC_write_TD","DARC_hists: failed to create histograms\n" );
      return(FAILURE);
    }
  
  /*
   *  Collect comments if they exist
   */
  pMUD_cmtGrp = DARC_comments( p_odb_data );

  /*
   *  Assemble the first level sections
   */
  /* keep this code in case we need to time something */
  /*  if (timer)
      {
      time(&timbuf);
      strcpy(timbuf_ascii,ctime(&timbuf));
      printf (
      "DARC_write_TD (TIMER) Assembling 1st level sections  %s\n",timbuf_ascii);
      }
  */
  MUD_addToGroup( pMUD_fileGrp, pMUD_desc );
  MUD_addToGroup( pMUD_fileGrp, pMUD_scalGrp );
  MUD_addToGroup( pMUD_fileGrp, pMUD_histGrp );

  if ( pMUD_cmtGrp != NULL ) 
    MUD_addToGroup( pMUD_fileGrp, pMUD_cmtGrp );

  if( pMUD_indVarGrp_gbl != NULL )
    MUD_addToGroup( pMUD_fileGrp, pMUD_indVarGrp_gbl );

  
  
  /*
   *  Write the file
   */
  if(debug) printf("DARC_write_TD: calling DARC_write to write the file\n");
  status=DARC_write(p_odb_data, pMUD_fileGrp); // write the file
  return(status );
}



/*------------ DARC_write -------------------------------------*/
int
DARC_write( D_ODB* p_odb_data, MUD_SEC_GRP* pMUD_fileGrp )
/*---------------------------------------------------------*/
{
  /* Used for  IMUSR, TD-MUSR and BNMR/BNQR (Type 2 TD) */
  int status;
  char outFile[128];
  INT nfile, next_version;
  INT no_purge=0;
  FILE* fout;
  char str[126];
  /*
   *  Write the file
   */
  /* check if there is any REGULAR file with no version no. at all (symlink is OK) */
  if(debug_check)
  printf("Calling darc_find_saved_file \n");
  nfile = darc_find_saved_file (p_odb_data->run_number, p_odb_data->save_dir);
  if (nfile == 1 )  /* returns 1 if a REGULAR file */
    {
      cm_msg(MERROR,"DARC_write","final run file already exists (%s/%06d.msr)",
	     p_odb_data->save_dir,p_odb_data->run_number);
      printf("DARC_write: Cannot save histograms\n");
      return(FAILURE);
    }
  
  /* see if there are any files already with this run number to find the next version */  
  if(debug_check)printf("Calling darc_check_files with keep=%d\n",no_purge);
  nfile = darc_check_files (p_odb_data, no_purge , &next_version);
  
  // mdarc may have been restarted so there may be files already for this run 
  if(debug_check)printf("Found %d files for this run. Next file version will be %d\n",
                        nfile,next_version);  
  if(next_version == 0)
    {
      if(debug_check)
	{
	  printf("Darc_write: next_version from darc_check_files is zero (error)");
	  printf("DARC_write: Number of saved files for this run found on disk: %d",nfile);
	}
      cm_msg(MERROR,"DARC_write","Cannot save histograms");
      return(FAILURE);
    }    

  sprintf( outFile, "%s/%06d.msr_v%d",
           p_odb_data->save_dir,p_odb_data->run_number, next_version );
  
  /* OK to proceed */
  fout = MUD_openOutput( outFile );
  if( fout == NULL ) 
    {
      cm_msg(MERROR,"DARC_write","failed to open file %s", outFile );
      return( FAILURE );
    }
  
  
  if (debug_mud) 
    printf("Darc_write: about to call MUD_writeFile with pMUD_fileGrp=%p\n",pMUD_fileGrp);
  status = MUD_writeFile( fout, pMUD_fileGrp );
  
  fclose( fout );
  
  
  
  if (debug_mud) printf("Darc_write: about to call MUD_free with pMUD_fileGrp=%p\n",pMUD_fileGrp);
  
  MUD_free( pMUD_fileGrp );
  pMUD_indVarGrp_gbl =  (MUD_SEC_GRP*)  NULL; /* free pointer for recursion  SD/TW */
  numIndVar = 0;  /* global for recursion SD/TW */
  
  if( status != SUCCESS  )
    {
      cm_msg(MERROR,"DARC_write","failed to write file %s", outFile );
      return( FAILURE );
    }
  
  /* set up symbolic link for analysis programs 
     darc_unlink returns 1 for regular file found */
  if( darc_unlink (p_odb_data->run_number, p_odb_data->save_dir) != 1)
    {
      if(darc_set_symlink(p_odb_data->run_number, p_odb_data->save_dir,
			  outFile)==SUCCESS)
	{
	  if (debug_check) printf("DARC_write: symbolic link successfully set up\n");
	}
      else   // not fatal
	printf("DARC_write: Info - couldn't set up symbolic link\n");
    }
  else
    printf("DARC_write: Info darc_unlink found regular saved data file (expected symbolic link)\n");
  /*
   *  Update odb too.
   */
  time(&timbuf);
  strcpy(str, ctime(&timbuf));
  str[24] = 0;
  strncpy(p_odb_data->time_save, str, sizeof(p_odb_data->time_save));/* save time saved */
  strncpy( p_odb_data->file_save, outFile,sizeof(p_odb_data->file_save) ); /* save file name */
  darc_write_odb(p_odb_data);
 
#ifdef MUSR
  if(I_MUSR)
    {
      cm_msg(MINFO,"DARC_write","*** data saved in file %s at %s  ***", outFile,str);  
      /* update imdarc area in odb  */
      imusr.imdarc.histograms.num_bins = nHistBins;
      imusr.imdarc.histograms.number_defined = nH;
      size = sizeof(imusr.imdarc.histograms.num_bins);
      sprintf(str,"/equipment/%s/settings/imdarc/histograms/num bins",i_eqp_name);
      status = db_set_value(hDB, 0, str, &imusr.imdarc.histograms.num_bins, size, 1, TID_INT);
      if (status != DB_SUCCESS)
	cm_msg(MERROR, "bnmr_darc", "cannot update %s ",str);
      
      sprintf(str,"/equipment/%s/settings/imdarc/histograms/number defined",i_eqp_name);
      size = sizeof(imusr.imdarc.histograms.number_defined);
      status = db_set_value(hDB, 0, str, &imusr.imdarc.histograms.number_defined, size, 1, TID_INT);
      if (status != DB_SUCCESS)
	cm_msg(MERROR, "bnmr_darc", "cannot update %s ",str);
      
    }
  else
#endif
    cm_msg(MINFO,"DARC_write","*** data saved in file %s at %s (SN=%d) ***", outFile,str,last_saved_SN);  


  /*      Purge the files   */
  if(debug_check)
    printf("Calling darc_check_files with  keep=%d\n",p_odb_data->purge_after); 
  nfile = darc_check_files(p_odb_data, p_odb_data->purge_after, &next_version);
  
  if(debug_check) printf("DARC_write: darc_check_files returns nfile=%d\n",nfile);
  if(next_version == 0)
    {
      if(debug)
	printf("Darc_write: next_version from darc_check_files is zero (error)\n");
      cm_msg(MINFO,"DARC_write","WARNING - Number of saved files found on disk: %d",nfile);
      cm_msg(MINFO,"DARC_write","Can't determine next version for saved file. Cannot purge histograms");
      /* not a fatal error but there may be a problem next time */
    }    
  
  return( SUCCESS );
}



/*-----------------------------------------------------------*/
int 
DARC_hists( D_ODB *p_odb_data, MUD_SEC_GRP* pMUD_histGrp,
		caddr_t pHistData )
/*-----------------------------------------------------------*/
{
  
  int i,k;
  MUD_SEC_GEN_HIST_HDR* pMUD_histHdr;
  MUD_SEC_GEN_HIST_DAT* pMUD_histDat;
  int offset;
  int bytes;
  int m;
  
  bytes = sizeof(int);
  for( i = 0; i < p_odb_data->his_n; i++ ) /* loop on all histograms */
    {
      pMUD_histHdr = (MUD_SEC_GEN_HIST_HDR*) MUD_new( MUD_SEC_GEN_HIST_HDR_ID, i+1 );
      pMUD_histDat = (MUD_SEC_GEN_HIST_DAT*) MUD_new( MUD_SEC_GEN_HIST_DAT_ID, i+1 );
      
      /* separate this so we can check for failure from malloc 
	 pMUD_histDat->pData = (char*)zalloc( 4*p_odb_data->his_nbin ); 
      */
      
      /*
       *  TW/SD  21-Dec-2000  Must allocate for worst case when packing.
       *                      In worst case there can be 3 bytes in front of
       *                      each data word (2 for number of data to follow,
       *                      1 for the data size in bytes).  So, malloc
       *                      3 extra bytes per number of bins to be safe.
       */
      pMUD_histDat->pData = (char*)malloc( (bytes+3) * p_odb_data->his_nbin);
      if (pMUD_histDat->pData == NULL)
	{
	  cm_msg(MERROR,"DARC_hists","malloc fails to allocate %d bytes",
                 bytes * p_odb_data->his_nbin);
	  //        cm_msg(MERROR,"DARC_hists","Not all the histogram data can be saved");
	  printf("DARC_hists: You must reduce # of histograms and/or # of bins\n");
	  
	  return(FAILURE);
	}
      else
	{
	  
	  if(debug)printf("DARC_hists: malloc successful, allocates %d bytes \n",
			  bytes * p_odb_data->his_nbin);
	  
	}
      /* zero the array */
      memset((char*)pMUD_histDat->pData,0,bytes*p_odb_data->his_nbin );
#ifdef MUSR      
      if (I_MUSR)
	offset = i  *  imusr.imdarc.maximum_datapoints ;
      else
#endif
	offset = i  *  p_odb_data->his_nbin; // TD-MUSR and BNMR
      
      if(debug_mud || debug || debug_dump )
	{
	  printf("DARC_hists:About to call DARC_hist_TD with offset=%d, (caddr_t)pHistData = %p\n"
		 ,offset,pHistData);
	  if(debug_dump)
	    {
	      /* dump some data */
	      for (m=offset + db ; m < db + dl +offset; m++)
		printf("Array element %d  =  0x%x or %d (dec)  \n", 
		       m, ((int *)pHistData)[m], ((int *)pHistData)[m]);
	    }
	}
      // add extra cast back to caddr_t for unix conversion
#ifdef MUSR
      if (I_MUSR)
	DARC_hist_I( i, p_odb_data, (caddr_t)(&((int*)pHistData)[offset]), 
		     pMUD_histHdr, pMUD_histDat );
      else
#endif
	DARC_hist_TD( i, p_odb_data, (caddr_t)(&((int*)pHistData)[offset]), 
		      pMUD_histHdr, pMUD_histDat );
      
      if(debug_mud)printf("DARC_hists: calling MUD_addtoGroup for histogram header\n");
      MUD_addToGroup( pMUD_histGrp, pMUD_histHdr );
      if(debug_mud)printf("DARC_hists: calling MUD_addtoGroup for histogram data\n");
      MUD_addToGroup( pMUD_histGrp, pMUD_histDat );
    }
  
  return( SUCCESS );
}



/*--------------------- DARC_hist_TD --------------------------*/
int
DARC_hist_TD( int ind, D_ODB *p_odb_data, caddr_t pHistData, 
	   MUD_SEC_GEN_HIST_HDR* pMUD_histHdr, 
	   MUD_SEC_GEN_HIST_DAT* pMUD_histDat )
/*--------------------------------------------------------------*/
{
  char tempString[256];
  int nt0, nt00, nt01, nmid, ncl;
  double n, rfsPerBin;
  int* h;
  int i;
  
  
  if(debug || debug_mud )  
    printf("DARC_hist_TD starting with pHistData = %p \n",pHistData);
  /*
    if(debug_dump)  /* if needed add debug_dump to if ( debug ... )  above   
    {
    for (i = db; i < db + dl ; i++)
    printf("Array element  (int *) pHistData[%d] = 0x%x or %d (dec) \n",
    i,((int *)pHistData)[i], ((int *)pHistData)[i]);
    }
  */               
  
  
  strncpy( tempString, p_odb_data->his_titles[ind], HIS_SIZE-1 );
  tempString[HIS_SIZE-1] = '\0';
  trimBlanks( tempString, tempString );
  pMUD_histHdr->title = strdup( tempString );
  if(debug_mud)
    {
      printf("DARC_hist_TD: Working on histogram  %s \n",pMUD_histHdr->title);
      printf("DARC_hist_TD: index = %d, histogram title = %s \n",
	     ind, pMUD_histHdr->title);
    }
  pMUD_histHdr->histType = MUD_SEC_TRI_TD_HIST_ID;
  
  /*
   *  TW/SD  TEMP  21-Dec-2000  Set the bytesPerBin to 4 to turn off packing
   *                            By default, the MUD hist header is created with
   *                            bytesPerBin = 0, which is a flag to the packing
   *                            routine to do packing.
   *                            For now, leave packing turned ON.
   *                            Note that the malloc of the pMUD_histDat->pData
   *                            above may cause memory allocation problems because
   *                            of allocating (nbins * bytes+3) bytes 
   */
  /*  pMUD_histHdr->bytesPerBin = 4; /* turns off packing */
  
  
  pMUD_histHdr->nEvents = (UINT32) p_odb_data->his_total[ind];
  if(debug_mud)
    printf("(UINT32)pMUD_histHdr->nEvents = %u\n",pMUD_histHdr->nEvents);
  pMUD_histHdr->nBins = p_odb_data->his_nbin;
  
  /* BNMR - specific values:  check client name  */
  if (strncmp(feclient,"febnmr",6) == 0 ||strncmp(feclient,"febnqr",6) == 0 )
  {
    
    /* BNMR/BNQR SIS cannot use fsPerBin - use nsPerBin instead
       - convert  dwell_time (ms)  to ns
       note dwell_time is a real number. 
    */
    
    rfsPerBin = (double)  (p_odb_data->dwell_time) * (double)1000000; /* ns !!*/
    pMUD_histHdr->fsPerBin = (UINT32) rfsPerBin;
    if(debug_mud)
      printf("DARC_hist_TD: real dwell_time = %f (ms), ns/bin= %d (mud stores as fs/bin) \n",
             p_odb_data->dwell_time, pMUD_histHdr->fsPerBin);
  }
  else /* non-BNMR clients : dwell time is actually the resolution code */
    {
      /* codes < 16 are from camac TDC now obsolete */
      if ( p_odb_data->dwell_time  < 16)
	{
	  cm_msg(MERROR,"DARC_hist_TD","TDC resolution code for obsolete CAMAC TDC detected (%d)",
		 p_odb_data->dwell_time);
	  return(DB_INVALID_PARAM);
	}
      else 
	{      /* codes > 16 are from VME TDC */
	  rfsPerBin = 48828.1250 * pow( (double)2.0, 
					(double) p_odb_data->dwell_time - 16);
	  /* for codes 16,17,18  use code values */
	  if ( p_odb_data->dwell_time < 19) 
	    pMUD_histHdr->fsPerBin = (UINT32)  p_odb_data->dwell_time ;
	  else
	    pMUD_histHdr->fsPerBin = (UINT32) rfsPerBin ;
	}
      if (debug_mud)printf("DARC_hist_TD: VME TDC resolution code tdc_res= %d, fs/bin= %d\n",
			   p_odb_data->dwell_time, pMUD_histHdr->fsPerBin);    
      
    }
  nt0 = p_odb_data->his_bzero[ind];
  if( nt0 == 0 )
    {
      if(debug_mud)
	printf("DARC_hist_TD: his_bzero[%d] is zero; calculation in progress\n",ind);
      
      h = (int*)pHistData;
      
      nt0 = pMUD_histHdr->nBins/2;
      ncl = 0;
      while( h[nt0-1] > ncl )
	{
	  nt0 /= 2;
	  ncl = h[nt0-1];
	}
      nt00 = nt0;
      nt01 = 2*nt0;
      nmid = ncl/3;
      for( nt0 = nt00; nt0 < nt01; nt0++ )
	if( h[nt0-1] > nmid ) break;
    }                        
  
  pMUD_histHdr->t0_bin = nt0;
  pMUD_histHdr->goodBin1 = p_odb_data->his_good_begin[ind];
  pMUD_histHdr->goodBin2 = p_odb_data->his_good_end[ind];
  pMUD_histHdr->t0_ps = 0.001*rfsPerBin*( (double)nt0 - 0.5 ) + 0.5;
  pMUD_histHdr->bkgd1 = p_odb_data->his_first_bkg[ind];
  pMUD_histHdr->bkgd2 = p_odb_data->his_last_bkg[ind];
  
  
  if(debug_mud)
    {
      printf("DARC_hist_TD: pMUD_histHdr->t0_bin = %d\n",pMUD_histHdr->t0_bin);
      printf("DARC_hist_TD: pMUD_histHdr->goodBin1 = %d\n",pMUD_histHdr->goodBin1);
      printf("DARC_hist_TD: pMUD_histHdr->goodBin2 = %d\n",pMUD_histHdr->goodBin2);
      printf("DARC_hist_TD: pMUD_histHdr->t0_ps = %d\n",pMUD_histHdr->t0_ps);
    }
  if ( pMUD_histHdr->bkgd1 == 0 && pMUD_histHdr->bkgd2 == 0)
    {
      if(debug)
	{
	  printf(" nt0 = %d\n",nt0);
	  printf("DARC_hist_TD: using calculated bckgnd values for histo \"%s\" (index %d) (none supplied)\n",
		 pMUD_histHdr->title,ind );
	}
    }
  else 
    {
      if(debug_mud)printf 
		     ("DARC_hist_TD: Supplied values of bkgd1,bkgd2 will be used\n");
    }
  
  
  if( pMUD_histHdr->bkgd1 == 0 && pMUD_histHdr->bkgd2 == 0 && nt0 > 1 )
    {
      int kmid, kb1, kb2;
      int nb;
      double avg, diff, err, val;
      int* h;
      
      
      if(debug_mud) printf("DARC_hist_TD:  Calculating bkgd1 and bkgd2 values...\n");
      
      h = (int*)pHistData;
      kmid = nt0/2;
      nb = 0;
      avg = ( h[kmid-2] + h[kmid-1] + h[kmid] )/3;
      for( kb1 = kmid-1; kb1 >= 0; kb1-- )
	{
	  val = fabs( (double)h[kb1] );
	  diff = fabs( val - avg );
	  err = _max( 1.0, sqrt( avg ) );
	  if( diff > 5*err ) break;
	  avg = nb*avg + val;
	  nb++;
	  avg = avg/nb;
	}
      kb1 = _min( kb1 + 5, kmid-1 );
      
      for( kb2 = kmid-1; kb2 < nt0; kb2++ )
	{
	  val = fabs( (double)h[kb2] );
	  diff = fabs( val - avg );
	  err = _max( 1.0, sqrt( avg ) );
	  if( diff > 5*err ) break;
	  avg = nb*avg + val;
	  nb++;
	  avg = avg/nb;
	}
      kb2 = _max( kmid-1, kb2 - 5 );
      
      pMUD_histHdr->bkgd1 = kb1 + 1;
      pMUD_histHdr->bkgd2 = kb2 + 1;
    }
  if(debug_mud)
    {
      printf("DARC_hist_TD: pMUD_histHdr->bkgd1 = %d\n",pMUD_histHdr->bkgd1);
      printf("DARC_hist_TD: pMUD_histHdr->bkgd2 = %d\n",pMUD_histHdr->bkgd2);
    }
  
  pMUD_histHdr->nBytes = MUD_SEC_GEN_HIST_pack( pMUD_histHdr->nBins,
                                                4, pHistData,
                                                pMUD_histHdr->bytesPerBin, pMUD_histDat->pData );
  pMUD_histDat->nBytes = pMUD_histHdr->nBytes;
  
  return( SUCCESS );
}


/*---------------------- DARC_comments ------------------------------*/
MUD_SEC_GRP* 
DARC_comments( D_ODB *p_odb_data )
/*-------------------------------------------------------------------*/
/*   Read Comment file into mud section, returning pointer, or NULL
/*-------------------------------------------------------------------*/
{
  MUD_SEC_GRP* pMUD_cmtGrp;
  char cmtFile[128];
  FILE* fin;
  
  sprintf( cmtFile, "%s/%06d.cmt",p_odb_data->save_dir,p_odb_data->run_number );
  if (debug) printf("DARC_comments: Looking for any comments in file %s\n",cmtFile);

  fin = MUD_openInput( cmtFile );
  if ( fin == NULL )
    {
      if (debug) printf("DARC_comments: comment file %s found\n",cmtFile );
      return ( NULL );
    }

  if (debug) printf("DARC_comments","using comment file %s\n", cmtFile );
 
  pMUD_cmtGrp = MUD_readFile( fin );
  if( pMUD_cmtGrp != NULL )
    {
      if( MUD_instanceID( pMUD_cmtGrp ) != MUD_GRP_CMT_ID )
        {
          cm_msg(MINFO,"DARC_comments","failed to read comment file %s", cmtFile );
          MUD_free( pMUD_cmtGrp );
          pMUD_cmtGrp = NULL;
        }
    }
  else
    {
      cm_msg(MINFO,"DARC_comments","failed to read comment file %s", cmtFile );
    }

  fclose( fin );

  return( pMUD_cmtGrp );
}

/*-------------- DARC_release_camp -----------------------------*/
void
DARC_release_camp()
/*---------------------------------------------------------------*/
{
  if(campInit)
    {
      if(debug || debug_mud)
	printf("DARC_release_camp: calling camp_clntEnd()\n");
      camp_clntEnd();    /* call this only when mdarc exits */
      if(debug_mud) printf("DARC_release_camp: end of run or mdarc exiting: releasing camp connection\n");
      campInit=FALSE;
    }
  else
    if(debug_mud)
      printf("DARC_release_camp: end of run or mdarc exiting: camp connection had not been established\n");
}


/* ------------------------ DARC_camp_log_TD ---------------------------*/
void
DARC_camp_log_TD( D_ODB* p_odb_data, double dtotal_save )
/*----------------------------------------------------------------------*/
/*  Collect statistics for all logged Camp variables.  Give results
 *  in MUD section pMUD_indVarGrp_gbl.
 */
{
  int status;

  /* Global variables:
   * BOOL    campInit, campUpdated, debug, debug_mud
   * struct  fmdarc, aveKeys
   * pointer pCampSums 
   * pointer pVarList (from camp.h)
   * double  tot_events_gbl
   * int     numWtLogged_gbl
   * handle  hDB
   */

  if( campInit )
    {
      /*
       *  Get an update of the CAMP database
       */
      if( !campUpdated )
	{
	  if(debug) printf("Calling camp_clntUpdate()\n");
	  status = camp_clntUpdate();
	  if( status != CAMP_SUCCESS )
	    cm_msg(MERROR,"DARC_camp_logging_TD","failed call to update CAMP data (%d)",status );
	  else
	    campUpdated = TRUE ;
	}

      /*
       *  Check and possibly create ODB area for averages.
       */
      if(fmdarc.camp.enable_weighted_averaging)
        {
          DARC_odb_init_aver_rec();
        }

      if(campUpdated)
	{
	  status = campSrv_varGet( "/", CAMP_XDR_ALL );
	  if( status != CAMP_SUCCESS )
            {
              cm_msg(MERROR,"DARC_write_TD","failed call to retrieve CAMP data (%d)",status );
            }
	  else /* Update camp statistics */
	    {
              /*
               *  Remove any saved records when not weighting or if events count decreased
               */
              if( (!fmdarc.camp.enable_weighted_averaging) || (dtotal_save+0.4 < tot_events_gbl) )
                {
                  if ( pCampSums ) {
                    DARC_remove_camp_records( pCampSums );
                    pCampSums = NULL;
                  }
                  tot_events_gbl = 0.0;
                }

              tot_events_gbl = dtotal_save;

	      /*
	       *  Call proc DARC_camp_var for each variable
	       */

	      if(debug || debug_mud)printf("Calling camp_varDoProc\n");

	      camp_varDoProc_recursive( DARC_camp_var, pVarList );

              if( fmdarc.camp.enable_weighted_averaging && aveKeys.init )
                status = db_set_data(hDB, aveKeys.num, (void*)&numWtLogged_gbl, sizeof(INT), 1, TID_INT);
	      write_client_logging(1, 1); // camp logging is on

	    }

	} // campUpdated

    } // if (campInit)
  else  /* Camp failed to connect */
    {
      cm_msg(MINFO,"DARC_write_TD","Failed initialize call to CAMP");
      write_client_logging(1, 0) ;// camp logging is off
    }

  return;
}


/*--------------- DARC_camp_var ----------------------------*/
void
DARC_camp_var( CAMP_VAR* pVar )
/*------------------------------------------------------*/
{
  /* Calculate statistics for a particular logged Camp variable
   * Global vars:
   *   int    numIndVar
   *   int    numWtLogged_gbl
   *   MUD_SEC_GRP* pMUD_indVarGrp_gbl
   *   BOOL    fmdarc.camp.enable_weighted_averaging 
   *   double  tot_events_gbl
   *   CAMP_VAR_STATS* pCampSums
   */
  MUD_SEC_GEN_IND_VAR* pMUD_indVar;
  CAMP_NUMERIC* pNum;
  
  double f, delta;
  CAMP_VAR_STATS *pRec;

  /*
   *  Check if the variable is to be logged, allowing various 
   */
  if( !( pVar->core.status & CAMP_VAR_ATTR_LOG ) ) return;
  if( ! ( streq( pVar->core.logAction, "log_td_musr" ) || 
          streq( pVar->core.logAction, "log_mdarc_td" ) || 
          streq( pVar->core.logAction, "log_mdarc" ) ||
          streq( pVar->core.logAction, expt_name ) ) ) return;
  
  /*
   *  Create the group if it does not exist
   */
  if( pMUD_indVarGrp_gbl == NULL )
    {
      pMUD_indVarGrp_gbl = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_GRP_GEN_IND_VAR_ID );
      if(debug_mud)printf("DARC_camp_var: logging CAMP sections\n" );
    }
  
  if ( pVar->core.varType & CAMP_VAR_TYPE_NUMERIC )
    {
      numIndVar++;
      pMUD_indVar = (MUD_SEC_GEN_IND_VAR*) MUD_new( MUD_SEC_GEN_IND_VAR_ID, numIndVar );

      pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

      pMUD_indVar->name = strdup( pVar->core.path );
      pMUD_indVar->description = strdup( pVar->core.title );
      pMUD_indVar->units = strdup( pNum->units );
      pMUD_indVar->low = pNum->low;
      pMUD_indVar->high = pNum->hi;

      if( fmdarc.camp.enable_weighted_averaging )
        { /*  For statistics weighted by number of events... 
           *
           *  Locate record of previous sums:
           */
          //printf(">>>>>>> Calculate weighted Camp stats for %s\n", pMUD_indVar->name);
          pRec = pCampSums;

          if( pCampSums )
            {
              for ( ; pRec != NULL ; pRec = pRec->pNext )
                {
                  if( 0 == strcmp( pRec->path, pMUD_indVar->name) )
                    break;
                }
            }

          /*
           *  If no record exists; create new record
           */
          if( pRec == NULL )
            {
              //printf( ">>>>>>> No record, so create one\n");
              pRec = DARC_new_camp_sums( );
              pRec->path = strdup( pVar->core.path );
            }

          /* 
           *  If fewer readings than previous, or basis changed, then zero the record
           */
          if( pNum->num < pRec->raw.num || tot_events_gbl < pRec->weighted.num ||
                    fabs(pNum->offset - pRec->raw.offset) > fabs(1.0e-12 * pNum->offset) )
            {
              printf( "Reset statistics for Camp var %s (reasons: r %d c %d b %d)\n", 
		      pMUD_indVar->name, pNum->num < pRec->raw.num, tot_events_gbl < pRec->weighted.num,
		      fabs(pNum->offset - pRec->raw.offset) > fabs(1.0e-12 * pNum->offset) );
              DARC_zero_camp_sums( pRec );
            }

          if( pNum->num > pRec->raw.num )
            {
              /*
               *  If there are new Camp readings, select just the new contributions and
               *  re-weight them by the number of new counts; contribute to weighted sums.
               */
              f = (tot_events_gbl - pRec->weighted.num) / (pNum->num - pRec->raw.num);
              pRec->weighted.sum += ( pNum->sum - pRec->raw.sum ) * f;
              pRec->weighted.sumSquares += ( pNum->sumSquares - pRec->raw.sumSquares ) * f;
              pRec->weighted.sumCubes += ( pNum->sumCubes - pRec->raw.sumCubes ) * f;
            }
          else if ( tot_events_gbl > pRec->weighted.num )
            { 
              /* 
               *  For no new readings, advance weghted sums using current value 
               */
              f = (tot_events_gbl - pRec->weighted.num) / 1.0;
              delta = pNum->val - pNum->offset;
              pRec->weighted.sum += delta * f;
              pRec->weighted.sumSquares += delta * delta * f;
              pRec->weighted.sumCubes += delta * delta * delta * f;
            }
          pRec->weighted.num = tot_events_gbl;
          pRec->weighted.offset = pNum->offset;

          /* 
           *  Save current raw camp stats, for comparison next time
           */
          pRec->raw.num = pNum->num;
          pRec->raw.sum = pNum->sum;
          pRec->raw.sumSquares = pNum->sumSquares;
          pRec->raw.sumCubes = pNum->sumCubes;
          pRec->raw.offset = pNum->offset;

          /*
           *  Calculate stats from weighted sums; like camp_varNumCalcStats
           */
          if( tot_events_gbl < 2.0 || pNum->num < 1 )
            {
              pMUD_indVar->mean = pNum->val;
              pMUD_indVar->stddev = 0.0;
              pMUD_indVar->skewness = 0.0;
            }
          else
            {
              double sum, mean, var, stddev;
              sum = pRec->weighted.sum;
              pMUD_indVar->mean = mean = sum/tot_events_gbl + pNum->offset ;
              /* 
               * The variance usually has n-1 in the denominator, reflecting the number of
               * degrees of freedom.  More events do not really increase the number of 
               * measurements, so we scale the "1" up the same as we scaled from n to
               * tot_events.  But we add 1 to the (raw) n to allow the uncounted previous
               * reading to be tallied.
               */
              var = ( pRec->weighted.sumSquares - ( (sum*sum)/tot_events_gbl) ) / 
                         ( tot_events_gbl - tot_events_gbl/(pNum->num+1) );
              pMUD_indVar->stddev = stddev = sqrt( fabs( var ) );
              if( stddev == 0.0 )
                {
                  pMUD_indVar->skewness = 0.0;
                }
              else
                {
                  pMUD_indVar->skewness = ( pRec->weighted.sumCubes - 3.0*mean*pRec->weighted.sumSquares +
                           2.0*(sum*sum*sum)/(tot_events_gbl*tot_events_gbl) ) /
                           ( tot_events_gbl*stddev*stddev*stddev );

                }
            }
        }
      else /* unweighted -- taken directly from Camp */
        {
          if( pNum->num < 2 )
            {
              pMUD_indVar->mean = pNum->val;
              pMUD_indVar->stddev = 0.0;
              pMUD_indVar->skewness = 0.0;
            }
          else
            {
              camp_varNumCalcStats( pVar->core.path, &pMUD_indVar->mean, 
                                    &pMUD_indVar->stddev, &pMUD_indVar->skewness );
            }
        }

      MUD_addToGroup( pMUD_indVarGrp_gbl, pMUD_indVar );
      DARC_odb_set_average( numWtLogged_gbl, pMUD_indVar );
      numWtLogged_gbl++;
    }
  else if ( pVar->core.varType & ( CAMP_VAR_TYPE_STRING | CAMP_VAR_TYPE_SELECTION ) )
    { /* 
       *  This is a loggable selection or string variable
       */

      numIndVar++;
      
      pMUD_indVar = (MUD_SEC_GEN_IND_VAR*) MUD_new( MUD_SEC_GEN_IND_VAR_ID, numIndVar );
      
      pMUD_indVar->name = strdup( pVar->core.path );
      pMUD_indVar->description = strdup( pVar->core.title );
      
      if ( pVar->core.varType & CAMP_VAR_TYPE_STRING )
        {   /* String:  store zeros, but put the string in "units" */
	  pMUD_indVar->mean = 0.0;
	  pMUD_indVar->units = strdup( pVar->spec.CAMP_VAR_SPEC_u.pStr->val );
        }
      else /* CAMP_VAR_TYPE_SELECTION */
        {   /* Selection:  save the number as "mean" and the label as "units" */
	  char selbuff[255];

	  pMUD_indVar->mean = (double) pVar->spec.CAMP_VAR_SPEC_u.pSel->val ;

	  varSelGetIDLabel( pVar, pVar->spec.CAMP_VAR_SPEC_u.pSel->val, selbuff, sizeof(selbuff));
          selbuff[254] = '\0';
	  pMUD_indVar->units = strdup( selbuff );
        }
      
      pMUD_indVar->low = 0.0;
      pMUD_indVar->high = 0.0;
      pMUD_indVar->stddev = 0.0;
      pMUD_indVar->skewness = 0.0;
      
      MUD_addToGroup( pMUD_indVarGrp_gbl, pMUD_indVar );
    }
}

/* 
 * Remove all Camp variable statistics records (recursively traverses list)
 */
void
DARC_remove_camp_records( CAMP_VAR_STATS * pcamprec )
{
  if( pcamprec )
    {
      if( pcamprec->pNext ) DARC_remove_camp_records( pcamprec->pNext );
      if( pcamprec->path ) free (pcamprec->path);
    }
}

void 
DARC_zero_camp_sums( CAMP_VAR_STATS * prec )
{
  if( prec )
    {
      prec->raw.num = 0.0;
      prec->raw.sum = 0.0;
      prec->raw.sumSquares = 0.0;
      prec->raw.sumCubes = 0.0;
      prec->raw.offset = 0.0;

      prec->weighted.num = 0.0;
      prec->weighted.sum = 0.0;
      prec->weighted.sumSquares = 0.0;
      prec->weighted.sumCubes = 0.0;
      prec->weighted.offset = 0.0;
    }
}

/*
 *  Ensure existance of the ODB variables for weighted Camp averages.
 *  Record their keys in the global (struct) variable aveKeys.
 *
 *  Parameters:    None listed, but uses globals.
 *  Gets Globals:  ODB handle hDB
 *                 string odb_average_area
 *                 string odb_average_def_str
 *  Returns:       int status: DB_SUCCESS for success.
 *  Sets Globals:  keys structure aveKeys
 */

int
DARC_odb_init_aver_rec( )
{
  INT status;
  int j;

  for ( j=0; j<2; j++ )
    {
      status = db_find_key( hDB, 0, (void*)odb_average_area, &aveKeys.area );
      if( status == DB_SUCCESS ) 
        status = db_find_key( hDB, aveKeys.area, "number", &aveKeys.num );
      if( status == DB_SUCCESS ) 
        status = db_find_key( hDB, aveKeys.area, "variable", &aveKeys.name );
      if( status == DB_SUCCESS ) 
        status = db_find_key( hDB, aveKeys.area, "mean", &aveKeys.mean );
      if( status == DB_SUCCESS ) 
        status = db_find_key( hDB, aveKeys.area, "stddev", &aveKeys.stddev );

      if( status == DB_SUCCESS )
        {
          aveKeys.init = TRUE;
          break;
        }
      else
        {
          status = db_create_record( hDB, 0, (char*)odb_average_area, (char*)odb_average_def_str );
          aveKeys.init = FALSE;
        }
    }
  return (status);
}

/*
 * Save the list of numeric logged variables in the ODB, along with
 * their weighted mean and error.  Use our own area of the ODB not
 * covered by any fixed-size structures, because these lists can
 * grow.
 *        INT    /averages/number
 *        STRING /averages/variable[n]
 *        DOUBLE /averages/mean[n]
 *        DOUBLE /averages/stddev[n]
 *
 */

void
DARC_odb_set_average( int num, MUD_SEC_GEN_IND_VAR* pindVar )
{
  
  INT   status1,status2,status3;
  HNDLE hKey;
  KEY   key;

  if( !aveKeys.init ) return;

  /* enlarge name strings, if necessary; must apply to all strings in array */
  db_get_key(hDB, aveKeys.name, &key);
  if (strlen(pindVar->name) + 1 > key.item_size )
    {
      key.item_size = strlen(pindVar->name) + 1;
    }

  status1 = db_set_data_index(hDB, aveKeys.name, pindVar->name, key.item_size, num, TID_STRING);
  status2 = db_set_data_index(hDB, aveKeys.mean, (void*)&(pindVar->mean), sizeof(double), num, TID_DOUBLE);
  status3 = db_set_data_index(hDB, aveKeys.stddev, (void*)&(pindVar->stddev), sizeof(double), num, TID_DOUBLE);

  return;
}

/*
 *  Create new Camp var stats record; return pointer to it.
 */
CAMP_VAR_STATS * DARC_new_camp_sums( )
{
  /* Uses global var pCampSums */
  CAMP_VAR_STATS * prec;
  CAMP_VAR_STATS * pr;
  
  prec = malloc( sizeof(CAMP_VAR_STATS) );

  DARC_zero_camp_sums( prec );

  prec->pNext = NULL;
  prec->path = NULL;

  if( pCampSums )
    { /* Append this to tail of existing list */
      for ( pr = pCampSums; pr->pNext != NULL; pr = pr->pNext ) {} ;
      pr->pNext = prec;
    }
  else /* this is the first (only) record */
    {
      pCampSums = prec;
    }

  return( prec );
}

#define  _dup_desc_str( desc_item, str ) \
  strncpy(tempString, str, sizeof(str)); \
  tempString[sizeof(str)] = '\0'; \
  trimBlanks( tempString, tempString ); \
  desc_item = strdup( tempString )


/* ------------- DARC_runDesc_TD -------------------------------------*/
int
DARC_runDesc_TD( D_ODB* p_odb_data, MUD_SEC_GEN_RUN_DESC* pMUD_desc )
/*-------------------------------------------------------------------*/
{
  int status;
  int i;
  int tempTime[6];
  char tempString[256];
  int timadr[2];
  short timbuf[7];
  char buffer[128];
  char* pos;
  
  if(debug_mud)printf ("DARC_runDesc_TD: starting for run %d  \n",p_odb_data->run_number);
  
  pMUD_desc->runNumber = p_odb_data->run_number;
  pMUD_desc->exptNumber = p_odb_data->experiment_number;
  
  
  
  /* for linux use binary run start time directly */
  pMUD_desc->timeBegin = p_odb_data->start_time_binary ;    
  if( p_odb_data->run_state == STATE_RUNNING)
    {
      time( (time_t *)&pMUD_desc->timeEnd ); /* linux for now assume run ends now */        
      if(debug_mud) printf ("DARC_runDesc_TD: Run in progress - using current time for timeEnd \n");
    }
  else
    {
      pMUD_desc->timeEnd = p_odb_data->stop_time_binary ; /* run ended */
      if(debug_mud) printf ("DARC_runDesc_TD: Run NOT in progress - using saved time for timeEnd \n");
    }
  
  pMUD_desc->elapsedSec = pMUD_desc->timeEnd - pMUD_desc->timeBegin;
  
#ifdef MUSR
  pMUD_desc->method = strdup( "TD-�SR" ); /* TD MUSR */
#else
  sprintf( tempString, "TD-%s", beamline ); /* BNMR or BNQR type 2 */
  pMUD_desc->method = strdup( tempString );
#endif
  pMUD_desc->lab = strdup( "TRIUMF" );
  pMUD_desc->das = strdup( "MIDAS" ); /* different DAQ !! */
  
  pMUD_desc->area = strdup( beamline );
  
  _dup_desc_str( pMUD_desc->title, p_odb_data->run_title );

  _dup_desc_str( pMUD_desc->sample, p_odb_data->sample );

  _dup_desc_str( pMUD_desc->temperature, p_odb_data->temperature );

  _dup_desc_str( pMUD_desc->field, p_odb_data->field );

  _dup_desc_str( pMUD_desc->orient, p_odb_data->orientation );

  /* use rig name from ODB as apparatus, and mode as insert */
  _dup_desc_str( pMUD_desc->apparatus, p_odb_data->rig );

  _dup_desc_str( pMUD_desc->insert, p_odb_data->mode );

  _dup_desc_str( pMUD_desc->experimenter, p_odb_data->experimenter );
   
  if(debug_mud)
    {
      printf("Run descriptor:\n");
      printf("pMUD_desc->title = %s\n",pMUD_desc->title );
      printf("pMUD_desc->sample = %s\n",pMUD_desc->sample );
      printf("pMUD_desc->temperature = %s\n",pMUD_desc->temperature );
      printf("pMUD_desc->field = %s\n",pMUD_desc->field );
      printf("pMUD_desc->orient = %s\n",pMUD_desc->orient );
      printf("pMUD_desc->apparatus = %s\n",pMUD_desc->apparatus );
      printf("pMUD_desc->insert = %s\n",pMUD_desc->insert );
      
    }
  return( SUCCESS );
}


/*-------------- DARC_scalers ------------------------------*/
int
DARC_scalers( D_ODB* p_odb_data, MUD_SEC_GRP* pMUD_scalGrp )
/*------------------------------------------------------------*/
{
  char str[256];
  int i;
  MUD_SEC_GEN_SCALER* pMUD_scal;
  char* s;
  
  if(debug_mud) printf("DARC_scalers starting with nscal=%d\n",p_odb_data->nscal);
  
  for( i = 0; i < p_odb_data->nscal; i++ )
    {
      pMUD_scal = (MUD_SEC_GEN_SCALER*)MUD_new( MUD_SEC_GEN_SCALER_ID, i+1);
      pMUD_scal->counts[0] = (UINT32)p_odb_data->scaler_save[i];
#ifdef MUSR
      pMUD_scal->counts[1] = (UINT32)p_odb_data->scaler_rate[i];
#endif
      pMUD_scal->label = strdup(p_odb_data->scaler_titles[i]);
      
      
      if(debug_mud) 
	printf("DARC_scalers: Scaler %d label=%s contents (uint)=%u\n",
	       i, pMUD_scal->label, pMUD_scal->counts[0] );
      MUD_addToGroup( pMUD_scalGrp, pMUD_scal );
    }
  return( SUCCESS );
}



/*----------- darc_check_files -------------------------------*/  
INT
darc_check_files(D_ODB *p_odb_data, INT keep, INT *next_version)
/*------------------------------------------------------------*/
{
  /*
    This function deals with files of the form *_v* (i.e. version files)  
    
    For run = run_number, returns no. of files on the disk, and the next version
    number.  Purges files depending on the value of parameter keep (the number
    returned is the number of files before purging).
    
    Input   
    D_OBD *p_odb_data   pointer to odb data
    INT   keep          no. of files to keep for this run.
    if keep = 0   NO PURGE, returns no. of files found
    keep > 1   Files will be purged, retaining keep files on the disk.           
    
                             
    Output
    INT   *next_version    Version number of the next run file.
    returns                no. of files on the disk for this run.
    
    Note - on error, next_version = 0
  */
  
  
  
  char dir[128];
  int nfile,i,j, status;
  char * list = NULL;
  char outFile[128];
  char *pt;
  char filename[MAX_FILE_PATH];
  char *s;
  int max, min, ver, index;
  
  
  if(debug_check)
    {
      printf("\n");
      if (keep>0 )
	printf("darc_check_files starting, will purge if more than %d files for this run \n",keep);
      else
	printf("darc_check_files starting, will NOT purge \n");
    }
  *next_version = 0;  /* indicates error */
  max = 0; /* gives next_version = 1 if no files are found */
  min = 1000000000;
  sprintf(filename,"%06d.msr_v*",p_odb_data->run_number);
  sprintf(dir,"%s/",p_odb_data->save_dir);
  if(debug_check)
    {
      printf("p_odb_data->run_number=%d\n",p_odb_data->run_number);
      printf("p_odb_data->save_dir=%s\n",p_odb_data->save_dir);               
      printf("Looking for files: %s%s\n",dir,filename);
 
      printf("&list=%p;  list=%s\n",&list,list);
   }
  nfile = ss_file_find(dir, filename, &list);
  if(debug_check) 
    {  
      printf ("Found  %d files for this run \n",nfile);
      printf ("List: %s\n",list);
      printf("outFile:%s\n");
    }
  for ( j=0; j<nfile; j++ )
    {
      pt = list + j*MAX_STRING_LENGTH;

      if ( sscanf( pt, "%*06d.msr_v%d", &ver ) == 1 ) 
        {
          max = (ver > max ? ver : max);
          min = (ver < min ? ver : min);
        }
      else
	{
	  if(debug_check)  printf ("Ignore badly named file: %s\n", pt);
	}
    }
  if (debug_check) printf("max = %d,   min = %d\n", max, min);
  *next_version = max+1;

  // Free memory from list of file names (allocated in ss_file_find).
  free(list);

  if (debug_check) printf("returning next_version = %d\n",*next_version);
  if (keep < 1) return(nfile);  /* no purge wanted  */

  if (nfile <= keep)
    {
      if(debug_check)
	printf("returning as not enough files to purge, nfile=%d keep=%d\n",nfile,keep);      
      return (nfile); /* not enough files to purge */  
    }
  /* now purge the files */
  for (ver=min; ver<=max-keep; ver++)
    {
      sprintf(outFile,"%s%06d.msr_v%d",dir,p_odb_data->run_number,ver);

      if (debug_check) printf("darc_check_files: About to delete file %s (index=%d)\n",outFile,j);
      
      status = ss_file_remove(outFile);
      /* 
       * Ignore return status because we didn't verify that this particular file exists.
       */
    }

  return(nfile);
}


/*----------------- odb_save --------------------------*/
INT
odb_save ( INT run_number, char *odb_save_dir)
/*------------------------------------------------------*/
{
  /*  odb_save is modified from function in /midas/src/mlogger.c */
  
  int  size;
  char filename[20];
  char path[256];
  INT status; 
  
  if(debug || debug_check)
    {
      printf("\n");
      printf("odb_save: Starting with run_number: %d & odb_save_dir: %s\n",run_number,odb_save_dir);
    }
  strcpy(path,odb_save_dir); 
  if (path[0] != 0)
    if (path[strlen(path)-1] != DIR_SEPARATOR)
      strcat(path, DIR_SEPARATOR_STR);
  
  sprintf(filename,"%06d.odb",run_number);  
  strcat(path, filename);
  cm_msg(MINFO,"odb_save","Saving odb into %s",path); 
  
  status = db_save(hDB, 0, path, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MINFO,"odb_save","cannot save ODB");
      write_message1(status,"odb_save");
    }
  return(status);
}


/*------------ DARC_camp_connect --------------------------------*/
INT
DARC_camp_connect ( char * camp_host )
/*-------------------------------------------------------------*/
{
  int status;
  int len, j;
  int camp_running;
  char ctemp[LEN_NODENAME+1];
  char serverName[LEN_NODENAME+1];
  int debug = 1;
  /* Important globals: campInit, campFail, firstxTime  */
  
  if(debug) printf("DARC_camp_connect: Connect to Camp\n");
#ifndef MUSR
  write_client_code(CAMP_LOG_ERR,SET,"mdarc"); // write in case of timeout 
#endif
  camp_running = FALSE ;
  if (campFail)  /* camp has failed initialize. Retrying often leads to a hang */
    {
      cm_msg(MINFO,"DARC_camp_connect","Camp failed to initialize. Camp sections cannot be logged");
    }
  else
    {
      len = strlen(camp_host);
      if(debug) printf("DARC_camp_connect : camp hostname  = %s, string length= %d \n",camp_host, len);
      if (len > 0)    /* a name is specified */
	{ 
	  strcpy (ctemp,camp_host );    
	  for  (j=0; j< LEN_NODENAME ; j++) ctemp[j] = toupper (ctemp[j]); /* convert to upper case */                
	  trimBlanks (ctemp,ctemp);
	  if (strncmp(ctemp,"NONE",4) != 0)
	    {
	      strcpy( serverName, ctemp);
	      if(debug)printf("DARC_camp_connect : Camp hostname: %s\n",serverName );
	      camp_running  = TRUE   ; /* camp is running */
	    }
	}
    }
  
  if( camp_running )
    {
      
      /* ping the camp host because if someone turns off the crate, there is
	 no timeout and it can waste a lot of time  */ 
      {
	char cmd[256];
	sprintf(cmd,"ping -c 1 %s &>/dev/null",camp_host);
	if(debug) printf(" pinging camp host using command: %s\n",cmd);
	status=system(cmd);
      }
      if(status)
	cm_msg(MERROR,"DARC_camp_connect","Camp host %s is unreachable. Camp data cannot be saved",
	       camp_host);
      else
	{
	  
	  /*
	   *  Initialize CAMP connection  
	   *  (timeout of 10 sec.)
	   */   
	  if(firstxTime)
	    {
	      if(debug)printf("firstxTime through: calling camp_clntInit\n");
	      status = camp_clntInit( serverName, 10 );
	      if(status == CAMP_SUCCESS)
		{
		  campInit = TRUE;
		  firstxTime = FALSE ;
		  if(debug)printf("Returned from camp_clntInit with CAMP_SUCCESS. Set campInit TRUE & firstxTime FALSE\n");
		}
	      else
		{
		  cm_msg(MERROR,"DARC_camp_connect","Failed initialize call to CAMP");
		  printf("Returned from camp_clntInit with CAMP_FAILURE\n");
		  campInit = FALSE;
		  campFail = TRUE; /* set a flag to prevent retry */
		}
	    }
	} // end of camp_host reachable
    }  // end of camp_running
  else   /* camp not running */
    {  /* (if campFail is true, a message has already been written) */
      if (!campFail) cm_msg(MINFO,"DARC_camp_connect","Warning: CAMP is not running");
      campInit = FALSE;
    }
  
#ifndef MUSR
  if(campInit)write_client_code(CAMP_LOG_ERR,CLEAR,"mdarc"); // clear 
#endif
  return (campInit) ;
  
}


/*--------------DARC_get_dynamic_headers -------------------*/
int
DARC_get_dynamic_headers( D_ODB *p_odb_data )
/*-----------------------------------------------------------*/
{
  /* 
   * DARC_get_dynamic_headers: fill the temperature and/or field run header
   * (p_odb_data->) with the average value from a Camp variable.  All failures
   * are silently ignored -- always return SUCCESS status.
   *
   * If the camp variable is logged, then use the (perhaps weighted) mean 
   * determined previously. (Requires that logging of independent variables
   * be done before setting dynamic headers.)
   *
   * Don't yet know what to do with an automatic RF Power variable when there
   * is no Power header in the MUD file format.
   */

  CAMP_VAR* pVar;
  CAMP_NUMERIC* pNum;

  MUD_SEC_GEN_IND_VAR* pMUD_indVar;

  int status;
  double val, stddev, skewness;
  char units[16], str[32];
  char *pUnits;
  int n;
  BOOL eflag = TRUE;

  if( p_odb_data->field_variable[0] == '/' || p_odb_data->temperature_variable[0] == '/' )
  {

    DARC_camp_connect( p_odb_data->camp_host );

    if( campInit )
    {

      if( ! campUpdated ) 
      {
        status = camp_clntUpdate();
        if( status == CAMP_SUCCESS )
          campUpdated = TRUE ;
	else
	  cm_msg(MERROR,"DARC_get_dynamic_headers","failed call to update CAMP data (%d)",status );
      }

      /****** Automatic Field header ******/

      stddev = -2.0;
      pUnits = NULL;

      if( campUpdated && p_odb_data->field_variable[0] == '/' )
      {
        //printf("Automatic field\n");
        /* see if we calculated it already, saved in MUD section pMUD_indVarGrp_gbl. */
        pMUD_indVar = DARC_find_mud_indvar( p_odb_data->field_variable, pMUD_indVarGrp_gbl );
        if( pMUD_indVar )
        {
          /* found result; use it (average might be weighted by histogram counts) */
          //if(debug) printf("Re-use mean for field variable %s: %f %f\n", 
          //         p_odb_data->field_variable, pMUD_indVar->mean, pMUD_indVar->stddev );
          val = pMUD_indVar->mean;
          stddev = pMUD_indVar->stddev;
          pUnits = pMUD_indVar->units;
        }
        else
        {
          /* result not logged yet; get it from Camp */
          //if(debug) printf("Field variable %s not logged; get it from Camp\n", p_odb_data->field_variable);
          status = campSrv_varGet( p_odb_data->field_variable, CAMP_XDR_NO_CHILD );
          if( status != CAMP_SUCCESS )
          {
            cm_msg(MERROR,"DARC_get_dynamic_headers","Camp variable %s does not exist", 
                   p_odb_data->field_variable );
          }
          else
          {
            pVar = camp_varGetp( p_odb_data->field_variable );
            if( (pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK) != CAMP_VAR_TYPE_NUMERIC )
            {
              cm_msg(MERROR,"DARC_get_dynamic_headers","Camp variable %s is not numeric", 
                     p_odb_data->field_variable );
            }
            else
            {
              pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;
              pUnits = pNum->units;
              if( pNum->num < 2 )
              { /* not enough readings for an error bar */
                val = pNum->val;
                stddev = 0.0;
              }
              else
              { /* We want to calculate statistics */
                camp_varNumCalcStats( pVar->core.path, &val, &stddev, &skewness );
              }
            }
          }
        }
        if(debug) printf("Field value %f (%f)\n", val, stddev);
        /* Got field value.  Now do units conversion.
         * (see also title_validate in td_extra.tcl)
         */
        if( stddev > -1.0 )
        {
          strcpy( units, "G");
          if( pUnits )
          {
            strncpy( units, pUnits, 4 );
            units[0] = toupper( units[0] );
            units[1] = toupper( units[1] );
            units[3] = '\0';
            if( units[0] == 'G' || units[0] == 'O' )
            {
              strcpy( units, "G");
            }
            else if( units[0] == 'T' )
            {
              val = val*10000. ;
              stddev = stddev*10000. ;
              strcpy( units, "G");
            }
            else if( units[0] == 'M' && units[1] == 'T' )
            {
              val = val*10.0 ;
              stddev = stddev*10. ;
              strcpy( units, "G");
            }
            else if( units[0] == 'K' && ( units[1] == 'G' || units[1] == 'O' ) )
            {
              val = val*1000.0 ;
              stddev = stddev*1000. ;
              strcpy( units, "G");
            }
            else if( units[0] == 'M' && ( units[1] == 'G' || units[1] == 'O' ) )
            {
              val = val/1000.0 ;
              stddev = stddev/1000. ;
              strcpy( units, "G");
            }
            else if( isalpha(*pUnits) )
	    {
              strncpy( units, pUnits, 15 );
              units[15] = '\0';
            }
            else 
            {
              strcpy( units, "G" );
            }
          }

          eflag = fmdarc.camp.record_field_error;
          n = sizeof(p_odb_data->field) - 1 ;

          DARC_head_num( str, n, val, stddev, units, eflag ) ;

          strncpy( p_odb_data->field, str, n );
          p_odb_data->field[n] = '\0';
        }
      }  /* finished field variable */

      /****** Automatic Temperature header ******/

      stddev = -2.0;
      pUnits = NULL;

      if( campUpdated && p_odb_data->temperature_variable[0] == '/' )
      {
        //printf("Automatic temperature\n");
        /* see if we calculated it already, saved in MUD section pMUD_indVarGrp_gbl. */
        pMUD_indVar = DARC_find_mud_indvar( p_odb_data->temperature_variable, pMUD_indVarGrp_gbl );
        if( pMUD_indVar )
        {
          /* found result; use it (average might be weighted by histogram counts) */
          //if(debug) printf("Re-use mean for temperature variable %s.\n", p_odb_data->temperature_variable);
          val = pMUD_indVar->mean;
          stddev = pMUD_indVar->stddev;
          pUnits = pMUD_indVar->units;
        }
        else
        {
          /* result not logged yet; get it from Camp */
          //if(debug) printf("Temp variable %s not logged; get it from Camp\n", p_odb_data->temperature_variable);
          status = campSrv_varGet( p_odb_data->temperature_variable, CAMP_XDR_NO_CHILD );
          if( status != CAMP_SUCCESS )
          {
            cm_msg(MERROR,"DARC_get_dynamic_headers","Camp variable %s does not exist", 
                   p_odb_data->temperature_variable );
          }
          else
          {
            pVar = camp_varGetp( p_odb_data->temperature_variable );
            if( (pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK) != CAMP_VAR_TYPE_NUMERIC )
            {
              cm_msg(MERROR,"DARC_get_dynamic_headers","Camp variable %s is not numeric", 
                     p_odb_data->temperature_variable );
            }
            else
            {
              pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;
              pUnits = pNum->units;
              if( pNum->num < 2 )
              { /* not enough readings for an error bar */
                val = pNum->val;
                stddev = 0.0;
              }
              else
              {
                camp_varNumCalcStats( pVar->core.path, &val, &stddev, &skewness );
              }
            }
          }
        }
        if(debug) printf("Temperature value %f (%f)\n", val, stddev);
        /* Got temperature value.  Now do units conversion.
         * (see also title_validate in td_extra.tcl)
         */
        if( stddev > -1.0 )
        {
          strcpy( units, "K");
          if( pUnits )
          {
            strncpy( units, pUnits, 4 );
            units[0] = toupper( units[0] );
            units[1] = toupper( units[1] );
            units[3] = '\0';
            if( units[0] == 'C' )
            {
              val = val+273.15 ;
              strcpy( units, "K");
            }
            else if( units[0] == 'M' && units[1] == 'K' )
            {
              val = val/1000.0 ;
              stddev = stddev/1000.0 ;
              strcpy( units, "K");
            }
            else if( isalpha(*pUnits) )
            {
              strncpy( units, pUnits, 15 );
              units[15] = '\0';
            }
            else
            {
              strcpy( units, "K" );
            }
          }

          eflag = fmdarc.camp.record_temperature_error;
          n = sizeof(p_odb_data->temperature) - 1;

          DARC_head_num( str, n, val, stddev, units, eflag ) ;

          strncpy( p_odb_data->temperature, str, n );
          p_odb_data->temperature[n] = '\0';
        }
      }  /* finished temperature variable */

    }
  }
  return( SUCCESS );
}




/*---------------- DARC_head_num --------------------------------------------------*/
void
DARC_head_num( char* str, int len, double val, double err, char* units, BOOL eflag )
/*------------------------------------------------------------------------------------*/
{
/*
 *  Produce appropriately rounded character representation of val, including error err
 *  if requested by eflag.
 */

  double av, ae;
  int n;

  //printf("DARC_head_num( %d, %f, %f, %s, %d)\n", len, val, err, units, eflag ); 
  av = fabs(val);
  ae = fabs(err);

  if( eflag )
    {
      if( av >= 1000000.0 || ae >= 100000.0)
        n = sprintf( str, "%.5g(%.5g)", val, ae);
      else if( av >= 100000.0 || ae >= 5. )
        n = sprintf( str, "%.0f(%.0f)", val, ae);
      else if( av >= 10000.0 || ae >= 0.5 )
        n = sprintf( str, "%.1f(%.1f)", val, ae);
      else if( av >= 1000.0 || ae >= 0.05 )
        n = sprintf( str, "%.2f(%.2f)", val, ae);
      else 
        n = sprintf( str, "%.3f(%.3f)", val, ae);
    }
  else
    {
      if( ae == 0.0 ) 
        ae = 0.1;

      if( av >= 1000000.0 )
        n = sprintf( str, "%.5g", val);
      else if( av >= 10000.0 || ae >= 5. )
        n = sprintf( str, "%.0f", val);
      else if( av >= 1000.0 || ae >= 0.5 )
        n = sprintf( str, "%.1f", val);
      else if( av >= 100.0 || ae >= 0.05 )
        n = sprintf( str, "%.2f", val);
      else 
        n = sprintf( str, "%.3f", val);
    }

  strncat( str, units, len-n-1 ); 
  str[len-1] = '\0';
  //printf("DARC_head_num -> %s\n", str);
  return;

}

/* 
 * DARC_find_mud_indvar: 
 * Locate the independent variable identified by the campPath string.  
 * Return a pointer to the MUD section, or NULL if not found.
 */
MUD_SEC_GEN_IND_VAR* 
DARC_find_mud_indvar( char* campPath, MUD_SEC_GRP* pIndVarGrp )
{
  MUD_SEC_GEN_IND_VAR* pMember;


  if( !pIndVarGrp )   return( (MUD_SEC_GEN_IND_VAR*)NULL );

  for( pMember = (MUD_SEC_GEN_IND_VAR*)pIndVarGrp->pMem;
       pMember != NULL;
       pMember = (MUD_SEC_GEN_IND_VAR*)pMember->core.pNext )
    {
      if( pMember->core.secID == MUD_SEC_GEN_IND_VAR_ID )
        {
          if( strcmp(campPath, pMember->name) == 0 )
            {
              return( pMember );
            }
        }
    }
  return( (MUD_SEC_GEN_IND_VAR*)NULL );
}

/*
  
============= Integral MUSR routines  (not TD MUSR or BNMR/BNQR)  =======================

*/

#ifdef MUSR
/*----------------- DARC_hist_I ------------------------------------------------------*/
int
DARC_hist_I( int ind, D_ODB *p_odb_data, caddr_t pHistData, 
	     MUD_SEC_GEN_HIST_HDR* pMUD_histHdr, 
	     MUD_SEC_GEN_HIST_DAT* pMUD_histDat )
/*-----------------------------------------------------------------------------------*/
{
  char tempString[256];
  int nt0, nt00, nt01, nmid, ncl;
  double n, rfsPerBin;
  int* h;
  int i;
    
  if(debug || debug_mud )  
    printf("DARC_hist_I starting with pHistData = %p \n",pHistData);
  /*
    if(debug_dump)  /* if needed add debug_dump to if ( debug ... )  above   
    {
    for (i = db; i < db + dl ; i++)
                     printf("Array element  (int *) pHistData[%d] = 0x%x or %d (dec) \n",
                     i,((int *)pHistData)[i], ((int *)pHistData)[i]);
                     }
  */               


  strncpy( tempString, p_odb_data->IMUSR_his_titles[ind], HIS_SIZE-1 );
  tempString[HIS_SIZE-1] = '\0';
  trimBlanks( tempString, tempString );
  pMUD_histHdr->title = strdup( tempString );    
  if(debug_mud)
  {
    printf(
      "DARC_hist_I: Working on histogram  %s \n",pMUD_histHdr->title);      
    printf("DARC_hist_I: index = %d, histogram title = %s \n",
           ind, pMUD_histHdr->title);
  }
  pMUD_histHdr->histType = MUD_SEC_TRI_TI_HIST_ID;

  /*
   *  TW/SD  TEMP  21-Dec-2000  Set the bytesPerBin to 4 to turn off packing
   *                            By default, the MUD hist header is created with
   *                            bytesPerBin = 0, which is a flag to the packing
   *                            routine to do packing.
   *                            For now, leave packing turned ON.
   *                            Note that the malloc of the pMUD_histDat->pData
   *                            above may cause memory allocation problems because
   *                            of allocating (nbins * bytes+3) bytes 
   */
  /*  pMUD_histHdr->bytesPerBin = 4; /* turns off packing */


  pMUD_histHdr->nEvents = (UINT32) p_odb_data->IMUSR_his_total[ind];
  if(debug_mud)
    printf("(UINT32)pMUD_histHdr->nEvents = %u\n",pMUD_histHdr->nEvents);
  pMUD_histHdr->nBins = p_odb_data->his_nbin;

  pMUD_histHdr->fsPerBin =  0 ;

     
  pMUD_histHdr->t0_bin = 0;
  pMUD_histHdr->goodBin1 = 0;
  pMUD_histHdr->goodBin2 = 0;
  pMUD_histHdr->t0_ps = 0;
  pMUD_histHdr->bkgd1 = 0;
  pMUD_histHdr->bkgd2 = 0;
  

  pMUD_histHdr->nBytes = MUD_SEC_GEN_HIST_pack( pMUD_histHdr->nBins,
                                                4, pHistData,
                                                pMUD_histHdr->bytesPerBin, pMUD_histDat->pData );
  pMUD_histDat->nBytes = pMUD_histHdr->nBytes;
  
  return( SUCCESS );
}


/*----------------------- DARC_runDesc_I -------------------------------*/
int
DARC_runDesc_I( D_ODB* p_odb_data, MUD_SEC_TRI_TI_RUN_DESC* pMUD_desc )
/*------------------------------------------------------------------------*/
{
  int status;
  int i;
  int tempTime[6];
  char tempString[256];
  int timadr[2];
  short timbuf[7];
  char buffer[128];
  char* pos;

  if(debug_mud)printf ("DARC_runDesc_I: starting for run %d  \n",p_odb_data->run_number);

  pMUD_desc->runNumber = p_odb_data->run_number;
  pMUD_desc->exptNumber = p_odb_data->experiment_number;
  
  /* for linux use binary run start time directly */
  pMUD_desc->timeBegin = p_odb_data->start_time_binary ;    
  if( p_odb_data->run_state == STATE_RUNNING)
  {
    time( &pMUD_desc->timeEnd ); /* linux for now assume run ends now */        
    if(debug_mud) printf ("DARC_runDesc_I: Run in progress - using current time for timeEnd \n");
  }
  else
  {
    pMUD_desc->timeEnd = p_odb_data->stop_time_binary ; /* run ended */
    if(debug_mud) printf ("DARC_runDesc_I: Run NOT in progress - using saved time for timeEnd \n");
  }
  
  pMUD_desc->elapsedSec = pMUD_desc->timeEnd - pMUD_desc->timeBegin;
  
  pMUD_desc->method = strdup( "TI-�SR" ); /* IMUSR */
  pMUD_desc->lab = strdup( "TRIUMF" );
  pMUD_desc->das = strdup( "MIDAS" ); /* different DAQ !! */

  pMUD_desc->area = strdup( beamline );

  _dup_desc_str( pMUD_desc->title, p_odb_data->run_title );
  _dup_desc_str( pMUD_desc->sample, p_odb_data->sample );
  _dup_desc_str( pMUD_desc->subtitle, p_odb_data->subtitle );
  _dup_desc_str( pMUD_desc->comment1, p_odb_data->comment1 );
  _dup_desc_str( pMUD_desc->comment2, p_odb_data->comment2 );
  _dup_desc_str( pMUD_desc->comment3, p_odb_data->comment3 );
  _dup_desc_str( pMUD_desc->orient, p_odb_data->orientation );
  _dup_desc_str( pMUD_desc->apparatus, p_odb_data->rig );
  _dup_desc_str( pMUD_desc->insert, p_odb_data->mode );
  _dup_desc_str( pMUD_desc->experimenter, p_odb_data->experimenter );

  if(debug_mud)
  {
    printf("Run descriptor:\n");
    printf("pMUD_desc->title = %s\n",pMUD_desc->title );
    printf("pMUD_desc->sample = %s\n",pMUD_desc->sample );
    printf("pMUD_desc->subtitle = %s\n",pMUD_desc->subtitle );
    printf("pMUD_desc->comment1 = %s\n",pMUD_desc->comment1 );
    printf("pMUD_desc->comment2 = %s\n",pMUD_desc->comment2 );
    printf("pMUD_desc->comment3 = %s\n",pMUD_desc->comment3 );
    printf("pMUD_desc->orient = %s\n",pMUD_desc->orient );
    printf("pMUD_desc->apparatus = %s\n",pMUD_desc->apparatus );
    printf("pMUD_desc->insert = %s\n",pMUD_desc->insert );

  }
  return( SUCCESS );
}



/* --------------------- imdarc_camp ---------------------*/
int
imdarc_camp( MUD_SEC_GRP* pMUD_indVarGrpI)
/*--------------------------------------------------------*/
{
  /*  Integral MUSR only - not TD-MUSR or BNMR

  Assemble IMUSR CAMP section 

 */

  int h, b, d, p, i, j;
  
  MUD_SEC_GEN_IND_VAR* pMUD_indVarHdr;
  MUD_SEC_GEN_ARRAY* pMUD_indVarDat;
  int indVarOffset;
  int npoints;
  double val, sum, sum2, sum3, mean, stddev;
  int num;
  unsigned int uitemp;
  float f;
  char * testc;

  if(debug_mud)
    printf("imdarc_camp: starting with numCamp = %d and campVars=%p,  pCampData=%p\n",numCamp,campVars,pCampData);
  if (pCampData == NULL)
  {
      cm_msg(MERROR,"imdarc_camp", "pCampData is NULL, no CAMP data available");
      return(-1);
  }

  if (!bGotCamp)
  {
    printf("imdarc_camp: no CAMP data available at this time (bGotCamp is false)\n");
    return (-1);
  }

  npoints = gbl_camp_data_point+1; //gbl_camp_data_point starts at 0
  if (npoints <1 || npoints > imusr.imdarc.maximum_datapoints)
  {
    cm_msg(MERROR,"imdarc_camp","illegal number of camp data points (%d)\n",npoints);
    return(-1);
  }
  if(debug_mud)printf("npoints received=%d, nHistograms=%d\n",npoints,nH);
  /* sometimes these are one different if another IDAT event arrived while we are still processing this CVAR event */
  if(abs(npoints- nHistBins)>1)
    printf("imdarc_camp: npoints (%d) and nHistBins (%d) do not agree\n",npoints,nHistBins);
  // using npoints rather than nHistBins to save the data as npoints is correct for CAMP banks. 
  
  for( i = 0; i < numCamp; i++ )
  {
    h = i *  imusr.imdarc.maximum_datapoints; // offset to start of data

    pMUD_indVarHdr = (MUD_SEC_GEN_IND_VAR*)MUD_new( MUD_SEC_GEN_IND_VAR_ID, i+1 );
    pMUD_indVarDat = (MUD_SEC_GEN_ARRAY*)MUD_new( MUD_SEC_GEN_ARRAY_ID, i+1 );
    
    /*
     *  Allocate space for the array (4 bytes/bin, no packing)
     */
    pMUD_indVarDat->pData = (caddr_t)zalloc( 4*( npoints ) );

    if(debug_mud)printf("offset h to start of data for Camp variable %d  = %d\n",i,h);
    
    for( d = 0; d < npoints; d++ )
    {
      /*
       *  No need to decode/encode because MUD_writeFile encodes everything.
       *  In fact, if we encoded here, we would get garbage due to double-encoding.
       */

      f= pCampData[d+h];
      uitemp =   *((int*)&f); /* float not converted */
      if(debug_mud)printf("imdarc_camp:copying uitemp = %d to mud at index d=%d\n",uitemp,d);
      bcopy( &uitemp, &((UINT32*)pMUD_indVarDat->pData)[d], 4 );
    }
    
    
    pMUD_indVarDat->num = npoints;
    pMUD_indVarDat->elemSize = 4;   /* 4 bytes/bin */
    pMUD_indVarDat->type = 2;       /* 2=real */
    pMUD_indVarDat->hasTime = 0;    /* no time data */
    pMUD_indVarDat->pTime = NULL;
    pMUD_indVarDat->nBytes = pMUD_indVarDat->num*pMUD_indVarDat->elemSize;

    if(debug_mud)
    {
      printf("imdarc_camp:  pMUD_indVarDat->num =%d\n",  pMUD_indVarDat->num );
      printf("    pMUD_indVarDat->elemSize = %d\n", pMUD_indVarDat->elemSize);
    }
    if(campVars != NULL)
    {
      if(debug_mud)printf("path from campVars= %s\n", campVars[i].path);
      pMUD_indVarHdr->name = strndup( campVars[i].path, campVars[i].len_path );
      pMUD_indVarHdr->description = strndup( campVars[i].title, campVars[i].len_title );
      pMUD_indVarHdr->units = strndup( campVars[i].units, campVars[i].len_units );
    }
    else
      sprintf( pMUD_indVarHdr->name,"no info");


    /*
     *  Calculate statistics
     */
    num = pMUD_indVarDat->num;
    if( num > 0 )
    {
      sum = sum2 = sum3 = 0.0;
      pMUD_indVarHdr->low = pMUD_indVarHdr->high = 
	((REAL32*)pMUD_indVarDat->pData)[0];
      for( j = 0; j < num; j++ )
      {
	val = (double)((REAL32*)pMUD_indVarDat->pData)[j];
	pMUD_indVarHdr->low = _min( pMUD_indVarHdr->low, val );
	pMUD_indVarHdr->high = _max( pMUD_indVarHdr->high, val );
	sum += val;
	sum2 += val*val;
	sum3 += val*val*val;
      }
      mean = pMUD_indVarHdr->mean = sum/num;
      stddev = pMUD_indVarHdr->stddev = ( num == 1 ) ? 0.0 :
	sqrt( fabs( ( sum2 - sum*sum/num )/( num - 1 ) ) );
      /* avoid overflow in skewness when stddev near zero ...*/
      pMUD_indVarHdr->skewness = ( stddev < 1.0e-10 ) ? 0.0 :
	( sum3 - 3.0*mean*sum2 + 
	  2.0*( sum*sum*sum )/( ((double)num)*((double)num) ) ) /
	( ((double)num)*(stddev*stddev*stddev) );
      
    }
    
    MUD_addToGroup( pMUD_indVarGrpI, pMUD_indVarHdr );
    MUD_addToGroup( pMUD_indVarGrpI, pMUD_indVarDat );
    
  }

  return( 1 );
  
}


/* ------------------------ DARC_write_I ---------------------------------*/
int
DARC_write_I( D_ODB* p_odb_data, caddr_t pHistData)
/* -----------------------------------------------------------------------*/ 
{
  /* Integral MUSR only  - not TD-MUSR or BNMR */
  
  int status;
  MUD_SEC_GRP* pMUD_fileGrp;
  MUD_SEC_TRI_TI_RUN_DESC* pMUD_descI;
  MUD_SEC_GRP* pMUD_indVarGrpI;  
  MUD_SEC_GRP* pMUD_scalGrp;
  MUD_SEC_GRP* pMUD_histGrp;
  MUD_SEC_GRP* pMUD_cmtGrp;
  int i, j, k;
  int offset;

  /*
   *  Compute sums of bin quantities
   */
  
  p_odb_data->total_save = 0;
  
  
  offset = 0 ; /* initial offset into data array */
  
  
  if (debug || debug_mud) 
    printf("DARC_write_I: Starting with pHistData = %p and his_n=%d\n"
				 ,pHistData,p_odb_data->his_n);  
  for( i = 0; i <  p_odb_data->his_n; i++ )
    {
      if (debug_mud)printf("DARC_write_I: His %d  pHistData = %p\n",i,pHistData);
      p_odb_data->IMUSR_his_total[i] = 0.0;
      if(debug_mud)
	printf("DARC_write_I: his %d  offset= %d\n",i,offset);
      if (i > 0)
	{
	  for( j = 0; j < p_odb_data->his_nbin; j++ )
	    {
	      if(debug_dump)
		{          
		  if( j >= db && j < (db+dl) )
		    {
		      printf("Adding on data index %d = 0x%x (%u) at offset(%d) to IMUSR_his_total[%d](%f)\n",
			     j, ((unsigned int*)pHistData)[offset+j],  ((unsigned int*)pHistData)[offset+j],
			     (offset+j), i, p_odb_data->IMUSR_his_total[i]);
		    }
		}
	      
	      p_odb_data->IMUSR_his_total[i] += (float)  ((unsigned int*)pHistData)[offset+j] ;
	      //	if(debug_dump)
	      //	  printf("IMUSR_his_total[%d] is now %f\n",i, p_odb_data->IMUSR_his_total[i]);
	    }
	  
	}
      else
	{
	  j= p_odb_data->his_nbin-1;
	  if(debug_dump) 
	  printf("Skipping sums for i = %d, offset=%d (sweep value)\n",i,offset); 
	  printf("DARC_write_I: writing data for data point %d at sweep value %u  \n", j,
				((unsigned int*)pHistData)[offset+j] );
	}
      
      p_odb_data->total_save += p_odb_data->IMUSR_his_total[i];
      if(debug_dump || debug_mud)
	printf("DARC_write_I: his %d IMUSR_his_total[%d]= %f, total_save=%f\n",
	       i,i,p_odb_data->IMUSR_his_total[i], p_odb_data->total_save ) ;
      offset = offset +  imusr.imdarc.maximum_datapoints; /* point to next  histogram */
      if(debug_dump) printf("offset  + max_datapoints (%d)  ->%d\n",
			    imusr.imdarc.maximum_datapoints, offset);
    }
  /* end of loop on all histograms, zeroing totals for unused histograms */
  for( i = p_odb_data->his_n; i< MAX_IHIS; i++ )
    p_odb_data->IMUSR_his_total[i] = 0.0;
  
  pMUD_fileGrp = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_FMT_TRI_TI_ID );
  if (debug_mud) printf(" DARC_write_I: After MUD_new, pMUD_fileGrp = %p\n",pMUD_fileGrp ); 
  
  /*
   *  Convert the run description
   */
  pMUD_descI =  (MUD_SEC_TRI_TI_RUN_DESC*) MUD_new( MUD_SEC_TRI_TI_RUN_DESC_ID, 1 );
  DARC_runDesc_I( p_odb_data, pMUD_descI );
  
  /*
   *  Convert the histograms
   */
  pMUD_histGrp = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_GRP_TRI_TI_HIST_ID );
  
  if( DARC_hists( p_odb_data, pMUD_histGrp, pHistData) == FAILURE )
  {
    cm_msg(MERROR,"DARC_write_I","DARC_hists: failed to create histograms\n" );
    return(FAILURE);
  }
  
  /*
   *  Collect comments if they exist
   */
  pMUD_cmtGrp = DARC_comments( p_odb_data );

  /*
   *  Assemble the first level sections
   */  

/* keep this code in case we need to time something */  
/*  if (timer)  
  {   
    time(&timbuf);
    strcpy(timbuf_ascii,ctime(&timbuf));         
    printf (
      "DARC_write_I (TIMER) Assembling 1st level sections  %s\n",timbuf_ascii);
  }
*/  
  MUD_addToGroup( pMUD_fileGrp, pMUD_descI );
  MUD_addToGroup( pMUD_fileGrp, pMUD_histGrp );
  
  if( pMUD_cmtGrp != NULL ) 
    MUD_addToGroup( pMUD_fileGrp, pMUD_cmtGrp );

  /* 
   * IMUSR adds its own CAMP section, with readback values at every sweep point.
   */ 

  if( numCamp > 0 )
  {
    pMUD_indVarGrpI = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_GRP_GEN_IND_VAR_ARR_ID );
    
    status = imdarc_camp(pMUD_indVarGrpI);
    MUD_addToGroup( pMUD_fileGrp, pMUD_indVarGrpI );
  }
  else
  {
    pMUD_indVarGrpI = (MUD_SEC_GRP*)NULL;
  }

  if(debug) printf("DARC_write_I: calling DARC_write to write the file\n");
  status = DARC_write(p_odb_data, pMUD_fileGrp); // write the file
  return (status);
}

 // end of MUSR only (actually IMUSR)


#else
/* ====================== BNMR/BNQR Type 2 only ==============================
 */

/* DARC_log_ppg_par:
 *
 * Log the PPG parameterd -- the ODB parameters listed (as links) in /PPG/PPGxx
 * for experiment type "xx".  Save them as independent variables.  For numeric
 * parameters, save the number as the "mean".  For string parameters, save the
 * value as the "units".  For Booleans, save the number (0 or 1) as the mean,
 * and the words "no" or "yes" as the units.  Record the PPG path as the "name"
 * (which usually contains the Camp path) and the linked-to variable name as the
 * "description".
 */
/*-------------------------------------------------------------------------*/
static void
DARC_log_ppg_par( )
/*-------------------------------------------------------------------------*/
{
  /*
   * pMUD_indVarGrp_gbl and numIndVar are global.
   */
    HNDLE hKeyPPG, hKeyPar;
    KEY key;
    char exp_type[32];
    char ppgpath[48];

    int status, size, index;

    INT ivalue;
    WORD wvalue;
    DWORD uvalue;
    BOOL bvalue;
    float fvalue;
    double val;
    BOOL got;
    char svalue[255], ppgparpath[128];

    MUD_SEC_GEN_IND_VAR* pMUD_indVar;

    if( pMUD_indVarGrp_gbl == NULL )
    {
	pMUD_indVarGrp_gbl = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_GRP_GEN_IND_VAR_ID );
	if( debug_mud || debug_ppg )
            printf("DARC_log_ppg_par: logging PPG and Camp sections\n" );
    }

    size = sizeof(exp_type);
    status = db_get_value(hDB, 0, "Equipment/FIFO_acq/frontend/Input/Experiment name",
                          exp_type, &size, TID_STRING, FALSE);

    sprintf(ppgpath, "PPG/PPG%s", exp_type);
    /* printf("Look for parameters at PPG/PPG%s\n", exp_type);*/

    /* printf("Var types are KEY %d, LINK %d, STRING %d, INT %d\n",
           TID_KEY,TID_LINK,TID_STRING,TID_INT); */

    status = db_find_key(hDB, 0, ppgpath, &hKeyPPG);

    if( status != DB_SUCCESS )
      {
        printf( "ERROR: Could not read PPG parameters %s\n",ppgpath) ;
        cm_msg(MERROR,"DARC_log_ppg_par","Could not read PPG parameters %s\n",ppgpath) ;
        return;
      }


    for (index=0 ; ; index++)
      {
        status = db_enum_link(hDB, hKeyPPG, index, &hKeyPar);
        if (status == DB_NO_MORE_SUBKEYS) break;
        if (status != DB_SUCCESS ) continue;

        status = db_get_key(hDB, hKeyPar, &key);
        if (status != DB_SUCCESS ) continue;
        sprintf(ppgparpath, "%s/%s", ppgpath, key.name );
        /* printf("\n%d Got key \"%s\" type %d\n", index, key.name, key.type);*/

        status = db_enum_key(hDB, hKeyPPG, index, &hKeyPar);
        if (status == DB_NO_MORE_SUBKEYS) break;
        if (status != DB_SUCCESS ) continue;

        status = db_get_key(hDB, hKeyPar, &key);
        if (status != DB_SUCCESS ) continue;
        /* printf("%d Got key \"%s\" type %d, %d values\n",
           index, key.name, key.type, key.num_values);*/

        if ( key.num_values == 1 ) /* ignore any arrays */
          {
            svalue[0] = '\0';
            val = 0.0;
            got = TRUE;

            switch ( key.type )
              {
              case TID_INT:
		size=sizeof(ivalue);
                status = db_get_data( hDB, hKeyPar, &ivalue, &size, key.type );
                /* printf("Integer value %d\n", ivalue); */
                val = (double) ivalue;
                break;
              case TID_BOOL:
		size=sizeof(bvalue);
                status = db_get_data( hDB, hKeyPar, &bvalue, &size, key.type );
                /* printf("Boolean value %d\n", bvalue); */
                val = (double) bvalue;
                strcpy( svalue, (bvalue? "yes" : "no") );
                break;
              case TID_WORD:
		size=sizeof(wvalue);
                status = db_get_data( hDB, hKeyPar, &wvalue, &size, key.type );
                /* printf("Word value %d\n", wvalue); */
                val = (double) wvalue;
                break;
              case TID_DWORD:
		size=sizeof(uvalue);
                status = db_get_data( hDB, hKeyPar, &uvalue, &size, key.type );
                /* printf("Dword value %d\n", uvalue); */
                val = (double) uvalue;
                break;
              case TID_FLOAT:
		size=sizeof(fvalue);
                status = db_get_data( hDB, hKeyPar, &fvalue, &size, key.type );
                /* printf("Float value %f\n", fvalue); */
                val = (double) fvalue;
                break;
              case TID_DOUBLE:
		size=sizeof(val);
                status = db_get_data( hDB, hKeyPar, &val, &size, key.type );
                /* printf("Double value %lf\n", val); */
                break;
              case TID_STRING:
		size=sizeof(svalue);
                status = db_get_data( hDB, hKeyPar, &svalue, &size, key.type );
		/*printf("String value \"%s\", size=%d\n", svalue,size); */
                break;
              default:
                got = FALSE;
                break;
              }
	    if(status != DB_SUCCESS)
	      printf("DARC_log_ppg_par: error after db_get_data for key type=%d (%d)\n",key.type,status);

            if( got )
              {
                numIndVar++;

                pMUD_indVar = (MUD_SEC_GEN_IND_VAR*) MUD_new( MUD_SEC_GEN_IND_VAR_ID, numIndVar );

                pMUD_indVar->mean = val;
                pMUD_indVar->name = strdup( ppgparpath );
                pMUD_indVar->description = strdup( key.name );
                pMUD_indVar->units = strdup( svalue );
                pMUD_indVar->low = 0.0;
                pMUD_indVar->high = 0.0;
                pMUD_indVar->stddev = 0.0;
                pMUD_indVar->skewness = 0.0;

                MUD_addToGroup( pMUD_indVarGrp_gbl, pMUD_indVar );
              }
          }
      }
    return;
}
#endif

#ifdef EPICS_ACCESS

static void
DARC_epics_log_TD( )
/*-------------------------------------------------------------------------*/
{
/* DARC_epics_log:
 *
 * Log the EPICS values
 *  EPICS names are "name"
 *  
 */

  /*
   * pMUD_indVarGrp_gbl and numIndVar are global.
   */
  
    int i,j,nerror;
    int status;

    nerror=0;

    if(debug_epics) printf("DARC_epics_log_TD: starting  n_epics=%d\n",n_epics);

    if(n_epics <= 0)  // no EPICS values to log
      {
	write_client_logging(0, 0); // epics logging is off
	if(debug_epics) printf("DARC_log_epics: no Epics values to log\n");
	return;
      }

    MUD_SEC_GEN_IND_VAR* pMUD_indVar;

    if( pMUD_indVarGrp_gbl == NULL )
      {
	pMUD_indVarGrp_gbl = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_GRP_GEN_IND_VAR_ID );
	if( debug_epics )
	  printf("DARC_log_epics: logging Epics section\n" );
      }

    /* read the EPICS values */

    for (i=0; i<n_epics; i++)
      {
	status = read_epics_value(i,TRUE);  // reads a new value, updates mean and stddev, maximum and minimum
	if(status != SUCCESS)
	  {
	    printf("DARC_log_epics: could not read value from EPICS %s at i=%d\n", epics_log[i].Name,i);
	    nerror++;
	}
	else
	  {
	    j=epics_log[i].index;
	    if(debug_epics)
	      {
		printf("DARC_log_epics: read value from EPICS %s as %f%s (i=%d j=%d)\n", 
		       epics_log[i].Name,  (double)epics_log[i].value,
		       epics_params[j].Units,i,j) ;
		print_epics_log(i);
		printf("Epics name: %s title: %s\n",
		       epics_params[j].Rname,  epics_params[j].Title);
	      }
	    numIndVar++;
	    pMUD_indVar = (MUD_SEC_GEN_IND_VAR*) MUD_new( MUD_SEC_GEN_IND_VAR_ID, numIndVar );
	    pMUD_indVar->mean =  epics_log[i].mean;
	    pMUD_indVar->name = strdup( epics_log[i].Name );
	    pMUD_indVar->description = strdup( epics_params[j].Title );
	    pMUD_indVar->units = strdup( epics_params[j].Units );
	    pMUD_indVar->low = epics_log[i].minimum ;
	    pMUD_indVar->high = epics_log[i].maximum ;
	    pMUD_indVar->stddev = epics_log[i].stddev ;
	    pMUD_indVar->skewness =  epics_log[i].skew;

	    MUD_addToGroup( pMUD_indVarGrp_gbl, pMUD_indVar );
 
	  }
      }
    write_client_logging(0, 1); // epics logging is on
    return;
}
#endif // EPICS



#ifndef MUSR
INT write_client_logging(BOOL camp_flag, BOOL setval)
{
  /* write y or n to camp_logging flag or epics_logging flag 
     these flags are used by the custom web page
*/

  INT status=0;
  char string[80];
  
  if(camp_flag)
    {
      if(setval)
	printf("write_client_logging: camp is being logged \n");
      else
	printf("write_client_logging: camp is NOT being logged \n");
    }
  else
    {
      if(setval)
	printf("write_client_logging: epics is being logged \n");
      else
	printf("write_client_logging: epics is NOT being logged \n");
    }


  if(camp_flag)
    sprintf(string,"/Equipment/%s/client flags/logging camp",eqp_name);
  else
    sprintf(string,"/Equipment/%s/client flags/logging epics",eqp_name);

  status = db_set_value(hDB,0,string,&setval,sizeof(setval),1,TID_BOOL);
  if(status != DB_SUCCESS)
    cm_msg(MERROR,"write_client_logging","Error writing %d to \"%s\" (%d)",
	   setval,string,status);
  return;
  
}
#endif
