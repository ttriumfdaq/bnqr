/*   get_musr_type.c

 Determine whether Integral or TD MUSR is running
 This file included by mdarcMusr.c and mheader.c 

 $Log: get_musr_type.c,v $
 Revision 1.1  2013/01/21 21:42:38  suz
 initial VMIC version to cvs

 Revision 1.1  2004/02/11 17:44:55  suz
 original


*/
 /*--  find whether running I or TD MUSR ----------------------------------------*/
INT get_musr_type(BOOL *fe_flag)
{
  char musr_type[10];
  char str[128];
  int j,len;
  INT size,status;
 
  I_MUSR=TD_MUSR=FALSE; /* global flags */
  *fe_flag = FALSE;

  if(debug)printf("get_musr_type starting\n");
  
  sprintf(str,"/experiment/edit on start/musr type");
  size = sizeof(musr_type);
  status = db_get_value(hDB, 0, str,
                        &musr_type , &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"get_musr_type","cannot access key %s (%d)",
           str,status);
    write_message1(status,"running_IMUSR");
    return status;
  }

  len=strlen(musr_type);
  for(j=0; j<len; j++)
    musr_type[j] = toupper (musr_type[j]); /* convert to upper case */

  if(debug)printf("len:%d, string %s\n",len,musr_type);

  if (strchr(musr_type,'I'))
    I_MUSR=TRUE;
  if (strchr(musr_type,'T') )
    TD_MUSR=TRUE;
  
  if ( (I_MUSR == FALSE && TD_MUSR == FALSE) ||
       (I_MUSR == TRUE  && TD_MUSR == TRUE) )
  {
    cm_msg(MERROR,"get_musr_type","Bad value of key %s (%s). Must contain \"I\" or \"TD\" and not both ",
           str,musr_type);

    return(DB_INVALID_PARAM);
  }
  // fill expected frontend client name (feclient is in mdarc.h)
  if(TD_MUSR)sprintf(feclient,"fev680");
  if(I_MUSR)sprintf(feclient,"femusr");

      //      sprintf(ppg_mode,"20"); // dummy for perlscripts
      //      ppg_type =2;

  /* checks the correct frontend is running */ 
  status = get_FE_client_name(FE_ClientName);  // get the frontend client name
  if(status != CM_SUCCESS)
    {
      *fe_flag=FALSE;  /* wrong frontend booted */
      printf(" ******************************************************** \n");
      printf(" * WARNING : ppc is booted with the wrong frontend code *\n");
      printf(" ******************************************************** \n");
   }
  else
    *fe_flag=TRUE;  /* correct frontend or no frontend booted */
  printf("tr_start: frontend client name is \"%s\"\n",FE_ClientName);



  return DB_SUCCESS;
}




/*-----------------------------------------------------------------------------------------------*/
INT get_FE_client_name(char *pname)
/*-----------------------------------------------------------------------------------------------*/
{
  char name[256],str[256];
  INT i,len;
  HNDLE  hKeyTmp,hSubkey;
  INT size;
  
  /* get the ACTUAL client name for the frontend */
  printf("get_FE_client_name: starting with expected feclient=%s\n",feclient);
  sprintf(name,"none");
  len=strlen(feclient);
  status = cm_exist(feclient, FALSE);
  if (status == CM_SUCCESS)
    {
      if(debug)printf("get_FE_client_name: Client %s is running\n",feclient);
      status = db_find_key(hDB, 0, "System/Clients", &hKeyTmp);
      if (status != DB_SUCCESS)
	cm_msg(MERROR, "get_FE_client_name", "cannot find System/Clients entry in database");
      else
	{
	  /* search database for clients with transition mask set */
	  for (i=0,status=0 ; ; i++)
	    {
	      status = db_enum_key(hDB, hKeyTmp, i, &hSubkey);
	      if (status == DB_NO_MORE_SUBKEYS)
		break;
	      
	      if (status == DB_SUCCESS)
		{
		  size = sizeof(name);
		  db_get_value(hDB, hSubkey, "Name", name, &size, TID_STRING, TRUE);
		  if(strncmp(feclient,name,len)==0)
		    {
		      if(debug)printf("get_FE_client_name: found client name %s\n",name);
		      break;
		    }        
		}
	    }
	}
      strcpy(pname,name);
      printf("get_FE_client_name: returning client name as %s\n",pname);
      return DB_SUCCESS;
    }
  else
    {
      cm_msg(MERROR,"get_FE_client_name","Frontend \"%s\" is not running! (%d)", feclient,status);
      strcpy(pname,"none");
      /* check if the wrong type has been booted */
      if (I_MUSR)
	{
	  status = cm_exist(TD_ClientName, FALSE);
	  if (status == CM_SUCCESS)
	    {
	      printf("get_FE_client_name: TD frontend client %s is running; expected %s \n",TD_ClientName,I_ClientName);
	      cm_msg(MERROR,"get_FE_client_name","frontend is booted to run the wrong experiment (TD/Integral)");
	      return CM_NO_CLIENT;
	    }
	}
      else
	{
	  status = cm_exist(I_ClientName, FALSE);
	  if (status == CM_SUCCESS)
	    {
	      printf("get_FE_client_name: Integral frontend client %s is running; expected %s \n",I_ClientName,TD_ClientName);
	      cm_msg(MERROR,"get_FE_client_name","frontend is booted to run the wrong experiment (TD/Integral)");
	      return CM_NO_CLIENT;
	    }
	}
    }
  return CM_SUCCESS; /* frontend is not running */
}
