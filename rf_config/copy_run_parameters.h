// prototypes

// Maximum possible number of copy functions is given by 
// MAX_BNMR_EXPT in rf_config.h
INT copy_10(char *ppg_mode);
INT copy_1a(char *ppg_mode);
INT copy_1b(char *ppg_mode);
INT copy_1c(char *ppg_mode);
INT copy_1f(char *ppg_mode);
INT copy_1g(char *ppg_mode);
INT copy_1j(char *ppg_mode);
INT copy_1n(char *ppg_mode);

INT copy_20(char *ppg_mode);
INT copy_2a(char *ppg_mode);
INT copy_2b(char *ppg_mode);
INT copy_2c(char *ppg_mode);
INT copy_2d(char *ppg_mode);
INT copy_2e(char *ppg_mode);
INT copy_2f(char *ppg_mode);
INT copy_2s(char *ppg_mode);
INT copy_2h(char *ppg_mode);
INT copy_2w(char *ppg_mode);
INT get_param_key(char *ppg_mode);
INT update_hardware_params(INT mode);
