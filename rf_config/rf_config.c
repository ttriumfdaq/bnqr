/* rf_config.c 

Author: Pierre Amaudruz
Modifications by: C. Bommas, R. Poutissou, S. Kreitzman, S. Daviel
  
 This version for VMIC
 1. writes calculated values (nbins,nhis,dwell time) to mdarc area
 2. contains Syd's modifications to e2c_compute.
 3. calls perl compiler comp_int.pl directly with two parameters,
    saves compiler error messages in a file and displays it on
    error 
 4. If HAVE_NEWPPG, converts bytecode.dat into ppgload.dat
*/      
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <sys/time.h>
#include <sys/stat.h> // time
#include <errno.h>  // needed to interpret errors after "system" call

#define new_max(x,y) ((x) >= (y)) ? (x) : (y)
#define new_min(x,y) ((x) <= (y)) ? (x) : (y)

#ifdef CAMP
/* camp includes come before midas.h */
#include "camp_clnt.h"
/* to avoid conflict with midas defines, undefine these
   - they will be redefined by midas the same as camp defined them */
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#ifdef INLINE
#undef INLINE
#endif
#endif  // CAMP
  #define I_Q_DATA_MASK 0x3FF /* 10 bits */
#include "midas.h"  // SUCCESS is 1
#include "msystem.h"
#include "rf_config.h" // function prototypes and structures defined
#include "copy_run_parameters.h" // function prototypes and structures defined
#include "experim.h"

#ifdef CAMP
#include "camp_acq.h" // communicate with camp_acq
#endif

const int f_switch_boundary = 100000;   /* Phase discontinuity when changing the 100 kHz digit*/
const int f_max_stepsize    = 100000;

INT TwosComp_convert(INT value, INT twos_comp);

char eqp_name[32];
HNDLE  hDB, hSeq, hFiS, hIn, hOut, hHis, hMdarc, hTest, hPSM, hH, hClient;
char Expt_name[256]; /* global copy */
#ifdef CAMP
HNDLE hCamp;
FIFO_ACQ_CAMP_SWEEP_DEVICE fcamp;
/* camp_acq.h */
CAMP_PARAMS camp_params;
#endif
char comp_path[32];
FIFO_ACQ_FRONTEND ppg;
FIFO_ACQ_MDARC fmdarc;

char client_str[80];
HNDLE hMP, hParam;

FIFO_ACQ_MODE_PARAMETERS mode_param;

BOOL single_shot;
BOOL debug=1;
BOOL temp_set_quad=0;
BOOL no_ppg_flag=0; // set for sis3820 test mode
BOOL copy_last_params_flag; // will be set if rf_config is successful
INT expt_type; // 1 for Type 1, 2 for Type 2

/* The following modes are DUMMIES that revert to 1f and do not appear in the list:
     Note 1e is taken for FIELD  Epics Field scan
          1d is taken for LASER  Epics Laser scan
         (1h not used;  previously Pol's DAC scan)

    

If adding another expt Nx, also add prototype, to rf_config.h, and add to expt[] list below. 

Note: MAX_BNMR_EXPT in rf_config.h is now the maximum possible compute or copy functions, and
is set to 25.

The actual number of experiments num_expts_defined depends on ifdefs and will be calculated in 
expt_search().
There must be a null experiment at the end of the expt structure.


Create copy_Nx in update_run_parameters.c and add prototype to copy_run_parameters.h. 
Add to  param_update[] list below.  

The actual number of copy functions num_copy_defined depends on ifdefs and will be calculated in 
param_search().
There must be a null experiment at the end of the param_update structure. 
*/

BNMR_EXPT expt[] = {
  {"1a", e1a_compute},
  {"1b", e1b_compute},
#ifdef CAMP
  {"1c", e1c_compute},
  {"1j", e1j_compute},
#endif
  {"1n", e1n_compute},
  {"1g", e1g_compute},
  {"1f", e1f_compute},
  {"00", e00_compute},
  {"2a", e2a_compute},
  {"2b", e2b_compute},
  {"2c", e2c_compute},
  {"2d", e2d_compute},
  {"2e", e2e_compute},
  {"2f", e2f_compute},
#ifdef BNQR
#ifdef HAVE_SIS3820
  {"2h", e00_compute},
#endif
#endif // BNQR
#ifdef BNMR
#ifdef PSMIII
  {"2s", e2s_compute},
  {"2w", e2w_compute},
#endif
#endif // BNMR
  {""  , NULL}
};  // last value must be NULL

INT num_expts_defined=-1; // number of experiments actually defined in above structure 
                          // for this beamline. Set to -1. To be filled by function expt_search.  

BNMR_EXPT param_update[] = {
  {"10", copy_10},
  {"1a", copy_1a},
  {"1b", copy_1b},
#ifdef CAMP
  {"1c", copy_1c},
  {"1j", copy_1j},
#endif
  {"1e", copy_1n},
  {"1n", copy_1n},
  {"1g", copy_1g},
  {"1f", copy_1f},
  {"20", copy_20}, 
  {"2a", copy_2a},
  {"2b", copy_2b},
  {"2c", copy_2c},
  {"2d", copy_2d},
  {"2e", copy_2e},
  {"2f", copy_2f},
#ifdef BNQR
#ifdef HAVE_SIS3820
  {"2h", copy_2h},
#endif
#endif // BNQR
#ifdef BNMR
#ifdef PSMIII
  {"2s", copy_2s},
  {"2w", copy_2w},
#endif
#endif // BNMR
  {""  , NULL} // last value must be NULL
};
INT num_copy_defined=-1; // number of items actually defined in above structure 
                          // for this beamline. Set to -1 for now.

#ifdef CAMP
/* CAMP support */
//CAMP_PARAMS camp_params; // structure defined in camp_acq.h

/* needed for check_camp */
int campInit  = FALSE;
int camp_available;
char serverName[LEN_NODENAME+1];

#endif // CAMP

const char lasttune[]="_last"; // name of file into which tune will be saved at run start (i.e. _last.odb and _last_psm.odb)

/*------------------------------------------------------------------*/
// Include relevent code

#include "update_run_parameters.c"
#include "type1_compute.c"
#include "type2_compute.c"

/*------------------------------------------------------------------*/
int rangecheck(double delay, double min_delay, double time_slice)
{ /* Check for minimal delay and time_slice*/
  double f;
  //char str [32];
  DWORD i=0;

  if (delay < min_delay) return 0;      /*Errorcond.*/
  if (time_slice == 0) return 0;
  f = delay / time_slice;
  cm_msg(MINFO,"Rangecheck","pre calc delay %f ",delay);
  while (i<f) i++;
  f = i * time_slice;
  cm_msg(MINFO,"Rangecheck","post calc f %f ",f);

	 		
  return 1; // success
}
/*------------------------------------------------------------------*/

div_double div_f (double num, double denom,const double timeslice)
{
  div_t     n;
  div_double m;
  int a,b;

  //  printf("div_f:  num=%f, denom=%f, timeslice=%f\n",num,denom,timeslice);
  a = num   / timeslice;
  b = denom / timeslice;
  n = div (a,b);
  m.quot = n.quot;
  m.rem  = n.rem  * timeslice;
  // printf("num=%f;denom=%f;timeslice=%f;  a=%d;b=%d;  n.quot=%d;n.rem=%d; m.rem=%f\n",
  //	  num,denom,timeslice,a,b,n.quot,n.rem,m.rem);
 return m;
}


/*------------------------------------------------------------------*/

int build_f_table(char *ppg_mode)
{
  /* Modify so this works with start < stop value */
  FILE  *freqfile;
  char  str[256];
  INT freq,f_stop;
  INT f_inc   ;
  INT  i;
  div_t n;
  INT f_ninc;

 /*Create frequency table */
  i = 0;
  freq  =  ppg.input.frequency_start__hz_;
  f_inc =  ppg.input.frequency_increment__hz_;
  f_stop = ppg.input.frequency_stop__hz_;
  sprintf(str,"%s%s%s",ppg.input.cfg_path,ppg_mode,".fsc");

  /* printf("build_f_table: freq start = %d stop = %d incr = %d\n",
	ppg.input.frequency_start__hz_ ,
	ppg.input.frequency_stop__hz_, 
	ppg.input.frequency_increment__hz_);
  */

  if(f_inc == 0)
    {
      printf("build_f_table: frequency increment cannot be 0 \n");
      cm_msg(MERROR,"build_f_table","frequency increment cannot be 0");
      return -1;
    }
 if ( abs(f_inc) > abs(f_stop - freq))
   { 
     printf("build_f_table: frequency increment (%d) is too large \n",f_inc);
     cm_msg(MERROR,"build_f_table","frequency increment (%d) is too large",f_inc);
      return -1;
    }

 if( ((f_inc > 0) && (f_stop < freq)) ||
	      ((f_inc < 0) && (freq < f_stop)) )
	    f_inc *= -1;

 printf("build_f_table: freq start = %d stop = %d incr = %d\n",
	freq,f_stop,f_inc);

  f_ninc = (  abs(f_stop - freq)/ abs(f_inc) ) +1;

  // printf("no. increments %d diff %d freq_inc = %d\n",f_ninc,(f_stop-freq),f_inc);
  if( f_ninc < 1)
    {      
      cm_msg(MERROR,"build_f_table","too few frequency increments");
      printf("build_f_table: too few frequency increments\n");
      return -1;
    }
  
  printf("build_f_table: about to open frequency table file %s\n",str);
  if ((freqfile = (fopen(str,"w"))) != NULL)
   {
     for(i=0; i<f_ninc; i++)
       {
	 printf("i=%d freq=%d f_inc=%d\n",i,freq,f_inc);
	  
	 if (freq != f_switch_boundary)
	   n = div (freq,f_switch_boundary);
	 else
	   n = div ((freq + 1),f_switch_boundary);
	  
	  
	 sprintf(str,"%u \n",freq);
	 fputs(str,freqfile);
	 freq = freq + f_inc;
       }

     sprintf(str,"%u \n",f_ninc);
     if (!(1))fputs(str,freqfile);
     fclose(freqfile);

     printf("final freq: %d\n",freq);
     
     ppg.output.num_frequency_steps =  f_ninc;
     ppg.output.frequency_stop__hz_ = (float) (freq - f_inc);
     printf("final freq = %f,  num steps=%d\n",
	    ppg.output.frequency_stop__hz_,
	     ppg.output.num_frequency_steps);
     
     return f_ninc;
   }
  else
    {
      printf("Build_f_table: Error opening frequency file %s \n",str);
      cm_msg(MERROR,"Build_f_table","Error opening frequency file %s ",str);
      return -1;
    }
}

#ifdef PSM
//  PSM PSMII PSMIII
/*------------------------------------------------------------------*/
int build_f_table_psm(char *ppg_mode)
{
  /* build a frequency table for the psm 

  note - for Type 2 file is in HEX format (file will be loaded into PSM) for table-driven modes
         for Type 1 file is not loaded. 
	       File is used only by Types 1a,1b with randomized values, but freq values are in Hz)
*/
  FILE  *freqfile;
  char  str[256];
  double freq,fstep,fstop;
  INT  i,j;
  //char  * f_bsd;
  //int  ad_test;

  /* The following factors are calculated using
  For PSM
         freq (Hz) = (Frequency Tuning Word  * 40 * 10^6 ) / 2^32 
  For PSMII and PSMIII
         freq (Hz) = (Frequency Tuning Word  * 200 * 10^6 ) / 2^32 
         i.e.  PSMII/III finc is 5 * PSM finc  (see documentation)
  */
#ifdef PSMII
  const double finc =  0.04656613; // PSMII
  const double fmax =  8 * pow(10,7) - finc;
#else
#ifdef PSMIII
  const double finc =  0.04656613;  // PSMIII
  const double fmax =  8 * pow(10,7) - finc;
#else
  const double finc = 0.009313226;  // PSM
  const double fmax = 4 * pow(10,7) - finc;

#endif 
#endif

  double ftemp;
  unsigned long int freqx,fstepx,fstopx; 
  int my_debug=TRUE;
  long int freq_inc;
  INT f_ninc;
  BOOL hz;  /*if true, freq values will be in Hz in the file */



  printf(" build_f_table_psm:  finc = %f and fmax = %f Hz\n",finc,fmax);

  if( ppg.input.frequency_stop__hz_ > fmax)
    {
      cm_msg(MERROR,"build_f_table_psm","stop freq (%uMHz) exceeds device maximum (%.1e)",
	     (DWORD)(ppg.input.frequency_stop__hz_/pow(10,6)) , fmax);
      printf("build_f_table_psm: stop freq (%uMHz) exceeds device maximum (%.1e)\n",
	     (DWORD)(ppg.input.frequency_stop__hz_/pow(10,6)) , fmax);
      return -1;
    }

  freq_inc =  (DWORD)ppg.input.frequency_increment__hz_;
  if(freq_inc == 0)
    {
      cm_msg(MERROR,"build_f_table_psm","frequency increment cannot be 0");
      printf("build_f_table_psm: frequency increment cannot be 0 \n");
      return -1;
    }

 // use labs for long argument
 if ( labs(freq_inc) > labs( ppg.input.frequency_stop__hz_ - ppg.input.frequency_start__hz_ ))
   { 
     cm_msg(MERROR,"build_f_table_psm","frequency increment (%ld) is too large",freq_inc);
     printf("build_f_table_psm: frequency increment (%ld) is too large \n",freq_inc);
     return -1;
   }


  /* Modify so this works with start < stop value */
  if( ((freq_inc > 0) && 
       ( ppg.input.frequency_stop__hz_ <  ppg.input.frequency_start__hz_)) ||
      ((freq_inc < 0) && 
       ( ppg.input.frequency_start__hz_ <  ppg.input.frequency_start__hz_)) )
	    freq_inc *= -1;

 printf("build_f_table_psm: freq start = %u stop = %u incr = %ld\n",
	(DWORD)ppg.input.frequency_start__hz_, 
	(DWORD)ppg.input.frequency_stop__hz_,
	freq_inc);

 // use labs for long argument
 f_ninc = (  labs( (DWORD)ppg.input.frequency_stop__hz_ -  (DWORD)ppg.input.frequency_start__hz_)
	     / labs(freq_inc) ) +1;

 printf("no. increments %d  freq_inc = %ld\n",f_ninc,freq_inc);
 if( f_ninc < 1)
    { 
      cm_msg(MERROR,"build_f_table_psm","too few frequency increments");
      printf("build_f_table_psm: too few frequency increments\n");
      return -1;
    }


  /*Create frequency table */
  
  i = 0;
  freq  = (double) ppg.input.frequency_start__hz_;
  ftemp = freq/finc +0.5 ;// round up ftemp is type double
  freqx = (unsigned long int)ftemp; // freq start value in hex

  //  fstep =  (double) ppg.input.frequency_increment__hz_;
  fstep = (double) freq_inc; // direction may have been reversed above
  ftemp = fstep/finc +0.5 ;// round up
  fstepx = (unsigned long int)ftemp; // step value in hex
  
  fstop = (double) ppg.input.frequency_stop__hz_;
  ftemp = fstop/finc +0.5 ;// round up
  fstopx = (long int)ftemp; // stop value in hex
  
  /* calculate the number of steps needed */
  j = 1 + (int)(fstop-freq)/fstep;
  
  printf("start freq=%.1fHz; step by=%.1fHz  stop at %.1fHz\n",freq,fstep,fstop);
  printf("freqx = %lu (0x%lx) ;  fstepx = %lu (0x%lx); fstopx = %lu (0x%lx)\n",
	 freqx,freqx,fstepx,fstepx,fstopx,fstopx);

  //printf("Number of steps needed %d should agree with f_ninc=%d\n",j,f_ninc);
  
  sprintf(str,"%s%s%s",ppg.input.cfg_path,ppg_mode,".psm");
  if(strncmp("1",ppg_mode,1)==0) /* look for type 1 */
    hz=TRUE; /* values in table should be in hz */
  else
    hz = FALSE;

  printf("build_f_table_psm: about to open frequency table file %s\n",str);
  if(hz)
    printf("   freq values will be in Hz\n");
  else 
    printf("   freq values will be in Hex format\n");

  if ((freqfile = fopen(str,"w")) != NULL)
    {
      for(i=0; i<j; i++)
	{
	  /* print some extra stuff for debugging including freq in Hz */
	  if(my_debug)
	    {
	      if(i==0)
		{
		  printf(" 0x%lx  (%uHz) %d; stop=%uHz; step=%uHz; %d steps \n",
			 freqx,(unsigned int)freq, i,
			 (unsigned int)ppg.input.frequency_stop__hz_,
			 (unsigned int)freq_inc,j);
		}
	      else
		printf("0x%x  (%uHz) %d \n",
		       (unsigned int)freqx,(unsigned int)freq,i);
	    }

	  if(hz)
	    sprintf(str,"%lu",(unsigned long int)freq);
	  else
	    sprintf(str,"%lx",freqx);
	  fputs(str,freqfile);
          fputs("\n",freqfile);
	  freqx = freqx + fstepx;
	  freq = (double)freqx  * finc +0.5; // debugging, convert to Hz       
	}
      fclose(freqfile);
      ppg.output.num_frequency_steps =  j;
      ftemp = (double)(freqx - fstepx) ; // calculate last value
      freq = ftemp * finc + 0.5 ; // round up
    
      printf("num freq steps = %d\n",j);    
      ppg.output.frequency_stop__hz_ =  (DWORD)freq;
      printf("Freq stop in hz = %d\n",ppg.input.frequency_stop__hz_);
      printf("Actual freq stop will be %f or %lu\n",freq,(unsigned long int)freq);
      return j;
    }
  else
    {
      cm_msg(MERROR,"Build_f_table_psm","Error opening frequency file %s ",str);
      printf("Build_f_table_psm: Error opening frequency file %s ",str);
      return -1;
    }
}

// PSMIII only
#ifdef PSMIII
/*------------------------------------------------------------------*/
char *get_channel(INT n)
{
  char *channel_name[]={"f0ch1","f0ch2","f0ch3","f0ch4","f1","all","unknown"};
  return (n<0 || n>MAX_CHANNELS) ? channel_name[4] : channel_name[n];
  return "unknown";
}

/*------------------------------------------------------------------*/
INT check_identical_bandwidth(void)
{
  // Check bandwidth is identical for all enabled channels 
  // ( called for 2w mode only)
  INT channel_index;
  char channel_name[5];
  INT bandwidth,my_bandwidth;
  BOOL first;

  printf("check_identical_bandwidth starting\n");
  bandwidth=0; first=1;
  
  for (channel_index=0; channel_index< MAX_CHANNELS; channel_index++) // cycle through channels 
    {
      sprintf(channel_name, "%s", get_channel(channel_index));
      
      switch (channel_index)
	{
	case 0:
	  
	  if(ppg.hardware.psm.f0ch1.channel_enabled && ppg.hardware.psm.f0ch1.quadrature_modulation_mode) // if f0 ch1  is enabled and quad mode
	    {
	      my_bandwidth= ppg.hardware.psm.f0ch1.iq_modulation.requested_bandwidth__hz_;
	      
	      if(first)
		{
		  bandwidth = my_bandwidth;
		  first=0;
		}
	      else
		{
		  printf("ch1 bandwidth=%d my_bandwidth=%d\n",bandwidth,my_bandwidth);
		  if(abs(bandwidth) != abs(my_bandwidth))
		    goto err;
		}
	    }
	  break;
	  
	case 1:
	  if(ppg.hardware.psm.f0ch2.channel_enabled && ppg.hardware.psm.f0ch2.quadrature_modulation_mode) // if f0 ch2  is enabled and quad mode
	    
	    {  // Check bandwidth is identical  for ch2
	      
	      my_bandwidth= ppg.hardware.psm.f0ch2.iq_modulation.requested_bandwidth__hz_;
	      
	      if(first)
		{
		     bandwidth = my_bandwidth;
		     first=0;
		}
	      else
		{
		  printf("ch2 bandwidth=%d my_bandwidth=%d\n",bandwidth,my_bandwidth);
		    if(abs(bandwidth) != abs(my_bandwidth))
		    goto err;
		}
	      
	    } // end enabled chan 2
	  
	  break;
	case 2:
	  
	  if(ppg.hardware.psm.f0ch3.channel_enabled && ppg.hardware.psm.f0ch3.quadrature_modulation_mode ) // if f0 ch3  is enabled and quad mode
	    
	       {  // Check bandwidth is identical  for ch3
		 
		 my_bandwidth= ppg.hardware.psm.f0ch3.iq_modulation.requested_bandwidth__hz_;
		 
		 if(first)
		   {
		     bandwidth = my_bandwidth;
		     first=0;
		   }
		 else
		   {
		     printf("ch3 bandwidth=%d my_bandwidth=%d\n",bandwidth,my_bandwidth);
		       if(abs(bandwidth) != abs(my_bandwidth))
		       goto err;
		   }
		 
	       } // end enabled ch 3
	  break;
	  
	case 3:
	  
	  if(ppg.hardware.psm.f0ch4.channel_enabled && ppg.hardware.psm.f0ch4.quadrature_modulation_mode ) // if f0 ch4  is enabled and quad mode
	    
	    {  // Check bandwidth is identical  for ch4
	      my_bandwidth= ppg.hardware.psm.f0ch4.iq_modulation.requested_bandwidth__hz_;
	      
	      if(first)
		{
		  bandwidth = my_bandwidth;
		  first=0;
		}
	      else
		{
		  printf("ch4 bandwidth=%d my_bandwidth=%d\n",bandwidth,my_bandwidth);
		    if(abs(bandwidth) != abs(my_bandwidth))
		  goto err;
		}
	      
	    } // end ch 4
	  break;
	  
	case 4:
	  
	  if(ppg.hardware.psm.f1.channel_enabled && ppg.hardware.psm.f1.quadrature_modulation_mode) // if f1  is enabled and quad mode
	    
	    {  // Check bandwidth is identical  for ch4
	      
	      my_bandwidth= ppg.hardware.psm.f1.iq_modulation.requested_bandwidth__hz_;
	      
	      if(first)
		{
		  bandwidth = my_bandwidth;
		  first=0;
		}
	      else
		{
		  printf("ch f1 bandwidth=%d my_bandwidth=%d\n",bandwidth,my_bandwidth);
		    if(abs(bandwidth) != abs(my_bandwidth))
		    goto err;
		}
	    } // end ch f1
	  break;
	}// end switch
    }// end for loop

  if(bandwidth ==0 )
    {
      cm_msg(MERROR,"rf_config","mode 2w bandwidth must be > 0 ");
      printf("rf_config: mode 2w bandwidth must be > 0 \n");
      return -1;
    }

  return SUCCESS;
  
 err:
  cm_msg(MERROR,"rf_config","mode 2w bandwidth must be the same for all enabled channels");
  printf("check_identical_bandwidth: mode 2w bandwidth must be the same for all enabled channels\n");
  printf("       Channel \"%s\" has  bandwidth=%d but previous enabled channel has bandwidth= %d\n",			     
	 channel_name, my_bandwidth,  bandwidth );
  return -1;
}


/*------------------------------------------------------------------*/
INT check_psm_quad(char *ppg_mode)
{
  // called from tr_prestart
  INT status;
  char channel_name[5];
  BOOL quad,load_iq_file;
  INT channel_index;
  INT first;
  //char str[80];

  /* PSMIII 
        Single Tone mode : modulated with IQ pair
        Quadrature mode:   load an IQ file, build the I,Q pairs table
  */
  first=1;

  if(strncmp(ppg_mode,"2w",2)==0)
    {
      if(check_identical_bandwidth() != SUCCESS)
	{
	  printf("check_psm_quad: returning after error from check_identical_bandwidth (mode 2w)\n");
	  return -1;
	}
    }


  for (channel_index=0; channel_index< MAX_CHANNELS; channel_index++) // cycle through channels 
    {
      quad=0;
      sprintf(channel_name, "%s", get_channel(channel_index));
    printf("check_psm_quad:  channel %s \n",channel_name);
      switch (channel_index)
	{
	case 0:
	  if(ppg.hardware.psm.f0ch1.channel_enabled) // if f0 ch1  is enabled
	    {
	      printf("check_psm_quad: working on channel %s \n",channel_name);
              quad = ppg.hardware.psm.f0ch1.quadrature_modulation_mode;
	   
	      if(quad)
		{ // quad mode
		  printf("check_psm_quad: quad mode is selected for channel \"%s\" (channel_index=%d) in  ppg_mode=%s\n",
			 channel_name,channel_index ,ppg_mode);
		  load_iq_file =  ppg.hardware.psm.f0ch1.iq_modulation.load_i_q_pairs_file;
	      
		  if(load_iq_file)
		    {   
		      // build individual tables for each channel in case a phase correction is applied
		      printf("check_psm_quad: calling build_iq_table_psm3 for channel  \"%s\" \n",channel_name); 
		      status =  build_iq_table_psm3(ppg_mode, channel_name);
		      if ( status == -1 )
			{
			  cm_msg(MERROR,"check_psm_quad","Error return from build_iq_table_psm3 for channel  \"%s\" ",
				 channel_name);
			  return status;
			}
		    } // end of build table
		
		  
		} // end of quad mode for channel channel_index
	      
	      else   //  single tone mode - will be modulated by a single i,q pair; i,q file NOT loaded
		printf("check_psm_quad: quad mode is NOT selected for channel %s\n",channel_name);
	      
	      break;		
	    }


	case 1:

	  if(ppg.hardware.psm.f0ch2.channel_enabled) // if f0 ch2 is enabled
	    {
              quad = ppg.hardware.psm.f0ch2.quadrature_modulation_mode;
	      if(quad)
		{ // quad mode
		  printf("check_psm_quad: quad mode is selected for channel \"%s\" (channel_index=%d) in  ppg_mode=%s\n",
			 channel_name,channel_index ,ppg_mode);
		  load_iq_file =  ppg.hardware.psm.f0ch2.iq_modulation.load_i_q_pairs_file;
	      
		  if(load_iq_file)
		    {  // quad mode is enabled 
		      // build individual tables for each channel in case a phase correction is applied
		      printf("check_psm_quad: calling build_iq_table_psm3 for channel  \"%s\" \n",channel_name); 
		      status =  build_iq_table_psm3(ppg_mode, channel_name);
		      if ( status == -1 )
			{
			  cm_msg(MERROR,"check_psm_quad","Error return from build_iq_table_psm3 for channel  \"%s\" ",
				 channel_name);
			  return status;
			}
		    } // end of build table
	
	
		  
		} // end of quad mode for channel channel_index
	      
	      else   //  single tone mode - will be modulated by a single i,q pair; i,q file NOT loaded
		printf("check_psm_quad: quad mode is NOT selected for channel %s\n",channel_name);
	      
  
              break;
	    }
	 
	case 2:
	  if(ppg.hardware.psm.f0ch3.channel_enabled) // if f0 ch3 is enabled
	    {
              quad = ppg.hardware.psm.f0ch3.quadrature_modulation_mode;
	      if(quad)
		{
		  printf("check_psm_quad: quad mode is selected for channel \"%s\" (channel_index=%d) in  ppg_mode=%s\n",
			 channel_name,channel_index ,ppg_mode);
		  load_iq_file =  ppg.hardware.psm.f0ch3.iq_modulation.load_i_q_pairs_file;
	          	      
		  if(load_iq_file)
		    { 
		      // build individual tables for each channel in case a phase correction is applied
		      printf("check_psm_quad: calling build_iq_table_psm3 for channel  \"%s\" \n",channel_name); 
		      status =  build_iq_table_psm3(ppg_mode, channel_name);
		      if ( status == -1 )
			{
			  cm_msg(MERROR,"check_psm_quad","Error return from build_iq_table_psm3 for channel  \"%s\" ",
				 channel_name);
			  return status;
			}
		    } // end of build table
	
		
		  
		} // end of quad mode for channel channel_index
	      
	      else   //  single tone mode - will be modulated by a single i,q pair; i,q file NOT loaded
		printf("check_psm_quad: quad mode is NOT selected for channel %s\n",channel_name);
	      
	
              break;
	    }

	case 3:
	  if(ppg.hardware.psm.f0ch4.channel_enabled) // if f0 ch4 is enabled
	    {
              quad = ppg.hardware.psm.f0ch4.quadrature_modulation_mode;
	      if(quad)
		{	
		  printf("check_psm_quad: quad mode is selected for channel \"%s\" (channel_index=%d) in  ppg_mode=%s\n",
			 channel_name,channel_index ,ppg_mode);
		  load_iq_file =  ppg.hardware.psm.f0ch4.iq_modulation.load_i_q_pairs_file;
	          	      
		  if(load_iq_file)
		    { 
		      // build individual tables for each channel in case a phase correction is applied
		      printf("check_psm_quad: calling build_iq_table_psm3 for channel  \"%s\" \n",channel_name); 
		      status =  build_iq_table_psm3(ppg_mode, channel_name);
		      if ( status == -1 )
			{
			  cm_msg(MERROR,"check_psm_quad","Error return from build_iq_table_psm3 for channel  \"%s\" ",
				 channel_name);
			  return status;
			}
		    } // end of build table
	

	
		} // end of quad mode for channel channel_index
	      
	      else   //  single tone mode - will be modulated by a single i,q pair; i,q file NOT loaded
		printf("check_psm_quad: quad mode is NOT selected for channel %s\n",channel_name);
	      
              break;
	    }
	
	case 4:
	  printf("case 4\n");
	  if(ppg.hardware.psm.f1.channel_enabled) // if channel f1  is enabled
	    {
              quad = ppg.hardware.psm.f1.quadrature_modulation_mode;
	      if(quad)
		{	
		  printf("check_psm_quad: quad mode is selected for channel \"%s\" (channel_index=%d) in  ppg_mode=%s\n",
			 channel_name,channel_index ,ppg_mode);
		  load_iq_file =  ppg.hardware.psm.f1.iq_modulation.load_i_q_pairs_file;
		  
		  if(load_iq_file)
		    { 
		      // build individual tables for each channel in case a phase correction is applied
		      printf("check_psm_quad: calling build_iq_table_psm3 for channel  \"%s\" \n",channel_name); 
		      status =  build_iq_table_psm3(ppg_mode, channel_name);
		      if ( status == -1 )
			{
			  cm_msg(MERROR,"check_psm_quad","Error return from build_iq_table_psm3 for channel  \"%s\" ",
				 channel_name);
			  return status;
			}
		    } // end of build table
		  
		  
	
		} // end of quad mode for channel channel_index
	      
	      else   //  single tone mode - will be modulated by a single i,q pair; i,q file NOT loaded
		printf("check_psm_quad: quad mode is NOT selected for channel %s\n",channel_name);
	      
              break;
	    }
	 
	
	  default:
	    printf("check_psm_quad: channel %s (index %d)  is not enabled\n",channel_name,channel_index);
             goto next;
	
	}
    next: continue;   
    } // end of for loop on all channels

  // for 2s  with psm3 this is overwritten in  build_iq_table_psm3  with Nc+1
  // if(strncmp(ppg_mode,"2s",2)==0)
  //  {
  //    ppg.output.psm.num_cycles_iq_pairs__nc_[channel_index] =1; // buffer factor Nc
  //    cm_msg(MINFO,"rf_config","*** mode 2s writing  ppg.output.psm.num_cycles_iq_pairs__nc_ =1 for channel %d",
  //	     channel_index);
  //  }

  status = check_psm_channel();
  if(status == 1)
    printf("check_psm_quad: status=%d from check_psm_channel (1=success)\n",status);
  return status;  // SUCCESS =1
}


INT check_psm_channel(void)
{
  // also checks gate ( check_psm_gate)
  INT amplitude_flag;
  printf("***   check_psm_channel: starting\n");
  /* called from check_psm_quad */
  
  amplitude_flag=0;
  /* check input parameters:  at least one channel is enabled, it has amplitude and fp gate enabled > 0 */
 
  if(ppg.hardware.psm.f0ch1.channel_enabled  && ppg.hardware.psm.f0ch1.scale_factor__def_181_max_255_ > 0 
     && ppg.hardware.psm.f0ch1.gate_control == GATE_NORMAL_MODE)
    return SUCCESS;
  if(ppg.hardware.psm.f0ch2.channel_enabled && ppg.hardware.psm.f0ch2.scale_factor__def_181_max_255_ > 0
      && ppg.hardware.psm.f0ch2.gate_control == GATE_NORMAL_MODE)
    return SUCCESS;
  if(ppg.hardware.psm.f0ch3.channel_enabled && ppg.hardware.psm.f0ch3.scale_factor__def_181_max_255_ > 0
     && ppg.hardware.psm.f0ch3.gate_control == GATE_NORMAL_MODE )
    return SUCCESS;
  if(ppg.hardware.psm.f0ch4.channel_enabled && ppg.hardware.psm.f0ch4.scale_factor__def_181_max_255_ > 0
      && ppg.hardware.psm.f0ch4.gate_control == GATE_NORMAL_MODE)
   return SUCCESS;
  if(ppg.hardware.psm.f1.channel_enabled && ppg.hardware.psm.f1.scale_factor__def_181_max_255_ > 0
     && ppg.hardware.psm.f1.gate_control == GATE_NORMAL_MODE )
    return SUCCESS;
    
  printf("check_psm_channel: At least one PSM channel must be enabled, front panel gate selected and scale factor (amplitude) set > 0 \n");
  cm_msg(MERROR,"check_psm_channel",
	     "No PSM channel is enabled (i.e. enabled, front panel gates selected & scale factor (amplitude) set > 0) ");
 
 return -1; // FAILURE
}

/*------------------------------------------------------------------*/
INT get_channel_index(char *channel_name, INT *pindex)
{
  /* get the array index depending on channel (f0ch1-f0ch4 or f1) 
{"f0ch1","f0ch2","f0ch3","f0ch4","f1","all","unknown"};
   */
    INT i;
    for(i=0; i<strlen(channel_name);i++)
      channel_name[0]=tolower(channel_name[0]);

    if(strncmp("f0ch",channel_name,4)==0)
      i=atoi(&channel_name[4]);
    else if (strcmp("f1", channel_name)==0)
      i=5;
    else
      {
	printf("PSM channel name (%s) must be one of \"f0ch1\",\"f0ch2\",\"f0ch3\",\"f0ch4\",\"f1\"\n",channel_name);
	return -1;
      }
    *pindex = i-1;
  return(SUCCESS);
}




//--------------------------------------------------------
int build_iq_table_psm3(char *ppg_mode, char *channel_name)
//----------------------------------------------------
{
  /* build iq table for the psmIII; called from check_psm_quad 
   */
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
  FILE  *iqfile;
  FILE  *dbgfile;
  char  moduln_mode[16];
  char  str[256];
  char  astr[256];
  BOOL  I_zero; // temp flag
  INT  i,j,q;
  
  INT my_debug=FALSE;
  INT N, Niq;
  double dNiq;
  
  double d_nu;
  double b;
  INT I,Q;
  INT Niq_min;


  // temp variables
  double x;
  
  double d_nu_min,d_nu_max;
  double factor;
  const   INT u = 5;
  INT Nc1;
  double Tp,Tpc,MaxTpc,MinTpc,dNc1;
  
  INT mod_func; /* set to 0 for ln_sech, 1 for Hermite */  
  INT   Ncmx;  
  INT channel_index;

  BOOL set_const_i_file,  set_const_q_file;
  INT const_i, const_q;

  if(strncmp(ppg_mode,"2w",2)==0)
    {   
      INT status;
      status =  build_iq_table_psm3_2w(ppg_mode, channel_name);
      return status;
    }
    
    


  cm_msg(MINFO,"build_iq_table_psm3","starting with channel %s  ",channel_name);
  if(get_channel_index(channel_name,&channel_index) != SUCCESS)
  {
    printf("build_iq_table_psm3  : illegal channel_name %s\n",channel_name);
    cm_msg(MERROR,"build_iq_table_psm3","illegal channel_name %s  ",channel_name);
    return -1;
  }
  printf("\n  build_iq_table_psm3 : working on channel_name \"%s\" (channel_index %d) (PSM III) \n",channel_name,channel_index);
  
  switch (channel_index)
    {
    case 0:
      strncpy( moduln_mode, ppg.hardware.psm.f0ch1.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch1.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch1.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch1.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch1.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch1.iq_modulation.const_q__511__q__512_;
      break;

    case 1:
      strncpy( moduln_mode, ppg.hardware.psm.f0ch2.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch2.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch2.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch2.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch2.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch2.iq_modulation.const_q__511__q__512_;
      break;

   case 2:
      strncpy( moduln_mode, ppg.hardware.psm.f0ch3.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch3.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch3.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch3.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch3.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch3.iq_modulation.const_q__511__q__512_;
      break;

   case 3:
      strncpy( moduln_mode, ppg.hardware.psm.f0ch4.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch4.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch4.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch4.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch4.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch4.iq_modulation.const_q__511__q__512_;
      break;

    case 4:
      strncpy( moduln_mode, ppg.hardware.psm.f1.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f1.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f1.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f1.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f1.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f1.iq_modulation.const_q__511__q__512_;
      break;
    default:
      printf("build_iq_table_psm3 : Error: channel_name %d i.e. \"%s\" is not supported\n",channel_index,channel_name);
      cm_msg(MERROR,"build_iq_table_psm3","Error: channel_name %d i.e. \"%s\" is not supported ",channel_index,channel_name);
      return -1;
    }  

  /* Check the modulation mode (ln-sech or Hermite) */   
  sprintf( str,"%s", moduln_mode);
  str[2]='\0'; /* terminate after two characters */
  for  (j=0; j< strlen( str) ; j++) 
    str[j] = tolower (str[j]); /* convert to lower case */ 
  
  
  if (strncmp( str,"ln-sech",2) == 0)
    {
      mod_func=0 ;
      printf("build_iq_table_psm3: detected ln-sech modulation mode\n");
      b = 5.2983;
      factor =  b * u / M_PI;  // 8.4325
      d_nu_min = 40; // Hz
      d_nu_max = 164688; // Hz
      Niq_min = 1024;
    }
  else if  (strncmp( str,"hermite",2) == 0)
    {
      mod_func = 1;
      printf("build_iq_table_psm3: detected Hermite modulation mode\n");
      b = 2.5;
      factor =  4 * sqrt (-log(.5)) * b / M_PI; // 2.64
      d_nu_min = 12.6; // Hz
      d_nu_max = 51562; // Hz
      Niq_min = 256;
    }
  else
    {
      printf("build_iq_table_psm3: Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported \n",
	     moduln_mode);
      cm_msg(MERROR,"build_iq_table_psm3", "Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported ",
	     moduln_mode);
      return -1;
    }
  /* Ncmx  is written into ../output/psm by frontend
     We got the record for "output" */
 
  if(debug) printf("Ncmx from output record in odb is =%d\n", 
		   ppg.output.psm.max_cycles_iq_pairs__ncmx_); // note one value for all channels
  Ncmx =  ppg.output.psm.max_cycles_iq_pairs__ncmx_; // note one value for all channels

  if(Ncmx != 4096)
    {
      cm_msg(MINFO,"build_iq_table_psm3","Unexpected value of param Ncmx read from ODB ../output/psm for PSMIII (%d). Using Ncmx=4096",Ncmx);

      printf("build_iq_table_psm3: Unexpected value of param Ncmx read from ODB ../output/psm for PSMIII (%d). Expect 4096. Continuing with Ncmx=4096\n",Ncmx);
  // Set to 4096 and continue
    Ncmx = 4096; // Ncmx is 4096 for PSMIII
    }	
						   
  //  if (my_debug)
    {
      // printf("build_iq_table_psm3:  for Modulation mode \"%s\" :\n",moduln_mode);
      printf(" bandwidth (d_nu) = %lf Hz\n",d_nu);
      printf(" b=%f; factor=%f;  Ncmx = %d\n",b,factor,Ncmx);
      printf(" d_nu_min = %.3f  d_nu_max = %.3f   Hz\n",d_nu_min,d_nu_max);
    }

  /* check d_nu is in range */
  if( (d_nu < d_nu_min)  || (d_nu > d_nu_max) )
    {
      cm_msg(MERROR, "build_iq_table_psm3"," requested bandwidth (%.0fHz) is out of range ",
	     d_nu);
      cm_msg(MINFO, "build_iq_table_psm3","bandwidth must be between %f and %f Hz",d_nu_min,d_nu_max);
      printf("build_iq_table_psm3 : bandwidth must be between %f and %f Hz\n",d_nu_min,d_nu_max);
      printf("build_iq_table_psm3 : requested bandwidth (%.0fHz) is out of range\n",d_nu);
      return -1;
    }
  
  Tp = factor/(d_nu * pow(10,-3));   // d_nu in kHz, Tp in ms
  // check Tp in limits - what are the limits?
  printf("Tp(ms)=%lf/d_nu(kHz) -> Tp = %lf  ms\n",factor,Tp);

  //  Nc1= (INT) (0.5 + (Tp/(50*exp(-9)*2048)) ); // round up to nearest integer
  //  Niq =  (INT) (0.5 + (Tp/(50*exp(-9) * Nc1)));
  double f1,f2;
  // Tp in ms here?
  f1=  Tp/(50*pow(10,-6)*2048) ;  
  Nc1 = (INT)(f1 + 1); // next highest integer
  f2 = Tp/(50*pow(10,-6) * Nc1);
  printf("Nc+1=%lf, rounded up Nc+1 = %d\n",f1, Nc1);
  Niq = (INT)(f2 + 1); // next highest integer
  printf("Niq =%lf, rounded up Niq=%d\n",f2,Niq);

  Tpc = 50*pow(10,-6) * Nc1 * Niq; // ms
  printf("Tpc = %lf\n",Tpc);
  MaxTpc =  (2048 * 50 * pow(10,-6) * 2048) ; // in ms
  MinTpc =  (1 * 50 * 1024 * pow(10,-6) ); // in ms
  printf("MinTpc=%fms MaxTpc=%fms\n",MinTpc,MaxTpc);
  if (Tpc < MinTpc )
    printf("build_iq_table_psm3: calculated Tpc (%f)ms is less than minimum value (%f)ms\n",Tpc,MinTpc);
  else if (Tpc >= MaxTpc)
    printf("build_iq_table_psm3: calculated Tpc (%f)ms is greater than maximum value (%f)ms\n",Tpc,MaxTpc);
  if((Tpc < MinTpc) || (Tpc >= MaxTpc))
    {
      cm_msg(MERROR,"build_iq_table_psm3", "Calculated Tpc (%f) is out of range",Tpc);
      printf("build_iq_table_psm3: Calculated Tpc (%f) is out of range",Tpc);
      return -1;
    }

  //  if(Niq <1024 || Niq > 2048)
  if(Niq < Niq_min)
    {
      cm_msg(MERROR,"build_iq_table_psm3", "Calculated Niq (%d) is out of range %d < Niq  ",Niq,Niq_min);
      printf("build_iq_table_psm3: Calculated Niq (%d) is out of range %d < Niq  \n",Niq,Niq_min);
      return -1;
    }


  ppg.output.psm.num_cycles_iq_pairs__nc_[channel_index] = Nc1;
  ppg.output.psm.num_iq_pairs__niq_[channel_index] = Niq;

  dNc1   = (double) Nc1;
  dNiq = (double) Niq;

 
  printf("iq filename will be : %s%s_iq_%s.psm\n",ppg.input.cfg_path,ppg_mode,channel_name);
  sprintf(str,"%s%s_iq_%s.psm",ppg.input.cfg_path,ppg_mode,channel_name); // IQ pair file
  sprintf(astr,"%s%s_iq_%s.dbg",ppg.input.cfg_path,ppg_mode,channel_name); // debug file

  //if(my_debug)
    printf("build_iq_table_psm3 : about to open IQ table DEBUG file %s\n",str);

 

  /* If setting constant values, check they  are reasonable */
  I_zero=FALSE;
  printf("build_iq_table_psm3 : set_const_i_file = %d and set_const_q_file= %d \n",set_const_i_file,set_const_q_file );
  if(set_const_i_file)
    {
      if ( const_i == 0)
	{
	  I_zero=TRUE;
	  if(mod_func ==1) // Hermite: all Q values are zero
	    {
	      cm_msg(MERROR,"build_iq_table_psm3 ",
		     "Modulating with constant I=0 will result in no output for Hermite since Hermite Q values are also zero");
	      printf("build_iq_table_psm3 : Constant I=0 not allowed for Hermite since Q values are zero (no output)\n");
	      return -1;
	    }
	}
      else
	printf("All i values will be set to %d\n",
	       const_i);
      
    }
  
  if(set_const_q_file)
    {    
      if(mod_func == 1  )
	{  // Hermite; all Q values are zero
	  cm_msg(MINFO,"build_iq_table_psm3 ",
		 "Modulating with constant Q=%d has no effect since Q=0 always for Hermite ",
		 const_q);
	}
      else
	{ // ln-sech; check for I=Q=0
	  if ( const_q ==0 )
	    {
	      if(I_zero)
		{
		  cm_msg(MERROR,"build_iq_table_psm3 ",
			 "Modulating with constant I,Q pair=(0,0) will result in no output");
		  printf("build_iq_table_psm3: Constant I=0 and Q=0 are not allowed (no output)\n");
		  return -1;
		}
	    }
	}
      printf("All q values will be set to %d\n",
	     const_q);
      
    }
  
  dbgfile = fopen(astr,"w");
  if(dbgfile)
    {
      printf("successfully opened debug file: %s\n",astr);
      sprintf(astr,"Requested bandwidth(Hz)=%f\n",
	      d_nu);
      fputs(astr,dbgfile);

      sprintf(astr,"Num IQ pairs=%d; pulsewidth Tp =%f ms \n",
	      Niq,  ppg.output.psm.pulse_width__msec_[channel_index]);
      fputs(astr,dbgfile);

      sprintf(astr,"Index: I      Q              i    q (i,q=masked 2's comp) %s","\n"); 
      fputs(astr,dbgfile);
    }
  else
    printf("could not open debug file %s\n",astr);
 

  printf("build_iq_table_psm3 : about to open IQ table file %s\n",str);
  printf("       these will be the values programmed into \"%s\" channel_name\n",channel_name);

  iqfile = fopen(str,"w");
  if (iqfile)    
    {
      N=1; 
      do
	{ 
	  double di,dq;
	 
	  /* ln-sech */
	  if(mod_func == 0)
	    {  // sech = 1/cosh
	      x=b*2*(2*N-Niq-1)/(2*(Niq-1));
	      double sech_x = 1/cosh(x);
	      di = 511*sech_x * cos(u*log(sech_x));
	      dq = 511*sech_x * sin(u*log(sech_x));
	    }
	    
	  else
	    {  /* mod_func = 1, Hermite */	 
	      x=b*2*(2*N-Niq-1)/(2*(Niq-1));
	      di = 511 * (1-pow(x,2)) * exp(-(pow(x,2)));
	      dq = 0;
	    }

	  if(set_const_i_file)
	    I=const_i;
	  else	
	    {    
	      I = (int) (di + 0.5);  // closest integer

	      // Check I value when N=Niq/2
	      if(N==Niq/2)
		{
		  if (I != 510 && I != 511)
		    {
		      printf("build_iq_table_psm3 : Error I=%d for N=Niq/2=%d; expect 510 or 511\n",I,N);
		      return -1;
		    }
		}
	    }

	    i=(~I+1) & I_Q_DATA_MASK; /* 2's complement and mask to 10 bits */
	
	    //i= TwosComp_convert(I,0);
	    //printf("*** I=%d  after conversion i=%d 0x%x\n",I,i,i);

	  if(set_const_q_file)
	    Q=const_q;
	  else
	    Q = (int)(dq+0.5); // closest integer
	   

	    q=(~Q+1) & I_Q_DATA_MASK;/* 2's complement and mask to 10 bits */
	    //q= TwosComp_convert(Q,0);
	    //printf("*** Q=%d  after conversion q=%d 0x%x\n",Q,q,q);
	  if(my_debug)
	    printf("N=%d I,Q pair = %d,%d ->i,q pair =%d,%d; di=%g  dq=%g \n",
		   N,I,Q,i,q,di,dq);
 	  if(dbgfile)
	    {
	      sprintf(astr,"%d:  %d %d   %d  %d %s",N,I,Q,i,q,"\n"); 
	      fputs(astr,dbgfile);
	    }

	  sprintf(str,"%d %d%s",i,q,"\n"); // IQ file contains 2s compliment data
	  fputs(str,iqfile);
	  
	  N++;  
	}
      while ( N <= Niq ); 
  
      fclose(iqfile);
      if(dbgfile)fclose(dbgfile);
      return i;
    }
  else
    {
      cm_msg(MERROR,"build_iq_table_psm3","Error opening iq file %s  ",str);
      printf("build_iq_table_psm3 : Error opening iq file %s \n",str);
      return -1;
    }
}



//--------------------------------------------------------
int build_iq_table_psm3_2w(char *ppg_mode, char *channel_name)
//----------------------------------------------------
{
  /* build iq table for the psmIII; called from check_psm_quad 
   */
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
  FILE  *iqfile;
  FILE  *dbgfile;
  char  my_moduln_mode[16];
  char  str[256];
  char  astr[256];
  BOOL  I_zero; // temp flag
  INT  i,j,q,q0;
  INT Nf;
  

  char *moduln_modes[]={"ln-sech","hermite","wurst","unknown"};
  INT moduln_index; // index into moduln_modes
  INT my_debug=FALSE;

  const INT   Ncmx=4095; // value for PSMIII  
  const INT   Niq_min=1024;
  const INT   Niq_max=2048;

  INT n, Niq, Nwurst;
  double dNiq;
  
  double d_nu,d_nu_khz;
  double b;
  INT I,Q,f0_khz;
  float f1_khz; // Li

  // temp variables
  double x;
  
  double d_nu_min,d_nu_max;
  double factor;
  INT Nc1;
  double Tp,Tpc,MaxTpc,MinTpc,dNc1;
  double Tp_min;
  float phi, phi_radians;

  INT channel_index;

  BOOL set_const_i_file,  set_const_q_file;
  INT const_i, const_q;

  if(strncmp(ppg_mode,"2w",2)!=0)
    {
        cm_msg(MERROR,"build_iq_table_psm3_2w","ppg_mode must be 2w (not %s) for this routine. Use  build_iq_table_psm3  "
	       ,ppg_mode);
	return -1;
    }

  cm_msg(MINFO,"build_iq_table_psm3_2w","starting with channel %s  ",channel_name);
  if(get_channel_index(channel_name,&channel_index) != SUCCESS)
  {
    printf("build_iq_table_psm3_2w  : illegal channel_name %s\n",channel_name);
    cm_msg(MERROR,"build_iq_table_psm3_2w","illegal channel_name %s  ",channel_name);
    return -1;
  }
  printf("\n  build_iq_table_psm3_2w : working on channel_name \"%s\" (channel_index %d) (PSM III) \n",channel_name,channel_index);
  
  /* Ncmx  is written into ../output/psm by frontend
     We got the record for "output" */
  

  printf("Ncmx from output record in odb is =%d\n", 
	 ppg.output.psm.max_cycles_iq_pairs__ncmx_); // note one value for all channels

  if(  ppg.output.psm.max_cycles_iq_pairs__ncmx_ != Ncmx)
    {
      cm_msg(MINFO,"build_iq_table_psm3_2w","Unexpected value of param Ncmx read from ODB ../output/psm for PSMIII (%d). Using Ncmx=%d",
	     ppg.output.psm.max_cycles_iq_pairs__ncmx_, Ncmx);
      
      printf("build_iq_table_psm3_2w: Unexpected value of param Ncmx read from ODB ../output/psm for PSMIII (%d). Expect %d. Continuing with Ncmx=%d\n",  ppg.output.psm.max_cycles_iq_pairs__ncmx_,Ncmx,Ncmx);
      
    }	




  switch (channel_index)
    {
    case 0:
      strncpy( my_moduln_mode, ppg.hardware.psm.f0ch1.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch1.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch1.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch1.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch1.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch1.iq_modulation.const_q__511__q__512_;
      phi=0;
      break;

    case 1:
      strncpy( my_moduln_mode, ppg.hardware.psm.f0ch2.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch2.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch2.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch2.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch2.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch2.iq_modulation.const_q__511__q__512_;
      phi =  ppg.hardware.psm.f0ch2.iq_modulation.phase_correction__degrees_;
      break;

   case 2:
      strncpy( my_moduln_mode, ppg.hardware.psm.f0ch3.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch3.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch3.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch3.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch3.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch3.iq_modulation.const_q__511__q__512_;
      phi =  ppg.hardware.psm.f0ch3.iq_modulation.phase_correction__degrees_;
      break;

   case 3:
      strncpy( my_moduln_mode, ppg.hardware.psm.f0ch4.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch4.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch4.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch4.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch4.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch4.iq_modulation.const_q__511__q__512_;
      phi =  ppg.hardware.psm.f0ch4.iq_modulation.phase_correction__degrees_;
      break;

    case 4:
      strncpy( my_moduln_mode, ppg.hardware.psm.f1.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f1.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f1.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f1.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f1.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f1.iq_modulation.const_q__511__q__512_;
      phi =  ppg.hardware.psm.f1.iq_modulation.phase_correction__degrees_;
      break;
    default:
      printf("build_iq_table_psm3_2w : Error: channel_name %d i.e. \"%s\" is not supported\n",channel_index,channel_name);
      cm_msg(MERROR,"build_iq_table_psm3_2w","Error: channel_name %d i.e. \"%s\" is not supported ",channel_index,channel_name);
      return -1;
    }  

  f0_khz= ppg.hardware.psm.idle_freq__hz_ / 1000;
  f1_khz = ppg.input.e2w_li_precession_f1__khz_;
  q0 = ppg.input.e2w_q0__default_5_;
  Tp = (double)ppg.input.e2w_tp__ms_; //ms
  d_nu_khz = d_nu * pow(10,-3); // convert d_nu to khz
  Nf = ppg.input.e2w_freq_resolution_nf;
  Nwurst =  ppg.input.e2w_n__40_or_80_;

  phi_radians = phi*2*M_PI/360;

  printf("build_iq_table_psm3_2w: input params f0=%d khz f1=%f khz q0=%d Tp=%f ms d_nu_khz=%f Nf=%d phi=%f deg\n", 
	 f0_khz, f1_khz, q0,Tp,d_nu_khz,Nf,phi);

  /* Check the modulation mode (ln-sech or Hermite Wurst) */   
  sprintf( str,"%s", my_moduln_mode);
  str[2]='\0'; /* terminate after two characters */
  for  (j=0; j< strlen( str) ; j++) 
    str[j] = tolower (str[j]); /* convert to lower case */ 
  
  
  if (strncmp( str,moduln_modes[0],2) == 0)
    {
      moduln_index=0 ;
      printf("build_iq_table_psm3_2w: detected ln-sech modulation mode\n");
      b = 5.3;
      Tp_min = ( q0* b/(2 * M_PI))  * (fabs(d_nu_khz) /(f1_khz * f1_khz)  ); //ms
      d_nu_min = 0.0512 * 2*M_PI* (f1_khz*f1_khz)/(q0*b); // kHz    minimum bandwidth
      d_nu_max = 419.4304*2*M_PI* (f1_khz*f1_khz)/(q0*b); // kHz  maximum bandwidth
    }
  else if  (strncmp( str,moduln_modes[1],2) == 0)
    {
      moduln_index = 1;
      printf("build_iq_table_psm3_2w: detected Hermite modulation mode\n");
      b = 2.5;
      Tp_min = 4.69/fabs(d_nu_khz);
      d_nu_min = 0.01118; // kHz  minimum bandwidth
      d_nu_max = 91.6; // kHz     maximum bandwidth
      
    }
  else if (strncmp(str,moduln_modes[2],2) == 0)
    {
      moduln_index = 2;
      printf("build_iq_table_psm3_2w: detected Wurst modulation mode\n");
      if(strncmp(ppg_mode,"2w",2)!=0)
	{
	  cm_msg(MINFO,"build_iq_table_psm3_2w","Wurst modulation mode is only supported by PPG Mode 2w");
	  printf("build_iq_table_psm3_2w: Wurst modulation mode is only supported by PPG Mode 2w");
	  return -1;
		   
	}
      b = 1;
      // assuming f1= 0.63 kHz; larger values will give higher max, smaller values a lower min
      
      Tp_min = ( q0* b/(2 * M_PI))  * (fabs(d_nu_khz) /(f1_khz * f1_khz)  ); //ms
      d_nu_min = 0.0512 * 2*M_PI* (f1_khz*f1_khz)/q0; // kHz    minimum bandwidth
      d_nu_max = 419.4304*2*M_PI* (f1_khz*f1_khz)/q0; // kHz  maximum bandwidth
    }
  else
    {
      printf("build_iq_table_psm3_2w: Illegal modulation mode \"%s\"; only modes \"%s\", \"%s\" and  \"%s\" are presently supported \n",
	     str,  moduln_modes[0],  moduln_modes[1],  moduln_modes[2]);
      cm_msg(MERROR,"build_iq_table_psm3_2w", "Illegal modulation mode \"%s\"; only  modes \"%s\", \"%s\" and  \"%s\"are presently supported ",
	     str,moduln_modes[0],  moduln_modes[1],  moduln_modes[2] );
      return -1;
    }




  //  if (my_debug)
    {
      printf(" Modulation mode = \"%s\" \n", moduln_modes[moduln_index]);
      printf(" bandwidth (d_nu) = %lf Hz or %lf kHz\n",d_nu,d_nu_khz);
      printf(" b=%f; Ncmx = %d\n",b,Ncmx);
      printf(" d_nu_min = %.3f kHz  d_nu_max = %.3f   kHz\n",d_nu_min,d_nu_max);
      printf(" Tp=%f  Tp_min =  %f ms \n",Tp,Tp_min);
    }

  if(Tp < Tp_min)
    {
      printf("build_iq_table_psm3_2w:  input value of Tp= %f ms is less than calculated Tp_min= %f ms  for channel \"%s\" (channel_index %d) \n",
	     Tp, Tp_min,   channel_name,channel_index);
      cm_msg(MERROR,"build_iq_table_psm3_2w","input value of Tp= %f ms is less than calculated Tp_min= %f ms  for channel \"%s\" (channel_index %d) ",
	     Tp, Tp_min,   channel_name,channel_index);
	    
      return -1;
    }


  /* check d_nu_khz is in range */

  if( (fabs(d_nu_khz) < d_nu_min)  || (fabs(d_nu_khz) > d_nu_max) )
    {
      cm_msg(MERROR, "build_iq_table_psm3_2w"," requested bandwidth (%.0fHz) is out of range for modulation mode  \"%s\" ",
	     d_nu,moduln_modes[moduln_index]);
	    
      cm_msg(MERROR, "build_iq_table_psm3_2w","bandwidth must be between %f and %f Hz",d_nu_min,d_nu_max);
      printf("build_iq_table_psm3_2w : bandwidth must be between %f and %f Hz\n",d_nu_min,d_nu_max);
      printf("build_iq_table_psm3_2w : requested bandwidth (%.0fHz) is out of range\n",d_nu);
      return -1;
    }
  

 
  // Tp is in ms 
  factor=  Tp/(100*pow(10,-6)*2048) ; // was 50

  Nc1 = (INT)ceil(factor); // next highest integer

  printf("Nc=%lf  rounded up becomes Nc+1 = %d\n",factor, Nc1);

  factor =( Tp- pow(10,-6)) /(100*pow(10,-6) * Nc1);  // subtract 1ns to remove rounding errors was 50
  

  Niq = (INT)ceil(factor); // next highest integer 
  printf("Niq =%.10f  rounded up Niq=%d\n",factor,Niq);

#ifdef PSM2KLUDGE

  INT Nc_psmii, Ncic_psmii, Niq_psmii;

  Nc_psmii =(INT)  ceil(ceil(Tp/(pow(10,-4)*2048))/63);

  Ncic_psmii =(INT) new_min(ceil(Tp/(pow(10,-4)*Nc_psmii*2048)),63);

  Niq_psmii=(INT)ceil(Tp/(Nc_psmii*Ncic_psmii*pow(10,-4)));

  // Report the kludge values for testing with PSM2
  printf("**  build_iq_table_psm3_2w :  psmii kludge values  Nc_psmii=%d Ncic_psmii=%d Niq_psmii=%d **\n",
	 Nc_psmii,Ncic_psmii,Niq_psmii);
 
#endif


 

  Tpc = 100*pow(10,-6) * Nc1 * Niq; // ms was 50
  printf("Tpc = %lf\n",Tpc);
  MaxTpc =  (4096 * 100 * pow(10,-6) * 2048) ; //=  419.4304 ms   was 50
  MinTpc =  (1 * 100 * 1024 * pow(10,-6) ); // = 0.5012 ms was 50
  printf("MinTpc=%fms MaxTpc=%fms\n",MinTpc,MaxTpc);



  if (Tpc < MinTpc )
    {
      printf("build_iq_table_psm3_2w: calculated Tpc (%f)ms is less than minimum value (%f)ms\n",Tpc,MinTpc);
      cm_msg(MERROR,"build_iq_table_psm3_2w","calculated Tpc (%f)ms is less than minimum value (%f)ms",Tpc,MinTpc);
    }
  else if (Tpc >= MaxTpc)
    {
      printf("build_iq_table_psm3_2w: calculated Tpc (%f)ms is greater than maximum value (%f)ms\n",Tpc,MaxTpc);
      cm_msg(MERROR,"build_iq_table_psm3_2w"," calculated Tpc (%f)ms is greater than maximum value (%f)ms",Tpc,MaxTpc);
    }
  if((Tpc < MinTpc) || (Tpc >= MaxTpc))
    {
      cm_msg(MERROR,"build_iq_table_psm3_2w", "Calculated Tpc (%f) is out of range",Tpc);
      printf("build_iq_table_psm3_2w: Calculated Tpc (%f) is out of range",Tpc);
      return -1;
    }


  if(Niq < Niq_min || Niq > Niq_max )
    {
      cm_msg(MERROR,"build_iq_table_psm3_2w", "Calculated Niq (%d) is out of range  %d < Niq < %d  ",Niq,Niq_min,Niq_max);
      printf("build_iq_table_psm3_2w: Calculated Niq (%d) is out of range  %d < Niq < %d \n ",Niq,Niq_min,Niq_max);
   
      return -1;
    }


  ppg.output.psm.num_cycles_iq_pairs__nc_[channel_index] = Nc1;
  ppg.output.psm.num_iq_pairs__niq_[channel_index] = Niq;

  dNc1   = (double) Nc1;
  dNiq = (double) Niq;


  printf("iq filename will be : %s%s_iq_%s.psm\n",ppg.input.cfg_path,ppg_mode,channel_name);
  sprintf(str,"%s%s_iq_%s.psm",ppg.input.cfg_path,ppg_mode,channel_name); // IQ pair file
  sprintf(astr,"%s%s_iq_%s.dbg",ppg.input.cfg_path,ppg_mode,channel_name); // debug file


  //if(my_debug)
    printf("build_iq_table_psm3_2w : about to open IQ table DEBUG file %s\n",str);

 

  /* If setting constant values, check they  are reasonable */
  I_zero=FALSE;
  printf("build_iq_table_psm3_2w : set_const_i_file = %d and set_const_q_file= %d \n",set_const_i_file,set_const_q_file );
  if(set_const_i_file)
    {
      if ( const_i == 0)
	{
	  I_zero=TRUE;
	  if(moduln_index ==1) // Hermite: all Q values are zero
	    {
	      cm_msg(MERROR,"build_iq_table_psm3_2w ",
		     "Modulating with constant I=0 will result in no output for Hermite since Hermite Q values are also zero");
	      printf("build_iq_table_psm3_2w : Constant I=0 not allowed for Hermite since Q values are zero (no output)\n");
	      return -1;
	    }
	}
      else
	printf("All i values will be set to %d\n",
	       const_i);
      
    }
  
  if(set_const_q_file)
    {    
      if(moduln_index == 1  )
	{  // Hermite; all Q values are zero
	  cm_msg(MINFO,"build_iq_table_psm3_2w ",
		 "Modulating with constant Q=%d has no effect since Q=0 always for Hermite ",
		 const_q);
	}
      else
	{ // ln-sech; check for I=Q=0
	  if ( const_q ==0 )
	    {
	      if(I_zero)
		{
		  cm_msg(MERROR,"build_iq_table_psm3_2w ",
			 "Modulating with constant I,Q pair=(0,0) will result in no output");
		  printf("build_iq_table_psm3_2w: Constant I=0 and Q=0 are not allowed (no output)\n");
		  return -1;
		}
	    }
	}
      printf("All q values will be set to %d\n",
	     const_q);
      
    }
  printf("build_iq_table_psm3_2w: calculated num IQ pairs=%d; pulsewidth Tpc =%f ms dwell_time=%f \n",
	      Niq,  Tpc, (float)(Tpc/Nf));
  ppg.output.dwell_time__ms_ =  (float)(Tpc/Nf);

  dbgfile = fopen(astr,"w");
  if(dbgfile)
    {
      printf("successfully opened debug file: %s\n",astr);
      sprintf(astr,"Requested bandwidth(Hz)=%f\n",
	      d_nu);
      fputs(astr,dbgfile);

      sprintf(astr,"Num IQ pairs=%d; pulsewidth Tpc =%f ms dwell_time=%f \n",
	      Niq,  Tpc, (float)(Tpc/Nf));



      fputs(astr,dbgfile);

      sprintf(astr,"Index: I      Q     i    q (i,q=masked 2's comp) %s","\n"); 
      fputs(astr,dbgfile);
    }
  else
    printf("could not open debug file %s\n",astr);
 

  printf("build_iq_table_psm3_2w : about to open IQ table file %s\n",str);
  printf("       these will be the values programmed into \"%s\" channel_name\n",channel_name);

  iqfile = fopen(str,"w");
  if (iqfile)    
    {
      n=1; 
      do
	{ 
	  double di,dq;
	 
	  /* ln-sech */
	  if(moduln_index == 0)
	    {  // sech = 1/cosh
	      x=b*2*(2*n-Niq-1)/(2*(Niq-1));
	      double sech_x = 1/cosh(x);
	      di = 511 * (1.01*sech_x-0.01)*cos(d_nu_khz*2*(M_PI/(4*5.3))*Tpc*log(sech_x)+ phi_radians);
	      dq = 511 * (1.01*sech_x-0.01)*sin(d_nu_khz*2*(M_PI/(4*5.3))*Tpc*log(sech_x)+ phi_radians);
	    }
	    
	  // Hermite
	  else if (moduln_index == 1)
	    {   
	      x=b*2*(2*n-Niq-1)/(2*(Niq-1));
	      di = 511 *(0.99* (1-pow(x,2))+0.01) * exp(-(pow(x,2)));
	      dq = 0;
	    }
	    // Wurst
	  else if (moduln_index == 2)
	    {   
	      x=b*2*(2*n-Niq-1)/(2*(Niq-1));
	      di = 511 * (1-pow(fabs(sin(M_PI*x/2)),Nwurst))*cos(d_nu_khz*2*(M_PI/8)*Tpc*x*x+ phi_radians);
	      dq = 511 * (1-pow(fabs(sin(M_PI*x/2)),Nwurst))*sin(d_nu_khz*2*(M_PI/8)*Tpc*x*x+ phi_radians);
	       
	    }

	  if(set_const_i_file)
	    I=const_i;
	  else	
	    {    
	      I = (int) (di + 0.5);  // closest integer
	      
	      // Check amplitude value when N=Niq/2
	      if(n==Niq/2)
		{
		  //  if (I != 510 && I != 511)
		  double ftmp;
		  ftmp = sqrt( (di*di)+(dq*dq) );
		  printf("ftmp=%f\n",ftmp);
                  int k = floor(ftmp);
		  if(k   != 510 && k != 511)
		    {
		      printf("build_iq_table_psm3_2w : Error amplitude for n=Niq/2=%d is %d; expect 510 or 511\n",n,k);
		      return -1;
		    }


		}  
	    }

	    i=(~I+1) & I_Q_DATA_MASK; /* 2's complement and mask to 10 bits */
	
	    //i= TwosComp_convert(I,0);
	    //printf("*** I=%d  after conversion i=%d 0x%x\n",I,i,i);

	  if(set_const_q_file)
	    Q=const_q;
	  else
	    Q = (int)(dq+0.5); // closest integer
	   

          if(set_const_i_file || set_const_q_file) 
	    {
	      printf("build_iq_table_psm3_2w: not checking I,Q values at begin/end of file because constant I or Q is set\n");
	    }
	  else
	    {
	      // Add a check to make sure I=Q=0 at beginning and end of file
	      if (n==1 || n==Niq)
		{
		  int k = floor( (di*di)+(dq*dq) );
            
		  if(k   != 0 )
		    {
		      printf("build_iq_table_psm3_2w : Error amplitude for n=%d is %d; expect 0\n",n,k);
		      return -1;
		    }
		}
	    }

	    q=(~Q+1) & I_Q_DATA_MASK;/* 2's complement and mask to 10 bits */
	    //q= TwosComp_convert(Q,0);
	    //printf("*** Q=%d  after conversion q=%d 0x%x\n",Q,q,q);
	  if(my_debug)
	    printf("n=%d I,Q pair = %d,%d ->i,q pair =%d,%d; di=%g  dq=%g \n",
		   n,I,Q,i,q,di,dq);
 	  if(dbgfile)
	    {
	      sprintf(astr,"%d:  %d %d   %d  %d %s",n,I,Q,i,q,"\n"); 
	      fputs(astr,dbgfile);
	    }

	  sprintf(str,"%d %d%s",i,q,"\n"); // IQ file contains 2s compliment data
	  fputs(str,iqfile);
	  
	  n++;  
	}
      while ( n <= Niq ); 
  
      fclose(iqfile);
      if(dbgfile)fclose(dbgfile);
      return i;
    }
  else
    {
      cm_msg(MERROR,"build_iq_table_psm3_2w","Error opening iq file %s  ",str);
      printf("build_iq_table_psm3_2w : Error opening iq file %s \n",str);
      return -1;
    }
}




INT get_phase_corrections(void)
{
  const double pi = 3.14159265358979;
  float qpcr,x;
  INT I,Q;
  char str[80];
  INT status;
  INT channel_index;
  char channel_name[5];
  BOOL flag;
  
  status = SUCCESS; // default

  // No phase correction for f0ch1
  for (channel_index=0; channel_index< MAX_CHANNELS; channel_index++) // cycle through channels 
    {
      sprintf(channel_name, "%s", get_channel(channel_index));
      I=Q=flag=0;

      switch (channel_index)
	{
	case 0:
	  break;  // no phase correction
	case 1:
	  if(ppg.hardware.psm.f0ch2.channel_enabled) // if f0 ch2  is enabled
	    {
	      printf("get_phase_connections: f0ch2 phase correction =%f degrees\n",
		     ppg.hardware.psm.f0ch2.iq_modulation.phase_correction__degrees_);
	      if(ppg.hardware.psm.f0ch2.iq_modulation.phase_correction__degrees_  > 0 ) // phase correction > 0
		{
		  qpcr = ppg.hardware.psm.f0ch2.iq_modulation.phase_correction__degrees_ *pi/180;  // in radians
		  x = (float) (-511*sin(qpcr));
		  I = ceil(x); // nint
		  x = (float) (511*cos(qpcr));
		  Q = ceil(x); // nint
		  ppg.hardware.psm.f0ch2.iq_modulation.i_phase_correction__calc_ = I;
		  ppg.hardware.psm.f0ch2.iq_modulation.q_phase_correction__calc_ = Q;
		  printf("get_phase_connections: f0ch2 phase correction I=%d Q=%d\n",I,Q);
		  flag=1; // set flag
		}
	    }
	case 2:
	  if(ppg.hardware.psm.f0ch3.channel_enabled) // if f0 ch3  is enabled
	    {
	      printf("get_phase_connections: f0ch3 phase correction =%f degrees\n",
		     ppg.hardware.psm.f0ch3.iq_modulation.phase_correction__degrees_);
	      if(ppg.hardware.psm.f0ch3.iq_modulation.phase_correction__degrees_  > 0 ) // phase correction > 0
		{
		  qpcr = ppg.hardware.psm.f0ch3.iq_modulation.phase_correction__degrees_ *pi/180;  // in radians
		  x = (float) (-511*cos(qpcr));
		  I = ceil(x); // nint
		  x = (float) (-511*sin(qpcr));
		  Q = ceil(x); // nint
		  ppg.hardware.psm.f0ch3.iq_modulation.i_phase_correction__calc_ = I;
		  ppg.hardware.psm.f0ch3.iq_modulation.q_phase_correction__calc_ = Q;
		  printf("get_phase_connections: f0ch3 phase correction I=%d Q=%d\n",I,Q);
		  flag=1; // set flag
		}
	    }
	  break;
	case 3:
	  if(ppg.hardware.psm.f0ch4.channel_enabled) // if f0 ch4  is enabled
	    {
	      printf("get_phase_connections: f0ch4 phase correction =%f degrees\n",
		     ppg.hardware.psm.f0ch4.iq_modulation.phase_correction__degrees_);

	      if(ppg.hardware.psm.f0ch4.iq_modulation.phase_correction__degrees_  > 0 ) // phase correction > 0
		{
		  qpcr = ppg.hardware.psm.f0ch4.iq_modulation.phase_correction__degrees_  *pi/180;  // in radians
		  x = (float) (511*sin(qpcr));
		  I = ceil(x); // nint
		  x = (float) (-511*cos(qpcr));
		  Q = ceil(x); // nint
		  ppg.hardware.psm.f0ch4.iq_modulation.i_phase_correction__calc_ = I;
		  ppg.hardware.psm.f0ch4.iq_modulation.q_phase_correction__calc_ = Q;
		  printf("get_phase_connections: f0ch3 phase correction I=%d Q=%d\n",I,Q);

		  flag=1; // set flag
		}
	    }
	  break;

	case 4:
	  if(ppg.hardware.psm.f1.channel_enabled)
	    {
	      if(ppg.hardware.psm.f1.iq_modulation.phase_correction__degrees_  > 0 ) // phase correction > 0
		{
		  // correction?
		}
	    }
	  break;
	}// end switch

      if(flag)
	{ // write the calculated values for the frontend or the i,q file
	  sprintf(str,"%s/iq modulation/i phase correction (calc)",channel_name);
	  printf("writing phase correction to ../%s\n",str);
	  status = db_set_value(hDB, hPSM, str , &I, sizeof(I), 1, TID_INT);
	  if(status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"get_phase_corrections","Error setting ODB Key\"%s\" (%d)",str,status);
	      return status;
	    }
	  sprintf(str,"%s/iq modulation/i phase correction (calc)",channel_name);
	  printf("writing phase correction to ../%s\n",str);
	  status = db_set_value(hDB, hPSM, str , &I, sizeof(I), 1, TID_INT);
	  if(status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"get_phase_corrections","Error setting ODB Key\"%s\" (%d)",str,status);
	      return status;
	    }
	}
      
    } // end loop
  return status;
}
// end of PSMIII specific
#else
// PSM or PSMII

/*------------------------------------------------------------------*/
INT check_psm_quad(char *ppg_mode)
{
  // called from tr_prestart
  INT n,status;
  char my_profile[5];
  BOOL quad;
  //char str[80];
  printf(" check_psm_quad:  success = %d\n",SUCCESS);
  if(check_psm_profile() != SUCCESS) 
    return -1; /* checks at least one profile is enabled */
  

  // Quadrature mode: build the I,Q pairs table
  for (n=0; n< MAX_PROFILES; n++) // cycle through profiles 
    {
      quad=0;
      sprintf(my_profile, get_profile(n));
      switch (n)
	{
	  
	case 0:
	  if(ppg.hardware.psm.one_f.profile_enabled) // if profile is enabled
	    {
              quad = ppg.hardware.psm.one_f.quadrature_modulation_mode;
              break;
	    }
	  
	  
	case 3:
	  if(ppg.hardware.psm.fref.profile_enabled) // if profile is enabled
	    {
              quad = ppg.hardware.psm.fref.quadrature_modulation_mode;
              break;
	    }
	  
	  
	default:
	  printf("check_psm_quad: profile %s  is not enabled\n",my_profile);
	  goto next;
	} // switch

      printf("check_psm_quad: working on profile %s \n",my_profile);
      if(quad)
	{  // quad mode is enabled 
	  
	  printf("check_psm_quad: quad mode is selected for profile \"%s\" (n=%d) in  ppg_mode=%s\n",
		 my_profile,n ,ppg_mode);
#ifdef PSMII
	  printf("check_psm_quad: calling build_iq_table_psm2 for profile  \"%s\" \n",my_profile); 
	  status =  build_iq_table_psm2(ppg_mode, my_profile);
	  if ( status == -1 )
	    {
	      cm_msg(MERROR,"check_psm_quad","Error return from build_iq_table_psm2 for profile  \"%s\" ",
		     my_profile);
	      return status;
	    }
#else
	  printf("check_psm_quad: calling build_iq_table_psm for profile  \"%s\"\n",my_profile); 
	  status =  build_iq_table_psm(ppg_mode, my_profile);
	  if ( status == -1 )
	    {
	      cm_msg(MERROR,"check_psm_quad","Error return from build_iq_table_psm for profile  \"%s\" ",
		     my_profile);
	      return status;
	    }
#endif
	} // end of quad mode for profile n
      else
	printf("check_psm_quad: quad mode is NOT selected for profile %s\n",my_profile);
    next: continue;   
    } // end of for loop on all profiles
  printf("check_psm_quad: returning 1 (success)\n");
  return SUCCESS;  // SUCCESS =1
}



/*------------------------------------------------------------------*/

INT check_psm_profile(void)
{
  INT amplitude_flag;
  printf("***   check_psm_profile: starting\n");
  /* called from check_psm_quad */
  
  amplitude_flag=0;
  /* check that at least one profile is enabled  (only two supported at present)  and it has amplitude > 0 */
 
  if(ppg.hardware.psm.one_f.profile_enabled || ppg.hardware.psm.fref.profile_enabled )
    {
      if(ppg.hardware.psm.one_f.profile_enabled )
	{
          printf("***   check_psm_profile: ppg.hardware.psm.one_f.scale_factor__def_181_max_255_ =%d\n",ppg.hardware.psm.one_f.scale_factor__def_181_max_255_);
	  if(ppg.hardware.psm.one_f.scale_factor__def_181_max_255_ > 0 )
	    amplitude_flag=1; 
	}
      else if  (ppg.hardware.psm.fref.profile_enabled )
	{
	  printf("***   check_psm_profile: ppg.hardware.psm.fref.scale_factor__def_181_max_255_ =%d\n",ppg.hardware.psm.fref.scale_factor__def_181_max_255_);
	  if(ppg.hardware.psm.fref.scale_factor__def_181_max_255_ > 0 )
	    amplitude_flag=1; 
	  }
    }
  else
    {
      printf("check_psm_profile: At least one PSM profile  (i.e. \"one=f\" or \"fref\") must be enabled \n");
      cm_msg(MERROR,"check_psm_profile",
	     "At least one PSM profile  (i.e. \"one=f\" or \"fref\") must be enabled and its scale factor (amplitude) set > 0 ");
      return -1; // FAILURE
    }

  if(amplitude_flag)
    return SUCCESS;

  cm_msg(MERROR,"check_psm_profile",
	     "Scale factor (amplitude) of PSM for enabled profile(s) must be set > 0 for RF ");
  return -1; // FAILURE

}

#ifdef PSMII
//--------------------------------------------------------
int build_iq_table_psm2(char *ppg_mode, char *profile)
//----------------------------------------------------
{
  /* build an iq table for the psmII; called from check_psm_quad 
   */
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
  FILE  *iqfile;
  FILE  *dbgfile;
  char  moduln_mode[16];
  char  str[256];
  char  astr[256];
  BOOL  I_zero; // temp flag
  INT j,i,q;
  
  INT my_debug=FALSE;
  INT N, Ncic,Niq;
  double dNcic, dNtiq, dNiq, dn;
  double dNc, dNtiqtemp;
  
  double d_nu, d_w_req, dw;
  double a,phi,tmp;
  INT I,Q;
  double Tp;
  
  // temp variables
  double x,z;
  double d_const;
  int  actual_bandwidth__hz_;
  
  double d_w_max,d_w_min,d_nu_min,d_nu_max;
  double alpha, A; /* modulation params */
  
  INT mod_func; /* set to 0 for ln_sech, 1 for Hermite */  
  INT Ntiqtemp, Ntiq,  Nc, Ncmx;  
  INT pro_index;

  BOOL set_const_i_file,  set_const_q_file;
  INT const_i, const_q, req_bw;

  cm_msg(MINFO,"build_iq_table_psm2","starting with profile %s  ",profile);
  if(get_profile_index(profile,&pro_index) != SUCCESS)
  {
    printf("build_iq_table_psm2  : illegal profile %s\n",profile);
    cm_msg(MERROR,"build_iq_table_psm2","illegal profile %s  ",profile);
    return -1;
  }
  printf("\n  build_iq_table_psm2 : working on profile \"%s\" (profile index %d) (PSM II) \n",profile,pro_index);
  
  switch (pro_index)
    {
    case 0:
      strncpy( moduln_mode, ppg.hardware.psm.one_f.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.one_f.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.one_f.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.one_f.iq_modulation.const_i__max_plus_or_minus_511_;
      set_const_q_file = ppg.hardware.psm.one_f.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.one_f.iq_modulation.const_q__max_plus_or_minus_511_;
      req_bw =  ppg.hardware.psm.one_f.iq_modulation.requested_bandwidth__hz_;
      break;
    case 3:
      strncpy( moduln_mode, ppg.hardware.psm.fref.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.fref.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.fref.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.fref.iq_modulation.const_i__max_plus_or_minus_511_;
      set_const_q_file = ppg.hardware.psm.fref.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.fref.iq_modulation.const_q__max_plus_or_minus_511_;
      req_bw =  ppg.hardware.psm.fref.iq_modulation.requested_bandwidth__hz_;
      break;
    default:
      printf("build_iq_table_psm2 : Error: profile %d i.e. \"%s\" is not supported\n",pro_index,profile);
      cm_msg(MERROR,"build_iq_table_psm2","Error: profile %d i.e. \"%s\" is not supported ",pro_index,profile);
      return -1;
    }  

  /* Check the modulation mode (ln-sech or Hermite) */   
  sprintf( str,"%s", moduln_mode);
  str[2]='\0'; /* terminate after two characters */
  for  (j=0; j< strlen( str) ; j++) 
    str[j] = tolower (str[j]); /* convert to lower case */ 
  
  
  if (strncmp( str,"ln-sech",2) == 0)
    {
      mod_func=0 ;
      printf("build_iq_table_psm2: detected ln-sech modulation mode\n");
      alpha = ppg.output.psm.amplitude_mod_param__alpha_[pro_index] = 5;
      A     = ppg.output.psm.linewidth_mod_param__a_[pro_index]     = 0.1;
    }
  else if  (strncmp( str,"hermite",2) == 0)
    {
      mod_func = 1;
      printf("build_iq_table_psm2: detected Hermite modulation mode\n");
      alpha =  ppg.output.psm.amplitude_mod_param__alpha_[pro_index] = 2.2;
      A =      ppg.output.psm.linewidth_mod_param__a_[pro_index]     = 0.39714;
    }
  else
    {
      printf("build_iq_table_psm2: Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported \n",
	     moduln_mode);
      cm_msg(MERROR,"build_iq_table_psm2", "Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported ",
	     moduln_mode);
      return -1;
    }

  /* Ncmx  is a hardware param read from PSM by frontend_init (febnmr) and written into odb
     in the output directory, so that rf_config can use it.  We got the record for "output" */
 
  if(debug) printf("Ncmx from output record in odb is =%d\n", 
		   ppg.output.psm.max_cycles_iq_pairs__ncmx_); // note one value for all profiles
  Ncmx =  ppg.output.psm.max_cycles_iq_pairs__ncmx_; // note one value for all profiles
  
  if(Ncmx < 1 || Ncmx > 128)
    {
      cm_msg(MERROR,"build_iq_table_psm2","Illegal value of param Ncmx read from odb (%d), 0<Ncmx<128  ",Ncmx);
      printf("build_iq_table_psm2: Illegal value of param Ncmx read from odb (%d), 0<Ncmx<128 \n ",Ncmx);
#ifdef PSMII
      if(Ncmx != 128)
	{  /* PSMII gives 32 when it should give 128 */
	  printf("Ncmx is supposed to be set to 128  by the frontend reading value from PSM (frontend_init)\n");
	  printf("Default value of 128 will be used\n");
	  cm_msg(MINFO,"build_iq_table_psm2","Ncmx has not been filled by the frontend...using default Ncmx=128  ");
	  ppg.output.psm.max_cycles_iq_pairs__ncmx_ =  Ncmx = 128; // note one value for all profiles
	}
#else /*  PSMIIB  */
      if(Ncmx == 0)
	{  /* PSMIIB has this problem fixed */
	  printf("Ncmx is supposed to be filled by the frontend reading value from PSM (frontend_init)\n");
	  printf("Default value of 128 will be used\n");
	  cm_msg(MINFO,"build_iq_table_psm2","Ncmx has not been filled by the frontend...using default Ncmx=128 (PSMIIB)");
	  ppg.output.psm.max_cycles_iq_pairs__ncmx_ =  Ncmx = 128; // note one value for all profiles
	}
#endif /* PSMII  */
      else
	{
	  cm_msg(MERROR,"build_iq_table_psm2","Unexpected value of param Ncmx read from ODB ../output/psm for PSMII/PSMIIB (%d). Expect 0 or 128",Ncmx);
	  printf("build_iq_table_psm2: Unexpected value of param Ncmx read from ODB ../output/psm for PSMII/PSMIIB (%d). Expect 0 or 128\n",Ncmx);
	  return -1;
	}
    }
  // if(my_debug)
    printf("Input params: A=%f ; alpha = %f; Ncmx = %d\n",A,alpha,Ncmx);
 
  /* formulae for PSMII :  */ 
  d_w_max = ( pow(10,8)* alpha ) / (A * 512.0 * 5);  // rad/s
  /* changed from 128 to 2048 27-4-06 */
  d_w_min =  ( pow(10,8)* alpha ) / (A * 2048.0 * 63.0 * 128.0); // rad/s
  d_nu_min = d_w_min/(2*M_PI); // Hz
  d_nu_max = d_w_max/(2*M_PI); // Hz
								   
  //  if (my_debug)
    {
      printf(" d_w_min  = %.3f  d_w_max  = %.3g  rad/s\n",d_w_min,d_w_max);
      printf(" d_nu_min = %.3f  d_nu_max = %.3f   Hz\n",d_nu_min,d_nu_max);
    }

 


  /* check d_nu is in range */
  if( (d_nu < d_nu_min)  || (d_nu > d_nu_max) )
    {
      cm_msg(MERROR, "build_iq_table_psm2"," requested bandwidth (%.0fHz) is out of range ",
	     d_nu);
      cm_msg(MINFO, "build_iq_table_psm2","bandwidth must be between %f and %f Hz",d_nu_min,d_nu_max);
      printf("build_iq_table_psm2 : bandwidth must be between %f and %f Hz\n",d_nu_min,d_nu_max);
      printf("build_iq_table_psm2 : requested bandwidth (%.0fHz) is out of range\n",d_nu);
      return -1;
    }

  d_w_req= 2 * M_PI * d_nu; 

  printf("build_iq_table_psm2 : requested bandwidth d_w_req = %.2f rad/s; d_nu = %.0f Hz or %1e Hz; \n",
	 d_w_req,d_nu,d_nu);

  d_const = ( alpha * pow(10,8)) / A ; // this is 5 * 10**9 for A=0.1 and alpha=5
/* if(my_debug) */
    printf("  d_const = %.2e; expected value = 5*10e9 \n",d_const);

  Ntiqtemp = (INT) ( d_const/d_w_req );   // nearest smallest integer

  if( (Ntiqtemp < 2560)  || ( Ntiqtemp > 16515071 ))
    {
      cm_msg(MERROR,"build_iq_table_psm2","preliminary no. of IQ points (Ntiqtemp=%d) is out of range  ",
	     Ntiqtemp);
      cm_msg(MINFO,"build_iq_table_psm2","Ntiqtemp must be between 2560 and 16515071");
      printf("build_iq_table_psm2 : preliminary no. IQ points (Ntiqtemp=%d) is out of range\n", Ntiqtemp);
      printf("                            Ntiqtemp must be between 2560 and 16515071\n");
      return -1;
    }
   
  // select Nc, Ncic from the table of Ntiqtemp values
  Nc=Ncic=0;
  status = get_IQ_params(Ntiqtemp, &Nc, &Ncic); /* for PSMII */
  if(status== -1)
    {
      cm_msg(MERROR,"build_iq_table_psm2","Error status returned from get_IQ_params (called with Ntiqtemp=%d)",Ntiqtemp);
      printf("build_iq_table_psm2: Error status returned from get_IQ_params (called with Ntiqtemp=%d)\n",Ntiqtemp);
      return -1;
    }


  printf("build_iq_table_psm2 : Ntiqtemp=%d, Nc =%d, Ncic=%d\n",Ntiqtemp,Nc,Ncic);
  if(Nc > Ncmx)
    {
      cm_msg(MERROR,"build_iq_table_psm2","Calculated Nc=%d for Ntiqtemp=%d is out of range; must be <= Ncmx=%d ",
	     Ntiqtemp,Nc,Ncmx);
      printf("build_iq_table_psm2 : Calculated Nc=%d for Ntiqtemp=%d is out of range; must be <= Ncmx=%d \n",
	     Ntiqtemp,Nc,Ncmx);
      return -1;
    }

  ppg.output.psm.cic_interpoln_rate__ncic_[pro_index] = Ncic;
  ppg.output.psm.num_cycles_iq_pairs__nc_[pro_index] = Nc;


  dNcic = (double) Ncic;
  dNc   = (double) Nc;
  dNtiqtemp = (double) Ntiqtemp;


  /* calculate Niq */
  tmp = dNtiqtemp/(dNcic * dNc);
  Niq = (int) (1+ tmp);  // closest larger integer
  Ntiq= Niq * Nc * Ncic;

  if(my_debug)
    printf("build_iq_table_psm2 : dNcic = %.1f ; dNc = %.1f;  dNtiqtemp = %.1f; tmp=%.1f  \n",
	   dNcic,dNc,dNtiqtemp,tmp);

  printf("build_iq_table_psm2 : Calculated Niq = %d ; Ntiq = %d \n",Niq,Ntiq);

  if(Niq < 512 || Niq > 2048)
    {
      cm_msg(MERROR,"build_iq_table_psm2","Consistency check failure; Niq=%d, must be between 512 and 2048  ",Niq);
      printf("build_iq_table_psm2 : Consistency check failure; Niq=%d, must be between 512 and 2048\n",Niq);
      return -1;
    }
  if(Ntiq >= 129024 * Ncmx)
    {
      cm_msg(MERROR,"build_iq_table_psm2","Consistency check failure; Ntiq=%d, must be less than 129024*Ncmx = %d ",
	     Ntiq,129024*Ncmx);
      printf("build_iq_table_psm2 : Consistency check failure; Ntiq=%d, must be less than 129024*Ncmx = %d\n",
	     Ntiq,129024*Ncmx);

      return -1;
    }


  ppg.output.psm.num_iq_pairs__niq_[pro_index] = Niq;
  dNtiq = (double) Ntiq;
  dNiq = (double) Niq;

  /* d_const = 10**10   */
  dw = d_const /dNtiq  ; /* rad/sec */
  ppg.output.psm.bandwidth__rad_per_sec_[pro_index] = dw;

  x=dw/(2* M_PI);

  /* no longer output "actual bandwidth" as new calculation
     delivers the requested bandwidth */
  actual_bandwidth__hz_ = (int)(x +0.5); // as a check
  
  Tp = 2*  pow(10,-8) * dNtiq; // pulse width (sec)
  printf("Tp = %.3e sec\n",Tp);
  ppg.output.psm.pulse_width__msec_[pro_index] =  Tp * pow(10,3); // pulse width (ms)
  
  printf("\n **  IQ Output parameters: *** \n");
  printf("No. IQ pairs Niq= %d;  Ntiq= %d;    Ncic=%d\n",Niq,Ntiq,Ncic);
  printf("output bandwidth dw=%.2f rad/s or %d Hz;   pulse width Tp=%.3e sec or %.3f ms \n\n",
	 dw, actual_bandwidth__hz_, Tp, ppg.output.psm.pulse_width__msec_[pro_index]);



  N=1;
  dn=1.0;

  printf("iq filename will be : %s%s_iq_%s.psm\n",ppg.input.cfg_path,ppg_mode,profile);
  sprintf(str,"%s%s_iq_%s.psm",ppg.input.cfg_path,ppg_mode,profile); // IQ pair file
  sprintf(astr,"%s%s_iq_%s.dbg",ppg.input.cfg_path,ppg_mode,profile); // debug file

  if(my_debug)printf("build_iq_table_psm2 : about to open IQ table DEBUG file %s\n",str);

 

  /* If setting constant values, check they  are reasonable */
  I_zero=FALSE;
  if(set_const_i_file)
    {
      if ( const_i == 0)
	{
	  I_zero=TRUE;
	  if(mod_func ==1) // Hermite: all Q values are zero
	    {
	      cm_msg(MERROR,"build_iq_table_psm2 ",
		     "Modulating with constant I=0 will result in no output for Hermite since Q=0 also");
	      printf("build_iq_table_psm2 : Constant I=0 not allowed for Hermite since Q=0 (no output)\n");
	      return -1;
	    }
	}
      else
	printf("All i values will be set to %d\n",
	       const_i);
      
    }
  
  if(set_const_q_file)
    {    
      if(mod_func == 1  )
	{  // Hermite; all Q values are zero
	  cm_msg(MINFO,"build_iq_table_psm2 ",
		 "Modulating with constant Q=%d has no effect since Q=0 always for Hermite ",
		 const_q);
	}
      else
	{ // ln-sech; check for I=Q=0
	  if ( const_q ==0 )
	    {
	      if(I_zero)
		{
		  cm_msg(MERROR,"build_iq_table_psm2 ",
			 "Modulating with constant I,Q pair=(0,0) will result in no output");
		  printf("build_iq_table_psm2: Constant I=0 and Q=0 are not allowed (no output)\n");
		  return -1;
		}
	    }
	}
      printf("All q values will be set to %d\n",
	     const_q);
      
    }
  
  dbgfile = fopen(astr,"w");
  if(dbgfile)
    {
      printf("successfully opened debug file: %s\n",astr);
      sprintf(astr,"Req bandwidth(Hz)=%d; output bandwidth(Hz) =%d\n",
	      req_bw,
	      actual_bandwidth__hz_);
      fputs(astr,dbgfile);

      sprintf(astr,"Num IQ pairs=%d; pulsewidth Tp =%f ms ; Ncic=%d;\n",
	      Niq,  ppg.output.psm.pulse_width__msec_[pro_index], Ncic);
      fputs(astr,dbgfile);

      sprintf(astr,"Index: I      Q              i    q (i,q=masked 2's comp) %s","\n"); 
      fputs(astr,dbgfile);
    }
  else
    printf("could not open debug file %s\n",astr);
 

  printf("build_iq_table_psm2 : about to open IQ table file %s\n",str);
  printf("       these will be the values programmed into \"%s\" profile\n",profile);

  iqfile = fopen(str,"w");
  if (iqfile)    
    {
      do
	{ 
	  double di,dq;
	  /* ln-sech */
	  if(mod_func == 0)
	    {
	      // sech = 1/cosh
	      z=(dw*Tp/10.0)  * ((dn/dNiq) - 0.5);
	      a = 1.0/cosh (z);
	      phi = 5.0 * log(a);
	      di= 511*a*cos(phi) ;    
	      dq= 511*a*sin(phi) ;
	    }
	  else
	    {  /* mod_func = 1, Hermite */
	      z = A * dw * Tp *  ((dn/dNiq) - 0.5) ;
	      a = pow(z,2);
	      di = 511 * (1.0 -0.957 * a) * exp(-a);
	      dq = 0;
	    }

	  if(set_const_i_file)
	    I=const_i;
	  else	
	    {    
	      I = (int) (di + 0.5);  // closest integer

	      // Check I value when N=Niq/2
	      if(N==Niq/2)
		{
		  if (I != 510 && I != 511)
		    {
		      printf("build_iq_table_psm2 : Error I=%d for N=Niq/2=%d; expect 510 or 511\n",I,N);
		      return -1;
		    }
		}
	    }

	  i=(~I+1) & I_Q_DATA_MASK; /* 2's complement and mask to 10 bits */
	    
	  if(set_const_q_file)
	    Q=const_q;
	  else
	    Q = (int)(dq+0.5); // closest integer
	   

	  q=(~Q+1) & I_Q_DATA_MASK;/* 2's complement and mask to 10 bits */


	  if(my_debug)
	    printf("n=%d I,Q pair = %d,%d ->i,q pair =%d,%d; di=%g  dq=%g  a=%g  phi=%g \n",
		   (int)dn,I,Q,i,q,di,dq,a,phi);
 	  if(dbgfile)
	    {
	      sprintf(astr,"%d:  %d %d   %d  %d %s",N,I,Q,i,q,"\n"); 
	      fputs(astr,dbgfile);
	    }

	  sprintf(str,"%d %d%s",i,q,"\n"); // IQ file contains 2s compliment data
	  fputs(str,iqfile);
	  
	  N++;
	  dn=(float)N;
	  
	}
      while ( N <= Niq ); 
  
      fclose(iqfile);
      if(dbgfile)fclose(dbgfile);
      return i;
    }
  else
    {
      cm_msg(MERROR,"build_iq_table_psm2","Error opening iq file %s  ",str);
      printf("build_iq_table_psm2 : Error opening iq file %s \n",str);
      return -1;
    }
}
/*-----------------------------------------------------------*/
int get_IQ_params(INT Ntiqtemp, INT *pNc, INT *pNcic)
/*----------------------------------------------------------*/
{
  /* version for PSMII module */

  const int ival=4096;
  const int jval=129024;

  *pNc=*pNcic=0;

  if(Ntiqtemp < 2560)
    {
      printf("get_IQ_params: preliminary no. of IQ points (Ntiqtemp=%d) is out of range (min=2560)\n",
	     Ntiqtemp);
      cm_msg(MERROR,"get_IQ_params","preliminary no. of IQ points (Ntiqtemp=%d) is out of range (min=2560)",
	     Ntiqtemp);
      return -1;
    }
  
  if (Ntiqtemp < ival<<1)
    *pNcic=5;
  else if (Ntiqtemp < ival<<2)
    *pNcic=8;
  else if (Ntiqtemp < ival<<3)
    *pNcic=16;
  else if (Ntiqtemp < ival <<4)
    *pNcic=32;
  else
    *pNcic=63;
  
  

  if (Ntiqtemp < jval )
    *pNc = 1 ;
  else if (Ntiqtemp < jval <<1 )
    *pNc = 2 ;
  else if (Ntiqtemp < jval <<2 )
    *pNc = 4 ;
  else if (Ntiqtemp < jval <<3 )
    *pNc = 8 ;
  else if (Ntiqtemp < jval <<4 )
    *pNc = 16 ;
  else if (Ntiqtemp < jval <<5 )
    *pNc = 32 ;
  else if (Ntiqtemp < jval <<6 )
    *pNc = 64 ;
  else if (Ntiqtemp < jval <<7 )
    *pNc = 128 ;

  else
    {
      printf("get_IQ_params: preliminary no. of IQ points (Ntiqtemp=%d) is out of range (max=16515071)\n",
	     Ntiqtemp);
      cm_msg(MERROR,"get_IQ_params","preliminary no. of IQ points (Ntiqtemp=%d) is out of range (max=16515071)",
	     Ntiqtemp);
      return -1;
    }
  
  printf("Ntiqtemp=%7.7d    Nc=%2.2d     Ncic=%2.2d\n",Ntiqtemp,*pNc,*pNcic);
  return SUCCESS; // SUCCESS = 1
}
       /* end of PSMII specific */
#else  /* standard PSM */

//--------------------------------------------------------
int build_iq_table_psm(char *ppg_mode, char *profile )
//----------------------------------------------------
{
  /* build an iq table for the psm; called from check_psm_quad 
   */
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
  FILE  *iqfile;
  FILE  *dbgfile;
  char  str[256];
  char moduln_mode[16];
  char  astr[256];
  BOOL I_zero;

  INT status, size, j,i,q;

  INT my_debug=FALSE;
  INT N, Ncic,Niq;
  double dNcic, dNtiq, dNiq, dn;
  double dNc, dNtiqtemp;

  double d_nu, d_w_req, dw;
  double a,phi,tmp;
  INT I,Q;
  double Tp;

  // temp variables
  double x,z;
  double d_const;
  int  actual_bandwidth__hz_;

  double d_w_max,d_w_min,d_nu_min,d_nu_max;
  double alpha, A; /* modulation params */
  //double d_Ntiqtemp;
  INT mod_func; /* set to 0 for ln_sech, 1 for Hermite */

  INT Ntiqtemp, Ntiq, Ncmx, Nc;
  int pro_index;

  BOOL set_const_i_file,  set_const_q_file;
  INT const_i, const_q, req_bw;


#ifdef PSMII
  cm_msg(MERROR,"build_iq_table_psm  :","Coding error. This routine is for PSM, not PSMII" );
  return -1;
#endif
#ifdef PSMIIB
  cm_msg(MERROR,"build_iq_table_psm  :","Coding error. This routine is for PSM, not PSMIIB" );
  return -1;
#endif

  cm_msg(MINFO,"build_iq_table_psm","starting with profile %s  ",profile);
  if(get_profile_index(profile,&pro_index) != SUCCESS)
    {
      printf("build_iq_table_psm  : illegal profile %s \n",profile);
      cm_msg(MERROR,"build_iq_table_psm  :","illegal profile %s",profile);
      return -1;
    }
  
  printf("\n  build_iq_table_psm: working on profile \"%s\" (profile index %d) \n",profile,pro_index);
  
  switch (pro_index)
    {
    case 0:
      strncpy( moduln_mode, ppg.hardware.psm.one_f.iq_modulation.moduln_mode__ln_sech_hermite_ ,15);
      d_nu = (double)  ppg.hardware.psm.one_f.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.one_f.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.one_f.iq_modulation.const_i__max_plus_or_minus_511_;
      set_const_q_file = ppg.hardware.psm.one_f.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.one_f.iq_modulation.const_q__max_plus_or_minus_511_;
      req_bw =  ppg.hardware.psm.one_f.iq_modulation.requested_bandwidth__hz_;
      break;
    case 3:
      strncpy( moduln_mode, ppg.hardware.psm.fref.iq_modulation.moduln_mode__ln_sech_hermite_, 15);
      d_nu = (double)  ppg.hardware.psm.fref.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.fref.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.fref.iq_modulation.const_i__max_plus_or_minus_511_;
      set_const_q_file = ppg.hardware.psm.fref.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.fref.iq_modulation.const_q__max_plus_or_minus_511_;
      req_bw =  ppg.hardware.psm.fref.iq_modulation.requested_bandwidth__hz_;
      break;
    default:
      cm_msg(MERROR,"build_iq_table_psm","Error: profile %d i.e. \"%s\" is not supported",pro_index,profile);
      printf("build_iq_table_psm: Error profile %d i.e. \"%s\" is not supported\n",pro_index,profile);
      return -1;
    }  
  
  /* Check the modulation mode (ln-sech or Hermite) */
  sprintf( str,"%s", moduln_mode);
  str[2]='\0'; /* terminate after two characters */
  for  (j=0; j< strlen(str) ; j++) 
    str[j] = tolower (str[j]); /* convert to lower case */ 
  

  if (strncmp(str,"ln-sech",2) == 0)
    {
      mod_func=0 ;
      printf("build_iq_table_psm: detected ln-sech modulation mode\n");
      alpha = ppg.output.psm.amplitude_mod_param__alpha_[pro_index] = 5;
      A     = ppg.output.psm.linewidth_mod_param__a_[pro_index]     = 0.1;
    }
  else if (strncmp(str,"hermite",2) == 0)
    {
      mod_func = 1;
      printf("build_iq_table_psm: detected Hermite modulation mode\n");
      alpha =  ppg.output.psm.amplitude_mod_param__alpha_[pro_index] = 2.2;
      A =      ppg.output.psm.linewidth_mod_param__a_[pro_index]     = 0.39714;
    }
  else
    {
      printf("build_iq_table_psm: Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported\n",
	     moduln_mode);
      cm_msg(MERROR, "build_iq_table_psm","Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported",
	     moduln_mode);
      return -1;
    }
  
  
  size = sizeof(Ncmx);
  
  
  /* Ncmx  is a hardware param read from PSM by frontend_init (febnmr) and written into odb
     in the output directory, so that rf_config can use it.  We got the record for "output" */
 
  if(debug) printf("Ncmx from output record in odb is =%d\n", 
		   ppg.output.psm.max_cycles_iq_pairs__ncmx_); // Ncmx note one value for all profiles
  Ncmx =  ppg.output.psm.max_cycles_iq_pairs__ncmx_; // note one value for all profiles
  
  if(Ncmx < 1 || Ncmx > 32)
    {
      printf("build_iq_table_psm: Illegal value of param Ncmx read from odb (%d), (0<Ncmx<32) \n",Ncmx);
      cm_msg(MERROR,"build_iq_table_psm","Illegal value of param Ncmx read from odb (%d), (0<Ncmx<32) ",Ncmx);

      if(Ncmx == 0)
	{
	  printf("Ncmx is supposed to be filled by the frontend reading value from PSM (bnmr_init)\n");
	  printf("Default value of 32 will be used\n");
	  cm_msg(MINFO,"build_iq_table_psm","Ncmx has not been filled by the frontend...using default Ncmx=32");
	  ppg.output.psm.max_cycles_iq_pairs__ncmx_ =  Ncmx = 32;  // note one value for all profiles
	}
      else
	return -1;  // error
    }
  if(my_debug)
    printf("Input params: A=%f ; alpha = %f; Ncmx = %d\n",A,alpha,Ncmx);
  
  d_w_max = ( pow(10,7)* alpha ) / (A * 512.0);  // rad/s
  if(my_debug)
    { // for checking calc.
      x = d_w_max/128;
      printf(" d_w_min = %f / Ncmx\n", x);
    }
  d_w_min = d_w_max /(128 * Ncmx); // rad/s
  d_nu_min = d_w_min/(2*M_PI); // Hz
  d_nu_max = d_w_max/(2*M_PI); // Hz
								   
  //  if (my_debug)
    {
      printf(" d_w_min  = %.3f  d_w_max  = %.3g  rad/s\n",d_w_min,d_w_max);
      printf(" d_nu_min = %.3f  d_nu_max = %.3f   Hz\n",d_nu_min,d_nu_max);
    }


  /* check d_nu is in range */
  if( (d_nu < d_nu_min)  || (d_nu > d_nu_max) )
    {
      printf("build_iq_table_psm: requested bandwidth (%fHz) is out of range\n",
	     d_nu);
      printf("        bandwidth must be between %f and %f Hz\n",d_nu_min,d_nu_max);
      cm_msg(MERROR,"build_iq_table_psm"," requested bandwidth (%fHz) is out of range",d_nu);
      cm_msg(MINFO,"build_iq_table_psm","bandwidth must be between %f and %f Hz",d_nu_min,d_nu_max);
      return -1;
    }

  d_w_req= 2 * M_PI * d_nu; 

  printf("build_iq_table_psm: requested bandwidth =%f Hz \n",
	 d_nu);
  printf("build_iq_table_psm: requested bandwidth d_nu=%1e Hz;   d_w_req = %.2f rad/s\n",d_nu, d_w_req);


  d_const = ( alpha * 2 * pow(10,7)) / A ; // this is 10**9 for A=0.1 and alpha=5
  if(my_debug)
    printf("d_const = %.2e; expected value = 10**9 \n",d_const);

  Ntiqtemp = (INT) ( d_const/d_w_req );   // nearest smallest integer

  if( (Ntiqtemp < 1024)  || ( Ntiqtemp >= 129024 * Ncmx ))
    {
      printf("build_iq_table_psm: preliminary no. of IQ points (Ntiqtemp=%d) is out of range\n",
	     Ntiqtemp);
      printf("      Ntiqtemp must be between 1024 and %d \n",( 129024 * Ncmx));
      return -1;
    }
   
  // select Nc, Ncic from the table of Ntiqtemp values
  Nc=Ncic=0;
  status = get_IQ_params(Ntiqtemp, &Nc, &Ncic);
  if(status== -1)
    return -1;


  printf("Build_iq_table_psm: Ntiqtemp=%d, Nc =%d, Ncic=%d\n",Ntiqtemp,Nc,Ncic);
  if(Nc > Ncmx)
    {
      printf("build_iq_table_psm: Calculated Nc=%d for Ntiqtemp=%d is out of range; must be <= Ncmx=%d\n",
	     Ntiqtemp,Nc,Ncmx);
      cm_msg(MERROR,"build_iq_table_psm","Calculated Nc=%d for Ntiqtemp=%d is out of range; must be <= Ncmx=%d",
	     Ntiqtemp,Nc,Ncmx);
      return -1;
    }

  ppg.output.psm.cic_interpoln_rate__ncic_[pro_index] = Ncic;
  ppg.output.psm.num_cycles_iq_pairs__nc_[pro_index] = Nc;


  dNcic = (double) Ncic;
  dNc   = (double) Nc;
  dNtiqtemp = (double) Ntiqtemp;


  /* calculate Niq */
  tmp = dNtiqtemp/(dNcic * dNc);
  Niq = (int) (1+ tmp);  // closest larger integer
  Ntiq= Niq * Nc * Ncic;

  if(my_debug)
    printf("build_iq_table_psm: dNcic = %.1f ; dNc = %.1f;  dNtiqtemp = %.1f; tmp=%.1f  \n",
	   dNcic,dNc,dNtiqtemp,tmp);

  printf("build_iq_table_psm: Calculated Niq = %d ; Ntiq = %d \n",Niq,Ntiq);

  if(Niq < 512 || Niq > 2048)
    {
      printf("build_iq_table_psm: Consistency check failure; Niq=%d, must be between 512 and 2048\n",Niq);
      cm_msg(MERROR,"build_iq_table_psm","Consistency check failure; Niq=%d, must be between 512 and 2048",Niq);
      return -1;
    }
  if(Ntiq >= 129024 * Ncmx)
    {
      printf("build_iq_table_psm: Consistency check failure; Ntiq=%d, must be less than 129024*Ncmx = %d\n",
	     Ntiq,129024*Ncmx);
      cm_msg(MERROR,"build_iq_table_psm","Consistency check failure; Ntiq=%d, must be less than 129024*Ncmx = %d",
	     Ntiq,129024*Ncmx);

      return -1;
    }


  ppg.output.psm.num_iq_pairs__niq_[pro_index] = Niq;
  dNtiq = (double) Ntiq;
  dNiq = (double) Niq;

  /* d_const = 10**9   */
  dw = d_const /dNtiq  ; /* rad/sec */
  ppg.output.psm.bandwidth__rad_per_sec_[pro_index] = dw;

  x=dw/(2* M_PI);

  /* no longer output "actual bandwidth" as new calculation
     delivers the requested bandwidth */
  actual_bandwidth__hz_ = (int)(x +0.5); // as a check
  
  Tp =  pow(10,-7) * dNtiq; // pulse width (sec)
  printf("Tp = %.3e sec\n",Tp);
  ppg.output.psm.pulse_width__msec_[pro_index] =  Tp * pow(10,3); // pulse width (ms)
  
  printf("\n **  IQ Output parameters: *** \n");
  printf("No. IQ pairs Niq= %d;  Ntiq= %d;    Ncic=%d\n",Niq,Ntiq,Ncic);
  printf("output bandwidth dw=%.2f rad/s or %d Hz;   pulse width Tp=%.3e sec or %.3f ms \n\n",
	 dw, actual_bandwidth__hz_, Tp, ppg.output.psm.pulse_width__msec_[pro_index]);



  N=1;
  dn=1.0;
  sprintf(str,"%s%s_iq_%s.psm",ppg.input.cfg_path,ppg_mode,profile); // IQ pair file
  sprintf(astr,"%s%s_iq_%s.dbg",ppg.input.cfg_path,ppg_mode,profile); // debug file


  /* If setting constant values, check they  are reasonable 
     For standard PSM,  
     const_i and
     const_q are both 0.

*/
  I_zero=FALSE;
  if(set_const_i_file)
    {
      if ( const_i ==0)
	{
	  I_zero=TRUE;
	  if(mod_func ==1) // Hermite: all Q values are zero
	    {
	         cm_msg(MERROR,"Build_iq_table_psm",
		     "Modulating with constant I=0 will result in no output for Hermite since Q=0 also");
	      printf("Build_iq_table_psm: Constant I=0 not allowed for Hermite since Q=0 (no output)\n");
	      return -1;
	    }
	}
      else
	printf("All i values will be set to %d\n",
	       const_i);
      
    }
  if(set_const_q_file)
    {    
      if(mod_func == 1  )
	{  // Hermite; all Q values are zero
	    cm_msg(MINFO,"build_iq_table_psm",
	  	 "Modulating with constant Q=%d has no effect since Q=0 always for Hermite ",
	  const_q);
	  printf("Build_iq_table_psm: no effect from modulating with constant Q=%d  since Q=0 always for Hermite\n", const_q);
		 
	}
      else
	{ // ln-sech; check for I=Q=0
	  if ( const_q ==0 )
	    {
	      if(I_zero)
		{
		  cm_msg(MERROR,"Build_iq_table_psm",
			 "Modulating with constant I,Q pair=(0,0) will result in no output");
		  printf("Build_iq_table_psm: Constant I=0 and Q=0 are not allowed (no output)\n");
		  return -1;
		}
	    }
	}
      printf("All q values will be set to %d\n", const_q);
      
    }
  

  if(my_debug)printf("build_iq_table_psm: about to open IQ table DEBUG file %s\n",str);

  if (( dbgfile = fopen(astr,"w")) != NULL)
    {
      printf("successfully opened debug file: %s\n",astr);
      sprintf(astr,"Req bandwidth(Hz)=%d; output bandwidth(Hz) =%d\n",
	      req_bw,
	      actual_bandwidth__hz_);
      fputs(astr,dbgfile);

      sprintf(astr,"Num IQ pairs=%d; pulsewidth Tp =%f ms ; Ncic=%d;\n",
	      Niq,  ppg.output.psm.pulse_width__msec_[pro_index], Ncic);
      fputs(astr,dbgfile);

      sprintf(astr,"Index: I      Q              i    q (i,q=masked 2's comp) %s","\n"); 
      fputs(astr,dbgfile);
    }
  else
    printf("could not open debug file %s\n",astr);

  printf("build_iq_table_psm: about to open IQ table file %s\n",str);
  if ((iqfile = fopen(str,"w"))  != NULL)  
    {
      do
	{ 
	  double di,dq;
	  /* ln-sech */
	  if(mod_func == 0)
	    {
	      // sech = 1/cosh
	      z=(dw*Tp/10.0)  * ((dn/dNiq) - 0.5);
	      a = 1.0/cosh (z);
	      phi = 5.0 * log(a);
	      di= 511*a*cos(phi) ;    
	      dq= 511*a*sin(phi) ;
	    }
	  else
	    {  /* mod_func = 1, Hermite */
	      z = A * dw * Tp *  ((dn/dNiq) - 0.5) ;
	      a = pow(z,2);
	      di = 511 * (1.0 -0.957 * a) * exp(-a);
	      dq = 0;
	    }

	  if(set_const_i_file)
	    I=const_i;
	  else
	    {
	      I = (int) (di + 0.5);  // closest integer
 
	      // Check I value when N=Niq/2
	      if(N==Niq/2)
		{
		  if (I != 510 && I != 511)
		    {
		      printf("build_iq_table_psm: Error I=%d for N=Niq/2=%d; expect 510 or 511\n",I,N);
		      cm_msg(MERROR,"build_iq_table_psm","Error I=%d for N=Niq/2=%d; expect 510 or 511",I,N);
		      return -1;
		    }
		}
	    }

	  i=(~I+1) & I_Q_DATA_MASK; /* 2's complement and mask to 10 bits */
	    



	  if(set_const_q_file)
	    Q=const_q;
	  else
	    Q = (int)(dq+0.5); // closest integer

	  q=(~Q+1) & I_Q_DATA_MASK;/* 2's complement and mask to 10 bits */

	  if(my_debug)
	    printf("n=%d I,Q pair = %d,%d ->i,q pair =%d,%d; di=%g  dq=%g  a=%g  phi=%g \n",
		   (int)dn,I,Q,i,q,di,dq,a,phi);
 	  if(dbgfile)
	    {
	      sprintf(astr,"%d:  %d %d   %d  %d %s",N,I,Q,i,q,"\n"); 
	      fputs(astr,dbgfile);
	    }

	  sprintf(str,"%d %d%s",i,q,"\n"); // IQ file contains 2s compliment data
	  fputs(str,iqfile);
	  
	  N++;
	  dn=(float)N;
	  
	}
      while ( N <= Niq ); 
  
      fclose(iqfile);
      if(dbgfile)fclose(dbgfile);
      return i;
    }
  else
    {
      cm_msg(MERROR,"Build_iq_table_psm","Error opening iq file %s ",str);
      printf("Build_iq_table_psm: Error opening iq file %s ",str);
      return -1;
    }
}


/*-----------------------------------------------------------*/
int get_IQ_params(INT Ntiqtemp, INT *pNc, INT *pNcic)
/*----------------------------------------------------------*/
{  // PSM version

  const int ival=4096;
  const int jval=129024;

  *pNc=*pNcic=0;

  if(Ntiqtemp < 1024)
    {
      printf("get_IQ_params: preliminary no. of IQ points (Ntiqtemp=%d) is out of range (min=1024)\n",
	     Ntiqtemp);
      return -1;
    }

  if (Ntiqtemp < ival)
    *pNcic=2;
  else if (Ntiqtemp < ival<<1)
    *pNcic=4;
  else if (Ntiqtemp < ival<<2)
    *pNcic=8;
  else if (Ntiqtemp < ival<<3)
    *pNcic=16;
  else if (Ntiqtemp < ival <<4)
    *pNcic=32;
  else
    *pNcic=63;
  
  

  if (Ntiqtemp < jval )
    *pNc = 1 ;
  else if (Ntiqtemp < jval <<1 )
    *pNc = 2 ;
  else if (Ntiqtemp < jval <<2 )
    *pNc = 4 ;
  else if (Ntiqtemp < jval <<3 )
    *pNc = 8 ;
  else if (Ntiqtemp < jval <<4 )
    *pNc = 16 ;
  else if (Ntiqtemp < jval <<5 )
    *pNc = 32 ;

  else
    {
      printf("get_IQ_params: preliminary no. of IQ points (Ntiqtemp=%d) is out of range (max=4128767)\n",
	     Ntiqtemp);
      return -1;
    }
  
  //  printf("Ntiqtemp=%7.7d    Nc=%2.2d     Ncic=%2.2d\n",Ntiqtemp,Nc,Ncic);
  return SUCCESS; //1=SUCCESS
}
#endif /* PSM module */
/*------------------------------------------------------------------*/
char *get_profile(INT n)
{
  char *profile_name[]={"1f","3f","5f","fREF","unknown"};
 
  return (n<0 || n>MAX_PROFILES) ? profile_name[4] : profile_name[n];
}

//+++++++++++++++++++++++++++++++++++++
INT check_psm_gate(void)
{
  INT gate_control;
  INT n;
  BOOL enabled;
  char my_profile[15];

    
  for (n=0; n<MAX_PROFILES; n++)
    {
      enabled=0;
      
      switch (n)
	{
	case 0:
	  if(ppg.hardware.psm.one_f.profile_enabled) // if profile is enabled
	    {
	      sprintf(my_profile, get_profile(n));
	      enabled = 1;
              gate_control = ppg.hardware.psm.one_f.gate_control;
	    }
	  break;
	  
	case 3:
	  if(ppg.hardware.psm.fref.profile_enabled) // if profile is enabled
	    {
	      enabled = 1;
	      sprintf(my_profile, get_profile(n));
	      gate_control = ppg.hardware.psm.fref.gate_control;
	    }
	  break;
	  
	  //default:
	  // printf("check_psm_gate: profile %d i.e. \"%s\" is not supported\n",n);
	} 
      
    

      if(enabled)
	{	  
	  gate_control = gate_control & 3;
	  if(gate_control != 1)
	    {
	      cm_msg(MINFO,"check_psm_gate",
		     "WARNING...  PSM default mode is NOT enabled for profile \"%s\". This should only be used for testing PSM",
		     my_profile);
	    }
	}
    } //  loop
  printf("check_psm_gate: success\n");
  return 1; // SUCCESS=1
}
#endif // else...    PSMIII
#endif // PSM

/*------------------------------------------------------------------*/
double mult (double a,DWORD b)
{
 DWORD  d = 0;
 double x = 0;
 while (d<b)
  {d++;
   x = x + a;
  }
return x;
}



/*------------------------------------------------------------------*/
INT first_match(SUBSTRING *c, INT n)
{
  INT   k, klow = -1;
  char *ptmp=NULL;
  
  for(k=0 ; k<n ; k++)
  {
    if (c[k].p != NULL)
    {
      if (klow == -1)     ptmp = c[k].p;
      if (c[k].p <= ptmp) klow = k;
    }
  }
  return klow;
}

/*------------------------------------------------------------------*/
INT substitute(char *rulefile
	       , char *inpath
	       , char *outpath, char *outfile)
{
  INT    n_element, k, j, status = 1;
  char   str[256], *pb, *pp, infile[128];
  FILE   *inf=NULL, *outf=NULL, *rulef=NULL;

  debug=1; //temp
  /* init structure */
  memset(c, 0, N_SUBSTRINGS*sizeof(SUBSTRING));

  /* Open config file */
  rulef = fopen(rulefile, "r");
  if (rulef == NULL)
  {
    cm_msg(MERROR,"substitute","can't open configuration file:  %s", rulefile);
    return -4;
  }

  /* get in and out files path (read from rulefile) */
  fgets(infile, 128, rulef);
  infile[strlen(infile)-1] = '\0';
  if (debug) printf("rulef: Input template filename:%s\n",infile);

  fgets(outfile, 128, rulef);
  outfile[strlen(outfile)-1] = '\0';
  if (debug) printf("rulef: Output file:%s\n",outfile);

  /* Open infile  template file */
  sprintf(str,"%s%s", inpath, infile);
  inf = fopen(str, "r");
  if (inf == NULL)
  {
    status = -1;
    cm_msg(MERROR,"substitute","can't open (input) template file: %s", str);  
    cm_msg(MERROR,"substitute","template filename may not exist due to illegal combination of input parameters");  
    goto in_error;
  }

  /* Open outfile file */
  sprintf(str,"%s%s", outpath, outfile);
  outf = fopen(str, "w");
  if (outf == NULL)
  {
    status = -1;
    cm_msg(MERROR,"substitute","can't create configuration Sequencer file: %s", str);
    goto in_error;
  }

  /* store substitution strings */
  k = 0;
  while (fgets(str, 128, rulef) != NULL)
  {
    if (debug) printf("rulef:%s",str);
    pb = (char *) str;
    pp = strstr(pb, "%");
    if (pp == NULL)
    {
      status = -1;
      cm_msg(MERROR,"substitute","wrong rule format %s", str);
      goto in_error;
    }
    c[k].sl = pp-pb;
    strncpy(c[k].s, pb, c[k].sl);
    c[k].rl = strlen(pp)-2;
    strncpy(c[k].r, pp+1, c[k].rl);
    k++;
  }
  n_element = k;

  if (debug)
  {
    for (k=0;k<n_element;k++)
      printf("k:%d *p:%p :%d-s:%s / :%d-r:%s\n", k, c[k].p, c[k].sl, c[k].s, c[k].rl, c[k].r);
  }

  fclose(rulef); rulef = NULL;

  while (fgets(str, 128, inf) != NULL)
  {
    pb = (char *) str;

    /* mark string to be replaced */
    for(k=0;k<n_element;k++)
      c[k].p = strstr(str, c[k].s);

    /* replace ALL substring in that line */
    for (k=0;k<n_element;k++)
    {
      /* find first one */
      j = first_match(c, n_element);
      if (j < 0)
      {
	/* No substitution to be done in that line (last part) */
	fputs(pb , outf);
	break;
      }
      else
      {
	/* first part */
	for (; pb < c[j].p ; pb++)
	  {
	  fputc(*pb , outf);
	  //	  printf("outfile: *pb=\"%s\" ",pb);
	  }
	
	/* substitution part */
	fputs(c[j].r , outf);
		printf("writing c[%d].r=\"%s\" to outfile\n",j,c[j].r);
	/* substitution done */
	c[j].p = NULL;

	
	/* adjust source pointer */
	pb += c[j].sl;
      }
    }
  }

 in_error:
  if (rulef) fclose(rulef);
  if (inf) fclose(inf);
  if (outf) fclose(outf);
  if (status < 1)
    return status;

  if (debug)
    {
      printf("\nRule   file :%s\n", rulefile);
      printf("Input  file :%s%s\n", inpath,infile);
      printf("Output file :%s%s\n", outpath,outfile);
    }
   debug=0; //temp
  return SUCCESS;
}


/*------------------------------------------------------------------*/
INT settings_rec_get(void)
{
  /* retrieve the ODB structure
     check if the rule file exists in the given dir
     return the rulefile
  */
  INT size, status;
  // char str[64];
  INT param_index;


  /* get the record for mdarc area (we already found the key in main) */
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMdarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"settings_rec_get","Failed to retrieve mdarc record  (%d) size=%d ",status,size);
    return(-1); /* error */
  }
  else
    if(debug)printf("settings_rec_get: got the record for mdarc\n");

  /* get record for sis test mode */
  size = sizeof(ppg.sis_test_mode);
  status = db_get_record(hDB, hTest, &ppg.sis_test_mode, &size, 0);
  if (status != DB_SUCCESS)
    {
      printf("settings_rec_get: Failed to retrieve record for sis test mode  size=%d, struct size=%d (%d))\n",
	     size, (int)sizeof(ppg.sis_test_mode),(int)status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for sis test mode  size=%d, struct size=%d (%d))",
	     size, (int)sizeof(ppg.sis_test_mode),(int)status);
      return (-1);
    }
  else
    if(debug)
      printf("settings_rec_get: got the record for sis test mode\n");

 /* get record for hardware */
  size = sizeof(ppg.hardware);
  status = db_get_record(hDB, hH, &ppg.hardware, &size, 0);
  if (status != DB_SUCCESS)
    {
      printf("settings_rec_get: Failed to retrieve record for hardware  size=%d, struct size=%d (%d))\n",
	     size, (int)sizeof(ppg.hardware),(int)status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for hardware  size=%d, struct size=%d (%d))",
	     size, (int)sizeof(ppg.hardware),(int)status);
      return (-1);
    }
  else
    if(debug)
      printf("settings_rec_get: got the record for hardware\n");

  printf("settings_rec_get: ppg.hardware.psm.fref.profile_enabled=%d\n", ppg.hardware.psm.fref.profile_enabled);

  /* get the record for hIn */
  size = sizeof(ppg.input);
  status = db_get_record(hDB, hIn, &ppg.input, &size, 0);
  if (status != DB_SUCCESS)
  {
    printf("settings_rec_get:Failed to retrieve record for Input  (%d)",status); 
    cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for Input  (%d)",status);
    return (-1);
  }
  else
    //if(debug)
      printf("settings_rec_get: got the record for Input\n");


  printf("setting_rec_get: selected ppg mode is %s\n", ppg.input.experiment_name);


  /* get the record for hOut */
  size = sizeof(ppg.output);
  status = db_get_record(hDB, hOut, &ppg.output, &size, 0);
  if (status != DB_SUCCESS)
    {

      printf("settings_rec_get: Failed to retrieve record for Output  (%d)",status); 
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for Output  (%d)",status);
      return (-1);

    }
  else
    if(debug)
      printf("settings_rec_get: got the record for output\n");


  // call subroutine to copy the mode parameters into the input or hardware and psm areas
  // use ppg.input.experiment_name because ppg_mode may have been changed to call the correct eXX_compute.
  printf("calling param_search for ppg_mode %s\n",ppg.input.experiment_name);       
  param_index = param_search(ppg.input.experiment_name);
  if(num_copy_defined < 0)  // num_copy_defined is global
    {
      printf("settings_rec_get: Error - no null at end of  param_update structure or MAX_BNMR_EXPT is too small\n");
      cm_msg(MERROR, "settings_rec_get","programming error: no null at end of param_update structure or MAX_BNMR_EXPT too small");
      return -1;
    }
  else if( num_copy_defined == 0)
    {
      printf("settings_rec_get: Error - no copy functions defined in param_update structure\n");
      cm_msg(MERROR, "settings_rec_get","programming error: no copy functions defined in param_update structure");
      return -1;
    }
  if(param_index >=  num_copy_defined)
    {
      printf("settings_rec_get: Error - no copy function found for ppg_mode=\"%s\" for this beamline (\"%s\"). Cannot copy mode parameters\n", ppg.input.experiment_name, getenv ("BEAMLINE"));
      cm_msg(MERROR, "settings_rec_get","No copy function found for ppg_mode=\"%s\"", ppg.input.experiment_name );
      return -1;
    }
  else
    printf("settings_rec_get: found copy function for ppg_mode=\"%s\" at index=%d  \n",ppg.input.experiment_name,param_index);

  // Now update the parameters

  printf("settings_rec_get: calling param_update[%d]  with ppg_mode %s\n", param_index,  ppg.input.experiment_name);
  status = param_update[param_index].op_func(ppg.input.experiment_name);
  printf("settings_rec_get: status returned after param_update is %d\n",status);
  if(status != SUCCESS)
    return -1; // error


#ifdef PSM
 /* Now get record again for hardware/psm */
  size = sizeof(ppg.hardware.psm);
  status = db_get_record(hDB, hPSM, &ppg.hardware.psm, &size, 0);
  if (status != DB_SUCCESS)
    {
      printf("settings_rec_get: Failed to retrieve record for hardware/psm  size=%d, struct size=%d (%d))\n",
	     size, (int)sizeof(ppg.hardware.psm),(int)status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for hardware/psm  size=%d, struct size=%d (%d))",
	     size, (int)sizeof(ppg.hardware.psm),status);
      return (-1);
    }
  else
    if(debug)
      printf("settings_rec_get: got the record for hardware/psm\n");

#endif  //PSM



  return status;
}

INT check_params(char *ppg_mode)
{
  INT j,status;

  if(debug)printf("check_params: num histos=%d (type1) or %d (type2) \n",
		  ppg.input.num_type1_frontend_histograms, ppg.input.num_type2_frontend_histograms );


  if (ppg.input.ppg_path[0] != 0)
    if (ppg.input.ppg_path[strlen(ppg.input.ppg_path)-1] != DIR_SEPARATOR)
      strcat(ppg.input.ppg_path, DIR_SEPARATOR_STR);

  if (ppg.input.cfg_path[0] != 0)
    if (ppg.input.cfg_path[strlen(ppg.input.cfg_path)-1] != DIR_SEPARATOR)
      strcat(ppg.input.cfg_path, DIR_SEPARATOR_STR);

  ppg.output.ppg_loadfile[0] = '\0';
  ppg.output.ppg_template[0] = '\0';

  printf("check_params: PPG MODE is %s\n",ppg.input.experiment_name);

  /* make sure it's in lower case */
  if (ppg.input.experiment_name[0] != 0)
    {
      sprintf(ppg_mode,"%s",ppg.input.experiment_name);
      ppg_mode[2]='\0'; /* terminate after two characters */
      for  (j=0; j< strlen(ppg_mode) ; j++) 
	ppg_mode[j] = tolower (ppg_mode[j]); /* convert to lower case */  
      // ppg_mode is returned to caller
      
      printf("check_params: and ppg_mode is %s\n",ppg_mode);
      if( strncmp (ppg_mode,"1",1)==0) 	 
	expt_type=1; // type 1 mode 	 
      else 	 
	expt_type=2; // type 2 mode 	 
 
        // No Type 1 modes can be run in dual channel mode
	if(ppg.hardware.enable_dual_channel_mode)
	  {
	    if(expt_type == 1)
	      {
		printf("check_params: PPG mode %s cannot be run in Dual Channel Mode\n",ppg_mode);
		cm_msg(MERROR,"check_params",
		   "PPG mode %s cannot be run in Dual Channel Mode",ppg_mode);
		return -1;
	      }
	  }


      // TYPE2  modes
     
      /* Detect dummy mode 20 -> reverts to 00 */
      if (strcmp(ppg_mode,"20")==0 )
	{
	  cm_msg(MINFO,"check_params","Mode 20: ppg mode is actually 00"); 
	  printf("check_params: Mode 20: ppg mode is actually 00\n"); 
	  sprintf(ppg_mode,"00"); /* mode is really 00 */
	}
      else if (strcmp(ppg_mode,"2d")==0 )
	{
	  cm_msg(MINFO,"check_params","Mode 2d: ppg templates for 1b will be used");
	  printf("check_params: Mode 2d: ppg templates for 1b will be used\n");
	}
  

      // TYPE 1 modes
      /* Detect dummy modes 1d/1e -> revert to 1n */
      sprintf(ppg.output.e1n_epics_device,"none") ;   /* initialize for all modes */
      if (strcmp(ppg_mode,"1n")==0 )
	{
	  cm_msg(MINFO,"check_params","Mode 1n: epics scan device is NaCell"); 
	  sprintf(ppg.output.e1n_epics_device,"NaCell") ; /* write epics device */
	}
      else if (strcmp(ppg_mode,"1d")==0 )
	{
	  cm_msg(MINFO,"check_params","Mode 1d: setting epics scan device to  Laser & resetting ppg mode to 1n"); 
	  printf("check_params: Mode 1d: setting epics scan device to  Laser & resetting ppg mode to 1n\n"); 
	  sprintf(ppg_mode,"1n"); /* mode is really 1n */
	  sprintf(ppg.output.e1n_epics_device,"Laser") ; /* write epics device */
	}
      else if (strcmp(ppg_mode,"1e")==0 )
	{
	  cm_msg(MINFO,"check_params","Mode 1e: setting epics scan device to  Field & resetting ppg mode to 1n"); 
	  printf("check_params: Mode 1e: setting epics scan device to  Field & resetting ppg mode to 1n\n"); 

	  sprintf(ppg_mode,"1n"); /* mode is really 1n */
	  sprintf(ppg.output.e1n_epics_device,"Field") ; /* write epics device */
	}

      /* Detect dummy mode 10 (1f with dummy frequency scan) */
      else if (strcmp(ppg_mode,"10")==0 )
	{
	  cm_msg(MINFO,"check_params","Mode 10: dummy frequency scan; resetting ppg mode to 1f"); 
	  printf("check_params: Mode 10: dummy frequency scan; resetting ppg mode to 1f\n"); 
	  sprintf(ppg_mode,"1f"); /* mode 10 is really 1f */
	}
   

#ifdef CAMP
      else if (  (strcmp(ppg_mode,"1c")==0) || (strcmp(ppg_mode,"1j")==0 ))
	{
	  printf("CAMP mode %s is selected; getting CAMP record\n",ppg_mode);
	  status=camp_get_rec() ;
	  if(status != SUCCESS)
	    {
	      if (debug) printf("check_params: failure after camp_get_rec\n");
	      return(status);
	    }
	}
#endif // CAMP


      return 1; // success
    } // ppg.input.experiment_name
  else
    {
      cm_msg(MERROR,"check_params","Experiment name from ODB is empty");
      return -1;
    }
}



INT param_create_rec(void)
{
  INT size,status;
  char str[128];

  FIFO_ACQ_MODE_PARAMETERS_STR(fifo_acq_mode_parameters_str); // from experim.h

  /* Find the key hMP */

  sprintf(str,"/Equipment/fifo_acq/mode parameters"); 
  status = db_find_key(hDB,0,str, &hMP);
  if (status != DB_SUCCESS)
    {
      printf("param_create_rec: can't find key %s",str);
      
      /* So try to create record */
      
      printf("param_create_rec: attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(fifo_acq_mode_parameters_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"param_create_rec","Could not create mode parameters record (%d)\n",status);
	}
      else
	//if (debug)
	  printf("param_create_rec: success from create record for %s\n",str);
      
      /* Check  if the structure "mode parameters"  exists  */
      status = db_find_key(hDB,0,str, &hMP);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"param_create_rec","%s  not present. Cannot find key(%d)",str,status);
	  return status;
	}
      else
	printf("param_create_rec: found the key for %s\n",str);
    }
  else
    printf("param_create_rec: found the key for %s\n",str);
  
  /* check that the record size is as expected */
  status = db_get_record_size(hDB, hMP, 0, &size);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "main", "error during get_record_size (%d) for record %s",(int)status, str);
      return status;
    }

  printf("Size of ppg mode parameters (%s) saved structure: %d, size of record: %d\n", str, (int)sizeof(FIFO_ACQ_MODE_PARAMETERS) ,size);
  if (sizeof(FIFO_ACQ_MODE_PARAMETERS) != size) {
    /* create record */
    cm_msg(MINFO,"main","creating record for ppg modes parameters (%s); mismatch between size of structure (%d) & record size (%d)", 
	   str, (int)sizeof(FIFO_ACQ_MODE_PARAMETERS) , size); 
    status = db_create_record(hDB, 0, str , strcomb(fifo_acq_mode_parameters_str));
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"main","Could not create frontend record for ppg modes parameters i.e. \"%s\" (%d)\n",str,status);
	return status;
      }
    else
      //if (debug)
	printf("param_create_rec: success from create record for %s\n",str);
  

    /* check that the record size is as expected  now */
    status = db_get_record_size(hDB, hMP, 0, &size);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR, "main", "error during get_record_size (%d) for record %s",(int)status, str);
	return status;
      }

    printf("Size of ppg modes parameter saved structure: %d, size of record: %d\n", (int)sizeof(FIFO_ACQ_MODE_PARAMETERS) ,size);
    if (sizeof(FIFO_ACQ_MODE_PARAMETERS) != size) 
      {
	cm_msg(MERROR, "main", "Error getting ppg modes parameter record. Size of saved structure: %d, size of record: %d", (int)sizeof(FIFO_ACQ_MODE_PARAMETERS) ,size); 
	return DB_INVALID_PARAM;
      }
  }

  return SUCCESS;
}





#ifdef CAMP
/* ------------------------------------------------------------------*/
INT camp_create_rec(void)
{
  /* retrieve the ODB structure for CAMP sweep device
  */
  INT size, status;
  char str[128];
  FIFO_ACQ_CAMP_SWEEP_DEVICE_STR(fifo_acq_camp_sweep_device_str); /* for camp (from experim.h) */


  /* get the key hCamp  */
  sprintf(str,"/Equipment/%s/camp sweep device",eqp_name); 
  status = db_find_key(hDB, 0, str, &hCamp);
  if (status != DB_SUCCESS)
    {
      if(debug) printf("camp_create_rec: Failed to find the key %s ",str);
      
      /* Create record for camp area */     
      if(debug) printf("camp_create_rec:Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(fifo_acq_camp_sweep_device_str));
      if (status != DB_SUCCESS)
	{
	  if(debug)printf("camp_create_rec: Failure creating camp record\n");
	  cm_msg(MINFO,"camp_create_rec","Could not create record for %s  (%d)\n", str,status);
	  return(-1);
	}
      else
	if(debug) printf("camp_create_rec: Success from create record for %s\n",str);
      /* try again to get the key hCamp  */
      status = db_find_key(hDB, 0, str, &hCamp);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"camp_create_rec", "Failed to get the key %s ",str);
	  return(-1);
	}
    }    
  else  /* key hCamp has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hCamp, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "camp_create_rec", "error during get_record_size (%d) for camp",status);
	  return status;
	}
      printf("camp_create_rec - Size of camp saved structure: %d, size of camp record: %d\n", 
	     (int) sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICE) ,size);
      if (sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICE) != size) 
	{
	  cm_msg(MINFO,"camp_create_rec","creating record (camp; mismatch between size of structure (%d) & record size (%d)", 
		 (int) sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICE) , size);
	  /* create record */
	  status = db_create_record(hDB, 0, str , strcomb(fifo_acq_camp_sweep_device_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"camp_create_rec","Could not create camp record (%d)\n",status);
	      return status;
	    }
	  else
	    if (debug)printf("camp_create_rec: Success from create record for %s\n",str);
	}
    }
  return(SUCCESS);
}

/* ------------------------------------------------------------------*/
INT camp_get_rec(void)
{
  /* retrieve the ODB structure for CAMP sweep device
  */
  INT size, status;
  char str[128];

  /* check we have a key hCamp (found in main)  */
  if( hCamp)
    {
 /* get the record for camp area  */
      size = sizeof(fcamp);
      status = db_get_record (hDB, hCamp, &fcamp, &size, 0);/* get the whole record  */
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"camp_get_record","Failed to retrieve %s record  (%d) size=%d ",str,status,size);
	  return(-1); /* error */
	}
      else
	if(debug)printf("camp_get_rec: got the record for camp\n");
    }
  else
    {
      cm_msg(MERROR,"camp_get_record","No key has been found for camp record ");
      return(-1); /* error */
    }
  return(SUCCESS);
}
#endif // ifdef CAMP

/*------------------------------------------------------------------*/
INT expt_search(char *expt_name)
{
  /* search for a matching ext computation function pointer
     return index in ecode struct
  */

  INT k=0;
  printf("expt_search: starting with num_expts_defined=%d\n", num_expts_defined);
  if( num_expts_defined < 0)
    {
      // Find number of experments defined in structure
      for (k=0; k<MAX_BNMR_EXPT; k++)
	{
	  // if(debug) printf("k=%d  expt[%d].name=%s\n",k,k, expt[k].name);
	  if (expt[k].name[0] != 0)
	    {
	      //   if(debug)
	      // printf("expt_search: expt[%d].name=%s\n",k, expt[k].name);
	    }
	  else
	    {
	      //  if(debug)
	      printf("expt_search: there are k=%d experiments defined for beamline %s\n",k,  getenv ("BEAMLINE"));
	      num_expts_defined=k;
	      break;
	    }
	}
      if(num_expts_defined < 0)
	return -1;  // error - no null at end of expt structure
      
    
      k=0;
    }
 
  while (expt[k].name[0] != 0) // final value is NULL
    {
      if (strncmp(expt[k].name, expt_name, 2) == 0)
	{
	  if(debug)printf("expt_search: found expt at index=%d\n",k);
	  break;
	}
      k++;
    }
  return k;
}


INT param_search(char *ppg_mode)
{
  /* search for a matching ext computation function pointer
     return index in ecode struct
  */

  INT k=0;
  printf("param_search: starting with num_copy_defined=%d\n", num_copy_defined);
  if(num_copy_defined < 0)
    { // find how many copy functions are defined
      for (k=0; k<  MAX_BNMR_EXPT; k++)
	{
	  //  printf("k=%d  expt[%d].name=%s\n",k,k, expt[k].name);
	  if (param_update[k].name[0] != 0)
	    {
	      // if(debug)
	      //	printf("param_search: param_update[%d].name=%s\n",k, param_update[k].name);
	    }
	  else
	    {
	      //  if(debug)printf("param_search: break with k=%d\n",k);
	      printf("expt_search: there are k=%d experiments defined for copy update with beamline= %s\n",k,  getenv ("BEAMLINE"));
	      num_copy_defined=k;  // global
	      break;
	    }
	}
      if(num_copy_defined <= 0) // not changed
	return num_copy_defined;  // error - no null at end of structure or no expts defined
      k=0;
    }

  while (param_update[k].name[0] != 0)  // final value is NULL
  {
    printf("param_search: param_update[%d].name=%s\n",k, param_update[k].name);
    if (strncmp(param_update[k].name, ppg_mode, 2) == 0)
      {
	printf("param_search: found function %s for ppg_mode %s  k=%d, breaking\n",param_update[k].name, ppg_mode,k);
	break;
      }
    k++;
  }
  return k;
}



/*------------------------------------------------------------------*/
INT file_wait(char *path, char *file)
{
  /* wait for compiled file */
  INT    j, nfile;
  char   *list = NULL;

  j = 0;
  do {
    ss_sleep(500);
    nfile = ss_file_find(path, file, &list); 
    j++;
  } while(j<5 && nfile != 1);

  if (nfile != 1)
  {
   cm_msg(MERROR,"file_wait","File not found (%s%s)", path, file);
   return -6;  
  }
  else
    if(debug)printf("file_wait: found file %s%s\n",path,file);
  return 1;// SUCCESS=1
}

INT check_other(void)
{
  char bnmr[]="bnmr";
  char bnqr[]="bnqr";
  char *p,*q;
  char cmd[128];
  char  infofile[]="odbmsg.txt";
  char str[256];
  FILE *FOUT;
  INT status,ival=0;
  // In single_channel_mode,
  // Prevent run start if the other experiment (bnm/qr) is  running
  // No protection against both scanning the Na cell, flipping helicity
  printf("check_other: starting\n");   
 if(ppg.hardware.enable_dual_channel_mode)
   return SUCCESS; // both sides can be running


  if (strcmp(Expt_name,"bnmr")==0)
    p=bnqr;
  else
    p=bnmr;
  printf("other:%s\n",p);
  // ls -v to send out value only
  sprintf(cmd,"odb -e %s -c 'ls -v /Runinfo/State' &> %s ",p,infofile);
  printf("cmd:\"%s\"\n",cmd);
  status = system(cmd);
  if(status)
    {
      printf("rf_config: cannot access experiment %s to see if it is running\n",p);
      cm_msg(MERROR,"rf_config","cannot access experiment %s to see if it is running",p);
      cm_msg(MINFO,"rf_config","User should make sure experiment %s is STOPPED",p);
      return SUCCESS; // continue to start run.
    }

  FOUT= fopen(infofile,"r");    
  if (FOUT != NULL)
    {
      if(status) printf("ODB REPLY:\n");
      while(fgets(str,256,FOUT) != NULL)
	{
	  if (status)
	    printf("%s",str);
	  else
	    {
	      q=str; 
	      if( isdigit(*q) )
		{
		  ival = atoi ( str );
		  printf("ival=%d\n",ival);
		}
	      else
		{
		  printf("rf_config : run state is not a digit: \"%s\" \n",str);
		  cm_msg(MINFO,"rf_config","run state is not a digit: \"%s\" ",str);
		}
	      break;
	    }
	}	
    }
  else
    {
      printf("rf_config: cannot open file %s; cannot perform check on %s\n",infofile,p);
      cm_msg(MINFO,"rf_config","cannot open file %s",infofile);
      cm_msg(MINFO,"rf_config","User should make sure experiment %s is STOPPED",p);
    }
  if(ival != STATE_STOPPED)
    {
      // The other DAQ is running. Prevent this run from starting
      cm_msg(MERROR,"rf_config","%s DAQ system is RUNNING!! One DAQ only can be running in Single Channel Mode",
	     p);
      printf("rf_config: %s DAQ system is RUNNING!! One DAQ only allowed in Single Channel Mode\n",p);
      return FE_ERR_HW;
    }
  printf("rf_config: other side (%s) is NOT running; continuing... \n",p);
  cm_msg(MERROR,"rf_config","Other side (%s) is NOT running; continuing... ",p);
  return SUCCESS;
}

/*------------------------------------------------------------------*/
INT tr_precheck(INT run_number, char *error)
{
  INT status;
  

  sprintf(error,"error from rf_config (tr_precheck)... see MIDAS messages");
 
  if(!single_shot)
      status = al_reset_alarm(NULL); // reset all alarms

  copy_last_params_flag=0;  // set false in case of failure; prevents saving parameters in _last.odb (see lasttune)

  printf("tr_precheck: Calling settings_rec_get\n");

  /* get settings all records */
  status = settings_rec_get();/* returns -1 for fail, or SUCCESS */
  if (status < 0)
    {
      printf("tr_precheck: Error return after settings_rec_get. See odb messages for details\n");
      return status;
    }

#ifdef HAVE_SIS3820  
  //  char  ppg_mode[30];
  /* check whether we are using SIS3820 test mode = 0 i.e. no PPG required */
  if (ppg.hardware.sis3820.sis_mode == 0)
    {

      printf("tr_precheck: SIS3820 sis_mode=0. No PPG is needed for this mode. Done.\n");
      cm_msg(MINFO,"tr_precheck","SIS3820 sis_mode=0 No PPG is needed for this mode");
      if(strncmp (ppg.input.experiment_name,"10",2) != 0)
	{
	  printf("tr_precheck: PPG Mode must be 10 for sis_mode=0\n");
	  cm_msg(MERROR,"tr_precheck","PPG Mode must be 10 for sis_mode=0");
	  return  DB_INVALID_PARAM;
	}
      no_ppg_flag=1;
      return SUCCESS;
    }
#endif //  HAVE_SIS3820  
    
  sprintf(error," "); // no error
  return SUCCESS;
}

INT tr_start_abort(INT run_number, char *error)
{
  BOOL flag=TRUE;

  write_start_abort(flag);
  return SUCCESS;
} 

INT write_start_abort(BOOL flag)
{
  INT status;
  char str[32];

  sprintf(str, "equipment/%s/client flags",eqp_name );
  if(flag)
    cm_msg(MERROR,"rf_config","Detected start-abort transition");
  status = db_set_value(hDB, hClient, "start_abort", &flag, sizeof(flag), 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "tr_start_abort", "cannot set client flag to %d at path \"%s/start_abort\" (%d) ",flag, str,status);
      return status;
    }
  return SUCCESS;
} 




/*------------------------------------------------------------------*/
INT tr_prestart(INT run_number, char *error)
{
  INT   size, status;
  INT   expt_code;
  char  rulefile[256];
  char  outfile[256] , foutfile[256]  , cmd[512];
  char  ppg_mode[30];
  //#ifdef HAVE_NEWPPG
  //  char ppg_perl_path[64];
  //#endif
  char  infofile[256];
  FILE *errfile;
  char str[256];
  char cmd5[128];
  char timbuf_ascii[30];
  INT i;
   BOOL flag;
  
  int j;

  HNDLE hTot;
  KEY key;
  INT nh_tot;

  struct stat stat_buf;   
 // Note: called with run_number=10 for single loop; why 10? 10 is not a valid rn for bnmr/bnqr fortunately)
 //     using single_shot which is true for single loop

  if(no_ppg_flag)
    return SUCCESS;

  sprintf(error,"error from rf_config (tr_prestart)... see MIDAS messages"); // default return error msg to cm_transition

  /* remove old bytecode.dat from the CFG path */
  sprintf(foutfile,"%s/bytecode.dat", ppg.input.cfg_path);
  printf("foutfile=%s\n",foutfile);


  sprintf(cmd5,"ls %s",foutfile);
  printf("cmd = %s\n",cmd5);
  status =system(cmd5);
  printf("status=%d\n",status);

  status = ss_file_remove(foutfile);
  if (status != 0)
    cm_msg(MINFO,"prestart","old file %s not found (to be deleted)", foutfile);
  else
    printf("prestart: successfully removed old file %s\n",foutfile);
  
  
  printf("cmd = %s\n",cmd5);
  status =system(cmd5);
  printf("status=%d\n",status);
  
  if(!single_shot)
    {
      printf("tr_prestart: setting client flag for rf_config to false\n"); 
      // except on single loop mode....
      /* first set the client flag to FALSE in case of failure */
      
      flag = FALSE;
      status = db_set_value(hDB, hClient, "rf_config", &flag, sizeof(flag), 1, TID_BOOL);
      if (status != DB_SUCCESS)
      	{
      	  cm_msg(MERROR, "tr_prestart", "cannot clear client flag at path \"%s/rf_config\" (%d) ",client_str,status);
      	  return status;
      	}
    }
#ifdef EPICS_ACCESS     
 /*  do this before setting client flag for prestart failure (or message about run parameters appears on failure)
  */

  /* checks EPICS Access Permissions using caget */ 
  if(!ppg.hardware.enable_all_hel_checks)
    {
      cm_msg(MINFO,"tr_prestart","Warning... EPICS Helicity switch check is DISABLED in the ODB");
      write_alarm(0,""); // clear epics access alarm
    }
  else
    {
      status= check_epics_hel_switch(error);
      if(status != DB_SUCCESS)
	{
	  printf("tr_prestart: Error return after check_epics_hel_switch(%d). Error=\"%s\" See odb messages for details\n",status,error);
	   if(!single_shot)
	     return status;
	   else
	     cm_msg(MINFO,"tr_prestart","Continuing after helicity switch check failure, as running in single shot mode");
	}
    }
      
  if (!ppg.hardware.enable_epics_switch_checks)
    {
      cm_msg(MINFO,"tr_prestart","Warning... EPICS switch checks are DISABLED in the ODB");
      write_alarm(0,""); // clear epics access alarm
    }
  else
    { 
      status = check_epics_switch_access(error);
      if(status != DB_SUCCESS)
	{
	  printf("tr_prestart: Error return after check_epics_access(%d). Error=\"%s\". See odb messages for details\n",status,error);
	  if(!single_shot)
	    return status;
	  else
	    cm_msg(MINFO,"tr_prestart","Continuing after epics switch check failure, as running in single shot mode");
	}
    }
#else
  cm_msg(MINFO,"tr_prestart","Warning... no EPICS switch checks as EPICS support not built in rf_config ");
  write_alarm(0,""); // clear epics access alarm
#endif
  
  if(!single_shot)
    {
      flag = TRUE;
      status = db_set_value(hDB, hClient, "prestart_failure", &flag, sizeof(flag), 1, TID_BOOL);
      if (status != DB_SUCCESS)
      	{
      	  cm_msg(MERROR, "tr_prestart", "cannot set client flag at path \"%s/prestart_failure\" (%d) ",client_str,status);
      	  return status;
      	}
      
      i=1;
      status = db_set_value(hDB, 0, "/Alarms/alarms/prestart_alarm/triggered", &i, sizeof(i), 1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "tr_prestart", "cannot trigger alarm at path \"/Alarms/alarms/prestart_alarm/triggered\" (%d) ",status);
	  return status;
	}
    } // not single_shot
  else
    printf("tr_prestart: single loop mode, not setting client flag false\n"); 
  
  status = check_params(ppg_mode); /* returns ppg_mode = experiment_name in lower case (e.g. 1n_) */
  if(debug)printf("tr_prestart: received %s for ppg_mode from check_params\n",ppg_mode);
  if (status < 0)
    {
      printf("tr_prestart: Error return after check_params. See MIDAS messages for details\n");
      return status;
    }


#ifndef POL
  printf("tr_prestart: MIDBNMR  use_defaults=%d and no. regions=%d\n",
	 fmdarc.histograms.midbnmr.use_defaults, fmdarc.histograms.midbnmr.number_of_regions);
#endif
  /* single and dual channel mode: now PPG for Type 2 is no longer free-running,
     so set num_beam_acq_cycles to 0 so we can use the existing PPG templates
     for SINGLE and DUAL channel mode. PPG will be started by software (SINGLE channel mode)
     or by hardware (DUAL channel mode). 

     Type 1 and Type 2 are now both NOT free-running
  */
  //printf("dual channel mode is enabled... setting loop counter to 0 for PPG files\n");
    ppg.input.num_beam_acq_cycles=0;/* Type 2 now does one cycle only */
  // to avoid confusion, set this in the ODB
  
  status = db_set_value(hDB, hIn, "num_beam_acq_cycles", &ppg.input.num_beam_acq_cycles, sizeof(ppg.input.num_beam_acq_cycles), 1, TID_DWORD);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "tr_prestart", "cannot set client flag at path \"%s/rf_config\" (%d) ",client_str,status);
      return status;
    }
  printf("ppg.input.num_beam_acq_cycles = %d\n",ppg.input.num_beam_acq_cycles);
  
  
  /* Retrieve expt code */
  status = 0;
  expt_code = expt_search(ppg_mode);  // fills global num_expts_defined
 printf("tr_prestart: after expt_search,  num_expts_defined=%d  expt_code index=%d\n",num_expts_defined, expt_code);
  if (num_expts_defined < 0)
    {
      printf("Error - no null at end of expt structure or MAX_BNMR_EXPT is too small\n");
      cm_msg(MERROR,"tr_prestart"," error in expt structure or MAX_BNMR_EXPT is too small");
      return DB_INVALID_PARAM;
    }
  else if (num_expts_defined == 0)
    {
      printf("Error - no experiments are defined for beamline %s \n", getenv ("BEAMLINE"));
      cm_msg(MERROR,"tr_prestart","no experiments are defined for beamline %s", getenv ("BEAMLINE"));
      return DB_INVALID_PARAM;
    }

 
  if(expt_code >=  num_expts_defined)
    {
      cm_msg(MERROR,"tr_prestart","PPG mode (%s) not defined for this beamline (%s)", 
	     ppg_mode,  getenv ("BEAMLINE"));
      printf("tr_prestart: PPG mode (%s) not defined for this beamline (%s)\n", 
	     ppg_mode,  getenv ("BEAMLINE")); 
      return DB_INVALID_PARAM;
    }
  else
    printf("tr_prestart:found ppg_mode=\"%s\" at index=%d  \n",ppg_mode,expt_code);

  // Now call compute function 
  status = expt[expt_code].op_func(ppg_mode);
  printf("tr_prestart: status returned is %d\n",status); 
  if(status <= 0)
    {
      if(status == 0)
        {
          cm_msg(MERROR,"tr_prestart","PPG mode not found (%s)", ppg_mode);
          printf("tr_prestart: PPG mode not found (%s)\n", ppg_mode);
        }
      else
        {
          cm_msg(MERROR,"tr_prestart","Computation error. Check validity of your input parameters");
          printf("tr_prestart: error return after calling subroutine e%s_compute; see odb messages for details\n",
                 ppg_mode);
        }
      return DB_INVALID_PARAM;
    }




   // Calculate ppg cycle time (output parameter)
   ppg.output.ppg_cycle_time__ms_ =  ppg.output.dwell_time__ms_ * ppg.output.num_dwell_times;
 

  /* Do substitution */
  sprintf(rulefile,"%s%s.rule", ppg.input.cfg_path, ppg_mode);
  printf("tr_prestart: about to call substitute with rulefile: %s\n",rulefile);
  status = substitute(rulefile
		      , ppg.input.ppg_path
		      , ppg.input.cfg_path, outfile
);
  if (status < 1)
    {
      printf("tr_prestart: Error return from substitute. See odb messages for information\n");
      return status;
    }
  printf("tr_prestart: successful return from substitute\n");

  /* update internal structure */
  strcpy(ppg.output.ppg_loadfile, outfile);

  /* update odb parameters */
  ppg.output.compiled_file_time__binary_ = 0; /* binary time */
  strcpy (ppg.output.compiled_file_time,"file deleted"); 
  
  sprintf(infofile,"%sppgmsg.txt", ppg.input.cfg_path);
  /* compile file */
  /* sprintf(cmd,"%sppgcompile.sh %s %s &>%s", ppg.input.cfg_path, ppg.input.cfg_path, outfile,infofile);
  printf("cmd = \"%s\"\n",cmd);
     ss_system(cmd);
  */
  //  sprintf(cmd,"%sppgcompile.sh %s %s &>%s", ppg.input.cfg_path, ppg.input.cfg_path, outfile,infofile);
  sprintf(cmd,"/usr/bin/perl %s/comp_int.pl %s%s %s &>%s", comp_path, ppg.input.cfg_path, outfile, foutfile ,infofile);
  printf("cmd=\"%s\"\n",cmd);
  if(strlen(cmd) > sizeof(cmd))
    { /* overwriting cmd caused stat to fail due to overwrite of foutfile */
      cm_msg(MERROR,"prestart","Internal programming error. Length of cmd (%d) is too short for command string(%d)",(int)strlen(cmd) , (int)sizeof(cmd));
      printf("tr_prestart: Error return. See odb messages for details\n");
      return -1;
    }
  status = system(cmd);
  if(debug)
    {
      //printf("status after system command = %d\n",status);
      printf("cmd: %s\n",cmd);
    }
  if(status)
    {
      cm_msg(MERROR,"prestart","Error return from ppg compiler\n");
      errfile = fopen(infofile,"r");
      
      if (errfile != NULL)
	{
	  sprintf(str,"PPG COMPILER ERROR MESSAGES:");
	  cm_msg(MERROR,"prestart","%s",str);
	  while(fgets(str,256,errfile) != NULL)
	      cm_msg(MERROR,"prestart","%s",str);
	}
      
      else
	cm_msg(MERROR,"prestart","Couldn't open ppg compiler error file %s",infofile);

      cm_msg(MERROR,"prestart","File bytecode.dat has NOT been produced by ppg compiler");
      printf("tr_prestart: Error return. See odb messages for details\n");
      return status;
    }
  else
    cm_msg(MINFO,"prestart","Successfully compiled ppg file & written outfile %s ",foutfile);
  

  /* wait for compiled file */
  if(debug)printf("waiting for compiled file %sbytecode.dat...\n", ppg.input.cfg_path);
  status = file_wait(ppg.input.cfg_path, "bytecode.dat");
  //if(debug)printf("status after file wait = %d\n",status);
  if (status < 1)
    {               /* should not get this now */
      cm_msg(MINFO,"prestart","File %sbytecode.dat not found - Compile error", ppg.input.cfg_path);
      cm_msg(MINFO,"prestart","Compiler messages are in file %s\n",infofile); 
      printf("Compile command was: %s",cmd);
      printf("tr_prestart: Error return. See odb messages for details\n");
      return status;
    }
  else
    cm_msg(MINFO,"prestart","compiled file %s produced successfully\n",foutfile);


#ifdef HAVE_NEWPPG
/* Convert bytecode.dat to ppgload.dat for new ppg

  remove old ppgload.dat file from the PPG path */
  sprintf(foutfile,"%s/ppgload.dat", ppg.input.cfg_path);
  printf("foutfile=%s\n",foutfile);
  status = ss_file_remove(foutfile);
  if (status != 0)
      cm_msg(MINFO,"prestart","old file %s not found (to be deleted)", foutfile);

  /* Get the name of the output file (bytecode.dat) */
  sprintf(outfile,"%s/%s",ppg.input.cfg_path,"bytecode.dat");
/* update odb parameters */
  ppg.output.compiled_file_time__binary_ = 0; /* binary time */
  strcpy (ppg.output.compiled_file_time,"ppgload file deleted");
    
  sprintf(cmd,"/usr/bin/perl %s/convert_bytecode.pl %s %s 0 %d",
         comp_path,
          outfile, foutfile,  NOMINAL_PPG_CLOCK );

  errno=0;  // clear errno
  status = system(cmd);
  // if (debug)
    {
      printf("Command string for conversion to NEW PPG code was: \n\"%s\"\n",cmd);
      printf("status after system command = %d\n",status);
    }

  if(status)
    {
     cm_msg(MERROR,"prestart","sys error (from ppg converter) [%d/%d] %s", (status/256), errno, strerror(errno));
     printf("prestart: Error return from ppg converter status/256=%d errno=%d strerror(errno)=%s\n",
            status/256, errno, strerror(errno));
     printf("prestart: strerror(status/256)=%s\n",strerror(status/256));

     cm_msg(MERROR,"prestart","File ppgload.dat has NOT been produced by ppg converter");
     printf("tr_prestart: Error return. See odb messages for details\n");
     return status;
    }
  else
    {
      cm_msg(MINFO,"prestart","Successfully compiled ppg file & written %s ",foutfile);
      printf("prestart:Successfully converted ppg file & written %s\n",foutfile);
    }

  /* correct minimal delay time for the NEW PPG (3 clock cycles rather than 5 for OLD) */
  /*  ppg.input.minimal_delay__ms_ = 3 * ppg.input.time_slice__ms_;
   
          leave as they are for now or timing may change */

  /* wait for converted file */
  if(debug)printf("waiting for converted file %s/ppgload.dat...\n", ppg.input.cfg_path);
  status = file_wait(ppg.input.cfg_path, "ppgload.dat");
  //if(debug)printf("status after file wait = %d\n",status);
 if (status < 1)
    {               /* should not get this now */
      cm_msg(MINFO,"prestart","File %s/ppgload.dat not found - Conversion error",ppg.input.cfg_path );
      printf("Convert command was: %s",cmd);
      printf("tr_prestart: Error return. See odb messages for details\n");
      return status;
    }
    else
      //  cm_msg(MINFO,"prestart","compiled file %s produced successfully",foutfile);
      printf("prestart: converted file %s produced successfully\n",foutfile);
#endif // HAVE_NEWPPG










//printf("foutfile=%s\n",foutfile);
  if( stat (foutfile,&stat_buf) ==0 )
  {
    ppg.output.compiled_file_time__binary_ = stat_buf.st_ctime; /* binary time */
    strcpy(timbuf_ascii, (char *) (ctime(&stat_buf.st_ctime)) );  
     if(debug)printf("Last change to file %s:  %s\n",foutfile,timbuf_ascii); 
    {
      int j;
      j=strlen(timbuf_ascii);
      strncpy(ppg.output.compiled_file_time,timbuf_ascii, j);

      /* An extra carriage return appears unless we do this:  */
      ppg.output.compiled_file_time[j-1] ='\0';
    }    
      
  }
  else
    {
    strcpy (ppg.output.compiled_file_time,"no information available");
    cm_msg(MERROR,"prestart","No information available about compile time of file %s",foutfile);
    cm_msg(MINFO,"prestart","Front-end check on compile time of file will fail");
    }

   if(debug)printf("Writing %s to odb ( compiled file time)\n",ppg.output.compiled_file_time ); 


  /* Update output settings in ODB */
  size = sizeof(ppg.output);
  status = db_set_record(hDB, hOut, &ppg.output, size, 0); 
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to set output record (size %d) (%d)",size,status);
#ifdef BNQR
#ifdef HAVE_SIS3820
  if ( strcmp (ppg_mode,"2h")==0  )
    { // alpha mode
      // make sure input/enable_alpha is true (custom page sets when select mode 2h)
      if(!ppg.hardware.enable_alpha_mode)
	{
	  flag=1;
	  status= db_set_value(hDB, hH, "enable alpha mode", &flag, sizeof(flag), 1, TID_BOOL);
	  if (status != DB_SUCCESS)
	    cm_msg(MERROR, "tr_prestart", "cannot set \"enable alpha mode\" in hardware area (%d) ",status);
	}
    }
#endif // HAVE_3820
#endif // BNQR
  /* Zero mdarc histogram totals (all modes) */

  nh_tot =  fmdarc.histograms.number_defined; // default
  
  // figure out how large the array is

  status = db_find_key(hDB, hHis, "output/histogram totals", &hTot);
  if (status != DB_SUCCESS)
    printf("prestart: can't find output/histogram totals key (%d)\n",status);      
  else
    {
      status = db_get_key(hDB, hTot, &key);
      if (status != DB_SUCCESS)
	printf("prestart: can't get output/histogram totals key (%d)\n",status);      
      else
	{
	  printf("Number of values in output/histogram totals array is %d\n",key.num_values);
	  nh_tot = key.num_values;
	}
    }

  for (j=0;  j < nh_tot; j++)
    fmdarc.histograms.output.histogram_totals[j]=0;    

  fmdarc.histograms.output.total_saved=0;
  /* clear the last saved filename */
  strncpy(fmdarc.last_saved_filename,"",1);
  printf("prestart: histogram totals and last saved filename have been cleared\n");
 
  /* Update Histogram settings in ODB */
  size = sizeof(fmdarc.histograms);
  status = db_set_record(hDB, hHis, &fmdarc.histograms, size, 0);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to save mdarc histograms record (size=%d) (%d)",size,status);  

  if(!single_shot)
    {
         printf("prestart: setting client flag for rf_config true (success)\n"); 
      /* Set the client flag to TRUE (success) */
        flag = TRUE;
        size = sizeof(flag);
        status = db_set_value(hDB, hClient, "rf_config", &flag, sizeof(flag), 1, TID_BOOL);
        if (status != DB_SUCCESS)
      	{
      	  cm_msg(MERROR, "tr_prestart", "cannot set client flag at path \"%s/rf_config\" (%d) ",client_str,status);
      	  return status;
      	}

        flag = FALSE;
    
        status = db_set_value(hDB, hClient, "prestart_failure", &flag, sizeof(flag), 1, TID_BOOL);
       if (status != DB_SUCCESS)
      	{
      	  cm_msg(MERROR, "tr_prestart", "cannot clear client flag at path \"%s/prestart_failure\" (%d) ",client_str,status);
      	  return status;
      	}

      i=0;  
      status = db_set_value(hDB, 0, "/Alarms/alarms/prestart_alarm/triggered", &i, sizeof(i), 1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "tr_prestart", "cannot set alarm inactive at path \"/Alarms/alarms/prestart_alarm/triggered\" (%d) ",status);
	  return status;
	}


    }
  // else
  //  printf("prestart: single loop mode, NOT setting client flag for rf_config true (success)\n"); 

  copy_last_params_flag=TRUE;
  sprintf(error," "); // clear error string
  return SUCCESS;
}


#ifdef EPICS_ACCESS
/*------------------------------------------------------------------*/
INT check_epics_switch_access(char *error)
{
  char cmd[256];
  char str[128];
  char my_expect[10]; // expected response
  char my_mode[10];
  static char alarm_msg[128];
  char msg[256];
  INT status;
  FILE *fin=NULL;
  char filename[]="tmp.txt";
  int  nalarm;
  char err_msg[80];

  printf("\ncheck_epics_switch_access: starting\n");
  write_alarm(0,""); // clear alarm to start with

  /* check permission for EPICS switches access  */
  nalarm=1; // in case of failure
  sprintf(err_msg,"problem with EPICS access permission");// in case of failure
  sprintf(my_expect,"expbnmr");

  sprintf(alarm_msg,"set EPICS access permission to %s",my_expect);
  
  sprintf (cmd,"caget \"ILE2:BIAS15:VOL.ASG\" >%s",filename);
  printf("check_epics_switch_access:  cmd=%s\n",cmd);
  printf("check_epics_switch_access:  alarm_msg=%s\n",alarm_msg);
  status = read_and_retry(cmd);
  if(status != SUCCESS)
    {
      status=write_alarm(1,&alarm_msg[0]); // set alarm message
      sprintf(msg,"Error return from \"%s\"; cannot read EPICS access permission (%d)",cmd,status);  
      goto err;
    }

  fin = fopen(filename,"r");
  if(fin != NULL)
    {
      if(fgets(str, 120, fin) != NULL)
	{
	  if (strstr(str,my_expect)!= 0)
	    {
	      cm_msg(MINFO,"check_epics_switch_access","Access permission IS given to %s",my_expect);
	      // goto cont1;  // SUCCESS... next check
	    }
	  else
	    {
	      printf("check_epics_switch_access:\"%s\" not found in string \"%s\"\n",my_expect,str);
	      cm_msg(MERROR,"check_epics_switch_access",
		     "Access permission NOT given to %s (%s)",my_expect,str);
	      status = write_alarm(1,alarm_msg); // set alarm message
	      strcpy(error,err_msg);
	      return DB_INVALID_PARAM; // Check has failed
	    }
	}
      else
	{
	  sprintf(msg,"Cannot read line from file %s; cannot check EPICS  access permission",filename);
	  goto err;
	}
    }
  else
    {
      sprintf(msg,"Cannot open file %s. Cannot check EPICS access permission\n",filename);
      goto err;
    }
  // cont1:
  if(fin)fclose (fin);

  /* now check switch position for single/dual mode 
  */
  nalarm=3; // in case of failure
  sprintf(err_msg,"problem with EPICS switch position");// in case of failure
  if (ppg.hardware.enable_dual_channel_mode)
    {
      sprintf(my_mode,"dual");
      sprintf(my_expect,"on"); // switching should be enabled
    }
  else
    {
      sprintf(my_mode,"single");
      sprintf(my_expect,"off"); // switching should be disabled
    }
  /* Default message on error */
  if (ppg.hardware.enable_dual_channel_mode)
    sprintf(alarm_msg,"EPICS Mode switch should be set to Enable Switching for Dual Channel Mode");
  else
    sprintf(alarm_msg,"EPICS Mode switch should be set to Disable Switching for Single Channel Mode");

  sprintf (cmd,"caget \"BNMR:BNQRSW:ENBSWITCHING\" >%s",filename);

  status = read_and_retry(cmd);
  if(status != SUCCESS)
    {
      printf("check_epics_switch_access: Error return from \"%s\"\n ",cmd);
      sprintf(msg,"Error return from \"%s\"; cannot read Epics Single/Dual channel mode switch",cmd);    
      goto err;
    }

  fin = fopen(filename,"r");
  if(fin != NULL)
    {
      if(fgets(str, 120, fin) != NULL)
	{
	  printf("str:\"%s\"; expected response:\"%s\"\n",str,my_expect);
	  if (strstr(str,my_expect)!= 0)
	    {
	      cm_msg(MINFO,"check_epics_switch_access","Dual/Single channel mode EPICS switch is set correctly to %s",my_expect);
	      // goto cont2
	    }
	  else
	    {
	      printf("check_epics_switch_access:\"%s\" not found in string \"%s\"\n",my_expect,str);
	      cm_msg(MERROR,"check_epics_switch_access","Single/Dual channel mode EPICS switch is set incorrectly");
	      status = write_alarm(1,alarm_msg); // set alarm message
	      strcpy(error,err_msg);
	      return DB_INVALID_PARAM; // Check has failed
	    }
	}
      else
	{
	  sprintf(msg,"Cannot read line from file %s",filename);
	  goto err;
	}
    }
  else
    {
      sprintf(msg,"Cannot open file %s",filename);
      goto err;
    }
  if(fin)fclose(fin);
  printf("check_epics_switch_access: returning SUCCESS\n");
  return SUCCESS;

 err:
  if(fin)
    fclose(fin);
  cm_msg(MERROR, "check_epics_switch_access","%s",msg);
  cm_msg(MINFO,  "check_epics_switch_access"," User must %s ",alarm_msg);
  cm_msg(MINFO,  "check_epics_switch_access",
	 "To disable this check, set \".../Hardware/enable Epics switch checks\" to \"n\" ");

  status = write_alarm(nalarm,alarm_msg); // set alarm message
  strcpy(error,err_msg);
  return DB_INVALID_PARAM;
}

/*------------------------------------------------------------------*/
INT check_epics_hel_switch(char *error)
{
 /* check helicity switch position  (not fatal if not flipping) */
  char cmd[256];
  char str[128];
  char my_expect[10]; // expected response
  // char my_mode[10];
  char alarm_msg[128];
  char msg[256];
  INT status;
  FILE *fin=NULL;
  char filename[]="tmp.txt";
  int nalarm=2; // in case of failure
  char err_msg[80];

  printf("\ncheck_epics_hel_switch: starting\n");

  write_alarm(0,""); // clear alarm to start with
  

  sprintf(err_msg,"problem with EPICS helicity switch");// in case of failure
  if (ppg.hardware.enable_helicity_flipping)
    sprintf(my_expect,"off"); // helicity is to be flipped; DAQ should have control
  else
    sprintf(my_expect,"on"); // helicity not flipped;  EPICS should have control
  
  /* Default message */
  sprintf(alarm_msg,"check that EPICS helicity switch is set correctly");    
  sprintf (cmd,"caget \"ILE2:POLSW2:STATLOC\" >%s",filename);
  printf("alarm_msg=%s\n",alarm_msg);
  status = read_and_retry(cmd);
  if(status != SUCCESS)
    {
     printf("failure after read_and retry, alarm_msg=%s\n",alarm_msg);
      sprintf(msg,"Error return from \"%s\"; cannot read Epics Helicity switch (%d)",cmd,status);    
      goto err;
    }
  else
    printf("success from read_and_retry\n");

  fin = fopen(filename,"r");
  if(fin != NULL)
    {
      if(fgets(str, 128, fin) != NULL)
	{
	  printf("str:\"%s\"; my_expect:\"%s\"\n",str,my_expect);
	  if (strstr(str,my_expect)!= 0)
	    {
	      cm_msg(MINFO,"check_epics_hel_switch","Helicity switch is set correctly to %s",my_expect);
	      printf("check_epics_hel_switch: Helicity switch is set correctly to %s\n",my_expect);
	    }
	  
	  else
	    {
	      if (ppg.hardware.enable_helicity_flipping)
		{
		  cm_msg(MERROR,"check_epics_hel_switch","Helicity switch must be set Remote");
		  sprintf(alarm_msg,"set EPICS helicity switch to \"Remote\" for DAQ control");
		  status = write_alarm(nalarm,alarm_msg); // set alarm message
		  strcpy(error,err_msg);
		  return DB_INVALID_PARAM;
		}
	      else
		{
		  cm_msg(MINFO,"check_epics_hel_switch","Expect Helicity switch to be set to local mode (not fatal)");
		  sprintf(alarm_msg,"set initial helicity state.");
		  status = write_alarm(nalarm,alarm_msg); // set alarm message
		  return SUCCESS; 
		}    
	    }
	}
      else
	{
	  sprintf(msg,"Cannot read line from file %s",filename);
	  goto err;
	}
    }
  else
    {
      sprintf(msg,"Cannot open file %s",filename);
      goto err;
    }

  if(fin)fclose(fin);
  printf("check_epics_hel_switch: returning SUCCESS\n");
  return SUCCESS;

 err:
  if(fin)
    fclose(fin);
  printf("check_epics_hel_switch: %s\n",msg);
  cm_msg(MERROR, "check_epics_hel_switch","%s",msg);
  cm_msg(MINFO,  "check_epics_hel_switch: User must %s ",alarm_msg);
  cm_msg(MINFO,  "check_epics_hel_switch",
	 "To disable this check, set \".../Hardware/enable Epics hel switch check\" to \"n\" ");
  printf("calling write_alarm with alarm_msg=%s\n",alarm_msg);
  status = write_alarm(nalarm,&alarm_msg[0]); // set alarm message
  strcpy(error,err_msg);
  return DB_INVALID_PARAM;
}

INT read_and_retry(char *cmd)
{
  INT i,status;
 // Add a retry because sometimes there is a timeout
  i=0;
  while (i < 2)
    {
      status = system(cmd);
      if(!status)
	return SUCCESS;

      i++;
      printf("Sleeping 2s then retrying\n");
      sleep(2); // retry
    }
  return FE_ERR_HW;
}




#endif // EPICS_ACCESS

/*------------------------------------------------------------------*/

INT write_alarm(INT ival, char *msg)
{
  INT size,status;
  char message[80];

  //  if(single_shot)
  //   return SUCCESS; // don't write alarm

  //  printf("write_alarm: ival=%d\n",ival);
  if(ival > 0) //  set alarm message
    {
      size=sizeof(message);
      strncpy(message,msg,80);
            printf("write_alarm: message=\"%s\"\n",message);
      status = db_set_value(hDB, 0, "/Alarms/alarms/epics access/Alarm Message",message,sizeof(message),
			1,TID_STRING);
      if (status != SUCCESS)
	{
	  cm_msg(MERROR,"set_alarm",
		 "Error setting \"/Alarm/alarms/epics access/Alarm Message\" to %s(%d)",
		 message,status);
	}
    } // end of ival > 0

  //printf("write_alarm: ival=%d\n",ival);
  size=sizeof(ival);
  status = db_set_value(hDB, 0, "/Equipment/fifo_acq/client flags/epics access alarm", 
			&ival, size, 1, TID_INT);
  if (status != SUCCESS)
    {
      cm_msg(MERROR,"set_alarm",
	    "Error setting \"/Equipment/fifo_acq/client flags/epics access alarm\" to %d(%d)",
	     ival,status);
    }
  printf("write_alarm: sleeping...\n");
  sleep(50);

  return SUCCESS;
}
/*------------------------------------------------------------------*/
INT tr_stop(INT run_number, char *error)
{
  INT status;
  status = write_alarm(0," "); /* clear alarms */
  status = db_set_value(hDB, 0, "/custom/hidden/ppgtype last run",&expt_type, sizeof(expt_type), 1 , TID_INT); // write this for custom page
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "tr_stop", "cannot write %d to \"/custom/hidden/ppgtype last run\" (%d) ",expt_type,status);
      return status;
    }
  return status;
}
/*------------------------------------------------------------------*/
INT get_profile_index(char *p_profile_code, INT *pindex)
{
  /* get the array index depending on profile (1f,3f,5f or fREF) 
   */
  p_profile_code[0]=tolower(p_profile_code[0]);
 
  switch (p_profile_code[0])
    {
    case '1':
      *pindex=0;
      break;
    case '3':
      *pindex=1;
      break;
    case '5':
      *pindex=2;
      break;            
    case 'f':
      *pindex=3;
      break;
    default:
      printf("Register profile (%s) must be one of 1f,3f,5f or fREF\n",p_profile_code);
      return (-1);
    }
  return(SUCCESS);
}

/*------------------------------------------------------------------*/
BOOL check_sis_test_mode(void)
{
  BOOL test_mode; // flag

  test_mode=0;

#ifdef HAVE_SIS3801
  if( ppg.sis_test_mode.sis3801.test_mode) 
    test_mode=1;
#endif // HAVE_SIS3801

#ifdef HAVE_SIS3820
  if(ppg.hardware.sis3820.sis_mode == 0) // internal SIS3820 test mode
    test_mode =1;  /* sis3820 internal  test mode  */  
#endif

  // if(debug)
    if(test_mode)
      printf("check_sis_test_mode: SIS test mode is enabled\n"); 
  return test_mode;
} 

INT tr_poststart(INT run_number, char *error)
{
  INT   status;

  char filename[128];
 
  // Save input parameters to a file _last.odb (see lasttune)
  sprintf(filename,"/home/%s/%s/tunes/mode_%s/%s.odb",Expt_name,getenv("ONLINE"),ppg.input.experiment_name,lasttune);
  printf("\n tr_poststart starting with copy_last_params_flag=%d and filename=%s\n",copy_last_params_flag,filename);
  
  status = db_save(hDB, hParam, filename, FALSE);
  if(status!= SUCCESS)
    printf("tr_poststart: error saving input parameters in file %s (%d)\n",filename,status);
  else
    printf("tr_poststart: input parameters saved in file %s \n",filename);
  
  
  // Modes that (may) use PSM  1a,1b,1f,1g, SLR,2a,2b,2c,2d,2e
 
  if( (strncmp(ppg.input.experiment_name, "10",2)== 0) ||
      (strncmp(ppg.input.experiment_name, "1c",2)== 0) ||
      (strncmp(ppg.input.experiment_name, "1j",2)== 0) ||
      (strncmp(ppg.input.experiment_name, "1n",2)== 0))
    {
      printf("PPG Mode %s - no RF needed, not saving psm parameters\n",ppg.input.experiment_name);
      return SUCCESS;
    }
    
       
  // Save PSM parameters to a file __last_psm.odb (see lasttune)  These will be loaded when mode is changed by change_mode.pl
  sprintf(filename,"/home/%s/%s/tunes/mode_%s/%s_psm.odb",Expt_name,getenv("ONLINE"),ppg.input.experiment_name,lasttune);
  printf("\n tr_poststart starting with copy_last_params_flag=%d and filename=%s\n", copy_last_params_flag,filename);
  
  status = db_save(hDB, hPSM, filename, FALSE);
  if(status != SUCCESS)
    printf("tr_poststart: error saving psm parameters in file %s (%d)\n",filename,status);
  else
    printf("tr_poststart: psm parameters saved in file %s \n",filename);


 
  return SUCCESS;
}   

int TwosComp_convert(int value, int twos_comp)
{
 /*  Convert data into twos complement and back (10 bit data)
     for the PSM (not true twos compliment)

     twos_comp = FALSE -> converts data into 2s Compliment
     twos_comp = TRUE  -> converts data out of 2s Compliment
 
   Register is 10 bits.  
     Valid input data can be from -511 to +512 to convert to 2s compliment.
     2's compliment data is from 0 to 1023 

            Example:
     Input        2s compliment
     512             512
     511             511
     480             480
      30              30
       1               1
       0               0
      -1            1023
     -30             994
    -254             770
    -511             513   

  */
  int pdd=0;
  int IQ,iq;
  if(pdd)printf("TwosComp_convert: starting with value=%d twos_comp=%d\n",value,twos_comp);

  if(! twos_comp)
    {
      if(pdd)printf("TwosComp_convert: convert data to twos_comp \n");
      IQ=value;     

      if(IQ > 512 || IQ < -511) 
	{
	  printf("TwosComp_convert: Data is out of range for 10 bits (-512<IQ<512) \n");
	  return -1;    ; 
	}
      if(IQ > 0)
	iq=IQ;
      else
	{
          IQ*=-1;
	  iq=(~IQ +1) & I_Q_DATA_MASK ;
	}
      if(pdd)
	printf("*** TwosComp_convert: Data (%d 0x%x) converted to 2's complement and masked = %d 0x%x\n",IQ,IQ,iq,iq);
      return iq;
    }

  else
    {
      if(pdd)printf("TwosComp_convert: converting data back from twos_comp\n");
      iq=value;
      if(iq>1023 || iq < 0)
	{
	  printf("TwosComp_convert: 2s complement data (%d) is out of range for 10 bits (-1<iq<1024) \n",iq);
	  return -1024; 
	}
	
       if(iq<=512)
      //	IQ=~(iq)+1;
	 IQ=iq;
      else
	{
	  IQ=~(iq-1);
	  IQ = IQ & I_Q_DATA_MASK ;
	  IQ*=-1;
	}
      if(pdd)
	printf("TwosComp_convert: Data (%d 0x%x) converted back from 2's complement = %d 0x%x\n",iq,iq,IQ,IQ);
      return IQ;
    }
}

/*------------------------------------------------------------------*/
int main(int argc,char **argv)
{
  INT    status, stat1; 
  DWORD  i;
  HNDLE  hKey;
  char   host_name[HOST_NAME_LENGTH], expt_name[HOST_NAME_LENGTH];
  char   svpath[64], info[128];
  INT    msg, ch=0;
  BOOL   daemon;
  FIFO_ACQ_MDARC_STR(fifo_acq_mdarc_str); /* for mdarc (from experim.h) */
  FIFO_ACQ_FRONTEND_STR(fifo_acq_frontend_str); /* for "frontend" area (from experim.h) */
  char str[128];
  INT size;
 

  daemon = FALSE;

#ifdef BNMR
  printf("rf_config: This version is for BNMR");
#endif

#ifdef BNQR
  printf("rf_config: This version is for BNQR");
#endif


#ifdef CAMP
  printf(" with CAMP access\n");
#else // not CAMP
  printf("\n");
#endif // CAMP

  /* set default */
 cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);
   /* initialize global variables */
  sprintf(eqp_name,"FIFO_acq"); 

  single_shot=FALSE;
  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
  {
    if (argv[i][0] == '-' && argv[i][1] == 'd')
      debug = TRUE;
    else if (argv[i][0] == '-' && argv[i][1] == 'D')
      daemon = TRUE;
    else if (argv[i][0] == '-' && argv[i][1] == 's')
	single_shot=TRUE;
    else if (argv[i][0] == '-' && argv[i][1] == 'q')
      temp_set_quad=1; // temporarily set single into quad mode

    else if (argv[i][0] == '-')
    {
      if (i+1 >= argc || argv[i+1][0] == '-')
	goto usage;
      if (strncmp(argv[i],"-f",2) == 0)
	strcpy(svpath, argv[++i]);
      else if (strncmp(argv[i],"-e",2) == 0)
	strcpy(expt_name, argv[++i]);
      else if (strncmp(argv[i],"-h",2)==0)
	strcpy(host_name, argv[++i]);
    }
    else
    {
   usage:
      printf("usage: rf_config -d (debug) -s (single loop) \n");
      printf("             [-h Hostname] [-e Experiment] [-D] \n\n");
      return 0;
    }
  }
  
  if (daemon)
    {
      printf("Becoming a daemon...\n");
      ss_daemon_init(FALSE);
    }
  /* connect to experiment */
  status = cm_connect_experiment(host_name, expt_name, "rf_config", 0);
  if (status != CM_SUCCESS)
    return status;
  strcpy(Expt_name,expt_name);
#ifdef _DEBUG
  cm_set_watchdog_params(TRUE, 0);
#endif
  
  /* turn off message display, turn on message logging */
  cm_set_msg_print(MT_ALL, 0, NULL);
  
  /* register transition callbacks 
     frontend start is 500,  frontend stop is 500 */


 if(cm_register_transition(TR_START, tr_precheck, 350) != CM_SUCCESS)
    {
      cm_msg(MERROR, "main", "cannot register to transition for tr_precheck");
      goto error;
    }

 if(cm_register_transition(TR_START, tr_prestart,400) != CM_SUCCESS)
    {
      cm_msg(MERROR, "main", "cannot register to transition for prestart");
      goto error;
    }


 if(cm_register_transition(TR_START, tr_poststart,600) != CM_SUCCESS)
    {
      cm_msg(MERROR, "main", "cannot register to transition for poststart");
      goto error;
    }

 /* Dec 2013 I don't think we need this any more. Newer midas stops run from starting on error */
 
 // if(cm_register_transition(TR_STARTABORT, tr_start_abort,900) != CM_SUCCESS)
 //   {
 //     cm_msg(MERROR, "main", "cannot register to transition for start abort");
 //   }

 if(cm_register_transition(TR_STOP, tr_stop, 500) != CM_SUCCESS)
    {
      cm_msg(MERROR, "main", "cannot register to transition for stop");
      goto error;
    }


  /* connect to the database */
  cm_get_experiment_database(&hDB, &hKey);
  
  /* Check if the structure "frontend"  exists */
  sprintf(str,"/Equipment/%s/frontend",eqp_name); 
  status = db_find_key(hDB,0,"equipment/FIFO_Acq/frontend", &hFiS);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("main: can't find key Equipment/FIFO_Acq/frontend");
      
      /* So try to create record */
      
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(fifo_acq_frontend_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"main","Could not create frontend record (%d)\n",status);
	}
      else
	if (debug)printf("Success from create record for %s\n",str);
      
      /* Check again if the structure "frontend"  exists  */
      status = db_find_key(hDB,0,"equipment/FIFO_Acq/frontend", &hFiS);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"main","Equipment/FIFO_Acq/frontend  not present");
	  goto error;
	}
    }
  else
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hFiS, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "main", "error during get_record_size (%d)",(int)status);
	  return status;
	}
      printf("Size of sis saved structure: %d, size of sis record: %d\n", (int)sizeof(FIFO_ACQ_FRONTEND) ,size);
      if (sizeof(FIFO_ACQ_FRONTEND) != size) {
	/* create record */
	cm_msg(MINFO,"main","creating record (frontend); mismatch between size of structure (%d) & record size (%d)", (int)sizeof(FIFO_ACQ_FRONTEND) ,size); 
	status = db_create_record(hDB, 0, str , strcomb(fifo_acq_frontend_str));
	if (status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"main","Could not create frontend record (%d)\n",status);
	    return status;
	  }
	else
	  if (debug)printf("Success from create record for %s\n",str);
      }
    }


  status =param_create_rec();  // create record for the parameter mode storage area
  if(status != SUCCESS)
    return status;



  /* get the key hMdarc  */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name); 
  status = db_find_key(hDB, 0, str, &hMdarc);
  if (status != DB_SUCCESS)
    {
      if(debug) printf("main: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(fifo_acq_mdarc_str));
      if (status != DB_SUCCESS)
	{
	  if(debug)printf("main: Failure creating mdarc record. Checking if mdarc is running...\n");
	  stat1 = cm_exist("mdarc",FALSE) ;
	  if(stat1 == CM_SUCCESS)	  /* mdarc is already running */
	    {
	      /* how come we can't find the mdarc area if mdarc is running? This shouldn't happen */
	      if(debug)printf("mdarc IS running \n");	
	      cm_msg(MINFO,"main","Data archiver mdarc is running; mdarc record could not be created\n");
	    }
	  else
	    cm_msg(MINFO,"main","Could not create record for %s  (%d); (mdarc not running)", str,status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
      /* try again to get the key hMdarc  */
      status = db_find_key(hDB, 0, str, &hMdarc);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"main","Failed to get the key %s ",str);
	  goto error;
	}
    }    
  else  /* key mdarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMdarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "main", "error during get_record_size (%d) for mdarc",(int)status);
	  return status;
	}
      printf("Size of mdarc saved structure: %d, size of mdarc record: %d\n", (int)sizeof(FIFO_ACQ_MDARC) ,size);
      if (sizeof(FIFO_ACQ_MDARC) != size) {
	cm_msg(MINFO,"main","creating record (mdarc); mismatch between size of structure (%d) & record size (%d)", (int)sizeof(FIFO_ACQ_MDARC) ,size);
	/* create record */
	status = db_create_record(hDB, 0, str , strcomb(fifo_acq_mdarc_str));
	if (status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"rf_config","Could not create mdarc record (%d)",status);
	    printf("rf_config: error - could not create mdarc record  (%d)\n",status);
	    return status;
	  }
	else
	  if (debug)printf("Success from create record for %s\n",str);
      }
    }

  sprintf(comp_path,"/home/%s/%s/rf_config",expt_name,getenv("ONLINE") );
  printf("comp path = %s\n",comp_path); // /home/bn[mq]r/online/rf_config

#ifdef CAMP
  /* Create or Get record for the camp tree for BNMR & BNQR  */
  hCamp=0;
  /* now add pol so it can use camp as well */
  if (strcmp(expt_name,"bnmr")==0 || strcmp(expt_name,"bnqr")==0  || strcmp(expt_name,"pol")==0   )
    {
      if(debug)printf("Experiment %s detected; accessing camp record\n",expt_name);
      status = camp_create_rec();
      if(status != SUCCESS) goto error;
    }
  else
    {
      cm_msg(MINFO,"main","experiment \"%s\" does not support CAMP. Camp record not created",expt_name);
    printf("main: *** CAMP is defined but unexpected experiment name found (%s)\n",expt_name);
    printf("          Camp record not created\n");
    }
#endif    
  
  /* check if Input tree is available */
  status = db_find_key(hDB, hFiS, "Input", &hIn);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find Input record");
      goto error;
    }
 
 /* check if output tree is available */
  status = db_find_key(hDB, hFiS, "Output", &hOut);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find Output record");
      goto error;
    }
 /* check if sis test mode  tree is available */
  status = db_find_key(hDB, hFiS, "sis test mode", &hTest);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find sis test mode record");
      goto error;
    }
 /* check if hardware  tree is available */
  status = db_find_key(hDB, hFiS, "hardware", &hH);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find hardware record");
      goto error;
    }

#ifdef PSM
 /* check if hardware/psm  tree is available */
  status = db_find_key(hDB, hFiS, "hardware/psm", &hPSM);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find hardware/psm record");
      goto error;
    }

#endif

  /* check if mdarc/histograms tree is available */
  status = db_find_key(hDB, hMdarc, "Histograms", &hHis);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find Histograms record");
      goto error;
    }

 // get the key hClient for client flags
  sprintf(client_str, "equipment/%s/client flags",eqp_name );  // global
  status = db_find_key(hDB,0,client_str, &hClient);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","%s  not present",client_str);
      return status;
    }

   if (single_shot)    
     {         /* single sweep */

       status = tr_precheck(10, info); 
       if (status != SUCCESS)
	 goto error;

      status = tr_prestart(10, info); 
      if (status  != SUCCESS)
        goto error;

      status = tr_poststart(10, info); 
      if (status  == SUCCESS)
	{
	  printf("Thank you\n");
	  goto exit; /* force exit */
	}
      else
	goto error; 
     }

  /* initialize ss_getchar() */
  ss_getchar(0);
  do
    {
      while (ss_kbhit())
	{
	  ch = ss_getchar(0);
      if (ch == -1)
	ch = getchar();
      if (ch == 'R')
	ss_clear_screen();
      if ((char) ch == '!')
	break;
    }
    msg = cm_yield(1000);
  } while (msg != RPC_SHUTDOWN && msg != SS_ABORT && ch != '!');
  

  printf("Thank you\n");
  goto exit;
 
 error:
  printf("\n Error detected. Check odb messages for details\n");
  
 exit:


  /* reset terminal */
  ss_getchar(TRUE);
  
  cm_disconnect_experiment();
  return SUCCESS;
}

