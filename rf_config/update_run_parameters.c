/*   update_run_parameters.c 
     
     Copies mode parameters set by users with custom page "parameters.html" into the input and hardware areas
     PSM parameters are set directly into PSM area. 
 */

INT get_param_key(char *ppg_mode)
{
  INT status;
  char mode_name[32];
  hParam = 0;

  sprintf(mode_name, "Mode %s",ppg_mode);

 /* get ppg parameter settings key for this ppg mode */

  /* get the key hParam */
  status = db_find_key(hDB, hMP, mode_name, &hParam);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"get_param_key","can't find \"%s\" parameter record",mode_name);
      return (status);
    }

  if(hParam <= 0)
    {
      cm_msg(MERROR,"get_param_key","error...   key hParam is invalid (%d)", hParam);
      return DB_INVALID_PARAM;
    }

  return status;
}

INT  copy_10(char *ppg_mode)
{
  INT status,size;

  printf("copy_10: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_10: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_10);
  status = db_get_record (hDB, hParam, &mode_param.mode_10, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_10","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_10: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  ppg.input.e1f_num_dwell_times =  mode_param.mode_10.number_of_bins ;
  ppg.input.mcs_enable_gate__ms_ = mode_param.mode_10.mcs_enable_gate__ms_;


  ppg.hardware.enable_helicity_flipping = mode_param.mode_10.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ = mode_param.mode_10.helicity_sleep_time__ms_;
  
  ppg.input.num_cycles_per_supercycle =  mode_param.mode_10.num_cycles_per_scan_incr; 
  
  // psm
#ifdef PSMIII
  ppg.hardware.psm.f0ch1.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch2.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch3.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch4.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f1.channel_enabled = 0;  // off for this mode

#else // PSM,PSMII
  ppg.hardware.psm.one_f.profile_enabled = 0;  // off for this mode
  ppg.hardware.psm.fref.profile_enabled = 0;  // off for this mode
#endif  
  
  
  /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_10","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_10:  success setting the record for input area for ppg mode %s\n",ppg.input.experiment_name);


  /* In this mode (PSM OFF) user will not have changed any values so can set record for hardware
     set the record for Hardware (with key hH) */
  size = sizeof(ppg.hardware);
  status = db_set_record (hDB, hH, &ppg.hardware, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_10","Failed to set Hardware record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_10:  success setting the record Hardware for ppg mode %s\n",ppg.input.experiment_name);

  return SUCCESS;
}



INT  copy_1a(char *ppg_mode)
{  
  INT status,size;

  printf("copy_1a: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_1a: error from get_param_key\n");
      return status;
    }

  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_1a);
  status = db_get_record (hDB, hParam, &mode_param.mode_1a, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1a","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_1a: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  strncpy(ppg.input.e1a_and_e1b_pulse_pairs, mode_param.mode_1a.pulse_pairs, 3); // char[4]
  strncpy(ppg.input.e1a_and_e1b_freq_mode, mode_param.mode_1a.frequency_mode, 2); // char[3]

  ppg.input.beam_mode             =  mode_param.mode_1a.beam_mode; // char 
  ppg.input.bg_delay__ms_         =  mode_param.mode_1a.background_delay__ms_;
  ppg.input.rf_delay__ms_         =  mode_param.mode_1a.rf_delay__ms_;
  ppg.input.rf_on_time__ms_       =  mode_param.mode_1a.rf_on_time__ms_;
  ppg.input.rf_off_time__ms_      =  mode_param.mode_1a.rf_off_time__ms_;
  ppg.input.mcs_enable_gate__ms_  =  mode_param.mode_1a.mcs_enable_gate__ms_;
  ppg.input.mcs_enable_delay__ms_ =  mode_param.mode_1a.mcs_enable_delay__ms_;
  ppg.input.num_rf_cycles         =  mode_param.mode_1a.number_of_rf_cycles;

  // helicity
  ppg.hardware.enable_helicity_flipping   = mode_param.mode_1a.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ = mode_param.mode_1a.helicity_sleep_time__ms_;
  
  ppg.input.num_cycles_per_supercycle     = mode_param.mode_1a.num_cycles_per_scan_incr; 
 
  // frequency
  ppg.input.frequency_start__hz_     = mode_param.mode_1a.frequency_scan_start__hz_;
  ppg.input.frequency_stop__hz_      = mode_param.mode_1a.frequency_scan_stop__hz_;
  ppg.input.frequency_increment__hz_ = mode_param.mode_1a.frequency_scan_increment__hz_;
  ppg.input.randomize_freq_values    = mode_param.mode_1a.randomize_freq_scan_increments;


    /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1a","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1a: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);

  // cannot set record for hardware, PSM params may have been changed so use update_hardware_params(type) instead
  status =  update_hardware_params(1);  // param=1 for Type 1  
  return status;
}

INT  copy_1b(char *ppg_mode)
{

  INT status,size;

  printf("copy_1b: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_1b: error from get_param_key\n");
      return status;
    }

  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_1b);
  status = db_get_record (hDB, hParam, &mode_param.mode_1b, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1b","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_1b: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  strncpy(ppg.input.e1a_and_e1b_pulse_pairs, mode_param.mode_1b.pulse_pairs, 3); // char[4]
  strncpy(ppg.input.e1a_and_e1b_freq_mode, mode_param.mode_1b.frequency_mode, 2); // char[3]

  ppg.input.beam_mode             =  mode_param.mode_1b.beam_mode; // char 
  ppg.input.bg_delay__ms_         =  mode_param.mode_1b.background_delay__ms_;
  ppg.input.rf_delay__ms_         =  mode_param.mode_1b.rf_delay__ms_;
  ppg.input.rf_on_time__ms_       =  mode_param.mode_1b.rf_on_time__ms_;
  ppg.input.rf_off_time__ms_      =  mode_param.mode_1b.rf_off_time__ms_;
  ppg.input.mcs_enable_gate__ms_  =  mode_param.mode_1b.mcs_enable_gate__ms_;
  ppg.input.mcs_enable_delay__ms_ =  mode_param.mode_1b.mcs_enable_delay__ms_;
  ppg.input.num_rf_cycles         =  mode_param.mode_1b.number_of_rf_cycles;
  ppg.input.e1b_dwell_time__ms_   =  mode_param.mode_1b.dwell_time__ms_;

  // helicity
  ppg.hardware.enable_helicity_flipping   = mode_param.mode_1b.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ = mode_param.mode_1b.helicity_sleep_time__ms_;
  
  ppg.input.num_cycles_per_supercycle     = mode_param.mode_1b.num_cycles_per_scan_incr; 
 
  // frequency
  ppg.input.frequency_start__hz_     = mode_param.mode_1b.frequency_scan_start__hz_;
  ppg.input.frequency_stop__hz_      = mode_param.mode_1b.frequency_scan_stop__hz_;
  ppg.input.frequency_increment__hz_ = mode_param.mode_1b.frequency_scan_increment__hz_;
  ppg.input.randomize_freq_values    = mode_param.mode_1b.randomize_freq_scan_increments;


    /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1b","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1b: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);

  // cannot set record for hardware, PSM params may have been changed so use update_hardware_params(type) instead
  status =  update_hardware_params(1);  // param=1 for Type 1  
  return status;
 
}
INT  copy_1c(char *ppg_mode)
{
  INT status,size;

  printf("copy_1c: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_1c: error from get_param_key\n");
      return status;
    }

  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_1c);
  status = db_get_record (hDB, hParam, &mode_param.mode_1c, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1c","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_1c: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area
  ppg.input.e1f_num_dwell_times =  mode_param.mode_1c.number_of_bins ;
  ppg.input.mcs_enable_gate__ms_ = mode_param.mode_1c.mcs_enable_gate__ms_;

  ppg.input.e1c_camp_start  =  mode_param.mode_1c.camp_start_scan;
  ppg.input.e1c_camp_stop   =  mode_param.mode_1c.camp_stop_scan;
  ppg.input.e1c_camp_inc    =  mode_param.mode_1c.camp_increment;

#ifdef CAMP
  // update parameters in /equipment/fifo_acq/camp sweep device/
  strncpy(fcamp.camp_path,   mode_param.mode_1c.camp_path, 32);
  strncpy( fcamp.instrument_type, mode_param.mode_1c.instrument_type, 32);
  strncpy( fcamp.gpib_port_or_rs232_portname, mode_param.mode_1c.gpib_port_or_rs232_portname, 10);
  strncpy( fcamp.camp_scan_path, mode_param.mode_1c.camp_scan_path,80); 
  strncpy( fcamp.scan_units, mode_param.mode_1c.scan_units,9);
  fcamp.maximum= mode_param.mode_1c.maximum;
  fcamp.minimum= mode_param.mode_1c.minimum;
  fcamp.integer_conversion_factor= mode_param.mode_1c.integer_conversion_factor;
#endif

  // helicity
  ppg.hardware.enable_helicity_flipping   = mode_param.mode_1c.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ = mode_param.mode_1c.helicity_sleep_time__ms_;
  
  ppg.input.num_cycles_per_supercycle     =  mode_param.mode_1c.num_cycles_per_scan_incr; 

  // psm
#ifdef PSMIII
  ppg.hardware.psm.f0ch1.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch2.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch3.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch4.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f1.channel_enabled = 0;  // off for this mode
#else // PSM,PSMII
  ppg.hardware.psm.one_f.profile_enabled = 0;  // off for this mode
  ppg.hardware.psm.fref.profile_enabled = 0;  // off for this mode
#endif 


#ifdef CAMP
 /* set the record for "camp sweep device" (with key hCamp) */
  size = sizeof(fcamp);
  status = db_set_record (hDB, hCamp, &fcamp, size, 0);/* set the record  */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1c","Failed to set camp sweep device record  (%d) size=%d ",status,size);
      return(status); /* error */
    }
  else
    if(debug)printf("copy_1c: success setting the record for camp\n");
#endif // CAMP  

  /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1c","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1c:  success setting the record for input area for ppg mode %s\n",ppg.input.experiment_name);


  /* In this mode (PSM OFF) user will not have changed any values so can set record for hardware
     set the record for Hardware (with key hH) */
  size = sizeof(ppg.hardware);
  status = db_set_record (hDB, hH, &ppg.hardware, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1c","Failed to set Hardware record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1c:  success setting the record Hardware for ppg mode %s\n",ppg.input.experiment_name);
  return status;
}

INT  copy_1f(char *ppg_mode)
{
  INT status,size;

  printf("copy_1f: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_1f: error from get_param_key\n");
      return status;
    }

  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_1f);
  status = db_get_record (hDB, hParam, &mode_param.mode_1f, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1f","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_1f: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  ppg.input.e1f_num_dwell_times = mode_param.mode_1f.number_of_bins; 
  ppg.input.mcs_enable_gate__ms_ = mode_param.mode_1f.mcs_enable_gate__ms_;

  
  ppg.hardware.enable_helicity_flipping = mode_param.mode_1f.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ = mode_param.mode_1f.helicity_sleep_time__ms_;
  
  ppg.input.num_cycles_per_supercycle =  mode_param.mode_1f.num_cycles_per_scan_incr; 
  

  // frequency
  ppg.input.frequency_start__hz_ = mode_param.mode_1f.frequency_scan_start__hz_;
  ppg.input.frequency_stop__hz_ = mode_param.mode_1f.frequency_scan_stop__hz_;
  ppg.input.frequency_increment__hz_ = mode_param.mode_1f.frequency_scan_increment__hz_;
  ppg.input.randomize_freq_values = mode_param.mode_1f.randomize_freq_scan_increments;

  // constant time between cycles mode
  ppg.input.e1f_const_time_between_cycles = mode_param.mode_1f.const__time_between_cycles;
  ppg.input.daq_service_time__ms_ = mode_param.mode_1f.daq_service_time_ms_;
#ifdef BNQR
#ifdef HAVE_SIS3820  // BNQR only 
  ppg.input.e1f_enable_alpha_histos = mode_param.mode_1f.enable_alpha_histograms;
#endif
#endif // BNQR 
  /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1f","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1f: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);

  status =  update_hardware_params(1);  // Type 1 cannot set record for hardware, PSM params may have been changed
  return status;

}



INT  copy_1g(char *ppg_mode)
{
  INT status,size;

  printf("copy_1g: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_1g: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_1g);
  status = db_get_record (hDB, hParam, &mode_param.mode_1g, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1g","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_1g: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area


  ppg.input.mcs_enable_gate__ms_       = mode_param.mode_1g.mcs_enable_gate__ms_;
  ppg.input.e00_prebeam_dwelltimes     = mode_param.mode_1g.number_of_prebeam_dwelltimes; 
  ppg.input.e00_beam_on_dwelltimes     = mode_param.mode_1g.number_of_beam_on_dwelltimes; 
  ppg.input.e00_beam_off_dwelltimes    = mode_param.mode_1g.number_of_beam_off_dwelltimes; 
  ppg.input.rfon_dwelltime             = mode_param.mode_1g.rfon_delay__dwelltimes_; 
  ppg.input.rfon_duration__dwelltimes_ = mode_param.mode_1g.rfon_duration__dwelltimes_; 
  
  ppg.hardware.enable_helicity_flipping = mode_param.mode_1g.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_1g.helicity_sleep_time__ms_;

  ppg.input.num_cycles_per_supercycle =  mode_param.mode_1g.num_cycles_per_scan_incr; 


  // frequency
  ppg.input.frequency_start__hz_     = mode_param.mode_1g.frequency_scan_start__hz_;
  ppg.input.frequency_stop__hz_      = mode_param.mode_1g.frequency_scan_stop__hz_;
  ppg.input.frequency_increment__hz_ = mode_param.mode_1g.frequency_scan_increment__hz_;
  ppg.input.randomize_freq_values    = mode_param.mode_1g.randomize_freq_scan_increments;

    /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1g","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1g: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);

  // cannot set record for hardware, PSM params may have been changed so use update_hardware_params(type) instead
  status =  update_hardware_params(1);  // param=1 for Type 1  


  return SUCCESS;
}

INT  copy_1j(char *ppg_mode)
{
  INT status,size;

  printf("copy_1j: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_1j: error from get_param_key\n");
      return status;
    }

  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_1j);
  status = db_get_record (hDB, hParam, &mode_param.mode_1j, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1j","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_1j: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area
  
  ppg.input.mcs_enable_gate__ms_       = mode_param.mode_1j.mcs_enable_gate__ms_;
  ppg.input.e00_prebeam_dwelltimes     = mode_param.mode_1j.number_of_prebeam_dwelltimes; 
  ppg.input.e00_beam_on_dwelltimes     = mode_param.mode_1j.number_of_beam_on_dwelltimes; 
  ppg.input.e00_beam_off_dwelltimes    = mode_param.mode_1j.number_of_beam_off_dwelltimes; 

  // CAMP device
  ppg.input.e1c_camp_start  =  mode_param.mode_1j.camp_start_scan;
  ppg.input.e1c_camp_stop   =  mode_param.mode_1j.camp_stop_scan; 
  ppg.input.e1c_camp_inc    =  mode_param.mode_1j.camp_increment; 
  
#ifdef CAMP
  // update parameters in /equipment/fifo_acq/camp sweep device/
  strncpy(fcamp.camp_path,   mode_param.mode_1j.camp_path, 32);
  strncpy( fcamp.instrument_type, mode_param.mode_1j.instrument_type, 32);
  strncpy( fcamp.gpib_port_or_rs232_portname, mode_param.mode_1j.gpib_port_or_rs232_portname, 10);
  strncpy( fcamp.camp_scan_path, mode_param.mode_1j.camp_scan_path,80); 
  strncpy( fcamp.scan_units, mode_param.mode_1j.scan_units,9);
  fcamp.maximum= mode_param.mode_1j.maximum;
  fcamp.minimum= mode_param.mode_1j.minimum;
  fcamp.integer_conversion_factor= mode_param.mode_1j.integer_conversion_factor;
#endif

  // helicity
  ppg.hardware.enable_helicity_flipping   = mode_param.mode_1j.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ = mode_param.mode_1j.helicity_sleep_time__ms_;
  
  ppg.input.num_cycles_per_supercycle     =  mode_param.mode_1j.num_cycles_per_scan_incr; 

  // psm
#ifdef PSMIII
  ppg.hardware.psm.f0ch1.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch2.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch3.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch4.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f1.channel_enabled = 0;  // off for this mode
#else // PSM,PSMII
  ppg.hardware.psm.one_f.profile_enabled = 0;  // off for this mode
  ppg.hardware.psm.fref.profile_enabled = 0;  // off for this mode
#endif 

#ifdef CAMP
 /* set the record for "camp sweep device" (with key hCamp) */
  size = sizeof(fcamp);
  status = db_set_record (hDB, hCamp, &fcamp, size, 0);/* set the record  */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1j","Failed to set camp sweep device record  (%d) size=%d ",status,size);
      return(status); /* error */
    }
  else
    if(debug)printf("copy_1j: success setting the record for camp\n");
#endif  


  /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1j","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1j:  success setting the record for input area for ppg mode %s\n",ppg.input.experiment_name);


  /* In this mode (PSM OFF) user will not have changed any values so can set record for hardware
     set the record for Hardware (with key hH) */
  size = sizeof(ppg.hardware);
  status = db_set_record (hDB, hH, &ppg.hardware, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1j","Failed to set Hardware record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1j:  success setting the record Hardware for ppg mode %s\n",ppg.input.experiment_name);

  return SUCCESS;
}

INT  copy_1n(char *ppg_mode)
{
  INT status,size;

  printf("copy_1n: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_1n: error from get_param_key\n");
      return status;
    }


  if (strcmp(ppg_mode, "1n") == 0) {
	  /* get the record for this mode (with key hParam) */
	  size = sizeof(mode_param.mode_1n);
	  status = db_get_record (hDB, hParam, &mode_param.mode_1n, &size, 0);/* get the record for this mode */
  } else if (strcmp(ppg_mode, "1e") == 0) {
	  /* get the record for this mode (with key hParam) */
	  size = sizeof(mode_param.mode_1e);
	  status = db_get_record (hDB, hParam, &mode_param.mode_1e, &size, 0);/* get the record for this mode */
  } else {
      cm_msg(MERROR,"copy_1n","Unexpected parameter mode %s",ppg_mode);

  }
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1n","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_1n: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  if (strcmp(ppg_mode, "1n") == 0) {
	  ppg.input.e1f_num_dwell_times = mode_param.mode_1n.number_of_bins;
	  ppg.input.mcs_enable_gate__ms_ = mode_param.mode_1n.mcs_enable_gate__ms_;


	  ppg.hardware.enable_helicity_flipping = mode_param.mode_1n.flip_helicity;
	  if(ppg.hardware.enable_helicity_flipping)
		ppg.hardware.helicity_flip_sleep__ms_ = mode_param.mode_1n.helicity_sleep_time__ms_;

	  ppg.input.num_cycles_per_supercycle =  mode_param.mode_1n.num_cycles_per_scan_incr;

	  // NaCell parameters
	  ppg.input.navolt_start = mode_param.mode_1n.start_nacell_scan__volts_;
	  ppg.input.navolt_stop = mode_param.mode_1n.stop_nacell_scan__volts_;
	  ppg.input.navolt_inc = mode_param.mode_1n.nacell_increment__volts_;
  } else if (strcmp(ppg_mode, "1e") == 0) {
	  ppg.input.e1f_num_dwell_times = mode_param.mode_1e.number_of_bins;
	  ppg.input.mcs_enable_gate__ms_ = mode_param.mode_1e.mcs_enable_gate__ms_;


	  ppg.hardware.enable_helicity_flipping = mode_param.mode_1e.flip_helicity;
	  if(ppg.hardware.enable_helicity_flipping)
		ppg.hardware.helicity_flip_sleep__ms_ = mode_param.mode_1e.helicity_sleep_time__ms_;

	  ppg.input.num_cycles_per_supercycle =  mode_param.mode_1e.num_cycles_per_scan_incr;

	  // Field parameters
	  ppg.input.field_start = mode_param.mode_1e.start_field_scan__gauss_;
	  ppg.input.field_stop = mode_param.mode_1e.stop_field_scan__gauss_;
	  ppg.input.field_inc = mode_param.mode_1e.field_increment__gauss_;
  }
  
  // psm
#ifdef PSMIII
  ppg.hardware.psm.f0ch1.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch2.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch3.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f0ch4.channel_enabled = 0;  // off for this mode
  ppg.hardware.psm.f1.channel_enabled = 0;  // off for this mode
#else // PSM,PSMII
  ppg.hardware.psm.one_f.profile_enabled = 0;  // off for this mode
  ppg.hardware.psm.fref.profile_enabled = 0;  // off for this mode
#endif 

 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1n","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1n: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);

  /* set the record for Hardware  (no PSM this mode) */
  size = sizeof(ppg.hardware);
  status = db_set_record (hDB, hH, &ppg.hardware, size, 0);/* set the record for Hardware */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_1n","Failed to set Hardware record  (ppg mode %s)  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_1n: success setting the Hardware record (ppg mode %s)\n",ppg.input.experiment_name);

  return SUCCESS;
}

// Type 2

INT  copy_20(char *ppg_mode)
{
  INT status,size;

  printf("copy_20: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_20: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_20);
  status = db_get_record (hDB, hParam, &mode_param.mode_20, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_20","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_20: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  ppg.input.mcs_enable_gate__ms_ = mode_param.mode_20.mcs_enable_gate__ms_;
  ppg.input.e00_prebeam_dwelltimes = mode_param.mode_20.number_of_prebeam_dwelltimes; 
  ppg.input.e00_beam_on_dwelltimes = mode_param.mode_20.number_of_beam_on_dwelltimes; 
  ppg.input.e00_beam_off_dwelltimes = mode_param.mode_20.number_of_beam_off_dwelltimes; 
  ppg.input.rfon_dwelltime = mode_param.mode_20.rfon_delay__dwelltimes_; 
  ppg.input.rfon_duration__dwelltimes_ = mode_param.mode_20.rfon_duration__dwelltimes_; 
  
  ppg.hardware.enable_helicity_flipping = mode_param.mode_20.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_20.helicity_sleep_time__ms_;

  ppg.hardware.enable_sampleref_mode   =  mode_param.mode_20.enable_sample_reference_mode;

 
 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_20","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_20: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);



  // currently only mode 20 uses sample/reference mode, so copy it here
  // (not included in update_hardware_params)
  char str[64];
  size = sizeof(ppg.hardware.enable_sampleref_mode);  // assigned above
  sprintf(str,"enable sampleref mode");
  status = db_set_value (hDB, hH, str, &ppg.hardware.enable_sampleref_mode, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_20","Failed to set  \"%s\"  (%d) size=%d ",str,status,size);
      return(status); /* error */
    }
  else
    printf("copy_20: success setting %s\n",str);
    

  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;


}
INT  copy_2a(char *ppg_mode)
{
    INT status,size;

  printf("copy_2a: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_2a: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_2a);
  status = db_get_record (hDB, hParam, &mode_param.mode_2a, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2a","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_2a: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  ppg.input.mcs_enable_gate__ms_           =  mode_param.mode_2a.mcs_enable_gate__ms_;
  ppg.input.mcs_enable_delay__ms_          =  mode_param.mode_2a.mcs_enable_delay__ms_;
  ppg.input.rf_on_time__ms_                =  mode_param.mode_2a.rf_on_time__ms_;
  ppg.input.rf_off_time__ms_               =  mode_param.mode_2a.rf_off_time__ms_;
  ppg.input.num_rf_on_delays__dwell_times_ =  mode_param.mode_2a.num_rf_on_delays__dwelltimes_;
  ppg.input.beam_off_time__ms_             =  mode_param.mode_2a.beam_off_time__ms_;
  ppg.input.num_beam_precycles             =  mode_param.mode_2a.number_of_beam_precycles;
  ppg.input.e2a_180                        =  mode_param.mode_2a.enable_180;
  ppg.input.e2a_pulse_pairs                =  mode_param.mode_2a.enable_pulse_pairs;
  strncpy(ppg.input.e2a_ubit1_action, mode_param.mode_2a.bin_parameter,10);


  // helicity
  ppg.hardware.enable_helicity_flipping = mode_param.mode_2a.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_2a.helicity_sleep_time__ms_;

  // frequency
  ppg.input.frequency_start__hz_     = mode_param.mode_2a.frequency_scan_start__hz_;
  ppg.input.frequency_stop__hz_      = mode_param.mode_2a.frequency_scan_stop__hz_;
  ppg.input.frequency_increment__hz_ = mode_param.mode_2a.frequency_scan_increment__hz_;
  ppg.input.randomize_freq_values    = mode_param.mode_2a.randomize_freq_scan_increments;

 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2a","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_2a: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);


  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;


}

INT  copy_2b(char *ppg_mode)
{
   INT status,size;

  printf("copy_2b: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_2b: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_2b);
  status = db_get_record (hDB, hParam, &mode_param.mode_2b, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2b","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_2b: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  ppg.input.mcs_enable_gate__ms_           =  mode_param.mode_2b.mcs_enable_gate__ms_;
  ppg.input.mcs_enable_delay__ms_          =  mode_param.mode_2b.mcs_enable_delay__ms_;
  ppg.input.rf_on_time__ms_                =  mode_param.mode_2b.rf_on_time__ms_;
  ppg.input.rf_off_time__ms_               =  mode_param.mode_2b.rf_off_time__ms_;
  ppg.input.num_rf_on_delays__dwell_times_ =  mode_param.mode_2b.num_rf_on_delays__dwelltimes_;
  ppg.input.beam_off_time__ms_             =  mode_param.mode_2b.beam_off_time__ms_;
  ppg.input.num_beam_precycles             =  mode_param.mode_2b.number_of_beam_precycles;
  ppg.input.e2b_num_beam_on_dwell_times    =  mode_param.mode_2b.number_of_beam_on_dwelltimes;
 


  // helicity
  ppg.hardware.enable_helicity_flipping = mode_param.mode_2b.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_2b.helicity_sleep_time__ms_;

  // frequency
  ppg.input.frequency_start__hz_     = mode_param.mode_2b.frequency_scan_start__hz_;
  ppg.input.frequency_stop__hz_      = mode_param.mode_2b.frequency_scan_stop__hz_;
  ppg.input.frequency_increment__hz_ = mode_param.mode_2b.frequency_scan_increment__hz_;
  ppg.input.randomize_freq_values    = mode_param.mode_2b.randomize_freq_scan_increments;

 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2b","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_2b: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);


  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;



}
INT  copy_2c(char *ppg_mode)
{
   INT status,size;

  printf("copy_2c: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_2c: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_2c);
  status = db_get_record (hDB, hParam, &mode_param.mode_2c, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2c","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_2c: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  ppg.input.beam_mode                      =  mode_param.mode_2c.beam_mode; // char
  ppg.input.mcs_enable_gate__ms_           =  mode_param.mode_2c.mcs_enable_gate__ms_;
  ppg.input.mcs_enable_delay__ms_          =  mode_param.mode_2c.mcs_enable_delay__ms_;
  ppg.input.num_rf_on_delays__dwell_times_ =  mode_param.mode_2c.num_rf_on_delays__dwelltimes_;
  ppg.input.beam_off_time__ms_             =  mode_param.mode_2c.beam_off_time__ms_;
  ppg.input.prebeam_on_time__ms_           =  mode_param.mode_2c.prebeam_on_time__ms_;
  ppg.input.e2c_beam_on_time__ms_          =  mode_param.mode_2c.beam_on_time__ms_;
  ppg.input.f_select_pulselength__ms_      =  mode_param.mode_2c.rf_on_time__ms_;
  ppg.input.num_freq_slices                =  mode_param.mode_2c.number_of_frequency_slices;
  ppg.input.freq_single_slice_width__hz_   =  mode_param.mode_2c.freq_single_slice_width__hz_;
  ppg.input.f_slice_internal_delay__ms_    =  mode_param.mode_2c.freq_single_slice_int_delay_ms_;
  ppg.input.flip_180_delay__ms_            =  mode_param.mode_2c.flip_180_delay__ms_;
  ppg.input.flip_360_delay__ms_            =  mode_param.mode_2c.flip_360_delay__ms_;
  ppg.input.counting_mode                  =  mode_param.mode_2c.counting_mode;  // char
  

  // helicity
  ppg.hardware.enable_helicity_flipping = mode_param.mode_2c.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_2c.helicity_sleep_time__ms_;

  // frequency
  ppg.input.frequency_start__hz_     = mode_param.mode_2c.frequency_scan_start__hz_;
  ppg.input.frequency_stop__hz_      = mode_param.mode_2c.frequency_scan_stop__hz_;
  ppg.input.frequency_increment__hz_ = mode_param.mode_2c.frequency_scan_increment__hz_;
  ppg.input.randomize_freq_values    = mode_param.mode_2c.randomize_freq_scan_increments;

 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2c","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_2c: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);


  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;
}

INT  copy_2d(char *ppg_mode)
{

   INT status,size;

  printf("copy_2d: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_2d: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_2d);
  status = db_get_record (hDB, hParam, &mode_param.mode_2d, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2d","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_2d: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area
    strncpy(ppg.input.e1a_and_e1b_pulse_pairs, mode_param.mode_2d.pulse_pairs, 3); // char[4]
    strncpy(ppg.input.e1a_and_e1b_freq_mode, mode_param.mode_2d.frequency_mode, 2); // char[3]

     ppg.input.beam_mode                      =  mode_param.mode_2d.beam_mode; // char
     ppg.input.bg_delay__ms_         =  mode_param.mode_2d.background_delay__ms_;
     ppg.input.rf_delay__ms_         =  mode_param.mode_2d.rf_delay__ms_;
     ppg.input.num_rf_cycles         =  mode_param.mode_2d.number_of_rf_cycles;
     ppg.input.rf_on_time__ms_       =  mode_param.mode_2d.rf_on_time__ms_;
     ppg.input.rf_off_time__ms_      =  mode_param.mode_2d.rf_off_time__ms_;
     ppg.input.e1b_dwell_time__ms_   =  mode_param.mode_2d.dwell_time__ms_;  // 2d uses e1b_dwell_time


    // helicity
    ppg.hardware.enable_helicity_flipping = mode_param.mode_2d.flip_helicity;
    if(ppg.hardware.enable_helicity_flipping)
      ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_2d.helicity_sleep_time__ms_;

 

 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2d","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_2d: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);


  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;

}


INT  copy_2e(char *ppg_mode)
{
  INT status,size;
  
  printf("copy_2e: starting with ppg_mode %s\n", ppg_mode); 
  
  
  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_2e: error from get_param_key\n");
      return status;
    }
  
  
  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_2e);
  status = db_get_record (hDB, hParam, &mode_param.mode_2e, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2e","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_2e: got the record for ppg params mode %s\n",ppg.input.experiment_name);
  
  // Copy parameters from mode parameters area
  
  ppg.input.rf_on_time__ms_ =  mode_param.mode_2e.rf_on_time__ms_;
  ppg.input.num_rf_on_delays__dwell_times_ = mode_param.mode_2e.num_rf_on_delays__dwelltimes_;
  ppg.input.beam_off_time__ms_ = mode_param.mode_2e.beam_off_time__ms_; 
  ppg.input.e2e_num_dwelltimes_per_freq = mode_param.mode_2e.num_dwelltimes_per_freq;
  ppg.input.e2e_num_postrfbeamon_dwelltimes = mode_param.mode_2e.num_post_rfbeamon_dwelltimes;
  
  
  ppg.hardware.enable_helicity_flipping = mode_param.mode_2e.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_2e.helicity_sleep_time__ms_;



  // frequency
  ppg.input.frequency_start__hz_ = mode_param.mode_2e.frequency_scan_start__hz_;
  ppg.input.frequency_stop__hz_ = mode_param.mode_2e.frequency_scan_stop__hz_;
  ppg.input.frequency_increment__hz_ = mode_param.mode_2e.frequency_scan_increment__hz_;
  ppg.input.randomize_freq_values = mode_param.mode_2e.randomize_freq_scan_increments;
  

 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2e","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_2e: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);

  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;

}

INT update_hardware_params(INT ppg_type)
{  

  INT size, status;
  char str[64];
  BOOL off=0;

  /* Called by all Modes that use PSM
     cannot set the record for Hardware (PSM params may have been changed by user) 
     set values individually
  */

  if(ppg_type ==1) // for run type 1
    {      // make sure dual channel mode is set to off
      size = sizeof(off);
      sprintf(str,"Enable dual channel mode");
      status = db_set_value (hDB, hH, str, &off, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"update_hardware_params","Failed to set  \"%s\" OFF  (%d) size=%d ",str,status,size);
	  return(status); /* error */
	}
      else
	printf("update_hardware_params: success setting %s OFF for Type 1 run\n",str);
    }


  size = sizeof(ppg.hardware.enable_helicity_flipping);
  sprintf(str,"Enable helicity flipping");
  status = db_set_value (hDB, hH, str, &ppg.hardware.enable_helicity_flipping, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"update_hardware_params","Failed to set  \"%s\"  (%d) size=%d ",str,status,size);
      return(status); /* error */
    }
  else
    printf("update_hardware_params: success setting %s\n",str);


  size = sizeof(ppg.hardware.helicity_flip_sleep__ms_);
  sprintf(str,"Helicity flip sleep (ms)");
  status = db_set_value (hDB, hH, str, &ppg.hardware.helicity_flip_sleep__ms_, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"update_hardware_params","Failed to set  \"%s\"  (%d) size=%d ",str,status,size);
      return(status); /* error */
    }
  else
    printf("update_hardware_params: success setting %s\n",str);



  return status;
}


INT  copy_2f(char *ppg_mode)
{
   INT status,size;

  printf("copy_2f: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_2f: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_2f);
  status = db_get_record (hDB, hParam, &mode_param.mode_2f, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2f","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_2f: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  ppg.input.mcs_enable_gate__ms_           =  mode_param.mode_2f.mcs_enable_gate__ms_;
  ppg.input.mcs_enable_delay__ms_          =  mode_param.mode_2f.mcs_enable_delay__ms_;
  ppg.input.rf_on_time__ms_                =  mode_param.mode_2f.rf_on_time__ms_;
  ppg.input.rf_off_time__ms_               =  mode_param.mode_2f.rf_off_time__ms_;
  ppg.input.num_rf_on_delays__dwell_times_ =  mode_param.mode_2f.num_rf_on_delays__dwelltimes_;
  ppg.input.e2e_num_postrfbeamon_dwelltimes = mode_param.mode_2f.num_post_rfbeamon_dwelltimes;
  ppg.input.e00_beam_off_dwelltimes        =  mode_param.mode_2f.number_of_beam_off_dwelltimes;

  // helicity
  ppg.hardware.enable_helicity_flipping = mode_param.mode_2f.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_2f.helicity_sleep_time__ms_;

  // frequency
  ppg.input.frequency_start__hz_     = mode_param.mode_2f.frequency_scan_start__hz_;
  ppg.input.frequency_stop__hz_      = mode_param.mode_2f.frequency_scan_stop__hz_;
  ppg.input.frequency_increment__hz_ = mode_param.mode_2f.frequency_scan_increment__hz_;
  ppg.input.randomize_freq_values    = mode_param.mode_2f.randomize_freq_scan_increments;

 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2f","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_2f: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);


  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;
}
#ifdef BNQR
INT  copy_2h(char *ppg_mode)
{ // ALPHA - this is derived from copy_20
  INT status,size;

  printf("copy_2h: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_2h: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_2h);
  status = db_get_record (hDB, hParam, &mode_param.mode_2h, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2h","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_2h: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area

  ppg.input.mcs_enable_gate__ms_ = mode_param.mode_2h.mcs_enable_gate__ms_;
  ppg.input.e00_prebeam_dwelltimes = mode_param.mode_2h.number_of_prebeam_dwelltimes; 
  ppg.input.e00_beam_on_dwelltimes = mode_param.mode_2h.number_of_beam_on_dwelltimes; 
  ppg.input.e00_beam_off_dwelltimes = mode_param.mode_2h.number_of_beam_off_dwelltimes; 
  ppg.input.rfon_dwelltime = mode_param.mode_2h.rfon_delay__dwelltimes_; 
  ppg.input.rfon_duration__dwelltimes_ = mode_param.mode_2h.rfon_duration__dwelltimes_; 

  ppg.hardware.enable_helicity_flipping = mode_param.mode_2h.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_2h.helicity_sleep_time__ms_;


 
 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2h","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_2h: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);

  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;
}
#endif // BNQR


#ifdef BNMR
INT  copy_2s(char *ppg_mode)
{
  INT status,size;

  printf("copy_2s: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_2s: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_2s);
  status = db_get_record (hDB, hParam, &mode_param.mode_2s, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2s","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_2s: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area to ppg input area

  ppg.input.e2s_z_dwell_time__ms_ = mode_param.mode_2s.z_dwell_time__ms_;
  ppg.input.e2s_x_dwell_time__ms_ = mode_param.mode_2s.x_dwell_time__ms_;
  ppg.input.e2s_rf_pi_on__ms_ = mode_param.mode_2s.pi_pulse_length__ms_; 
  ppg.input.e2s_rf_half_pi_on__ms_ = mode_param.mode_2s.half_pi_pulse_length__ms_; 
  ppg.input.prebeam_on_time__ms_  = mode_param.mode_2s.prebeam_on_time__ms_; 
  ppg.input.e2c_beam_on_time__ms_  = mode_param.mode_2s.beam_on_time__ms_;
  ppg.input.e2s_num_fid_cycles = mode_param.mode_2s.num_fid_cycles; 
  if(mode_param.mode_2s.mode_se2_selected)
    ppg.input.e2s_select_mode = 2;
  else
    ppg.input.e2s_select_mode = 1;
  
  ppg.input.e2s_enable_pi_pulse =  mode_param.mode_2s.enable_pi_pulse;  // both BOOL
  ppg.input.e2s_pi_pulse_cycle_num = mode_param.mode_2s.pi_pulse_cycle_num;

   ppg.input.e2s_xy_flip_half_pulse__ms_ = mode_param.mode_2s.xy_flip_half_pulse__ms_;
 
  ppg.hardware.enable_helicity_flipping = mode_param.mode_2s.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_2s.helicity_sleep_time__ms_;

  

 
 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2s","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_2s: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);

  

  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;
}


INT  copy_2w(char *ppg_mode)
{
  INT status,size;

  printf("copy_2w: starting with ppg_mode %s\n", ppg_mode); 


  status = get_param_key(ppg_mode);  // fills hParam key
  if(status != SUCCESS)
    {
      printf("copy_2w: error from get_param_key\n");
      return status;
    }


  /* get the record for this mode (with key hParam) */
  size = sizeof(mode_param.mode_2w);
  status = db_get_record (hDB, hParam, &mode_param.mode_2w, &size, 0);/* get the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2w","Failed to retrieve ppg mode param record for mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    //if(debug)
    printf("copy_2w: got the record for ppg params mode %s\n",ppg.input.experiment_name);

  // Copy parameters from mode parameters area to ppg input area

  ppg.input.e2w_central_freq_f0__khz_ = mode_param.mode_2w.central_freq_f0__khz_;
  ppg.input.e2w_frequency_sweep__khz_ = mode_param.mode_2w.frequency_sweep__khz_;
  ppg.input.e2w_li_precession_f1__khz_ = mode_param.mode_2w.li_precession_f1__khz_; 
  ppg.input.e2w_n__40_or_80_ = mode_param.mode_2w.n__40_or_80_; 
  ppg.input.e2w_q0__default_5_ = mode_param.mode_2w.q0__default_5_; 
  ppg.input.e2w_freq_resolution_nf =  mode_param.mode_2w.freq_resolution_nf;
  ppg.input.e2w_total_num_freq_scans = mode_param.mode_2w.total_number_of_frequency_scans;
  ppg.input.e2w_tp__ms_ = mode_param.mode_2w.tp__ms_;
  ppg.input.e00_beam_on_dwelltimes  = mode_param.mode_2w.number_of_beam_on_dwelltimes; 
  ppg.input.e00_prebeam_dwelltimes  = mode_param.mode_2w.number_of_prebeam_dwelltimes;

  ppg.hardware.enable_helicity_flipping = mode_param.mode_2w.flip_helicity;
  if(ppg.hardware.enable_helicity_flipping)
    ppg.hardware.helicity_flip_sleep__ms_ =  mode_param.mode_2w.helicity_sleep_time__ms_;

 
 /* set the record for Input (with key hIn) */
  size = sizeof(ppg.input);
  status = db_set_record (hDB, hIn, &ppg.input, size, 0);/* set the record for this mode */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"copy_2w","Failed to set input param record for ppg mode %s  (%d) size=%d ",ppg_mode,status,size);
      return(status); /* error */
    }
  else
    printf("copy_2w: set the record for input area for ppg mode %s\n",ppg.input.experiment_name);

  

  status =  update_hardware_params(2);  // cannot set record for hardware, PSM params may have been changed
  return status;
}
#endif // BNMR
