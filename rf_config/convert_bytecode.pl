#!/usr/bin/perl -w 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d  cbyte.pl
#
#   Note: for testing, use cbyte.dat with does not require input params 1,2,3
#
# Translates bytecode.dat (for old PPG) to instructions for new PPG
# Input parameters
#       param 1 path and name of input file (bytecode.dat)
#       param 2 path and name of output file (ppgload.dat -> $ppgload)
#
#       Only the above two parameters are required. The following params are used for testing.
#       param 3 write address of scan loop counter to settings/output area of odb  (Y=1 N=0)
#       param 4 nominal PPG frequency - (e.g. if nominal clock is 10MHz and actual clock of NEW PPG is 100MHz,
#           output times will be multiplied by 10 to give the correct result i.e. as if NEW PPG runs at 10MHz).
#       param 5 shift_outputs  - shift left all the outputs of newppg (for testing)


my ($filename, $ppgload, $write_scanloop_pc, $nominal_freq, $shift_outputs) = @ARGV;
my $num = $#ARGV;
my $clock_freq_MHz = 100; 
my $multiply;
# these are input parameters
#my $filename="/home/$exp/online/ppg/ppgload/bytecode.dat"; # old PPG instruction file
#my $ppgload="/home/$exp/online/ppg/ppgload/ppgload.dat";

my $exp = $ENV{MIDAS_EXPT_NAME}; # testing... might be alpha or pol (alpha includes a lot of options not
                                 # actually needed for pol expt) 
my $home = $ENV{HOME};
my $minimal_delay=3; # 3 clock cycles (new ppg)
my $halt = 0; # halt instruction (new ppg)
my $vme_addr = 0x100000;
my $status_reg = $vme_addr;
my $pc_reg = $vme_addr | 0x8;
my $lo_reg = $vme_addr | 0xC;
my $med_reg = $vme_addr | 0x10;
my $hi_reg = $vme_addr | 0x14;
my $top_reg = $vme_addr | 0x18;
my ($cmd,$comment);
my ($ins,$lc,$delay,$bitpat, $clrpat);
my $tmp;
my $numlines;
my @fields;
# instruction list for new and old ppg
my @new_instructions= ("Halt","Continue","Begin Loop","End Loop","Call Subr","Return","Branch");
my @old_instructions=("Continue","Stop","Begin Loop","End Loop","JSR","RSR","Branch","Long Delay");
my @convert_instructions=qw(1 0 2 3 4 5 6); # no "Long Delay"
my $old_ins;
my $len=$#new_instructions;
my $lenc=$#convert_instructions;
my $pc =0; # program counter
my $time_ms;
my $debug=0;
my $count=0;
my $freq_MHz = $clock_freq_MHz;
my $numloops = 0;
my $odb_eqpname = "pol_acq";
my $odb_scan_pc = "/equipment/$odb_eqpname/settings/output/scanloop pc";
print "\nconvert_bytecode.pl starting\n";

if ($exp eq ""){ die "MIDAS_EXPT_NAME environment variable not defined";}
require  "$home/online/rf_config/perlmidas.pl";
MIDAS_env(); 

$num++;
print "Number of parameters supplied: $num\n";

if ($num < 2)
{
     MIDAS_sendmsg("convert_bytecode","Not enough parameters supplied");
    die "Must supply at least 2 parameters: Input file, Output file, [ write scan loopcount addr, nominal freq; shift inputs ]\n";
}
unless ($nominal_freq) { $nominal_freq = 100 }; # 100MHz frequency; do not adjust times for freq difference
unless ($shift_outputs) {$shift_outputs = 0 }; # do not shift outputs
unless ($write_scanloop_pc ) {$write_scanloop_pc = 0 }; # do not write addr to odb
print "Input Parameters: input path $filename  output path $ppgload\n";
print "        nominal_freq=$nominal_freq; shift_outputs=$shift_outputs; write_scanloop_pc=$write_scanloop_pc\n";

if($write_scanloop_pc ) { MIDAS_varset($odb_scan_pc, 0);} # clear value to start
if($shift_outputs > 0)
{ 
    my ($i,$j);
    $i=$shift_outputs+1;
    $j=$shift_outputs+5;
    print "PPG Outputs will all be shifted left by $shift_outputs compared with the input file\n"; 
    print "e.g. PPG outputs 1-5 will be shifted to outputs $i - $j \n";
}

if ($nominal_freq != 100)
{ 
    $multiply = $clock_freq_MHz/ $nominal_freq;
    print "PPG delay counts will be multiplied by $multiply compared with the input file\n";
    print "PPG will appear to be clocked at $nominal_freq MHz (actual clock frequence is $clock_freq_MHz)  \n";
}
else
{$multiply = 1;}

if($debug){ print "@new_instructions; len=$len\n";}


open (IN,"$filename") or ($tmp= $!);
if ($tmp)
{
    #send_message($name,$MINFO,"FAILURE cannot open file \"$file\"; $tmp" ) ;
    MIDAS_sendmsg("convert_bytecode","Cannot open input file \"$filename\";$tmp");
    die "FAILURE cannot open input file \"$filename\";$tmp\n";
}
open (LDF,">$ppgload") or ($tmp= $!);
if ($tmp)
{
    #send_message($name,$MINFO,"FAILURE cannot open file \"$file\"; $tmp" ) ;
    MIDAS_sendmsg("convert_bytecode","Cannot open input file \"$ppgload\";$tmp");
    die "FAILURE cannot open input file \"$ppgload\";$tmp\n";
}
printf LDF "#Ins 0=halt 1=cont 2=loop 3=endloop   \n"; 
printf LDF "#Note: PC is decimal; bitpats,delay,ins/data are hex  \n";
printf LDF "#PC set bitpat clr bitpat       delay  ins/data\n";

my $linenum=0;
while (<IN>)
{
    $linenum++;
    if (/Instruction Lines/)
    {
	s/Instruction Lines//;
	$numlines = $_ + 0;
        print "number of program lines in bytecode.dat : $numlines\n"; # same number of lines in output file
        $numlines++; # add one for the halt instruction   
        printf LDF ("Num Instruction Lines = %4.0d         #  IDDDDD\n",$numlines);
	}
    if ($linenum >= 8)
    {
	$count++;
	print "\nWorking on line $linenum (instructions line $count): $_ ";
	@fields=split;
	$old_ins = hex($fields[0]);  # old PPG instruction
	$lc  = hex($fields[1]);
	$delay  = hex($fields[2]);
        $bitpat  = hex($fields[3]);
	
	if($shift_outputs > 0) 
	{
	    printf "Bitpat 0x%x ", $bitpat;
	    $bitpat = $bitpat << $shift_outputs;
	    printf "shifted by %d ->  0x%x \n",$shift_outputs,$bitpat;
	}
	
	if ($multiply > 1)
	{
	    printf "Delay 0x%x (%d) ", $delay,$delay;
	    $delay*=$multiply;
	    printf "multiplied by %d ->  0x%x (%d) \n",$multiply,$delay,$delay;
	}
	
        if ($old_ins > $lenc) { die "Instruction (\"$ins\")at line $linenum in $filename is not supported by new PPG \n"; } 
        $ins = $convert_instructions[$old_ins]; # convert old instruction to new.
	if($debug)
	{
	    if($ins != $old_ins)
	    { print "Converted old instruction $old_ins to $ins\n"; }
	}
	
	
	if ($ins == 2 ) # begin loop
	{
	    $numloops++;
	    # Check loop count (maximum 20 bits)
	    $lc++; # convert from old ppg loop count (where 0 meant 1 loop)
	    if ($lc >= 0x100000)
	    {
	       	print "WARNING : data field for loop count is 20 bits maximum. Loop count will be set to $lc\n";
		MIDAS_sendmsg("convert_bytecode","loop count ($lc) for loop $numloops is too large. Loop count will be set to max");
		print "WARNING : data field for loop count is 20 bits maximum. Loop count will be set to $lc\n";
		$lc = 0xFFFFF;
	    }
	}
	elsif ($ins == 4 || $ins == 6)
	{ # check address (max 20 bits)
	    if ($lc >= 0x100000)
	    { 
		MIDAS_sendmsg("convert_bytecode","Address ($lc) at line $linenum for JSR/Branch  too large. Must be less than 0x100000");
		die "Illegal address $lc at line $linenum. Address must be 20bits or less";
	    }
	}
	
	print "fields: @fields\n"; # fields[0..3] code; loop count; clock count; bitpat 	
	printf ("ins=%d lc=0x%x delay=0x%x bitpat=0x%x \n",$ins,$lc,$delay,$bitpat);
	print "Assembling instruction at pc = $pc\n";
	
	
	$time_ms =  ($delay + 3)/($freq_MHz * 1000); # add the 3 clock cycles that each instruction takes
	$tmp = $ins << 20 ;
	$tmp = $tmp | $lc;
	
	if  ($ins == 2)  # begin loop
	{
	    if( $numloops == 1 &&  $write_scanloop_pc)
	    {
		# record the pc of the scan loop instruction
		printf("($numloops, $write_scanloop_pc)PC for begin_scan instruction is $pc\n");
		MIDAS_varset($odb_scan_pc, $pc);
	    }
	} # end of begin_loop
        printf "pc=%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$delay,$tmp;
	printf LDF "%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$delay,$tmp;
	$pc++;  # next instruction
    }
}
# end of input file

# Add HALT at end of program; keep output pattern
$tmp=$ins=0;
$count++; # add one for HALT
printf ("\nAdding HALT instruction, keeping output pattern\n");
printf ("ins=%d lc=0x%x delay=0x%x bitpat=0x%x \n",$ins,$lc,$delay,$bitpat);
printf "pc=%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$minimal_delay,$tmp;
printf LDF "%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$minimal_delay,$tmp;
# set pc back to zero
close LDF;
close IN;
if  ($numlines != $count) 
{ print "Error - number of instructions lines in file ($numlines) does not agree with count ($count)\n";}

print "\nDone. Output file is $ppgload\n";
