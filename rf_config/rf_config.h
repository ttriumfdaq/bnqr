// rf_config.h

#define MAX_BNMR_EXPT      25 // maximum possible experiments 
#define N_SUBSTRINGS       64
#define MAX_MIDBNMR_REGIONS 5
#ifdef PSM
#ifdef PSMIII
#define MAX_CHANNELS 5
#else
#define MAX_PROFILES 4
#endif
#endif // PSM
#ifdef HAVE_SIS3820
// Alpha mode (2h) only supported with a SIS3820 scaler. Currently BNQR only.
#define ALPHA_OFFSET  2  // 2 real scaler inputs will be skipped in alpha mode (2h) to save space
#endif //  HAVE_SIS3820


#ifdef HAVE_NEWPPG
#define NOMINAL_PPG_CLOCK  10 // MHz  needed for convert_bytecode.pl
                             // newppg runs at 100MHz, old PPG at 10MHz
#endif

typedef struct {
    char s[64];
    INT  sl;
    char r[64];
    INT  rl;
    char *p;
  } SUBSTRING;

SUBSTRING c[N_SUBSTRINGS];

typedef struct {
    double quot;
    double rem;
  } div_double;

typedef struct {
    char name[3];
    INT (*op_func)(char *);
} BNMR_EXPT;

#ifdef PSM
#define FP_GATE_DISABLED 0
#define GATE_NORMAL_MODE 1
#define GATE_PULSE_INVERTED 2
#define INTERNAL_GATE 3
#endif

/* prototypes */
div_double div_f (double num, double denom,const double timeslice);
int rangecheck(double delay, double min_delay, double time_slice);
int build_f_table(char *ppg_mode);
double mult (double a,DWORD b);
INT first_match(SUBSTRING *c, INT n);
INT check_regions(void);
INT substitute(char *rulefile, char *inpath, char *outpath, char *outfile);
INT settings_rec_get(void);
INT check_params(char *ppg_mode);
INT expt_search(char *expt_name);
INT file_wait(char *path, char *file);
INT tr_prestart(INT run_number, char *error);
int main(int argc, char **argv);
INT write_alarm(INT ival, char *msg);
BOOL check_sis_test_mode(void);
INT param_search(char *ppg_mode);
#ifdef PSM
#ifdef PSMIII
INT  build_iq_table_psm3(char *ppg_mode, char *channel_name);
INT  build_iq_table_psm3_2w(char *ppg_mode, char *channel_name);
INT check_identical_bandwidth_2w(void);
INT check_psm_channel(void);
char *get_channel(INT n);
INT get_channel_index(char *p_channel_code, INT *pindex);
INT get_phase_corrections(void);
#else
INT check_psm_gate(void);
INT check_psm_profile(void);
INT get_profile_index(char *p_profile_code, INT *pindex);
char *get_profile(INT n);
#endif
INT get_IQ_params(INT Ntiqtemp, INT *Nc, INT *Ncic);
int build_f_table_psm(char *ppg_mode);
INT check_psm_quad(char *ppg_mode);
#ifdef PSMII
int build_iq_table_psm2(char *ppg_mode, char *profile);
#else
int build_iq_table_psm(char *ppg_mode, char *profile);
#endif
INT write_start_abort(BOOL flag);
#endif

INT e1a_compute(char *);
INT e1b_compute(char *);
INT e1c_compute(char *);
INT e1n_compute(char *);
INT e1g_compute(char *);
INT e1j_compute(char *);
INT e1f_compute(char *);
INT e00_compute(char *);
INT e2a_compute(char *);
INT e2b_compute(char *);
INT e2c_compute(char *);
INT e2d_compute(char *);
INT e2e_compute(char *);
INT e2f_compute(char *);
INT e2s_compute(char *);  // need PSMIII, currently BNMR only
INT e2w_compute(char *);  // need PSMIII, currently BNMR only
#ifdef CAMP
INT camp_get_rec(void);
INT camp_create_rec(void);
INT ping_camp(void);
void check_camp(void);
#endif


#ifdef EPICS_ACCESS
INT check_epics_switch_access(char *error);
INT check_epics_hel_switch(char *error);
INT read_and_retry(char *cmd);
#endif
