/*   type1_compute.c 

Type 1 Compute routines included into rf_config.c
   i.e.   1c 1f 1n 1b

VMIC version
*/


/*------------------------------------------------------------------
NOTE:  DAQ service time is not used for Type 1 
     (d_dac_service is set to 0 in the Type 1 ppg templates)
 ------------------------------------------------------------------*/
INT e1a_compute(char *ppg_mode)
{
  FILE  *rulef;
  char  str[256];
  //char  str2[256];
  char  rulefile_name[256];

  //double a;
  double d_background;
  double d_msnp;
  double d_dac_service;
  double d_rf_on;
  double d_count;
  double d_precount;
  double d_postcount;
  double d_rf_off;
  double d_RF_delaytime;
  double d_minimal;
  double time_slice;
  double d_ctime1;
  double d_ctime2;
  char m1,m2;
  INT n_main;
  INT n_fcycles;
  int   i,j;
  const  int num_spectra_per_freq=1; /* was ppg.input.num_spectra_per_freq; replaced by 
               num_cycles_per_supercycle (in febnmr.c)
	       set to 1 so loop count in ppg files are zero. */

  if(debug)printf("e1a_compute is starting with input ppg_mode=%s\n",ppg_mode);

  /* -- Get ODB values and initialize -- */

#ifdef PSM
#ifndef PSMIII  // check combined in check_psm_channel
  /* check only one gate parameter is selected */
  if(check_psm_gate() != SUCCESS)return -1;
#endif
#endif

  time_slice       = 	ppg.input.time_slice__ms_;
  d_minimal        = 	ppg.input.minimal_delay__ms_;
  d_msnp           = 	d_minimal;

  d_ctime1         =    d_minimal;
  d_ctime2         =    d_minimal;
  d_precount       =    d_minimal;
  d_postcount      =    d_minimal;
  d_rf_on          = 	ppg.input.rf_on_time__ms_;

  if (d_rf_on < 0.1 )
    {cm_msg(MERROR,"e1a_compute","RF on time (ms) too short = %f ms < 0.1 ms ",d_rf_on);
    return -1;
    }

  d_rf_off         = 	ppg.input.rf_off_time__ms_;
  if (d_rf_off < d_minimal)
                      {cm_msg(MINFO,"e1a_compute","RF off time 0, set to minimal delay time %f ms",d_minimal);
	               d_rf_off = d_minimal;
                      }

  d_background     =    ppg.input.bg_delay__ms_;
  d_dac_service    =    ppg.input.daq_service_time__ms_;
  d_count	   =    ppg.input.mcs_enable_gate__ms_;
  d_RF_delaytime   =    ppg.input.rf_delay__ms_;

  /* make sure daq service time is at least the minimal delay or comp_int.pl fails...
     dac service time is set to 0 in the template file  */

  // if(d_dac_service <=0)   //  make sure daq_service_time is min delay for this mode
    d_dac_service = d_minimal;

  /*Check the loop conditions. No loop -> nothing to do -> something is wrong*/
  n_main           =    num_spectra_per_freq - 1;
			if(n_main == -1)
	
	{cm_msg(MERROR,"e1a_compute","Loop count zero in n_main, please set 'input.Num spectra per freq' > 0");
	 return -1;
    	}
  n_fcycles        =    ppg.input.num_rf_cycles -1;
			if (n_fcycles == -1)
	
	{cm_msg(MERROR,"e1a_compute","Loop count zero in n_fcycles, please set 'input.Num RF cycles' > 0");
	return -1;
    	}
			
/*
Build the freq. table. The table is not needed in this mode, 
but the number freq. steps must be calculated
Also needed if randomized frequencies are required (1a,1b)
*/
#ifdef PSM
  if(check_psm_quad(ppg_mode)!= SUCCESS) // may call build_iq_table_psm for quad mode 
  {
     cm_msg(MERROR,"e1a_compute","Error return from check_psm_quad ");
     return -1;
  }
  i = build_f_table_psm(ppg_mode);
#else
  i = build_f_table(ppg_mode);
#endif

  if ( i == -1 )
  {cm_msg(MERROR,"e1a_compute","Illegal Number of frequency steps = %i ",i);
  return -1;
  }

  /* -- Selection of the matching template file -- */

    /* Filename: ppg_mode         C/P  000/180  FO/FR+ m1 +m2 .ppg */
    /* like      1a_C180FR11      C     180      FR    1   1  .ppg */

  /* determine the first three parameters: beam mode, pulse pairs, frequency mode */
  ppg.input.beam_mode = toupper (ppg.input.beam_mode);
  if  (ppg.input.beam_mode != 'P' && ppg.input.beam_mode != 'C' )
    {
      cm_msg(MERROR,"e1a_compute","Invalid beam mode parameter (%c).  Must be P or C ",ppg.input.beam_mode );
      return -1;
    }
  if  ( ( strstr(ppg.input.e1a_and_e1b_pulse_pairs, "000") || strstr(ppg.input.e1a_and_e1b_pulse_pairs, "180")) == 0)
    {
      cm_msg(MERROR,"e1a_compute","Invalid pulse pairs parameter (%s).  Must be 000 or 180 ",
	     ppg.input.e1a_and_e1b_pulse_pairs );
      return -1;
    }
  for  (j=0; j< strlen(ppg.input.e1a_and_e1b_freq_mode); j++)   
    ppg.input.e1a_and_e1b_freq_mode[j]  = toupper (ppg.input.e1a_and_e1b_freq_mode[j] );
 
   if  ( (strstr(ppg.input.e1a_and_e1b_freq_mode, "F0") || strstr(ppg.input.e1a_and_e1b_freq_mode, "FR") ) == 0)
    {
      cm_msg(MERROR,"e1a_compute","Invalid frequency mode parameter (%s).  Must be F0 (Fzero)  or FR ",
	     ppg.input.e1a_and_e1b_freq_mode  );
      return -1;
    }


  /*Counting gate mode selection, m2 */
  if (ppg.input.mcs_enable_delay__ms_ > d_rf_on)
    { 				
      /*Counting mode 2, counting gate in RF off */
      d_precount  = ppg.input.mcs_enable_delay__ms_ - d_rf_on;
      if (!rangecheck(d_precount,d_minimal,time_slice))
          {cm_msg(MERROR,"e1a_compute","Counting in RF off: Rangecheck-Error for d_precount = %f ",d_precount);
           cm_msg(MERROR,"e1a_compute","MCS enable delay (ms) must be >= RF on + Minimal delay");
           return -1;
          }

      d_postcount = d_rf_off - (d_precount + d_count);
      if (!rangecheck(d_postcount,d_minimal,time_slice))
          {cm_msg(MERROR,"e1a_compute","Rangecheck-Error for d_postcount = %f ",d_postcount);
           cm_msg(MERROR,"e1a_compute","MCS enable delay + counting gate must be <= RF on + RF off - Minimal delay");
           return -1;
          }
      m2 = '2';
    }
  else {
    if ((ppg.input.mcs_enable_delay__ms_ + d_count) > d_rf_on)
      {
        /*Counting mode 3, counting gate over RF on-off transition*/
	d_postcount =  d_rf_on+d_rf_off-d_count-ppg.input.mcs_enable_delay__ms_;
	if (!rangecheck(d_postcount,d_minimal,time_slice))
	  {cm_msg(MERROR,"e1a_compute","Counting during the RF on->off transition: Rangecheck-Error for d_postcount = %f ",d_postcount);
	   cm_msg(MERROR,"e1a_compute","MCS enable delay + counting gate must be <= RF on + RF off - Minimal delay");
          return -1;
	  }
	
	d_precount  =  ppg.input.mcs_enable_delay__ms_- d_msnp;
	if (!rangecheck(d_precount,d_minimal,time_slice))
	  {cm_msg(MERROR,"e1a_compute","Counting during the RF on->off transition: Rangecheck-Error for d_precount = %f ",d_precount);
	   cm_msg(MERROR,"e1a_compute","MCS enable delay (ms) must be >= 2 * Minimal delay");
           return -1;
	  }
	
	d_ctime1    =  d_rf_on - ppg.input.mcs_enable_delay__ms_;
	if (!rangecheck(d_ctime1,d_minimal,time_slice))
	  {cm_msg(MERROR,"e1a_compute","Counting during the RF on->off transition: Rangecheck-Error for d_ctime1 = %f ",d_ctime1);
	   cm_msg(MERROR,"e1a_compute","MCS enable delay (ms) must be <= RF on - Minimal delay");
           return -1;
	  }
	
	d_ctime2    =  d_rf_off - d_postcount;
	if (!rangecheck(d_ctime2,d_minimal,time_slice))
	  {cm_msg(MERROR,"e1a_compute","Counting during the RF on->off transition: Rangecheck-Error for d_ctime2 = %f ",d_ctime2);
	   cm_msg(MERROR,"e1a_compute","MCS enable delay (ms) must be >= RF on + Minimal delay");
           return -1;
	  }
	
	m2 = '3';

      }
     else          	
      {
        /*Counting mode 1, counting gate in RF on */
	d_postcount = d_rf_on - d_count - ppg.input.mcs_enable_delay__ms_;
	d_precount =  ppg.input.mcs_enable_delay__ms_- d_msnp;
	if (!rangecheck(d_precount,d_minimal,time_slice))
	  {cm_msg(MERROR,"e1a_compute","Counting in RF on: Rangecheck-Error for d_precount = %f ",d_precount);
	   cm_msg(MERROR,"e1a_compute","MCS enable delay must be >= 2 * Minimal delay");
           return -1;
	  }
	
	if (!rangecheck(d_postcount,d_minimal,time_slice))
	  {cm_msg(MERROR,"e1a_compute","Counting in RF on: Rangecheck-Error for d_postcount = %f ",d_postcount);
	   cm_msg(MERROR,"e1a_compute","MCS enable delay + counting gate must be <= RF on - Minimal delay");
           return -1;
	  }
	
	m2 = '1';
      }

  }

  /*Background delay mode selection, m1*/

  if (d_background == 0)
    { /*No background delay*/
      m1 = '0';
      d_background = d_minimal;
    }
  else if (!rangecheck(d_background,d_minimal,time_slice))
	  {cm_msg(MERROR,"e1a_compute","Background delay time too short (can be 0 or greater than minimal delay time %f ms)",d_minimal);
	   return -1;
	  }
	 else m1 = '1';



 /* -- Calculation and writing of the ODB output values -- */

 /* Number of dwelltimes */

 if (strncmp (ppg.input.e1a_and_e1b_freq_mode,"FR",2)==0)
    {ppg.output.num_dwell_times = 4 * (n_fcycles+1) ;}
   else
    {ppg.output.num_dwell_times = 2 * (n_fcycles+1) ;}


 /* Beam on time */
 ppg.output.vme_beam_control=FALSE;
  if (ppg.input.beam_mode == 'C')
    {
      ppg.output.beam_on_time__ms_= d_RF_delaytime + ppg.output.num_dwell_times * (d_rf_on + d_rf_off);
      if( m1 == '0')
	ppg.output.vme_beam_control=TRUE;
    }
   else
       ppg.output.beam_on_time__ms_= d_RF_delaytime;


 ppg.output.dwell_time__ms_  = d_rf_on + d_rf_off;
 ppg.output.rf_on_time__ms_  = d_rf_on;
 ppg.output.rf_off_time__ms_ = d_rf_off;

 /* -- Calculating and writing the ODB histogram information (e1a) -- */

 i =  ppg.output.num_dwell_times;
 for (j=0; j <  ppg.input.num_type1_frontend_histograms ; j ++)
     {fmdarc.histograms.bin_zero             [j] = 1;
      fmdarc.histograms.first_good_bin       [j] = 1;
      fmdarc.histograms.first_background_bin [j] = 0;
      fmdarc.histograms.last_background_bin  [j] = 0;
      fmdarc.histograms.last_good_bin	  [j] = i;
     }

 /* write some information to mdarc area for e1a */
 fmdarc.histograms.number_defined = ppg.input.num_type1_frontend_histograms ; /*  number of  histograms as written by frontend */
 fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */

  if(!check_sis_test_mode() ) /* real mode (front panel signals, uses ppg and fsc */
    {
     if(debug)printf("SIS test mode is disabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.output.num_dwell_times ;
   }
 else   /* sis internal  test mode  */  
   {
#ifdef HAVE_SIS3801
     fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.sis3801.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif // HAVE_SIS3801
#ifdef HAVE_SIS3820
      fmdarc.histograms.dwell_time__ms_ =   10000000/ ppg.sis_test_mode.sis3820.lne_prescale_factor;
      fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif //  HAVE_SIS3820
     
     for (j=0; j <  ppg.input.num_type1_frontend_histograms; j ++)
       fmdarc.histograms.last_good_bin	[j] =  fmdarc.histograms.num_bins ;
   }




 if(fmdarc.histograms.midbnmr.use_defaults)   
   {
     printf("e1a_compute: using default values for midbnmr parameters\n");
     /*  write defaults for midbnmr */
     fmdarc.histograms.midbnmr.number_of_regions =1;
     //sprintf(fmdarc.histograms.midbnmr.range_label [0],"");
     fmdarc.histograms.midbnmr.range_label[0][0]='\0';
     fmdarc.histograms.midbnmr.last_time_bin [0] = fmdarc.histograms.num_bins;
     fmdarc.histograms.midbnmr.first_time_bin [0] =1;

    /* clean out the unused regions */
    for (j= fmdarc.histograms.midbnmr.number_of_regions; j <  MAX_MIDBNMR_REGIONS; j++)
      {
	//sprintf(fmdarc.histograms.midbnmr.range_label [j],"");
	fmdarc.histograms.midbnmr.range_label [j][0] ='\0';
	fmdarc.histograms.midbnmr.first_time_bin[j] =  0;
	fmdarc.histograms.midbnmr.last_time_bin[j] = 0 ;
      }
     fmdarc.histograms.midbnmr.start_freq__hz_ = ppg.input.frequency_start__hz_;
     fmdarc.histograms.midbnmr.end_freq__hz_ = ppg.input.frequency_stop__hz_;
     printf("  Midbnmr range label       : %s\n", fmdarc.histograms.midbnmr.range_label[0] );
     printf("  Midbnmr first time bin    : %d\n", fmdarc.histograms.midbnmr.first_time_bin[0] );
     printf("  Midbnmr last time bin     : %d\n", fmdarc.histograms.midbnmr.last_time_bin[0] );
     printf("  Midbnmr start_freq (Hz)   : %d\n", fmdarc.histograms.midbnmr.start_freq__hz_  );
     printf("  Midbnmr end_freq   (Hz)   : %d\n", fmdarc.histograms.midbnmr.end_freq__hz_  );
   }

 else
   {
   if(check_regions() < 0)
     {
       printf("e1a_compute: error return from check_regions for midbnmr parameters\n");
       return -1;
     }
   printf("e1a_compute: success from checking values for midbnmr parameters\n");
   }

 if(debug)
   {
     printf("Writing values to mdarc area:\n");
     printf("  Number frontend histograms: %d\n", fmdarc.histograms.number_defined);
     printf("  Number of bins            : %d\n", fmdarc.histograms.num_bins);
     printf("  Dwell time (ms)           : %f\n", fmdarc.histograms.dwell_time__ms_);
   }
  
 /* -- Writing the rulefile -- */

  cm_msg(MINFO,"e1a_compute","Computing values and writing to the ODB done, writing rulefile.");


  sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");
  if(debug)printf("e1a_compute: about to open rulefile: %s\n",rulefile_name);
  
  rulef = fopen(rulefile_name, "w");

  if (rulef) {
    sprintf(str,"%s_%c%s%s%c%c%s", ppg_mode, ppg.input.beam_mode, ppg.input.e1a_and_e1b_pulse_pairs,  
	    ppg.input.e1a_and_e1b_freq_mode, m1,m2,".ppg\n"); /* add an underscore after ppg_mode */
    {
      int len,size;
     
      len=strlen(str);
      size=sizeof(ppg.output.ppg_template);
      //printf("strlen of str %s is %d, size=%d\n",str,len,size);
      strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */  
      ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file
      
      //printf("ppg.output.ppg_template=%s; \n", ppg.output.ppg_template);
    }

    if (debug) printf("e1a_compute: writing ppg template file name (%s) into rulefile\n",ppg.output.ppg_template);

    fputs(str,rulef);

    sprintf(str,"%s%s",ppg_mode,".ppg\n");
    fputs(str,rulef);

    sprintf(str,"%s%s%s%f%s","d_msnp=0","%","d_msnp=",d_msnp,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_background=0","%","d_background=",d_background,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_rf_on=0","%","d_rf_on=",d_rf_on,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_rf_off=0","%","d_rf_off=",d_rf_off,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_postcount=0","%","d_postcount=",d_postcount,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_precount=0","%","d_precount=",d_precount,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_count=0","%","d_count=",d_count,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ctime1=0","%","d_ctime1=",d_ctime1,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ctime2=0","%","d_ctime2=",d_ctime2,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dac_service=0","%","d_dac_service=",d_dac_service,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_RF_delaytime=0","%","d_RF_delaytime=",d_RF_delaytime,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_fcycles","%",n_fcycles,"\n");
    fputs(str,rulef);

    sprintf(str,"%s%s%d%s","n_main","%",n_main,"\n");
    fputs(str,rulef);
    fclose (rulef);

              }
   else   {
    	cm_msg(MERROR,"e1a_compute","can't open rule file %s", rulefile_name);
    	return -1;
    	}

  cm_msg(MINFO,"e1a_compute","Rulefile %s written.",rulefile_name);

  return 1;
}
/*------------------------------------------------------------------*/
INT e1b_compute(char *ppg_mode)
{
  FILE  *rulef;
  char  str [256];
  //char  str2[256];
  char  rulefile_name[256];

  char  mod1,mod2;
  double d_predt1 = 0;
  double d_predt2 = 0;
  double d_dt1=0;
  double d_dt2=0;
  double d_RF_delay;
  double d_dwelltime;
  double d_dac_service;
  double d_msnp;
  double d_minimal;
  double d_rf_off;
  double d_rf_on;
  double d_background;
  double time_slice;
  int   i,j;
  INT n_main    = 0;
  INT n_fcycles = 0;
  INT n_backd   = 0;
  INT n_RFd     = 0;
  INT n_pulse   = 0;
  INT n_nopulse = 0;
  INT delay_dwlts = 0;
  INT RF_dwlts = 0;
  int ad_warning;
  div_double n_rf_on;
  div_double n_rf_off;
  div_double n_background;
  n_background.rem=n_background.quot=0; // initialize to get rid of warnings
  div_double n_RF_delay;
  const  int num_spectra_per_freq=1; /* set to 1 so loop count in ppg files are zero. */
  BOOL mode2d_flag;
  DWORD num_his;

  if(debug)printf("e1b_compute is starting with input ppg_mode=%s\n",ppg_mode);
  if(strncmp(ppg_mode,"2d",2)==0)
    {
      printf("e1b_compute: ppg mode 2d is running, using 1b template files\n");
      mode2d_flag=TRUE;
      ppg.input.randomize_freq_values=0; /* randomization not permitted for 2d (no freq scan)*/
    }
  else
    mode2d_flag=FALSE;
  
#ifdef PSM
#ifndef PSMIII  // check combined in check_psm_channel
  /* check only one gate parameter is selected */
  if(check_psm_gate() != SUCCESS)return -1;
#endif
#endif

  ad_warning       =    0;

  /* -- Assign ODB input values to the variables -- */

  time_slice       = 	ppg.input.time_slice__ms_;
  d_minimal        = 	ppg.input.minimal_delay__ms_;

  d_RF_delay	   = 	ppg.input.rf_delay__ms_;
  d_dwelltime	   =    ppg.input.e1b_dwell_time__ms_;
  if (d_dwelltime < d_minimal)
    {cm_msg(MERROR,"e1b_compute","Rangecheck-Error for d_dwelltime = %f , but must be > %f ms",d_dwelltime,d_minimal);
    return -1;
    }
  ppg.output.dwell_time__ms_ = d_dwelltime;

  d_dac_service    =    ppg.input.daq_service_time__ms_;
  d_rf_on          = 	ppg.input.rf_on_time__ms_;
  d_rf_off         = 	ppg.input.rf_off_time__ms_;
  d_background     =    ppg.input.bg_delay__ms_;
  d_msnp    	   =    d_minimal;
  n_fcycles        =    ppg.input.num_rf_cycles -1;
  if (n_fcycles == -1)
    {cm_msg(MERROR,"e1b_compute","Loop count zero in n_fcycles, please set 'input.Num RF cycles' > 0");
    return -1;
    }
  
  /* make sure daq service time is at least the minimal delay or comp_int.pl fails...
     dac service time is set to 0 in the template file  */

  // if(d_dac_service <=0)   //  make sure daq_service_time is min delay for this mode
    d_dac_service = d_minimal;

  n_main           =    num_spectra_per_freq -1;
  if (n_main == -1)
    {cm_msg(MERROR,"e1b_compute","Loop count zero in n_main, please set 'input.Num spectra per freq' > 0");
    return -1;
    }
  
  /* -- Build the frequency table. The table is not needed for this exp.type, but the number of freq.steps is needed
        Also needed if randomized frequencies are required (1a,1b)
 */
#ifdef PSM
  if(check_psm_quad(ppg_mode)!= SUCCESS) // may call build_iq_table_psm for quad mode 
  {
     cm_msg(MERROR,"e1b_compute","Error return from check_psm_quad ");
     return -1;
  }
  i = build_f_table_psm(ppg_mode);
#else
  i = build_f_table(ppg_mode);
#endif

   if ( i == -1 )
    {cm_msg(MERROR,"e1b_compute","Error or Invalid number of frequency steps %i",i);
    return -1;
    }
  
  /* -- Selection of the matching template file -- */

    /* Filename: ppg_mode         C/P  000/180  FO/FR+ mod1 +mod2 .ppg */
    /* like      1b_P180FO         P     180      FO    1      2  .ppg */
  
  /* determine the first three parameters: beam mode, pulse pairs, frequency mode */
  ppg.input.beam_mode = toupper (ppg.input.beam_mode);
  if  (ppg.input.beam_mode != 'P' && ppg.input.beam_mode != 'C' )
    {
      cm_msg(MERROR,"e1b_compute","Invalid beam mode parameter (%c).  Must be P or C ",ppg.input.beam_mode );
      return -1;
    }
  if  ( ( strstr(ppg.input.e1a_and_e1b_pulse_pairs, "000") || strstr(ppg.input.e1a_and_e1b_pulse_pairs, "180")) == 0)
    {
      cm_msg(MERROR,"e1b_compute","Invalid pulse pairs parameter (%s).  Must be 000 or 180 ",
	     ppg.input.e1a_and_e1b_pulse_pairs );
      return -1;
    }
  for  (j=0; j< strlen(ppg.input.e1a_and_e1b_freq_mode); j++)   
    ppg.input.e1a_and_e1b_freq_mode[j]  = toupper (ppg.input.e1a_and_e1b_freq_mode[j] );

   if  ( (strstr(ppg.input.e1a_and_e1b_freq_mode, "F0") || strstr(ppg.input.e1a_and_e1b_freq_mode, "FR") ) == 0)
   {
      cm_msg(MERROR,"e1b_compute","Invalid frequency mode parameter (%s).  Must be F0 (Fzero) or FR ",
	     ppg.input.e1a_and_e1b_freq_mode  );
      return -1;
    }


  
  /*Mode selection for the RF on off transition, mod2 */

  n_rf_on  = div_f (d_rf_on,d_dwelltime,time_slice);
  n_rf_off = div_f ((d_rf_off - n_rf_on.rem),d_dwelltime,time_slice);

  printf(" n_rf_on.quot =%f n_rf_on.rem = %f  after def\n",n_rf_on.quot ,n_rf_on.rem);
  printf(" n_rf_off.quot =%f n_rf_off.rem = %f after def\n",n_rf_off.quot ,n_rf_off.rem); 
  mod2 = 'x'; /* flag for not catagorizing mod2 properly */
  
  if (n_rf_on.quot > 0)
  {
  if (n_rf_on.rem == 0.)
      	{
	  /*RF on (and eventuall RF off) integer number of dwelltimes > 0 */
	  
	  mod2 = '2';
	  d_dt1     = d_minimal;    /*Both must be defined for the Perl Compiler, since both  */
	  d_dt2     = d_minimal;    /*are defined, but not used in the corresponding template */
	  n_pulse   = n_rf_on.quot - 1;
	  n_nopulse = n_rf_off.quot - (n_rf_off.rem == 0.);
	  ppg.output.rf_on_time__ms_  = (n_pulse + 1) * d_dwelltime;
	  ppg.output.rf_off_time__ms_ = (n_nopulse + 1) * d_dwelltime;
	  ppg.output.e1b_rf_on__dwelltimes_ = 1 + n_pulse;
	  ppg.output.e1b_rf_off__dwelltimes_ = 1 + n_nopulse;
	  RF_dwlts = 2 + n_pulse + n_nopulse; /* pulse loop + no_pulse loop */ 
	}
   else
      {	
   if  (d_rf_off <= n_rf_on.rem )
	{
          /*RF_on not integer > 1 and RF off ends within the same dwelltime */
	  
	  if (mod2 != 'x')
          {
	    printf(" n_rf_on.quot =%f n_rf_on.rem = %f  after def\n",n_rf_on.quot ,n_rf_on.rem);
	    printf(" n_rf_off.quot =%f n_rf_off.rem = %f after def\n",n_rf_off.quot ,n_rf_off.rem);
	    printf(" incoming mod2 = %c and current comp mod2 = 4\n", mod2); 
           cm_msg(MERROR,"e1b_compute","RFon/off Mode selection error, check e1b compute");
           return -1;
          }     
	  
          mod2 = '4';
	  n_pulse   = n_rf_on.quot - 1;
  	  d_dt1     = n_rf_on.rem;
	  if (d_dt1 < d_minimal) d_dt1 = d_minimal;
  	  d_dt2     =  d_dwelltime - d_dt1;
	  if (d_dt2 < d_minimal)
		{
		  d_dt2 = d_minimal;
		  d_dt1 = d_dwelltime - d_dt2;
		}
  	   n_nopulse = 0;
  	   ppg.output.rf_on_time__ms_  = ((n_pulse + 1) * d_dwelltime)+d_dt1;
  	   ppg.output.rf_off_time__ms_	= d_dt2;
	   ppg.output.e1b_rf_on__dwelltimes_ = 2 + n_pulse;
	   ppg.output.e1b_rf_off__dwelltimes_ = 0;	
	   RF_dwlts = 2 + n_pulse; /* pulse loop + (dt1,dt2) */
 	}
	
  if (d_rf_off > n_rf_on.rem)
	{
           /*RF_on not integer >1 and RF off ends in subsequent dwelltimes */

	if (mod2 != 'x')
          {
	    printf(" n_rf_on.quot =%f n_rf_on.rem = %f  after def\n",n_rf_on.quot ,n_rf_on.rem);
	    printf(" n_rf_off.quot =%f n_rf_off.rem = %f after def\n",n_rf_off.quot ,n_rf_off.rem);
	    printf(" incoming mod2 = %c and current comp mod2 = 1\n", mod2); 
           cm_msg(MERROR,"e1b_compute","RFon/off Mode selection error, check e1b compute");
           return -1;
          }
	         
  	   mod2 = '1';
           n_pulse   = n_rf_on.quot - 1;
           d_dt1     = n_rf_on.rem;
           if (d_dt1 < d_minimal) d_dt1 = d_minimal;
           d_dt2     = d_dwelltime - d_dt1;
           if (d_dt2 < d_minimal)
		{
		  d_dt2 = d_minimal;
		  d_dt1 = d_dwelltime - d_dt2;
		}
           n_nopulse = n_rf_off.quot;
           ppg.output.rf_on_time__ms_  = ((n_pulse +1)  *d_dwelltime)+d_dt1;
           ppg.output.rf_off_time__ms_ = ((n_nopulse+1) *d_dwelltime)+d_dt2;
	   ppg.output.e1b_rf_on__dwelltimes_ = 2 + n_pulse;
	   ppg.output.e1b_rf_off__dwelltimes_ = 1 + n_nopulse;
           RF_dwlts = 3 + n_pulse + n_nopulse; /* pulse loop + (dt1,dt2) + no_pulse loop  */ 
        }
     }	
  }	
  else	
        {
            /*d_RF_on < 1 dwelltime */

 	if (mod2 != 'x')
          {
	    printf(" n_rf_on.quot =%f n_rf_on.rem = %f  after def\n",n_rf_on.quot ,n_rf_on.rem);
	    printf(" n_rf_off.quot =%f n_rf_off.rem = %f after def\n",n_rf_off.quot ,n_rf_off.rem);
	    printf(" incoming mod2 = %c and current comp mod2 =3\n", mod2); 
           cm_msg(MERROR,"e1b_compute","RFon/off Mode selection error, check e1b compute");
           return -1;
          }
      
           mod2 = '3';
           d_dt1     = d_rf_on;
           if (d_dt1 < d_minimal) d_dt1 = d_minimal;
           n_pulse   = 0;
           d_dt2 = d_dwelltime - d_dt1;
           if (d_dt2 < d_minimal)
		{
		  d_dt2 = d_minimal;
		  d_dt1 = d_dwelltime - d_dt2;
		}
           n_nopulse = n_rf_off.quot;
           ppg.output.rf_on_time__ms_  = d_dt1;
           ppg.output.rf_off_time__ms_ = ((n_nopulse+1)*d_dwelltime)+d_dt2;
	   ppg.output.e1b_rf_on__dwelltimes_ = 1;
	   ppg.output.e1b_rf_off__dwelltimes_ = 1 + n_nopulse;
           RF_dwlts = 2 + n_nopulse; /* (dt1,dt2) + no_pulse loop */
         }
	 
  /* check to see if the RFon/off mode was properly selected */
            
	    printf(" n_rf_on.quot =%f n_rf_on.rem = %f  after def\n",n_rf_on.quot ,n_rf_on.rem);
	    printf(" n_rf_off.quot =%f n_rf_off.rem = %f after def\n",n_rf_off.quot ,n_rf_off.rem);
	    printf(" mod2 = %c\n", mod2); 
  
  if (mod2 == 'x')
          {
           cm_msg(MERROR,"e1b_compute","RFon/off Mode selection error, check e1b compute");
           return -1;
          }
  
  /*Mode selection for the background delay -> RF delay transition, mod1*/
  
  mod1 = 'x'; /* flag to make sure that mod1 is properly catagorized */
  
  if (d_RF_delay < d_dwelltime)
    {cm_msg(MERROR,"e1b_compute","RF delay must be >= dwelltime");
    return -1;    
    }
  n_RF_delay = div_f(d_RF_delay, d_dwelltime, time_slice);
  
  if ((d_background != 0) && d_background < d_dwelltime)
    {cm_msg(MERROR,"e1b_compute","Non zero background delay must be >= dwelltime");
    return -1;    
    }

  if ( (ppg.input.beam_mode == 'C' ) && (d_background != 0.0))
    {cm_msg(MERROR,"e1b_compute","Background delay (%.2fms) must be zero for Continuous beam.",
	    d_background);
     return -1;
    }
	
  if (d_background == 0)
    {
      mod1 = '0';             /* only RF delay loop */
      n_RFd = n_RF_delay.quot - 1 + (n_RF_delay.rem > 0);
      if (n_RF_delay.rem > 0) 
        {cm_msg(MINFO,"e1b_compute","RF delay is rounded up to nearest dwelltime boundary.");
	}
      n_backd = 0;	
      ppg.output.e1b_bg_delay__dwelltimes_ = 0;
      ppg.output.e1b_rf_delay__dwelltimes_ = 1 + n_RFd;
      delay_dwlts = 1 + n_RFd;  
    }
  
    else  
    { if (n_RF_delay.rem == 0)
        { mod1 ='2';          /* background loop + RF delay loop */
	  n_RFd = n_RF_delay.quot - 1;
	  n_background = div_f(d_background, d_dwelltime, time_slice);
	  n_backd = n_background.quot -1 + (n_background.rem > 0); 
	  if (n_background.rem > 0) 
             {cm_msg(MINFO,"e1b_compute","Background delay is rounded up to nearest dwelltime boundary.");
	     }
	  ppg.output.e1b_bg_delay__dwelltimes_ = 1 + n_backd;
          ppg.output.e1b_rf_delay__dwelltimes_ = 1 + n_RFd;
	  delay_dwlts = 2 + n_RFd + n_backd;
	}          
      else
        { mod1 = '1';          /* background loop +(d_predt1, d_predt2) + RF delay loop */
	  n_RFd = n_RF_delay.quot - 1;
	  d_predt2 = n_RF_delay.rem;
          if (!rangecheck(d_predt2,d_minimal,time_slice)) d_predt2 = d_minimal;
          d_predt1 = d_dwelltime - d_predt2;
          if (!rangecheck(d_predt1,d_minimal,time_slice)) 
     	   {  d_predt1 = d_minimal;
	      d_predt2 = d_dwelltime - d_predt1;
	   }
	   n_background = div_f(d_background - d_predt1, d_dwelltime, time_slice);
	   n_backd =  n_background.quot -1 + (n_background.rem > 0);
	   if (n_background.rem > 0) 
              {cm_msg(MINFO,"e1b_compute","Background delay is rounded up to nearest dwelltime boundary.");
	      }
	   ppg.output.e1b_bg_delay__dwelltimes_ = 1 + n_backd;
           ppg.output.e1b_rf_delay__dwelltimes_ = 2 + n_RFd;  
           delay_dwlts = 3 + n_RFd + n_backd;
	}
     }	
      
      if (mod1 == 'x')
       { cm_msg(MERROR,"e1b_compute","mod1 value invalid, check selection code consistency.");   
               return -1;
       }
	 printf(" n_RF_delay.quot =%f n_RF_delay.rem = %f",n_RF_delay.quot ,n_RF_delay.rem);
	 printf(" n_background.quot =%f n_background.rem = %f",n_background.quot ,n_background.rem);
	 printf(" n_RFd =%d n_backd = %d ",n_RFd ,n_backd);
	 printf(" d_predt1 = %f d_predt2 = %f", d_predt1, d_predt2); 
	 printf(" mod1 = %c\n", mod1); 
 
      
     /* C; */
  
  /* -- Computing and writing the output values to the ODB -- */
  
  /* Number of dwelltimes */
  if (strcmp(ppg.input.e1a_and_e1b_freq_mode,"FR")==0)
    { ppg.output.num_dwell_times = ( delay_dwlts + (RF_dwlts * 4) * (n_fcycles+1));}
  else
    { ppg.output.num_dwell_times = ( delay_dwlts + (RF_dwlts * 2) * (n_fcycles+1));}
  
 /* check the number of dwelltimes is valid */
  if ( (INT)ppg.output.num_dwell_times <= 0 )
    {
      cm_msg(MERROR,"e1b_compute","invalid number of dwelltimes calculated (%d); consult an expert",ppg.output.num_dwell_times);   
      return -1;
    
    }

  printf("delay_dwlts=%d  RF_dwlts=%d n_fcycles=%d num_dwell_times=%d\n",delay_dwlts,RF_dwlts,n_fcycles,
	 ppg.output.num_dwell_times);
  /* Beam on time */
  
  if (ppg.input.beam_mode == 'C')
    {
      ppg.output.beam_on_time__ms_= (ppg.output.num_dwell_times) * d_dwelltime;
      ppg.output.vme_beam_control=TRUE;
    }
  else
    {
      ppg.output.beam_on_time__ms_= (n_RFd+1) * d_dwelltime;
      ppg.output.vme_beam_control=FALSE;      
   }
  
  if(!mode2d_flag)
    {
      num_his= ppg.input.num_type1_frontend_histograms;
      /* -- Computing and writing the histogram values to the ODB (1b) -- */
  
  
      if (mod1 == '0')
	{   /*No background delay*/
	  for (j=0; j <  ppg.input.num_type1_frontend_histograms  ; j ++)
	    {fmdarc.histograms.first_background_bin [j] = 0;     /*No background delay*/
	    fmdarc.histograms.last_background_bin  [j] = 0;
	    fmdarc.histograms.bin_zero [j]             = 1;     /*First dwelltime with beam on*/
	    }
	}
      else
	{
	  i = n_backd +1;
	  for (j=0; j <  ppg.input.num_type1_frontend_histograms ; j ++)
	    {fmdarc.histograms.first_background_bin [j] = 1;
	    fmdarc.histograms.last_background_bin  [j] = i;
	    fmdarc.histograms.bin_zero [j]             = i + 1; /*First dwelltime with beam on (in case */
	    }                                                 /*mod1 = '1' dwtime with partial beam on*/
	}
  
      for (j=0; j <  ppg.input.num_type1_frontend_histograms ; j ++)
	{fmdarc.histograms.first_good_bin [j] =   fmdarc.histograms.bin_zero [j];
	fmdarc.histograms.last_good_bin  [j] =   ppg.output.num_dwell_times;
	}
    }
  else
    { /* Mode 2d */
      num_his= ppg.input.num_type2_frontend_histograms;
      for (j=0; j <  num_his  ; j ++)
	{
	  fmdarc.histograms.first_background_bin [j] = 0;
          fmdarc.histograms.last_background_bin  [j] = 0;
	  fmdarc.histograms.bin_zero [j]       = 1;
	  fmdarc.histograms.last_good_bin [j] = ppg.output.num_dwell_times ;

	}
    }
  
  /* -- Final rangechecking and construction of the rulefile -- */
  
  d_dwelltime = d_dwelltime - d_msnp;
  if (!rangecheck (d_dwelltime,d_minimal,time_slice))
    {cm_msg(MERROR,"e1b_compute","Rangecheck-Error for d_dwelltime = %f , but must be > 2* Minimal delay",d_dwelltime);
    return -1;
    }
  
  /* write some information to mdarc area for  e1b */
 fmdarc.histograms.number_defined = num_his ; /*  number of  histograms as written by frontend */
 fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */

  if(!check_sis_test_mode() )   /* real mode (front panel signals, uses ppg and fsc */
   /* The number of bins as calculated by rf_config */
   {
     if(debug)printf("SIS test mode is disabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.output.num_dwell_times ;
   }
 else   /* sis internal  test mode  */  
   {
#ifdef HAVE_SIS3801
     fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.sis3801.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif
#ifdef HAVE_SIS3820
      fmdarc.histograms.dwell_time__ms_ =   10000000/ ppg.sis_test_mode.sis3820.lne_prescale_factor;
      fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif
     for (j=0; j < num_his; j ++)
       {
	 fmdarc.histograms.last_good_bin	[j] =  fmdarc.histograms.num_bins ;
	 fmdarc.histograms.last_background_bin  [j] = 0;
       }
   }


 if(!mode2d_flag)
   { /* file midbnmr parameters */
     if(fmdarc.histograms.midbnmr.use_defaults)   
       {
	 printf("e1b_compute: using default values for midbnmr parameters\n");
	 /*  write defaults for midbnmr */
	 j=0;
	 if( ppg.output.e1b_bg_delay__dwelltimes_ > 0)  
	   {
	     sprintf(fmdarc.histograms.midbnmr.range_label [j],"BgDelay");
	     fmdarc.histograms.midbnmr.first_time_bin[j] =1;
	     fmdarc.histograms.midbnmr.last_time_bin[j] = ppg.output.e1b_bg_delay__dwelltimes_;
	     j++;
	   }
	 if( ppg.output.e1b_rf_delay__dwelltimes_ > 0)  
	   {  
	     sprintf(fmdarc.histograms.midbnmr.range_label [j],"RFdelay");
	     fmdarc.histograms.midbnmr.first_time_bin[j] = ppg.output.e1b_bg_delay__dwelltimes_ +1;
	     fmdarc.histograms.midbnmr.last_time_bin[j] =  
	       (ppg.output.e1b_bg_delay__dwelltimes_ +1 +ppg.output.e1b_rf_delay__dwelltimes_);
	     j++;
	   }
	 
	 /* last region */
	 sprintf(fmdarc.histograms.midbnmr.range_label [j],"RFcycl");
	 fmdarc.histograms.midbnmr.first_time_bin[j] =  fmdarc.histograms.midbnmr.last_time_bin[j-1] +1;
	 fmdarc.histograms.midbnmr.last_time_bin[j] =fmdarc.histograms.num_bins ;
	 j++;
	 
	 fmdarc.histograms.midbnmr.number_of_regions =j;
	 
	 /* clean out the unused regions */
	 for (j= fmdarc.histograms.midbnmr.number_of_regions; j <  MAX_MIDBNMR_REGIONS; j++)
	   {
	     sprintf(fmdarc.histograms.midbnmr.range_label [j]," ");
	     fmdarc.histograms.midbnmr.first_time_bin[j] =  0;
	     fmdarc.histograms.midbnmr.last_time_bin[j] = 0 ;
	   }

	 fmdarc.histograms.midbnmr.start_freq__hz_ = ppg.input.frequency_start__hz_;
	 fmdarc.histograms.midbnmr.end_freq__hz_ = ppg.input.frequency_stop__hz_;
 

	 for(j=0; j <    fmdarc.histograms.midbnmr.number_of_regions; j++)
	   {
	     printf("  Midbnmr range label %d       : %s\n", j,fmdarc.histograms.midbnmr.range_label[j] );
	     printf("  Midbnmr first time bin %d    : %d\n", j,fmdarc.histograms.midbnmr.first_time_bin[j] );
	     printf("  Midbnmr last time bin  %d    : %d\n", j,fmdarc.histograms.midbnmr.last_time_bin[j] );
	   }
	 printf("  Midbnmr start_freq (Hz)   : %d\n", fmdarc.histograms.midbnmr.start_freq__hz_  );
	 printf("  Midbnmr end_freq   (Hz)   : %d\n", fmdarc.histograms.midbnmr.end_freq__hz_  );
       }
     else
       {
	 if(check_regions() < 0)
	   {
	     printf("e1b_compute: error return from check_regions for midbnmr parameters\n");
	     return -1;
	   }
	 printf("e1b_compute: success from checking values for midbnmr parameters\n");
       }
   }
 if(debug)
   {
     printf("Writing values to mdarc area:\n");
     printf("  Number frontend histograms: %d\n", fmdarc.histograms.number_defined);
     printf("  Number of bins            : %d\n", fmdarc.histograms.num_bins);
     printf("  Dwell time (ms)           : %f\n", fmdarc.histograms.dwell_time__ms_);
   }
 
 cm_msg(MINFO,"e1b_compute","Computing values and writing to the ODB done, writing rulefile.");
 
 
 sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");
 if(debug)printf("e1b_compute: about to open rulefile: %s\n",rulefile_name);
 
 rulef = fopen(rulefile_name, "w");
 if (rulef) {
   sprintf(str,"%s_%c%s%s%c%c%s",ppg_mode, ppg.input.beam_mode, ppg.input.e1a_and_e1b_pulse_pairs,  
	   ppg.input.e1a_and_e1b_freq_mode ,mod1, mod2,".ppg\n"); /* add an underscore after ppg_mode */
   {
     int len,size;

     if(mode2d_flag)
       sprintf(str,"%s_%c%s%s%c%c%s","1b", ppg.input.beam_mode, ppg.input.e1a_and_e1b_pulse_pairs,  
	       ppg.input.e1a_and_e1b_freq_mode ,mod1, mod2,".ppg\n"); /* add an underscore after ppg_mode */
     
     len=strlen(str);
     size=sizeof(ppg.output.ppg_template);
     strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */  
     ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file
   }
    
    if (debug) printf("e1b_compute: writing ppg template file name (%s) into rulefile\n", ppg.output.ppg_template);

    fputs(str,rulef);
    sprintf(str,"%s%s",ppg_mode,".ppg\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dwelltime=0","%","d_dwelltime=",d_dwelltime,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_msnp=0","%","d_msnp=",d_msnp,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dac_service=0","%","d_dac_service=",d_dac_service,"ms\n");
    fputs(str,rulef);
    if (d_predt1 < d_minimal) d_predt1 = d_minimal;
    sprintf(str,"%s%s%s%f%s","d_predt1=0","%","d_predt1=",d_predt1,"ms\n");
    fputs(str,rulef);
    if (d_predt2 < d_minimal) d_predt2 = d_minimal;
    sprintf(str,"%s%s%s%f%s","d_predt2=0","%","d_predt2=",d_predt2,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dt1=0","%","d_dt1=",d_dt1,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dt2=0","%","d_dt2=",d_dt2,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_main","%",n_main,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_fcycles","%",n_fcycles,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_backd","%",n_backd,"\n");
    fputs(str,rulef);

    printf("n_RFd = %d (%u)\n",n_RFd,n_RFd);

    sprintf(str,"%s%s%d%s","n_RFd","%",n_RFd,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_pulse","%",n_pulse,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_nopulse","%",n_nopulse,"\n");
    fputs(str,rulef);
    
    fclose (rulef);
  }
  
  else {
    cm_msg(MERROR,"e1b_compute","can't open rule file %s", rulefile_name);
    return -1;
  }
  cm_msg(MINFO,"e1b_compute","Rulefile %s written.",rulefile_name);
  
  return 1;
}

/*------------------------------------------------------------------*/
INT e1f_compute(char *ppg_mode)
{
  FILE  *rulef;
  char  str [256];
  char  rulefile_name[256];

  double d_dac_service;
  double d_msnp;
  double d_count; 
  double d_minimal;
  INT n_main_lc; 
  INT n_main_less;
  int i,j;
  INT status;
  int num_his;
#ifdef HAVE_SIS3820
  BOOL alpha_flag; 
  alpha_flag=FALSE;
#endif
 
  printf("ppg_mode=%s\n",ppg_mode);
  debug=1;
  if(debug)printf("e1f_compute is starting with ppg mode %s\n",ppg_mode);
  ppg.output.vme_beam_control=TRUE; /* continuous beam, VME control */

#ifdef BNQR
#ifdef HAVE_SIS3820
  alpha_flag = ppg.input.e1f_enable_alpha_histos;
#endif
#endif

#ifdef PSM
  // Look for modes that don't use PSM (i.e. 10) but do use this code
  if (strncmp(ppg.input.experiment_name,"10",2) ==0)
    printf("Detected Actual PPG Mode as \"10\". No RF for this mode\n");
  else
    {
      /* check only one gate parameter is selected per profile */
#ifndef PSMIII  // check combined in check_psm_channel
      if(check_psm_gate() != SUCCESS)
	return -1;
#endif
      if( check_psm_quad(ppg_mode)!= SUCCESS) // may call build_iq_table_psm for quad mode 
	{
	  cm_msg(MERROR,"e1f_compute","error return from check_psm_quad");
	  return -1; 
	}
    }
#endif

  /* Compute necessary values */
  d_minimal        =    ppg.input.minimal_delay__ms_;
  d_msnp           =    d_minimal;
  d_count          =    ppg.input.mcs_enable_gate__ms_ - d_minimal;
  //  d_dac_service    =    ppg.input.daq_service_time__ms_;

#ifdef HAVE_SIS3820
  n_main_less=3; // Loop counter should be 3 less for SIS3820 when using mode with empty 1st bin
  
  n_main_lc  =    ppg.input.e1f_num_dwell_times - n_main_less;

  #ifdef ARM
  n_main_lc++;  // when using ARM
  #endif

  if(ppg.hardware.sis3820.discard_first_bin ) 
    n_main_lc++; // one extra bin
#else // HAVE_SIS3801
  n_main_less=2; //  Loop counter should be 2 less for SIS3801
  n_main_lc  =    ppg.input.e1f_num_dwell_times - n_main_less; 
#endif


  if(n_main_lc < n_main_less)
    {
      cm_msg(MERROR,"e1f_compute","number of dwell times must be at least %d",n_main_less);
      return -1; 
    }

  ppg.output.num_dwell_times = n_main_lc + n_main_less;
  ppg.output.dwell_time__ms_  = ppg.input.mcs_enable_gate__ms_;


  ppg.output.rf_on_time__ms_  = 0; /* not used for 1f */
  ppg.output.rf_off_time__ms_ = 0;


  /* Jun 09
     Mode 1f with constant time between cycles: 

        This mode to use computer busy and a large loop so
        PPG is started once only; uses ppg-template 1f_loop.ppg  
 
        Make sure daq service time is at least the minimal delay or comp_int.pl fails...
        dac service time is - no longer (Jun 09) - set to 0 in the template file  

     Regular Mode 1f uses template 1f_.ppg and is restarted each cycle  */


  d_dac_service = d_minimal; // default - set dac service time to d_mininal

  if (strncmp(ppg.input.experiment_name,"1f",2) ==0)
    {  // If the mode is 1f (other modes may use this code)
      if(ppg.input.e1f_const_time_between_cycles)  // 1f special mode that uses dac service time (ppg started once)
	{
	  printf("\n\n ** Const 1f mode detected **\n");

	  d_dac_service    =    ppg.input.daq_service_time__ms_;   // float
		 printf("Daq service time for const 1f mode is %f ms (minimum %f ms)\n",
			d_dac_service,ppg.input.e1f_min_daq_service__ms_ );  // float
	  if(d_dac_service < ppg.input.e1f_min_daq_service__ms_ )
	    { // no point in writing it to input as parameter is overwritten by mode parameter every BOR
	      cm_msg(MINFO,"e1f_compute","setting DAQ service time in PPG (%f) to minimum value of %f",
		     d_dac_service, ppg.input.e1f_min_daq_service__ms_);
	      d_dac_service =  ppg.input.e1f_min_daq_service__ms_;
	    }
	    
	  printf("Daq service time for const 1f mode is %f\n",d_dac_service);  // float

	  if(ppg.input.num_cycles_per_supercycle != 1)
	    {
	      cm_msg(MINFO,"e1f_compute","setting number of cycles per supercycle to 1 for 1f mode with constant time between cycles");
	      ppg.input.num_cycles_per_supercycle = 1;
	      status =  db_set_value(hDB, hIn, "num cycles per supercycle", &ppg.input.num_cycles_per_supercycle, sizeof(ppg.input.num_cycles_per_supercycle), 1, TID_INT);
	      if (status != DB_SUCCESS)
		{
		  cm_msg(MERROR, "e1f_compute", "cannot set num cycles per supercycle to %d at path \"../input/num cycles per supercycle\" (%d) ",
			 ppg.input.num_cycles_per_supercycle, status);
		  return status;
		}
	    }
	}
    }

 /* -- Calculating and writing the ODB histogram information -- */

 /* write some information to mdarc area for e1f */
    num_his = ppg.input.num_type1_frontend_histograms ; /*  number of  histograms as written by frontend */
#ifdef BNQR
#ifdef HAVE_SIS3820
  if (alpha_flag)
    num_his = ppg.input.num_type1_fe_histos_alpha_mode; // number with alpha histograms
#endif
#endif


  fmdarc.histograms.number_defined = num_his;
  fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */

 i =  ppg.output.num_dwell_times;
 for (j=0; j <= (num_his - 1); j ++)
   {
     fmdarc.histograms.bin_zero             [j] = 1;
     fmdarc.histograms.first_good_bin       [j] = 1;
     fmdarc.histograms.first_background_bin [j] = 0;
     fmdarc.histograms.last_background_bin  [j] = 0;
     fmdarc.histograms.last_good_bin	  [j] = i;
   }

  if( ! check_sis_test_mode() )  /* real mode (front panel signals, uses ppg and fsc */
   /* The number of bins as calculated by rf_config */
   {
     if(debug)printf("SIS test mode is disabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.output.num_dwell_times ;
   }
 else   /* sis internal  test mode  */  
   {
#ifdef HAVE_SIS3801
     fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.sis3801.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif
#ifdef HAVE_SIS3820
      fmdarc.histograms.dwell_time__ms_ =   10000000/ ppg.sis_test_mode.sis3820.lne_prescale_factor;
      fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif
     for (j=0; j <  ppg.input.num_type1_frontend_histograms; j ++)
	 fmdarc.histograms.last_good_bin	[j] =  fmdarc.histograms.num_bins ;
   }

 if(fmdarc.histograms.midbnmr.use_defaults)   
   {
     printf("e1f_compute: using default values for midbnmr parameters\n");
     /*  write defaults for midbnmr */
     fmdarc.histograms.midbnmr.number_of_regions =1;
     //sprintf(fmdarc.histograms.midbnmr.range_label [0],""); 
     fmdarc.histograms.midbnmr.range_label [0][0]='\0';
     fmdarc.histograms.midbnmr.last_time_bin[0] = fmdarc.histograms.num_bins;
     fmdarc.histograms.midbnmr.first_time_bin[0] =1;
    /* clean out the unused regions */
    for (j= fmdarc.histograms.midbnmr.number_of_regions; j < MAX_MIDBNMR_REGIONS ; j++)
      {
	//sprintf(fmdarc.histograms.midbnmr.range_label [j],"");
	fmdarc.histograms.midbnmr.range_label [j][0] = '\0';
	fmdarc.histograms.midbnmr.first_time_bin[j] =  0;
	fmdarc.histograms.midbnmr.last_time_bin[j] = 0 ;
      }
     fmdarc.histograms.midbnmr.start_freq__hz_ = ppg.input.frequency_start__hz_;
     fmdarc.histograms.midbnmr.end_freq__hz_ = ppg.input.frequency_stop__hz_;
     printf("  Midbnmr range label       : %s\n", fmdarc.histograms.midbnmr.range_label[0] );
     printf("  Midbnmr first time bin    : %d\n", fmdarc.histograms.midbnmr.first_time_bin[0] );
     printf("  Midbnmr last time bin     : %d\n", fmdarc.histograms.midbnmr.last_time_bin[0] );
     printf("  Midbnmr start_freq (Hz)   : %d\n", fmdarc.histograms.midbnmr.start_freq__hz_  );
     printf("  Midbnmr end_freq   (Hz)   : %d\n", fmdarc.histograms.midbnmr.end_freq__hz_  );
   }

 else
   {
   if(check_regions() < 0)
     {
       printf("e1f_compute: error return from check_regions for midbnmr parameters\n");
       return -1;
     }
   printf("e1f_compute: success from checking values for midbnmr parameters\n");
   }
 if(debug)
   {
     printf("Writing values to mdarc area:\n");
     printf("  Number defined histograms: %d\n", fmdarc.histograms.number_defined);
     printf("  Number of bins           : %d\n", fmdarc.histograms.num_bins);
     printf("  Dwell time (ms)          : %f\n", fmdarc.histograms.dwell_time__ms_);
   }
 
 /* -- Writing the rulefile -- */

  cm_msg(MINFO,"e1f_compute","Computing values and writing to the ODB done, writing rulefile.");
  

  sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");
  if(debug)printf("e1f_compute: about to open rulefile: %s\n",rulefile_name);

  rulef = fopen(rulefile_name, "w");
  if (rulef) {



    if(ppg.input.e1f_const_time_between_cycles)
      sprintf(str,"%s_loop%s",ppg_mode,".ppg\n"); /* add an underscore and "loop" after ppg_mode */
    else
      sprintf(str,"%s_%s",ppg_mode,".ppg\n"); /* add an underscore after ppg_mode */

    printf("e1f_compute: Using template %s\n",str);
    {
      int len,size;
     
      len=strlen(str);
      size=sizeof(ppg.output.ppg_template);
      strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */  
      ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file
    }
    if (debug) printf("e1f_compute: writing ppg template file name (%s) into rulefile\n", ppg.output.ppg_template);
    fputs(str,rulef);
    sprintf(str,"%s%s",ppg_mode,".ppg\n");
    fputs(str,rulef);

    sprintf(str,"%s%s%s%f%s","d_msnp=0","%","d_msnp=",d_msnp,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_count=0","%","d_count=",d_count,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dac_service=0","%","d_dac_service=",d_dac_service,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_main_less_two","%",n_main_lc,"\n");
    fputs(str,rulef);
    fclose (rulef);
  }
  else   {
    cm_msg(MERROR,"e1f_compute","can't open rule file %s", rulefile_name);
    return -1;
  }

  cm_msg(MINFO,"e1f_compute","Rulefile %s written.",rulefile_name);

  return 1;
}  

//----------------------------------------------------------------------------------------------
#ifdef CAMP
INT e1j_compute(char *ppg_mode)
{   
/* 1j  (LCR mode) is a combination of 20 and 1c modes */

  INT status;

  if(debug)printf("e1j_compute is starting with ppg mode %s\n",ppg_mode);
  printf("e1j_compute: calling e1c_compute to set up CAMP\n");
  status = e1c_compute(ppg_mode);
  if(status != 1)
    {
      printf("e1j_compute: error returned from e1c_compute (%d)\n",status);
      return -1; // camp is down
    }
  /* use these for scan start and stop */
  fmdarc.histograms.midbnmr.start_freq__hz_ = (INT)(0.49 + (ppg.input.e1c_camp_start *  camp_params.conversion_factor));
  fmdarc.histograms.midbnmr.end_freq__hz_ = (INT) (0.49 + (ppg.input.e1c_camp_stop *  camp_params.conversion_factor));

  status = e1g_compute(ppg_mode);
  if(status != 1)
    {
      printf("e1j_compute: error returned from e1g_compute\n");
      return -1;
    }
  else
    printf("e1j_compute: returns success\n");
  return 1;  
}

INT e1c_compute(char *ppg_mode)
{   /* also called from 1j (combination of 20 and 1c modes) */

  FILE  *rulef;
  char  str [256];
  char  rulefile_name[256];

  double d_dac_service;
  double d_msnp;
  double d_count; 
  double d_minimal;
  INT n_main_less_two; 
  int i,j,status;

  if(debug)printf("e1c_compute is starting with ppg mode %s\n",ppg_mode);
  ppg.output.vme_beam_control=TRUE; /* continuous beam, VME control */

  /* 1c uses CAMP */

  /* camp handle and record should have been got already */
  if(!hCamp)
    {
      cm_msg(MINFO,"e1c_compute","Error: no CAMP settings available");
      return(-1);
    }

  // general camp device

  strcpy( ppg.output.e1c_camp_path,             fcamp.camp_path);
  strcpy( ppg.output.e1c_camp_ifmod,            fcamp.gpib_port_or_rs232_portname);
  strcpy( ppg.output.e1c_camp_instr_type,  fcamp.instrument_type);
 

  if(strlen( ppg.output.e1c_camp_path) == 0)
    {
      cm_msg(MINFO,"e1c_compute","Error: CAMP path for Sweep Device is blank");
      return(-1);
    }

 /* Check there is a valid camp hostname */
  check_camp(); // sets camp_available and serverName
  if (!camp_available)
    return DB_INVALID_PARAM;
    
  /* We have a valid CAMP hostname - but is it active?  */
  status = ping_camp();
  if (status == DB_NO_ACCESS)
    {
      cm_msg(MERROR,"frontend_init","Camp host %s is unreachable. Camp data cannot be saved",
	     serverName);
      return status;
    }
  else
    if (debug) printf ("Camp host IS responding to ping\n");

  
  /* fill structure camp_params */
  strcpy(camp_params.SweepDevice, "general");  // general camp device
  strncpy(camp_params.serverName,serverName,LEN_NODENAME ); 

  
  strncpy(camp_params.InsPath, fcamp.camp_path,32); // add max length
  strncpy(camp_params.InsType,  fcamp.instrument_type,32);
  strncpy(camp_params.IfMod,  fcamp.gpib_port_or_rs232_portname,10);
  strncpy(camp_params.setPath,  fcamp.camp_scan_path,80); 
  strncpy(camp_params.units,  fcamp.scan_units,9);  
  camp_params.maximum_value = fcamp.maximum;
  camp_params.minimum_value = fcamp.minimum;
  camp_params.conversion_factor = fcamp.integer_conversion_factor;
  strcpy(camp_params.DevDepPath, ""); // not used
    

  if(debug)
    {
      printf("e1c_compute:  camp parameter settings:\n");
      printf("    host: %s\n",camp_params.serverName);
      printf("    sweep device: %s\n",camp_params.SweepDevice);
      printf("    InsPath: %s\n",camp_params.InsPath);
      printf("    InsType: %s\n",camp_params.InsType);
      printf("    IfMod: %s\n",camp_params.IfMod);
      printf("    setPath: %s\n",camp_params.setPath);
      printf("    units: %s\n",camp_params.units);
      printf("    max: %f\n",camp_params.maximum_value);
      printf("    min: %f\n",camp_params.minimum_value);
    }

  /* One further check - check setPath contains string InsPath */

  /* now exercise CAMP and make sure device is available */
  status = init_sweep_device(camp_params);
  if(status != CAMP_SUCCESS)
    {
      printf("e1c_compute: failed to initialize camp instrument via init_sweep_device (%d)\n",status);
      cm_msg(MERROR,"e1c_compute"," failed to initialize camp instrument %s",camp_params.InsPath);
      camp_end(); //  disconnect from CAMP 
      return(-1);
    }
  if(debug)printf("e1c_compute: Success returned from camp (init_sweep_device) %d \n",status);

  camp_end(); //  disconnecting from CAMP 
  printf("Disconnected from camp\n");

  /* check that the sweep parameters are valid - 
     camp_init may have read them from the device and reset them  */

  if (  ppg.input.e1c_camp_start < camp_params.minimum_value ||
	ppg.input.e1c_camp_start > camp_params.maximum_value)
    {
      cm_msg(MERROR,"e1c_compute","Camp scan start value %f is out of range; (maximum and minimum for device %s: %f and %f %s ",
	      ppg.input.e1c_camp_start, camp_params.SweepDevice, camp_params.maximum_value, camp_params.minimum_value, camp_params.units);
      return(-1);
    }
  else if (  ppg.input.e1c_camp_stop < camp_params.minimum_value ||
	     ppg.input.e1c_camp_stop > camp_params.maximum_value)
    {
      cm_msg(MERROR,"e1c_compute","Camp scan stop value %.2f is out of range; (maximum and minimum for device %s: %.2f and %.2f %s ",
	      ppg.input.e1c_camp_stop, camp_params.SweepDevice, camp_params.maximum_value, camp_params.minimum_value, camp_params.units);
      return(-1);
    }


#ifdef CAMPTEST
  // TESTING ONLY - the frontend will  use camp_initPath -  put an ifdef around it
  printf("Waiting for 20s, then trying to scan camp (frontend will do this) .......\n");
  ss_sleep(20000);
  status=camp_initPath(camp_params);
  if(status != CAMP_SUCCESS)
    {
      printf("e1c_compute: Error returned from camp_check_path \n");
      camp_end(); //  disconnect from CAMP 
      return -1;
    }
  else
    printf("e1c_compute: Success returned from camp_initPath \n");
  {
    float val;
    status = read_sweep_device(&val, camp_params);
    if(status != CAMP_SUCCESS)
      {
	printf("e1c_compute: Error returned from read_sweep_device \n");
	camp_end(); //  disconnect from CAMP 
	return -1;
      }
    else
      printf("e1c_compute: Success from read_sweep_device value read =%f \n",val);
  }
  status = set_sweep_device(2000, camp_params);
  if(status != CAMP_SUCCESS)
    {
      printf("e1c_compute: Error returned from set_sweep_device \n");
      camp_end(); //  disconnect from CAMP 
      return -1;
    }
  
  printf("e1c_compute: Success returned from set_sweep_device \n");
  printf("e1c_compute: camp device now set to 2000 \n");   
  camp_end(); //  disconnect from CAMP 
#endif // end of CAMPTEST

  if(strcmp (ppg_mode,"1j")==0)
    {
      printf("e1c_compute: returning for mode 1j after setting up CAMP\n");
      return 1;  // CAMP is now set up for 1j
    }


  cm_msg(MINFO,"e1c_compute"," *** INFORMATION  *** camp ppg file is same as  1f");
    


  /*  TEMP       THIS IS ALL  copied from 1f : */
  /* Compute necessary values */
  d_minimal        =    ppg.input.minimal_delay__ms_;
  d_msnp           =    d_minimal;
  d_count          =    ppg.input.mcs_enable_gate__ms_ - d_minimal;
  d_dac_service    =    ppg.input.daq_service_time__ms_;
  n_main_less_two  =    ppg.input.e1f_num_dwell_times -2; 
  if(n_main_less_two < 0)
    {
      cm_msg(MERROR,"e1c_compute","number of dwell times must be at least 2");
	  return -1; 
    }

  ppg.output.num_dwell_times = n_main_less_two + 2;
  ppg.output.dwell_time__ms_  = ppg.input.mcs_enable_gate__ms_;


  ppg.output.rf_on_time__ms_  = 0; /* not used for 1c */
  ppg.output.rf_off_time__ms_ = 0;

  /* make sure daq service time is at least the minimal delay or comp_int.pl fails...
     dac service time is set to 0 in the template file  */
  //  if(d_dac_service <=0)  //  set dac service time to d_mininal  (not used)
    d_dac_service = d_minimal;

 /* -- Calculating and writing the ODB histogram information -- */

 /* write some information to mdarc area for e1c */
 fmdarc.histograms.number_defined = ppg.input.num_type1_frontend_histograms ; /*  number of  histograms as written by frontend */
 fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */

 i =  ppg.output.num_dwell_times;
 for (j=0; j <= (fmdarc.histograms.number_defined-1); j ++)
   {
     fmdarc.histograms.bin_zero             [j] = 1;
     fmdarc.histograms.first_good_bin       [j] = 1;
     fmdarc.histograms.first_background_bin [j] = 0;
     fmdarc.histograms.last_background_bin  [j] = 0;
     fmdarc.histograms.last_good_bin	  [j] = i;
   }


    if( ! check_sis_test_mode() )    /* real mode (front panel signals, uses ppg and fsc */
   /* The number of bins as calculated by rf_config */
   {
     if(debug)printf("SIS test mode is disabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.output.num_dwell_times ;
   }
 else   /* sis internal  test mode  */  
   {
#ifdef HAVE_SIS3801
     fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.sis3801.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif
#ifdef HAVE_SIS3820
      fmdarc.histograms.dwell_time__ms_ =   10000000/ ppg.sis_test_mode.sis3820.lne_prescale_factor;
      fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif
     for (j=0; j <  ppg.input.num_type1_frontend_histograms; j ++)
	 fmdarc.histograms.last_good_bin	[j] =  fmdarc.histograms.num_bins ;
   }


if(fmdarc.histograms.midbnmr.use_defaults)   
   {
     printf("e1c_compute: using default values for midbnmr parameters\n");
     /*  write defaults for midbnmr */
     fmdarc.histograms.midbnmr.number_of_regions =1;
     //sprintf(fmdarc.histograms.midbnmr.range_label [0],"");
     fmdarc.histograms.midbnmr.range_label [0][0] = '\0';
     fmdarc.histograms.midbnmr.last_time_bin[0] = fmdarc.histograms.num_bins;
     fmdarc.histograms.midbnmr.first_time_bin[0] =1;
    /* clean out the unused regions */
    for (j= fmdarc.histograms.midbnmr.number_of_regions; j < MAX_MIDBNMR_REGIONS ; j++)
      {
	//sprintf(fmdarc.histograms.midbnmr.range_label [j],"");
	fmdarc.histograms.midbnmr.range_label [j][0] = '\0';
	fmdarc.histograms.midbnmr.first_time_bin[j] =  0;
	fmdarc.histograms.midbnmr.last_time_bin[j] = 0 ;
      }
     fmdarc.histograms.midbnmr.start_freq__hz_ = (INT)ppg.input.e1c_camp_start;
     fmdarc.histograms.midbnmr.end_freq__hz_ = (INT)ppg.input.e1c_camp_stop;
     printf("  Midbnmr range label       : %s\n", fmdarc.histograms.midbnmr.range_label[0] );
     printf("  Midbnmr first time bin    : %d\n", fmdarc.histograms.midbnmr.first_time_bin[0] );
     printf("  Midbnmr last time bin     : %d\n", fmdarc.histograms.midbnmr.last_time_bin[0] );
     printf("  Midbnmr scan start value       : %d\n", fmdarc.histograms.midbnmr.start_freq__hz_  );
     printf("  Midbnmr scan end value         : %d\n", fmdarc.histograms.midbnmr.end_freq__hz_  );
   }
 else
   {
   if(check_regions() < 0)
     {
       printf("e1c_compute: error return from check_regions for midbnmr parameters\n");
       return -1;
     }
   printf("e1c_compute: success from checking values for midbnmr parameters\n");
   }

 if(debug)
   {
     printf("Writing values to mdarc area:\n");
     printf("  Number defined histograms: %d\n", fmdarc.histograms.number_defined);
     printf("  Number of bins           : %d\n", fmdarc.histograms.num_bins);
     printf("  Dwell time (ms)          : %f\n", fmdarc.histograms.dwell_time__ms_);
   }
 
 /* -- Writing the rulefile -- */

  cm_msg(MINFO,"e1c_compute","Computing values and writing to the ODB done, writing rulefile.");
  
  sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");
  if(debug)printf("e1c_compute: about to open rulefile: %s\n",rulefile_name);

  rulef = fopen(rulefile_name, "w");
  if (rulef) {
    sprintf(str,"%s_%s",ppg_mode,".ppg\n"); /* add an underscore after ppg_mode */
     {
      int len,size;
     
      len=strlen(str);
      size=sizeof(ppg.output.ppg_template);
      strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */  
      ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file
    }
    if (debug) printf("e1c_compute: writing ppg template file name (%s) into rulefile\n", ppg.output.ppg_template);
    fputs(str,rulef);
    sprintf(str,"%s%s",ppg_mode,".ppg\n");
    fputs(str,rulef);

    sprintf(str,"%s%s%s%f%s","d_msnp=0","%","d_msnp=",d_msnp,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_count=0","%","d_count=",d_count,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dac_service=0","%","d_dac_service=",d_dac_service,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_main_less_two","%",n_main_less_two,"\n");
    fputs(str,rulef);
    fclose (rulef);
  }
  else   {
    cm_msg(MERROR,"e1c_compute","can't open rule file %s", rulefile_name);
    return -1;
  }

  cm_msg(MINFO,"e1c_compute","Rulefile %s written.",rulefile_name);

  return 1;
}  
#endif   // CAMP



/*------------------------------------------------------------------*/
INT e1n_compute(char *ppg_mode)
{
  FILE  *rulef;
  char  str [256];
  char  rulefile_name[256];

  double d_dac_service;
  double d_msnp;
  double d_count; 
  double d_minimal;
  INT n_main_less_two; 
  int i,j;
  
  if(debug)printf("e1n_compute is starting with input ppg_mode=%s and epics device: %s\n",
		  ppg_mode,ppg.output.e1n_epics_device);
  ppg.output.vme_beam_control=TRUE; /* continuous beam, VME control */

  /* Compute necessary values */
  d_minimal        =    ppg.input.minimal_delay__ms_;
  d_msnp           =    d_minimal;
  d_count          =    ppg.input.mcs_enable_gate__ms_ - d_minimal;
  d_dac_service    =    ppg.input.daq_service_time__ms_;
  n_main_less_two  =    ppg.input.e1f_num_dwell_times -2; 
 if(n_main_less_two < 0)
    {
      cm_msg(MERROR,"e1n_compute","number of dwell times must be at least 2");
	  return -1; 
    }
  ppg.output.num_dwell_times = n_main_less_two + 2;
  ppg.output.dwell_time__ms_  = ppg.input.mcs_enable_gate__ms_;

  ppg.output.rf_on_time__ms_  = 0; /* clear these output parameters */
  ppg.output.rf_off_time__ms_ = 0; /* not relevent for 1n */

  /* make sure daq service time is at least the minimal delay or comp_int.pl fails...
     dac service time is set to 0 in the template file  */

  // if(d_dac_service <=0) // default - set dac service time to d_mininal
    d_dac_service = d_minimal;

 /* -- Calculating and writing the ODB histogram information -- */

 /* write some information to mdarc area for e1n */
 fmdarc.histograms.number_defined = ppg.input.num_type1_frontend_histograms ; /*  number of  histograms as written by frontend */
 fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */

 i =  ppg.output.num_dwell_times;
 for (j=0; j <= (fmdarc.histograms.number_defined-1); j ++)
   {
     fmdarc.histograms.bin_zero             [j] = 1;
     fmdarc.histograms.first_good_bin       [j] = 1;
     fmdarc.histograms.first_background_bin [j] = 0;
     fmdarc.histograms.last_background_bin  [j] = 0;
     fmdarc.histograms.last_good_bin	  [j] = i;
   }


    if( ! check_sis_test_mode() )   /* real mode (front panel signals, uses ppg and fsc */
   /* The number of bins as calculated by rf_config */
   {
     if(debug)printf("SIS test mode is disabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.output.num_dwell_times ;
   }
 else   /* sis internal  test mode  */  
   {
#ifdef HAVE_SIS3801
     fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.sis3801.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif
#ifdef HAVE_SIS3820
      fmdarc.histograms.dwell_time__ms_ =   10000000/ ppg.sis_test_mode.sis3820.lne_prescale_factor;
      fmdarc.histograms.num_bins = ppg.sis_test_mode.sis3801.num_bins;
#endif
     for (j=0; j <  ppg.input.num_type1_frontend_histograms; j ++)
	 fmdarc.histograms.last_good_bin	[j] =  fmdarc.histograms.num_bins ;
   }

 if(fmdarc.histograms.midbnmr.use_defaults)   
   {
     printf("e1n_compute: using default values for midbnmr parameters\n");
     /*  write defaults for midbnmr */
     fmdarc.histograms.midbnmr.number_of_regions =1;
     //sprintf(fmdarc.histograms.midbnmr.range_label [0],"");
     fmdarc.histograms.midbnmr.range_label [0][0] = '\0';
     fmdarc.histograms.midbnmr.last_time_bin[0] = fmdarc.histograms.num_bins;
     fmdarc.histograms.midbnmr.first_time_bin[0] =1;

    /* clean out the unused regions */
    for (j= fmdarc.histograms.midbnmr.number_of_regions; j <  MAX_MIDBNMR_REGIONS; j++)
      {
	//sprintf(fmdarc.histograms.midbnmr.range_label [j],"");
	fmdarc.histograms.midbnmr.range_label [j][0] = '\0';
	fmdarc.histograms.midbnmr.first_time_bin[j] =  0;
	fmdarc.histograms.midbnmr.last_time_bin[j] = 0 ;
      }

    if (strcmp(ppg_mode,"1n") == 0) {
     fmdarc.histograms.midbnmr.start_freq__hz_ = (INT) ppg.input.navolt_start;
     fmdarc.histograms.midbnmr.end_freq__hz_ = (INT) ppg.input.navolt_stop;
    } else if (strcmp(ppg_mode,"1e") == 0) {
        fmdarc.histograms.midbnmr.start_freq__hz_ = (INT) ppg.input.field_start;
        fmdarc.histograms.midbnmr.end_freq__hz_ = (INT) ppg.input.field_stop;
    } else {
        printf("e1n_compute: Unexpected PPG mode %s\n",ppg_mode);
    	return -1;
    }
     printf("  Midbnmr range label       : %s\n", fmdarc.histograms.midbnmr.range_label[0] );
     printf("  Midbnmr first time bin    : %d\n", fmdarc.histograms.midbnmr.first_time_bin[0] );
     printf("  Midbnmr last time bin     : %d\n", fmdarc.histograms.midbnmr.last_time_bin[0] );
     printf("  Midbnmr start (volts)     : %d\n", fmdarc.histograms.midbnmr.start_freq__hz_  );
     printf("  Midbnmr stop  (volts)     : %d\n", fmdarc.histograms.midbnmr.end_freq__hz_  );
   }
 else
   {
   if(check_regions() < 0)
     {
       printf("e1n_compute: error return from check_regions for midbnmr parameters\n");
       return -1;
     }
   printf("e1n_compute: success from checking values for midbnmr parameters\n");
   }

 if(debug)
   {
     printf("Writing values to mdarc area:\n");
     printf("  Number defined histograms: %d\n", fmdarc.histograms.number_defined);
     printf("  Number of bins           : %d\n", fmdarc.histograms.num_bins);
     printf("  Dwell time (ms)          : %f\n", fmdarc.histograms.dwell_time__ms_);
   }
 
 /* -- Writing the rulefile -- */

  cm_msg(MINFO,"e1n_compute","Computing values and writing to the ODB done, writing rulefile.");
  
  sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");
  if(debug)printf("e1n_compute: about to open rulefile: %s\n",rulefile_name);

  rulef = fopen(rulefile_name, "w");
  if (rulef) {
    sprintf(str,"%s_%s",ppg_mode,".ppg\n"); /* add an underscore after ppg_mode */
    {
      int len,size;
     
      len=strlen(str);
      size=sizeof(ppg.output.ppg_template);
      strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */  
      ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file
    }
    if (debug) printf("e1n_compute: writing ppg template file name (%s) into rulefile\n", ppg.output.ppg_template);

    fputs(str,rulef);
    sprintf(str,"%s%s",ppg_mode,".ppg\n");
    fputs(str,rulef);

    sprintf(str,"%s%s%s%f%s","d_msnp=0","%","d_msnp=",d_msnp,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_count=0","%","d_count=",d_count,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dac_service=0","%","d_dac_service=",d_dac_service,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_main_less_two","%",n_main_less_two,"\n");
    fputs(str,rulef);
    fclose (rulef);
  }
  else   {
    cm_msg(MERROR,"e1n_compute","can't open rule file %s", rulefile_name);
    return -1;
  }

  cm_msg(MINFO,"e1n_compute","Rulefile %s written.",rulefile_name);

  return 1;
}  


INT e1g_compute(char *ppg_mode)
{
  INT i,status;
  
  if(debug)printf("e1g_compute is starting with ppg mode %s\n",ppg_mode);

  if(fmdarc.histograms.midbnmr.use_defaults)   
    {
      printf("e1g_compute: using default values for midbnmr parameters\n");
      if(strcmp (ppg_mode,"1j")==0)
	{  // one region of full time range
	  fmdarc.histograms.midbnmr.number_of_regions =1;
	  cm_msg(MINFO,"e1g_compute","by default defining %d region(s) for midbnmr",
		 fmdarc.histograms.midbnmr.number_of_regions);
	}
      else
	{  // three regions
	  fmdarc.histograms.midbnmr.number_of_regions =3;
	  cm_msg(MINFO,"e1g_compute","by default defining %d regions for midbnmr; USER must setup midbnmr bin ranges",
		 fmdarc.histograms.midbnmr.number_of_regions);
	}
      

      /*  write defaults for midbnmr */
      for(i=0; i <  fmdarc.histograms.midbnmr.number_of_regions; i++)
	{
	  fmdarc.histograms.midbnmr.last_time_bin[i] = fmdarc.histograms.num_bins;
	  fmdarc.histograms.midbnmr.first_time_bin[i] =1;
	  //  sprintf(fmdarc.histograms.midbnmr.range_label[i],"");
	  fmdarc.histograms.midbnmr.range_label[i][0] ='\0';
	}
      /* clean out the unused regions */
      for (i= fmdarc.histograms.midbnmr.number_of_regions; i <  MAX_MIDBNMR_REGIONS; i++)
	{
	  //sprintf(fmdarc.histograms.midbnmr.range_label [i],"");
	  fmdarc.histograms.midbnmr.range_label[i][0] ='\0';
	  fmdarc.histograms.midbnmr.first_time_bin[i] =  0;
	  fmdarc.histograms.midbnmr.last_time_bin[i] = 0 ;
	}
      
      if (strcmp (ppg_mode,"1j")==0)
	{
	  printf("  Midbnmr scan start value(%.2f) * integer conversion factor  : %d\n",
		 ppg.input.e1c_camp_start, fmdarc.histograms.midbnmr.start_freq__hz_  );
	  printf("  Midbnmr scan  end  value(%.2f) * integer conversion factor  : %d\n", 
		 ppg.input.e1c_camp_stop,  fmdarc.histograms.midbnmr.end_freq__hz_  );
	}
      else
	{
	  fmdarc.histograms.midbnmr.start_freq__hz_ = ppg.input.frequency_start__hz_;
	  fmdarc.histograms.midbnmr.end_freq__hz_ = ppg.input.frequency_stop__hz_;
	  
	  printf("  Midbnmr start_freq (Hz)   : %d\n", fmdarc.histograms.midbnmr.start_freq__hz_  );
	  printf("  Midbnmr end_freq   (Hz)   : %d\n", fmdarc.histograms.midbnmr.end_freq__hz_  );
	}
      printf("  Midbnmr first time bin all %d regions    : %d\n", fmdarc.histograms.midbnmr.number_of_regions, 1 );
      printf("  Midbnmr last time bin all %d regions     : %d\n",  fmdarc.histograms.midbnmr.number_of_regions,
	     fmdarc.histograms.num_bins  );
    }
  else
    {
      status = check_regions(); /* check midbnmr regions */
      if (status <= 0)
	{
	  printf("e1g_compute: error returned from check_regions\n");
	  return(status);
	}
    }
  status = e00_compute(ppg_mode); // identical to e00
  if (status <= 0)
    {
      printf("e1g_compute: error returned from e00_compute\n");
      return(status);
    }
  return 1;
}

INT check_regions(void)
{
  /* check that the regions for midbnmr are between 0 and num histo bins,
  */
  INT i;
  
  printf("check_regions: Midbnmr parameters \n");
  printf("  Number of bins             : %d\n", fmdarc.histograms.num_bins);
  printf("  Number of regions          :%d\n",  fmdarc.histograms.midbnmr.number_of_regions);
  if( (fmdarc.histograms.midbnmr.number_of_regions > MAX_MIDBNMR_REGIONS) ||
      (fmdarc.histograms.midbnmr.number_of_regions < 1 ))
    {
      cm_msg(MERROR,"check_regions","no. of midbnmr regions defined (%d) is out of range (min=1 max=%d)",
	     fmdarc.histograms.midbnmr.number_of_regions, MAX_MIDBNMR_REGIONS);	     
      return -1;
    }

  for (i=0; i< fmdarc.histograms.midbnmr.number_of_regions; i++)
    {
      printf("  Midbnmr Region: %d   Range_label: \"%-12s\" first bin: %d  last bin: %d\n",
	     i, fmdarc.histograms.midbnmr.range_label[i], 
	     fmdarc.histograms.midbnmr.first_time_bin[i],
	     fmdarc.histograms.midbnmr.last_time_bin[i]);
    } 
  printf("  Midbnmr start_freq (Hz)  : %d\n", fmdarc.histograms.midbnmr.start_freq__hz_  );
  printf("  Midbnmr end_freq   (Hz)  : %d\n", fmdarc.histograms.midbnmr.end_freq__hz_  );
  
  /* sanity check ... midbnmr handles this check now  */
#ifdef GONE
  j=0;
  for (i=0; i<fmdarc.histograms.midbnmr.number_of_regions; i++)
    {   
      if ((fmdarc.histograms.midbnmr.last_time_bin[i]  >  fmdarc.histograms.num_bins)  ||
	  (fmdarc.histograms.midbnmr.last_time_bin[i]  <= 0) )
	{
	  cm_msg(MERROR,"check_regions","last time bin for region %d (i.e. %d) out of range( >max(%d) or <= 0) ",
		 i, fmdarc.histograms.midbnmr.last_time_bin[i] , fmdarc.histograms.num_bins );
	  return -1;
	}
      if ((  fmdarc.histograms.midbnmr.first_time_bin[i] >  fmdarc.histograms.num_bins) ||
	  (  fmdarc.histograms.midbnmr.first_time_bin[i] < 0) )
	{
	  cm_msg(MERROR,"check_regions","first time bin for region %d (i.e. %d) out of range( >max(%d) or < 0) ",
		 i, fmdarc.histograms.midbnmr.first_time_bin[i] , fmdarc.histograms.num_bins );
	  return -1;
	}
      /* fill temporary array */
      temp[j]= fmdarc.histograms.midbnmr.first_time_bin[i];
      j++;
      temp[j]= fmdarc.histograms.midbnmr.last_time_bin[i];
      j++;
    }
  
  for ( j=0; j < 2*fmdarc.histograms.midbnmr.number_of_regions-1; j++)
    {
      if (temp[j] >= temp[j+1] )
	{
	  cm_msg(MERROR,"check_regions","regions overlap (region %d)",(INT)j/2);
	  return -1;
	}
    }
#endif

  return SUCCESS;
}
















